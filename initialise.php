<?php
if(($server_ip = @$_SERVER['SERVER_ADDR']) == '127.0.0.1' || @$_SERVER['OS'] == 'Windows_NT'){
	define('SERVERTYPE','local');
	require_once('C:\xampp\htdocs\passionstreet3\config.php');
}
else{
	if(strpos($_SERVER['HTTP_HOST'],'elasticbeanstalk'))
	{
	define('SERVERTYPE','aws');
	require_once('/var/app/current/config.php');
	}
	else if(strpos(__DIR__,'passionstreet3') || strpos($_SERVER['REQUEST_URI'],'passionstreet3') === true || (strpos($_SERVER['HTTP_HOST'],'test.passionstreet.in') > -1))
	{
	
	define('SERVERTYPE','stg');
	require_once('/home/passionstreet/public_html/passionstreet3/config.php');
	}
	else
	{
	define('SERVERTYPE','live');
	require_once('/home/passionstreet/public_html/config.php');
	}
}
require_once(CORE_DIR.'/PSUtils.php');

//require_once('config.php');
require_once(CORE_DIR.'/functions.php');
//require_once(CORE_DIR.'/Cache-1-5-6/Function.php');
require_once(ROOT_DIR.'/params.php');
require_once(ROOT_DIR.'/api.php');
//require_once(ROOT_DIR.'/includes.php');
@session_start();
$module_data = array();

//if(CACHING == true)
//$cache = new Cache_Function();

/*if(!loggedId()) bug resets ajax data also
{
	$_POST = array();
	$_FILES = array();
}*/

sanitizeAllInput();

if(SERVERTYPE != 'live'){
    $PSParams['server'] = SERVERTYPE;
    $PSParams['server_ip'] = $server_ip;
    $PSJavascript['server'] = 'local';
    $PSJavascript['clientid']['facebook'] = '652032041525469';
    $PSJavascript['clientid']['google'] = '264361764383-6dm4te0evqolkbc94m8u9ng2qgvbfi0h.apps.googleusercontent.com';
    $PSJavascript['clientid']['twitter'] = 'ExTLGEGlgTdHsDQusBnB5fNzV';
}
else
{
    $PSParams['server'] = SERVERTYPE;
    $PSParams['server_ip'] = $server_ip;
    $PSJavascript['server'] = 'live';
    $PSJavascript['clientid']['facebook'] = '601029180034751';
    $PSJavascript['clientid']['google'] = '702907059749-tb2dphgsgj1u29ch63d72bk0grcu3o3k.apps.googleusercontent.com';
    $PSJavascript['clientid']['twitter'] = 'ExTLGEGlgTdHsDQusBnB5fNzV';
}
$PSParams['canonical'] = $page_meta_defaults['url'];
$PSParams['clientid'] = $PSJavascript['clientid'];
$social['fb']['appid'] = $PSJavascript['clientid']['facebook'];
$PSJavascript['clientid']['twitterhandle'] = $GLOBALS['social']['tw']['handle'];

//filter_incoming_fields();
$PSParams['google_maps_key']=$google_maps_key;
$PSParams['isMobile']=$isMobile=isMobile();
$PSJavascript['isMobile']=$PSParams['isMobile'];
$PSJavascript['map_position'] = '{"lat": 28.63145, "lng": 77.21769}';
parse_str($_SERVER['QUERY_STRING'],$temp);
$PSJavascript['q'] = $temp;
$_SERVER['PHP_SELF_URL'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].@$_SERVER['REDIRECT_URL'];
$_SERVER['PHP_BROWSER_URL'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].@$_SERVER['REQUEST_URI'];
$PSJavascript['relurl'] = rtrim(str_replace(ROOT_PATH, '', $_SERVER['PHP_SELF_URL']),'/').'/';
$PSJavascript['orgurl'] = $_SERVER['PHP_SELF_URL'];
$PSJavascript['monthnamesfull'] = $PSParams['monthsfull'];
$PSJavascript['DEFAULT_IMG'] = DEFAULT_IMG;
$PSJavascript['DEFAULT_IMG_HORIZONTAL'] = DEFAULT_IMG_HORIZONTAL;
$PSJavascript['DEFAULT_IMG_VERTICAL'] = DEFAULT_IMG_VERTICAL;
$PSJavascript['login_callback'] = '';
$PSJavascript['minimal_login_callback'] = '';
$PSJavascript['profile_id'] = '';
$PSJavascript['parentwindowcontext'] = '';
$PSJavascript['currentuserlocation'] = array();

$PSJsincludes=array();

$PSJsincludes['core'][] = 'js/init.js';
$PSJsincludes['core'][] = 'js/common.js';
$PSJsincludes['core'][] = 'js/callback.js';
$PSJsincludes['core'][] = 'js/hello.js';
$PSJsincludes['core'][] = 'js/bootstrap.min.js';
$PSJsincludes['core'][] = 'js/jquery.unveil.js';
$PSJsincludes['internal'] = array();
//$PSJsincludes['external'][] = 'js/jquery.timeago.js';
//$PSJsincludes['external'][] = 'js/fontfaceobserver.js';
$PSJsincludes['external'][] = 'js/hoverpopup.js';
$PSJsincludes['external'][] = 'js/cleverinfinitescroll.js';
$PSJsincludes['external'][] = 'js/jquery.bxslider.min.js';
$PSJsincludes['external'][] = 'js/jBox.min.js';
$PSJsincludes['external'][] = 'js/jquery.touchSwipe.min.js';
$PSJsincludes['external'][] = 'js/history.js';
//$PSJsincludes['external'][] = 'js/summernote-master/dist/summernote.js';
if(!($isMobile))
{
$PSJsincludes['external'][] = 'js/jquery.nicescroll.min.js';
//$PSJsincludes['external'][] = 'https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js';
}
//$PSJsincludes['external'][] = 'js/bootstrap-select.min.js';
//$PSJsincludes['external'][] = 'js/bootstrap-fileinput/js/fileinput.js';
//$PSJsincludes['external'][] = 'js/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js';
if(!($isMobile))
$PSJsincludes['external'][] = 'js/stickykit.js';
if($isMobile)
$PSJsincludes['external'][] = 'js/isInViewport.js';

$PSCssincludes=array();
$PSCssincludes['external'][] = "css/jBox.css";
//$PSCssincludes['external'][] = "js/summernote-master/dist/summernote.css";
//$PSCssincludes['external'][] = "css/bootstrap-fileinput/css/fileinput.css";
require_once(CORE_DIR.'/db_conn.php');
require_once(CORE_DIR.'/user.php');
//print_array($_SERVER);
if((strpos($_SERVER['PHP_SELF'],'PSMailerclicktracker') === false) && (strpos($_SERVER['PHP_SELF'],'PSMaileropentracker') === false) && isset($_GET['mailer_id']) && isset($_GET['email']))
mailer_data_update($_GET['mailer_id'],$_GET['email'],'click');
//print_array(get_browser());
$userheaders = getallheaders();
$userauthtoken = array();
$userauthtoken['Host'] = $userheaders['Host'];
$userauthtoken['User-Agent'] = $userheaders['User-Agent'];
$userauthtoken['Accept-Language'] = $userheaders['Accept-Language'];
$temp = parse_url(@$userheaders['Referer']);
$userauthtoken['RefererDomain'] = (!empty($temp['host']))?$temp['host']:$userheaders['Host'];
$userauthtoken['Cookie'] = $userheaders['Cookie'];
$userauth_current = (string)serialize($userauthtoken);
unset($temp);
unset($userheaders);
$PSData['user'] = user_profile();
if($PSData['user']['profile_id'])
load_session();
?>