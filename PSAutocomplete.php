<?php
require_once('initialise.php');
$header = getallheaders();
if ($header['X-Requested-With'] != 'XMLHttpRequest')
{
	//$response['status'] = '403';
	//preventing ajax file call from script
}

$type               = $_GET['type'];
$profile_id         = $_SESSION['user']['profile_id'];
$response['status'] = '200';

ob_start();
unset($_POST['login_status']);
unset($_POST['token']);
unset($_GET['login_status']);
unset($_GET['token']);

$searchParam = $_REQUEST['term'];
$actionType = $_REQUEST['type'];
$loggedInUserId = $_SESSION['user']['id'];

if ($actionType == 'tagMember') {
	$notInuser = isset($_REQUEST['taggedIds']) === true ? $_REQUEST['taggedIds'].",$loggedInUserId" : "$loggedInUserId";
	$notInuser = trim($notInuser ,',');
	$where = " id NOT IN(".$notInuser.") AND (fname LIKE '%$searchParam%' OR lname LIKE '%$searchParam%')";
	if ($_REQUEST['taggedIds'] != '') {
		$where = " id NOT IN(".$_REQUEST['taggedIds'].") AND (fname LIKE '%$searchParam%' OR lname LIKE '%$searchParam%')";
	}

	$query = "SELECT pr.id, concat(pr.fname,' ',pr.lname) AS value, pr.email, pr.userdp, pr.profile_id FROM profile AS pr WHERE $where LIMIT 10";
	$result = $connection->fetchAll($query, array());
	
} elseif ($actionType == 'searchCommunityMember') {
	$where = "(fname LIKE '%$searchParam%' OR lname LIKE '%$searchParam%')";	
	$sql = "SELECT pr.id, concat(pr.fname,' ',pr.lname) AS name, pr.email, pr.userdp, pr.profile_id  FROM `profile` as pr WHERE $where LIMIT 10";
	$result = $connection->fetchAll($sql, array());
} else {
	$where = " (fname LIKE '%$searchParam%' OR lname LIKE '%$searchParam%')";	
	$sql   = "SELECT pr.id, concat(pr.fname,' ',pr.lname) AS name, pr.email, pr.userdp, pr.profile_id  FROM `profile` as pr LEFT JOIN group_invites AS gi ON gi.invitedTo = pr.id WHERE $where AND pr.id != ? AND gi.invitedTo IS NULL LIMIT 10";
	$result = $connection->fetchAll($sql, array($loggedInUserId));

	if (empty($result) === true) {
		$result 	= array();
		$result[] 	= array(
			'id' 			=> '0',
			'name' 			=> $_REQUEST['term'],
			'email' 		=> $_REQUEST['term'],
			'userdp' 		=> $_REQUEST['userdp'],
			'profile_id' 	=> $_REQUEST['profile_id']
		);
	}
}

$response 		= $result;
$responsedata 	= ob_get_contents();
ob_end_clean();


echo json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

?>