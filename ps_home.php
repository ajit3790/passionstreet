<?php
require_once('initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'section1'
             ),
             'rowWrapper'=>array("<div class='container sm'>","</div>"),
             'columnStructure' =>true,
             'columns'=>array(
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-12'
                     ),
                    'modules'=>array(
                         /*'eventlisting'=>array(
                            'modulesParams'=>array('module_display_style'=>'boxed_coresel_2')
                          )*/
                     )
                     
		 )
		)
        ),
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'rowWrapper'=>array("<div class='container sm'>","</div>"),
             'columnStructure' =>true,
             'columns'=>array(
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-7'
                     ),
                    'modules'=>array(
                         'wallform'=>array(
                            'modulesParams'=>array()
                          ),
			 'wall'=>array(
                            'modulesParams'=>array()
                          )
                     )
                     
                 ),
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-5 sidebar-right sidebar'
                     ),
                    'modules'=>array(
                         'invite'=>array(
                            'modulesParams'=>array('module_display_style'=>'3')
                          ),
                         'eventlisting'=>array(
                            'modulesParams'=>array('module_display_style'=>'boxed_coresel')
                          ),
                          /*'adbanner'=>array(
                            'modulesParams'=>array("adtype"=>"event")
                          ),*/
                          'memberlisting'=>array(
                            'modulesParams'=>array()
                          ),                         
                         /*'invite'=>array(
                            'modulesParams'=>array()
                          ) ,*/
                          'adbanner'=>array(
                            'modulesParams'=>array("adtype"=>"event")
                          ) 
                     )
                     
                 )
            )
        )
);

$page_meta = array(
	'title'=>'PASSIONSTREET- Home'
);
if(!loggedId())
header('location:'.ROOT_PATH); 
echo render_modules();


?>