<?php
require_once('initialise.php');
//$url = $_REQUEST['url'];
//if (preg_match('/\b(https?|ftp):\/\/*/', $url) !== 1) die;
//echo (file_get_contents($url));

//error_reporting(E_ALL);
//ini_set('display_errors', 1);


/* Site to forward requests to.  */
// $site = 'http://remotesite.domain.tld/';
$site = trim(urldecode($_GET['psurl']));
if(empty($site))
exit();
unset($_GET['psurl']);
$queryParams = '';

foreach($_GET as $key=>$value)
{
	$queryParams .= '&'.$key.'='.$value;
}
if(!empty($queryParams))
$site = $site.'?'.$queryParams;

/* Domains to use when rewriting some headers. */
$remoteDomain = ROOT_PATH;
$proxyDomain = ROOT_PATH;

//$request = $_SERVER['REQUEST_URI'];
//print_array($_SERVER['REQUEST_URI']);

$ch = curl_init();

/* If there was a POST request, then forward that as well.*/
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
}
// curl_setopt($ch, CURLOPT_URL, $site . $request);
curl_setopt($ch, CURLOPT_URL, $site . $request);
curl_setopt($ch, CURLOPT_HEADER, TRUE);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$headers = getallheaders();

/* Translate some headers to make the remote party think we actually browsing that site. */
$extraHeaders = array();
if (isset($headers['Referer'])) 
{
    $extraHeaders[] = 'Referer: '. str_replace($proxyDomain, $remoteDomain, $headers['Referer']);
}
if (isset($headers['Origin'])) 
{
    $extraHeaders[] = 'Origin: '. str_replace($proxyDomain, $remoteDomain, $headers['Origin']);
}

/* Forward cookie as it came.  */
curl_setopt($ch, CURLOPT_HTTPHEADER, $extraHeaders);
if (isset($headers['Cookie']))
{
    curl_setopt($ch, CURLOPT_COOKIE, $headers['Cookie']);
}
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$response = curl_exec($ch);
$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
$headers = substr($response, 0, $header_size);
$body = substr($response, $header_size);

$headerArray = explode(PHP_EOL, $headers);
//print_array($headerArray);
/* Process response headers. */
$newCacheTime = 0;
$renderhtml = 0;
foreach($headerArray as $header)
{
    $colonPos = strpos($header, ':');
    if ($colonPos !== FALSE) 
    {
        $headerName = substr($header, 0, $colonPos);
        $headerName = trim($headerName);
        /* Ignore content headers, let the webserver decide how to deal with the content. */
        if ($headerName == 'Content-Encoding') continue;
        if ($headerName == 'Content-Length') continue;
        if ($headerName == 'Transfer-Encoding') continue;
        if ($headerName == 'Location') continue;
        if ($headerName == 'Cache-Control'){
		$pos = strpos($header,'max-age=',14);
		if($pos !== false)
		{
			$end = strpos($header, ' ',$pos+7);
			if(empty($end))
			$end = strlen($header);
			$start = strpos($header, '=',$pos+7);
			$maxage = substr($header, $start+1, $end);
			$newCacheTime = (int)$maxage;
			$setDefaultCacheHeader = 0;
		}
	}
	if($headerName == 'Content-Type')
        {
            if(strpos($header,'text/html')>-1)
            $renderhtml = 1;        
        }
	continue;
        /* -- */
        /* Change cookie domain for the proxy */
        if (trim($headerName) == 'Set-Cookie')
        {
            $header = str_replace('domain='.$remoteDomain, 'domain='.$proxyDomain, $header);
        }
        /* -- */

    }
    header($header, FALSE);
}
if($renderhtml)
echo str_replace('<head>','<head><base href="'.$site.'">',$body);
else
echo $body;    

curl_close($ch);
?>