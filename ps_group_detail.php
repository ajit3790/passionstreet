<?php
require_once('initialise.php');

$groupId    = isset($_REQUEST['communityId']) ? trim($_REQUEST['communityId']) : '';
$actionName = isset($_REQUEST['action']) ? trim($_REQUEST['action']) : 'home';

$groupDetail = $connection->fetchAssoc("SELECT * FROM groups WHERE groupId = ?", array($groupId));
if ($groupDetail === NULL) {
    header('Location:'.ROOT_PATH.'communities');die;
}

$groupMembers = array();

// Get user's permission
$groupUserPermission = $connection->fetchAssoc("SELECT * FROM group_members WHERE groupId = ? AND userId = ?", array($groupId, $_SESSION['user']['id']));

// Get joined members
$totalGroupMembers = $connection->fetchAssoc("SELECT count(1) AS totalMembers FROM group_members WHERE groupId = ? AND isMember = ?", array($groupId, '1'));

// Join Group
if ($actionName === 'join') {
   $isCreated = $connection->executeUpdate("INSERT INTO group_members(groupId, userId, role, isMember, isFollower, status) VALUES(?, ? ,? ,?, ?, ?)", array($groupId, $_SESSION['user']['id'], '0', '1', '1', '1'));
    header('Location:'.ROOT_PATH.'community/'.$groupId);die;
} elseif ($actionName === 'edit') {
    $middleContent = array(
        'sectionParams' => array(
            'class'=>'container-fluid profile-box'
        ),
        'rowWrapper' => array("<div class='container sm'>","</div>"),
        'modules' => array(
            'groupCreate' => array(
                'modulesParams' => array()
            )
        )
    );
} elseif ($actionName === 'members') {
    // Show group members
    $middleContent = array(
        'sectionParams' => array(
            'class'=>'section'
        ),
        'rowWrapper' => array("<div class='container sm'>","</div>"),
        'modules' => array(
            'groupMembers' => array(
                'modulesParams' => array()
            )
        )
    );
} else {
    $includeMiddleModules = array();
    /*
    if (isset($groupUserPermission['role']) === true && $groupUserPermission['isMember'] == '1' && (($groupUserPermission['role'] == 1 || $groupUserPermission['role'] == 2) || ($groupDetail['privacyType'] == 'public') || ($groupDetail['postingPermission'] == 1) ) ) {
        $includeMiddleModules['wallform'] = array(
            'modulesParams' => array()
        );
    }
    */

    $allowPosting = false;
    if ($groupDetail['postingPermission'] == 1) {
        $allowPosting = true;
    } else if (isset($groupUserPermission['role']) === true && in_array($groupDetail['postingPermission'], array(2,3)) === true ) {
        $allowPosting = true;
    } else if (isset($groupUserPermission['role']) === true && in_array($groupUserPermission['role'], array(1, 2)) === true) {
        $allowPosting = true;
    }

    if ($allowPosting === true) {
        $includeMiddleModules['wallform'] = array(
            'modulesParams' => array()
        );
    }

    if ($allowPosting === true || isset($groupUserPermission['role']) === true) {
        $includeMiddleModules['wall'] = array(
            'modulesParams' => array()
        );
    } else {
        $includeMiddleModules['groupMembers'] = array(
            'modulesParams' => array()
        );
    }

    $middleContent = array(
        'sectionParams' => array(
            'class' => 'section'
        ),
        'rowWrapper' => array("<div class='container sm'>","</div>"),
        'columnStructure' => true,
        'columns' =>  array (
            array(
                'columnParams'=>array(
                    'class'=>'col-md-7'
                ),
                'modules' => $includeMiddleModules
            ),
            array(
                'columnParams'=>array(
                    'class'=>'col-md-5'
                ),
                'modules' => array(
                    'groupSuggestion' => array(
                        'modulesParams' => array()
                    )
                )
            )
        )
    );
}

if ($actionName === 'edit') {
    $rows = array(
        "row_1" => array(
            'sectionParams' => array(
                'class' => 'section'
            ),
            'rowWrapper' => array("<div class='container sm'>","</div>"),
            'modules' => array(
                'groupCreate' => array(
                    'modulesParams' => array()
                )
            )
        )
    );

} else {
    $rows = array(
        "row_1" => array(
            'sectionParams' => array(
                'class' => 'container-fluid group-detail-box'
            ),
            'rowWrapper' => array("<div class='container sm'>","</div>"),
            'modules' => array(
                'groupCoverPhoto'   => array(
                    'modulesParams' => array()
                )
            )
        ),
        "row_2" => $middleContent
    );
}

$page_meta = array(
	'title' => 'Group'
);
echo render_modules();
?>
