<?php


require_once('initialise.php');

$PSJavascript['login_required'] = false;

$rows = array(
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
	     'rowWrapper'=>array("<div class='container sm'>","</div>"),
             'columnStructure' =>true,
             'columns'=>array(
                /* array(
                     'columnParams'=>array(
                        'class'=>'col-md-3 sidebar-left sidebar'
                     ),
                    'modules'=>array(
                         'memberlisting'=>array(
                            'modulesParams'=>array()
                          )
                     )
                     
                 ),*/
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-7'
                     ),
                    'modules'=>array(
                         'postdetail'=>array(
                            'modulesParams'=>array()
                          ),
			  'wall'=>array(
                            'modulesParams'=>array()
                          )
                     )
                     
                 ),
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-5 sidebar-right sidebar'
                     ),
                    'modules'=>array(
                         'profiledetail'=>array(
                            'modulesParams'=>array('module_display_style'=>'minimal','attrs'=>'style="margin-left:0px"')
                          ),
			  'eventlisting'=>array(
                            'modulesParams'=>array()
                          ),
			  'memberlisting'=>array(
                            'modulesParams'=>array()
                          )
			  
                     )
                 )
            )
        )
);




$page_meta = array(


	'title'=>'Passion Street'


);





$page_includes = array(


	"header"=>1,


	"sitemap"=>1,


	"footer"=>1,


        "navigation"=>1


);	





echo render_modules();





?>