<?php
require_once('initialise.php');
$header = getallheaders();

$requestedActionName = isset($_REQUEST['requestType']) === true ? $_REQUEST['requestType'] : '';
$groupId = isset($_REQUEST['groupId']) === true ? $_REQUEST['groupId'] : '';

if ($requestedActionName == 'publishActivity') {
    $moduleId = isset($_REQUEST['fmId']) === true ? $_REQUEST['fmId'] : '';
    $createdAt = date('Y-m-d H:i:s');    
    $tagged_members = array();
    
    if (isset($_POST['membersId']) === true && $_POST['membersId'] != '') {
        $memberIds = explode(',', $_POST['membersId']);
        $memberNames = explode(',', $_POST['tagged_members']);
        $memberPIds = explode(',', $_POST['membersPID']);
        
        foreach ($memberIds as $key => $id) {
            $tagged_members[] = array(
                'id'    => $id,
                'name'  => $memberNames[$key],
                'profile_id' => $memberPIds[$key]
            );
        }
    }
    // Tagged members
    $members    = serialize($tagged_members);

    $_POST['log']['workouttype'] = $_POST['workouttype'];
    $workout    = serialize($_POST['log']);
    $images     = '';
    
    $imageArray = isset($_POST['photoes']) ? $_POST['photoes'] : '';
    if ($imageArray != '') {
        $orgImages  = move_images_from_temp($imageArray,'posts');
        $thumbImages= create_thumbnail($orgImages);
        $images     = get_image_ratio($orgImages);
        $images     = serialize($images);
    }

    $category   = $_POST['category'];
    $tagMembers = $members;
    $postParams = array(
        $moduleId,
        $_POST['fm'],
        $_POST['ps_uid'],
        $category,
        $images,
        $_POST['postsummary'],
        $workout,
        $_POST['video'],
        $_POST['videoMeta'],
        $tagMembers,
        $createdAt
    );

    $connection->executeUpdate("INSERT INTO ps_posts(moduleId, module, createdBy, category, images, about, workout, video, videoMeta, tagged_members, createdAt) VALUES(?, ?, ? ,? ,?, ?, ?, ?, ?, ?, ?)", $postParams);

    $lastInsetId = $connection->lastInsertId();
    
    $postData = group_posts(array('postId' => $lastInsetId));
    
    $userfullName = ucfirst($_SESSION['user']['fname']).' '.ucfirst($_SESSION['user']['lname']);
    $postData[0]['createrseopath']= get_alphanumeric($userfullName).'/'.$_SESSION['user']['profile_id'];

    format_post($postData[0]);
  
} elseif ($requestedActionName == 'groupDiscoverList') {
    $userId = isset($_REQUEST['user']) === true ? $_REQUEST['user'] : '0';
    $category = isset($_REQUEST['category']) === true ? $_REQUEST['category'] : 'all';

    // find group by gooup'scategory
    if ($category == 'all') {
        //$searchedGroups = $connection->fetchAll("SELECT gp_gm.* FROM (SELECT count(gm.groupId) AS totalMembers, gp.* FROM groups AS gp RIGHT JOIN group_members AS gm ON gp.groupId = gm.groupId WHERE gm.userId <> ?) AS gp_gm WHERE totalMembers > 0 LIMIT ?", array($userId, 6));
        $searchedGroups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId GROUP BY gm.groupId) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId != ? GROUP BY gm.groupId) AS temp_2 ON temp_2.totalMembers = temp_1.totalMembers AND temp_1.groupId = temp_2.groupId  LIMIT ?", array($userId, 6));
    } else {
        //$searchedGroups = $connection->fetchAll("SELECT gp_gm.* FROM (SELECT count(gm.groupId) AS totalMembers, gp.* FROM groups AS gp RIGHT JOIN group_members AS gm ON gp.groupId = gm.groupId WHERE gm.userId <> ? AND gp.category =?) AS gp_gm WHERE totalMembers > 0 LIMIT ?", array($userId, $category, 6));

        $searchedGroups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId GROUP BY gm.groupId) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId != ? GROUP BY gm.groupId) AS temp_2 ON temp_2.totalMembers = temp_1.totalMembers AND temp_1.groupId = temp_2.groupId  WHERE temp_1.category = ? LIMIT ?", array($userId, $category, 6));
    }
    
    $responseHtml = '';
    foreach ($searchedGroups as $key => $group) {
        $totalMembers   = $group['totalMembers'];
        $groupId        = $group['groupId'];
        $groupCategory  = $group['category'];
        $groupName      = $group['name'];
        $groupPhoto     = $group['photo'];

        $responseHtml  .= '<div class="col-md-4">';
        $responseHtml  .= '<div class="bx-styl clearfix">';
        $responseHtml  .= "<figure> <a href='group/".$groupId."' title=''>";
        $responseHtml  .= "<img class='unveil' alt='$groupName' src='".DEFAULT_IMG_HORIZONTAL."' data-src=''></a></figure>";
        $responseHtml  .= "<div class='description'><h4><a href='group/".$groupId."' title='".$groupName."'>".$groupName."</a></h4></div>";
        $responseHtml  .= "<div>Category: $groupCategory</div><div>Members: $totalMembers Followers : 1000+</div>";
        $responseHtml  .= '<div class="footer"><button class="btn btn-primary">Be A member</button><button class="btn btn-primary">Follow</button></div> </div></div>';
    }
    $result = array(
        'result' => $responseHtml
    );
    echo json_encode($result);
    
} elseif ($requestedActionName == 'leaveGroup') {
    $response = array();
    $deleted = $connection->executeUpdate("DELETE FROM group_members WHERE groupId = ? AND userId =?", array($groupId, $_SESSION['user']['id']));
        
        $response['responseCode'] = '2001';// Deleted

    $checkOtherAdmin = $connection->fetchAssoc("SELECT pr.id AS userId, pr.profile_id AS profileId, pr.fname, pr.lname FROM group_members AS gm JOIN profile AS pr ON pr.id = gm.userId WHERE gm.groupId = ? LIMIT ?", array($groupId, 1)); 
    
    if (isset($checkOtherAdmin['userId']) === false) {
        $connection->executeUpdate("DELETE FROM groups WHERE groupId = ?", array($groupId));
        $response['responseCode'] = '2002';// Group deleted
    }

   // TODO:: Need to handle admin scenario here
   /*
   $checkOtherAdmin = $connection->fetchAssoc("SELECT pr.id AS userId, pr.profile_id AS profileId, pr.fname, pr.lname FROM group_members AS gm JOIN profile AS pr ON pr.id = gm.userId WHERE gm.groupId = ? LIMIT ?", array($groupId, 1));

    $response['responseCode'] = '2000';// Not able to deleted

    if (isset($checkOtherAdmin['userId']) === true) {
        $deleted = $connection->executeUpdate("DELETE FROM group_members WHERE groupId = ? AND userId =?", array($groupId, $_SESSION['user']['id']));
        $response['responseCode'] = '2001';// Deleted
    }
    */

    $response['result'] = 'Exit';
    echo json_encode($response);
    die;
} elseif ($requestedActionName == 'joinGroup') {
    $response = array();
    $isemberExist = $connection->fetchAssoc("SELECT id FROM group_members WHERE groupId =? AND userId =? ", array($groupId, $_SESSION['user']['id'])); 
    if (isset($isemberExist['id']) === true) {
        $isCreated = $connection->executeUpdate("UPDATE group_members SET isMember =? WHERE id = ?", array('1', $isemberExist['id']));
    } else {
        $isCreated = $connection->executeUpdate("INSERT INTO group_members(groupId, userId, role, isMember, isFollower, status) VALUES(?, ? ,? ,?, ?, ?)", array($groupId, $_SESSION['user']['id'], '0', '1', '1', '1'));
    }
    
    $connection->executeUpdate("DELETE FROM group_invites WHERE invitedTo = ? AND groupId = ?", array($_SESSION['user']['id'], $groupId));

    $response['responseCode'] = '2003';
    if ($isCreated) {
        $response['responseCode'] = '2004'; // Not joined
    }
    $response['result'] = 'Joined';
    echo json_encode($response);exit;

} elseif ($requestedActionName == 'followGroup') {
    $isCreated = $connection->executeUpdate("INSERT INTO group_members (groupId, userId, role, isFollower, isMember, status) VALUES(?, ?, ?, ?, ?, ?)", array($groupId, $_SESSION['user']['id'], '0', '1', '0', '1'));

    $response = array();
    $response['result'] = 'Requested action has been completed';
    $response['responseCode'] = '2005';// Not joined
    echo json_encode($response);exit;
} elseif ($requestedActionName == 'unfollowGroup') {
    $connection->executeUpdate("DELETE FROM group_members WHERE groupId=? AND userId=?", array($groupId, $_SESSION['user']['id']));
   
    $response = array();
    $response['result'] = 'Requested action has been completed';
    $response['responseCode'] = '2006';// Not joined
    echo json_encode($response);exit;
} elseif ($requestedActionName == 'editPost') {
    $connection->executeUpdate("UPDATE ps_posts SET about=? WHERE id=?", array($_REQUEST['about'],$_REQUEST['id']));
    $response = array();
    $response['result'] = 'Requested action has been completed';
    $response['responseCode'] = '2006';// Not joined
    echo json_encode($response);exit;
} elseif ($requestedActionName == 'editPostComment') {
    $connection->executeUpdate("UPDATE group_comments SET comment=? WHERE id=?", array($_REQUEST['comment'],$_REQUEST['id']));
    $response = array();
    $response['result'] = 'Requested action has been completed';
    $response['responseCode'] = '2006';// Not joined
    echo json_encode($response);exit;
} elseif ($requestedActionName == 'deletePost') {
    $connection->executeUpdate("UPDATE ps_posts SET status = ? WHERE id=?", array('0', $_REQUEST['id']));
    $response = array();
    $response['result'] = 'Requested action has been completed';
    $response['responseCode'] = '2006';// Not joined
    echo json_encode($response);exit;
} elseif ($requestedActionName == 'deleteGroupComment') {
    $connection->executeUpdate("UPDATE group_comments SET status =? WHERE id = ?", array('0',$_REQUEST['id']));
    $response = array();
    $response['result'] = 'Requested action has been completed';
    $response['id'] = $_REQUEST['id'];// Not joined
    $response['responseCode'] = '2006';// Not joined
    echo json_encode($response);exit;
}  elseif ($requestedActionName == 'memberAdmin') {
    $connection->executeUpdate("UPDATE group_members SET role = ? WHERE userId = ? AND groupId =?", array($_REQUEST['requestId'], $_REQUEST['memberId'], $_REQUEST['groupId']));
    $response = array();
    $response['result'] = 'Requested action has been completed';
    $response['responseCode'] = '2006';// Not joined
    echo json_encode($response);exit;
} elseif ($requestedActionName == 'inviteMember') {
    $alreadyInvited = false;
    $response   = array();

    $groupId    = isset($_REQUEST['groupId']) ? $_REQUEST['groupId'] : 0;
    $userId     = isset($_REQUEST['userId']) ? $_REQUEST['userId'] : 0;
    $memberId   = isset($_REQUEST['memberId']) ? $_REQUEST['memberId'] : 0;
    
    $invitedToEmail = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
    if ($memberId < 1) {
       $isMemberExist = $connection->fetchAssoc("SELECT id FROM group_invites WHERE groupId =? AND invitedToEmail =? ", array($groupId, $invitedToEmail)); 
        if (isset($isMemberExist['id']) === true) {
            $alreadyInvited = true;
        }
    } else {
        $invitedToEmail = isset($_REQUEST['emid']) ? base64_decode($_REQUEST['emid']) : '';
    }
    
    if ($alreadyInvited === false) {
        $community = getCommunityById($groupId);
        $requesterName = $_SESSION['user']['fname'].' '.$_SESSION['user']['lname'];        

        $title = $requesterName.' has invited you to join the community:: '.$community['name'].' on PASSIONSTREET';
        $desc = $requesterName.' has invited you to join the community <b>'.$community['name'].'</b> on PASSIONSTREET.';
        
        $img = ROOT_PATH.'userbg';
        if ($community['photo'] != '') {
            $img = ROOT_PATH.'files/'.$community['photo'];
        }

        $desc .= "<tr><td style='padding-top:10px; padding-bottom:15px;line-height:22px'><img src='$img' width='100%'></td></tr>";
        if ($community['description'] != '') {
            $desc .= '<tr><td style="padding-bottom:8px;line-height:22px">'.$community['description'].'</td></tr>';
        }
        $groupUrl = ROOT_PATH."group/".$community['groupId'];
        $desc .= "<tr><td style='padding-bottom:10px;line-height:22px'><a href='$groupUrl' target='_blank' style='color:#1e7cdc;text-decoration:underline'> <span class='il'>Click here</span></a> to join the community.</td></tr>";
                    
        mailer(array("profile_id"=>0,"email_id"=>$invitedToEmail),'default', array('data'=>$desc,'title'=>$title));

        $connection->executeUpdate("INSERT INTO group_invites(groupId, invitedBy, invitedTo, invitedToEmail, status) VALUES(?, ? ,? ,?, ?)", array($groupId, $userId, $memberId, $invitedToEmail, '1'));
        
        $response['responseCode'] = '2006';
        $response['result'] = 'Requested action has been completed';
    } else {
        $response['responseCode'] = '5001';
        $response['result'] = $invitedToEmail.' has already been invited';
    }
    echo json_encode($response);exit;

} elseif ($requestedActionName == 'addComment') {
    $response['message'] = 'Requested action has been completed';
    $createdAt = date('Y-m-d H:i:s');
    $connection->executeUpdate("INSERT INTO group_comments(postId, userId, comment, createdAt) VALUES(?, ? , ?, ?)", array($_REQUEST['postId'], $_SESSION['user']['id'], $_REQUEST['comment'], $createdAt));
    
    $userfullName = ucfirst($_SESSION['user']['fname']).' '.ucfirst($_SESSION['user']['lname']);
    $response['result'] = array(
        'name'      => $userfullName,
        'userdp'    => $_SESSION['user']['userdp'],
        "comment"   => $_REQUEST['comment'],
        "id"        => $connection->lastInsertId(),
        "profileUrl"=> ROOT_PATH.'profile/'.get_alphanumeric($userfullName).'/'.$_SESSION['user']['profile_id']
    );
    echo json_encode($response);exit;
} elseif ($requestedActionName == 'likePost') {
    if ($_REQUEST['liked'] == '1') {
        $affectedRows = $connection->executeUpdate("DELETE FROM group_likes WHERE postId =? AND userId = ?", array($_REQUEST['postId'], $_SESSION['user']['id']));
        $response['message'] = 'You have unliked this post.';
    } else {
        $affectedRows = $connection->executeUpdate("INSERT IGNORE INTO group_likes(postId, userId) VALUES(?, ?)", array($_REQUEST['postId'], $_SESSION['user']['id']));
        $response['message'] = 'You have liked this post.';
    }
    $response['affectedRows'] = $affectedRows;
    
    echo json_encode($response);exit;    
} else if ($requestedActionName == 'changeGroupCoverPhoto') {        
    $response['data'] = '';
    $targetPath = $_POST['pictype'];
    if ($_POST['multicropdata'][$targetPath]) {
        $imgpaths = upload_post_images_new2($_FILES, $targetPath);
        $imgpath  = $imgpaths[0];
        if ($imgpath) {
            $affectedRows = $connection->executeUpdate("UPDATE groups SET photo = ? WHERE groupId = ?", array($imgpath, $_REQUEST['groupId']));
        }
        $response['affectedRows'] = $affectedRows;
        $response['data'] = $imgpath;

    }
    echo json_encode($response);exit;    
}
else if ($requestedActionName == 'registerNotificationToken') {
    $affectedRows = $connection->executeUpdate("INSERT IGNORE INTO desktop_notification(token) VALUES(?)", array($_REQUEST['token']));
    $response = array();
    $response['result'] = 'Requested action has been completed';
    if ($affectedRows > 0) {
          $apiKey = 'AAAANoWLoyc:APA91bGNuxm2NolehcBLxC0yrSvq4b7m2QcDzsm4dYOu4DOuBbzkcNH8A-bz03b9uc0-cfeCyKJ2H2-c5Z6n3gBPJdVxBgoNuMjuyeIDpxugvcBF3yl21XzJi8T82-caExcS1hKCYBsE';
	$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
	$topic   = "/topics/passionst-topic";
	
	$registration_tokens=array($_REQUEST['token']);
	$post = array('to'=> $topic,'registration_tokens'=> $registration_tokens);
	$ch = curl_init();    
	curl_setopt($ch, CURLOPT_URL, 'https://iid.googleapis.com/iid/v1:batchAdd');
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
	curl_setopt($ch, CURLOPT_POST, true);     
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));  
	$result = curl_exec($ch);
	
	if (curl_errno($ch)) {
		//echo 'GCM error: ' . curl_error($ch);
		$response['result'] = curl_error($ch);
	} else {
		$response['result'] = 'Token has been registered';
	}
	
	curl_close($ch);  
	//$ret=json_decode($result,true);
	
	$response['payload'] = $result;
    }
    
    $response['rowsAffected'] = $affectedRows; //$connection->affected_rows();
            $response['responseCode'] = '2006';// Not joined
    echo json_encode($response);exit;
}
?>