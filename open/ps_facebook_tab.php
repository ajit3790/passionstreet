<?php
require_once('../initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
            ),
            'rowWrapper'=>array(),
            'modules'=>array(
                'social/facebooktab'=>array(
                )
            )
        )
);

//$page_meta['title'] = 'PASSIONSTREET : #LiveWhatYouLove - Follow, Connect, Share & Exhibit your Passion';
// $page_meta['description'] = 'In PASSIONSTREET follow and share the bond with others having similar passion,Connect and Interact with Expert,Explore and Join Groups,Participate in Events and Workshops,Exhibit and Track Record of your activities. It also allows you to Create and Manage Events - Single window for Ticketing, Interactions,Promotions and Analytics.';
//$page_meta['description'] = 'In PASSIONSTREET, it is all about your passion for doing something which you love to do the most in your life. Follow and share the bond with others having similar passion, connect and interact with experts, explore and join groups, participate in events, exhibit and track record of your activities. It also allows you creating and managing events - a single window for ticketing, interaction, promotion and analytics.';
//$page_meta['keywords'] = 'PASSIONSTREET,Events,Featured Events,Workshops,Seminar,Conference,Exhibitions,Charity,Sports and Fitness,Award Programs,Competitions,Reunion,Networking,Concerts,Members,Experts,Followers,Club,Friend Zone,Trails,Passion Zone , Passion Categories , Only Passion, PASSIONSTREET, Passion Trails, Marathon';

$page_includes = array(
	"header"=>1,
	"sitemap"=>1,
	"footer"=>1,
        "navigation"=>1
);	

$PSJavascript['login_required'] = false;

echo render_modules();

?>