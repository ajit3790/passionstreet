<?php
require_once('initialise.php');
$PSJavascript['login_required'] = true;

$rows = array(
    "row_1"=>array(
        'sectionParams'=>array(
            'class'=>'section'
         ),
         'rowWrapper'=>array("<div class='container sm'>","</div>"),
         'modules'=>array(
            'communities'=>array(
                'modulesParams' => array('module_display_style'=>'carousel_layout')
            )
        )
    )
);

echo render_modules();

?>