<?php
require_once('initialise.php');
$header = getallheaders();
$response['status'] = '200';
if (empty($_GET['module']))
{
	$response['status'] = '406';
}
else
{
	$actionsNotRequiringLogin          = array(
		"maps",
		"invite",
		"mailprocessor",
		"print"
	);
	$actionsRequiringMinimalLogin      = array();
	$currentActionRequiresLogin        = !in_array($_GET['module'], $actionsNotRequiringLogin);
	//$currentActionRequiresLogin = false;
	$currentActionRequiresMinimalLogin = in_array($_GET['module'], $actionsRequiringMinimalLogin);
	//print_array(array('JMA',$currentActionRequiresLogin && ($_REQUEST['login_status'] == 'false' || empty($_REQUEST['login_status']) || ($_REQUEST['login_status'] == 'true' && !loggedId()))));
	//exit();
	if ($currentActionRequiresMinimalLogin && empty($_SESSION['user']['profile_id']))
	{
		$response['status'] = '4051';
	}
	else if ($currentActionRequiresLogin && empty($_SESSION['user']['profile_id']))
	{
        //else if ($currentActionRequiresLogin && ($_REQUEST['login_status'] != 'true' || ($_REQUEST['login_status'] == 'true' && !loggedId())))
        	$response['status'] = '405';
	}
	else if (!authorized())
	{
		$response['status'] = '403';
	}
	else
	{
		$rows = array(
			"row_1" => array(
				'sectionParams' => array(
					'class' => 'section'
				),
				'rowWrapper' => array(
					"<div class='container'>",
					"</div>"
				),
				'columnStructure' => true,
				'columns' => array(
					array(
						'columnParams' => array(
							'class' => 'col-md-12'
						),
						'modules' => array(
							$_GET['module'] => array(
								'modulesParams' => array(
									''
								)
							)
						)
					)
				)
			)
		);
                $PSJavascript['parentwindowcontext'] = 'parent';
		$page_includes = array(
			"layout" => "internaliframe"
		);
                echo render_modules();
	}
}
if($response['status'] == '405')
{
	?>
	<script>
		parent.loginModal('');
	</script>
	<?php
}
else if($response['status'] == '403'){
	?>
	<script>
		myModal("Unauthorized Access","It seems something went wrong . Please re-login",'');
		setTimeout(function(){ parent.closemyModal(); }, 3000);
		parent.reloadwindow()
	</script>
	<?php
}
else if($response['status'] == '406'){
	?>
	<script>
		myModal("Input Mismatch","It seems something went wrong . Please re-try",'');
		setTimeout(function(){ parent.closemyModal(); }, 3000);
	</script>
	<?php
}
?>