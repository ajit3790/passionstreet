<?php
require_once('initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
	    'rowWrapper'=>array("<div class='container sm'>","</div>"),
            'modules'=>array(
                'eventpublish'=>array(
                    'modulesParams'=>array()
                )/*,
                'eventpublishcomposite'=>array(
                    'modulesParams'=>array()
                )*/
            )
        )
);

$page_meta = array(
	'title'=>'Event Publish'
);

$PSParams['blockbots'] = 1;

if(!loggedId())
header('location:'.ROOT_PATH); 

echo render_modules();

?>