<?php
require_once('initialise.php');
$PSJavascript['login_required'] = false;
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
	     'rowWrapper'=>array("<div class='container sm'>","</div>"),
             'columnStructure' =>true,
             'columns'=>array(
                array(
                     'columnParams'=>array(
                        'class'=>'col-md-12'
                     ),
                    'modules'=>array(
			  'pagedetail'=>array(
                            'modulesParams'=>array()
                          )
                     )
                )
            )
        ),
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
	     'rowWrapper'=>array("<div class='container sm'>","</div>"),
             'columnStructure' =>true,
             'columns'=>array(
                array(
                     'columnParams'=>array(
                        'class'=>'col-md-7'
                     ),
                    'modules'=>array(
			  'postform'=>array(
                            'modulesParams'=>array()
                          ),
                         'wall'=>array(
                            'modulesParams'=>array()
                          )
                     )
                     
                ),
                array(
                     'columnParams'=>array(
                        'class'=>'col-md-5 sidebar-right sidebar'
                     ),
                    'modules'=>array(
                         'memberlisting'=>array(
                            'modulesParams'=>array('module_display_style'=>'coresel')
                          ),
			  'eventlisting'=>array(
                            'modulesParams'=>array('module_display_style'=>'boxed')
                          )
                     )
                )
            )
        )
);


$page_meta = array(
);

echo render_modules();

?>