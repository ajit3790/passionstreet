<?php
function user_get_list($fields=array(),$profile_id = PROFILE_ID,$limit = '',$page = 1,$refreshType = false,$cacheMeta = array('expiry'=>20)){
    global $connection;
    $profile_id = ($profile_id == null)?PROFILE_ID:$profile_id;
    $page = (!empty($page)?$page:1);
    $limit = (!empty($limit)?$limit:12);
    
    if($refreshType !== true)
    return preprocessApi('user_get_list', $fields , $profile_id,$limit,$page,$refreshType,$cacheMeta);
    
    $paging['page'] = $page;
    $paging['show'] = $limit;
    
    $fields['paging'] = $paging;
    
    $queryParams = construct_list_params($fields);
    $user_list = $connection->fetchAll("
        select profile_id,fname,lname,email,userbg,userdp,following,follower,ishefollowingme,amifollowinghim,passionatein,expertin,event from profile profile 
        LEFT JOIN 
        (select profile_id as followerid ,(1) as ishefollowingme from relations where relation = 'followsUser' and object='".$profile_id."' )  myfollowertable on profile.profile_id = myfollowertable.followerid
        LEFT JOIN 
        (select object as followerid ,(1) as amifollowinghim from relations where relation = 'followsUser' and profile_id='".$profile_id."' )  myfollowingtable on profile.profile_id = myfollowingtable.followerid
        LEFT JOIN 
        (select count(*)as following,profile_id as followerid  from relations where relation = 'followsUser' group by profile_id)  followingtable on profile.profile_id = followingtable.followerid 
        LEFT JOIN 
        (select count(*)as follower,object as followerid  from relations where relation = 'followsUser' group by object)  followertable on profile.profile_id = followertable.followerid
        LEFT JOIN 
        (select GROUP_CONCAT(object SEPARATOR ',') as passionatein,profile_id as followerid,relation as passionlevel from relations where relation = 'joinedPassion' group by profile_id) passiontable1 on profile.profile_id = passiontable1.followerid
        LEFT JOIN 
        (select GROUP_CONCAT(object SEPARATOR ',') as expertin,profile_id as followerid,relation as passionlevel from relations where relation = 'isExpert' group by profile_id) passiontable2 on profile.profile_id = passiontable2.followerid
        LEFT JOIN 
        (select object as event,profile_id as eventmemberid from relations where relation = 'joinedevent') eventtable on profile.profile_id = eventtable.eventmemberid ".$queryParams[0].' order by RAND() '.$queryParams[2],$queryParams[1]);
    $user_list_final = array();
    foreach($user_list as $key=>$user)
    {
        //$user_list[$key]['isExpert']=1;
        $user_list_final[$user['profile_id']] = $user;
        $user_list_final[$user['profile_id']] = array_merge($user_list[$key] , addUserElements($user));
        $user_list_final[$user['profile_id']]['following']=($user['following'])?$user['following']:0;
        $user_list_final[$user['profile_id']]['follower']=($user['follower'])?$user['follower']:0;
        $user_list_final[$user['profile_id']]['ishefollowingme']=$user['ishefollowingme'];
        $user_list_final[$user['profile_id']]['amifollowinghim']=$user['amifollowinghim'];
        $user_list_final[$user['profile_id']]['passion']['joinedPassion'] = (!empty($user_list[$key]['passionatein']))?explode(',',$user_list[$key]['passionatein']):array();
        $user_list_final[$user['profile_id']]['passion']['isExpert'] = (!empty($user_list[$key]['expertin']))?explode(',',$user_list[$key]['expertin']):array();
        if(count($user_list_final[$user['profile_id']]['passion']['isExpert'])>0)
        $user_list_final[$user['profile_id']]['isExpert']=1;
    }
    return $user_list_final;
}
function user_get_list_network($fields=array(),$profile_id = PROFILE_ID,$limit = '',$page = 1,$refreshType = false,$cacheMeta = array('expiry'=>20)){
    global $connection;
    $profile_id = ($profile_id == null)?PROFILE_ID:$profile_id;
    if(empty($profile_id))
    return false;
    $page = (!empty($page)?$page:1);
    $limit = (!empty($limit)?$limit:12);
    
    if($refreshType !== true)
    return preprocessApi('user_get_list_network', $fields , $profile_id,$limit,$page,$refreshType,$cacheMeta);
    
    if($page != 'all')
    {
    $paging['page'] = $page;
    $paging['show'] = $limit;
    $fields['paging'] = $paging;
    }
    
    $queryParams = construct_list_params($fields);
    $user_list = $connection->fetchAll("select profile_id,1 as followingme,'' as iamfollowing from relations where relation = 'followsUser' AND object = 'profile_1456704072_694534' UNION select object as profile_id,'' as followingme,1 as iamfollowing from relations where relation = 'followsUser' AND profile_id = 'profile_1456704072_694534'");
    $user_list2 = array();
    foreach($user_list as $user)
    {
        $user_list2[$user['profile_id']]['profile_id'] = $user['profile_id'];
        if($user['followingme'])    $user_list2[$user['profile_id']]['followingme'] = $user['followingme'];
        if($user['iamfollowing'])   $user_list2[$user['profile_id']]['iamfollowing'] = $user['iamfollowing'];
    }
    $user_profiles = $connection->fetchAll("select fname,lname,profile_id,userdp from profile ".$queryParams[0]." AND profile_id in ('".implode("','",array_keys($user_list2))."')" .$queryParams[2],$queryParams[1]);
    foreach($user_profiles as $key=>$user)
    {
        $user_profiles[$key]['followingme'] = $user_list2[$user['profile_id']]['followingme'];
        $user_profiles[$key]['iamfollowing'] = $user_list2[$user['profile_id']]['iamfollowing'];
        $user_profiles[$key] = array_merge($user_profiles[$key] , addUserElements($user_profiles[$key]));
    }
    return $user_profiles;
}
function user_get_count($fields,$refreshType = false,$cacheMeta = array('expiry'=>15)){
    global $connection;
    
    //if($refreshType !== true)
    //return preprocessApi('user_get_count', $fields ,$refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $return_data =  $connection->fetchColumn('select count(*) as usercount from profile '.$queryParams[0],$queryParams[1]);
    return $return_data;
}
function user_create_profile_sns($fields){
    global $connection;
    return ($connection->insert('profile',$fields));
}
function user_create_profile($fields){
    global $connection;
    $credential['password'] = $fields['password'];
    $credential['email'] = $fields['email'];
    $credential['profile_id'] = $fields['profile_id'];
    unset($fields['password']);
    //print_array(array($fields,$credential));
    return ($connection->insert('profile',$fields))?user_create_user_credential($credential):false;
}
function user_create_user_credential($fields){
    global $connection;
    $fields['password'] = md5($fields['password']);
    return $connection->insert('profile_credential',$fields)?$fields['profile_id']:false;
}
function user_log_in($credentails,$type = ""){
    global $connection,$PSData,$PSJavascript;
    @session_start();
    /*if(!empty($credentails['email'])){
        $email = $credentails['email'];
        if(strpos($email,'gmail.com')!==false){
        $email = str_replace ("gmail.com", "",$email); 
        $email = str_replace (".", "",$email); 
        $email = $email.'gmail.com'; 
        }
        $credentails['email'] = $email;
    }*/
    if($type == "")
    {
        $fields[] = $credentails['email'];
        $fields[] = md5($credentails['password']);
        $fields[] = 1;
        $profile = $connection->fetchAssoc("select profile_id from profile_credential where email=? AND password=? limit ?",$fields);
        $profile_id = $profile['profile_id'];
        if($profile_id)
        {
            $PSData['user'] = user_profile(array("profile_id"=>$profile_id));
            $_SESSION['user']['profile_id'] = $PSData['user']['profile_id'];
            $PSJavascript['profile_id'] = $PSData['user']['profile_id'];
            $PSJavascript['login_status'] = true;
            return $PSData['user']['profile_id'];
        }
        else{
            return false;;
        }
    }
    else
    {
        
    }
    return false;
}
function user_log_in_sns($type , $id){
    global $connection,$PSData,$PSJavascript;
    @session_start();
    if($type == "")
    {
        $fields[] = $credentails['email'];
        $fields[] = md5($credentails['password']);
        $fields[] = 1;
        $profile = $connection->fetchAssoc("select profile_id from profile_credential where email=? AND password=? limit ?",$fields);
        $profile_id = $profile['profile_id'];
        if($profile_id)
        {
            $PSData['user'] = user_profile(array("profile_id"=>$profile_id));
            $_SESSION['user']['profile_id'] = $PSData['user']['profile_id'];
            $PSJavascript['profile_id'] = $PSData['user']['profile_id'];
            $PSJavascript['login_status'] = true;
            return true;
        }
        else{
            return false;;
        }
    }
    else
    {
        
    }
    return false;
}
function user_profile($fields = array('profile_id'=>PROFILE_ID),$refreshType=false,$cacheMeta=array('expiry'=>30)){
    global $connection;
    if($fields['profile_id'] == $_SESSION['user']['profile_id'])
    {
        if(!empty($_SESSION['user']) && !empty($_SESSION['user']['fname']) && empty($_SESSION['user']['updateProfile']))
        return $_SESSION['user'];
        $userProfile = 'self';
    }
    else {
        $userProfile = 'external';
    }
    if($refreshType !== true)
    return preprocessApi('user_profile', $fields ,$refreshType,$cacheMeta);
    
    
    if(!empty($fields['email']))
    $fields['email'] = get_standard_email($fields['email']);
    
    $queryParams = construct_list_params($fields);
    $profile = $connection->fetchAssoc("select * from profile ".$queryParams[0],$queryParams[1]);
        
    if($profile)
    {
        $profile = array_merge($profile,addUserElements($profile));
        if($profile['extra']){
            $profile['extra'] = unserialize($profile['extra']);
        }
        
        $profile['age'] = floor((time() - strtotime($profile['dob'])) / (365 * 24 * 60 * 60));
        $passions = $connection->fetchAll("select object as passion from relations where profile_id = ? and relation = ?",array($profile['profile_id'],'joinedPassion'));
        foreach($passions as $value)
        $profile['passions'][] = $value['passion'];
        if($userProfile == 'external')
        $amIfollowinghim = $connection->fetchAssoc("select count(*) as count from relations where profile_id = ? and relation = ? and object = ?",array(PROFILE_ID,'followsUser',$profile['profile_id']));
        $profile['amIfollowinghim'] = $amIfollowinghim['count'];
        return $profile;
    }
    else
        return false;
}
function user_profile_update($incomingfields=array()){
    global $connection;
    @session_start();
    if($incomingfields['profile_id'])
    {
    $userid = $incomingfields['profile_id'];
    unset($incomingfields['profile_id']);
    }
    else    
    $userid = $_SESSION['user']['profile_id'];
    $fields['profile_id'] = $userid;
    if(!empty($incomingfields['extra']))
    $incomingfields['extra'] = serialize($incomingfields['extra']);
    $return = $connection->update("profile",$incomingfields,$fields);
    $_SESSION['user']['updateProfile'] = true;
    return $return;
}
function user_credential_update($incomingfields=array()){
    global $connection;
    @session_start();
    if($incomingfields['profile_id'])
    {
    $userid = $incomingfields['profile_id'];
    unset($incomingfields['profile_id']);
    }
    else    
    $userid = $_SESSION['user']['profile_id'];
    $fields['profile_id'] = $userid;
    $incomingfields['password'] = md5($incomingfields['password']);
    $return = $connection->update("profile_credential",$incomingfields,$fields);
    if(empty($return)) //chech if update not performed due to password being same 
    {
    $x = $connection->fetchAssoc("select count(*) as count from profile_credential where profile_id=? limit 1",array($fields['profile_id']));
    return $x['count'];
    }
    return $return;
}
function user_notification(){
    return 0;
}
function relation_register($profile_id,$relation,$object){
    global $connection;
    $fields['profile_id'] = $profile_id;
    $fields['relation'] = $relation;
    $fields['object'] = $object;
    $fields['dateadded'] = date("Y-m-d H:i:s");
    return $connection->insertignore('relations',$fields);
}
function relation_list($fields=array(),$refreshType = false , $cacheMeta = array('expiry'=>30)){
    global $connection;
    
    if($refreshType !== true)
    return preprocessApi('relation_list', $fields ,$refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $event_list = $connection->fetchAll("select * from relations ".$queryParams[0],$queryParams[1]);
    return $event_list;
}
function user_passions($profile_id,$refreshType= false , $cacheMeta = array('expiry'=>60)){
    global $connection;
    
    if($refreshType !== true)
    return preprocessApi('user_passions', $profile_id ,$refreshType,$cacheMeta);
    
    $fields[] = $profile_id;
    $fields[] = 'joinedPassion';
    $fields[] = 'isExpert';
    $fields[] = 1;
    $passion_list = $connection->fetchAll("select relation,object from relations where profile_id = ? and relation in (?,?) and status = ?",$fields);
    foreach($passion_list as $passion)
    {
        $passion_list_new[$passion['relation']][] = $passion['object'];
    }
    return $passion_list_new;
}
function post_create($fields){
    global $connection;
    @session_start();
    $fields['creator'] = $_SESSION['user']['profile_id'];
    $fields['create_date'] = date("Y-m-d H:i:s");
    $fields['status'] = 1;
    $fields['visibility'] = 1;
    $fields['extra'] = '';
    $fields['images'] = serialize($fields['images']);
    return $connection->insert('posts',$fields);
}
function post_get_list($fields = array(),$page = 1,$refreshType = false /*false,true,delete*/,$cacheMeta = array('expiry'=>10)){
    global $connection,$PSParams;
    $paging['page'] = (!empty($page)?$page:1);
    $paging['show'] = 5;
    if($refreshType !== true)
    return preprocessApi('post_get_list', $fields , $page, $refreshType,$cacheMeta);
    $fields['paging'] = $paging;
    $queryParams = construct_list_params($fields);
    //if($singleView)
    //$post_list = $connection->fetchAll("select posts.*,profile.fname,profile.lname,profile.profile_id,profile.userdp from posts LEFT JOIN profile on posts.creator = profile.profile_id ".$queryParams[0]." and posts.status=1 limit 1",$queryParams[1]);
    //else
    // print_array("select posts.*,profile.fname,profile.lname,profile.profile_id,profile.userdp from posts LEFT JOIN profile on posts.creator = profile.profile_id ".$queryParams[0]." and posts.status=1 order by id desc ".$queryParams[2],$queryParams[1]);
    // return ;
    $post_list = $connection->fetchAll("select posts.*,profile.fname,profile.lname,profile.profile_id,profile.userdp from posts LEFT JOIN profile on posts.creator = profile.profile_id ".$queryParams[0]." and posts.status=1 order by id desc ".$queryParams[2],$queryParams[1]);
    foreach($post_list as $key=>$value)
    {
        $inArray[] = $post_list[$key]['creator'];
        $inArrayPosts[] = $post_list[$key]['post_id'];
    $post_list[$key] = array_merge($post_list[$key],addUserElements($post_list[$key]));
    
    if($post_list[$key]['postedontype'] == 'event' && $post_list[$key]['postedon'])
    {
        $eventarray[] = $post_list[$key]['postedon'];
    }
    }
    $eventdatanew = array();
    if($eventarray)
    {
	$eventarray = array_unique($eventarray);
	$eventdata = $connection->fetchAll("select events.eventname,events.event_id,events.eventpic from events where event_id in ('".implode(',',$eventarray)."')");
	foreach($eventdata as $key=>$value)
	{
		$eventdatanew[$eventdata[$key]['event_id']] = $eventdata[$key]['eventname'];
	}
    }
    
    //$comment_count = $connection->fetchAll("select count(*) as commentcount,post_id from (select commentedon as post_id,commentor from comments where commentor in ('".implode("','",$inArray)."')) comment group by post_id");
    $comment_count = $connection->fetchAll("select count(*) as commentcount,post_id from (select commentedon as post_id,commentor from comments where commentedon in ('".implode("','",$inArrayPosts)."')) comment group by post_id");
    foreach($comment_count as $key=>$value)
    {
        $comment_count_arr[$comment_count[$key]['post_id']] = $comment_count[$key]['commentcount'];
    }
    //$like_count = $connection->fetchAll("select * from (( select count(*) as likescount,post_id from (select post_id,user from likes where post_id in ('".implode("','",$inArrayPosts)."')) likestable group by post_id) as likeTable2 LEFT JOIN (select count(*),post_id as doILikeThis where user='".$_SESSION['user']['profile_id']."' group by post_id) as userLikeTable  on userLikeTable.post_id = likeTable2.post_id)");
    //print_array($like_count);
    $like_count = $connection->fetchAll("select count(*) as likescount,post_id from (select post_id,user from likes where post_id in ('".implode("','",$inArrayPosts)."')) likestable group by post_id");
    foreach($like_count as $key=>$value)
    {
        $like_count_arr[$like_count[$key]['post_id']] = $like_count[$key]['likescount'];
    }
    foreach($post_list as $key=>$post)
    {
        
        $post_list[$key]['categoryname'] = $PSParams['PSCategories'][$post['category']]['name'];
        $post_list[$key]['subcategoryname'] = $PSParams['PSSubCategories'][$post['subcategory']]['name'];
        $post_list[$key]['creatorname'] = ucfirst($post['fname']);
        $post_list[$key]['creatorfullname'] = $post['fullname'];
        $post_list[$key]['creatorseopath'] = $post['seopath'];
        $post_list[$key]['create_date'] = strtotime($post['create_date']);
        //$post_list[$key]['create_date'] = date(DATE_ISO8601, strtotime($post['create_date']));
        //$post_list[$key]['create_date'] = date("Y-m-d H:i:s", strtotime($post['create_date']));
        $post_list[$key]['userdp'] = $post['userdp'];
        $temp = unserialize($post['images']);
        foreach($temp as $key2=>$value2)
        {
            foreach($value2 as $key3=>$value3)
            {
        if($post_list[$key]['create_date'] > 1477631272) //thumbnail introduced on 28/oct/2016
                $temp[$key2][$key3] = get_upload_path('thumb/'.$value3);
        else
        $temp[$key2][$key3] = get_upload_path($value3);
            }
        }
    $post_list[$key]['images'] = $temp;
        
    $temp = explode("###",$post['post_type']);
        if($temp[0] == 'posttype')
        {
            $post['post_type'] = $temp[1];    
            $post_list[$key]['posttypename'] = $PSParams['PSCategories'][$post['category']]['posttype'][$post['post_type']];
        }
        else if($temp[0] == 'subcategory')
        {
            $post['post_type'] = $temp[1];    
            $post_list[$key]['posttypename'] = $PSParams['PSSubCategories'][$post['post_type']]['name'];
        }
        $post_list[$key]['commentcount'] = ($comment_count_arr[$post_list[$key]['post_id']])?$comment_count_arr[$post_list[$key]['post_id']]:0;
        $post_list[$key]['likescount'] = ($like_count_arr[$post_list[$key]['post_id']])?$like_count_arr[$post_list[$key]['post_id']]:0;
        if(!empty($post_list[$key]['extra']))
        {
        $post_list[$key]['extra'] = unserialize($post_list[$key]['extra']);
        //print_array($post_list[$key]['extra']);
        }
    if($post_list[$key]['postedontype'] == 'event')
    {
        if($eventdatanew[$post_list[$key]['postedon']])
        {
        $eventid = $post_list[$key]['postedon'];
        $post_list[$key]['postedonobject']['name'] = $eventdatanew[$eventid];
        $post_list[$key]['postedonobject']['event_id'] = $eventid;
        $post_list[$key]['postedonobject']['path'] = 'event/'.get_alphanumeric($eventdatanew[$eventid]).'/'.$eventid;
        $temp = unserialize($eventdatanew['eventpic']);
        $post_list[$key]['postedonobject']['image'] = $temp[0];
        }
    }
    
    }
    return $post_list;
}
function post_update($post_id,$incomingfields = array()){
    global $connection;
    $fields['post_id'] = $post_id;
    if(!empty($incomingfields['extra']))
    {
    $incomingfields['extra'] = serialize($incomingfields['extra']);
    }
    $return = $connection->update("posts",$incomingfields,$fields);
    return $return;
}
function post_details($fields){
    global $connection;
    $queryParams = construct_list_params($fields);
    $post = $connection->fetchAssoc("select * from posts ".$queryParams[0]." limit 1",$queryParams[1]);
    $post['extra'] = unserialize($post['extra']);
    return $post;
}
function mailer_data_add($mailertype,$email,$profile_id=0){
    global $connection;
    @session_start();
    $fields['mailer_id'] = generate_id("mailer");
    $fields['email'] = $email;
    $fields['profile_id'] = $profile_id;
    $fields['mailertype'] = $mailertype;
    $fields['initiatedon'] = date("Y-m-d H:i:s");
    $fields['opencount'] = 0;
    $fields['clickcount'] = 0;
    return array("result"=>$connection->insert('mailer_track',$fields),"mailer_id"=>$fields['mailer_id']);
}
function mailer_data_update($mailer_id,$email,$type='',$fieldsforupdate=array()){
    global $connection;
    @session_start();
    $tfields['email'] = $email;
    $tfields['mailer_id'] = $mailer_id;
    $date = date('Y-m-d h:i:s');
    if($type == 'open')
    {
        $count = $connection->executeUpdate("UPDATE mailer_track SET fopenedon = ?,lopenedon=?,opencount = opencount+1 WHERE email=? AND mailer_id=? AND fopenedon=?",array($date,$date,$email,$mailer_id,'0000-00-00 00:00:00'));
        if($count == 0)
        $count = $connection->executeUpdate("UPDATE mailer_track SET lopenedon=?,opencount = opencount+1 WHERE email=? AND mailer_id=?",array($date,$email,$mailer_id));
        return $count;
    }
    else if($type == 'click')
    {
        $count = $connection->executeUpdate("UPDATE mailer_track SET fclickedon = ?,lclickedon=?,clickcount = clickcount+1 WHERE email=? AND mailer_id=? AND fclickedon=?",array($date,$date,$email,$mailer_id,'0000-00-00 00:00:00'));
        if($count == 0)
        $count = $connection->executeUpdate("UPDATE mailer_track SET lclickedon=?,clickcount = clickcount+1 WHERE email=? AND mailer_id=?",array($date,$email,$mailer_id));
        return $count;
    }
    else
    {
        $return = $connection->update("mailer_track",$fieldsforupdate,$fields);
        return $return;
    }
    return $return;
}
function event_create($fields){
    global $connection;
    if($fields['eventinfo'])
    $fields['eventinfo'] = serialize($fields['eventinfo']);
    return ($connection->insertignore('events',$fields));
}
function event_get_filters($column){
	global $connection;
	$timenow = date("Y-m-d H:i:s");
	$events = $connection->fetchAll("select count(*) as count,$column from events  where (bookingenddate >'".$timenow."' OR (bookingenddate = '0000-00-00 00:00:00' AND datestart > '".$timenow."')) AND $column <> '' group by $column order by count desc",array());
	return $events;
}
function event_get_list($params = array(),$refreshType = false,$cacheMeta = array('expiry'=>15)){
    global $connection;
    $fields = $params['fields'];
    
    $profile_id = ($params['profile_id'])?$params['profile_id']:PROFILE_ID;
    
    if($refreshType !== true)
    return preprocessApi('event_get_list', $params, $refreshType,$cacheMeta);
    
    if($fields['status'] == 'all')
    {
        unset($fields['status']);
    }
    else if($fields['status'])
    $fields['status'] = $fields['status'];
    else
    $fields['status'] = 'published';
    
    $params['fields'] = $fields;
    
    
    if(!empty($fields['status']) && empty($fields['paging']['showall']))
    {
    $fields['paging']['page'] = ($fields['paging']['page'])?$fields['paging']['page']:1;
    $fields['paging']['show'] = ($fields['paging']['show'])?$fields['paging']['show']:10;
    }
    else
    {
	unset($fields['paging']);
    }
    
    $orderstr = " order by source desc,datestart desc";
    $timenow = date("Y-m-d H:i:s");
    if(!empty($fields['status']) && empty($fields['extraquery']) && empty($fields['upcoming']))
    $fields['upcoming'] = 1;
    if($fields['upcoming'] == 1)
    {
    //$orderstr = " order by bookingenddate asc";
    $orderstr = " order by source desc,datestart asc";
    //$fields['extraquery'] = ($fields['extraquery'])?($fields['extraquery'] .' AND bookingenddate >"'.date("Y-m-d H:i:s").'" '):(' bookingenddate >"'.date("Y-m-d H:i:s").'"'); 
    $fields['extraquery'] = ' (datestart >"'.$timenow.'" OR (bookingenddate = "0000-00-00 00:00:00" AND datestart > "'.$timenow.'"))'; 
    unset($fields['upcoming']);
    }
    if($fields['randomise'] == 1)
    {
    $orderstr = " order by RAND()";
    unset($fields['randomise']);
    }
    
    $queryParams = construct_list_params($fields);
    if($profile_id)
    {
    //$events = $connection->fetchAll("select events.*,eventtable.isJoinedEvent from events LEFT JOIN 
    //(select count(profile_id) as isJoinedEvent , object as event,profile_id as eventmemberid from relations where relation = 'joinedevent' && profile_id='".$profile_id."') eventtable on events.event_id = eventtable.event ".$queryParams[0]."  order by events.createdate desc",$queryParams[1]);
    $events = $connection->fetchAll("select events.event_id,events.source,events.producttype,events.eventtype,events.eventtypebyfee,events.eventname,events.category,events.description,events.venue,events.city,events.rating,events.tickettype,events.ticketvaluerange,events.eventpic,datetimeslots.sdatestart as datestart,datetimeslots.sdateend as dateend,datetimeslots.sbookingenddate as bookingenddate,eventtable.isJoinedEvent from events LEFT JOIN (select productid,datestart as sdatestart,dateend as sdateend,bookingenddate as sbookingenddate from datetimeslots where status = 1) as datetimeslots on datetimeslots.productid = events.event_id LEFT JOIN (select count(profile_id) as isJoinedEvent , object as event,profile_id as eventmemberid from relations where relation = 'joinedevent' AND profile_id='".$profile_id."') eventtable on events.event_id = eventtable.event ".$queryParams[0]." group by event_id ".$orderstr.$queryParams[2],$queryParams[1]);
    }
    else
    {
    $events = $connection->fetchAll("select events.event_id,events.source,events.producttype,events.eventtype,events.eventtypebyfee,events.eventname,events.category,events.description,events.venue,events.city,events.rating,events.tickettype,events.ticketvaluerange,events.eventpic,datetimeslots.sdatestart as datestart,datetimeslots.sdateend as dateend,datetimeslots.sbookingenddate as bookingenddate from events LEFT JOIN (select productid,datestart as sdatestart,dateend as sdateend,bookingenddate as sbookingenddate from datetimeslots where status = 1) as datetimeslots on datetimeslots.productid = events.event_id ".$queryParams[0]." group by event_id ".$orderstr.$queryParams[2],$queryParams[1]);
    }
    foreach($events as $key=>$event)
    {
        $events[$key]['eventpic'] = unserialize($events[$key]['eventpic']);
        $events[$key] = $events[$key];
        $events[$key]['eventpic']= get_upload_path($events[$key]['eventpic'][0]);
        $events[$key]['description']= nl2br($events[$key]['description']);
        $events[$key]['seopath']= get_alphanumeric($events[$key]['eventname']).'/'.$events[$key]['event_id'];
    }
    //print_array($events);
    return $events;
}

function event_get_details($eventid,$detailed = false,$profile_id = PROFILE_ID,$producttype='NA',$refreshType = false,$cacheMeta = array('expiry'=>120)){
    global $connection;
    if($producttype === true || $producttype === 1)
    {
    $refreshtype = true;
    $producttype = 'NA';
    }
    
    if($refreshType !== true)
    return preprocessApi('event_get_details',$eventid ,$detailed,$profile_id,$producttype,$refreshType,$cacheMeta);
    $fields[] = $eventid;
    $fields[] = $eventid;
    if($producttype != 'NA')
    {
    $fields[] = $producttype;
    $event = $connection->fetchAssoc("select events.*,eventtable.isJoinedEvent from events LEFT JOIN (select count(profile_id) as isJoinedEvent , object as event,profile_id as eventmemberid from relations where relation = 'joinedevent' && profile_id='".$profile_id."' && object = ?) eventtable on events.event_id = eventtable.event where event_id = ? and producttype = ?",$fields);
    }
    else
    $event = $connection->fetchAssoc("select events.*,eventtable.isJoinedEvent from events LEFT JOIN (select count(profile_id) as isJoinedEvent , object as event,profile_id as eventmemberid from relations where relation = 'joinedevent' && profile_id='".$profile_id."' && object = ?) eventtable on events.event_id = eventtable.event where event_id = ?",$fields);
    
    if($event)
    {
	if($detailed && $event['eventinfo'])
	{
		$event['eventinfo'] = unserialize($event['eventinfo']);
		foreach($event['eventinfo'] as $key=>$value)
		{
		    $event[$key] = $value;
		}
	}
        if($event['creator'] == $profile_id)
        $event['isAdmin'] = 1; 
        $event['eventpic'] = unserialize($event['eventpic']);
        $event['eventpic'] = get_upload_path($event['eventpic'][0]);
        $event['mapimage'] = get_upload_path($event['mapimage']);
        $event['routeimage'] = get_upload_path($event['routeimage']);
        if($event['routemap'])
        $event['routemap'] = unserialize($event['routemap']);
        $event['rating'] = unserialize($event['rating']);
        if($detailed)
        {
            $where = array();
            $where['tktcateventid'] = $eventid;
            $where['tktcatstatus'] = 1;
            $tickets = ticketcategories_get_list($where,true);
            $ticketfinal = array();
            foreach($tickets as $ticket)
            {
                $temp = array();
                $temp['id'] = $ticket['tktcatid'];
                $temp['name'] = $ticket['tktcatname'];
                $temp['price'] = $ticket['tktcatprice'];
                $temp['bookingprice'] = $ticket['tktcatbookamount'];
                $temp['minbuy'] = $ticket['tktcatminbuy'];
                $temp['maxbuy'] = $ticket['tktcatmaxbuy'];
                $temp['totalcount'] = $ticket['tktcatcount'];
                                $temp['type'] = $ticket['tkttype'];
                if($temp['minbuy'] > 1 && ($temp['minbuy'] == $temp['maxbuy']))
                {
                                    if($temp['type'] == 3) //couple
                                    {
                    $temp['iscombo'] = 1;
                    $temp['combosize'] = $temp['minbuy'];
                    $temp['name'] = $temp['name'];
                    $temp['price'] = $temp['price'] * $temp['minbuy'];
                                    }
                                    else
                                    {
                    $temp['iscombo'] = 1;
                    $temp['combosize'] = $temp['minbuy'];
                    $temp['name'] = $temp['name'].' ('.$temp['minbuy'].' member)';
                    $temp['price'] = $temp['price'] * $temp['minbuy'];
                                    }
                }
                else
                $temp['iscombo'] = 0;
                $temp['bibinitial'] = $ticket['tktcatbibintial'];
                $temp['bibstart'] = $ticket['tktcatbibstart'];
                $temp['lastbib'] = $ticket['tktcatlastbibno'];
                $temp['agerestrict'] = $ticket['agerestrict'];
                $temp['description'] = $ticket['description'];
                $temp['requiremobile'] = $ticket['requiremobile'];
                
                $ticketfinal[$ticket['tktcatid']] = $temp;
            }
            $where = array();
            $where['relatedtotype'] = 'eventquestionaire';
            $where['relatedto'] = $eventid;
            $where['status'] = 1;
            $questionaire = question_get_list($where,true);
            
            $event['ticketcategories'] = $ticketfinal; 
            $event['preconditionOption'] = $questionaire; 
            $where = array();
            $where['productid'] = $eventid;
            $where['producttype'] = $event['producttype'];
            $where['status'] = 1;
            //$where['extraquery'] = ' bookingenddate > "'.date("Y-m-d H:i:s").'"';
            $datetimeslots = datetimeslot_get_list($where,true);
            $event['datetimeslots'] = $datetimeslots;
            
            $where = array();
            $where['productid'] = $eventid;
            $where['producttype'] = $event['producttype'];
            $event['attendeefields'] = attendeefields_get_details($where);
	    
	    $where = array();
            $where['productid'] = $eventid;
            $where['producttype'] = $event['producttype'];
            $event['displayblocks'] = htmlblocks_get_list($where);
		$fields = array();
		$fields['name'] = array('Name',1,'text',1);
		$fields['email'] = array('Email Id',1,'email',1);
		$fields['gender'] = array('Gender',1,'text',1);
		$fields['dob'] = array('Date Of Birth',1,'text',1);
		$fields['mobile'] = array('Mobile',1,'number',1);
		if(!(($event['status'] == 'unpublished' && $event['isAdmin'] == 1) || $event['status'] == 'published'))
		$event['ticketcategories'] = array();
		if($event['ticketcategories'] || ($event['source'] == 'aggregated'))
		$event['attendeefields']['fields'] = ($event['attendeefields']['fields'])?$event['attendeefields']['fields']:$fields;
		else
		$event['attendeefields']['fields'] = $event['attendeefields']['fields'];
		$event['skiptoleadcapture'] = (empty($event['ticketcategories']) && !empty($event['attendeefields']['fields']))?1:0;
		if($event['skiptoleadcapture'])
		$event['ticketcategories'] = array('leadcapture'=>array('id'=>'leadcapture','name'=>(($event['source'] == 'aggregated' || $event['status'] == 'unpublished')?'Express Interest':'Register'),'price'=>0,'bookingprice'=>0,'minbuy'=>1,'maxbuy'=>1,'totalcount'=>100000,'type'=>1,'iscombo'=>0,'bibinitial'=>'','bibstart'=>0,'lastbib'=>'','agerestrict'=>7,'description'=>'','requiremobile'=>1));
	}
        //if($event['ticketcategories'])
        //$event['ticketcategories'] = unserialize($event['ticketcategories']);
        //if($event['preconditionOption'])
        //$event['preconditionOption'] = unserialize($event['preconditionOption']);
        $event['tickettype'] = $event['tickettype'];
    if($event['extra'])
    {
        $temp = unserialize($event['extra']);
        unset($event['extra']);
        $event['organiser'] = $temp['organiser'];
        $event['allowuserpost'] = $temp['allowuserpost'];
    }
    $event['seopath'] = get_alphanumeric($event['eventname']).'/'.$event['event_id'];
    return $event;
    }
    else
        return false;
}
function event_update($eventid,$incomingfields=array()){
    global $connection;
    @session_start();
    $fields['event_id'] = $eventid;
    if($incomingfields['eventinfo'])
    {
    $event = event_get_details($eventid,1);
    if($event['eventinfo'])
    $event['eventinfo'] = array_merge($event['eventinfo'],$incomingfields['eventinfo']);
    else
    $event['eventinfo'] = $incomingfields['eventinfo'];
    $incomingfields['eventinfo'] =  serialize($event['eventinfo']);
    }
    $return = $connection->update("events",$incomingfields,$fields);
    return $return;
}
function event_get_followers(){
    return array();
}
function passion_get_list($fields=array(),$profile_id = PROFILE_ID,$refreshType = false,$cacheMeta = array('expiry'=>120)){
    global $connection;
    
    if($refreshType !== true)
    return preprocessApi('passion_get_list', $fields ,$profile_id,$refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $passion_list = $connection->fetchAll("
        select count(*) as membercount ,relation as passionlevel, object ,eventcount,amifollowingthis,postcount  from relations 
        LEFT JOIN 
        (select profile_id as followerid,object as category ,(1) as amifollowingthis from relations where relation = 'joinedPassion' and profile_id='".$profile_id."' )  myfollowertable on myfollowertable.category = relations.object
        LEFT JOIN
        (select count(*) as eventcount , category from events group by category) eventstable on eventstable.category = relations.object
        LEFT JOIN
        (select count(*) as postcount , category from posts group by category) poststable on poststable.category = relations.object 
        where (relation = 'joinedPassion' or relation = 'isExpert')group by relation,object"
        );
    foreach($passion_list as $passion)
    {
        if(!($passion_list_final[$passion['object']]['passion']))
        {
        $passion_list_final[$passion['object']]['passion']=$passion['object'];
        $passion_list_final[$passion['object']]['eventcount']=$passion['eventcount'];
        $passion_list_final[$passion['object']]['postcount']=$passion['postcount'];
        $passion_list_final[$passion['object']]['amifollowingthis']=$passion['amifollowingthis'];
        }
        $passion_list_final[$passion['object']]['totalmember']=$passion_list_final[$passion['object']]['totalmember'] + $passion['membercount'];
        $passion_list_final[$passion['object']][$passion['passionlevel']]=$passion['membercount'];
            
    }
    return $passion_list_final;
}
function bankaccount_add($fields){
    global $connection;
    $fields['acntnmbr'] = encrypt($fields['acntnmbr'],ENCRYPTION_KEY);
    return ($connection->insert('bankaccounts',$fields))?$connection->lastInsertId():false;
}
function bankaccount_get_list($fields = array()){
    global $connection;
    $queryParams = construct_list_params($fields);
    $profile_id = $_SESSION['user']['profile_id'];
    $account_list = $connection->fetchAll("select * from bankaccounts ".$queryParams[0]." AND profile_id = '".$profile_id."'",$queryParams[1]);
    $newaccountlist = array();
    foreach($account_list as $key=>$account){
        //$account_list[$key]['acntnmbr'] = decrypt($account_list[$key]['acntnmbr'], ENCRYPTION_KEY); 
        $newaccountlist[$account['account_id']] = $account;
        $newaccountlist[$account['account_id']]['acntnmbr'] = decrypt($newaccountlist[$account['account_id']]['acntnmbr'], ENCRYPTION_KEY); 
    }
    return $newaccountlist;
}
function comment_add($fields){
    global $connection;
    return $connection->insert('comments',$fields);
}
function comment_get_list($fields,$refreshType = false,$cacheMeta = array('expiry'=>60)){
    global $connection;
    
    if($refreshType !== true)
    return preprocessApi('comment_get_list', $fields, $refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    //$profile_id = $_SESSION['user']['profile_id'];
    // $comment_list = $connection->fetchAll("select * from comments LEFT JOIN (select profile_id,fname,lname,userdp from profile) profile on comments.commentor = profile.profile_id ".$queryParams[0]." AND commentor = '".$profile_id."'",$queryParams[1]);
    $comment_list = $connection->fetchAll("select * from comments LEFT JOIN (select profile_id,fname,lname,userdp from profile) profile on comments.commentor = profile.profile_id ".$queryParams[0]." ",$queryParams[1]);
    foreach($comment_list as $key=>$value)
    {
        $comment_list[$key]['name'] = ucwords($comment_list[$key]['fname'].' '.$comment_list[$key]['lname']);
        $comment_list[$key]['userdp'] = get_upload_path($comment_list[$key]['userdp']);
        $comment_list[$key]['comment'] = $comment_list[$key]['comment'];
    }
    return $comment_list;
}
function photoes_add($fields){
    global $connection;
    return $connection->insert('photoes',$fields);
}
function photoes_get_list($fields,$refreshType = false,$cacheMeta = array('expiry'=>180)){
    global $connection;
    
    if($refreshType !== true)
    return preprocessApi('photoes_get_list', $fields, $refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $profile_id = $_SESSION['user']['profile_id'];
    $photoes_list = $connection->fetchAll("select path as imgurl from photoes ".$queryParams[0],$queryParams[1]);
    foreach($photoes_list as $key=>$value)
    {
        $photoes_list[$key]['imgurl'] = get_upload_path($photoes_list[$key]['imgurl']);
    }
    return $photoes_list;
}
function messages_add($fields){
    global $connection;
    return $connection->insert('messages',$fields);
}
function messages_update($message_id,$incomingfields=array()){
    global $connection;
    @session_start();
    $fields['message_id'] = $message_id;
    $return = $connection->update("messages",$incomingfields,$fields);
    return $return;
}
function like_update($postid,$posttype,$status=1)
{
    global $connection;
    $fields['user'] = $_SESSION['user']['profile_id'];
    $fields['post_id'] = $postid;
    $fields['postedontype'] = $posttype;
    $fields['date'] = date("Y-m-d H:i:s");
    $fields['status'] = $status;
    if($connection->insertignore('likes',$fields))
    {
        
    }
    else
    {
        $wherefields = array();
        $wherefields['post_id'] = $postid;
        $wherefields['user'] = $_SESSION['user']['profile_id'];
        $queryParams = construct_list_params($wherefields);
        $x = $connection->fetchAssoc("select * from likes ".$queryParams[0],$queryParams[1]);
        $status = ($x['status'])?0:1;
        $updatefields['status'] = $status;
        $return = $connection->update("likes",$updatefields,$wherefields);
    }
    return $status;
}
function question_add($fields){
    global $connection;
    return $connection->insert('questions',$fields);
}
function question_update($questionid,$incomingfields=array()){
    global $connection;
    @session_start();
    $fields['questionid'] = $questionid;
    $return = $connection->update("questions",$incomingfields,$fields);
    return $return;
}
function question_get_list($fields = array(),$refreshType = false , $cacheMeta = array('expiry'=>120)){
    global $connection;
    
    if($refreshType !== true)
    return preprocessApi('question_get_list', $fields ,$refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $question_list = $connection->fetchAll("select * from questions ".$queryParams[0],$queryParams[1]);
    foreach($question_list as $key=>$question)
    {
        if($question_list[$key]['questiontype'] == 'multipleoption')
        $question_list[$key]['questionoptions'] = unserialize ($question_list[$key]['questionoptions']);
        else
        $question_list[$key]['questionoptions'] = array();
    }
    return $question_list;
}
function invite_add($fields){
    global $connection;
    return $connection->insertignore('invites',$fields);
}
function invite_update($inviteid,$incomingfields=array()){
    global $connection;
    @session_start();
    $fields['inviteid'] = $inviteid;
    $return = $connection->update("invites",$incomingfields,$fields);
    return $return;
}
function invite_list($fields){
    global $connection;
    $queryParams = construct_list_params($fields);
    $invite_list = $connection->fetchAll("select * from invites ".$queryParams[0],$queryParams[1]);
    return $invite_list;
}
function tagged_post_list($tag)
{
    global $connection;
    $taggedpostlist = $connection->fetchAll("select * from tags where tag = ? limit 1",array($tag));
    return $taggedpostlist;
}
function tag_update($tagarray,$postid)
{
    global $connection;
    foreach($tagarray as $tag)
    {
        $taggedpostlist = tagged_post_list($tag);
        if(empty($taggedpostlist))
        {
            $connection->insertignore('tags',array('tag'=>$tag,'postid'=>$postid));
        }
        else
        {
            $posts = $postid.','.$taggedpostlist[0]['postid'];
            $return = $connection->update("tags",array('postid'=>$posts),array('id'=>$taggedpostlist[0]['id']));
        }
    }
}
function transaction_add($fields){
    global $connection,$PSData;
	if(!empty($fields['productid']) && !empty($fields['producttype']))
    {
		$transactiontable = 'transactions';
		/*
		if($fields['subtransactiontype'])
		$transactiontable = 'transactionsmisc';
		*/
		
		$fields['transactionid'] = ($fields['transactionid'])?$fields['transactionid']:generate_id("transaction");
        $fields['userid'] = ($PSData['user']['profile_id'])?$PSData['user']['profile_id']:($fields['userid']?$fields['userid']:'unknown');
        $fields['initialisedate'] = date("Y-m-d H:i:s");
        $fields['lastupdatedate'] = $fields['initialisedate'];
        $fields['status'] = ($fields['status'])?$fields['status']:'paymentstarted'; 
        $fields['amount'] = $fields['amount'];
		if($fields['extraamount'])
		$fields['productdetails']['extraamount'] = $fields['extraamount'];
		unset($fields['extraamount']);
        $fields['productdetails'] = serialize($fields['productdetails']); 
        //$fields['transactionidentifier'] = encrypt($fields['transactionid'].'|'.$fields['userid'].'|'.$fields['productid'].'|'.$fields['producttype']);
        $fields['transactionidentifier'] = $fields['transactionid'].'|'.$fields['userid'].'|'.$fields['productid'].'|'.$fields['producttype'];
        $fields['paymentgateway'] = PAYMENTGATEWAY;
        $fields['couponsapplied'] = serialize($fields['couponsapplied']);
		$transaction_id =  $connection->insert($transactiontable,$fields);
        if($transaction_id)
        {
            //return transact_payu($fields['transactionid']);
            return call_user_func_array('transaction_'.PAYMENTGATEWAY, array($fields['transactionid'],$fields['subtransactiontype']));
        }
    }
}
function transaction_update($id,$updatefields,$misc = false){
    global $connection;
	$transactiontable = 'transactions';
	if($misc)
	$transactiontable = 'transactionsmisc';
    
	$whereFields['transactionid'] = $id;
	$updatefields['lastupdatedate'] = date("Y-m-d H:i:s"); 
	$updatefields['gatewayresponse'] = serialize($updatefields['gatewayresponse']);
	if($updatefields['gatewaylog'])
	{
		if($whereFields['transactionid'] && $updatefields['status'] && $updatefields['gatewayresponse'] && $updatefields['gatewaylog'] && $updatefields['updatedByCron'] )
		return $connection->executeUpdate("UPDATE ".$transactiontable." SET status=?,gatewayresponse = ?,gatewaylog = CONCAT(gatewaylog,?),updatedByCron=? WHERE transactionid=? order by id desc limit 1",array($updatefields['status'],$updatefields['gatewayresponse'],date('Y-m-d h:i:s').' ->'.$updatefields['gatewaylog'].'\r\n',$updatefields['updatedByCron'],$whereFields['transactionid']));
		else 
		return false;
	}
	else
	$return = $connection->update($transactiontable,$updatefields,$whereFields);
	return $return;
}
function transaction_get_details($id,$misc = false){
    global $connection;
	$transactiontable = 'transactions';
	if($misc)
	$transactiontable = 'transactionsmisc';
    
	$fields['transactionid'] = $id;
    $queryParams = construct_list_params($fields);
    $transaction = $connection->fetchAssoc("select * from ".$transactiontable." ".$queryParams[0]." limit 1",$queryParams[1]);
    return $transaction;
}
function transaction_get_list($fields,$misc = false,$refreshType = false , $cacheMeta = array('expiry'=>180)){
    global $connection;
    if(empty($fields))
    {
        return ;
    }
    
    if($refreshType !== true)
    return preprocessApi('transaction_get_list', $fields ,$misc, $refreshType,$cacheMeta);
    
	$transactiontable = 'transactions';
	if($misc)
	$transactiontable = 'transactionsmisc';
    
	if(empty($fields['txnhtype']))
	$fields['txnhtype'] = 'parent';
	else if($fields['txnhtype'] = 'all')
	unset($fields['txnhtype']);
	
    $queryParams = construct_list_params($fields);
	//print_array(array("select * from ".$transactiontable." ".$queryParams[0],$queryParams[1]));
    $transaction = $connection->fetchAll("select * from ".$transactiontable." ".$queryParams[0]." order by id desc",$queryParams[1]);
    return $transaction;
}
function transaction_get_subtransaction_list($fields,$misc = false,$refreshType = false , $cacheMeta = array('expiry'=>180)){
    global $connection;
    if(empty($fields) || empty($fields['transactionid']))
    {
        return ;
    }
    if($refreshType !== true)
    return preprocessApi('transaction_get_subtransaction_list', $fields ,$misc, $refreshType,$cacheMeta);
    
	$fields['parenttxnid'] 	= $fields['transactionid'];
    $fields['txnhtype'] 		= 'child';
	unset($fields['transactionid']);
    
	
	$transactiontable = 'transactions';
	if($misc)
	$transactiontable = 'transactionsmisc';
    
	
    $queryParams = construct_list_params($fields);
	$transaction = $connection->fetchAll("select * from ".$transactiontable." ".$queryParams[0],$queryParams[1]);
    return $transaction;
}
function transaction_update_status($transactionid,$status,$gatewayresponse,$log,$misc = false){
    global $connection,$PSData;
	$transactiontable = 'transactions';
	if($misc)
	$transactiontable = 'transactionsmisc';
    $gatewayresponse = serialize($gatewayresponse);
    $count = $connection->executeUpdate("UPDATE ".$transactiontable." SET status=?,gatewayresponse = ?,gatewaylog = CONCAT(gatewaylog,?),updatedByCron=? WHERE transactionid=? order by id desc limit 1",array($status,$gatewayresponse,date('Y-m-d h:i:s').' ->'.$log.'\r\n',1,$transactionid));
    return $count;
    
    /*UPDATE
    some_table
SET
    some_text = CONCAT(some_text, '<<echo php variable here>>')
WHERE
    id = 3*/
}
function event_get_attending_members($eventid,$refreshType = false,$cacheMeta = array('expiry'=>600)){
    global $connection;
        
        if($refreshType !== true)
        return preprocessApi('event_get_attending_members', $eventid ,$refreshType,$cacheMeta);
    
    $fields['transactions.producttype'] = 'event';
    $fields['transactions.productid'] = $eventid;
    $fields['transactions.status'] = 'complete|success';
    $queryParams = construct_list_params($fields);
    $transactions = $connection->fetchAll("select transactions.status as transactionstatus,transactions.id as transactionid ,profile.* from transactions transactions LEFT JOIN (select * from profile) profile on transactions.userid = profile.profile_id ".$queryParams[0].' group by profile_id',$queryParams[1]);
    foreach($transactions as $key=>$user)
    $transactions[$key] = array_merge($transactions[$key],addUserElements($user));
    return $transactions;
}
function event_get_pending_members($eventid,$refreshType = false,$cacheMeta = array('expiry'=>600)){
    global $connection;
        
        if($refreshType !== true)
        return preprocessApi('event_get_pending_members', $eventid ,$refreshType,$cacheMeta);
        
    $fields['transactions.producttype'] = 'event';
    $fields['transactions.productid'] = $eventid;
    $fields['extraquery'] = "transactions.status <> 'complete|success'";
    $queryParams = construct_list_params($fields);
    $transactions = $connection->fetchAll("select transactions.status as transactionstatus,transactions.id as transactionid ,profile.* from transactions transactions LEFT JOIN (select * from profile) profile on transactions.userid = profile.profile_id ".$queryParams[0].' group by profile_id',$queryParams[1]);
    foreach($transactions as $key=>$user)
    $transactions[$key] = array_merge($transactions[$key],addUserElements($user));
    return $transactions;
}
function page_add($fields){
    global $connection,$PSData;
    if(!empty($fields['page_email']))
    {
        $fields['page_id'] = generate_id("page");
        $fields['page_name'] = $fields['page_name'];
        $fields['page_email'] = $fields['page_email'];
        $fields['creator_id'] = $PSData['user']['profile_id'];
        $fields['dateadded'] = date("Y-m-d H:i:s");
        $fields['status'] = 1; //1=>pagecreated / 2=>pageemailverified
        $pageinsert =  $connection->insertignore('page',$fields);
    if($pageinsert)
    $pageid = $fields['page_id'];
    else
    $pageid = NULL;
        return $pageid;
    }
}
function page_update($where_fields = array('page_id'=>''),$updatefields = array()){
    global $connection;
    $return = $connection->update("page",$updatefields,$where_fields);
    return $return;
}
function page_get_list($fields,$refreshType = false,$cacheMeta = array('expiry'=>1200)){
    global $connection;
    
    if($refreshType !== true)
    return preprocessApi('page_get_list', $fields ,$refreshType,$cacheMeta);
    
    
        
    $queryParams = construct_list_params($fields);
    $page = $connection->fetchAll("select * from page ".$queryParams[0].' order by id desc',$queryParams[1]);
    return $page;
}
function page_get_details($pageid,$refreshType = false,$cacheMeta = array('expiry'=>1200)){
    global $connection;
    
    if($refreshType !== true)
    return preprocessApi('page_get_details', $pageid ,$refreshType,$cacheMeta);
    
    $fields = array();
    $fields['page_id'] = $pageid;
    $queryParams = construct_list_params($fields);
    $page = $connection->fetchAssoc("select * from page ".$queryParams[0]." limit 1",$queryParams[1]);
    return $page;
}
function ticket_add($fields){
    global $connection,$PSData;
    if(!empty($fields['tkteventid']) && !empty($fields['tktemail']))
    {
        $fields['tktid'] = ($fields['tktid'])?$fields['tktid']:generate_id("tkt");
        $ticketinsertid =  $connection->insertignore('eventtickets',$fields);
    if($ticketinsertid)
    $tktid = $fields['tktid'];
    else
    $tktid = NULL;
        return $tktid;
    }
}
function ticket_get_list($fields = array(),$refreshType = false,$cacheMeta = array('expiry'=>60)){
    
    global $connection;
    if(empty($fields))
    return;
    
    if($refreshType !== true)
    return preprocessApi('ticket_get_list', $fields ,$refreshType,$cacheMeta);
    
    //$fields['tkttransactionid'] = $txnid;
    $queryParams = construct_list_params($fields);
    $tktlist = $connection->fetchAll("select * from eventtickets ".$queryParams[0],$queryParams[1]);
    return $tktlist;
}
function ticket_update($tktid,$incomingfields=array()){
    global $connection;
    @session_start();
    $fields = array();
    $fields['tktid'] = $tktid;
    $return = $connection->update("eventtickets",$incomingfields,$fields);
    return $return;
}
function ticketcategories_add($fields){
    global $connection,$PSData;
    if(!empty($fields['tktcateventid']))
    {
        $fields['tktcatid'] = ($fields['tktcatid'])?$fields['tktcatid']:generate_id("tktcat");
        $fields['tktcatdateadded'] = date("Y-m-d H:i:s");
        $ticketinsertid =  $connection->insertignore('eventticketcategories',$fields);
    if($ticketinsertid)
    $tktid = $fields['tkcattid'];
    else
    $tktid = NULL;
        return $tktid;
    }
}
function ticketcategories_get_list($fields = array(),$refreshType = false , $cacheMeta = array('expiry'=>600)){
    
    global $connection;
    if(empty($fields))
    return;
    
    if($refreshType !== true)
    return preprocessApi('ticketcategories_get_list', $fields ,$refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $tktlist = $connection->fetchAll("select * from eventticketcategories ".$queryParams[0],$queryParams[1]);
    return $tktlist;
}
function ticketcategories_update($tktcatid,$incomingfields=array()){
    global $connection;
    @session_start();
    $fields = array();
    $fields['tktcatid'] = $tktcatid;
    $return = $connection->update("eventticketcategories",$incomingfields,$fields);
    return $return;
}
function pagemeta_add($fields){
    global $connection;
    $fields['pageAdmin'] = $_SESSION['user']['profile_id'];
    return $connection->insert('pagemeta',$fields);
}
function pagemeta_update($pagemetaid,$incomingfields=array()){
    global $connection;
    @session_start();
    $fields['pagemetaid'] = $pagemetaid;
    $fields['pageAdmin'] = $_SESSION['user']['profile_id'];
    $return = $connection->update("pagemeta",$incomingfields,$fields);
    return $return;
}
function pagemeta_get_details($fields,$refreshType = false , $cacheMeta = array('expiry'=>1440)){
    global $connection;
    if(!($fields['pagemetaid'] || ($fields['pageType'] && $fields['pageId'])))
    return;
    
    if($refreshType !== true)
    return preprocessApi('pagemeta_get_details', $fields ,$refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $transaction = $connection->fetchAssoc("select * from pagemeta ".$queryParams[0]." limit 1",$queryParams[1]);
    return $transaction;
}
function coupon_add($fields){
    global $connection;
    $fields['couponCreatedBy'] = $_SESSION['user']['profile_id'];
    return $connection->insert('coupons',$fields);
}
function coupon_update($couponid,$incomingfields=array()){
    global $connection;
    @session_start();
    $fields['couponId'] = $couponid;
    $fields['couponCreatedBy'] = $_SESSION['user']['profile_id'];
    $return = $connection->update("coupons",$incomingfields,$fields);
    return $return;
}
function coupon_consume($couponCode,$couponsConsumed,$fields=array()){
    global $connection;
    if($couponCode && $couponsConsumed && $fields['couponEntityId'] && $fields['couponEntityType'])
    $return = $connection->executeUpdate("UPDATE coupons set couponsRemaining = couponsRemaining - ? where couponCode = ? and couponEntityType = ? and couponEntityId = ? limit 1",array($couponsConsumed,$couponCode,$fields['couponEntityType'],$fields['couponEntityId']));
    return $return;
}
function coupon_get_details($fields,$refreshType = false ,$cacheMeta = array('expiry'=>1440)){
    global $connection;
    if(empty($fields['couponEntityId']) || empty($fields['couponEntityType']))
    return;
    
    if($refreshType !== true)
    return preprocessApi('coupon_get_details', $fields ,$refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $coupons = $connection->fetchAll("select * from coupons ".$queryParams[0]." order by id desc",$queryParams[1]);
    return $coupons;
}
function event_data($fields,$detailedview = true,$showcount = false){
    global $connection;
    if(empty($fields['eventid']) && empty($fields['extraquery']))
    return;
    
    $fields['transactions.productid'] = $fields['eventid'];
    unset($fields['eventid']);
    $queryParams = construct_list_params($fields);
	if($showcount)
	{
	// $datas = $connection->fetchAll("select count(*) as count from eventtickets LEFT JOIN transactions on transactions.transactionid = eventtickets.tkttransactionid /*LEFT JOIN  events on transactions.productid = events.event_id*/ LEFT JOIN eventticketcategories on eventticketcategories.tktcatid = eventtickets.tktcode LEFT JOIN profile on transactions.userid = profile.profile_id ".$queryParams[0]." order by transactions.id desc",$queryParams[1]);
	$datas = $connection->fetchAll("select count(*) as count from eventtickets LEFT JOIN transactions on transactions.transactionid = eventtickets.tkttransactionid /*LEFT JOIN  events on transactions.productid = events.event_id*/ LEFT JOIN eventticketcategories on eventticketcategories.tktcatid = eventtickets.tktcode LEFT JOIN profile on transactions.userid = profile.profile_id ".$queryParams[0]." order by transactions.id desc",$queryParams[1]);
    }
	else if($detailedview)
	{
		// $datas = $connection->fetchAll("select transactions.productid as eventid,transactions.transactionid,transactions.status,transactions.amount,transactions.initialisedate as dateofbooking,eventtickets.tktid,eventtickets.tktid,eventtickets.tktemail,eventtickets.tktname,eventtickets.tktgender,eventtickets.tktbibno,eventtickets.tktextra,eventtickets.tktmobile,eventtickets.tktdob,eventticketcategories.tktcatid,eventticketcategories.tktcatname,profile.profile_id,profile.fname,profile.lname,profile.email,profile.gender from eventtickets LEFT JOIN transactions on transactions.transactionid = eventtickets.tkttransactionid /*LEFT JOIN  events on transactions.productid = events.event_id*/ LEFT JOIN eventticketcategories on eventticketcategories.tktcatid = eventtickets.tktcode LEFT JOIN profile on transactions.userid = profile.profile_id ".$queryParams[0]." order by transactions.id desc",$queryParams[1]);
		// $datas = $connection->fetchAll("select transactions.productid as eventid,transactions.transactionid,transactions.initialisedate as dateofbooking,eventtickets.tktid,eventtickets.tktid,eventtickets.tktemail,eventtickets.tktname,eventtickets.tktgender,eventtickets.tktbibno,eventtickets.tktextra,eventtickets.tktmobile,eventtickets.tktdob,eventticketcategories.tktcatid,eventticketcategories.tktcatname,profile.profile_id,profile.fname,profile.lname,profile.email,profile.gender from eventtickets LEFT JOIN transactions on transactions.transactionid = eventtickets.tkttransactionid /*LEFT JOIN  events on transactions.productid = events.event_id*/ LEFT JOIN eventticketcategories on eventticketcategories.tktcatid = eventtickets.tktcode LEFT JOIN profile on transactions.userid = profile.profile_id ".$queryParams[0]." order by transactions.id desc",$queryParams[1]);
		$datas = $connection->fetchAll("select transactions.productid as eventid,transactions.transactionid,transactions.initialisedate as dateofbooking,eventtickets.tktid,eventtickets.tktid,eventtickets.tktemail,eventtickets.tktname,eventtickets.tktgender,eventtickets.tktbibno,eventtickets.tktextra,eventtickets.tktmobile,eventtickets.tktdob,eventticketcategories.tktcatid,eventticketcategories.tktcatname,transactions.userid from eventtickets LEFT JOIN transactions on transactions.transactionid = eventtickets.tkttransactionid /*LEFT JOIN  events on transactions.productid = events.event_id*/ LEFT JOIN eventticketcategories on eventticketcategories.tktcatid = eventtickets.tktcode ".$queryParams[0]." order by transactions.id desc",$queryParams[1]);
		
		if($datas)
		{
		$questions = $connection->fetchAll("select question from  questions where relatedtotype = 'eventquestionaire' and relatedto = '".$datas[0]['eventid']."' and status = 1");
		}
		if($questions)
		{
			foreach($datas as $key=>$data){
				$extra = unserialize($datas[$key]['tktextra']);
				foreach($extra as $key2=>$value2)
				{
					$datas[$key]['ques-'.get_alphanumeric($questions[$key2]['question'])] = $value2;
				}
				unset($datas[$key]['tktextra']);
			}
		}
	}
	else
	$datas = $connection->fetchAll("select transactions.transactionid,transactions.initialisedate as dateofbooking,eventtickets.tktid,eventtickets.tktemail,eventtickets.tktname,eventtickets.tktgender,eventtickets.tktmobile,eventtickets.tktdob,eventticketcategories.tktcatname from eventtickets LEFT JOIN transactions on transactions.transactionid = eventtickets.tkttransactionid /*LEFT JOIN  events on transactions.productid = events.event_id*/ LEFT JOIN eventticketcategories on eventticketcategories.tktcatid = eventtickets.tktcode LEFT JOIN profile on transactions.userid = profile.profile_id ".$queryParams[0]." order by transactions.id desc",$queryParams[1]);
	// $datas = $connection->fetchAll("select transactions.transactionid,transactions.status,transactions.amount,transactions.initialisedate as dateofbooking,eventtickets.tktid,eventtickets.tktemail,eventtickets.tktname,eventtickets.tktgender,eventtickets.tktmobile,eventtickets.tktdob,eventticketcategories.tktcatname from eventtickets LEFT JOIN transactions on transactions.transactionid = eventtickets.tkttransactionid /*LEFT JOIN  events on transactions.productid = events.event_id*/ LEFT JOIN eventticketcategories on eventticketcategories.tktcatid = eventtickets.tktcode LEFT JOIN profile on transactions.userid = profile.profile_id ".$queryParams[0]." order by transactions.id desc",$queryParams[1]);
    
    return $datas;
}
function mailercampaign_add($fields){
    global $connection;
    return $connection->insert('mailercampaign',$fields);
}
function mailercampaign_get_list($fields,$refreshType = false,$cacheMeta = array('expiry'=>60)){
    global $connection;
    
    if($refreshType !== true)
    return preprocessApi('mailercampaign_get_list', $fields, $refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $mailercampaigns = $connection->fetchAll("select * from mailercampaign ".$queryParams[0]." ",$queryParams[1]);
    return $mailercampaigns;
}
function mailercampaign_update($mailerid,$incomingfields=array()){
    global $connection;
    @session_start();
    $fields['mailerid'] = $mailerid;
    $return = $connection->update("mailercampaign",$incomingfields,$fields);
    return $return;
}
function attendeefields_manage($wherefields = array(),$updatefields=array()){
    global $connection;
    if(!($wherefields['productid'] && $wherefields['producttype'] && $updatefields['fields']))
    return false;
    @session_start();
    $updatefields['fields'] = addslashes(serialize($updatefields['fields']));
    $updatefields['updatelog'] = addslashes('updated by '.$_SESSION['user']['profile_id'].' on '.date("Y-m-d H:i:s"));
    $return = $connection->executeUpdate("insert into attendeefields (productid,producttype,fields,updatelog) values ('".$wherefields['productid']."','".$wherefields['producttype']."','".$updatefields['fields']."','".$updatefields['updatelog']."') ON DUPLICATE KEY UPDATE fields = '".$updatefields['fields']."',updatelog = CONCAT(updatelog,'|".$updatefields['updatelog']."') ",array());
    return true;
}
function attendeefields_get_details($fields = array()){
    global $connection;
    if(!($fields['productid'] && $fields['producttype']))
    return false;
    $queryParams = construct_list_params($fields);
    $data = $connection->fetchAssoc("select * from attendeefields ".$queryParams[0]." ",$queryParams[1]);
    $data['fields'] = unserialize($data['fields']);
    return $data;
}
function datetimeslot_add($fields){
    global $connection;
    return $connection->insertignore('datetimeslots',$fields);
}
function datetimeslot_update($updateFields,$whereFields=array()){
    global $connection;
    @session_start();
    $return = $connection->update("datetimeslots",$updateFields,$whereFields);
    return $return;
}
function datetimeslot_get_list($fields,$refreshType = false ,$cacheMeta = array('expiry'=>1440)){
    global $connection;
    
    if(empty($fields['producttype']) || empty($fields['productid']))
    return;
    
    if($refreshType !== true)
    return preprocessApi('datetimeslot_get_list', $fields ,$refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $slots = $connection->fetchAll("select * from datetimeslots ".$queryParams[0]." order by bookingenddate asc",$queryParams[1]);
    return $slots;
}

function htmlblocks_manage($updatefields=array()){
    global $connection;
    if(!($updatefields['productid'] && $updatefields['producttype'] && $updatefields['blockid']))
    return false;
    @session_start();
    $updatefields['data'] = serialize($updatefields['data']);
    $return = $connection->executeUpdate("insert into htmlblocks (productid,producttype,blockid,title,type,data,addedon) values (?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE data = ?",array($updatefields['productid'],$updatefields['producttype'],$updatefields['blockid'],$updatefields['title'],$updatefields['type'],$updatefields['data'],date("Y-m-d H:i:s"),$updatefields['data']));
    return true;
}
function htmlblocks_get_list($fields){
    global $connection;
    
    if(empty($fields['producttype']) || empty($fields['productid']))
    return;
    
    
    $queryParams = construct_list_params($fields);
    $blocks = $connection->fetchAll("select * from htmlblocks ".$queryParams[0],$queryParams[1]);
    $newblocks = array();
    foreach($blocks as $block)
    {
        $block['data'] = unserialize($block['data']);
        if($block['type'] == 'itenarary')
        {
            $datain = $block['data'];
            $data = array();
            foreach($datain['time'] as $dayno=>$data1)
            {
                $temp = array();
                $temp['dayno'] = $dayno;
                foreach($data1 as $key=>$val)
                {
                    $temp2 = array();
                    $temp2['time'] = $val;
                    $temp2['item'] = $datain['item'][$dayno][$key];
                    $temp2['dayno'] = $dayno;
                    $temp['itemlist'][] = $temp2;
                }
                $data[] = $temp;
            }
        }
        $block['data'] = $data;
        $newblocks[$block['type']][]=$block;
    }
    return $newblocks;
}
function event_invite_add($fields){
    global $connection;
    return $connection->insertignore('eventinvites',$fields);
}
function event_invite_update($updateFields,$whereFields=array()){
    global $connection;
    @session_start();
    $return = $connection->update("eventinvites",$updateFields,$whereFields);
    return $return;
}
function event_invite_get_list($fields,$refreshType = false ,$cacheMeta = array('expiry'=>1440)){
    global $connection;
    
    if(empty($fields['producttype']) || empty($fields['productid']))
    return;
    
    if($refreshType !== true)
    return preprocessApi('event_invite_get_list', $fields ,$refreshType,$cacheMeta);
    
    $queryParams = construct_list_params($fields);
    $slots = $connection->fetchAll("select eventinvites.*,profile.fname,profile.lname from eventinvites LEFT JOIN profile on profile.profile_id = eventinvites.profile_id ".$queryParams[0]." order by dateadded desc",$queryParams[1]);
    return $slots;
}

function group_posts($fields = array(),$page = 1,$refreshType = false /*false,true,delete*/,$cacheMeta = array('expiry'=>10)){
    global $connection, $PSParams;
    
    if (isset($fields['postId']) === true) {
        $post_list = $connection->fetchAll("select ps_posts.*,profile.fname,profile.lname,profile.profile_id,profile.userdp from ps_posts LEFT JOIN profile on ps_posts.createdBy = profile.id WHERE ps_posts.id = ? ORDER BY ps_posts.id DESC", array($fields['postId']));
    } else {
        $paging['page'] = $page;
        $paging['show'] = 5;
        $fields['paging'] = $paging;

        if ($paging['page'] > 1) {
            $offset = ($page-1) * $paging['show'];
            $limitParams = sprintf("%d, %d", $offset, $paging['show']);
        } else {
            $limitParams = sprintf("%d", $paging['show']);
        }
        
        // if module is group detail page, event detail page, passion category page
        if ($fields['moduleName'] == 'passion') {
            $moduleId = $fields['moduleId'];
            $post_list = $connection->fetchAll("select ps_posts.*,profile.fname,profile.lname,profile.profile_id,profile.userdp from ps_posts JOIN profile on ps_posts.createdBy = profile.id AND category =? ORDER BY ps_posts.createdAt DESC LIMIT $limitParams", array($fields['moduleId']));
            
        } elseif ($fields['moduleName'] == 'community' || $fields['moduleName'] == 'event') {
            $post_list = $connection->fetchAll("select ps_posts.*,profile.fname,profile.lname,profile.profile_id,profile.userdp from ps_posts LEFT JOIN profile on ps_posts.createdBy = profile.id WHERE module =? AND moduleId =? ORDER BY ps_posts.createdAt DESC LIMIT $limitParams", array($fields['moduleName'], $fields['moduleId']));
        } elseif ($fields['moduleName'] == 'userprofile') {
            $post_list = $connection->fetchAll("select ps_posts.*,profile.fname,profile.lname,profile.profile_id,profile.userdp from ps_posts LEFT JOIN profile on ps_posts.createdBy = profile.id WHERE profile.profile_id =? ORDER BY ps_posts.createdAt DESC LIMIT $limitParams", array($fields['moduleId']));
        } else {
            // home -> user's passion specific
            // profile -> logged in user specific, user tagging, joined community 
            $post_list = $connection->fetchAll("select ps_posts.*,profile.fname,profile.lname,profile.profile_id,profile.userdp from ps_posts LEFT JOIN profile on ps_posts.createdBy = profile.id ORDER BY ps_posts.createdAt DESC LIMIT $limitParams", array());
        }
    }
    
    foreach ($post_list as $key => $data) {
        $post_list[$key]['fullname'] = ucwords($data['fname'].' '.$data['lname']);
        $post_list[$key]['createrseopath']= get_alphanumeric($post_list[$key]['fullname']).'/'.$data['profile_id'];
        $post_list[$key]['userdp']=get_upload_path($data['userdp'],'profiledp');
        $post_list[$key]['userbg']=get_upload_path($data['userbg'],'coverbg');
        $post_list[$key]['create_date']=strtotime($data['createdAt']);


        $temp = unserialize($data['images']);
        foreach($temp as $key2=>$value2) {
            foreach($value2 as $key3 => $value3) {
                if($post_list[$key]['create_date'] > 1477631272)
                    $temp[$key2][$key3] = get_upload_path('thumb/'.$value3);
                else
                    $temp[$key2][$key3] = get_upload_path($value3);
            }
        }
        $post_list[$key]['images'] = $temp;

        // Post coment
        $postcomments = $connection->fetchAll("SELECT gc.*, concat(pr.fname, ' ', pr.lname) AS user, pr.userdp, pr.profile_id FROM group_comments AS gc JOIN profile AS pr ON pr.id = gc.userId WHERE postId =? ORDER BY id DESC LIMIT 3", array($data['id']));
        if (is_array($postcomments) === true) {
           $postcomments = array_reverse($postcomments);
           $commentCount = count($postcomments);
           if ($commentCount == 3) {
               $postcommentsCount = $connection->fetchAssoc("SELECT count(1) AS total FROM group_comments WHERE postId =?", array($data['id']));
               $commentCount = $postcommentsCount['total'];
           }
        } else {
            $commentCount = 0;
        }

        // Post likes
        $post_list[$key]['comments'] = array(
            'count'     => $commentCount,
            'comments'  => $postcomments
        );
        $postlikes = $connection->fetchAll("SELECT gl.*, concat(pr.fname, ' ', pr.lname) AS user FROM group_likes AS gl JOIN profile AS pr ON pr.id = gl.userId WHERE postId = ? ORDER BY id DESC", array($data['id']));
        if (is_array($postlikes) === true) {
           $likesCount = count($postlikes);
        } else {
            $likesCount = 0;
        }
        
        $groupPostLikesUserId = array();
        foreach ($postlikes as $value) {
            $groupPostLikesUserId[] = $value['userId'];
        }

        $groupPostLikesUserName = array();
        foreach ($postlikes as $value) {
            $groupPostLikesUserName[] = $value['user'];
        }
        
        $likeUsers = array_combine($groupPostLikesUserId, $groupPostLikesUserName);

        $post_list[$key]['likes'] = array(
            'count'  => $likesCount,
            'likes'  => $likeUsers
        );
    }

    // Map events and communities in post
    $communitiesId  = array();
    $eventsId       = array();
    foreach ($post_list as $key => $data) {
        if ($data['module'] == 'event') {
            $eventsId[$key] = $data['moduleId'];
        } elseif ($data['module'] == 'community') {
            $communitiesId[$key] = $data['moduleId'];
        }
    }

    if (empty($eventsId) === false) {
        $uniqueEventsId = array_unique($eventsId);
        $event_ids = '';
        foreach ($uniqueEventsId as $key => $value) {
            $event_ids .= sprintf("'%s'", $value).',';
        }
        $event_ids = trim($event_ids, ',');
        $eventdata = $connection->fetchAll("select events.eventname,events.event_id from events where event_id in ($event_ids)");
        if (empty($eventsId) === false) {
            foreach ($eventsId as $postKey => $postEventId) {
                foreach ($eventdata as $k => $eventData) {
                    if ($postEventId == $eventData['event_id']) {
                        $post_list[$postKey]['postRefName'] = $eventData['eventname'];
                        $post_list[$postKey]['postRefUrl']  = 'event/'.get_alphanumeric($eventData['eventname']).'/'.$eventData['event_id'];
                        break;
                    }
                }
            }
        }
    }

    if (empty($communitiesId) === false) {
        $uniqueIds = array_unique($communitiesId);
        $communities_id = '';
        foreach ($uniqueIds as $key => $value) {
            $communities_id .= sprintf("'%s'", $value).',';
        }
        $communities_id = trim($communities_id, ',');
        $communityData = $connection->fetchAll("select groups.name, groups.groupId from groups where groupId in ($communities_id)");

        if (empty($communitiesId) === false) {
            foreach ($communitiesId as $postKey => $postCommunityId) {
                foreach ($communityData as $k => $detail) {
                    if ($postCommunityId == $detail['groupId']) {
                        $post_list[$postKey]['postRefName'] = $detail['name'];
                        $post_list[$postKey]['postRefUrl']  = 'community/'.$detail['groupId'];
                        break;
                    }
                }
            }
        }
    }
    return $post_list;
}

?>