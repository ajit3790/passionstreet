<?php
require_once('initialise.php');
$header = getallheaders();
if ($header['X-Requested-With'] != 'XMLHttpRequest') 
{    
	$response['status'] = '403';    
	//preventing ajax file call from script
} 
else if (empty($_GET['module'])) 
{   
	$response['status'] = '406';
} 
else 
{    
    $modulesNotRequiring          = array(
            "wall"
    );
    $modulesRequiringMinimalLogin      = array(

    );
    $currentModuleRequiresLogin        = !in_array($_GET['module'], $modulesNotRequiring);
    $currentModuleRequiresMinimalLogin = in_array($_GET['module'], $modulesRequiringMinimalLogin);
    //print_array(array('JMA',$currentModuleRequiresLogin && ($_REQUEST['login_status'] == 'false' || empty($_REQUEST['login_status']) || ($_REQUEST['login_status'] == 'true' && !loggedId()))));
    //exit();
    if ($currentModuleRequiresMinimalLogin && empty($_SESSION['user']['profile_id']))
    {
            $response['status'] = '4051';
    }
    else if ($currentModuleRequiresLogin && ($_REQUEST['login_status'] != 'true' || ($_REQUEST['login_status'] == 'true' && !loggedId())))
    {
            $response['status'] = '405';
    }
    else if (!authorized())
    {
            $response['status'] = '403';
    }
    else 
    {
        $response['status'] = '200';
        $rows = array(
            "row_1"=>array(
                    'sectionParams'=>array(
                        'class'=>'section'
                     ),
                     'modules'=>array(
                        $_GET['module']=>array(
                            'modulesParams'=>array()
                        )
                   )
              )
        );

        $page_includes = array(
                "layout"=>"onlymodule"
        );

        $minifyhtml = 1;

        $response['data'] = render_modules();
    }
}
echo $response['data'];
?>