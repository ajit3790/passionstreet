<?php
require_once('initialise.php');
$PSJavascript['login_required'] = false;
$rows = array(
    "row_1"=>array(
        'sectionParams'=>array(
            'class'=>'container-fluid profile-box'
        ),
        'rowWrapper'=>array(),
        'modules'=>array(
            'profiledetail'=>array(
                'modulesParams'=>array()
            )
        )
    ),
    "row_2"=>array(
        'sectionParams'=>array(
            'class'=>'section'
        ),
    'rowWrapper'=>array("<div class='container sm'>","</div>"),
        'columnStructure' =>true,
        'columns'=>array(
            /* array(
                 'columnParams'=>array(
                    'class'=>'col-md-3 sidebar-left sidebar'
                 ),
                'modules'=>array(
                     'memberlisting'=>array(
                        'modulesParams'=>array()
                      )
                 )
             ),*/

            array(
                'columnParams'=>array(
                    'class'=>'col-md-7'
                ),
                'modules'=>array(
                    'wallform'=>array(
                        'modulesParams'=>array()
                    ),
                    'wall'=>array(
                        'modulesParams'=>array()
                    )
                )
            ),
            array(
                'columnParams'=>array(
                    'class'=>'col-md-5 sidebar-right sidebar'
                ),
                'modules'=>array(
                    'memberlisting'=>array(
                        'modulesParams'=>array()
                    ),
		            'eventlisting'=>array(
                        'modulesParams'=>array()
                    )
                 )
            )
        )
    )
);


$page_meta = array(
);

echo render_modules();

?>