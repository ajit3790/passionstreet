<?php
echo "sss";
require_once('initialise.php');
$header = getallheaders();
if ($header['X-Requested-With'] != 'XMLHttpRequest')
{
	$response['status'] = '403';
	//preventing ajax file call from script
}
else if (empty($_GET['type']))
{
	$response['status'] = '406';
}
else
{
	if (!authorized() || !isAdminLoggedIn())
	{
		$response['status'] = '403';
	}
	else
	{
		$type               = $_GET['type'];
		$profile_id         = $_SESSION['user']['profile_id'];
		$response['status'] = '200';
		ob_start();
		unset($_POST['login_status']);
		unset($_POST['token']);
		unset($_GET['login_status']);
		unset($_GET['token']);
		switch ($type)
		{
			case 'eventwisetransactiondata':
				$extraquery = '';
				if($_POST['status'])
				{
					if($_POST['status'] == 'success')
					$extraquery .= ' AND status="complete|success" ';
					if($_POST['status'] == 'failure')
					$extraquery .= ' AND status<>"complete|success" ';
					if($_POST['status'] == 'all')
					$extraquery .= ' ';
				}
				$transaction = $connection->fetchAll("select transactionid,userid,initialisedate,status,amount,couponsapplied from transactions where productid = ? and producttype = ? ".$extraquery." order by id desc",array($_REQUEST['event_id'],'event'));
				foreach($transaction as $key=>$value)
				{
					$temp = unserialize($transaction[$key]['couponsapplied']);
					if($temp)
					{
						$transaction[$key]['coupon'] = $temp['couponcode'];
						$temp2 = explode('|',$temp['pricedistribution']);
						foreach($temp2 as $k2=>$v2)
						{
							$temp3 = explode(":",$v2);
							$transaction[$key][$temp3[0]] = $temp3[1];
						}
					}
					unset($transaction[$key]['couponsapplied']);
				}
                                $response['list'] = $transaction;
                                break;
            case 'eventwiseticketdata':
				$extraquery = '';
				if($_POST['status'])
				{
					if($_POST['status'] == 'success')
					$extraquery .= ' AND tr.status="complete|success" ';
					if($_POST['status'] == 'failure')
					$extraquery .= ' AND tr.status<>"complete|success" ';
					if($_POST['status'] == 'all')
					$extraquery .= ' ';
				}
				$eventdata  = event_get_details($_REQUEST['event_id'],true,PROFILE_ID);
				$transaction = $connection->fetchAll("select tkt.tktname as participant,tkt.tktgender as gender,tkt.tktdob as age,tkt.tktemail,tkt.tktid,tr.transactionid,tkt.tktcode as tktname,tr.initialisedate,tr.status,'tktcost' as 'tktprice',tr.couponsapplied from eventtickets as tkt LEFT JOIN transactions as tr on tr.transactionid = tkt.tkttransactionid where tr.productid = ? and tr.producttype = ? ".$extraquery." order by tr.id desc",array($_REQUEST['event_id'],'event'));
				foreach($transaction as $key=>$value)
				{
					$temp = unserialize($transaction[$key]['couponsapplied']);
					if($temp)
					{
						$transaction[$key]['coupon'] = $temp['couponcode'];
					}
					unset($transaction[$key]['couponsapplied']);
					if($transaction[$key]['tktname'])
					{
						$temp2 = array();
						$temp2['age'] = (date('Y') - date('Y',strtotime($transaction[$key]['age'])));
						$temp2['tktprice'] = $eventdata['ticketcategories'][$transaction[$key]['tktname']]['price'];
						$temp2['tktname'] = $eventdata['ticketcategories'][$transaction[$key]['tktname']]['name'];
						$temp3 = geteventticketprice($temp2['tktprice'],$eventdata['eventtypebyfee'],$eventdata['feepaymentoption']);
						$temp2['grosspayment'] = round($temp3['displaycost'],0);
						$temp2['gateway'] = $temp3['paymentgatewayfee'];
						$temp2['processing'] = $temp3['processingfee'];
						$temp2['ser.tax'] = $temp3['totaltaxfee'];
						$temp2['org.revenue'] = $temp3['organiserrevenue'];
						$temp2['org.pays.ps'] = $temp3['organiserpaysps'];
						$transaction[$key] = array_merge($transaction[$key],$temp2);
					}
					
				}
				$response['list'] = $transaction;
				break;
			case 'eventwiseuserdata':
				$extraquery = '';
				if($_POST['status'])
				{
					if($_POST['status'] == 'success')
					$extraquery .= ' transactions.status="complete|success" ';
					if($_POST['status'] == 'failure')
					$extraquery .= ' transactions.status <>"complete|success" ';
					if($_POST['status'] == 'all')
					$extraquery .= ' ';
				}
				$fields = array();
				$fields['eventid'] = $_REQUEST['event_id'];
				$fields['extraquery'] = $extraquery;
				$response['list'] = event_data($fields);
				break;
			case 'ticketbytxnid':
				if(!empty($_POST['txnid']))
				{
				$response['text'] = generate_event_ticket($_POST['txnid'],1,1);
				}
				break;
			case 'ticketbyemail':
				if(!empty($_POST['email']))
				{
				$_POST['email'] = get_standard_email($_POST['email']);
				$users = user_profile(array('email'=>$_POST['email']));
				$transaction = $connection->fetchAssoc("select transactionid from transactions where userid = ? order by id desc limit 1",array($users['profile_id']));
				//$response['list']['profile'] = $users;
				//$response['list']['transaction'] = $transaction;
				$response['text'] = generate_event_ticket($transaction['transactionid'],1,1);
				}
				break;
			case 'loginasuser':
				if(!empty($_POST['profile_id']))
				{
					$PSData['user'] = user_profile(array("profile_id"=>$_POST['profile_id']));
					$_SESSION['user'] = $PSData['user'];
					$PSJavascript['profile_id'] = $PSData['user']['profile_id'];
					$PSJavascript['login_status'] = true;
				}
				break;
			case 'fbeventfetch':
				if(!empty($_POST['eventids']))
				{
					$response['text'] = curlCustom('GET',array('eventids'=>$_POST['eventids']),'eventfetch',ROOT_PATH.'core/crons/facebookeventfetch.php');
				}
				break;
			default:
				echo NULL;
		}
		$responsedata = ob_get_contents();
		ob_end_clean();
		$response['data']  = $responsedata;
		$response['boxid'] = ($_REQUEST['boxid']) ? $_REQUEST['boxid'] : '';
	}
}
echo json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
//print_array($response);
?>
