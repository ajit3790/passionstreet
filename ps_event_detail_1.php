<?php
require_once('initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
             ),
             'rowWrapper'=>array(),
             'modules'=>array(
                'home1banner'=>array(
                    'modulesParams'=>array()
                )
            )
        ),
    "row_20"=>array(
            'sectionParams'=>array(
                'class'=>'section bg-main-color'
             ),
             'modules'=>array(
                'eventbanner'=>array(
                    'modulesParams'=>array("class"=>"col-md-8")
                ),
                 'eventcard'=>array(
                    'modulesParams'=>array("class"=>"col-md-4")
                )
            )
        ),
    "row_21"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'modules'=>array(
                'eventdetail0'=>array(
                    'modulesParams'=>array()
                )
            )
        ),
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'modules'=>array(
                'eventdetail11'=>array(
                    'modulesParams'=>array()
                )
            )
        ),
    "row_3"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'modules'=>array(
                'memberListing'=>array(
                    'modulesParams'=>array("class"=>"col-md-8 bx-styl")
                ),
                'eventgallery'=>array(
                    'modulesParams'=>array("class"=>"col-md-4")
                )
            )
        ),
    "row_4"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'modules'=>array(
                'eventdetail21'=>array(
                    'modulesParams'=>array()
                )
            )
        ),
    "row_5"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'modules'=>array(
                'wall'=>array(
                    'modulesParams'=>array()
                ),
                'postform'=>array(
                    'modulesParams'=>array()
                )
            )
        )
);

$page_meta = array(
	'title'=>'Event Details'
);

echo render_modules();

?>