<?php
require_once('initialise.php');
$PSJavascript['login_required'] = false;
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
             ),
             'rowWrapper'=>array("<div class=\"jumbotron text-center\">","</div>"),
             'modules'=>array(
                'paymentprocessor'=>array(
                    'modulesParams'=>array()
                )
            )
        )
);

$page_meta['title'] = 'Payment Confirmation';

echo render_modules();

?>