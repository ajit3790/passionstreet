<?php
//include_once('config.php');
require ROOT_DIR.'/Doctrine/Common/ClassLoader.php';
use Doctrine\Common\ClassLoader;
use Doctrine\DBAL\Schema\Schema;
$classLoader = new ClassLoader();
$classLoader->register();
$config = new \Doctrine\DBAL\Configuration();



if(SERVERTYPE == 'live'){
	$connectionParams = array(
		'dbname'    => 'passionstreet',
		'user'      => 'psdbadmin',
		'password'  => 'dbadmin@ps911',
		'host'      => 'localhost',
		'driver'    => 'mysqli',
	);
}
else if(SERVERTYPE == 'stg'){
	$connectionParams = array(
		'dbname'    => 'passionstreet_stg',
		'user'      => 'psdbadmin',
		'password'  => 'dbadmin@ps911',
		'host'      => 'localhost',
		'driver'    => 'mysqli',
	);
}
else if(SERVERTYPE == 'local'){
    $connectionParams = array(
            'dbname'    => 'passionstreet',
            'user'      => 'root',
            'password'  => '',
            'host'      => 'localhost',
            'driver'    => 'mysqli',
    );
}
$connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
?>