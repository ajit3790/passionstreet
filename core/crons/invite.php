<?php
require_once(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'initialise.php');

$invite_list = invite_list(array('status'=>1,'mailerstatus'=>0));
foreach($invite_list as $invite)
{
	$invitee = current(user_get_list(array('profile_id'=>$invite['invitee'])));
	ob_start();
	renderer_member($invitee,2);
	$invitee_data = ob_get_contents();
	ob_end_clean();
	if(mailer(array("email_id"=>$invite['invited']),'invitation',array("data"=>$invitee_data.'<hr />',"title"=>$invitee['fullname']." has invited you to join ".($invitee['gender']=='female'?'her':'him')." on PASSIONSTREET","altmsg"=>"")));
	{
		invite_update($invite['inviteid'],array('mailerstatus'=>1));
		echo 'Invite sent to '.$invite['invited'].'<br />';
	}
}
?>
