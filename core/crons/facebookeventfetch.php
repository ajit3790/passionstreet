<?php
require_once(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'initialise.php');
set_time_limit(0);
//error_reporting(E_ALL);
require_once CORE_DIR . '/php-graph-sdk-4.0.9/autoload.php';
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

// init app with app id and secret
$app_id = '652032041525469';
$app_secret = 'e6cbe362f51e9934335f4c53727b6608';

FacebookSession::setDefaultApplication( '652032041525469','e6cbe362f51e9934335f4c53727b6608' );

$accesstoken = 'EAAJRBQo3oN0BAD780ZCvVwA9SZB5E8SD5PFJTZAnINxOoCTQf9ZCec9yB8O90N7KfjjXglYtkfThEwMTS6ZCL5OfoFdO5wpZBIyCgqSDnXWw2zwUbGigwQc1ZBqtPdFUUiR7rzWxINSCT4rezf6VZCFSTe3jXTHdbqgZD';
if($accesstoken)
{
	try{
		$session = new FacebookSession($accesstoken);
	}
	catch( SessionException $ex){
		//print_array($ex);
		$accesstoken = null;
	}
	catch( Exception $ex ) {
		//print_array($ex);
		$accesstoken = null;
	}
}
if(empty($accesstoken))
{
	$helper = new FacebookRedirectLoginHelper(ROOT_PATH.'core/crons/facebookeventfetch.php' );
	try {
		$session = $helper->getSessionFromRedirect();
	} catch( FacebookRequestException $ex ) {
		// When Facebook returns an error
	} catch( Exception $ex ) {
		// When validation fails or other local issues
	}
	if ( isset( $session ) )
	{
	$session->getLongLivedSession(); //generate long lived accesstoken;
	}
}

/*
$request = new FacebookRequest( $session, 'GET', '/1570164663?fields=id,ids_for_pages{page,id}' ,null,'v2.9');
try{
$response = $request->execute();
} 
catch( FacebookResponseException $ex ) {
	// When Facebook returns an error
	print_array(array('response',$ex));
}
catch( Exception $ex ) {
	// When validation fails or other local issues
	print_array(array('xxxxx',$ex));
}
$graphObject = $response->getGraphObject();
$x = $graphObject->getProperty('data');    

print_array($graphObject);
print_array($response);
print_array($x);
exit();
*/

$passions['running'] = array('marathon','triathlon','ultra marathon','run','trail running','10K','21K');
$passions['cycling'] = array('cycle','cycling','MTB');
$passions['motorbiking'] = array('ride','biking','mountain biking','bike');
$passions['trekking'] = array('trek','hiking','hike','camping');
$passions['music'] = array('music','sufi','rock','band');
$passions['painting'] = array('paint','draw','art craft');
$passions['photography'] = array('photo','lense');
$passions['dancing'] = array('dance','dancing');
$passions['adventure'] = array('adventure trip','adventure sport','adventure');
$passions['culinary'] = array('Food','Food Drinks','Dining','Cooking','Chef','Cuisine');

$cities = array('Delhi','Mumbai','Noida','Pune','Bangalore','Chennai','Hyderabad','Kolkata','Guwahati','Shillong','Mashobra','Ladakh','Manali','Jaipur','Chandigarh','Amritsar','Shimla','Dehradun');
// $cities = array('Delhi','Mumbai');

// see if we have a session
if ( isset( $session ) ) {
	//graph api request for user data
	//$request = new FacebookRequest( $session, 'GET', '/me' );
	//$request = new FacebookRequest( $session, 'GET', '/search?q=coffee&type=place&center=28.63145,77.21769&distance=1000' );
	//$request = new FacebookRequest( $session, 'GET', '/search?type=event&q=""' ,null,'v2.9');
	$i = 0;
	$eventids = '';
	if(executiontype == 'cron')
	$eventids = $argv[1];
	else if(executiontype == 'application')
	$eventids = $_GET['eventids'];
	if($eventids)
	{
		$request = new FacebookRequest( $session, 'GET', '?ids='.$eventids.'&fields=name,description,start_time,end_time,updated_time,ticket_uri,place,cover,admins{id,name,link,profile_type},id,is_page_owned,owner&limit=100&after='.$paging['cursors']['after'] ,null,'v2.9');
		$response = $request->execute();
		$graphObject = $response->getGraphObject();
		$events = array();
		if($graphObject)
		{
			$eventstemp = json_decode(json_encode($graphObject->asArray()),true);
			if(empty($events))
			$events = $eventstemp;
			else if(empty($eventstemp))
			$events = $events;
			else
			$events = array_merge($events,$eventstemp);
		}
		processfbevents($events,1);
		
	}
	else
	{
		$newarray = array();
		foreach($passions as $passion=>$keywords){
			foreach($cities as $city)
			{
				foreach($keywords as $keyword)
				{
					$newarray[$passion][$city][$keyword] = 1;
				}
			}
		}
		//print_array($newarray);
		$j = 0;
		while(1)
		{
		if(empty($newarray))
		break;
		$passionkeys 	= array_keys($newarray);
		$passionindex 	= rand(0,count($newarray) -1);
		$passion 	= $passionkeys[$passionindex];
		
		$citykeys 	= array_keys($newarray[$passion]);
		$cityindex 	= rand(0,count($newarray[$passion]) -1);
		$city 		= $citykeys[$cityindex];
		
		$keywordkeys 	= array_keys($newarray[$passion][$city]);
		$keywordindex 	= rand(0,count($newarray[$passion][$city]) -1);
		$keyword	= $keywordkeys[$keywordindex];
		
		unset($newarray[$passion][$city][$keyword]);
		if(empty($newarray[$passion][$city]))
		unset($newarray[$passion][$city]);
		if(empty($newarray[$passion]))
		unset($newarray[$passion]);
		
		$events = array();
		$eventstemp = array();
		$page = 1;
		$paging = array();
		$page  = 0;
		do{
			$page++;
			// $request = new FacebookRequest( $session, 'GET', '/search?type=event&q='.$keyword.' '.$city.'&fields=name,description,start_time,end_time,ticket_uri,place,cover,admins{id,name,link,profile_type},id,is_page_owned,owner,posts,feed&limit=2' ,null,'v2.9');
			$request = new FacebookRequest( $session, 'GET', '/search?type=event&q='.$keyword.' '.$city.'&fields=name,description,start_time,end_time,updated_time,ticket_uri,place,cover,admins{id,name,link,profile_type},id,is_page_owned,owner&limit=100&after='.$paging['cursors']['after'] ,null,'v2.9');
			$response = $request->execute();
			$graphObject = $response->getGraphObject();
			$x = $graphObject->getProperty('data');
			
			$pagetemp = $graphObject->getProperty('paging');
			if(!empty($pagetemp))
			$paging = json_decode(json_encode($graphObject->getProperty('paging')->asArray()),true);
			else
			$paging = array();
			$eventstemp = array();
			if($graphObject->getProperty('data'))
			{
				$eventstemp = json_decode(json_encode($graphObject->getProperty('data')->asArray()),true);
				if(empty($events))
				$events = $eventstemp;
				else if(empty($eventstemp))
				$events = $events;
				else
				$events = array_merge($events,$eventstemp);
			}
			echo "\n\nQuery = ".$keyword." ".$city." page-".$page."\n*********************************\n";
		} while (!empty($eventstemp));
		processfbevents($events);
		}
		/*
		$allowedtime = strtotime('-30 days', time());
		foreach($passions as $passion=>$keywords)
		{
			foreach($keywords as $keyword)
			{
				foreach($cities as $city)
				{
					$events = array();
					$eventstemp = array();
					$page = 1;
					$paging = array();
					$page  = 0;
					do{
						$page++;
						// $request = new FacebookRequest( $session, 'GET', '/search?type=event&q='.$keyword.' '.$city.'&fields=name,description,start_time,end_time,ticket_uri,place,cover,admins{id,name,link,profile_type},id,is_page_owned,owner,posts,feed&limit=2' ,null,'v2.9');
						$request = new FacebookRequest( $session, 'GET', '/search?type=event&q='.$keyword.' '.$city.'&fields=name,description,start_time,end_time,updated_time,ticket_uri,place,cover,admins{id,name,link,profile_type},id,is_page_owned,owner&limit=100&after='.$paging['cursors']['after'] ,null,'v2.9');
						$response = $request->execute();
						$graphObject = $response->getGraphObject();
						$x = $graphObject->getProperty('data');
						
						$pagetemp = $graphObject->getProperty('paging');
						if(!empty($pagetemp))
						$paging = json_decode(json_encode($graphObject->getProperty('paging')->asArray()),true);
						else
						$paging = array();
						$eventstemp = array();
						if($graphObject->getProperty('data'))
						{
							$eventstemp = json_decode(json_encode($graphObject->getProperty('data')->asArray()),true);
							if(empty($events))
							$events = $eventstemp;
							else if(empty($eventstemp))
							$events = $events;
							else
							$events = array_merge($events,$eventstemp);
						}
						echo "\n\nQuery = ".$keyword." ".$city." page-".$page."\n*********************************\n";
					} while (!empty($eventstemp));
					processfbevents($events);
				}
			}
		}
		*/
	}
} else {
  $loginUrl = $helper->getLoginUrl();
 header("Location: ".$loginUrl);
}



function processfbevents($events,$forceadd = 0){
	global $session,$i,$passion;
	$eventaddedids = array();
	$eventaddednamesowner = array();
	$organiserids = array();
	$allowedtime = strtotime('-15 days', time());
	foreach($events as $event)
	{
		if(strtotime($event['updated_time']) <= $allowedtime)
		{
		echo "Failed : Old Event ".$event['id'].' -> '.$event['name']."\n";
		continue;
		}
		if(empty($forceadd))
		{
		if(!($event['is_page_owned']))
		{
		echo "Failed : Blocked - Individual Event ".$event['id'].' -> '.$event['name']."\n";
		continue;
		}
		if(strlen($event['description'])<150)
		{
		echo "Failed : Blocked - Description Short Event ".$event['id'].' -> '.$event['name']."\n";
		continue;
		}
		}
		if(@filemtime(UPLOAD_DIR.DIRECTORY_SEPARATOR.'eventbanner'.DIRECTORY_SEPARATOR.'eventbanner_'.$event['id'].'.jpg'))
		{
		echo "Failed : Event already in db ".$event['id'].' -> '.$event['name']."\n";
		continue;
		}
		if($eventaddedids[$event['id']])
		{
		echo "Failed : Repeated Event ".$event['id'].' -> '.$event['name']."\n";
		continue;
		}
		if($eventaddednamesowner[$event['name'].'-'.$event['owner']['id']])
		{
		echo "Failed : Repeated Event ".$event['id'].' -> '.$event['name']."\n";
		continue;
		}
		if(returnBadWords($event['name']) || returnBadWords($event['description']))
		{
		echo "Failed : Blocked - Badword found Event ".$event['id'].' -> '.$event['name']."\n";
		continue;
		}
		
		$insertData = array();
		// $insertData['event_id'] = generate_id('event');
		$insertData['event_id'] = $event['id'];
		$insertData['producttype'] = 'event';
		$insertData['eventtypebyfee'] = 1;
		$insertData['eventtype'] = '';
		$insertData['source'] = 'aggregated';
		$insertData['scope'] = 'public';
		$insertData['eventname'] = $event['name'];
		$insertData['category'] = $passion;
		// if($event['ticket_uri'])
		// $insertData['tickettype'] = 'paid';
		// else
		$insertData['tickettype'] = 'free';
		$insertData['multislot'] = 0;
		$insertData['datestart'] = date("Y-m-d H:i:s",strtotime($event['start_time']));
		$insertData['dateend'] = date("Y-m-d H:i:s",strtotime($event['end_time']));
		// $insertData['bookingenddate'] = $insertData['datestart'];
		$insertData['bookingenddate'] = '0000-00-00 00:00:00';
		$event['description'] = nl2br(chop_string($event['description'],20000));
		$insertData['description'] = chop_string(strip_tags($event['description']),180);
		$insertData['eventinfo']['description'] = $event['description'];
		$insertData['eventinfo']['terms'] = '';
		if($event['place']['location']['street'])
		$insertData['eventinfo']['address'] = $event['place']['name'].','.$event['place']['location']['street'].','.$event['place']['location']['city'];
		else if($event['place']['location']['city'])
		$insertData['eventinfo']['address'] = $event['place']['name'].','.$event['place']['location']['city'];
		$insertData['eventinfo']['address'] = $event['place']['name'];
		$insertData['eventinfo']['maploc'] = json_encode(array("lat"=>$event['place']['location']['latitude'],"lng"=>$event['place']['location']['longitude']));
		$insertData['eventinfo']['mapimage'] = '';
		$insertData['eventinfo']['termsdoc'] = '';
		$insertData['eventinfo']['organiser'] = array();
		$insertData['eventinfo']['organiser']['id'] = $event['owner']['id'];
		$insertData['eventinfo']['organiser']['name'] = $event['owner']['name'];
		$insertData['eventinfo']['organiser']['type'] = ($event['is_page_owned'])?'fbpage':'fbuser';
		if($event['is_page_owned'])
		{
			$organiserids[$insertData['event_id']] = $insertData['eventinfo']['organiser']['id'];
		}
		else if(!($forceadd))
		{
			echo "Failed : user event ".$event['id'].' -> '.$event['name']."\n";
			$insertData['eventinfo']['organiser']['type'] = 'fbuser';
			continue;
		}
		$insertData['eventinfo']['aggregatesource'] = 'facebook';
		$insertData['eventinfo']['sourcedata']['ticket_url'] = $event['ticket_uri'];
		$insertData['eventinfo']['sourcedata']['admins'] = $event['admins'];
		$insertData['eventinfo']['sourcedata']['id'] = $event['id'];
		$insertData['venue'] = ($event['place']['name'])?$event['place']['name']:$city;
		$temp = explode(',',$event['place']['location']['city']);
		$event['place']['location']['city'] = $temp[0];
		$insertData['city'] = ($event['place']['location']['city'])?$event['place']['location']['city']:$city;
		$insertData['state'] = '';
		$insertData['eventpic'] = $event['cover']['source'];
		$insertData['createdate'] = date("Y-m-d H:i:s");
		$insertData['status'] = 'published';
		$eventaddedids[$event['id']] = $insertData;
		$eventaddednamesowner[$event['name'].'-'.$event['owner']['id']] = 1;
		//if($i == 2)
		//exit();
	}
	
	$organisercount = count($organiserids);
	$bulkorganisercount = 10;
	for($j=0;$j<((int)($organisercount/$bulkorganisercount) + 1);$j++)
	{
		$temporganisers = array_slice($organiserids,$j*$bulkorganisercount,$bulkorganisercount,true);
		if(empty($temporganisers))
		continue;
		$request2 = new FacebookRequest( $session, 'GET', '/?ids='.implode(',',$temporganisers).'&fields=emails,contact_address,single_line_address' ,null,'v2.9');
		$response2 = $request2->execute();
		$graphObject2 = $response2->getGraphObject();
		$organiserdatas = json_decode(json_encode($graphObject2->asArray()),true);
		foreach($organiserdatas as $organiserid=>$organiserdata){
			foreach($organiserids as $eventid=>$organiserid2)
			{
				if($eventaddedids[$eventid] && $organiserid2 == $organiserid)
				{
					if($organiserdata['emails'][0])
					{
						//echo "Success : Event $i ".$eventid.' -> '.$eventaddedids[$eventid]['eventname']."\n";
						$eventaddedids[$eventid]['eventinfo']['organiser']['email'] = get_standard_email($organiserdata['emails'][0]);
						$eventaddedids[$eventid]['eventinfo']['organiser']['address'] = $organiserdata['single_line_address'];
					}
					/*else
					{
						echo "Failed : Email Id not provided for page event ".$eventid.' -> '.$eventaddedids[$eventid]['eventname']."\n";
						unset($eventaddedids[$eventid]);
					}*/
				}
			}
		}
	}
	//$eventaddedids['832175993606631']['eventinfo']['organiser']['email'] = 'jauharamir@gmail.com';
	foreach($eventaddedids as $eventid=>$insertData){
		$i++;
		echo "Success : Event $i ".$eventid .' -> '.$insertData['eventname']."\n";
		$insertData['eventpic'] = serialize(array(imageresizecrop($insertData['eventpic'],'eventbanner',true,array('width'=>1000,'height'=>500),null,null,$eventid)));
		if(event_create($insertData))
		{
			// echo "Insert Success : Event $i ".$insertData['event_id'].' -> '.$insertData['eventname']."\n";
			$insertData2 = array();
			$insertData2['producttype'] = 'event';
			$insertData2['productid'] = $insertData['event_id'];
			$insertData2['slotid'] = generate_id("slot");
			$insertData2['datestart'] = $insertData['datestart'];
			$insertData2['dateend'] = $insertData['dateend'];
			$insertData2['bookingenddate'] = $insertData['bookingenddate'];
			datetimeslot_add($insertData2);
			if($insertData['eventinfo']['organiser']['email'])
			{
			mailer(array('email_id'=>$insertData['eventinfo']['organiser']['email'],'event_id'=>$insertData['event_id'],'eventname'=>$insertData['eventname'],'eventurl'=>ROOT_PATH.'event/'.get_alphanumeric($insertData['eventname']).'/'.$insertData['event_id']),'eventclaiminvite',array(),'blank.tpl');
			sleep(3);
			}
		}
		// else
		// echo "Insert Failed : Event $i ".$insertData['event_id'].' -> '.$insertData['eventname']."\n";
	}
}
?>