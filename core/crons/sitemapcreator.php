<?php
require_once(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'initialise.php');

//header("Content-type: text/xml; charset=utf-8");

$urlset = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" /><!--?xml version="1.0" encoding="UTF-8"?-->');
$event_list  = event_get_list(array('fields'=>array('status'=>'published','paging'=>array('showall'=>1))));

foreach ($event_list as $item):

    $url = $urlset->addChild('url');
    $url->addChild('loc', ROOT_PATH.'event/'.$item['seopath'] );
    // $url->addChild('lastmod', gmdate('c',strtotime($item['createdate'])) );
    $url->addChild('lastmod', gmdate('c',time() ));
    $url->addChild('priority', '1.0');

    //add an image
    if ( $item['eventpic']):
        $image = $url->addChild('image:image', null, 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:loc',$item['eventpic'], 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:caption',$item['eventname'].' - Banner' , 'http://www.google.com/schemas/sitemap-image/1.1');
    endif;
    if ( $item['mapimage']):
        $image = $url->addChild('image:image', null, 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:loc',$item['mapimage'], 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:caption',$item['eventname'].' - Location on Map' , 'http://www.google.com/schemas/sitemap-image/1.1');
    endif;
    if ( $item['routeimage']):
        $image = $url->addChild('image:image', null, 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:loc',$item['routeimage'], 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:caption',$item['eventname'].' - Route Map' , 'http://www.google.com/schemas/sitemap-image/1.1');
    endif;

endforeach;

//add whitespaces to xml output (optional, of course)
$dom = new DomDocument();
$dom->loadXML($urlset->asXML());
$dom->formatOutput = true;
//output xml

// echo $dom->saveXML();
$dom->save(UPLOAD_DIR.DIRECTORY_SEPARATOR.'sitemaps'.DIRECTORY_SEPARATOR.'events.xml');

$urlset = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" /><!--?xml version="1.0" encoding="UTF-8"?-->');
$user_list  = user_get_list();
foreach ($user_list as $item):

    $url = $urlset->addChild('url');
    $url->addChild('loc', ROOT_PATH.'profile/'.$item['seopath'] );
    $url->addChild('lastmod', gmdate('c',time()) );
    $url->addChild('priority', '0.5');

    //add an image
    if ( $item['userdp']):
        $image = $url->addChild('image:image', null, 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:loc',$item['userdp'], 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:caption',$item['fullname'].' - User Profile Picture' , 'http://www.google.com/schemas/sitemap-image/1.1');
    endif;
    if ( $item['userbg']):
        $image = $url->addChild('image:image', null, 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:loc',$item['userbg'], 'http://www.google.com/schemas/sitemap-image/1.1');
        $image->addChild('image:caption',$item['fullname'].' - User Cover Picture' , 'http://www.google.com/schemas/sitemap-image/1.1');
    endif;

endforeach;

//add whitespaces to xml output (optional, of course)
$dom = new DomDocument();
$dom->loadXML($urlset->asXML());
$dom->formatOutput = true;
//output xml

// echo $dom->saveXML();
$dom->save(UPLOAD_DIR.DIRECTORY_SEPARATOR.'sitemaps'.DIRECTORY_SEPARATOR.'profiles.xml');
?>
