<?php
$mailer_title = "Transaction update";
$mailer_alt = "Attached is an update on transaction you did on PASSIONSTREET .";
?>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:0px">
    <tbody>
        <tr><td style="width:200px">Transaction Id</td><td><?=$data['txnid']?></td></tr>
        <tr><td style="width:200px">Transaction Amount</td><td>Rs. <?=$data['amount']?></td></tr>
        <tr><td style="width:200px">Transaction Status</td><td><?=implode(' | ',explode('|',$data['status']))?></td></tr>
        <tr><td style="width:200px">Product Type</td><td><?=$data['producttype']?></td></tr>
        <tr><td style="width:200px">Product ID</td><td><?=$data['productid']?></td></tr>
        <tr><td style="width:200px">Product PAGE</td><td><a href="<?=$data['producturl']?>" target="_blank">Click to View</a></td></tr>
    </tbody>
</table>