<?php
$mailer_title = "Welcome to PASSIONSTREET ";
$mailer_alt = "Action Required for account activation";
?>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:0px">
    <tbody>
        <tr>
            <td style="padding-bottom:8px">
                Welcome to PASSIONSTREET!
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
               PASSIONSTREET is an online activity based networking community for collaborating with like minded passionate individuals, experts, groups and organisations who share a common goal. It is also a place to host and participate in events & workshops, wellness programs, adventure tourism, sports activity campaign and much more. 
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                PASSIONSTREET helps your desire to live for what you love to do the most!
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                <br />You are just a step away from activating your membership account.
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                <a target="_blank" style="color:#1e7cdc;text-decoration:underline" href="<?=ROOT_PATH?>?module=mailprocessor&title=Profile%20Activation&p=<?=urlencode('action=activation&profile_id=--psuserid--&token=--pstoken--&t=--pstime--')?>">Click here to 
                    <span class="il">Activate</span> your account.
                </a>
            </td>
        </tr>
    </tbody>
</table>