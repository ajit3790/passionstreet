<?php
$mailer_title = "Verify pages page on PASSIONSTREET ".$data['pages_name'];
$mailer_alt = ucwords($data['user_name']).' has requested to create a page with this as official email ID';
?>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:0px">
    <tbody>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">We have received a request from <?=$data['user_name']?> (<?=$data['user_email']?>) to create a page <br />
	    <span style="width:150px;display:inline-block">Page Name</span> 
	    <span style=""><?=$data['page_name']?></span> <br />
	    <span style="width:150px;display:inline-block">Page Email</span> 
	    <span style=""><!--##psemail##--></span>	<br />    
	    
	    </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                <a target="_blank" style="color:#1e7cdc;text-decoration:underline" href="<?=ROOT_PATH?>?module=mailprocessor&title=Verify%20page&p=<?=urlencode('action=verifypage&profile_id=--psuserid--&email_id=--psemail--&token=--pstoken--&t=--pstime--&ext='.base64_encode('usr='.$data['user_id'].'&pg='.$data['page_id']))?>">Click here to 
                    <span class="il">Verify the page</span>.
                </a>
            </td>
        </tr> 
    </tbody>
</table>