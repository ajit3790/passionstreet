<?php
$mailer_title = "Reset PASSIONSTREET account password";
$mailer_alt = "";
?>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:0px">
    <tbody>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">We have received a request for PASSIONSTREET account password reset.<br />Please click below link to reset your password<br />Kindly ignore if you have not initiated this request.</td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                <a target="_blank" style="color:#1e7cdc;text-decoration:underline" href="<?=ROOT_PATH?>?module=mailprocessor&title=Reset%20Password&p=<?=urlencode('action=resetpassword&profile_id=--psuserid--&email_id=--psemail--&token=--pstoken--&t=--pstime--')?>">Click here to 
                    <span class="il">RESET</span> your password.
                </a>
            </td>
        </tr> 
    </tbody>
</table>