
    <!-- content -->
    <tr>
    	<td  align="left" valign="top" style="padding:5px 20px 20px;">
        	<table width="100%"  cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right" style="font-family:Arial, sans-serif; color:#000; padding-bottom:30px; font-size:11px; line-height:14px;"><!--##psmailerdate##--></td>
                </tr>
            	<tr>
                    <td align="left" style="font-family:Arial, sans-serif; color:#000; padding-bottom:10px; font-size:16px; font-weight:bold; line-height:18px;"><!--##psheadersalutation##--></td>
                </tr>
                <tr>
                    <td align="left" style="font-family:Arial, sans-serif; color:#333; padding-bottom:10px; font-size:14px; line-height:22px;">
                        <?php
                        if($mailcontent[$currentmailertype]['tpl'])
                        {
                            $mailercontentfromdb = -1;
                            @include_once (TEMPLATE_DIR.'/mailer/mailertype/'.$mailcontent[$currentmailertype]['tpl']);
                        }
                        else
                        {
                            $mailercontentfromdb = 1;
                            echo '<!--##psmailerbody##-->';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="">
                        <!--##psfootersalutation##-->
                    </td>
                </tr>
                
            </table>
        </td>
    </tr>
    <!-- /content -->