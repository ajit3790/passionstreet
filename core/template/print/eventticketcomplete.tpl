<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
  
  <style>
  h1,h2,h3,h4,h5,h6{
      margin:4px 0px;
  }
  p{
      margin:0px;
      line-height:2em;
  }
  *{
		margin:0px;
  }
  </style>

</head>

<body style="width:100%;margin:auto;font-family: helvetica;background-color:#dddddd !important">
<div style="width:100%;margin:auto;font-family: helvetica;background-color:#dddddd !important">
<div style="width:700px;margin:auto;">
<table border="0" width="100%">
  <tr>
      <td colspan="2">
          <table width="100%" style="margin-bottom:20px;margin-top:10px ">
              <tr>
                  <td width="50%">
                      <h2><!--##eventtitle##--></h2>
                  </td>
                  <td width="50%" style="text-align: right">
                      <img style="float: right; clear: both;width:300px"   src="<!--##root_path##-->images/logo.png" />
                  </td>
              </tr>
              <tr>
                  <td colspan="2">
                      <strong><!--##eventsubtitle##--></strong>
                  </td>
              </tr>
          </table>
      </td>
  </tr>
  <tr>
      <td width="50%">
          Order No. - <!--##transactionid##-->
      </td>
      <td  width="50%" style="float:right;text-align: right">
          <span class="amount">AMOUNT: <!--##transactionamount##--></span>
      </td>
  </tr>
  <tr>
      <td colspan="2" style="padding-bottom:10px;padding-left:2%;padding-right:2%;padding-top:10px;background-color:#ffffff !important">
		<!--##transactiontickets##-->
      </td>
  </tr>
  <tr>
      <td colspan="2">
          <h2><!--##eventtitle2##--></h2>
          <table width="100%" style="border: 1px solid #cfcfcf;    margin-bottom: 20px !important;padding:20px !important;background-color:#ffffff !important">
            <tr>
                <td width="100%">
                    <p><!--##eventtimedetailed##--></p>
                    <p><!--##eventvenue##--></p>
                </td>
            </tr>
			<tr>
                <td width="100%">
                    <!--##eventtables##-->
                </td>
            </tr>
          </table>
      </td>
  </tr>
  <tr>
      <td colspan="2">
          <h2>Organizer</h2>
          <table width="100%" style="border: 1px solid #cfcfcf;    margin-bottom: 20px !important;padding:20px !important;background-color:#ffffff  !important">
            <tr>
                <td width="100%">
                    <p><strong><!--##eventorganiser##--></strong></p><br />
                    <p><strong>Note:</strong> For any further clarification you can reach out to <strong><!--##eventorganiseremail##--></strong></p>
                </td>
            </tr>
          </table>
      </td>
  </tr>
</table>
</div>
</div>
</body>
</html>
