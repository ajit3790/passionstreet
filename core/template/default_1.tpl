<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<base href="<?=ROOT_PATH?>">
<?php generate_meta()?>
<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css?<?=filemtime('css/bootstrap.min.css')?>" />
<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-theme.min.css?<?=filemtime('css/bootstrap-theme.min.css')?>" />
<link rel="stylesheet" type="text/css" media="screen" href="css/style.css?<?=filemtime('css/style.css')?>" />
<link rel="stylesheet" type="text/css" media="screen" href="css/style_extra.css?<?=filemtime('css/style_extra.css')?>" />
<style>
@import url('https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,500,500italic,700,700italic');
@import url('https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700');
body {
    font-family: sans-serif;
}
.fonts-loaded body{
  font-family: 'Roboto', sans-serif;
}
</style>
<script src='js/jquery-1.11.3.min.js' type='text/javascript'></script>
<script>
<?php
    foreach($PSJavascript as $key=>$value)
    {
        echo "\tvar ".$key." = ".json_encode($value).";\r\n";
    }
?>

/*$.ajaxSetup({
    data: {
        login_status:login_status,
        token:formtoken
    }
});*/
</script>
</head>
<body>
<div id="container"> 
<?php include(MODULE_DIR.'/login/login.php');?>
<?php if($page_includes['header']) include(MODULE_DIR.'/header/header.php');?>
<?php
foreach($rows as $rowName=>$rowDetails)
{
	echo '<section class="hrows '.$rowDetails['sectionParams']['class'].'" '.$rowDetails['sectionParams']['attrs'].'>'."\r\n";
	echo $rowDetails['rowWrapper'][0]."\r\n\t";
        echo '<div class="row '.$rowDetails['rowParams']['class'].'" '.$rowDetails['rowParams']['attrs'].'>'."\r\n\t";
        foreach($rowDetails['columns'] as $columnsIndex=>$columnDetails)
        {
            //$columnDetails['columnParams']['class'] = ($columnDetails['columnParams']['class'])?$columnDetails['columnParams']['class']:'col-md-12';
            //$columnDetails['columnParams']['class'] = $columnDetails['columnParams']['class'] . ' no-padding';
            echo '<div class="col '.$columnDetails['columnParams']['class'].'" '.$columnDetails['columnParams']['attrs'].'>';
            foreach($columnDetails['modules'] as $module=>$moduleParams)
            {
                echo '<div class="module '.$moduleParams['name'].' '.$moduleParams['moduleIdentifier'].' '.$moduleParams['class'].'" '.$moduleParams['attrs'].'>'."\r\n\t";
                echo $modules_html[$moduleParams['moduleIdentifier']]."\r\n\t";
                echo '</div>'."\r\n\t";
            }
            echo '</div>';
        }
        echo '</div>'."\r\n\t";
        echo $rowDetails['rowWrapper'][1]."\r\n\t";
        echo '</section>'."\r\n\t";
}
?>
<?php if($page_includes['sitemap']) include(MODULE_DIR.'/sitemap/sitemap.php');?>
</div>
<?php if($page_includes['footer']) include(MODULE_DIR.'/footer/footer.php');?>
<div class="jbox-container">
    <div class="img-alt-text"></div>
    <img src="" />
    <i id="prev" class="fa fa-angle-left"></i>
    <i id="next" class="fa fa-angle-right"></i>
    <i id="close" class="fa fa-times"></i>
</div>
<div role="dialog" tabindex="-1" class="modal fade" id="modal-box">
    <div class="modal-dialog popup1 login-box">
        <h2>WELCOME TO PASSIONSTREET</h2>
        <div class="modal-container">
        <div class="row two-clm">
            <div class="col-md-5 col-sm-5">
                    <div class="login_form" style="display:block">
                    <h5>Please enter your details below to login</h5>
                    <div class="row">
                        <span style="display:none" class="error-text">
                            Invalid userID or Password
                        </span>
                        <div class="col-md-12">
                            <div class="form-group">
                                <i class="fa fa-user"></i>
                                <input type="email" name="email" id="loginemail" required placeholder="Email " class="form-control">
                              </div>
                        </div>
                      </div>
                      <div class="row">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <i class="fa fa-lock"></i>
                                <input type="password" name="password" id="loginpassword" required placeholder="Password" class="form-control">
                              </div>
                        </div>
                      </div> 

                      <div class="row">
                        <div class="col-md-12 ">
                            <input type="button" id="login_btn" class="btn btn-primary" value="LOGIN" />
                        </div>
                      </div>
                      </div>
                    
                      <div class="forgot_form" style="display:none">
                      <div class="row">
                        <span style="display:none" class="error-text">
                            Invalid userID or Password
                        </span>
                        <div class="col-md-12">
                            <div class="form-group">
                                <i class="fa fa-user"></i>
                                <input type="email" id="frgt_email" required placeholder="Email" class="form-control">
                              </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 ">
                            <input type="button" id="send_pswd_btn" class="btn btn-primary" value="SEND VERIFICATION LINK" />
                        </div>
                      </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                              <a class="forgot" id="forgot_password_btn" alternatetext="Back To Login Form" href="#">Forgot Password?</a>
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col-md-12 new">
                              New User? <a href="register">Sign UP Now</a>
                        </div>
                </div>              
                
            </div>
            <div class="col-md-5 col-sm-5 pull-right">
                <h5>You can also login using one of your social/email accounts</h5>
                <div class="social">
                    <a href='module/loginsns' class="callmodaliframe1 createloginpopup" targetsrc="module/loginsns/fb" type="fb" type1="facebook"><span class="fb"><i class="fa fa-facebook"></i> Login with Facebook</span></a>
                    <a href='module/loginsns' class="callmodaliframe1 createloginpopup" targetsrc="module/loginsns/tw" type="tw" type1="twitter"><span class="twtr"><i class="fa fa-twitter"></i> Login with Twitter</span></a>
                    <a href='module/loginsns' class="callmodaliframe1 createloginpopup" targetsrc="module/loginsns/gg" type="gg" type1="google"><span class="gplus"><i class="fa fa-google-plus"></i> Login with Google</span></a>
                </div>
            </div>
        </div>
        </div>
    </div>  
</div>
<?php
foreach($PSJsincludes as $type=>$files){
    $attr = '';
    if($type=='external')
    $attr = ' async ';
    foreach($files as $jsinclude)
    echo "<script ".$attr." src='".$jsinclude."' type='text/javascript'></script>\r\n";
};
foreach($PSCssincludes as $type=>$files){
    foreach($files as $cssinclude){
        echo "<link rel='stylesheet' type='text/css' media='screen' href='".$cssinclude."' />\r\n";
    }
};
?>
<!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</body>
</html>