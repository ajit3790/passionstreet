<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<base href="<?=ROOT_PATH?>">
<script src='js/jquery-1.11.3.min.js' type='text/javascript'></script>
<script>
<?php
    foreach($PSJavascript as $key=>$value)
    {
        echo "\tvar ".$key." = ".json_encode($value).";\r\n";
    }
?>
</script>
</head>
<body style="padding-top:inherit;background:none;">
<div id="container" class="no_min_height internalIframe"> 
<?php include(MODULE_DIR.'/login/login.php');?>
<?php
foreach($rows as $rowName=>$rowDetails)
{
	echo '<section class="hrows '.$rowDetails['sectionParams']['class'].'" '.$rowDetails['sectionParams']['attrs'].'>'."\r\n";
	echo $rowDetails['rowWrapper'][0]."\r\n\t";
        echo '<div class="row '.$rowDetails['rowParams']['class'].'" '.$rowDetails['rowParams']['attrs'].'>'."\r\n\t";
        foreach($rowDetails['columns'] as $columnsIndex=>$columnDetails)
        {
            //$columnDetails['columnParams']['class'] = ($columnDetails['columnParams']['class'])?$columnDetails['columnParams']['class']:'col-md-12';
            //$columnDetails['columnParams']['class'] = $columnDetails['columnParams']['class'] . ' no-padding';
            echo '<div class="col '.$columnDetails['columnParams']['class'].'" '.$columnDetails['columnParams']['attrs'].'>';
            foreach($columnDetails['modules'] as $module=>$moduleParams)
            {
                echo '<div class="module '.$moduleParams['name'].' '.$moduleParams['moduleIdentifier'].' '.$moduleParams['class'].'" '.$moduleParams['attrs'].'>'."\r\n\t";
                echo $modules_html[$moduleParams['moduleIdentifier']]."\r\n\t";
                echo '</div>'."\r\n\t";
            }
            echo '</div>';
        }
        echo '</div>'."\r\n\t";
        echo $rowDetails['rowWrapper'][1]."\r\n\t";
        echo '</section>'."\r\n\t";
}
?>
</div>
<!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php
foreach($PSJsincludes['external2'] as $jsinclude){
    echo "<script src='".$jsinclude."' type='text/javascript'></script>\r\n";
};
foreach($PSCssincludes['external2'] as $cssinclude){
    echo "<link rel='stylesheet' type='text/css' media='screen' href='".$cssinclude."' />\r\n";
};
?>
<script>
function importParentStyles() {
    try{
        var parentStyleSheets = parent.document.styleSheets;
	console.log('parentStyleSheets');
	console.log(parentStyleSheets);
        var cssString = "";
        for (var i = 0, count = parentStyleSheets.length; i < count; ++i) {
            if (parentStyleSheets[i].cssRules) {
		var cssRules = parentStyleSheets[i].cssRules;
                for (var j = 0, countJ = cssRules.length; j < countJ; ++j)
                    cssString += cssRules[j].cssText;
            }
            else
                cssString += parentStyleSheets[i].cssText;  // IE8 and earlier
        }
        var style = document.createElement("style");
        style.type = "text/css";
        try {
		console.log('try');
            style.innerHTML = cssString;
            document.getElementsByTagName("head")[0].appendChild(style);
        }
        catch (ex) {
		console.log('catch');
            style.styleSheet.cssText = cssString;  // IE8 and earlier
        }
    }
    catch (ex) {
        
    }
}
document.addEventListener('DOMContentLoaded',function(){

    importParentStyles();
    parent.iframeLoaded();
    var links = document.getElementsByTagName('a');
    var len = links.length;
    for(var i=0; i<len; i++)
    {
       links[i].target = "_parent";
    }
})
$(document).ready(function(){
    if(typeof close_window != 'undefined' && close_window)
    setTimeout(function(){ parent.closemyModal(); }, 2000);
    
    $("body").on("click change focus",function(){
        parent.iframeLoaded();
    });
});
</script>
</body>
</html>