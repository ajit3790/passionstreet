<?php
$mailer_title = "Every little bit counts! You are awesome...";
$mailer_alt = "Thank you for your contribution to the cause.";
$mailer_footersalutation = "Thank you";
?>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:0px">
    <tbody>
        <tr><td>
			We like to thank you for contributing <span style="font-weight:bold">Rs. <?=round($data['amount'])?></span> towards <span style="font-weight:bold"><?=ucwords($data['productname'])?></span>, a charitable event. Your contribution is certainly help to achieve the objective of the event.
			<br />
			<br />
			Please accept our sincere appreciation!
		</td></tr>
    </tbody>
</table>