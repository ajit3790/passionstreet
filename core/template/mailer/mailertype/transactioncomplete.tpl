<?php
$mailer_title = "Transaction update";
$mailer_alt = "Attached is an update on transaction you did on PASSIONSTREET .";
$temp = explode('|',$data['status']);
?>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:0px">
    <tbody>
        <tr><td style="width:200px">Transaction Id</td><td><?=$data['txnid']?></td></tr>
        <tr><td style="width:200px">Transaction Amount</td><td>Rs. <?=round($data['amount'])?></td></tr>
        <tr><td style="width:200px">Transaction Status</td><td><?=ucwords(($temp[1])?$temp[1]:$temp[0])?></td></tr>
        <tr><td style="width:200px">Type</td><td><?=ucwords($data['producttype'])?></td></tr>
        <tr><td style="width:200px">Name</td><td><?=ucwords($data['productname'])?></td></tr>
        <tr><td style="width:200px">ID</td><td><?=$data['productid']?></td></tr>
        <tr><td style="width:200px">URL</td><td><a href="<?=$data['producturl']?>" target="_blank">Click to View</a></td></tr>
    </tbody>
</table>