<?php
$mailer_title = "Is anything missing in ".$data['eventname'];
$mailer_alt = 'Congratulations , your event has been listed on PASSIONSTREET';
?>
<div>Hi,</div>
<div>&nbsp;</div>
<div>Are you missing out something in your event <b><?=$data['eventname']?></b> posted on Facebook? Add online ticketing options using PASSIONSTREET platform and engage with your target audience.</div>
<div>&nbsp;</div>
<div>PASSIONSTREET is a community driven networking and user engagement platform&nbsp;for self managed event ticketing, adventure tourism, sports activity campaign management and promotion.&nbsp;It is closely integrated with other social media platforms so that your events get the maximum milage. Use the following link to enable ticketing option in your upcoming event/s listed on Facebook and let&nbsp;your audience buy event tickets online.&nbsp;</div>
<div>&nbsp;</div>
<div>
<?php
$time = time();
//$mailtoken = strtolower(hash('sha512', implode("|",array($data['email_id'],$data['event_id'],$time,ENCRYPTION_KEY))));
$temp = array();
$temp['email_id'] = $data['email_id'];
$temp['event_id'] = $data['event_id'];
$temp['time']     = $time;
$mailtoken = createsignedrequest($temp);
?>
<a href="<?=$data['eventurl']?>" target="_blank" style="color:#000000; text-decoration:none; display:inline; line-height:32px; font-family:Helvetica, Arial, sans-serif; font-size:14px;background-color:#ffd200;padding:10px 15px;text-align:center;margin-right:10px;">View Event</a>
<!--<a target="_blank" style="color:#000000; text-decoration:none; display:inline; line-height:32px; font-family:Helvetica, Arial, sans-serif; font-size:14px;background-color:#ffd200;padding:10px 15px;text-align:center;margin-right:10px;" href="<?=$data['eventurl']?>?module=mailprocessor&title=Verify%20page&p=<?=urlencode('action=claimfbevent&profile_id=--psuserid--&email_id=--psemail--&mailtoken=--pstoken--&t=--pstime--&ext='.base64_encode('email='.$data['email_id'].'&fbeventid='.$data['event_id']))?>">Manage / Claim Event</a>-->
<!--<a target="_blank" style="color:#000000; text-decoration:none; display:inline; line-height:32px; font-family:Helvetica, Arial, sans-serif; font-size:14px;background-color:#ffd200;padding:10px 15px;text-align:center;margin-right:10px;" href="<?=$data['eventurl']?>?email_id=--psemail--&action=claimeventfromemail&mailtoken=<?=$mailtoken?>&t=<?=$time?>">Manage / Claim Event</a>-->
<a target="_blank" style="color:#000000; text-decoration:none; display:inline; line-height:32px; font-family:Helvetica, Arial, sans-serif; font-size:14px;background-color:#ffd200;padding:10px 15px;text-align:center;margin-right:10px;" href="<?=$data['eventurl']?>?email_id=--psemail--&mailtoken=<?=$mailtoken?>&action=claimeventfromemail">Manage / Claim Event</a>
</div>
<div>&nbsp;</div>
<div>Our pricing are the best in the&nbsp;industry.&nbsp;And please note, we&nbsp;do not charge our commission for any charitable event hosting on our platform. &nbsp; &nbsp;</div>
<div>&nbsp; &nbsp;&nbsp;</div>
<div>WHY YOU SHOULD USE PASSIONSTREET?</div>
<ul>
<!--<li>You can list your Organisation and Manage your users free</li>-->
<li>Use our self-managed Ticketing Engine and boost your events.</li>
<li>Use our other Social Media plug-in, including Facebook Widget for better conversion</li>
<li>Get listed in Featured Events</li>
<li>Attendee Registration Management Module</li>
<li>Convenient Payment, Collection &amp; Tax management</li>
<li>Multi-level Discount Coupon Management</li>
<li>Pre and Post Event User Engagement</li>
<li>Event Analytics&nbsp;</li>
<li>Sponsors Management</li>
<li>Customised Notification and Display Elements</li>
<li>Intelligent SEO for Google page ranking</li>
<!--<li>Event Promotion tools &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</li>-->
</ul>
<div>If you want to know more, please write to us at&nbsp;<a href="mailto:social@passionstreet.in" target="_blank" rel="noopener">social@passionstreet.in</a></div>
<div>&nbsp;</div>
<div>Yours&nbsp;Sincerely</div>
<div>Team PASSIONSTREET</div>