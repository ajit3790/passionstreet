<?php
$mailer_title = "Welcome to PASSIONSTREET";
$mailer_alt = "Account activation request";
?>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:0px">
    <tbody>
        <tr>
            <td style="padding-bottom:8px">
                <!--##psmailerbody##-->
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                PASSIONSTREET is an online activity based networking community for collaborating with like minded passionate individuals, experts, groups and organizations, who share a common goal. It is also a place to host and participate in events & workshops, wellness programs, adventure tourism, sports activity campaign and much more.
            </td>
        </tr>
        <tr>
	    <td style="padding-bottom:8px;line-height:22px">
                <a target="_blank" style="color:#1e7cdc;text-decoration:underline" href="<?=ROOT_PATH?>register"> 
                    <span class="il">Click here</span>  to Sign Up
                </a>
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                <table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:30px">
                    <tr>
                    <?php
                    $count = 0;
                    foreach($GLOBALS['PSParams']['PSCategories'] as $i=>$param){
                        echo '<td style="width:25%;padding:5px">';
                            echo '<a href="'.ROOT_PATH.$param['link'].'"><img src="'.ROOT_PATH.'images/'.$param['icon'].'" alt="'.$param['name'].'" style="width:100%" /></a>';
                        echo '</td>';
                        $count++;
                        if(($count % 4)==0)
                        echo '</tr><tr>';
                    }
                    ?>  
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>