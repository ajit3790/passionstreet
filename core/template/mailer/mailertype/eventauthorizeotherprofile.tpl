<?php
$mailer_title = "Authorization request - ".$data['eventname'];
$mailer_alt = '';
$time = time();
?>
<div>Hi,</div>
<div>&nbsp;</div>
<div>With regard to your event <b><?=$data['eventname']?></b> posted on PASSIONSTREET , <b><?=$data['authorizeemailid']?></b> has requested to manage the event on your behalf.</div>
<div>&nbsp;</div>
<div>
<a href="<?=$data['eventurl']?>" target="_blank" style="color:#000000; text-decoration:none; display:inline; line-height:32px; font-family:Helvetica, Arial, sans-serif; font-size:14px;background-color:#ffd200;padding:10px 15px;margin-bottom:20px;text-align:center;margin-right:10px;">View Event</a>
<?php
$temp = array();
$temp['email_id'] = $data['email_id'];
$temp['event_id'] = $data['event_id'];
$temp['time']     = $time;
$mailtoken = createsignedrequest($temp);
// $mailtoken = strtolower(hash('sha512', implode("|",array($data['email_id'],$data['event_id'],$time,ENCRYPTION_KEY))));
?>
<a target="_blank" style="color:#000000; text-decoration:none; display:inline; line-height:32px; font-family:Helvetica, Arial, sans-serif; font-size:14px;background-color:#ffd200;padding:10px 15px;margin-bottom:20px;text-align:center;margin-right:10px;" href="<?=$data['eventurl']?>?email_id=--psemail--&action=claimeventfromemail&mailtoken=<?=$mailtoken?>&t=<?=$time?>">Manage / Claim Event</a>
<?php
$temp = array();
$temp['email_id'] = $data['email_id'];
$temp['event_id'] = $data['event_id'];
$temp['authorizeemailid'] = $data['authorizeemailid'];
$temp['authorizeprofileid'] = $data['authorizeprofileid'];
$temp['time']     = $time;
$mailtoken = createsignedrequest($temp);
// $mailtoken = strtolower(hash('sha512', implode("|",array($data['email_id'],$data['event_id'],$data['authorizeemailid'],$data['authorizeprofileid'],$time,ENCRYPTION_KEY))));
?>
<br /><br /><a target="_blank" style="color:#000000; text-decoration:none; display:inline; line-height:32px; font-family:Helvetica, Arial, sans-serif; font-size:14px;background-color:#ffd200;padding:10px 15px;margin-bottom:20px;text-align:center;margin-right:10px;" href="<?=$data['eventurl']?>?email_id=--psemail--&action=eventauthorizeotherprofile&mailtoken=<?=$mailtoken?>">Authorize <b><?=$data['authorizeemailid']?></b></a>
</div>
<div>&nbsp;</div>
<div>If you want to know more, please write to us at&nbsp;<a href="mailto:social@passionstreet.in" target="_blank" rel="noopener">social@passionstreet.in</a></div>
<div>&nbsp;</div>
<div>Yours&nbsp;Sincerely</div>
<div>Team PASSIONSTREET</div>