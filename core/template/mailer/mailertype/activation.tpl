<?php
$mailer_title = "Welcome to PASSIONSTREET";
$mailer_alt = "Account activation request";
?>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:0px">
    <tbody>
        <tr>
            <td style="padding-bottom:8px">
                Welcome to PASSIONSTREET!
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
               It gives us immense pleasure to have you on PASSIONSTREET, which advocates action that will evoke passion for doing things you are passionate about and enjoy doing, consciously and responsibly! Here you join and share a bond with individuals having similar passion, explore and exhibit your activities, which you are passionate about, connect with subject matter experts, join relevant events & workshops and keep track of your workout activities.
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                PASSIONSTREET helps your desire to live for what you love to do the most!
            </td>
        </tr>
	<tr>
            <td style="padding-bottom:8px;line-height:22px">
                It is also a platform for self managed activity campaign management, event & workshop ticketing, guided adventure tourism, corporate engagement and much more.
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                <br />You are just click away to complete the sign-up process.
            </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                <a target="_blank" style="color:#1e7cdc;text-decoration:underline" href="<?=ROOT_PATH?>?module=mailprocessor&title=Profile%20Activation&p=<?=urlencode('action=activation&profile_id=--psuserid--&mailtoken=--pstoken--&t=--pstime--')?>"> 
                    <span class="il">Click here</span> to activate your account! 
                </a>
            </td>
        </tr>
	<tr>
            <td style="padding-bottom:8px;line-height:22px">
                <table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:30px">
                    <tr>
                    <?php
                    $count = 0;
                    foreach($GLOBALS['PSParams']['PSCategories'] as $i=>$param){
                        echo '<td style="width:25%;padding:5px">';
                            echo '<a href="'.ROOT_PATH.$param['link'].'"><img src="'.ROOT_PATH.'images/'.$param['icon'].'" alt="'.$param['name'].'" style="width:100%" /></a>';
                        echo '</td>';
                        $count++;
                        if(($count % 4)==0)
                        echo '</tr><tr>';
                    }
                    ?>  
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>