<?php
$mailer_title = "PASSIONSTREET account password reset request";
$mailer_alt = "";
?>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size:14px;padding:0px">
    <tbody>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">We have received a request to reset the password for your account. </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">If you have requested to reset the password for your account, click the link below, otherwise please ignore this mail.   </td>
        </tr>
        <tr>
            <td style="padding-bottom:8px;line-height:22px">
                <a target="_blank" style="color:#1e7cdc;text-decoration:underline" href="<?=ROOT_PATH?>?module=mailprocessor&title=Reset%20Password&p=<?=urlencode('action=resetpassword&profile_id=--psuserid--&email_id=--psemail--&mailtoken=--pstoken--&t=--pstime--')?>"> 
                    <span class="il">Click Here</span>to reset your password.
                </a>
            </td>
        </tr> 
    </tbody>
</table>