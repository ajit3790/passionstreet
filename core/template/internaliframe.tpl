<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<base href="<?=ROOT_PATH?>">
<script src='js/jquery-1.11.3.min.js' type='text/javascript'></script>
<script>
<?php
    foreach($PSJavascript as $key=>$value)
    {
        echo "\tvar ".$key." = ".json_encode($value).";\r\n";
    }
?>
var overridehistoryurl = false;   
$.ajaxSetup({
    data: {
        login_status:login_status,
        token:formtoken
    },
    cache: true
});
</script>
<script>
function importParentStyles() {
	var parentStyleSheets
        parentStyleSheets = parent.document.styleSheets;
	var cssString = "";
        for (var i = 0, count = parentStyleSheets.length; i < count; i++) {
		var cssrule;
		try{
			cssrule = parentStyleSheets[i].cssRules;
		}
		catch(e)
		{
			//throw e;
		}
		if (cssrule) {
			var cssRules = cssrule;
			for (var j = 0, countJ = cssRules.length; j < countJ; ++j)
			{
			    cssString += cssRules[j].cssText;
			}
		}
		else
		{
			cssString += parentStyleSheets[i].cssText;  // IE8 and earlier
		}
	}
	var style = document.createElement("style");
        style.type = "text/css";
        try {
	    style.innerHTML = cssString;
            document.getElementsByTagName("head")[0].appendChild(style);
        }
        catch (ex) {
	    style.styleSheet.cssText = cssString;  // IE8 and earlier
        }
}
document.addEventListener('DOMContentLoaded',function(){

    importParentStyles();
    parent.iframeLoaded();
    var links = document.getElementsByTagName('a');
    var len = links.length;
    for(var i=0; i<len; i++)
    {
       links[i].target = "_parent";
    }
});
$(document).ready(function(){
    if(typeof close_window != 'undefined' && close_window)
    setTimeout(function(){ parent.closemyModal(); }, 2000);
    setTimeout(function(){ parent.iframeLoaded(); }, 1000);
    //onBodyLoaded = parent.onBodyLoaded;
    //onBodyLoaded();
    $("body").on("click change focus resize",function(){
        parent.iframeLoaded();
    });
});
</script>
</head>
<body style="padding-top:inherit;background:none;">
<div id="container" class="no_min_height internalIframe"> 
<?php include(MODULE_DIR.'/login/login.php');?>
<?php
foreach($rows as $rowName=>$rowDetails)
{
	echo '<section class="hrows '.$rowDetails['sectionParams']['class'].'" '.$rowDetails['sectionParams']['attrs'].'>'."\r\n";
	echo $rowDetails['rowWrapper'][0]."\r\n\t";
        echo '<div class="row '.$rowDetails['rowParams']['class'].'" '.$rowDetails['rowParams']['attrs'].'>'."\r\n\t";
        foreach($rowDetails['columns'] as $columnsIndex=>$columnDetails)
        {
            //$columnDetails['columnParams']['class'] = ($columnDetails['columnParams']['class'])?$columnDetails['columnParams']['class']:'col-md-12';
            //$columnDetails['columnParams']['class'] = $columnDetails['columnParams']['class'] . ' no-padding';
            echo '<div class="col '.$columnDetails['columnParams']['class'].'" '.$columnDetails['columnParams']['attrs'].'>';
            foreach($columnDetails['modules'] as $module=>$moduleParams)
            {
                echo '<div class="module '.$moduleParams['name'].' '.$moduleParams['moduleIdentifier'].' '.$moduleParams['class'].'" '.$moduleParams['attrs'].'>'."\r\n\t";
                echo $modules_html[$moduleParams['moduleIdentifier']]."\r\n\t";
                echo '</div>'."\r\n\t";
            }
            echo '</div>';
        }
        echo '</div>'."\r\n\t";
        echo $rowDetails['rowWrapper'][1]."\r\n\t";
        echo '</section>'."\r\n\t";
}
?>
</div>
<?php
if(MINIFY)
{
	merge_js();
	echo "<script src='".ROOT_PATH.'js/all.js?'.filemtime('js/all.js')."' type='text/javascript'></script>\r\n";
}
else
{
	foreach($PSJsincludes['core'] as $key=>$file){
		$PSJsincludes['core'][$key] = $file.'?'.filemtime(ROOT_DIR.DIRECTORY_SEPARATOR. str_replace('/',DIRECTORY_SEPARATOR,$file));
	};
	if($PSJsincludes["internal"])
	$PSJsincludes["internal"] = array_merge($PSJsincludes["core"],$PSJsincludes["internal"]); 
	else
	$PSJsincludes["internal"] = $PSJsincludes["core"];
	foreach($PSJsincludes['internal'] as $jsinclude){
	    echo "<script src='".ROOT_PATH.$jsinclude."' type='text/javascript'></script>\r\n";
	};
}
if($PSJsincludes["external"] && $PSJsincludes["external2"])
$PSJsincludes["external"] = array_merge($PSJsincludes["external"],$PSJsincludes["external2"]);
else
{
    if($PSJsincludes["external2"])
    $PSJsincludes["external"] = $PSJsincludes["external2"];
    else
    $PSJsincludes["external"] = $PSJsincludes["external"];
}
if($PSCssincludes["external"] && $PSCssincludes["external2"])
$PSCssincludes["external"] = array_merge($PSCssincludes["external"],$PSCssincludes["external2"]);
else
{
    if($PSCssincludes["external2"])
    $PSCssincludes["external"] = $PSCssincludes["external2"];
    else
    $PSCssincludes["external"] = $PSCssincludes["external"];
}
?>
<script>
var $jsLoaded = {};
$(document).ready(function(){
	<?php
	if(MINIFY)
	{
		foreach($PSJsincludes["core"] as $file)
		{
		?>
		jsfileQueueAdd('<?=$file?>',1);
		<?php
		}
	}
	?>
	var extscript = <?=json_encode($PSJsincludes["external"])?>;
        var intscript = <?=json_encode($PSJsincludes["internal"])?>;
        $.each(intscript,function(i,v){
		jsfileQueueAdd(v,1);
		calljscallback(v);
	});
        if(extscript)
        {
	$.each(extscript,function(i,v){
		jsfileQueueAdd(v,0);
		$.getScript( v, function(e) {
            		calljscallback(v);
		});
	});
        }
	var extcss = <?=json_encode($PSCssincludes["external"])?>;
	<?php
	if(!(MINIFY))
	{
		?>
		$('<link/>', {   rel: 'stylesheet',   type: 'text/css',   href: 'css/bootstrap-theme.min.css?<?=filemtime('css/bootstrap-theme.min.css')?>'}).appendTo('head');
		$('<link/>', {   rel: 'stylesheet',   type: 'text/css',   href: 'css/style_extra.css?<?=filemtime('css/style_extra.css')?>'}).appendTo('head');
		<?php
	}
	?>
        if(extcss)
        {
	$.each(extcss,function(i,v){
		loadCss(v);
	});
        }
	//loadCss('https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Lancelot');
});
</script>
<?php
/*foreach($PSCssincludes as $type=>$files){
    foreach($files as $cssinclude){
        echo "<link rel='stylesheet' type='text/css' media='screen' href='".$cssinclude."' />\r\n";
    }
};*/
?>
<!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</body>
</html>