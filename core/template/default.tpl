<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head>
<base href="<?=ROOT_PATH?>">
<?php 
generate_meta();
if(MINIFY)
{
	merge_css();
	echo '<link rel="stylesheet" type="text/css" media="screen" href="'.ROOT_PATH.'css/all.css?'.filemtime('css/all.css').'" />';
}
else
{
	echo '<link rel="stylesheet" type="text/css" href="'.ROOT_PATH.'css/bootstrap.min.css?'.filemtime('css/bootstrap.min.css').'" />';
	echo '<link rel="stylesheet" type="text/css" href="'.ROOT_PATH.'css/style.css?'.filemtime('css/style.css').'" />';
}
?>
<style>
</style>

<script>document.write('<script src="<?=ROOT_PATH?>js/jquery-1.11.3.min.js" type="text/javascript"  language="JavaScript"></'+'script>')</script>
<script>
<?php
    foreach($PSJavascript as $key=>$value)
    {
        echo "\tvar ".$key." = ".json_encode($value).";\r\n";
    }
?>

$.ajaxSetup({
    data: {
        login_status:login_status,
        token:formtoken
    },
    cache: true
});
$( document ).ajaxComplete(function( event, xhr, settings ) {
    if(settings.url.search("PSAjax") > -1)
    {
		var result = xhr.responseText;
		$res = JSON.parse(result);
		if($res.status == "405")
		loginModal('login-box');
		else if($res.status == "4051")
		loginModalMinimal('login-box');
    }
	
});
</script>
</head>
<body class="<?=$_GET['pagetype']?>">
<div id="container"> 
<?php include(MODULE_DIR.'/login/login.php');?>
<?php if($page_includes['header']) include(MODULE_DIR.'/header/header.php');?>
<?php if($page_includes['schemadefault']) include(MODULE_DIR.'/schemadefault/schemadefault.php');?>
<?php
foreach($rows as $rowName=>$rowDetails)
{
	echo '<section class="hrows '.$rowDetails['sectionParams']['class'].'" '.$rowDetails['sectionParams']['attrs'].'>'."\r\n";
	echo $rowDetails['rowWrapper'][0]."\r\n\t";
	//if($rowDetails['rowParams']['class'])
	if (strpos($rowDetails['sectionParams']['class'], 'container-fluid') === false)
        echo '<div class="row '.$rowDetails['rowParams']['class'].'" '.$rowDetails['rowParams']['attrs'].'>'."\r\n\t";
        foreach($rowDetails['columns'] as $columnsIndex=>$columnDetails)
        {
            $columnDetails['columnParams']['class'] = ($columnDetails['columnParams']['class'])?$columnDetails['columnParams']['class']:'col-md-12';
            if(strpos($rowDetails['sectionParams']['class'], 'container-fluid') !== false)
            $columnDetails['columnParams']['class'] = $columnDetails['columnParams']['class'] . ' no-padding';
            if($columnDetails['columnParams']['class'])
            echo '<div class="col '.$columnDetails['columnParams']['class'].'" '.$columnDetails['columnParams']['attrs'].'>';
            foreach($columnDetails['modules'] as $module=>$moduleParams)
            {
                echo '<div class="module '.$moduleParams['name'].' '.$moduleParams['moduleIdentifier'].' '.$moduleParams['class'].'" '.$moduleParams['attrs'].'>'."\r\n\t";
                echo $modules_html[$moduleParams['moduleIdentifier']]."\r\n\t";
                echo '</div>'."\r\n\t";
            }
            if($columnDetails['columnParams']['class'])
            echo '</div>';
        }
        //if($rowDetails['rowParams']['class'])
	if (strpos($rowDetails['sectionParams']['class'], 'container-fluid') === false)
        echo '</div>'."\r\n\t";
        echo $rowDetails['rowWrapper'][1]."\r\n\t";
        echo '</section>'."\r\n\t";
}
?>
<?php if($page_includes['sitemap']) include(MODULE_DIR.'/sitemap/sitemap.php');?>
</div>
<?php if($page_includes['footer']) include(MODULE_DIR.'/footer/footer.php');?>
<?php include(MODULE_DIR.'/aboutpage/aboutpage.php');?>
<!--<div class="jbox-container">
    <div class="img-alt-text"></div>
    <img src="" />
    <i id="prev" class="fa fa-angle-left"></i>
    <i id="next" class="fa fa-angle-right"></i>
    <i id="close" class="fa fa-times"></i>
</div>-->
<div role="dialog" tabindex="-1" class="modal fade" id="modal-box">
    <?php
    ob_start();
    ?>
    <div class="modal-dialog popup1">
        
        <h2>LOGIN/SIGN UP</h2>
        <div class="modal-container">
        <div class="row two-clm">
            <div class="col-md-6 col-sm-6">
					<div class="row form-group hide" id="toggleloginoptions">
						<div class="radio col-xs-6">
							<label  ><input type="radio" name="user" checked onchange="$('.lgnfrm-clm-1').toggleClass('hide');" /> Existing user</label>
						</div>
						<div class="radio col-xs-6">
							<label  ><input type="radio" name="user" onchange="$('.lgnfrm-clm-1').toggleClass('hide');"  /> New user</label>
						</div>
					</div>	
		<div class="lgnfrm-clm-1" style="margin-bottom:20px;">
					
                    <div class="login_form" style="display:block;">
                    					
                    <div class="row">
                        <span style="display:none" class="error-text">
                            Invalid userID or Password
                        </span>
                        <div class="col-md-12">
                            <div class="form-group">
                                <i class="fa fa-user"></i>
                                <input type="email" name="email" id="loginemail" required placeholder="Email " class="form-control">
                              </div>
                        </div>
                      </div>
                      <div class="row">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <i class="fa fa-lock"></i>
                                <input type="password" name="password" id="loginpassword" required placeholder="Password" class="form-control">
                              </div>
                        </div>
                      </div> 

                      <div class="row">
                        <div class="col-md-12 ">
                            <input type="button" id="login_btn" class="btn btn-primary" value="Login" />
                        </div>
                      </div>
                      </div>
                    
                      <div class="forgot_form" style="display:none">
                      <div class="row">
                        <span style="display:none" class="error-text">
                            Invalid userID or Password
                        </span>
                        <div class="col-md-12">
                            <div class="form-group">
                                <i class="fa fa-user"></i>
                                <input type="email" id="frgt_email" required placeholder="Email" class="form-control">
                              </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 ">
                            <input type="button" id="send_pswd_btn" class="btn btn-primary" value="Send  Verification Link" />
                        </div>
                      </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                              <a class="forgot" id="forgot_password_btn" alternatetext="Back To Login Form" href="#">Forgot Password?</a>
                        </div>
                      </div>
                      
                      <div class="row ">
                        <div class="col-md-12 new">
                              New User? <a href="register">Sign UP Now</a>
                        </div>
			</div>              
                </div>
		<div class="lgnfrm-clm-1 hide" id="reg_min"  style="margin-bottom:20px;">
                
		<div class="login_form" style="display:block;margin-bottom:20px">
                    
					
                    <div class="row">
                        <span style="display:none" class="error-text">
                            
                        </span>  
                        <div class="col-md-12">
                            <div class="form-group">
                                <i class="fa fa-user"></i>
                                <input type="text" name="name" id="minregistername" required placeholder="Full Name" class="form-control">
                            </div>
                        </div>
			
			<div class="col-md-12">
                            <div class="form-group">
                                <i class="fa fa-envelope"></i>
                                <input type="email" name="email" id="minregisteremail" required placeholder="Email " class="form-control">
								<input type="hidden" name="dob" id="minregisterdob" required placeholder="DOB " class="form-control">
								<input type="hidden" name="gender" id="minregistergender" required placeholder="Gender " class="form-control">
								<input type="hidden" name="passions" id="minregisterpassions" required placeholder="Passions " class="form-control">
                              </div>
                        </div>
                      </div>
                      <div class="row">  
                        <div class="col-md-12">
                            <div class="form-group">
                                <i class="fa fa-lock"></i>
                                <input type="password" name="password" id="minregisterpassword" required placeholder="Create Password" class="form-control">
                              </div>
                        </div>
                      </div>
			
			

                      <div class="row">
                        <div class="col-md-12 ">
                            <input type="button" id="min_register_btn" class="btn btn-primary" value="Register" />
                        </div>
                      </div>
                      </div>
                    
                        
                </div>
            </div>
            <div class="col-md-6 col-sm-6 pull-right">
                
                <div class="social">
                    <a href='module/loginsns' class="callmodaliframe1 createloginpopup" data-targetsrc="module/loginsns/fb" type="fb" type1="facebook"><span class="fb"><i class="fa fa-facebook"></i> Login with Facebook</span></a>
                    <a href='module/loginsns' class="callmodaliframe1 createloginpopup" data-targetsrc="module/loginsns/tw" type="tw" type1="twitter"><span class="twtr"><i class="fa fa-twitter"></i> Login with Twitter</span></a>
                    <a href='module/loginsns' class="callmodaliframe1 createloginpopup" data-targetsrc="module/loginsns/gg" type="gg" type1="google"><span class="gplus"><i class="fa fa-google-plus"></i> Login with Google</span></a>
                </div>
            </div>
        </div>
        </div>
    </div>
    <?php
    $loginform = ob_get_contents();
    ob_end_clean();
    echo $loginform;
    ?></div>
<?php
echo '<script> var loginform = '.json_encode($loginform, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE).';</script>';
if(MINIFY)
{
	merge_js();
	echo "<script src='".ROOT_PATH.'js/all.js?'.filemtime('js/all.js')."' type='text/javascript'></script>\r\n";
}
else
{
	foreach($PSJsincludes['core'] as $key=>$file){
		$PSJsincludes['core'][$key] = $file.'?'.filemtime(ROOT_DIR.DIRECTORY_SEPARATOR. str_replace('/',DIRECTORY_SEPARATOR,$file));
	};
	if($PSJsincludes["internal"])
	$PSJsincludes["internal"] = array_merge($PSJsincludes["core"],$PSJsincludes["internal"]); 
	else
	$PSJsincludes["internal"] = $PSJsincludes["core"];
	foreach($PSJsincludes['internal'] as $jsinclude){
	    echo "<script src='".ROOT_PATH.$jsinclude."' type='text/javascript'></script>\r\n";
	};
}
?>
<script>
var $jsLoaded = {};
$(document).ready(function(){
	var extcss = <?=json_encode($PSCssincludes["external"])?>;
	<?php
	if(!(MINIFY))
	{
		?>
		$('<link/>', {   rel: 'stylesheet',   type: 'text/css',   href: 'css/bootstrap-theme.min.css?<?=filemtime('css/bootstrap-theme.min.css')?>'}).appendTo('head');
		$('<link/>', {   rel: 'stylesheet',   type: 'text/css',   href: 'css/style_extra.css?<?=filemtime('css/style_extra.css')?>'}).appendTo('head');
		<?php
	}
	?>
	$.each(extcss,function(i,v){
		loadCss(v);
	});
	<?php
	if(MINIFY)
	{
		foreach($PSJsincludes["core"] as $file)
		{
		?>
		jsfileQueueAdd('<?=$file?>',1);
		<?php
		}
	}
	?>
	var extscript = <?=json_encode($PSJsincludes["external"])?>;
        var intscript = <?=json_encode($PSJsincludes["internal"])?>;
        $.each(intscript,function(i,v){
		jsfileQueueAdd(v,1);
		calljscallback(v);
	});
	$.each(extscript,function(i,v){
		jsfileQueueAdd(v,0);
		$.getScript( v, function(e) {
            		calljscallback(v);
		});
	});
	//loadCss('https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Lancelot');
});
</script>
<?php
/*foreach($PSCssincludes as $type=>$files){
    foreach($files as $cssinclude){
        echo "<link rel='stylesheet' type='text/css' media='screen' href='".$cssinclude."' />\r\n";
    }
};*/
?>
<!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</body>
</html>