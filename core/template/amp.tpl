<!DOCTYPE html>
<html amp>
<head>
<?php 
generate_meta();
?>
<?php include(MODULE_DIR.'/header/header_amp_css.php');?>
</head>
<body>
<?php if($page_includes['header']) include(MODULE_DIR.'/header/header_amp.php');?>
<?php if($page_includes['schemadefault']) include(MODULE_DIR.'/schemadefault/schemadefault.php');?>
<?php
foreach($rows as $rowName=>$rowDetails)
{
	echo '<section class="hrows '.$rowDetails['sectionParams']['class'].'" '.$rowDetails['sectionParams']['attrs'].'>'."\r\n";
	echo $rowDetails['rowWrapper'][0]."\r\n\t";
	//if($rowDetails['rowParams']['class'])
	if (strpos($rowDetails['sectionParams']['class'], 'container-fluid') === false)
        echo '<div class="row '.$rowDetails['rowParams']['class'].'" '.$rowDetails['rowParams']['attrs'].'>'."\r\n\t";
        foreach($rowDetails['columns'] as $columnsIndex=>$columnDetails)
        {
            $columnDetails['columnParams']['class'] = ($columnDetails['columnParams']['class'])?$columnDetails['columnParams']['class']:'col-md-12';
            if(strpos($rowDetails['sectionParams']['class'], 'container-fluid') !== false)
            $columnDetails['columnParams']['class'] = $columnDetails['columnParams']['class'] . ' no-padding';
            if($columnDetails['columnParams']['class'])
            echo '<div class="col '.$columnDetails['columnParams']['class'].'" '.$columnDetails['columnParams']['attrs'].'>';
            foreach($columnDetails['modules'] as $module=>$moduleParams)
            {
                echo '<div class="module '.$moduleParams['name'].' '.$moduleParams['moduleIdentifier'].' '.$moduleParams['class'].'" '.$moduleParams['attrs'].'>'."\r\n\t";
                echo $modules_html[$moduleParams['moduleIdentifier']]."\r\n\t";
                echo '</div>'."\r\n\t";
            }
            if($columnDetails['columnParams']['class'])
            echo '</div>';
        }
        //if($rowDetails['rowParams']['class'])
	if (strpos($rowDetails['sectionParams']['class'], 'container-fluid') === false)
        echo '</div>'."\r\n\t";
        echo $rowDetails['rowWrapper'][1]."\r\n\t";
        echo '</section>'."\r\n\t";
}
?>
<?php if($page_includes['sitemap']) include(MODULE_DIR.'/sitemap/sitemap.php');?>
<?php if($page_includes['footer']) include(MODULE_DIR.'/footer/footer.php');?>
<?php include(MODULE_DIR.'/aboutpage/aboutpage.php');?>
</body>
</html>