<?php
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    //return preg_match("/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i", $_SERVER["HTTP_USER_AGENT"]);
}
function encrypt($pure_string, $encryption_key = ENCRYPTION_KEY) {
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
    return $encrypted_string;
}
function encryptv2($input = array()) {
    if($input)
    {
    if($input[count($input) - 1] != ENCRYPTION_KEY)
    $input[] = ENCRYPTION_KEY;
    return strtolower(hash('sha512', implode("|",$input)));
    }
    else
    return false;
}
function decrypt($encrypted_string, $encryption_key) {
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
    return $decrypted_string;
}
function createsignedrequest($payload,$secret = ENCRYPTION_KEY){
    $payload = base64_url_encode(json_encode($payload));
    return base64_url_encode(hash_hmac('sha256', $payload, $secret, $raw = true)).'.'.$payload;
}
function decodesignedrequest($signed_request,$secret = ENCRYPTION_KEY){
    list($encoded_sig, $payload) = explode('.', $signed_request, 2);
    $sig = base64_url_decode($encoded_sig);
    $data = json_decode(base64_url_decode($payload), true);
    if(hash_hmac('sha256', $payload, $secret, $raw = true) == $sig)
    {
        return $data;
    }
    return false;
}

function authorized(){
    global $userauth_current;
    return true;
    @session_start();
    $userauth_previous= (string)decrypt($_SESSION['ui'],ENCRYPTION_KEY);
    if($userauth_current == $userauth_previous)
        return true;
    else
        return false;
}
function base64_url_encode($input) {
    return strtr(base64_encode($input), '+/=', '-_,');
}
function base64_url_decode($input) {
    return base64_decode(strtr($input, '-_,', '+/='));
}
function loggedId(){
    @session_start();
    if($_SESSION['user']['profile_id'])
        return true;
    else
        return false;
}
function render_module($module,$moduledata = array(),$render = true,$moduleRequestParams = array()){
    global $module_defaults,$row_defaults,$rows,$modules,$modules_html,$layout,$connection,$PSParams,$page_includes,$PSModData,$PSData,$PSJavascript,$PSJsincludes,$PSCssincludes;
    
    $moduledir = '';
    
    $moduleD = explode('/',$module); //to handle admin/module condition
    $module = array_pop($moduleD);
    if($moduleD)
    {
    $moduledir = implode('/',$moduleD);
    $moduledir = $moduledir.'/';
    }
    if(isset($_GET['moduletype']))
    $moduledir = $_GET['moduletype'].'/';
    
    $module_identifier = $module.'_'.rand(1000,9999);
            
    //$modules = $rowmodules[$module_defaults];
    $param = $moduledata['modulesParams'];
    $param = array_merge($module_defaults,$param);
    
    $moduleparams=$param;
    $moduleparams['name']=$module; 
    $moduleparams['moduleIdentifier']= $module_identifier;

    foreach($param as $key=>$value)
    $$key = $value;
    ob_start();
    
    include(MODULE_DIR.'/'.$moduledir.$module.'/'.$module.'.php');
    $file = MODULE_DIR.'/'.$moduledir.$module.'/'.$module.'.php';
    
    /*if(!$module_display_style)
    $module_display_style = $modules[$module]['module_display_style'];*/
    if(empty($module_display_style) && !empty($moduleparams['module_display_style']))
    $module_display_style = $moduleparams['module_display_style'];
    else
    $module_display_style = $module_display_style;
    
    if($module_display_style)
    $tpl = str_replace('.php','_'.$module_display_style.'.tpl',$file);
    else
    $tpl = str_replace('.php','.tpl',$file);
    unset($module_display_style);
    unset($file);

    render_tpl($tpl,$module);

    unset($tpl);

    $module_data_html = ob_get_contents();
    ob_end_clean();
    
    foreach($param as $key=>$value)
    unset($$key);
    if($render)
    {
    echo '<section class="hrows section"><div class="row"><div class="col-md-12">';
    echo '<div class="module '.$moduleparams['name'].' '.$moduleparams['moduleIdentifier'].'">';
    echo $module_data_html;
    echo '</div>';
    echo '</div></div></section>';
    }
    else
    {
    $return = array();
    $return['html'] = $module_data_html;
    $return['params'] = $moduleparams;
    $return['module_identifier'] = $module_identifier;
    
    return $return;
    }
}
function get_modules_layout_wise()
{
    global $module_defaults,$modules,$modules_html,$layout,$connection,$PSParams,$page_includes;
    $layout = array();
    foreach($modules as $module_name=>$module)
    {
        $layout_key = (@$module['module_layout_row'])?($module['module_layout'] .' '. $module['module_layout_row']):$module['module_layout'];
        $layout[$layout_key][] = array($module['module_identifier'],$module_name);
    }
}
function page_config(){
    global $page_meta_defaults,$page_meta,$page_includes_defaults,$page_includes,$PSParams;
    if(!$page_meta)
    $page_meta = array();
    $page_meta = array_merge($page_meta_defaults,$page_meta);
    
    if(!$page_includes)
    $page_includes = array();
    $page_includes = array_merge($page_includes_defaults,$page_includes);
    $PSParams['canonical'] = $page_meta['url'];
}
function render_tpl($tpl,$moduleName)
{
    global $PSModData,$PSParams,$PSData,$PSJavascript,$PSJsincludes,$PSCssincludes;
    foreach($PSModData as $PSModDatakey => $PSModDatavalue)
    {
        $$PSModDatakey = $PSModDatavalue;
    }
    include($tpl);
    foreach($PSModData as $PSModDatakey => $PSModDatavalue)
    {
        unset($$PSModDatakey);
    }
}
function render_modules()
{
    global $module_defaults,$row_defaults,$rows,$modules,$modules_html,$layout,$connection,$PSParams,$page_includes,$PSModData,$PSData,$PSJavascript,$PSJsincludes,$PSCssincludes;
    initialise();
        foreach($rows as $rowname=>$rowdetail)
        {
            if(empty($rowdetail['columnStructure']))
            {
                $rows[$rowname]['columnStructure'] = true;
                $rows[$rowname]['columns'][0] = array('columnParams'=>array(
                        'class'=>''
                     ),'modules'=>$rowdetail['modules']);
                unset($rows[$rowname]['modules']);
            }
        }
        //print_array($rows);
        foreach($rows as $rowname=>$rowdetail)
        {
            $rowparam = $rowdetail;
            $rowparam = array_merge($row_defaults,$rowparam);
            $rows[$rowname] = $rowparam;
            
            foreach($rowdetail['columns'] as $columnIndex=>$columnDetails)
            {
                $rowmodules = $columnDetails['modules'];
                //print_array($rowparam);
                
                foreach($rowmodules as $module=>$moduledata)
                {
                        $data = render_module($module,$moduledata,false);
            unset($rows[$rowname]['columns'][$columnIndex]['modules'][$module]);
            $modules_html[$data['module_identifier']] = $data['html'];
                        $rows[$rowname]['columns'][$columnIndex]['modules'][$data['module_identifier']]=$data['params'];

        }
            }
        }
    
    $GLOBALS['PSJsincludes']['external'] = array_unique($GLOBALS['PSJsincludes']['external']);
        $GLOBALS['PSJsincludes']['internal'] = array_unique($GLOBALS['PSJsincludes']['internal']);
        $GLOBALS['PSCssincludes']['external'] = array_unique($GLOBALS['PSCssincludes']['external']);
        $GLOBALS['PSCssincludes']['internal'] = array_unique($GLOBALS['PSCssincludes']['internal']);
        
        array_walk_recursive($GLOBALS['PSJsincludes']['external'],function($value,$key){$GLOBALS['PSJsincludes']['external'][$key] = ($temp = filemtime($value))?$value."?".  $temp:$value;});
        array_walk_recursive($GLOBALS['PSJsincludes']['internal'],function($value,$key){$GLOBALS['PSJsincludes']['internal'][$key] = ($temp = filemtime($value))?$value."?".  $temp:$value;});
        array_walk_recursive($GLOBALS['PSCssincludes']['external'],function($value,$key){$GLOBALS['PSCssincludes']['external'][$key] = ($temp = filemtime($value))?$value."?".  $temp:$value;});
        array_walk_recursive($GLOBALS['PSCssincludes']['internal'],function($value,$key){$GLOBALS['PSCssincludes']['internal'][$key] = ($temp = filemtime($value))?$value."?".  $temp:$value;});
        
        $PSJavascript['formtoken'] = strtolower(hash('sha512', implode("|",getallheaders())."|page|".CSRF_KEY));
        $PSJavascript['relurl'] = rtrim(str_replace(ROOT_PATH, '', $PSJavascript['orgurl']),'/').'/';
    
        $template = $page_includes['layout'];
        ob_start();
    require_once(CORE_DIR.'/template/'.$template.'.tpl');
        $page_html = ob_get_contents();
        ob_end_clean();
        if($_GET['layout'] == 'amp')
        {
            $page_html = preg_replace('#<script>(.*?)</script>#is', '', $page_html);
            $page_html = preg_replace('#<form(.*?)</form>#is', '', $page_html);
            $page_html = preg_replace('/(<[^>]*) style=("[^"]+"|\'[^\']+\')([^>]*>)/i', '$1$3', $page_html);
            preg_match_all('/<img[^>]+>/i',$page_html, $result);
            $imgObj = array();
            foreach( $result[0] as $img_tag)
            {
                preg_match_all('/(alt|title|src|class|data-src)=("[^"]*")/i',$img_tag, $imgObj[$img_tag]);
            }
            foreach($imgObj as $imgsrc=>$img){
                $attributes = array_flip($img[1]);
                if(isset($attributes['data-src']))
                $newsrc = $img[2][$attributes['data-src']];
                else if(isset($attributes['src']))
                $newsrc = $img[2][$attributes['src']];
                else
                continue;
                $newimg = '<amp-img class="fullwidth_img" src='.$newsrc.' layout="responsive" width="700" height="446"></amp-img>';
                $page_html = str_replace($imgsrc,$newimg,$page_html);
            }
        }
        finalise();
        return minify_html($page_html);
}
function initialise(){
    //if(!ob_start("ob_gzhandler")) ob_start();
    global $PS_DEBUG,$overridecache;
    if(array_key_exists('debug', $_GET) && isAdminLoggedIn())
    {
        $overridecache = true;
        if(array_key_exists('error', $_GET)){
        error_reporting(E_ALL);
        }
    }
    else
    $overridecache = false;
    $PS_DEBUG['starttime'] = microtime(true);
    page_config();
}
function finalise(){
    global $userauth_current,$userauth_previous,$PS_DEBUG;
    if(isset($_SESSION['fbtaballow']) && $_SESSION['fbtaballow'] ==1)
    {
    header("X-Frame-Options: ALLOWALL");
    }
    else
    {
    header("X-Frame-Options: SAMEORIGIN");
    }
    $_SESSION['ui'] = encrypt($userauth_current,ENCRYPTION_KEY);
    write_complete_log();
    $PS_DEBUG['endtime'] = microtime(true);
    $PS_DEBUG['totaltimeexecution'] = ($PS_DEBUG['endtime'] - $PS_DEBUG['starttime']);
    if(array_key_exists('debug', $_GET) && isAdminLoggedIn())
    print_array($PS_DEBUG);
    //$txt = $_SERVER['SCRIPT_NAME'].','.$PS_DEBUG['starttime'].','.$PS_DEBUG['endtime'].','.$PS_DEBUG['totaltimeexecution'];
    //$myfile = file_put_contents('logs/executiontimewithcaching.csv', $txt.PHP_EOL , FILE_APPEND);
}
function generate_meta(){
    global $page_meta,$PSJavascript,$PSParams;
        if($page_meta['allowfetchfromdb'])
    {
    $temp = pagemeta_get_details(array('pageType'=>$_GET['pagetype'],'pageId'=>$_GET['pageId']));
    if($temp)
    {
        $temp = unserialize($temp['meta']);
        $page_meta['title'] = ($temp['title'])?$temp['title']:$page_meta['title'];
        $page_meta['description'] = ($temp['description'])?$temp['description']:$page_meta['description'];
        $page_meta['keywords'] = ($temp['keywords'])?$temp['keywords']:$page_meta['keywords'];
    }
    }
        
        $page_meta['title'] = htmlentities(strip_tags($page_meta['title']),ENT_QUOTES);
    $page_meta['description'] = str_replace('<br>',' \n',$page_meta['description']);
        $page_meta['description'] = str_replace('<br />',' \n',$page_meta['description']);
        $page_meta['description'] = str_replace('</br>',' \n',$page_meta['description']);
        $page_meta['description'] = htmlentities(strip_tags($page_meta['description']),ENT_QUOTES);
    
        if(!is_array($page_meta['image']))
        {
            $temp =$page_meta['image'];
            $page_meta['image'] = array();
            $page_meta['image'][0] = $temp;
        }
        
        $PSParams['page_meta'] = $page_meta;
        if(stripos($page_meta['title'],'passionstreet') === false)
    $page_meta['title'] = chop_string($page_meta['title'],64).' - PASSIONSTREET';
        else
        $page_meta['title'] = chop_string($page_meta['title'],80);
        // $page_meta['description'] = chop_string($page_meta['description'],160);
        $page_meta['description'] = chop_string($page_meta['description'],250);
        
        $PSJavascript['meta'] = $page_meta;
    require_once(CORE_DIR.'/meta/meta.tpl');
}
function generate_standard_date($date,$fullformat=1){
    $date = str_replace('/', '-', $date);
    if(empty($fullformat))
    return date('Y-m-d', strtotime($date));
    else
    return date('Y-m-d H:i:s', strtotime($date));
}
function generate_standard_time($time){
    $timecomponents = explode(' ',$time);
    $timeparts = explode(':',$timecomponents[0]);
    if(strtolower($time[1]) == 'am')
    {
        $timehh = sprintf("%02d", $timeparts[0]);
    }
    else
    {
        $timehh = sprintf("%02d", ((int)$timeparts[0] + 12));
    }
    $timemm = sprintf("%02d",$timeparts[1]);
    $timess = sprintf("%02d",$timeparts[2]);
    return $timehh.':'.$timemm.':'.$timess;
}
function print_array($array)
{
    if((SERVERTYPE == 'live' && array_key_exists('debug', $_GET) && isAdminLoggedIn()) || (executiontype == 'cron') || SERVERTYPE != 'live')
    {
    echo '<pre>';
    print_r($array);
    echo '</pre>';
    }
    else
    return false;
}
function convert_string_numeric($string){
    for($i=0;$i<strlen($string);$i++)
    $numeric[$i] = ord($string[$i]);
    return implode("",$numeric);
}
function generate_id($pre){
    return $pre."-".(time()+rand(1000000000,9999999999));
}
function load_session($user_id=''){
    global $PSData,$PSJavascript;
    @session_start();
    if($PSData['user']['profile_id'])
    {
        $_SESSION['user'] = $PSData['user'];
        $PSJavascript['ps_uid'] = $PSData['user']['id'];
        $PSJavascript['profile_id'] = $PSData['user']['profile_id'];
        $PSJavascript['login_status'] = true;
        if(empty($PSData['user']['gender']) || empty($PSData['user']['dob']) || empty($PSData['user']['country']) || empty($PSData['user']['city']))
            $PSJavascript['profile_complete'] = false;
        else
            $PSJavascript['profile_complete'] = true;
        return true;
    }else{
    $PSJavascript['profile_id'] = '';
        $PSJavascript['login_status'] = false;
        return false;
    }
}
function console_log($log){
    global $PSPHPConsolelogs;
    if(is_array($log) || is_object($log) )
    $temp= array('PHP',(array)$log);
    else
    $temp= 'PHP :'.$log;
    $PSPHPConsolelogs[] = $temp ;
    write_log($temp);
}
function write_log($log){
    echo '<script>';
    if(is_array($log) || is_object($log) )
        echo 'console.log('.json_encode((array)$log).');';
    else
        echo 'console.log("'.$log.'");';
    echo '</script>';
}
function write_complete_log(){
    return ;
    global $PSPHPConsolelogs;
    echo '<script>';
    foreach($PSPHPConsolelogs as $log)
    {
        if(is_array($log))
            echo 'console.log('.json_encode($log).');';
        else
            echo 'console.log("'.$log.'");';
    }
    
    echo '</script>';
}
function get_alphanumeric($string){
    $string = strtolower($string);
    $string = preg_replace('/\s+/', '-',$string);
    $string = preg_replace('/-+/', '-',$string);
    return preg_replace("/[^A-Za-z0-9-]/", '', $string);
}
function upload_v1($targetPath,$datatype,$file){
    
    $relurl = false;
    $name     = $file['name'];
    $tmpName  = $file['tmp_name'];
    $error    = $file['error'];
    $size     = $file['size'];
    $ext      = strtolower(pathinfo($name, PATHINFO_EXTENSION));
    switch ($error) {
        case UPLOAD_ERR_OK:
            $valid = true;
            //validate file extensions
            if ($datatype=='image' && !in_array($ext, array('jpg','jpeg','png','gif')) ) {
                $valid = false;
                $response = 'Invalid file extension.';
            }
            //validate file size
            if ( $size/1024/1024 > 2 ) {
                //$valid = false;
                //$response = 'File size is exceeding maximum allowed size.';
            }
            //upload file
            if ($valid) {
                
                $relurl = upload_final($ext,$tmpName,$targetPath);
                if($relurl)
                {
                    $valid = true;
                    $response = "Upload Successfull";
                }
                else {
                    $valid = false;
                    $response = "Upload Failed Due to Unknown Error";
                }
            }
            break;
        case UPLOAD_ERR_INI_SIZE:
            $response = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
            break;
        case UPLOAD_ERR_FORM_SIZE:
            $response = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.';
            break;
        case UPLOAD_ERR_PARTIAL:
            $response = 'The uploaded file was only partially uploaded.';
            break;
        case UPLOAD_ERR_NO_FILE:
            $response = 'No file was uploaded.';
            break;
        case UPLOAD_ERR_NO_TMP_DIR:
            $response = 'Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.';
            break;
        case UPLOAD_ERR_CANT_WRITE:
            $response = 'Failed to write file to disk. Introduced in PHP 5.1.0.';
            break;
        case UPLOAD_ERR_EXTENSION:
            $response = 'File upload stopped by extension. Introduced in PHP 5.2.0.';
            break;
        default:
            $response = 'Unknown error';
        break;
    }

    $temp['valid'] = $valid;
    $temp['response'] = $response;
    $temp['relurl'] = $relurl;
    //print_array($temp);
    return $temp['relurl'];
}
function upload($targetPath,$datatype,$file,$generatethumb = true)
{
    global $PSData;
    $s = UPLOAD_DIR.DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR;
    $p = UPLOAD_DIR.DIRECTORY_SEPARATOR.$targetPath.DIRECTORY_SEPARATOR;
    $t = UPLOAD_DIR.DIRECTORY_SEPARATOR.'thumb'.DIRECTORY_SEPARATOR.$targetPath.DIRECTORY_SEPARATOR;
    // ESTABLISH THE LARGEST FILE WE WILL UPLOAD
    $max_file_size = '9000000'; // A BIT MORE THAN 8MB
        $temp['valid'] = true;
    // THIS IS A LIST OF THE POSSIBLE ERRORS THAT CAN BE REPORTED in $_FILES[]["error"]
    $errors = array
    ( 0 => "Success!"
    , 1 => "The uploaded file exceeds the upload_max_filesize directive in php.ini"
    , 2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form"
    , 3 => "The uploaded file was only partially uploaded"
    , 4 => "No file was uploaded"
    , 5 => "UNDEFINED"
    , 6 => "Missing a temporary folder"
    , 7 => "Cannot write file to disk"
    )
    ;

    // IF THERE ARE ERRORS
    $error_code    = $file["error"];
    if ($error_code)
    {
        //die($errors[$error_code]);
        $temp['valid'] = false;
        $temp['response'] = $errors[$error_code];
    return $temp;
    }
    

    // SYNTHESIZE THE NEW FILE NAME FOR TEMPORARY STORAGE
    //$f = basename($file['name']);
    $ext      = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
    $f = generate_id($targetPath).'-'.str_replace('profile-','',str_replace('_','-',$PSData['user']['profile_id'])).'.'.$ext;
    
    $my_new  = $s . $f;
    $my_temp = $s . $f;

    // MOVE THE FILE INTO THE STORAGE DIRECTORY
    if (!move_uploaded_file($file['tmp_name'], $my_new))
    {
        //die("MOVE TO $my_new FAILED");
    $temp['valid'] = false;
        $temp['response'] = 'Upload Failed';
    return $temp;
    }
    // RESIZE THE PIC
    if ($image_resource = create_right_size_image(array('fileloc'=>$my_temp), 1000))
    {
        // SAVE THE PIC
        imagejpeg($image_resource, $p . $f, 75);
        imagedestroy($image_resource);
    }
    if($generatethumb)
    {
        // RESIZE THE THUMB
        if ($image_resource = create_right_size_image(array('fileloc'=>$my_temp), 400))
        {
        // SAVE THE THUMB
        imagejpeg($image_resource, $t . $f, 75);
        imagedestroy($image_resource);
        }
    }
    // OPTIONAL: DISPOSE OF THE UNWANTED ORIGINAL
    // unlink($my_new);
    if($image_resource)
    {
    $temp['valid'] = true;
    $temp['response'] = 'Image Upload Successful';
    $temp['relurl'] = $relurl = $targetPath.'/'.$f;
    //print_array($temp);
    return $temp['relurl'];
    }
}
function upload_final($ext, $tmpName , $targetPath)
{
    
    $name = $targetPath.'_'. get_alphanumeric(microtime()).'.'.$ext;
    $targetPathfinal =  UPLOAD_DIR . DIRECTORY_SEPARATOR . $targetPath . DIRECTORY_SEPARATOR . $name;
    if(move_uploaded_file($tmpName,$targetPathfinal))
    $relurl = $targetPath.'/'.$name;
    else
    $relurl = false;
    return $relurl;
}
function upload_from_uri($targetPath,$img){
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $fileData = base64_decode($img);
    //saving
    $ext = 'jpg';
    $name = $targetPath.'_'.get_alphanumeric(microtime()).'.'.$ext;
    $targetPathfinal =  UPLOAD_DIR . DIRECTORY_SEPARATOR . $targetPath . DIRECTORY_SEPARATOR . $name;
    file_put_contents($targetPathfinal, $fileData);
    return $targetPath.'/'.$name;
}
function upload_from_url($targetPath,$img,$crop = array()){
    
    if(!empty($crop))
    {
        $width = $crop['width'];
        $height = $crop['height'];
        $imgnew = ROOT_PATH."core/wideimage/demo/image.php?image=".urlencode($img)."&output=preset%20for%20demo&colors=255&dither=1&match_palette=1&demo=resize&width=".$width."&height=".$height."&fit=inside&scale=any";
        return upload_from_url($targetPath,$imgnew,array());
    }
    else
    {
        $fileData = file_get_contents($img);
        //saving
        $ext = 'jpg';
        $name = $targetPath.'_'.get_alphanumeric(microtime()).'.'.$ext;
        $targetPathfinal =  UPLOAD_DIR . DIRECTORY_SEPARATOR . $targetPath . DIRECTORY_SEPARATOR . $name;
        file_put_contents($targetPathfinal, $fileData);
        return $targetPath.'/'.$name;
    }
}
function upload_thumb($relimg,$size = array()){
    $width = $size['width'];
    $height = ($size['height'])?$size['height']:250;
    $img = get_upload_path($relimg);
    
    $imgnew = ROOT_PATH."core/wideimage/demo/image.php?image=".urlencode($img)."&output=preset%20for%20demo&colors=255&dither=1&match_palette=1&demo=resize&width=".$width."&height=".$height."&fit=inside&scale=any";
    //print_array($imgnew);
    $fileData = file_get_contents($imgnew);
    
    $targetPathfinal =  UPLOAD_DIR .  DIRECTORY_SEPARATOR .'thumb'.  DIRECTORY_SEPARATOR . $relimg;
    
    if(file_force_contents($targetPathfinal, $fileData))
    return 'thumb/'.$relimg;
    else
    return '';
}
function imageresizecrop($img,$targetPath, $exactmatch = false , $desiredimage = array('width'=>0,'height'=>0,'ratio'=>1),$requireThumb = 0,$dirpath = "",$filname=""){
    $file = $img;
    if(filter_var($img, FILTER_VALIDATE_URL) === FALSE)
    {
	$imgg = file_get_contents($file["tmp_name"]);
	$source = imagecreatefromstring( $imgg );
	$image_info = getimagesize($file["tmp_name"]);
	$image_width = $image_info[0];
	$image_height = $image_info[1];
    }
    else
    {
	$imgg = file_get_contents($img);
	$source = imagecreatefromstring($imgg);
	$image_width = imagesx($source);
	$image_height = imagesy($source);
	//print_array(array($image_width,$image_height));
    }
    //exit();
    $current_w_h = $image_width / $image_height;
    
    $current_allowed_ratio = (!empty($desiredimage['ratio'])?$desiredimage['ratio']:($desiredimage['width'] / $desiredimage['height']));
    
    
    if($image_width > $image_height * $current_allowed_ratio)
    {
        $height_change = 0;
        $newheight = $image_height - $height_change;
        $width_change = $image_width - ($image_height * $current_allowed_ratio);
        $newwidth = $image_width - $width_change;
    }
    else
    {
        $width_change = 0;
        $newwidth = $image_width - $width_change;
        $height_change = $image_height - ($image_width / $current_allowed_ratio);
        $newheight = $image_height - $height_change;
    }
    
    $maxWidth = 500;
    $maxHeight = $maxWidth / $current_allowed_ratio;
        
    if($exactmatch)
    {
        if($desiredimage['width'] && $desiredimage['height'])
        {
            $newheight = $desiredimage['height'];
            $newwidth = $desiredimage['width'];
        }
        else
        {
            $newwidth  = $maxWidth;
            $newheight = $maxWidth / $current_allowed_ratio;
        }
    }
    else if($newwidth > $maxWidth)
    {
        $newwidth = $maxWidth;
        $newheight = $maxWidth / $current_allowed_ratio ;
    }
    else if($newheight > $maxHeight)
    {
        $newheight = $maxHeight;
        $newwidth = $maxHeight * $current_allowed_ratio ;
    }
    
    $thumb = imagecreatetruecolor($newwidth, $newheight);
    imageinterlace($thumb, true);

    imagecopyresampled($thumb, $source, 0, 0, $width_change/2,$height_change/2, $newwidth, $newheight, $image_width - $width_change, $image_height - $height_change);
    ob_start();
    imagejpeg($thumb);
    $content = ob_get_contents();
    ob_end_clean();
    if(!empty($desiredimage['name']))
    $name = $desiredimage['name'];
    else
    $name = $targetPath.'_'. (!empty($filname)?$filname:get_alphanumeric(microtime())).'.jpg';
    $dirpath = (!empty($dirpath)?$dirpath:UPLOAD_DIR);
    $targetPathfinal =  $dirpath . DIRECTORY_SEPARATOR . $targetPath . DIRECTORY_SEPARATOR . $name;
    file_put_contents($targetPathfinal, $content);
    
    if($requireThumb)
    imageresizecrop($img,$targetPath, true , array('width'=>64,'height'=>64,'name'=>$name),0,'files'.DIRECTORY_SEPARATOR.'thumb');
    return $targetPath.'/'.$name;
}
function file_force_contents($dir, $contents){
        $parts = explode(DIRECTORY_SEPARATOR, $dir);
        $file = array_pop($parts);
        if(SERVERTYPE == 'local'){
            $dir = $parts[0];
            unset($parts[0]);
        }
        else
            $dir = '';
        foreach($parts as $part){
            if(!is_dir($dir .= DIRECTORY_SEPARATOR."$part")) mkdir($dir);
        }
    return file_put_contents("$dir".DIRECTORY_SEPARATOR."$file", $contents);
}
function reArrayFiles(&$file_post) {
    //$isAssociative = isAssocArray($file_post);
    $file_ary = array();
    //if(!$isAssociative)
    //{
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
        }
    //}
    //else
    //$file_ary[0] = $file_post;
    return $file_ary;
}
function get_upload_path($x,$path='')
{
    if($x)
    return UPLOAD_PATH.$x;
    else if($path)
    return ROOT_PATH. str_replace(ROOT_PATH,'',$path);
    else
    return false;    
    //return ROOT_PATH.'images/passionstreet.png';
}
function upload_post_images($infiles,$targetPath="posts")
{
    $files = reArrayFiles($infiles);
    //$image_ratios_w_h =array("img-wh-r-1"=>"0.25","img-wh-r-2"=>"0.5","img-wh-r-3"=>"0.75","img-wh-r-4"=>"1","img-wh-r-5"=>"1.25","img-wh-r-6"=>"1.5","img-wh-r-7"=>"1.75","img-wh-r-8"=>"2");
    $image_ratios_w_h =array("img-wh-r-1"=>"0.5","img-wh-r-2"=>"1","img-wh-r-3"=>"1.5","img-wh-r-4"=>"2");
    foreach ( $files as $key=>$file)
    {
        $temp = array();
        $image_info = getimagesize($file["tmp_name"]);
        $image_width = $image_info[0];
        $image_height = $image_info[1];
        $current_w_h = $image_width / $image_height;
        foreach ($image_ratios_w_h as $ratiokey=>$ratio)
        {
            if(($current_w_h - $ratio) > 0)
            $temp[$ratiokey] = $current_w_h - $ratio;
        }
        if(empty($temp))
        continue;
        asort($temp);
        reset($temp);
        $current_allowed_ratio = key($temp);
        $imgg = file_get_contents($file["tmp_name"]);
        $source = imagecreatefromstring( $imgg );
        if($image_ratios_w_h[$current_allowed_ratio]>=1)
        {
        $height_change = 0;
        $newheight = $image_height - $height_change;
        $width_change = $image_width - ($image_height * $image_ratios_w_h[$current_allowed_ratio]);
        $newwidth = $image_width - $width_change;
        }
        else
        {
        $height_change = 0;
        $newheight = $image_height - $height_change;
        $width_change = $image_width - ($image_height * $image_ratios_w_h[$current_allowed_ratio]);
        $newwidth = $image_width - $width_change;
        }
        if($newheight > 800)
        {
            $newwidth = $newwidth * (800/$newheight);
            $newheight = $newheight * (800/$newheight);
        }
        else if($newwidth > 1000)
        {
            $newheight = $newheight * (1000/$newwidth);
            $newwidth = $newwidth * (1000/$newwidth);
        }
        $thumb = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresized($thumb, $source, 0, 0, $width_change/2,$height_change/2, $newwidth, $newheight, $image_width - $width_change, $image_height - $height_change);
        ob_start();
        imagejpeg($thumb);
        $content = ob_get_contents();
        ob_end_clean();
        $name = $targetPath.'_'. get_alphanumeric(microtime()).'.jpg';
        $targetPathfinal =  UPLOAD_DIR . DIRECTORY_SEPARATOR . $targetPath . DIRECTORY_SEPARATOR . $name;
        file_put_contents($targetPathfinal, $content);
        $data[$current_allowed_ratio][] = $targetPath.'/'.$name;
    }
    return $data;
}
function upload_post_images_new2($infiles,$targetPath="posts",$isThumbRequired =  '')
{
    global $PSJavascript;
    $files = reArrayFiles($infiles[$targetPath]);
    $file = $files[0];
    
    if(!empty($desiredimage['name']))
    $name = $desiredimage['name'];
    else
    $name = $targetPath.'_'. get_alphanumeric(microtime()).'.jpg';
    $dirpath = UPLOAD_DIR;
    $targetPathfinal =  $dirpath . DIRECTORY_SEPARATOR . $targetPath . DIRECTORY_SEPARATOR . $name;
    
    if(!empty($_POST['multicropdata'][$targetPath][0]))
    {
        $x = explode('_',$_POST['multicropdata'][$targetPath][0]);
    }
    
    $imgg = file_get_contents($file["tmp_name"]);
    $source = imagecreatefromstring( $imgg );
    $thumb = imagecreatetruecolor($x[4], $x[5]);
    imageinterlace($thumb, true);
    imagecopyresampled($thumb, $source, 0, 0, $x[0],$x[1], $x[2], $x[3], $x[2], $x[3]);
    
    //imagejpeg($thumb, $targetPathfinal, 75);
    //imagedestroy($thumb);
    $cropWidth = 900;
    if($PSJavascript['croptype'][$targetPath]['sizes'][0][0])
    $cropWidth = $PSJavascript['croptype'][$targetPath]['sizes'][0][0];
    
    if ($thumb = create_right_size_image(array('imgObj'=>$thumb), $cropWidth,1))
    {
    // SAVE THE PIC
    imagejpeg($thumb, $targetPathfinal, 75);
        imagedestroy($thumb);
    }
    else
    {
    imagejpeg($thumb, $targetPathfinal, 75);
        imagedestroy($thumb);
    }
    return array($targetPath.'/'.$name);
}
function upload_post_images_new($infiles,$targetPath="posts",$isThumbRequired =  '')
{
    $uploadkey = key($infiles);
    $files = reArrayFiles($infiles[$uploadkey]);
    //$image_ratios_w_h =array("img-wh-r-1"=>"0.25","img-wh-r-2"=>"0.5","img-wh-r-3"=>"0.75","img-wh-r-4"=>"1","img-wh-r-5"=>"1.25","img-wh-r-6"=>"1.5","img-wh-r-7"=>"1.75","img-wh-r-8"=>"2");
    $image_ratios_w_h =array("img-wh-r-1"=>"0.5","img-wh-r-2"=>"1","img-wh-r-3"=>"1.5","img-wh-r-4"=>"2");
    foreach ( $files as $key=>$file)
    {
        $name = $targetPath.'_'. get_alphanumeric(microtime()).'.jpg';
        $targetPathfinal =  UPLOAD_DIR . DIRECTORY_SEPARATOR . $targetPath . DIRECTORY_SEPARATOR . $name;
        if(!empty($_POST['multicropdata'][$uploadkey][$key]))
        {
            move_uploaded_file($file['tmp_name'], $targetPathfinal);
            $img = get_upload_path($targetPath.'/'.$name);
            $crop = explode("_",$_POST['multicropdata'][$uploadkey][$key]);
            $width = $crop[4];
            $height = $crop[5];
            $x = $crop[0];
            $y = $crop[1];
            $aspect = $width / $height;
            $imgnew = ROOT_PATH."core/wideimage/demo/image.php?image=".urlencode($img)."&output=preset%20for%20demo&colors=255&dither=1&match_palette=1&demo=crop&left=".$x."&top=".$y."&width=".$width."&height=".$height;
            $imgrelurl = upload_from_url($targetPath,$imgnew,array());
            if($targetPath == 'posts'){
            $current_allowed_ratio = "img-wh-r-".floor($aspect /(0.5));
            $data[$current_allowed_ratio][] = $imgrelurl;
            }
            else
            {
                $data[] = $imgrelurl;
            }
            if($isThumbRequired == 'required');
            $imgnewthumb = upload_thumb($imgrelurl,array('height'=>250));
        }
        else
        {
            $x = getimagesize($file["tmp_name"]);
            move_uploaded_file($file['tmp_name'], $targetPathfinal);
            $width = $x[0];
            $height = $x[1];
            $aspect = $width / $height;
            $current_allowed_ratio = "img-wh-r-".floor($aspect /(0.5));
            $imgrelurl = $targetPath.'/'.$name;
            if($targetPath == 'posts')
            $data[$current_allowed_ratio][] = $imgrelurl;
            else
            $data[] = $imgrelurl;
            if($isThumbRequired == 'required');
            $imgnewthumb = upload_thumb($imgrelurl,array('height'=>250));
        }
    }
    return $data;
}
function mailer($data=array("profile_id"=>0,"email_id"=>0,"ccusers"=>array()),$currentmailertype='default',$message = array('data'=>'','title'=>'','altmsg'=>'','attachments'=>array()),$template='default.tpl'){
    global $additionalUrl,$urlparts;
    //$profile_id = 'profile_1450724789_643350';
    $ctime = time();
    if(!empty($data['profile_id']))
    {
        $profile_id = $data['profile_id'];
        $profiledetails = user_profile(array("profile_id"=>$profile_id));
        $email_id = $profiledetails['email'];
    
    }
    else if(!empty($data['email_id']))
    {
        $data['email_id'] = get_standard_email($data['email_id']);
        $profiledetails = user_profile(array("email"=>$data['email_id']));
        $email_id = $data['email_id'];
        if(empty($profiledetails))
        $profile_id = 0;
        else
        $profile_id = $profiledetails['profile_id'];    
    }
    else {
        return false;
    }
    
    $urlparts["utm_source"] = "newsletter"; //newsletter , google ,facebook , search engine
    $urlparts["utm_medium"] = "email"; //email , CPC ,Sharing
    $urlparts["utm_campaign"] = "launch"; //launch , login , logout
    $urlparts["email"] = $email_id;
    $urlparts["profile_id"] = $profile_id;
    
    
    $temp = mailer_data_add($currentmailertype,$email_id,$profile_id);
    if($temp['result'])
    {
        $mailerid = $urlparts["mailer_id"] = $temp['mailer_id'];
    }
    else
    return false;
    
    $additionalUrl = "";
    foreach($urlparts as $urlpart=>$urlvalue)
    {
        $additionalUrl .= "&".$urlpart."=".$urlvalue; 
    }

    $mailcontent['default']['tpl'] = 'default.tpl';
    $mailcontent['welcome']['tpl'] = 'welcome.tpl';
    $mailcontent['activation']['tpl'] = 'activation.tpl';
    $mailcontent['groupinvitation']['tpl'] = 'groupinvitation.tpl';
    $mailcontent['successregisteration']['tpl'] = 'successregisteration.tpl';
    $mailcontent['resetpassword']['tpl'] = 'resetpassword.tpl';
    $mailcontent['invitation']['tpl'] = 'invitation.tpl';
    $mailcontent['newsletter']['tpl'] = 'newsletter.tpl';
    $mailcontent['verifycompany']['tpl'] = 'verifycompany.tpl';
    $mailcontent['transactioncomplete']['tpl'] = 'transactioncomplete.tpl';
    $mailcontent['ticketonmail']['tpl'] = 'ticketonmail.tpl';
    $mailcontent['eventclaiminvite']['tpl'] = 'eventclaiminvite.tpl';
    $mailcontent['eventauthorizeotherprofile']['tpl'] = 'eventauthorizeotherprofile.tpl';
    
    ob_start();
    include (TEMPLATE_DIR.'/mailer/header.tpl');
    include (TEMPLATE_DIR.'/mailer/'.$template);
    include (TEMPLATE_DIR.'/mailer/footer.tpl');
    $mailer_content = ob_get_contents();
    ob_end_clean();

    $params = array();
    $params['<!--##psmailerbody##-->'] = $message['data'];  
    $params['<!--##psemail##-->'] = $email_id;
    $params['--psemail--'] = $email_id;
    $params['<!--##psusername##-->'] = ucwords($profiledetails['fname']);
    $params['<!--##psuserimage##-->'] = $profiledetails['userdp'];
    $params['<!--##psuserid##-->'] = $profile_id;
    $params['--psuserid--'] = $profile_id;
    $params['<!--##psmailerdate##-->'] = date("l, d-m-Y");
    $params['<!--##psfootersalutation##-->'] = "<br />Thank You, <br />Team Passionstreet";
    if(!empty($profiledetails['fname']))
    $params['<!--##psheadersalutation##-->'] = "Dear ".ucwords($profiledetails['fname']);
    else
    $params['<!--##psheadersalutation##-->'] = "Dear User ";
    $params['--pstime--'] = $ctime;
    $params['--pstoken--'] = strtolower(hash('sha512', implode("|",array($email_id,$profile_id,@$profiledetails['id'],$ctime,ENCRYPTION_KEY))));
    $params['<!--##psmailerbody##-->'] = $message['data'];  
    

    foreach($params as $search_str=>$replace_str)
    {
             $mailer_content = str_replace($search_str, $replace_str, $mailer_content);
    }
    //$pattern = '/https?\:\/\/[^\" ]+/i';
    $pattern = '/https?\:\/\/(?![^\" ]*(?:jpg|png|gif))[^\" ]+/i';
    $mailer_content = preg_replace_callback($pattern, "edit_mailer_urls", $mailer_content);
    
    $mailer_content = str_replace("</body>","<img src='".ROOT_PATH."t.php?url=".$additionalUrl."' height=0 width=0 style='width:0px;height:0px'/></body>",$mailer_content);
    //if(SERVERTYPE == 'local')
    //echo $mailer_content;
    $mailer_info['user']['email'] = $email_id;
    $mailer_info['user']['name'] = @$profiledetails['fname'];
    $mailer_info['title'] = (!empty($mailer_title))?$mailer_title:$message['title'];
    $mailer_info['altmsg'] = (!empty($mailer_alt))?$mailer_alt:$message['altmsg'];
    $mailer_info['content'] = $mailer_content;
    foreach($data['ccusers'] as $user)
    $mailer_info['user']['cc'][] = array('email'=>$user[0],'name'=>$user[1]);
    // $mailer_info['attachments'] = (!empty($mailer_upload))?$mailer_upload:(!empty($message['attachments'])?$message['attachments']:array());
    $mailer_info['attachments'] = (!empty($message['attachments'])?$message['attachments']:array());
    // if(SERVERTYPE == 'local')
    // {
    // ob_start();
    // $mailer_info['time'] = time();
    // print_array($mailer_info);
    // $mailer_content = ob_get_contents();
    // ob_end_clean();
    // $file = fopen(ROOT_DIR.'/mailer.html',"w");
    // fwrite($file,$mailer_content);
    // fclose($file);
    // }
    // else
    // {
    if(SERVERTYPE == 'local')
    {
    ob_start();
    $mailer_info['time'] = time();
    print_array($mailer_info);
    $html = ob_get_contents();
    ob_end_clean();
    }
    else
    $html = $mailer_content;
    $filename = $mailerid.".html";
    $file_to_save = UPLOAD_DIR.DIRECTORY_SEPARATOR.'mails'.DIRECTORY_SEPARATOR.$filename;
    file_put_contents($file_to_save, $html); 
    //$mailer_info['user'] = array('name'=>'Jauhar','email'=>'jauharamir@gmail.com');
    if(SERVERTYPE == 'live')
    sendmail($mailer_info);
    // }
    return $temp['mailer_id'];
}

function edit_mailer_urls($match){
    global $additionalUrl,$urlparts;
    $incomingqeryarray = parse_url($match[0]);
    if ($incomingqeryarray['host'] == $_SERVER['HTTP_HOST']) {
        parse_str(@$incomingqeryarray['query'],$incomingqery);
        if(empty($incomingqery))
            $incomingqery = array();
        $newquery = array_merge($urlparts,$incomingqery);
        $additionalUrl = "";
        foreach($newquery as $urlpart=>$urlvalue)
        {
            $additionalUrl .= "&".$urlpart."=".$urlvalue; 
        }

        //$returnstring = (strtok($match[0],'?'))."?".$additionalUrl;
    }
    else
    {
        
    }
    $returnstring = ROOT_PATH."l.php?url=".urlencode($match[0]).$additionalUrl;
    return $returnstring;
}
function getmailercontent(){
    return $html;
}
function usePlugin($pluginname,$params = array()){
    global $PSParams;
    switch ($pluginname){
        case 'mailer':
            if(empty($PSParams['plugins'][$pluginname]))
            {
                require_once CORE_DIR. '/PHPMailer-master/PHPMailerAutoload.php';
                $mail = new PHPMailer;
                $mail->isSMTP();
        if($_GET['admin'] == 1)
                $mail->SMTPDebug = 1;
        else
                $mail->SMTPDebug = 0;
                $mail->Debugoutput = 'html';
                $mail->Host = "mail.passionstreet.in";
                $mail->Port = 25;
                $mail->SMTPAuth = true;
                $mail->Username = "social@passionstreet.in";
                $mail->Password = "social@123";
                $mail->setFrom('social@passionstreet.in', 'Team PASSIONSTREET');
                $mail->addReplyTo('social@passionstreet.in', 'Team PASSIONSTREET');
                $PSParams['plugins'][$pluginname] = $mail;
            }
            else
            {
                return $PSParams['plugins'][$pluginname];
            }
            break;
        case 'htmlpurifier':
            if(empty($PSParams['plugins'][$pluginname]))
            {
                require_once CORE_DIR.'/htmlpurifier-4.8.0/library/HTMLPurifier.auto.php';
                $PSParams['plugins']['htmlpurifier'] = new HTMLPurifier(HTMLPurifier_Config::createDefault());
            }
            else
            {
                return $PSParams['plugins'][$pluginname];
            }
            break;
    case 'DomXPath':
            if(empty($PSParams['plugins']['DomDocument']))
            {
        $dom = new DomDocument;
            }
        else
        $dom = $PSParams['plugins']['DomDocument'];
        if(isset($params['html']))
        {
        $dom->loadHTML($params['html']);
        $PSParams['plugins']['DomXPath'] = new DomXPath($dom);
        }
        else if(isset($params['url']))
        {
        $dom->loadHTMLFile($params['url']);
        $PSParams['plugins']['DomXPath'] = new DomXPath($dom);
        }
            break;
        default :
            echo NULL;
    }
}
function sendmail($data = array()){
    global $PSParams;
    if($PSParams['server'] == 'local')
    return true;
    usePlugin('mailer');
    $content = $data['content'];
    $mail = & $PSParams['plugins']['mailer'];
    $mail->ClearAllRecipients();
    $mail->clearAttachments();
    $mail->addAddress($data['user']['email'], $data['user']['name']);
    $mail->Subject = $data['title'];
    $mail->msgHTML($content);
    $mail->AltBody = $data['altmsg'];
    foreach($data['attachments'] as $attachment)
    $mail->AddAttachment(UPLOAD_DIR.DIRECTORY_SEPARATOR.$attachment[0], $attachment[1]);
    //$mail->AddAttachment = $attachment;
    foreach($data['user']['cc'] as $data1)
    $mail->addCC($data1['email'],$data1['name']);
    if (!$mail->send()) {
    //echo $mail->ErrorInfo;
    if($_GET['admin'] == 1)
        print_array("Mailer Error",$mail->ErrorInfo);
    else
        return "Mailer Error: " . $mail->ErrorInfo;
    } else {
    //print_array('Message Sent');
        return "Message sent!";
    }

}
function construct_list_params($fields){
    $input_str = ' WHERE 1=1 AND ';
    $extrasql = ' ';
    $input_array = array();
    $limtistr = '';
    if(array_key_exists('paging',$fields))
    {
    $paging = $fields['paging'];
    $paging['page'] = (!empty($paging['page'])?$paging['page']:1);
    $paging['show'] = (!empty($paging['show'])?$paging['show']:10);
    
    $limtistr = ' limit ';
    $limtistr .= ($paging['page'] - 1)*$paging['show'];
    $limtistr .= ',';
    $limtistr .= $paging['show'];
    unset($fields['paging']);
    }
    foreach($fields as $key=>$field)
    {
        if($key == 'extraquery'){
            $extrasql .= $field.' AND ';
        }
        else{
            $input_str .= $key.' = ? AND ';
            $input_array[] = $field;
        }
    }
    $input_str .= $extrasql.'1=1';
    return array($input_str,$input_array,$limtistr);    
}

function renderer_member($user,$display = 3){
    global $PSParams;
    if($display == 1 || $display==2)
    {
        if($_GET['layout'] == 'amp')
        $display = 2;
    if($display == 1)
    {
        $linkonclick = 'onclick="loadPage(\'profile/'.$user['seopath'].'\')"';
        $link = '';
    }
    else if($display == 2)
    {
        $linkonclick = '';
        $link = ROOT_PATH.'profile/'.$user['seopath'];
    }
    ?>
        <div class="card-styl_2 <?=($user['isExpert'])?'expert':''?> clearfix">
            <span class="<?=($user['isExpert'])?'expert-flag':''?>"></span>
            <figure>
                <img alt="<?=$user['fullname']?>" class="unveil" src="<?=DEFAULT_IMG?>" data-src="<?=$user['userdp']?>">
                <figcaption><?=$user['fullname']?></figcaption>
            </figure>
            <div class="desc">
                <span class="name" <?=$linkonclick?> ><?=$user['fullname']?></span>
                <h5  <?=$linkonclick?>><?=($user['isExpert'])?'Expert In':'Passion'?></h5>
                <div  <?=$linkonclick?> class="passions">
                    <?php
                    $looppassion = ($user['isExpert'])?'isExpert':'joinedPassion';
                    foreach($user['passion'][$looppassion] as $key=>$passion)
                    {
                        if($key>=3)
                        {
                        echo '+';
                        break;
                        }
                    echo '<img '.$linkonclick.' alt="'.$PSParams['PSCategories'][$passion]['name'].'" class="unveil" src="'.DEFAULT_IMG.'" data-src="'.ROOT_PATH.'images/'.$PSParams['PSCategories'][$passion]['icon'].'" />';
                    }
                    ?>
                </div>
                <div <?=$linkonclick?> class="stat clearfix">
                    <section>
                        <strong><?=($user['follower'])?$user['follower']:0?></strong> Followers
                    </section>
                    <section>
                        <strong><?=($user['following'])?$user['following']:0?></strong> Following
                    </section>
                </div>
                <?php
                if($user['profile_id'] == $_SESSION['user']['profile_id'])
                {
                    echo '';
                }
                else
                {
                    if(empty($user['ishefollowingme']))
                        $temp = "width:80px; left:50%; right:auto; margin-left:-40px;";
                    else
                        $temp = "width:49.5%;";
                    if($display == 1)
                    {
                        echo ($user['amifollowinghim'])?'<span class="following " style="'.$temp.'">You Are<br/> Following</span>':'<button id="profile_follow_'.$user['profile_id'].'" class="btn btn-primary  ajax-btn " style="'.$temp.'"  data-profile_id="'.$user['profile_id'].'" data-t="user-follow" data-p-str="profile_id" data-aj="true" data-action="post">Follow</button>';
                        echo ($user['ishefollowingme'])?'<span class="following " style="'.$temp.'">Following<br/> You</span>':'';
                    }
                    else if($display==2)
                    {
                        echo ($user['amifollowinghim'])?'<span class="following " style="'.$temp.'">You Are<br/> Following</span>':'<a href="'.$link.'" class="btn btn-primary " style="'.$temp.'" >Follow</a>';
                        echo ($user['ishefollowingme'])?'<span class="following " style="'.$temp.'">Following <br/>You</span>':'';
                    }
                }
                ?>
             </div>
        </div>
    <?php
    }
    else if($display==3)
    {
        ?>
        <a class="hovercard" id="pp-<?=$user['profile_id']?>" htype="profile" hentity="<?=$user['profile_id']?>" href="<?=ROOT_PATH.'profile/'.$user['seopath']?>">
            <figure>
                <img class="unveil" alt="<?=$user['fullname']?>" src="<?=DEFAULT_IMG?>" data-src="<?=$user['userdp']?>">
                <figcaption>
                <?=$user['fullname']?>
                </figcaption>
                <span class="<?=($user['isExpert'])?'expert-flag':''?>"></span>
            </figure>
        </a>
        <?php
    }
}
function renderer_event($event){
    $path = ROOT_PATH.'event/'. $event['seopath'];
    ?>
    <figure>
        <a href="<?=$path?>">
            <img class="unveil" alt="<?=$event['eventname']?>" src="<?=DEFAULT_IMG_HORIZONTAL?>" data-src="<?=$event['eventpic']?>">
        </a>
    <a href="<?=$path?>">
        <div class="overlay">
            <button class="view-event"><?=(($event['isJoinedEvent'])?'View Event':'View Event')?></button>
        </div>
        </a>
    </figure>
    <!--<h3><a href="<?=$path?>"><?=$event['eventname']?></a></h3>-->
    <figcaption class='clearfix'>
        <div class="date-stamp">
            <?php
                //$eventdate = date_parse_from_format("Y-m-d", $event['datestart']);
                $eventdateData = getdate(strtotime($event['datestart']));
                $eventMonth = strtoupper($GLOBALS['PSParams']['months'][$eventdateData['mon']]);
                $eventDate = $eventdateData['mday'];
            ?>
            <span><?=$eventMonth?></span>
            <span><?=$eventDate?></span>
        </div>  
            <!--<span><?=generate_standard_date($event['datestart'])?></span> / <span><?=$event['address']?></span>-->
            <a href="<?=$path?>" class='title'><?=$event['eventname']?></a>
             
        </figcaption>
    <?php
}
function renderer_post($post){
	global $PSParams;
       $posturl = ROOT_PATH.'post/'.$post['post_id'];
       if($PSParams['posturlpagewise'] && $post['postedontype'] == 'event' && $post['postedonobject'])
       $posturl = ROOT_PATH.$post['postedonobject']['path'].'?sharedpost='.$post['post_id'];
       // $posturl = ROOT_PATH.'event/ultra-mumbai/308394226241353?sharedpost='.$post['post_id'];
    ?>
    <div class="post bx-styl" id="post_<?=$post['post_id']?>">
        <div class="metadata clearfix">
            <?php
            if($post['profile_id'] == $_SESSION['user']['profile_id'])
            {?>
            <div class="pull-right post-control">
                <i class="fa fa-angle-down"></i>
                <div class="dropdown">
                <ul>
                <li><a class="post-edit-btn btn-block" data-post_id="<?=$post['post_id']?>">Edit</a></li>
                <li><a class="ajax-btn btn-block" data-action="post" data-aj="true" data-p-str="post_id" data-t="post-delete" data-post_id="<?=$post['post_id']?>" data-trgtid="post_<?=$post['post_id']?>" data-c="post_delete">Delete</a></li>
                </ul>
                </div>
            </div>
            <?php
            }
            ?>
            <div class=" user">
                <a href="<?=ROOT_PATH.'profile/'.$post['creatorseopath']?>"><img class="unveil" src="<?=DEFAULT_IMG?>" data-src="<?=$post['userdp']?>"> <strong><?=$post['creatorfullname']?> </strong></a>
                posted <span><a><?=$post['posttypename']?></a></span>
                <?php
		if($post['postedontype'] == 'event' && $post['postedonobject'])
		echo ' in <span>event - <strong><a href="'.ROOT_PATH.$post['postedonobject']['path'].'">'.$post['postedonobject']['name'].'</a></strong>.</span><span class="hide"> You can also share your views,experiences about this event. </span>';
		else if($post['categoryname'])
		echo ' in <span><a href="'.ROOT_PATH.'passion/'.$post['category'].'">'.$post['categoryname'].'</a> </span><span class="hide"> You can also share your views,experiences in the passion. </span>';
		?>
		<span class="time reltime" data-time=<?=$post['create_date']?>></span>
            </div>
        </div>
        <?php
    if(strlen(strip_tags($post['content']))<300)
    echo '<div class="post-content">'.nl2br(htmlspecialchars_decode($post['content'])).'</div>';
        
    ?>
        <div class="post-header"><?=htmlspecialchars_decode($post['title'])?></div>
    <div class="post-desc jbox-gallery clearfix">
            <?php
        // $post['images2']['portrait'] = $post['images']['img-wh-r-1'];
        // $post['images2']['landscape'] = array_merge($post['images']['img-wh-r-4'],$post['images']['img-wh-r-5'],$post['images']['img-wh-r-6']);
        // $post['images2']['squre'] = array_merge($post['images']['img-wh-r-2'],$post['images']['img-wh-r-3']);
        
        foreach($post['images']['img-wh-r-1'] as $img)
        $post['images2']['portrait'][] = $img;
        foreach($post['images']['img-wh-r-2'] as $img)
        $post['images2']['squre'][] = $img;
        foreach($post['images']['img-wh-r-3'] as $img)
        $post['images2']['squre'][] = $img;
        foreach($post['images']['img-wh-r-4'] as $img)
        $post['images2']['landscape'][] = $img;
        
        //print_array($post['images2']);
        //print_array($post['images']);
        // --------------------------- v1 --------------------------------------//
        /*
        if(count($post['images2']['portrait'])>0){
               $x = '<a  class="portrait"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG_VERTICAL.'"  data-src="'.$post['images2']['portrait'][0].'" /></div></a>';
               unset($post['images']['img-wh-r-1'][0]);
               if((count($post['images2']['squre']) + count($post['images2']['squre'])) < 2)
               {
                if(count($post['images2']['portrait']) >= 2){
                    $xparent = '<div class="img_set clearfix">';
                    $x .= '<a  class="portrait"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG_VERTICAL.'"  data-src="'.$post['images2']['portrait'][1].'" /></div></a>';
                    unset($post['images']['img-wh-r-1'][1]);
                }
                else
                $xparent = '<div class="img_set single-portrait clearfix">';
               }
               else
               {
                $xparent = '<div class="img_set clearfix">';
                if(count($post['images2']['squre']) >= 2){
                    $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
                    $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][1].'" /></div></a>';
                    unset($post['images']['img-wh-r-2'][0]);
                    unset($post['images']['img-wh-r-2'][1]);
                }
                else if(count($post['images2']['landscape']) >= 2){
                    $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][0].'" /></div></a>';
                    $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][1].'" /></div></a>';
                    unset($post['images']['img-wh-r-4'][0]);
                    unset($post['images']['img-wh-r-4'][1]);
                }
                else if($post['images2']['landscape'][0] && $post['images2']['squre'][0])
                {
                    $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
                    $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][1].'" /></div></a>';
                    unset($post['images']['img-wh-r-4'][0]);
                    unset($post['images']['img-wh-r-2'][1]);
                }
               }
               $xparentclose = '</div>';
               echo $xparent.$x.$xparentclose ;
        }
        else if (count($post['images2']['landscape'])>=1){
               $xparent = '<div class="img_set clearfix">';
               $x = '<a  class="landscape"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG_HORIZONTAL.'"  data-src="'.$post['images2']['landscape'][0].'" /></div></a>';
               unset($post['images']['img-wh-r-4'][0]);
                
               if((count($post['images2']['squre'])) >= 2)
               {
                
                $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
                $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][1].'" /></div></a>';
                unset($post['images']['img-wh-r-2'][0]);
                unset($post['images']['img-wh-r-2'][1]);
               }
               else if(!empty($post['images2']['landscape'][1]))
               {
                
                $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][1].'" /></div></a>';
                unset($post['images']['img-wh-r-4'][1]);
               }
               $xparentclose = '</div>';
               echo $xparent.$x.$xparentclose ;
        }
        else if(count($post['images2']['squre'])>=4)
        {
               $xparent = '<div class="img_set clearfix">';
                
               $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
               $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][1].'" /></div></a>';
               $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][2].'" /></div></a>';
               $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][3].'" /></div></a>';
               $xparentclose = '</div>';
               echo $xparent.$x.$xparentclose ;
               unset($post['images']['img-wh-r-2'][0]);
               unset($post['images']['img-wh-r-2'][1]);
               unset($post['images']['img-wh-r-2'][2]);
               unset($post['images']['img-wh-r-2'][3]);
        }
        else if(count($post['images2']['squre'])>=2)
        {
              $xparent = '<div class="img_set clearfix">';
                
               $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
               $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][1].'" /></div></a>';
               $xparentclose = '</div>';
               echo $xparent.$x.$xparentclose ;
               unset($post['images']['img-wh-r-2'][0]);
               unset($post['images']['img-wh-r-2'][1]);
        }
        else if(count($post['images2']['squre'])>=1)
        {   
               $xparent = '<div class="img_set  single-fullwidth clearfix">';
                
               $x .= '<a  class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
               $xparentclose = '</div>';
               echo $xparent.$x.$xparentclose ;
               unset($post['images']['img-wh-r-2'][0]);
               
        }
        
            foreach($post['images'] as $imratios){
                foreach($imratios as $img){
                    echo '<img class="jbox-img unveil hide" src="'.DEFAULT_IMG.'"  data-src="'.$img.'" />';
                }
            }
        */
        
        // ------------------------------------ v2 ----------------------------------//
        /*
        $countportrait = count($post['images2']['portrait']);
        $countlandscape = count($post['images2']['landscape']);
        $countsqure = count($post['images2']['squre']);
        
        if($countportrait + $countlandscape + $countsqure)
        echo '<div class="img_set clearfix">';
        else
        echo '<div class="img_set1 clearfix hide">';
        
        
        //print_array($post['images2']);
        
        if(($countportrait)>0 && (($countlandscape) >=3 ))
        {
            echo '<a class="portrait"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['portrait'][0].'" /></div></a>';
            echo '<a class="landscape"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][0].'" style="height:190px" /></div></a>';
            echo '<a class="landscape"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][1].'" style="height:190px" /></div></a>';
            echo '<a class="landscape"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][2].'" style="height:190px" /></div></a>';
            unset($post['images2']['portrait'][0]);
            unset($post['images2']['landscape'][0]);
            unset($post['images2']['landscape'][1]);
            unset($post['images2']['landscape'][2]);
        }
        else if(($countportrait)>0 && (($countsqure + $countlandscape) >=2 ))
        {
            foreach($post['images2']['landscape'] as $img)
            $post['images2']['squre'][] = $img;
            echo '<a class="portrait"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['portrait'][0].'" /></div></a>';
            echo '<a class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
            echo '<a class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][1].'" /></div></a>';
            unset($post['images2']['portrait'][0]);
            unset($post['images2']['squre'][0]);
            unset($post['images2']['squre'][1]);
        }
        else if(($countlandscape)>0 && (($countsqure + $countlandscape) >=3 ))
        {
            echo '<a class="landscape"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][0].'" /></div></a>';
            unset($post['images2']['landscape'][0]);
            foreach($post['images2']['landscape'] as $img)
            $post['images2']['squre'][] = $img;
            if($countsqure + $countlandscape >=4)
            $onethird = 1;
            else
            $onethird = 0;
            echo '<a class="squre '.(($onethird)?'one-third':'').'"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
            echo '<a class="squre '.(($onethird)?'one-third':'').'"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][1].'" /></div></a>';
            if($onethird)
            echo '<a class="squre '.(($onethird)?'one-third':'').'"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][2].'" /></div></a>';
            unset($post['images2']['landscape'][0]);
            unset($post['images2']['squre'][0]);
            unset($post['images2']['squre'][1]);
            if($onethird)
            unset($post['images2']['squre'][2]);
        }
        else if(($countsqure)>=2)
        {
            echo '<a class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
            echo '<a class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][1].'" /></div></a>';
            if($post['images2']['squre'][2] && $post['images2']['squre'][3])
            {
            echo '<a class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][2].'" /></div></a>';
            echo '<a class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][3].'" /></div></a>';
            unset($post['images2']['squre'][2]);
            unset($post['images2']['squre'][3]);
            }
            unset($post['images2']['squre'][0]);
            unset($post['images2']['squre'][1]);
        }
        else
        {
            if($countportrait>=2)
            {
                echo '<a class="portrait"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['portrait'][0].'" /></div></a>';
                echo '<a class="portrait"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['portrait'][1].'" /></div></a>';
                unset($post['images2']['portrait'][0]);
                unset($post['images2']['portrait'][1]);
            
            }
            else if($countportrait>=1)
            {
                echo '<a class="portrait single-fullwidth"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['portrait'][0].'" /></div></a>';
                unset($post['images2']['portrait'][0]);
            }
            else if($countlandscape>=2)
            {
                echo '<a class="landscape"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][0].'" /></div></a>';
                echo '<a class="landscape"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][1].'" /></div></a>';
                unset($post['images2']['landscape'][0]);
                unset($post['images2']['landscape'][1]);
            }
            else if($countlandscape>=1)
            {
                echo '<a class="landscape single-fullwidth"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['landscape'][0].'" /></div></a>';
                unset($post['images2']['landscape'][0]);
            }
            else if($countsqure>=2)
            {
                echo '<a class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
                echo '<a class="squre"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][1].'" /></div></a>';
                unset($post['images2']['squre'][0]);
                unset($post['images2']['squre'][1]);
            }
            else if($countsqure>=1)
            {
                echo '<a class="squre single-fullwidth"><div><img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$post['images2']['squre'][0].'" /></div></a>';
                unset($post['images2']['squre'][0]);
            }
            
        }
        $remainginimgcount = 0;
        foreach($post['images2'] as $imratios){
                foreach($imratios as $img){
                    echo '<img class="jbox-img unveil hide" src="'.DEFAULT_IMG.'"  data-src="'.$img.'" />';
                $remainginimgcount++;
                }
            }
        if($remainginimgcount)
        echo '<span class="_ex"><div><div>+'.$remainginimgcount.'<img class="jbox-img unveil" src="'.DEFAULT_IMG.'"  data-src="'.$img.'" /></div></div></span>';
        
        echo '</div>';
        */
        // ------------------------------------------- v3 ------------------------------------------//
        $countportrait = count($post['images2']['portrait']);
        $countlandscape = count($post['images2']['landscape']);
        $countsqure = count($post['images2']['squre']);
        
        if($countportrait + $countlandscape + $countsqure)
        echo '<div class="img_set clearfix">';
        else
        echo '<div class="img_set1 clearfix hide">';
        foreach($post['images2'] as $imratios){
                foreach($imratios as $img){
                    echo '<img class="jbox-img " src="'.DEFAULT_IMG.'"  data-src="'.$img.'" />';
            }
            }
        echo '</div>';
        $postLog = unserialize($post['log']);
            
        if(!empty($postLog['date']))
        {
            echo '<div class="data row">';
            if(!empty($postLog['date'])) echo '<div class="col-md-4 col-sm-4  col-xs-6"><section><i class="date"></i><strong>Date</strong>'.$postLog['date'].'</section></div>';
            if(!empty($postLog['distance'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="distance"></i><strong>Distance</strong>'.$postLog['distance'].' KM</section></div>';
            if(!empty($postLog['totaltimehh'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="duration"></i><strong>Duration</strong>'.$postLog['totaltimehh'].'</section></div>';
            if(!empty($postLog['heartrateavg'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="heartrate"></i><strong>Heart Rate</strong>'.$postLog['heartrateavg'].(!empty($postLog['heartratemax'])?'(Max - '.$postLog['heartratemax'].')':'').'</section></div>';
            if(!empty($postLog['altitudehigh'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="ascent"></i><strong>Ascent</strong>'.$postLog['altitudehigh'].'</section></div>';
            if(!empty($postLog['altitudelow'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="descent"></i><strong>Descent</strong>'.$postLog['altitudelow'].'</section></div>';
            if(!empty($postLog['avgspeed'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="speed"></i><strong>Speed</strong>'.$postLog['avgspeed'].'</section></div>';
            if(!empty($postLog['calories'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="calories"></i><strong>Calories</strong>'.$postLog['calories'].'</section></div>';
            if(!empty($postLog['felt'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section class="felt">FEELING: <strong>'.$GLOBALS['PSParams']['PSSmilies'][$postLog['felt']].'</strong></section></div>';
            echo '</div>';
        }
        if(strlen(strip_tags($post['content']))>=300)
        echo '<div class="post-content collapsed">'.nl2br(htmlspecialchars_decode($post['content'])).'</div>';
        ?>
        </div>
        <?php
        $tags = explode(',',$post['subcategory']);
        if(!empty($tags) && !empty($post['subcategory']))
        {
        echo '<div class="row tags"><div class="col-md-12">';
        foreach($tags as $tag)
        echo "<span>#$tag</span>";
        echo '</div></div>';
        }
        ?>
        <div class="row post-footer">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <a class="post-ctrl" data-trgt="commentbox" data-post_id="<?=$post['post_id']?>"><i class="fa fa-comment"></i> Comment <?=(!empty($post['commentcount']))?'('.$post['commentcount'].')':''?></a>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                <span class=" ajax-btn" data-action="post" data-aj="true" data-p-str="post_id,post-c" data-t="post-like" data-post-c="<?=$post['likescount']?>" data-post_id="<?=$post['post_id']?>" data-trgtid="likescount_<?=$post['post_id']?>"><a  id="likescount_<?=$post['post_id']?>"><i class="fa fa-thumbs-up"></i> Like <?=(!empty($post['likescount']))?'('.$post['likescount'].')':''?></a></span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 text-right share">
                <a  class="post-ctrl" data-trgt="sharebox" data-post_id="<?=$post['post_id']?>"><i class="fa fa-share"></i><span id="sharescount_<?=$post['post_id']?>"> Share (<?=($post['extra']['tshares'])?$post['extra']['tshares']:0;?>)</span></a>
                
                <div class="share_cmt_box sharebox hide hideable">
                <div class="share_cmt_box_inner">
                    

                    <div class="right_p1 text-center">
                            <div class="socialbar_1 inverse text-center circle ">
                            <a class="sharepost ajax-btn fb" data-dialog=<?=(($PSParams['posturlpagewise'])?1:0)?> data-shareurl="<?=$posturl?>" data-action="post" data-aj="true" data-p-str="post_id,type"  data-t="post-share" data-post_id="<?=$post['post_id']?>" data-type="fb" data-trgtid="sharescount_<?=$post['post_id']?>" data-type="fb"><i class="fa fa-facebook"></i></a> 
                            <a class="sharepost ajax-btn twtr" data-dialog=<?=(($PSParams['posturlpagewise'])?1:0)?> data-shareurl="<?=$posturl?>" data-action="post" data-aj="true" data-p-str="post_id,type" data-t="post-share" data-post_id="<?=$post['post_id']?>" data-type="tw" data-trgtid="sharescount_<?=$post['post_id']?>" data-type="tw"><i class="fa fa-twitter"></i></a>
                            <a class="sharepost ajax-btn gplus" data-dialog=<?=(($PSParams['posturlpagewise'])?1:0)?> data-shareurl="<?=$posturl?>" data-action="post" data-aj="true" data-p-str="post_id,type" data-t="post-share" data-post_id="<?=$post['post_id']?>" data-type="gg" data-trgtid="sharescount_<?=$post['post_id']?>" data-type="gg"><i class="fa fa-google-plus"></i></a>
                            <a class="shareonemail email"><i class="fa fa-envelope"></i></a>
                            </div>
                    </div>
                        <form class="form1 email-share hide">                       
                                <div class="row form-group">
                                   <div class="col-md-12">
                                        <textarea class="form-control" id="emaillist" name="emaillist" rows="2" cols="20" placeholder="You may enter multiple email id's coma seperated"></textarea>
                                   </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-md-12 text-right">
                                     <button type="button" data-action="post" data-aj="true" data-p-str="post_id,type" data-t="post-share" data-post_id="<?=$post['post_id']?>" data-type="email" data-trgtid="sharescount_<?=$post['post_id']?>"  class="ajax-btn btn btn-default sharepostonemail" id="shareonemail_<?=$post['post_id']?>" data-post_id="<?=$post['post_id']?>" aria-expanded="false">Share</button>
                                     </div>
                                 </div>
                           </form>
                        </div>  
                        <div class="clearfix hide"></div>
                </div>
           </div>
                
        </div>
        <div class="share_cmt_box commentbox hide hideable">
            <div class="share_cmt_box_inner">
                <div class="left_p">
                    <h5>Comments</h5>

                    <div class="all_comments ps-container">
                        <div class="all_comments_container">
                        </div>
                    </div>   
                </div>
                <div class="right_p comment_form">
                    <textarea class="comment_tarea"  data-post_id="<?=$post['post_id']?>" data-comment_on="post" placeholder="Write your comment here..." cols="" rows=""></textarea>
                   <!-- <input type="button" style="margin:0" class="btn btn-primary submit-comment" value="Post"> -->
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
        
        
    <?php
}

/*
 Format post
*/
function format_post($post){
    $userProfileUrl = ROOT_PATH.'profile/'.$post['createrseopath'];
    $userProfileDP  = $post['userdp'];
    $postId = $post['id'];
    ?>
    <div class="post bx-styl" id="post_<?=$post['id']?>">
        <div class="metadata clearfix">
            <?php if($post['profile_id'] == $_SESSION['user']['profile_id']) { ?>
                <div class="pull-right post-control">
                    <i class="fa fa-angle-down"></i>
                    <div class="dropdown">
                    <ul>
                        <li>
                            <a class="post-edit-btn btn-block" href="javascript:void(0)" data-post_id="<?=$post['id']?>">Edit</a>
                        </li>
                        <li>
                            <a class="post-delete-btn btn-block" href="javascript:void(0)" data-action="post" data-aj="true" data-p-str="post_id" data-t="post-delete" data-post_id="<?=$post['id']?>" trgtid="post_<?=$post['id']?>" data-c="post_delete">Delete</a>
                        </li>
                    </ul>
                    </div>
                </div>
            <?php } ?>

            <div class=" user">
                <a href="<?=$userProfileUrl?>"><img class="unveil" src="<?=ROOT_PATH.'profiledp'?>" data-src="<?=$userProfileDP?>"> <strong><?=$post['fullname']?> </strong></a>
                <?php
                $headerText = '';
                
                if (isset($post['postRefName']) === true && $post['postRefName'] != '') {
                    $refUrl = ROOT_PATH.$post['postRefUrl'];
                    $headerText .= 'has posted in '.$post['module'].' : <a href="'.$refUrl.'"><strong>'.$post['postRefName'].'</strong></a>';
                } elseif ($post['category']) {
                    $categoryName = PSUtils::passionCategory($post['category']);

                    $refUrl = ROOT_PATH.'passion/'.$post['category'];
                    $headerText .= 'has posted in <a href="'.$refUrl.'"><strong>'.$categoryName['name'].'</strong></a>';
                }
                $taggedHtml     = '';
                $taggedMembers  = unserialize($post['tagged_members']);

                if (is_array($taggedMembers) === true && empty($taggedMembers) === false) {
                    $taggedProfileUrl = ROOT_PATH.'profile/'.get_alphanumeric($taggedMembers[0]['name']).'/'.$taggedMembers[0]['profile_id'];
                    $totalTaggedMember = count($taggedMembers);
                    
                    $taggedHtml  = "with<span class='tg-m-user'><a href='$taggedProfileUrl'>".$taggedMembers[0]['name']."</a></span>";
                    
                    if ($totalTaggedMember > 1) {
                        $moreTaggedMembers = '';
                        foreach ($taggedMembers as $key => $member) {
                            if ($key == 0) {
                                continue;
                            }
                            $moreTaggedMembers .= '<div>'.$member['name'].'</div>';
                        }
                        $totalTaggedMember = ($totalTaggedMember-1);
                        if ($totalTaggedMember == 1) {
                            $totalTaggedMember = $totalTaggedMember.' more';
                        } else {
                            $totalTaggedMember = $totalTaggedMember.' others';
                        }
                        $taggedHtml .= "and <span data-toggle='tooltip' data-placement='bottom' data-html='true' title='$moreTaggedMembers'> $totalTaggedMember</span>";
                    }
                }
                echo $headerText.' '.$taggedHtml;
                ?>
                <span class="time reltime" data-time="<?=strtotime($post['createdAt'])?>"></span>
            </div>
        </div>

        <?php
            $contentDisplayed = false;
            $postContent = nl2br(htmlspecialchars_decode(trim($post['about'])));
            if (strlen($postContent) < 300) {
                $contentDisplayed = true;
                echo '<div class="post-content">'.$postContent.'</div>';
            }

            // If url Video url 
            $videoMeta = json_decode($post['videoMeta'], true);
            if (isset($videoMeta['videoembed']) === true) {
                $thumbnail = strstr($videoMeta['pageimage'], 'psurl=');
                $thumbnail = str_ireplace('psurl=', '', $thumbnail);

                $generate = '';
                $generate .= '<div class="post external clearfix bx-styl">';
                $generate .= '<div class="flexi-video unveil" vsrc="'.$videoMeta['videoembed'].'" vsrc="'.DEFAULT_IMG.'" data-src="'.$thumbnail.'"></div>';
                $generate .= '<h3>'.$videoMeta['title'].'</h3>';
                $generate .= '<p class="clearfix">'.$videoMeta['description'].'</p>';
                $generate .= '<a class="hosturl" rel="noindex,nofollow" href="'.$videoMeta['url'].'">'.$videoMeta['host'].'</a></div>';
                echo '<div class="post-header">'.$generate.'</div>';
            } elseif (isset($videoMeta['title']) === true) {
                $generate  = "<div class='post external fullwidth clearfix bx-styl'>";
                $generate .= '<a href="'.$videoMeta['url'].'" target="_blank" rel="noindex,nofollow">';
                
                if (isset($videoMeta['pageimage']) === true) {
                    $thumbnail = strstr($videoMeta['pageimage'], 'psurl=');
                    $thumbnail = str_ireplace('psurl=', '', $thumbnail);
                    $generate .= '<div class="img-frame"><img class="unveil" src="'.DEFAULT_IMG.'" data-src="'.$thumbnail.'" /></div>';
                }
                $generate .= '<h3>'.$videoMeta['title'].'</h3>';
                $generate .= '<p class="clearfix">'.$videoMeta['description'].'</p> </a>';
                $generate .= '<a class="hosturl" rel="noindex,nofollow" href="'.$videoMeta['url'].'">'.$videoMeta['host'].'</a></div>';
                echo '<div class="post-header">'.$generate.'</div>';
            }
        ?>

        <div class="post-desc jbox-gallery clearfix">
        <?php
        foreach($post['images']['img-wh-r-1'] as $img)
            $post['images2']['portrait'][] = $img;
        foreach($post['images']['img-wh-r-2'] as $img)
            $post['images2']['squre'][] = $img;
        foreach($post['images']['img-wh-r-3'] as $img)
            $post['images2']['squre'][] = $img;
        foreach($post['images']['img-wh-r-4'] as $img)
            $post['images2']['landscape'][] = $img;
        
        
        // ------------------------------------------- v3 ------------------------------------------//
        $countportrait = count($post['images2']['portrait']);
        $countlandscape = count($post['images2']['landscape']);
        $countsqure = count($post['images2']['squre']);
        
        if($countportrait + $countlandscape + $countsqure)
            echo '<div class="img_set clearfix">';
        else
            echo '<div class="img_set1 clearfix hide">';

        foreach($post['images2'] as $imratios){
            foreach($imratios as $img){
                echo '<img class="jbox-img " src="'.DEFAULT_IMG.'"  data-src="'.$img.'" />';
            }
        }
        echo '</div>';
        $postLog = unserialize($post['workout']);
            
        if(!empty($postLog['date'])) {
            echo '<div class="data row">';
            if(!empty($postLog['date'])) echo '<div class="col-md-4 col-sm-4  col-xs-6"><section><i class="date"></i><strong>Date</strong>'.$postLog['date'].'</section></div>';
            if(!empty($postLog['distance'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="distance"></i><strong>Distance</strong>'.$postLog['distance'].' KM</section></div>';
            if(!empty($postLog['totaltimehh'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="duration"></i><strong>Duration</strong>'.$postLog['totaltimehh'].'</section></div>';
            if(!empty($postLog['heartrateavg'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="heartrate"></i><strong>Heart Rate</strong>'.$postLog['heartrateavg'].(!empty($postLog['heartratemax'])?'(Max - '.$postLog['heartratemax'].')':'').'</section></div>';
            if(!empty($postLog['altitudehigh'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="ascent"></i><strong>Ascent</strong>'.$postLog['altitudehigh'].'</section></div>';
            if(!empty($postLog['altitudelow'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="descent"></i><strong>Descent</strong>'.$postLog['altitudelow'].'</section></div>';
            if(!empty($postLog['avgspeed'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="speed"></i><strong>Speed</strong>'.$postLog['avgspeed'].'</section></div>';
            if(!empty($postLog['calories'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section><i class="calories"></i><strong>Calories</strong>'.$postLog['calories'].'</section></div>';
            if(!empty($postLog['felt'])) echo '<div class="col-md-4 col-sm-4 col-xs-6"><section class="felt">FEELING: <strong>'.$GLOBALS['PSParams']['PSSmilies'][$postLog['felt']].'</strong></section></div>';
            echo '</div>';
        }

        if ($contentDisplayed === false) {
            echo '<div class="post-content collapsed">'.$postContent.'</div>';
        }
        ?>
        </div>

        <div class="row post-footer">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <a href="javascript:void(0)" class="post-ctrl" data-trgt="commentbox" post_id="<?=$post['id']?>"><i class="fa fa-comment"></i> Comment <?=(isset($post['comments']['count']) === true && $post['comments']['count'] > 0) ?'('.$post['comments']['count'].')':''?></a>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                <?php

                    $likesCount = isset($post['likes']['count']) === true && $post['likes']['count'] > 0 ? $post['likes']['count'] : 0;
                    $userLike = 0;
                    if (isset($post['likes']['count']) === true && array_key_exists($_SESSION['user']['id'], $post['likes']['likes'])) {
                        $userLike = 1;
                    }

                    $likeMembersList  = '';
                    if ($likesCount > 0) {
                        foreach ($post['likes']['likes'] as $key => $value) {
                            $likeMembersList .= '<div id="like-'.$key.'">'.$value.'<div>';
                        }
                    }
                ?>

                <span class="post-like-btn" data-postId="<?=$postId?>" data-likecount="<?=$likesCount?>" data-userlike='<?=$userLike?>'>
                    <a href="javascript:void(0)">
                        <i class="fa fa-thumbs-up"></i> Like 
                        <span id="pl-<?=$postId?>" data-toggle='tooltip' data-placement='bottom' data-html='true' title='<?=$likeMembersList?>'><?=$likesCount > 0 ? '('.$likesCount.')':''?></span>
                    </a>
                </span>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-4 text-right share">
                <a class="post-ctrl" data-trgt="sharebox" data-post_id="<?=$postId?>"><i class="fa fa-share"></i><span id="sharescount_<?=$postId?>"> Share (0)</span></a>

                <div class="share_cmt_box sharebox hide hideable">
                <div class="share_cmt_box_inner">
                    <div class="right_p1 text-center">
                            <div class="socialbar_1 inverse text-center circle ">
                            <a class="sharepost ajax-btn fb" data-shareurl="<?=ROOT_PATH.'post/'.$postId?>" data-action="post" data-aj="true" data-p-str="post_id,type"  data-t="post-share" data-post_id="<?=$postId?>" data-type="fb" data-trgtid="sharescount_<?=$postId?>" ><i class="fa fa-facebook"></i></a> 
			    <a class="sharepost ajax-btn twtr" data-shareurl="<?=ROOT_PATH.'post/'.$postId?>" data-action="post" data-aj="true" data-p-str="post_id,type" data-t="post-share" data-post_id="<?=$postId?>" data-type="tw" data-trgtid="sharescount_<?=$postId?>" ><i class="fa fa-twitter"></i></a>
                            <a class="sharepost ajax-btn gplus" data-shareurl="<?=ROOT_PATH.'post/'.$postId?>" data-action="post" data-aj="true" data-p-str="post_id,type" data-t="post-share" data-post_id="<?=$postId?>" data-type="gg" data-trgtid="sharescount_<?=$postId?>" ><i class="fa fa-google-plus"></i></a>
                            <a class="shareonemail email"><i class="fa fa-envelope"></i></a>
                            </div>
                    </div>
                        <form class="form1 email-share hide">                       
                                <div class="row form-group">
                                   <div class="col-md-12">
                                        <textarea class="form-control" id="emaillist" name="emaillist" rows="2" cols="20" placeholder="You may enter multiple email id's coma seperated"></textarea>
                                   </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-md-12 text-right">
                                     <button type="button" data-action="post" data-aj="true" data-p-str="post_id,type" data-t="post-share" data-post_id="<?=$post['post_id']?>" data-type="email" trgtid="sharescount_<?=$post['post_id']?>"  class="ajax-btn btn btn-default sharepostonemail" id="shareonemail_<?=$post['post_id']?>" post_id="<?=$post['post_id']?>" aria-expanded="false">Share</button>
                                     </div>
                                 </div>
                           </form>
                        </div>  
                        <div class="clearfix hide"></div>
                </div>
           </div>
        </div>

        <div class="share_cmt_box commentbox">
            <div class="share_cmt_box_inner">
                <div class="left_p">
                    <?php
                        if (isset($post['comments']['count']) === true && $post['comments']['count'] > 3) {
                            echo "<span class='pull-right'>showing 3 of ".$post['comments']['count']."</span>";
                        }
                    ?>
                    <h5>Comments</h5>
                    <div class="all_comments ps-container">
                        <div class="all_comments_container">
                            <?php
                            if (isset($post['comments']['comments']) === true) {
                                foreach ($post['comments']['comments'] as $comment) {
                                    $commentTime = strtotime($comment['createdAt']);
                                    $commentId   = $comment['id'];

                                    $cUserProfileUrl = ROOT_PATH.'profile/'.get_alphanumeric($comment['user']).'/'.$comment['profile_id'];

                                    $userPic = ROOT_PATH.'files/'.$comment['userdp'];
                                    $commentHtml  = "<div class='notif_row nobg_border post-sec-cmnt-$commentId'>";
                                    
                                    $editCommentHtml = '';
                                    if($comment['userId'] == $_SESSION['user']['id']) {
                                        $editCommentHtml .= "<div class='pull-right post-control post-comment-control' id='post-cmnt-box-id-$commentId'>";
                                        $editCommentHtml .= "<i class='fa fa-pencil post-cmnt-edit-btn' data-id='$commentId'></i>";
                                        $editCommentHtml .= "<i class='fa fa-trash post-cmnt-delete-btn' data-action='post_cmnt' data-aj='true' data-p-str='post_cmnt_id' data-t='post-cmnt-delete' data-id='$commentId' trgtid='post_cmnt_$commentId' data-c='post_cmnt_delete'></i>";
                                        $editCommentHtml .= '</div>';
                                    }

                                    $commentHtml .= $editCommentHtml;
                                    $commentHtml .= '<div class="usr_dp">';
                                    $commentHtml .= "<a href='$cUserProfileUrl'><img class='unveil' src='".ROOT_PATH."profiledp' data-src='$userPic'></a></div>";
                                    $commentHtml .= '<div class="user_comment">';
                                    $commentHtml .= "<span class='gen_usr_nm'><a href='$cUserProfileUrl' class='post-cmnt-uname-$commentId'>".$comment['user'].'</a><span class="post-cmnt-content"> '.$comment['comment'].'</span></span>';

                                    $commentHtml .= "<span class='time reltime post-cmnt-tm-$commentId' data-time='$commentTime'></span>";
                                    $commentHtml .= '</div></div>';
                                    echo $commentHtml;
                                }
                            }
                            ?>
                        </div>
                    </div>   
                </div>
                <div class="right_p comment_form">
                    <textarea class="comment_tarea" onkeypress="if(isEnterPressed(event)){post_add_comment(this)}" post_id="<?=$post['id']?>" comment_on="group" placeholder="Write your comment here..." cols="" rows=""></textarea>
                   <!-- <input type="button" style="margin:0" class="btn btn-primary submit-comment" value="Post"> -->
                </div>
                <div class="clearfix"></div>
            </div>
       </div>
    </div>
    <?php
}

function minify_html($input) {
    if(!(MINIFY))
        return $input;
    
    if(trim($input) === "") return $input;
    // Remove extra white-space(s) between HTML attribute(s)
    $input = preg_replace_callback('#<([^\/\s<>!]+)(?:\s+([^<>]*?)\s*|\s*)(\/?)>#s', function($matches) {
        return '<' . $matches[1] . preg_replace('#([^\s=]+)(\=([\'"]?)(.*?)\3)?(\s+|$)#s', ' $1$2', $matches[2]) . $matches[3] . '>';
    }, str_replace("\r", "", $input));
    // Minify inline CSS declaration(s)
    if(strpos($input, ' style=') !== false) {
        $input = preg_replace_callback('#<([^<]+?)\s+style=([\'"])(.*?)\2(?=[\/\s>])#s', function($matches) {
            return '<' . $matches[1] . ' style=' . $matches[2] . minify_css($matches[3]) . $matches[2];
        }, $input);
    }
    return preg_replace(
        array(
            // t = text
            // o = tag open
            // c = tag close
            // Keep important white-space(s) after self-closing HTML tag(s)
            '#<(img|input)(>| .*?>)#s',
            // Remove a line break and two or more white-space(s) between tag(s)
            '#(<!--.*?-->)|(>)(?:\n*|\s{2,})(<)|^\s*|\s*$#s',
            '#(<!--.*?-->)|(?<!\>)\s+(<\/.*?>)|(<[^\/]*?>)\s+(?!\<)#s', // t+c || o+t
            '#(<!--.*?-->)|(<[^\/]*?>)\s+(<[^\/]*?>)|(<\/.*?>)\s+(<\/.*?>)#s', // o+o || c+c
            '#(<!--.*?-->)|(<\/.*?>)\s+(\s)(?!\<)|(?<!\>)\s+(\s)(<[^\/]*?\/?>)|(<[^\/]*?\/?>)\s+(\s)(?!\<)#s', // c+t || t+o || o+t -- separated by long white-space(s)
            '#(<!--.*?-->)|(<[^\/]*?>)\s+(<\/.*?>)#s', // empty tag
            '#<(img|input)(>| .*?>)<\/\1>#s', // reset previous fix
            '#(&nbsp;)&nbsp;(?![<\s])#', // clean up ...
            '#(?<=\>)(&nbsp;)(?=\<)#', // --ibid
            // Remove HTML comment(s) except IE comment(s)
            '#\s*<!--(?!\[if\s).*?-->\s*|(?<!\>)\n+(?=\<[^!])#s'
        ),
        array(
            '<$1$2</$1>',
            '$1$2$3',
            '$1$2$3',
            '$1$2$3$4$5',
            '$1$2$3$4$5$6$7',
            '$1$2$3',
            '<$1$2',
            '$1 ',
            '$1',
            ""
        ),
    $input);
}
// CSS Minifier => http://ideone.com/Q5USEF + improvement(s)
function minify_css($input) {
    if(trim($input) === "") return $input;
    return preg_replace(
        array(
            // Remove comment(s)
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
            // Remove unused white-space(s)
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~+]|\s*+-(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
            // Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
            '#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
            // Replace `:0 0 0 0` with `:0`
            '#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
            // Replace `background-position:0` with `background-position:0 0`
            '#(background-position):0(?=[;\}])#si',
            // Replace `0.6` with `.6`, but only when preceded by `:`, `,`, `-` or a white-space
            '#(?<=[\s:,\-])0+\.(\d+)#s',
            // Minify string value
            '#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][a-z0-9\-_]*?)\2(?=[\s\{\}\];,])#si',
            '#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
            // Minify HEX color code
            '#(?<=[\s:,\-]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
            // Replace `(border|outline):none` with `(border|outline):0`
            '#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
            // Remove empty selector(s)
            '#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s'
        ),
        array(
            '$1',
            '$1$2$3$4$5$6$7',
            '$1',
            ':0',
            '$1:0 0',
            '.$1',
            '$1$3',
            '$1$2$4$5',
            '$1$2$3',
            '$1:0',
            '$1$2'
        ),
    $input);
}
// JavaScript Minifier
function minify_js($input) {
    if(trim($input) === "") return $input;
    $input = str_replace('\\//','||regex||',$input);
    $input = preg_replace(
        array(
            // Remove comment(s)
            '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
            // Remove white-space(s) outside the string and regex
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
            // Remove the last semicolon
            '#;+\}#',
            // Minify object attribute(s) except JSON attribute(s). From `{'foo':'bar'}` to `{foo:'bar'}`
            '#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
            // --ibid. From `foo['bar']` to `foo.bar`
            '#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
        ),
        array(
            '$1',
            '$1$2',
            '}',
            '$1$3',
            '$1.$3'
        ),
    $input);
    $input = str_replace('||regex||','\\//',$input);
    return $input;
}
function validate_email($email){
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}
function validate_url($url){
    return preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url);
}
function filter_incoming_fields(){
    foreach($_POST as $key=>$value)
    {
        $_POST[$key] = htmlspecialchars($value);
    }
    foreach($_GET as $key=>$value)
    {
        $_GET[$key] = htmlspecialchars($value);
    }
    foreach($_REQUEST as $key=>$value)
    {
        $_REQUEST[$key] = htmlspecialchars($value);
    }
}
if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
           $headers = '';
       foreach ($_SERVER as $name => $value)
       {
           if (substr($name, 0, 5) == 'HTTP_')
           {
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
           }
       }
       return $headers;
    }
}
function get_standard_email($email){
    $email = trim($email);
    if(endsWith($email,'gmail.com') || endsWith($email,'googlemail.com')){
    //if(strpos(strtolower($email),'gmail.com')!==false || strpos(strtolower($email),'googlemail.com')!==false){
        $email = strtolower($email);
        $email = str_replace ("@gmail.com", "",$email); 
        $email = str_replace (".", "",$email); 
        $email = $email.'@gmail.com'; 
    }
    return validate_email($email)?$email:false;
} 
function get_standard_mobile($mobile){
    $mobile = trim($mobile);
    //$mobile = preg_replace("/[^0-9,.]/", "", $mobile);
    //$regex="/^[1-9][0-9]*$/"; 
    // $regex="/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/"; 
    $regex="/^[789]\d{9}$/"; 
    return (preg_match($regex, $mobile))?$mobile:false;
} 
function merge_css(){
        if((filemtime(ROOT_DIR.'/css/style.css') > filemtime(ROOT_DIR.'/css/all.css')) || (filemtime(ROOT_DIR.'/css/style_extra.css') > filemtime(ROOT_DIR.'/css/all.css')))
    {
            $cssfiles[] = 'css/bootstrap.min.css';
        $cssfiles[] = 'css/bootstrap-theme.min.css';
        $cssfiles[] = 'css/style.css';
        $cssfiles[] = 'css/style_extra.css';
        $cssoutput = '';
        foreach($cssfiles as $cssfile)
        {
            $cssoutput .= minify_css(file_get_contents(ROOT_DIR.DIRECTORY_SEPARATOR.$cssfile));
        }
        file_put_contents(ROOT_DIR.DIRECTORY_SEPARATOR.'css'.DIRECTORY_SEPARATOR.'all.css',$cssoutput);
    }
}
function merge_js(){
    global $PSJsincludes;
    $corefilelist = $PSJsincludes['core'];
    $alljsmtime = filemtime(ROOT_DIR.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'all.js');
    $minify = 0;
    foreach($corefilelist as $key=>$file)
    {
        $corefilelist[$key] = ROOT_DIR.DIRECTORY_SEPARATOR. str_replace('/',DIRECTORY_SEPARATOR,$file);
        if(filemtime($corefilelist[$key]) > $alljsmtime)
        {
            $minify = 1;
        }
    }
    if($minify)
    {
        $jsoutput = '';
        foreach($corefilelist as $corefile)
        {
            $jsoutput .= minify_js(file_get_contents($corefile));
        }
        file_put_contents(ROOT_DIR.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'all.js',$jsoutput);
    }
}
function assoc_array_shuffle($array)
{
    $keys = array_keys($array);
    shuffle($keys);
    return array_merge(array_flip($keys), $array);
}
function preprocessApi(){
    global $cache,$overridecache;
    $arguments = func_get_args();
    $lastindex = count($arguments)-1;
    if(CACHING && !($overridecache))
    {
        return cache_function($arguments);
    }
    else
    {
        $arguments[$lastindex-1] = true;
        $functionName = $arguments[0];
        unset($arguments[0]);
        return call_user_func_array($functionName,$arguments);
    }
}
function cache_function($arguments){
        $fn = $arguments[0];
    unset($arguments[0]);
    $fn_args = array_values($arguments);
    //$folder = md5($fn);
        $folder = $fn;
    $lastargumentindx = count($fn_args) - 1;
        //remove this for variable caching time
        $fn_args[$lastargumentindx]['expiry'] = 3;
    $hours = ($fn_args[$lastargumentindx]['expiry'] / 60);
    $current_time = time(); 
    $expire_time = $hours * 60 * 60; 
        $fn_args_temp = $fn_args;
        $fn_args_temp[$lastargumentindx - 1] = '';
        $file = ROOT_DIR.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.'_'.md5(serialize($fn_args_temp));
    $file_time = filemtime($file);
        if((empty($fn_args[$lastargumentindx - 1]) || $fn_args[$lastargumentindx - 1] == false) && file_exists($file) && ($current_time - $expire_time < $file_time)) {
        return unserialize(file_get_contents($file));
    }
        else if($fn_args[$lastargumentindx - 1] == 'delete'){
            unlink($file);
        }
    else {
        if($fn) {
            $fn_args[$lastargumentindx - 1] = true;
            $content = call_user_func_array($fn,$fn_args); 
        }
        $scontent .= serialize($content);
        if (!is_dir(ROOT_DIR.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.$folder)) {
            // dir doesn't exist, make it
            mkdir(ROOT_DIR.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.$folder);
        }
        file_put_contents($file,$scontent);
        return $content;
    }
    
}
function transaction_start($fields){ //$misc table for donations etc
    return transaction_add($fields);
}
function transaction_end($postParams){
	$x =  call_user_func_array('transaction_complete_'.PAYMENTGATEWAY, array($postParams));
    $y = transaction_get_details($x['txnid'],(($x['subtransactiontype'])?true:false));
    $response = $y;
        $productDetail = updategetProductDetails($y,$x);
    //$productDetail = updategetProductDetails($y['producttype'],$y['productid'],$x['txnid'],$x['status']);
    /*$response['producttype'] = $y['producttype'];
    $response['productid'] = $y['productid'];
    $response['producturl'] = $y['producturl'];*/
    $response['txnstatus'] = $x['status'];
    $response['txnid'] = $x['txnid'];
    $response['txnamount'] = $x['amount'];
    $response['txnhashOK'] = $x['hashOK'];
    $response['productdetail'] = $productDetail;
    return $response;
}
function transaction_payu($transactionid,$subtransactiontype = '')
{
    global $connection,$PSData;
	$misc = false;
	if($subtransactiontype)
	$misc = true;
    $temp = transaction_get_details($transactionid,$misc);
	if(empty($temp))
	return false;
	
	$_SESSION['ticketing'] = $temp;
    $_SESSION['payumoneydata']['txnid'] = $temp['transactionid'];
    $_SESSION['payumoneydata']['amount'] = $temp['amount'];
    $_SESSION['payumoneydata']['firstname'] = $PSData['user']['fname'];
    $_SESSION['payumoneydata']['email'] = $PSData['user']['email'];
    $_SESSION['payumoneydata']['phone'] = '';
    // $_SESSION['payumoneydata']['productinfo'] = 'Event Ticket Booking - '.$temp['transactionid'];
    $_SESSION['payumoneydata']['productinfo'] = array();
	$_SESSION['payumoneydata']['productinfo']['txnid'] = $temp['transactionid'];
    $_SESSION['payumoneydata']['productinfo']['productid'] = $temp['productid'];
    $_SESSION['payumoneydata']['productinfo']['producttype'] = $temp['producttype'];
	if($temp['productdetails'] && unserialize($temp['productdetails']))
	{
		$x = array();
		$x = unserialize($temp['productdetails']);
		$_SESSION['payumoneydata']['productinfo']['subtransactions'] = $x['subtransactions'];
		foreach($x['extraamount'] as $amount)
		$_SESSION['payumoneydata']['amount'] = $_SESSION['payumoneydata']['amount'] + $amount;
	}
    //$_SESSION['payumoneydata']['productinfo']['subtransactiontype'] = $temp['subtransactiontype'];
    $_SESSION['payumoneydata']['productinfo'] = serialize($_SESSION['payumoneydata']['productinfo']);
    $_SESSION['payumoneydata']['surl'] = ROOT_PATH.'postpayment/payu/'.$temp['transactionid'].'/success';
    $_SESSION['payumoneydata']['furl'] = ROOT_PATH.'postpayment/payu/'.$temp['transactionid'].'/failure';
    $_SESSION['payumoneydata']['service_provider'] = 'payu_paisa';
    $_SESSION['payumoneydata']['key'] = $MERCHANT_KEY ='8b7L5m';
    $_SESSION['payumoneydata']['address1'] = '';
    $_SESSION['payumoneydata']['address2'] = '';
    $_SESSION['payumoneydata']['city'] = '';
    $_SESSION['payumoneydata']['country'] = '';
    $_SESSION['payumoneydata']['curl'] = '';
    $_SESSION['payumoneydata']['lastname'] = '';
    $_SESSION['payumoneydata']['pg'] = '';
    $_SESSION['payumoneydata']['state'] = '';
    $_SESSION['payumoneydata']['udf1'] = '';
    $_SESSION['payumoneydata']['udf2'] = '';
    $_SESSION['payumoneydata']['udf3'] = '';
    $_SESSION['payumoneydata']['udf4'] = '';
    $_SESSION['payumoneydata']['udf5'] = '';
    $_SESSION['payumoneydata']['zipcode'] = '';


    $posted = $_SESSION['payumoneydata'];
    $hash = '';
    $formError = 0;

    $SALT = "jqEBBsAv";
    $PAYU_BASE_URL = "https://secure.payu.in";

    $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
    $hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';  
        foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));

    $_SESSION['payumoneydata']['hash'] = $hash;
    return $_SESSION['payumoneydata'];
}
function transaction_complete_payu($postParams){
	$status=$postParams["status"];
    $firstname=$postParams["firstname"];
    $amount=$postParams["amount"];
    $txnid=$postParams["txnid"];
    $posted_hash=$postParams["hash"];
    $key=$postParams["key"];
    $productinfo=$postParams["productinfo"];
    $email=$postParams["email"];
	$salt="jqEBBsAv";
    if(isset($postParams["additionalCharges"])) {
            $additionalCharges=$postParams["additionalCharges"];
            $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
    }
    else {    
            $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
    }
    $hash = hash("sha512", $retHashSeq);
    if ($hash != $posted_hash){
        $hashOk = false;
    }
    else
    {
        $hashOk = true;
    }
    /*if($hashOk)
	{
		$response['status'] = 'invalid';
        $transactionUpdate['status'] = 'complete|'.$response['status'];
	}
    else*/ if($response['status'])
    {
        $response['status'] = 'success';
        $transactionUpdate['status'] = 'complete|success';
    }
    else
    {
        $response['status'] = $status;
        $transactionUpdate['status'] = 'complete|'.$response['status'];
    }
    $transactionUpdate['gatewayresponse'] = $postParams; 
	$response['subtransactiontype'] = false;
	if($x =  unserialize($productinfo)){
		if($x['subtransactiontype'])
		$response['subtransactiontype'] = $x['subtransactiontype'];
	}
	if(transaction_update_transactions($txnid,$transactionUpdate,$response['subtransactiontype'])) // for misc transactions , donations
    {
        $response['txnid'] = $txnid;
        $response['amount'] = $amount;
        $response['hashOK'] = $hashOk;
        $response['status'] = $transactionUpdate['status'];
	}
    else
    {
        $response['hashOK'] = 'Some Error Occured , Please try again';
        $response['status'] = $transactionUpdate['status'];
    }
    return $response;
}
function transaction_update_transactions($txnid,$transactionUpdate,$misc = false){
	if($txnid && $transactionUpdate)
	{
		$return = transaction_update($txnid,$transactionUpdate,$misc);
		$subtransactions = transaction_get_subtransaction_list(array('transactionid'=>$txnid));
		foreach($subtransactions as $subtransaction){
			transaction_update($subtransaction['transactionid'],$transactionUpdate,$misc);
		}
	}
	return $return;
}
function transaction_update_payu($fields,$bulk=1){
    if(empty($fields['transactionids']))
    return;
    $pagesize = 48;
    if($bulk == 1)
    {
        $count  = count($fields['transactionids']);
        $page = ceil($count / $pagesize);
    }
    else
    {
        $bulk = 0;
        $count  = 1;
        $page = 1;
    }
    print_array($fields['transactionids']);
    
    for($i=0;$i<$page;$i++)
    {
        $transactionids = implode('|',array_slice($fields['transactionids'],$i*$pagesize,$pagesize));
        $url = 'https://www.payumoney.com/payment/op/getPaymentResponse?merchantKey=8b7L5m&merchantTransactionIds='.$transactionids; 
		print_array($url);
        $data =array('merchantKey'=>'8b7L5m','merchantTransactionIds'=>$transactionids);
        $options = array( 
          'http' => array( 
            'header' => "Authorization: EvAm/COTUjsRPy0pzHkTEEQOa0dU+HnGApiPEKrkDQ8=\r\n".
                        "Content-Type: application/json\r\n", 
            'method' => 'POST', 
            'Authorization'=> 'EvAm/COTUjsRPy0pzHkTEEQOa0dU+HnGApiPEKrkDQ8=', 
            'content' => http_build_query($data) 
            ), 
          ); 
        $context = stream_context_create($options); 
        $result = file_get_contents($url, false, $context); 
        if($result === FALSE) {  

        } 
        else
        {
            $output = json_decode($result, true);
			print_array($output);
            $validtransactionids = array();
            if($bulk == 1)
            {
                if($output['status'] != -1)
                {
                    $outputdata = $output['result'];
                    foreach($outputdata as $transaction){
                        $validtransactionids[] = $transaction['merchantTransactionId'];
                        $response = transaction_update_transactions($transaction['merchantTransactionId'],array('status'=>(($transaction['postBackParam']['status'] == 'success')?'complete|success':'complete|failure') ,'gatewayresponse' =>$transaction['postBackParam'], 'gatewaylog'=>'Bulk fetch update','updatedByCron'=>1 ));
						//$response = transaction_update_status($transaction['merchantTransactionId'],(($transaction['postBackParam']['status'] == 'success')?'complete|success':'complete|failure'),$transaction['postBackParam'],'Bulk fetch update');
                        if($response)
                        $updatetransactionids[] = $transaction['merchantTransactionId'];    
                    }
                    $othertransactionids = array_diff($fields['transactionids'], $validtransactionids);
                    foreach($othertransactionids as $transactionid){
                        transaction_update_payu(array('transactionids'=>array($transactionid)),0);
                    }
                }
                else
                {
                    foreach($fields['transactionids'] as $transactionid){
                        transaction_update_payu(array('transactionids'=>array($transactionid)),0);
                    }
                }
            }
            else{
				if($output['status'] != -1)
                {
					$transaction = $output['result'][0];
                    $response = transaction_update_transactions($transaction['merchantTransactionId'],array('status'=>'complete|success','gatewayresponse' =>$transaction['postBackParam'], 'gatewaylog'=>'individual fetch update','updatedByCron'=>1 ));
					//$response = transaction_update_status($transaction['merchantTransactionId'],(($transaction['postBackParam']['status'] == 'success')?'complete|success':'complete|success'),$transaction['postBackParam'],'individual fetch update');
                    if($response)
                    $updatetransactionids[] = $transaction['merchantTransactionId'];
                }
                else
                {
					$response = transaction_update_transactions($fields['transactionids'][0],array('status'=>'complete|failure','gatewayresponse' =>$output, 'gatewaylog'=>'individual fetch update','updatedByCron'=>1 ));
					//$response = transaction_update_status($fields['transactionids'][0],'complete|failure',$output,'individual fetch update');
                    if($response)
                    $updatetransactionids[] = $fields['transactionids'][0];
                }
                
            }
        }
        if($updatetransactionids)
        {
            foreach($updatetransactionids as $transactionid)
            {
                $x['txnid'] = $transactionid;
                $y = transaction_get_details($x['txnid']);
                $x['status'] = $y['status'];
                $productDetail = updategetProductDetails($y,$x);
                echo $transactionid.'->'.$x['status'].'<br>';
                if($x['status'] == 'complete|success')
                generate_event_ticket ($transactionid,1);
            }
		}
    }
}
function updategetProductDetails($transaction,$paymentinfo){
        $productType = $transaction['producttype'];
        $productId = $transaction['productid'];
        $transactionid = $paymentinfo['txnid'];
        $transactionstatus = $paymentinfo['status'];
    if($productType == 'event')
    {
        $eventdata = event_get_details($productId,1);
        if($transactionstatus == 'complete|success')
        {
            $ticket_get_list = ticket_get_list(array('tkteventid'=>$productId,'tkttransactionid'=>$transactionid));
            $ticketcount      = array();
            foreach($ticket_get_list as $ticket)
            {
                                if($ticket['tktcatbibstart'] != 0 && $ticket['tktcatbibintial'])
                                {
                                    if (empty($ticketcount[$ticket['tktcode']]))
                                            $ticketcount[$ticket['tktcode']] = 1;
                                    else
                                            $ticketcount[$ticket['tktcode']]++;
                                    $ticketcode = $ticket['tktcode'];
                                    $newbib = $eventdata['ticketcategories'][$ticket['tktcode']]['lastbib'] + $ticketcount[$ticket['tktcode']];
                                    $newbibcode = $eventdata['ticketcategories'][$ticket['tktcode']]['bibinitial'] . $newbib;
                                    ticketcategories_update($ticketcode, array(
                                            'tktcatlastbibno' => $newbib
                                    ));
                                    ticket_update($ticket['tktid'],array('tktbibno'=>$newbibcode));
                                }
            }
                        $couponsmeta = unserialize($transaction['couponsapplied']);
                        if($couponsmeta && $couponsmeta['couponcode'] && $couponsmeta['couponusercount']>0)
                        {
                            $fields = array();
                            $fields['couponEntityType'] = 'event';
                            $fields['couponEntityId'] = $productId;
                            coupon_consume($couponsmeta['couponcode'],$couponsmeta['couponusercount'],$fields);
                        }
                        relation_register($transaction['userid'], 'joinedEvent', $transaction['productid']);
        }
        return $eventdata;
    }
}
function addUserElements($user)
{
    $temp['fullname']= ucwords($user['fname'].' '.$user['lname']);
        $temp['seopath']= get_alphanumeric($temp['fullname']).'/'.$user['profile_id'];
        $temp['userdp']=get_upload_path($user['userdp'],'profiledp');
        $temp['userbg']=get_upload_path($user['userbg'],'coverbg');
        return $temp;
}
function move_images_from_temp($imgArray,$target){
    if(empty($imgArray))
    return;
    $returnArray = array();
    foreach($imgArray as $img)
    {
        $img1 = str_replace(UPLOAD_PATH,'',$img);
        $img2 = str_replace('temp',$target,$img1);
        $img1 = $img1;
        $img3 = explode('/',$img1);
        $img4 = implode(DIRECTORY_SEPARATOR,$img3);
        $img5 = str_replace('temp',$target,$img4);
        //rename($img4,$img5);
        rename(UPLOAD_DIR.DIRECTORY_SEPARATOR.$img4,UPLOAD_DIR.DIRECTORY_SEPARATOR.$img5);
        //rename('thumb'.DIRECTORY_SEPARATOR.$img4,'thumb'.DIRECTORY_SEPARATOR.$img5);
        @rename(UPLOAD_DIR.DIRECTORY_SEPARATOR.'thumb'.DIRECTORY_SEPARATOR.$img4,UPLOAD_DIR.DIRECTORY_SEPARATOR.'thumb'.DIRECTORY_SEPARATOR.$img5);
        $returnArray[] = $img2;
    }
    return $returnArray;
}
function get_image_ratio($imgArray){
    if(empty($imgArray))
    return;
    $data = array();
    foreach($imgArray as $img)
    {
        $img1 = str_replace(UPLOAD_PATH,'',$img);
        $img2 = $img1;
        $img1 = UPLOAD_DIR.DIRECTORY_SEPARATOR.$img1;
        $img3 = explode('/',$img1);
        $img4 = implode(DIRECTORY_SEPARATOR,$img3);
        
        $x = getimagesize($img4);
        $width = $x[0];
        $height = $x[1];
        $aspect = $width / $height;
        $x = floor($aspect /(0.5));
        if($x > 4)
        $x = 4;
        $current_allowed_ratio = "img-wh-r-".$x;
        $data[$current_allowed_ratio][] = $img2;
    }
    return $data;
}
function get_gd_info($display=FALSE)
{

    // IS GD INSTALLED AT ALL?
    if (!function_exists("gd_info"))
    {
        if ($display) echo "<br/>GD NOT INSTALLED\n";
        return FALSE;
    }

    // IF GD IS INSTALLED GET DETAILS
    $gd = gd_info();

    // IF DISPLAY IS REQUESTED, PRINT DETAILS
    if ($display)
    {
        echo "<br/>GD DETAILS:\n";
        foreach ($gd as $key => $value)
        {
            if ($value === TRUE)  $value = 'YES';
            if ($value === FALSE) $value = 'NO';
            echo "<br/>$key = $value \n";
        }
    }

    // RETURN THE VERSION NUMBER
    $gd_version = preg_replace('/[^0-9\.]/', '', $gd["GD Version"]);
    return $gd_version;
}
function create_right_size_image($img = array('fileloc'=>'','imgObj'=>'','src'=>''), $width=720,$fixDimention = 0)
{
    // IS GD HERE?
    $gdv = get_gd_info();
    if (!$gdv) return FALSE;

    // GET AN IMAGE THING
    if($img['src']){
    $img1 = $img['src'];
    $img1 = str_replace(UPLOAD_PATH,'',$img1);
    $img2 = UPLOAD_DIR.DIRECTORY_SEPARATOR.$img1;
    $img3 = explode('/',$img2);
    $imgloc = implode(DIRECTORY_SEPARATOR,$img3);
    $img['fileloc'] = $imgloc;
    }
    
    if($img['fileloc'])
    {
    //$source = imagecreatefromjpeg($img['fileloc']);
    $explode = explode(".", $img['fileloc']);
        $filetype = $explode[1];

        if ($filetype == 'jpg') {
        $source = imagecreatefromjpeg($img['fileloc']);
        } else
        if ($filetype == 'jpeg') {
        $source = imagecreatefromjpeg($img['fileloc']);
        } else
        if ($filetype == 'png') {
        $source = imagecreatefrompng($img['fileloc']);
        } else
        if ($filetype == 'gif') {
        $source = imagecreatefromgif($img['fileloc']);
        }
    }
    else if($img['imgObj'])
    $source = $img['imgObj'];

    // GET THE X AND Y DIMENSIONS
    $imageX = imagesx($source);
    $imageY = imagesy($source);
    // IF NO RESIZING IS NEEDED
    if (!empty($imageX) && $imageX <= $width && $fixDimention == 0)
    {
        return $source;
    }

    // THE WIDTH IS TOO GREAT - MUST RESIZE
    $tnailX = $width;
    $tnailY = (int) (($tnailX * $imageY) / $imageX );
    // WHICH FUNCTIONS CAN RESIZE / RESAMPLE THE IMAGE?
    if ($gdv >= 2)
    {
        // IF GD IS AT LEVEL 2 OR ABOVE
        $target = imagecreatetruecolor($tnailX, $tnailY);
    imageinterlace($target, true);
        imagecopyresampled ($target, $source, 0, 0, 0, 0, $tnailX, $tnailY, $imageX, $imageY);
    }
    else
    {
        // IF GD IS AT A LOWER REVISION LEVEL
       $target = imagecreate($tnailX, $tnailY);
       imageinterlace($target, true);
       imagecopyresized   ($target, $source, 0, 0, 0, 0, $tnailX, $tnailY, $imageX, $imageY);
       
    }
    // SHARPEN THE PIC
    $sharpenMatrix = array
    ( array( -1.2, -1, -1.2 )
    , array( -1,   20, -1 )
    , array( -1.2, -1, -1.2 )
    )
    ;
    $divisor = array_sum(array_map('array_sum', $sharpenMatrix));
    $offset  = 0;
    imageconvolution($target, $sharpenMatrix, $divisor, $offset);
    if($img['src'])
    {
    imagejpeg($target, str_replace(UPLOAD_DIR,UPLOAD_DIR.DIRECTORY_SEPARATOR.'thumb',$imgloc), 75);
        imagedestroy($target);
    return 'thumb/'.$img1;
    }
    else
    return $target ;
}
function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}
function sanitizeInput($data){
    if(is_array($data))
    {
        foreach($data as $key=>$value)
        {
            $data[$key] = sanitizeInput($value);
        }
        return $data;
    }
    else
    {
        return xss_clean($data);
    }
}
function sanitizeAllInput(){
    $_GET = sanitizeInput($_GET);
    $_POST = sanitizeInput($_POST);
    $_REQUEST = sanitizeInput($_REQUEST);
}
function xss_clean($data)
{
    // Fix &entity\n;
    $data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
    $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
    $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
    $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

    // Remove any attribute starting with "on" or xmlns
    $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

    // Remove javascript: and vbscript: protocols
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

    // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

    // Remove namespaced elements (we do not need them)
    $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

    do
    {
        // Remove really unwanted tags
        $old_data = $data;
        $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
    }
    while ($old_data !== $data);

    // we are done...
    return $data;
}
function isAssocArray(array $arr)
{
    if (array() === $arr) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
}
function create_thumbnail($img,$size){
    if(is_array($img))
    {
        $x = array();
        foreach($img as $i=>$v)
        $x[$i] = create_right_size_image(array('src'=>$v),550,1);
        return $x;
    }
    else
    {
        return create_right_size_image(array('src'=>$img),550,1);
    }
}
function getRandomString($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}
function validate_event_form(){
    global $eventParticipationData,$transactionfields,$eventdata,$tickettemp,$ticketanswers;
    $response = array();
    $response['status'] = 200;
        
    $eventParticipationData              = $_POST['event']['ticketing'];

    $transactionfields['transactionid']  = generate_id("transaction");
    $transactionfields['productid']      = $_POST['event']['productid'];
    $transactionfields['producturl']     = $_POST['event']['producturl'];
    $transactionfields['producttype']    = 'event'; //$_POST['event']['producttype'];
    
    $eventdata = event_get_details($transactionfields['productid'],1);
    if(!($eventdata))
    {
        $response['status'] = 406;
        $response['error']['eventid'] = 'Please contact the administrator';
    }
    
    $tickettemp = $_POST['event']['ticketing'];
    //$_POST['event']['ticketing'] = array();
    $tickets = array();
    $users = array();
    $participantindex = 0;
    foreach($tickettemp as $ticketcode=>$ticketdata1)
    {
        foreach($ticketdata1 as $ticketdata)
        {
            foreach($ticketdata as $fieldname=>$tktdata)
            {
                if($fieldname=='name' && strlen(trim($tktdata)) == 0)
                {
                    $response['status'] = 406;
                    $response['error']['participants'][$participantindex]['name'] = 'Please provide participant\'s name';
                }
                else if($fieldname=='email' && !(get_standard_email($tktdata)))
                {
                    $response['status'] = 406;
                    $response['error']['participants'][$participantindex]['email'] = 'Please provide a valid email';
                }
                else if($fieldname=='mobile' && !(get_standard_mobile($tktdata)))
                {
                    $response['status'] = 406;
                    $response['error']['participants'][$participantindex]['mobile'] = 'Please provide a valid mobile number';
                }
                else if($fieldname=='dob' && !preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $tktdata))
                {
                    $response['status'] = 406;
                    $response['error']['participants'][$participantindex]['dob'] = 'Please provide date of birth';
                }
                else if(strlen(trim($tktdata)) == 0)
                {
                    $response['status'] = 406;
                    $response['error']['participants'][$participantindex][$fieldname] = 'Required field';
                }
            }
            $participantindex++;
        }
    }
    
    $ticketanswers = $_POST['event']['answers'];
    $_POST['event']['answers'] = array();
    $participantindex = 0;
    foreach($ticketanswers as $ticketcode=>$ticketdata1)
    {
        foreach($ticketdata1 as $ticketdata)
        {
            foreach($ticketdata as $questionindex=>$answer)
            {
                if(strlen(trim($answer)) == 0)
                {
                    $response['status'] = 406;
                    $response['error']['answers'][$participantindex][$questionindex] = 'Required Field';
                }
            }
            $participantindex++;
        }
    }
    if($eventdata['tickettype'] == 'free')
    $eventdata['isAllowMultiple'] = 0;
    else
    $eventdata['isAllowMultiple'] = 1;
    if($response['status'] != 406 && $eventdata['isAllowMultiple'] ==0)
    {   
        $participantindex = 0;
        foreach($tickettemp as $ticketcode=>$ticketdata1)
        {
            foreach($ticketdata1 as $ticketdata)
            {
                $ticket = array();
                $ticket['tkteventid'] = $transactionfields['productid'];
                $ticket['tktemail'] = get_standard_email($ticketdata['email']);
                $usertickets = ticket_get_list(array('tkteventid'=>$ticket['tkteventid'],'tktemail'=>$ticket['tktemail']));
                if(!empty($usertickets))
                {
                    $response['status'] = 406;
		    if($eventdata['source'] == 'aggregated')
                    $response['error']['existingemailids'][$participantindex]['email'] = 'We already have recieved interest from email id : '.$ticket['tktemail'].' and his interest been shared to the event organiser';
		    else
                    $response['error']['existingemailids'][$participantindex]['email'] = 'Ticket has already been bought for this email id : '.$ticket['tktemail'].'. Please check in mailbox or use a new email id to buy ticket';
                }
                $participantindex++;
            }
        }
    }
    return $response;
}
function breadcrumbs($breadcrumbs,$schema = 1,$navigation = 0){
    if($breadcrumbs)
    {
        if($schema == 1)
        {
            $breadcrumbarray = array();
            $breadcrumbarray['@context'] = "http://schema.org";
            $breadcrumbarray['@type'] = "BreadcrumbList";
            $breadcrumbarray['itemListElement'] = array();
        }
        if($navigation == 1)
        {
            $navigationHTML = '';
        }
        foreach($breadcrumbs as $pos=>$breadcrumb)
        {
            $breadcrumbelement = array();
            if($schema == 1)
            {
                $breadcrumbelement['@type'] = 'ListItem';
                $breadcrumbelement['position'] = $pos+1;
                $breadcrumbelement['item']['@id'] = $breadcrumb['url'];
                $breadcrumbelement['item']['name'] = $breadcrumb['name'];
                if($breadcrumb['image'])
                $breadcrumbelement['item']['image'] = $breadcrumb['image'];
                $breadcrumbarray['itemListElement'][] = $breadcrumbelement;
            }
            if($navigation == 1)
            {
                $class = '';
                if($_SERVER['PHP_SELF_URL'] == $breadcrumb['url'])
                $class = 'active';
                $navigationHTML .= "&nbsp;<a class='".$class."' href='".$breadcrumb['url']."'>".$breadcrumb['name']."</a>&nbsp;>>";
            }
        }
        if($navigationHTML)
        $navigationHTML = 'You are here : '. substr($navigationHTML,0,strlen($navigationHTML)-2);
        return array('schema'=>$breadcrumbarray,'navigation'=>$navigationHTML);
    }
    else
    {
        return false;
    }
}
function extractKeyWords($string,$limit = 0,$minFrequency = 0) {
    $string = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $string);
    mb_internal_encoding('UTF-8');
    $stopwords = array('i','a','about','an','and','are','as','at','be','by','com','de','en','for','from','how','in','is','it','la','of','on','or','that','the','this','to','was','what','when','where','who','will','with','und','the','www','also');
    $string = preg_replace('/[\pP]/u', '', trim(preg_replace('/\s+/iu', ' ', mb_strtolower(strip_tags(str_replace('<',' <',nl2br($string)))))));
    $matchWords = array_filter(explode(' ',$string) , function ($item) use ($stopwords) { return !($item == '' || in_array($item, $stopwords) || mb_strlen($item) <= 2 || is_numeric($item));});
    $wordCountArr = array_count_values($matchWords);
    arsort($wordCountArr);
    //return array_keys(array_slice($wordCountArr, 0, $limit));
    if($limit)
    return array_keys(array_slice($wordCountArr, 0, $limit));
    else if ($minFrequency)
    {
        $temp = $wordCountArr;
        $wordCountArr = array();
        foreach($temp as $key=>$val)
        {
            if($val >= $minFrequency)
            $wordCountArr[$key] = $val;
        }
        return array_keys($wordCountArr);
    }
    else
    return array_keys($wordCountArr);
}
function chop_string($in,$length = 0){
    if($length)
    return  ((strlen($in) > $length )? substr($in,0,($length - 3))."..." : $in);
    else
    return $in;
}
function generate_event_ticket($txnid,$superadmin = 0,$sendMail = 1){
    global $PSData;
    $txndetail = transaction_get_details($txnid);
    if(!($txndetail['userid'] == $PSData['user']['profile_id'] || $txndetail['userid'] == 'unknown') && $superadmin == 0)
    {
            return -2;
    }
    $eventdetail = event_get_details($txndetail['productid'],1);
    //$creatordetail = user_profile(array('profile_id'=>$txndetail['userid']));
    $ticketdetails = unserialize($txndetail['productdetails']);
    $ticketarray = ticket_get_list(array('tkttransactionid'=>$txnid));
    if($eventdetail && $txndetail['status'] == 'complete|success')
    {
            $ticketnewarray = array();
            foreach($ticketarray as $ticketdata)
            {
                    $ticketnewarray[$ticketdata['tktcode']][] = $ticketdata;
            }
            $ticketdetails['ticketing'] = $ticketnewarray;

            //print_array(array($txndetail,$eventdetail,$ticketnewarray));

            $eventstartdate = $eventdetail['datestart'];
            $eventenddate = $eventdetail['dateend'];
            if($eventenddate == $eventstartdate)
            {
                    $eventdatetxt = date('jS M,Y',strtotime($eventstartdate));
                    $eventdatedetailedtxt = date('l, F j Y h:i a',strtotime($eventstartdate));
            }
            else
            {
                    $eventdatetxt = date('jS M,Y',strtotime($eventstartdate)).' - '.date('jS M,Y',strtotime($eventenddate));
                    $eventdatedetailedtxt = date('l, F j Y h:i a',strtotime($eventstartdate)).' To '.date('l, F j Y h:i a',strtotime($eventenddate));
            }
            $eventlocationtext = $eventdetail['location'];
            if(strlen($eventlocationtext)>40)
            $eventsubtitle = $eventdatetxt.'<br>'.$eventlocationtext;
            else
            $eventsubtitle = $eventdatetxt.$eventlocationtext;
            $title2 = 'Time &amp; Venue';
            $tables = '';
            if($eventdetail['description'])
        {
            /*preg_match_all('(<table.*?>(.*?)<\/table>)', $eventdetail['description'], $matches);
            if($matches && $matches[0][0])
            {
            $tables .= str_replace('<table','<table width=100% border=1 cellspacing=0 cellpadding=5 ',$matches[0][0]).'</table>';
            $title2 .= ' And Description';
            }*/
            //preg_match_all('/(<table[^>]*>(?:.|\n)*?<\/table>)/', $eventdetail['description'], $matches);
            preg_match_all('(<table.*?>(.*?)<\/table>)', $eventdetail['description'], $matches);
            if($matches)
            {
                foreach($matches[0] as $key=>$data)
                {
                    $tables .= str_replace('<table','<br /><table style="margin-left:2%;margin-right:2%;width:96%;margin-top:5px;margin-bottom:5px;" border=1 cellspacing=0 cellpadding=5 ',$matches[0][$key]);
                }
                $title2 .= ' And Description';
            }
        }
            $ticketextra = '';

            $html = file_get_contents(CORE_DIR.'/template/print/eventticketcomplete.tpl');
            $html = str_replace('<!--##eventtitle##-->',$eventdetail['eventname'],$html);
            $html = str_replace('<!--##eventsubtitle##-->',$eventsubtitle,$html);
            $html = str_replace('<!--##transactionid##-->',$txndetail['transactionid'],$html);
            $html = str_replace('<!--##transactionamount##-->',$txndetail['amount'],$html);
            $html = str_replace('<!--##eventvenue##-->',$eventdetail['address'],$html);
            $html = str_replace('<!--##eventtimedetailed##-->',$eventdatedetailedtxt,$html);
            $html = str_replace('<!--##eventtables##-->',$tables,$html);
            $html = str_replace('<!--##eventtitle2##-->',$title2,$html);
            //$html = str_replace('<!--##eventcreator##-->',ucwords($creatordetail['fname'].' '.$creatordetail['fname']),$html);
            $html = str_replace('<!--##eventorganiser##-->',$eventdetail['organiser']['name'],$html);
            $html = str_replace('<!--##eventorganiseremail##-->',$eventdetail['organiser']['email'],$html);

            $alltickethtml = '';
    
            $tickettemplate = file_get_contents(CORE_DIR.'/template/print/eventticketindividual.tpl');
            //print_array($ticketdetails);
            //print_array($eventdetail);
            $ticketcategoriesdetail = array();
            foreach($eventdetail['ticketcategories'] as $temp)
            {
                    $ticketcategoriesdetail[$temp['name']] = $temp;
            }
        $ccusers = array();
            foreach($ticketdetails['ticketing'] as $tickettype=>$ticket)
            {
                    foreach($ticket as $index=>$ticketdata)
                    {
                            if($ticketdata['tktbibno'])
                            $ticketextra = '<br />Bib No.&nbsp;<strong>'.$ticketdata['tktbibno'].'</strong>';
                $ccusers[] = array($ticketdata['tktemail'],$ticketdata['tktname']);
                            $temp = $tickettemplate;
                            $temp = str_replace('<!--##ticketuser##-->',$ticketdata['tktname'].' ('.$ticketdata['tktemail'].')',$temp);
                            $temp = str_replace('<!--##ticketnumber##-->',$ticketdata['tktid'],$temp);
                            $temp = str_replace('<!--##tickettype##-->',$eventdetail['ticketcategories'][$tickettype]['name'],$temp);
                            $temp = str_replace('<!--##ticketprice##-->',$eventdetail['ticketcategories'][$tickettype]['price'],$temp);
                            $temp = str_replace('<!--##ticketextra##-->',$ticketextra,$temp);
                            $barcodedata = urlencode('Event:'.$eventdetail['eventname']);
                            $barcodedata .= '%0A'.urlencode('Event Link:'.ROOT_PATH.'event/'.$eventdetail['seopath']);
                            $barcodedata .= '%0A'.urlencode('Attendee:'.$ticketdata['tktname']);
                            $barcodedata .= '%0A'.urlencode('Ticket Number:'.$ticketdata['tktid']);
                            $barcodedata .= '%0A'.urlencode('Ticket Type:'.$tickettype);
                            $barcodedata .= '%0A'.urlencode('Ticket Price:'.$ticketcategoriesdetail[$tickettype]['price']);
                            $barcodedata .= '%0A'.urlencode('Order Id:'.$txndetail['transactionid']);
                            $barcodedataimg = 'https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl='.$barcodedata.'&choe=UTF-8';
                            $temp = str_replace('<!--##ticketbarcode##-->',$barcodedataimg,$temp);
                            $alltickethtml .= $temp;
                    }
            }
            $html = str_replace('<!--##transactiontickets##-->',$alltickethtml,$html);
            $html = str_replace('<!--##root_path##-->',ROOT_PATH,$html);
        
        $filename = get_alphanumeric($txndetail['userid'].'-'.$txndetail['transactionid']).".html";
        $file_to_save = UPLOAD_DIR.DIRECTORY_SEPARATOR.'tickets'.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.$filename;
            file_put_contents($file_to_save, $html); 
        /*
            set_include_path(get_include_path() . PATH_SEPARATOR . CORE_DIR.DIRECTORY_SEPARATOR."dompdf");
            
            require_once CORE_DIR.DIRECTORY_SEPARATOR."dompdf".DIRECTORY_SEPARATOR."dompdf_config.inc.php";

            $dompdf = new DOMPDF();

            $dompdf->load_html($html);
            $dompdf->render();

            $filename = get_alphanumeric($eventdetail['eventname'].'-'.$txnid).".pdf";

            $output = $dompdf->output();
            $file_to_save = UPLOAD_DIR.DIRECTORY_SEPARATOR.'tickets'.DIRECTORY_SEPARATOR.'pdf'.DIRECTORY_SEPARATOR.$filename;
            file_put_contents($file_to_save, $output); 
            $output = $dompdf->output();
            $dompdf->stream(get_alphanumeric($eventdetail['eventname'].'-'.$txnid).".pdf");
            */
	    
        if($sendMail == 1)
        {
		if($txndetail['userid'] == 'unknown')
		{
		$productdetails = (unserialize($txndetail['productdetails']));
		$key = key($productdetails['ticketing']);
		$email_id = $productdetails['ticketing'][$key][0]['email'];
		}
		else
		$email_id = '';
            mailer(array(
                "profile_id" => ($txndetail['userid'] != 'unknown')?$txndetail['userid']:0,
		"email_id" => $email_id,
		"ccusers"=> $ccusers,
                "eventdetail" => $eventdetail,
                "txndetail" => $txndetail,
                "tickets" => $ticketnewarray,
                "html"=>$html
            ), 'ticketonmail',NULL,'blank.tpl');
        }
        if($superadmin)
        return UPLOAD_PATH.'tickets/html/'.$filename;
        else
        return 1;
    
    }
    else if($txndetail['status'] == 'complete|failure')
    {
            return 0;
    }
    else if(empty($eventdetail))
    {
            return -1;
    }
    else
    {
            return -1;
    }
}
function curlCustom($type,$dataExtra=array(),$endpoint = '',$path='',$headersExtra=array()){
    $query=http_build_query($dataExtra);
        if($endpoint == 'googleapis')
        {
    $url = "https://www.googleapis.com";
        $path=(($path == "")?"/oauth2/v3/token":$path);
        $headers['Accept']='application/json';
        }
        else
        $url = "";
        if($path == '')
        return;
        $url = $url.$path;
        $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    if($type=="POST")
    {
    curl_setopt($ch, CURLOPT_POST, count($dataExtra));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
    }
    else if($type=="GET")
    {
    $url=$url."?".$query;
    curl_setopt($ch, CURLOPT_URL, $url);    
    }
    //$headers['Accept']='application/json';
    $headers=array_merge($headers,$headersExtra);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $a = curl_exec($ch);
        if(!empty($headers['Accept']))
    return json_decode($a, true);
        else
        return $a;
}
function googleapisaccesstoken($fields=array(),$cacheType = false,$cacheMeta = array('expiry'=>30)){
    if($cacheType == false)
    return preprocessApi('googleapisaccesstoken', $fields , $cacheType,$cacheMeta);
    
    /*
    step 1
    https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/analytics.readonly&redirect_uri=https://passionstreet.in&response_type=code&client_id=702907059749-tb2dphgsgj1u29ch63d72bk0grcu3o3k.apps.googleusercontent.com&access_type=offline
    4/LMzlZv1cqrbTDAWL5T7JFwPtW-XsNgujCGHHPrlASPQ
    
    step 2
    $a = curlCustom('POST',array('code'=>'4/LMzlZv1cqrbTDAWL5T7JFwPtW-XsNgujCGHHPrlASPQ','scope'=>'','grant_type'=>'authorization_code',"client_id"=>"702907059749-tb2dphgsgj1u29ch63d72bk0grcu3o3k.apps.googleusercontent.com","client_secret"=>"-OE0Bsq0rXe2kd4l0mW3vM_V","redirect_uri"=>"https://passionstreet.in"));
    print_array($a);
    
    Array
    (
        [access_token] => ya29.Ci_MA8er46qJILoQHxCy4Ky__1nHzQzAmeMgUItkJTEOy1VnE6rq0uKl__JSIL3D-w
        [token_type] => Bearer
        [expires_in] => 3600
        [refresh_token] => 1/eKuOSj84cb6WLLKEp8DUd-VYH2MjzXa_yF_OLLOM6L0
    )
    
    Step 3
    $accessTokenobj = curlCustom('POST',array('grant_type'=>'refresh_token','refresh_token'=>'1/eKuOSj84cb6WLLKEp8DUd-VYH2MjzXa_yF_OLLOM6L0','client_id'=>'702907059749-tb2dphgsgj1u29ch63d72bk0grcu3o3k.apps.googleusercontent.com','client_secret'=>'-OE0Bsq0rXe2kd4l0mW3vM_V','redirect_uri'=>'http://indiatimes.com'));
    $accessToken=$accessTokenobj['access_token'];
    print_array($accessTokenobj);
    Array
    (
        [access_token] => ya29.Ci_MA_KOM6R7gGh3o7oZK-DZig2dx1amLHKxyssyCekKyqDg0XTBiFuXYencH27_CQ
        [token_type] => Bearer
        [expires_in] => 3600
    )
    
    */
    $accessTokenobj = curlCustom('POST',array('grant_type'=>'refresh_token','refresh_token'=>'1/eKuOSj84cb6WLLKEp8DUd-VYH2MjzXa_yF_OLLOM6L0','client_id'=>'702907059749-tb2dphgsgj1u29ch63d72bk0grcu3o3k.apps.googleusercontent.com','client_secret'=>'-OE0Bsq0rXe2kd4l0mW3vM_V','redirect_uri'=>'https://passionstreet.in/'),'googleapis');
    return $accessTokenobj['access_token'];
}
function getpageviews($page,$startDate = "",$cacheType = false,$cacheMeta = array('expiry'=>5)){
    if($startDate == "")
    return "";
    if($cacheType == false)
    return preprocessApi('getpageviews', $page ,$startDate, $cacheType,$cacheMeta);
    
    $accesstoken = googleapisaccesstoken();
    // $result = curlCustom('GET',array('ids'=>'ga:118818372','start-date'=>$startDate,'end-date'=>'today','access_token'=>$accesstoken,'dimensions'=>'ga:pagePath','metrics'=>'ga:pageviews','filters'=>'ga:pagePath==/'.str_replace('https://passionstreet.in/','',$page),'max-results'=>1),'googleapis','/analytics/v3/data/ga');
    $result = curlCustom('GET',array('ids'=>'ga:118818372','start-date'=>$startDate,'end-date'=>'today','access_token'=>$accesstoken,'dimensions'=>'ga:pagePath','metrics'=>'ga:pageviews','filters'=>'ga:pagePath=~/'.str_replace('https://passionstreet.in/','',$page).'*','max-results'=>1),'googleapis','/analytics/v3/data/ga');
    if($result['totalsForAllResults']['ga:pageviews'])
    return $result['totalsForAllResults']['ga:pageviews'];
    else
    return '';
}
function getUrlMeta($url,$minified = 1) // $raw - enable for raw display
{ //http://php.net/manual/en/function.get-meta-tags.php
    $result = false;
   
    $contents = getUrlContents($url);
    $metalist = array();
    if (isset($contents) && is_string($contents))
    {
        $title = null;
        $metalist = null;
        $metaProperties = null;
       
        preg_match('/<title>([^>]*)<\/title>/si', $contents, $match );

        if (isset($match) && is_array($match) && count($match) > 0)
        {
            $title = strip_tags($match[1]);
            $metalist['title'] = trim($title);
        }
       
        preg_match_all('/<[\s]*meta[\s]*(name|property|itemprop)=["|\']?' . '([^>"\']*)["|\']?[\s]*' . 'content=["|\']?([^>"|\']*)["|\']?[\s]*[\/]?[\s]*>/si', $contents, $match);
        if (isset($match) && is_array($match) && count($match) == 4)
        {
            $originals = $match[0];
            $names = $match[2];
            $values = $match[3];
           
            if (count($originals) == count($names) && count($names) == count($values))
            {
                for ($i=0, $limiti=count($names); $i < $limiti; $i++)
                {
                        $metalist[strtolower($names[$i])] = trim($values[$i]);
                }
            }
        }
        /*preg_match_all('/<[\s]*link[\s]*(rel)=["|\']?' . '([^>"\']*)["|\']?[\s]*' . 'href=["|\']?([^>"\']*)["|\']?[\s]*[\/]?[\s]*>/si', $contents, $match);
        if (isset($match) && is_array($match) && count($match) == 4)
        {
            $originals = $match[0];
            $names = $match[2];
            $values = $match[3];
           
            if (count($originals) == count($names) && count($names) == count($values))
            {
                for ($i=0, $limiti=count($names); $i < $limiti; $i++)
                {
                        $metaTags[strtolower($names[$i])] = trim($values[$i]);
                }
            }
        }*/
    }
    if($minified)
    {
        $finalmetalist = array();
        $finalmetalist['title'] = $metalist['title'];
        if($metalist['og:image'])
        $finalmetalist['pageimage'] = ROOT_PATH.'ps_proxy.php?type=image&psurl='.$metalist['og:image'];
        else if($metalist['twitter:image'])
        $finalmetalist['pageimage'] = ROOT_PATH.'ps_proxy.php?type=image&psurl='.$metalist['twitter:image'];
        else if($metalist['image'])
        $finalmetalist['pageimage'] = ROOT_PATH.'ps_proxy.php?type=image&psurl='.$metalist['image'];
        else
        $finalmetalist['pageimage'] = '';
        if(empty($metalist['description']))
        {
            if($metalist['og:description']!==undefined && $metalist['og:description']!='')
            $finalmetalist['description'] = $metalist['og:description'];
            else if($metalist['twitter:description']!==undefined  && $metalist['twitter:description']!='')
            $finalmetalist['description'] = $metalist['twitter:description'];
            else
            $finalmetalist['description'] = '';
        }
        else
        $finalmetalist['description'] = $metalist['description'];
    }
    if($metalist['og:video:url']!==undefined)
    {
        //if($url.search('youtube.com')>-1)
        if($metalist['og:site_name']=='YouTube')
        {
            $videoid = youtube_parser($url);
            $metalist['ytvideoid'] = $finalmetalist['ytvideoid'] = $videoid;
            $metalist['videoembed'] = $finalmetalist['videoembed'] = "https://www.youtube.com/embed/" . $videoid . "?autoplay=1&autohide=1";
        }
    }
    if($minified)
    return $finalmetalist;
    else
    return $metaTags;
}

function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0)
{
    $result = false;
   
    $contents = @file_get_contents($url);
   
    // Check if we need to go somewhere else
   
    if (isset($contents) && is_string($contents))
    {
        preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);
       
        if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1)
        {
            if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections)
            {
                return getUrlContents($match[1][0], $maximumRedirections, ++$currentRedirection);
            }
           
            $result = false;
        }
        else
        {
            $result = $contents;
        }
    }
   
    return $contents;
}
function youtube_parser($url){
    $regExp = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
    preg_match($regExp,$url,$match);
    return ($match && strlen($match[7])==11 )? $match[7] : false;
}
function geteventprocessingfeev1($ticketcost,$eventtypebasedonfees){
    global $PSParams;
    $processfeerelative = $PSParams[$PSParams['psproducttype'].'default']['processingfee']['relative'];
    $processfeeabsolute = $PSParams[$PSParams['psproducttype'].'default']['processingfee']['absolute'];
    $processfeerelative = 1.5; //override for event event-7358358790 which is running at the time of running code
    $processfeeabsolute = 12; //override for event event-7358358790 which is running at the time of running code
    $processfeeflagbasedonevent = $PSParams[$PSParams['psproducttype'].'default']['typesbasedonfees'][$eventtypebasedonfees]['processingfee'];
    if($processfeeflagbasedonevent && $ticketcost)
    return round(($ticketcost*$processfeerelative/100) + $processfeeabsolute,2);
    else
    return 0;
}
function geteventprocessingfee($ticketcost,$eventtypebasedonfees){
    global $PSParams;
    /*
    if($_POST['eventId'] == 'event-7358358790') //this event was already running with previous processing cost ie 12rs + 1.5%
    return geteventprocessingfeev1($ticketcost,$eventtypebasedonfees);
    */
    //$x * $PSParams[$PSParams['psproducttype'].'default']['processingfee']['relative'] / 100 = $PSParams[$PSParams['psproducttype'].'default']['processingfee']['absolute'];
    //$x = $PSParams[$PSParams['psproducttype'].'default']['processingfee']['absolute'] * 100 / $PSParams[$PSParams['psproducttype'].'default']['processingfee']['relative'];
    if(($PSParams['psproducttype'] == 'event' && $ticketcost > 850) || ($PSParams['psproducttype'] == 'tour' && $ticketcost > 2000))
    {
    $processfeerelative = $PSParams[$PSParams['psproducttype'].'default']['processingfee']['relative'];
    $processfeeabsolute = 0;
    }
    else
    {
    $processfeerelative = 0;
    $processfeeabsolute = $PSParams[$PSParams['psproducttype'].'default']['processingfee']['absolute'];
    }
    if($event['creator'] == 'profile-4068255186' && $event['event_id'] == 'event-2975884678')
    $processfeerelative = 3;
    $processfeeflagbasedonevent = $PSParams[$PSParams['psproducttype'].'default']['typesbasedonfees'][$eventtypebasedonfees]['processingfee'];
    if($processfeeflagbasedonevent && $ticketcost)
    return round(($ticketcost*$processfeerelative/100) + $processfeeabsolute,2);
    else
    return 0;
}
function geteventpaymentgatewayfee($ticketcost,$eventtypebasedonfees){
    global $PSParams;
    $paymentfeerelative = $PSParams[$PSParams['psproducttype'].'default']['paymentgatewayfee'][PAYMENTGATEWAY]['relative'];
    $paymentfeeabsolute = $PSParams[$PSParams['psproducttype'].'default']['paymentgatewayfee'][PAYMENTGATEWAY]['absolute'];
    $paymentfeeflagbasedonevent = $PSParams[$PSParams['psproducttype'].'default']['typesbasedonfees'][$eventtypebasedonfees]['gatewayfee'];
    if($paymentfeeflagbasedonevent && $ticketcost)
    return round(($ticketcost*$paymentfeerelative/100) + $paymentfeeabsolute,2);
    else
    return 0;    
};
function geteventtaxfee($conveniencefee){
    global $PSParams;
    $taxfeerelative = $PSParams[$PSParams['psproducttype'].'default']['servicetax']['relative'];
    $taxfeeabsolute = $PSParams[$PSParams['psproducttype'].'default']['servicetax']['absolute'];
    return round(($conveniencefee*$taxfeerelative/100) + $taxfeeabsolute,2);
};
function geteventticketprice($ticketcost,$eventtypebasedonfees = 3,$feepaymentoptions=2){ // $eventtypebasedonfees = (1-free,2-nonprofit,3-paid) //feepaymentoptions = (1-buyerall,2-organiserall,3-distributed)
    global $PSParams;
    $ticketcost = (int)$ticketcost;
    $ticketcostorg = $ticketcost;
    $data['organiserpaysps'] = 0;
    $data['userpaysextra'] = 0;
    $data['processingfee'] = geteventprocessingfee($ticketcost,$eventtypebasedonfees);
    //$ticketcost = $ticketcost + $data['processingfee']; //paymentgateway charge is being calculated on ticket price , the difference will be borne by PS as per saumitra
    $data['paymentgatewayfee'] = geteventpaymentgatewayfee($ticketcost + $data['processingfee'],$eventtypebasedonfees);
    $data['totaltaxfee'] = geteventtaxfee($data['processingfee'] + $data['paymentgatewayfee']);
    
    $currentFeePaymentOption = $PSParams[$PSParams['psproducttype'].'default']['feepaymentoptions'][$feepaymentoptions];
    
    if($currentFeePaymentOption['processingfeepaidby'] == 'buyer')
    $data['userpaysextra'] = $data['userpaysextra'] + $data['processingfee'];
    else if($currentFeePaymentOption['processingfeepaidby'] == 'organiser')
    $data['organiserpaysps'] = $data['organiserpaysps'] + $data['processingfee'];  //
    
    if($currentFeePaymentOption['gatewayfeepaidby'] == 'buyer')
    $data['userpaysextra'] = $data['userpaysextra'] + $data['paymentgatewayfee'];
    else if($currentFeePaymentOption['gatewayfeepaidby'] == 'organiser')
    $data['organiserpaysps'] = $data['organiserpaysps'] + $data['paymentgatewayfee'];
    
    //if($currentFeePaymentOption['taxpaidby'] == 'buyer')
    //$data['userpaysextra'] = $data['userpaysextra'] + $data['taxfee'];
    //else if($currentFeePaymentOption['taxpaidby'] == 'organiser')
    //$data['organiserpaysps'] = $data['organiserpaysps'] + $data['taxfee'];  //
    $data['ticketcostonps'] = $ticketcost +  $data['processingfee'] + $data['paymentgatewayfee'] + $data['totaltaxfee'];
    
    $data['organisertax'] = geteventtaxfee($data['organiserpaysps']);
    $data['organiserpaysps'] = $data['organiserpaysps'] + $data['organisertax'];
    
    $data['usertax'] = geteventtaxfee($data['userpaysextra']);
    $data['userpaysextra'] = $data['userpaysextra'] + $data['usertax'];
    
    $data['displaycost'] = $ticketcost + $data['userpaysextra'];
    $data['organiserrevenue'] = $ticketcost - $data['organiserpaysps'];
    $data['initialcost'] = $ticketcost;
    //print_array($data);
    //$data['totalcost'] = ceil($data['ticketcost'] +  $data['processingfee'] + $data['paymentfee'] + $data['taxfee']);
    return $data;
};
function geteventticketprice_v1($ticketcost,$eventtypebasedonfees = 3,$feepaymentoptions=2){ // $eventtypebasedonfees = (1-free,2-nonprofit,3-paid) //feepaymentoptions = (1-buyerall,2-organiserall,3-distributed)
    global $PSParams;
    $ticketcost = (int)$ticketcost;
    $data['organiserpaysps'] = 0;
    $data['userpaysextra'] = 0;
    $data['processingfee'] = geteventprocessingfee($ticketcost,$eventtypebasedonfees);
    //$ticketcost = $ticketcost + $data['processingfee']; //paymentgateway charge is being calculated on ticket price , the difference will be borne by PS as per saumitra
    $data['paymentgatewayfee'] = geteventpaymentgatewayfee($ticketcost,$eventtypebasedonfees);
    $data['totaltaxfee'] = geteventtaxfee($data['processingfee'] + $data['paymentgatewayfee']);
    
    $currentFeePaymentOption = $PSParams[$PSParams['psproducttype'].'default']['feepaymentoptions'][$feepaymentoptions];
    
    if($currentFeePaymentOption['processingfeepaidby'] == 'buyer')
    $data['userpaysextra'] = $data['userpaysextra'] + $data['processingfee'];
    else if($currentFeePaymentOption['processingfeepaidby'] == 'organiser')
    $data['organiserpaysps'] = $data['organiserpaysps'] + $data['processingfee'];  //
    
    if($currentFeePaymentOption['gatewayfeepaidby'] == 'buyer')
    $data['userpaysextra'] = $data['userpaysextra'] + $data['paymentgatewayfee'];
    else if($currentFeePaymentOption['gatewayfeepaidby'] == 'organiser')
    $data['organiserpaysps'] = $data['organiserpaysps'] + $data['paymentgatewayfee'];
    
    //if($currentFeePaymentOption['taxpaidby'] == 'buyer')
    //$data['userpaysextra'] = $data['userpaysextra'] + $data['taxfee'];
    //else if($currentFeePaymentOption['taxpaidby'] == 'organiser')
    //$data['organiserpaysps'] = $data['organiserpaysps'] + $data['taxfee'];  //
    $data['ticketcostonps'] = $ticketcost +  $data['processingfee'] + $data['paymentgatewayfee'] + $data['totaltaxfee'];
    
    $data['organisertax'] = geteventtaxfee($data['organiserpaysps']);
    $data['organiserpaysps'] = $data['organiserpaysps'] + $data['organisertax'];
    
    $data['usertax'] = geteventtaxfee($data['userpaysextra']);
    $data['userpaysextra'] = $data['userpaysextra'] + $data['usertax'];
    
    $data['displaycost'] = $ticketcost + $data['userpaysextra'];
    $data['organiserrevenue'] = $ticketcost - $data['organiserpaysps'];
    $data['initialcost'] = $ticketcost;
    //print_array($data);
    //$data['totalcost'] = ceil($data['ticketcost'] +  $data['processingfee'] + $data['paymentfee'] + $data['taxfee']);
    return $data;
};
function parse_urls_from_text($string){
    //$string = "this is my friend's website http://example.com I think it is cool, but this is cooler http://www.memelpower.com :)";
    $regex = '/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i';
    preg_match_all($regex, $string, $matches);
    $urls = $matches[0];
    return $urls;
}
function isAdminLoggedIn(){
    @session_start();
    if($_SESSION['user']['profile_id'] == 'profile_1456704072_694534' || $_SESSION['user']['profile_id'] == 'profile_1456728612_617562'  || $_SESSION['user']['profile_id'] == 'profile_1457162238_911787'  || $_SESSION['user']['profile_id'] == 'profile-6968044737')
    return true;
    else
    return false;
}

function getCommunityById($groupId = 0) {
    global $connection;
    $community = $connection->fetchAssoc('SELECT groupId, name,description, photo FROM groups WHERE groupId = ?', array($groupId));
    
    return $community;
}
function returnBadWords($string){
	$badWords = array("adult","anal","anus","bitch","boobs","butt","clit","cunts","fuck","erotic","gangbang","gaysex","masturbate","nigga","orgasm","phonesex","porn","pussy","sex","shit","tits","xxx");
	$matches = array();
	$matchFound = preg_match_all(
			"/\b(" . implode($badWords,"|") . ")\b/i", 
			$string, 
			$matches
		      );
	$badwords = array();	      
	if ($matchFound) {
	  $words = array_unique($matches[0]);
	  foreach($words as $word) {
	    $badwords[] = $word;
	  }
	}
	return $badwords;
}
function strip_only($str, $tags, $stripContent = true) {
    $content = '';
    if(!is_array($tags)) {
        $tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
        if(end($tags) == '') array_pop($tags);
    }
    foreach($tags as $tag) {
        if ($stripContent)
             $content = '(.+</'.$tag.'[^>]*>|)';
         $str = preg_replace('#</?'.$tag.'[^>]*>'.$content.'#is', '', $str);
    }
    return $str;
}
?>