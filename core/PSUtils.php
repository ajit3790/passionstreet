<?php
/**
* Utility class
*/
class PSUtils
{
    /**
     * Generate a valid V4 UUID
     * @return string
     */
    public static function uuidV4()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Generate 16 Digit unique number
     */
    public static function uuid()
    {
        return sprintf('%2d%2d', 
            mt_rand(10000000000000, 99999999999999), 
            mt_rand(11, 99)
        );
    }
    /**
     * get all passion category
     */
    public static function passionCategory($categoryName = '')
    {        
        $categories = array(
            'rafting' => array(
                'name' => 'Rafting',
                'icon' => 'rafting.jpg'
            ),
            'culinary' => array(
                'name' => 'Food Safari & Culinary',
                'icon' => 'culinary.jpg'
            ),
            'painting' => array(
                'name' => 'Painting',
                'icon' => 'painting.jpg'
            ),
            'photography' => array(
                'name' => 'Photography',
                'icon' => 'photography.jpg'
            ),
            'music' => array(
                'name' => 'Music',
                'icon' => 'music.jpg'
            ),
            'voyage' => array(
                'name' => 'Voyage',
                'icon' => 'voyage.jpg'
            ),
            'cycling' => array(
                'name' => 'Cycling',
                'icon' => 'cycling.jpg'
            ),
            'trekkingnmountaineering' => array(
                'name' => 'Trekking',
                'icon' => 'treknmount.jpg'
            ),
            'yoga' => array(
                'name' => 'Yoga',
                'icon' => 'yoga.jpg'
            ),                       
            'motorbiking' => array(
                'name' => 'Motorbiking',
                'icon' => 'motorbiking.jpg'
            ),
            'dancing' => array(
                'name' => 'Dancing',
                'icon' => 'dancing.jpg'
            ),
            'running' => array(
                'name' => 'Running',
                'icon' => 'running.jpg'
            )
        );
        if ($categoryName != '') {
            $category = isset($categories[$categoryName]) ? $categories[$categoryName] : array();
            return $category;
        }
        return $categories;

    }

    function workoutCategory () {
        return array(
            'trekkingnmountaineering' => array(
                'name' => 'Trekking',
                'icon' => 'treknmount.jpg'
            ),
            'running' => array(
                'name' => 'Running',
                'icon' => 'running.jpg'
            ),
            'cycling' => array(
                'name' => 'Cycling',
                'icon' => 'cycling.jpg'
            )
        );
    }
}