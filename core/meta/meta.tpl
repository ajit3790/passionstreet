<title><?=$page_meta['title']?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0,minimum-scale=1">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="<?=$page_meta['keywords']?>">
<meta name="description" content="<?=$page_meta['description']?>">
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<meta name="google-site-verification" content="<?=$GLOBALS['social']['gg']['siteverification']?>">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<?php
if($_GET['layout'] != 'amp')
{
if(PROTOCOL == 'https://')
echo '<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">'."\n";
if(SERVERTYPE != 'live')
echo '<meta name="robots" content="'.(($PSParams['blockbots'] == 1)?'noindex, nofollow':'noindex, nofollow').'">'."\n";
else
echo '<meta name="robots" content="'.(($PSParams['blockbots'] == 1)?'noindex, nofollow':'index, follow').'">'."\n";
?>
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<?php
}
?>
<?php
if($_GET['pagetype'] == 'eventDetail' && $_GET['pageId'] == 'event-7358358790')
$page_meta['url'] = "https://passionstreet.in/event/summer-quarter-marathon-2nd-edition/event-7358358790";
?>
<link rel="canonical" href="<?=$page_meta['url']?>" >
<?php
if($page_meta['nexturl'])
echo '<link rel="next" href="'.$page_meta['nexturl'].'" >'."\n";
if($page_meta['prevurl'])
echo '<link rel="prev" href="'.$page_meta['prevurl'].'" >'."\n";
?>
<!-- Open Graph data start -->
<meta property="og:title" content="<?=$page_meta['title']?>">
<meta property="og:type" content="<?=$GLOBALS['social']['pagetype']?>">
<meta property="og:url" content="<?=$page_meta['url']?>">
<?php
foreach($page_meta['image'] as $key=>$image)
{
?>
<meta property="og:image" content="<?=$image?>">
<?php
}
?>
<meta property="og:description" content="<?=$page_meta['description']?>">
<meta property="og:site_name" content="<?=$GLOBALS['social']['sitename']?>">
<!-- and other facebook stuff -->
<meta property="fb:app_id" content="<?=$GLOBALS['social']['fb']['appid']?>">
<!-- Open Graph data end -->

<!-- Twitter Card data start -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@<?=$GLOBALS['social']['tw']['handle']?>">
<meta name="twitter:title" content="<?=$page_meta['title']?>">
<meta name="twitter:description" content="<?=$page_meta['description']?>">
<meta name="twitter:image" content="<?=$page_meta['image'][0]?>">
<meta name="twitter:creator" content="<?=$GLOBALS['social']['tw']['handle']?>">
<meta name="twitter:image:src" content="<?=$page_meta['image'][0]?>">
<meta name="twitter:domain" content="<?=$_SERVER['SERVER_NAME']?>">
<!-- Twitter Card data end -->

<!-- Google Authorship and Publisher Markup -->
<link rel="publisher" href="<?=$GLOBALS['social']['gg']['publisher']?>">
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?=$page_meta['title']?>">
<meta itemprop="description" content="<?=$page_meta['description']?>">
<meta itemprop="image" content="<?=$page_meta['image'][0]?>">
<!--
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-3921574572757592",
    enable_page_level_ads: true
  });
</script>
-->
<?php
if($_GET['pagetype'] == 'eventDetail')
echo '<link rel="amphtml" href="'.str_replace(ROOT_PATH,ROOT_PATH.'amp/',$page_meta['url']).'" />'
?>