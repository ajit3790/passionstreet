<?php
require_once('initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
             ),
             'rowWrapper'=>array(),
             'modules'=>array(
                'homebanner'=>array(
                    'modulesParams'=>array()
                )
            )
        ),
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'modules'=>array(
                'passionlisting'=>array(
                    'modulesParams'=>array("class"=>"col-mod-12 ")
                )
            )
        )
);

$page_meta['title'] = 'Passion Categories on PASSIONSTREET';$page_meta['description'] = 'Follow your passion and share experience with others';$page_meta['keywords'] = 'Passion Listing, Passion Zone , Passion Categories , Only Passion, PASSIONSTREET, Passion Trails';

echo render_modules();

?>