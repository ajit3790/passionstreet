<?php
header("Content-type: text/javascript");
?>

importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-messaging.js');


var config = {
    messagingSenderId: "234168754983"
};
firebase.initializeApp(config);


const messaging = firebase.messaging();
// [END initialize_firebase_in_sw]


messaging.setBackgroundMessageHandler(function(payload) {
  
  // Customize notification here
  const notificationTitle = payload.data.status;
  const notificationOptions = {
    body: payload.data.body,
    icon: payload.data.icon,
	data :payload.data.url
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});

self.addEventListener('notificationclick', function(event) {
    var url = event.notification.data;
    event.waitUntil(
    clients.openWindow(url).then(function (){
     event.notification.close();})
    );
});
