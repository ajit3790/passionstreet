<?php
if($event['status'] == 'unpublished')
echo '<img src="<?=ROOT_PATH?>images/preview.png" class="preview-tag" />';
if($event['source'] == 'aggregated' && $event['organiser']['type'] == 'fbpage' && $event['organiser']['email'] && ($event['datestart'] > date("Y-m-d H:i:s",strtotime('+3 days'))))
{
	?>
	
	<button class="btn btn-claim" data-onclick="claimevent"><i class="fa fa-check"></i> Enable Ticketing & Manage Event </i></button>
	
	<?php
}
 ?>
<div class="widget bx-styl event-meta" id="tck_dtl"> 
	
	<div class="widget-heading clearfix">	
		<?php
		if($event['isAdmin']){
		?>
			<div class="settings">
				<!--<span class="toggle-setting">Settings <i class="fa fa-cog"></i></span>-->
				<span class="toggle-setting"><a href="<?=ROOT_PATH.'events/publish/'.$event['event_id']?>" target="_blank">Settings <i class="fa fa-cog"></i></a></span>
				<span class="toggle-setting"><a href="<?=ROOT_PATH.'events/data/'.$event['event_id']?>" target="_blank">&nbsp;Attendees <i class="fa fa-user"></i></a></span>
                                <?php
                                /*
				<ul>
					<li><a href="#">Edit Event <i class="fa fa-pencil"></i></a> </li>
					<li><a href="#">Assign More Admin <i class="fa fa-pencil"></i></a> </li>
					<li><a href="#">Invite People <i class="fa fa-pencil"></i></a> </li>
					<li><a href="#">Delete Event <i class="fa fa-pencil"></i></a> </li>
					<li><a data-targetsrc="module/settingsevent/password/1234/sdfasd" class="callmodaliframe" href="#">Change Password <i class="fa fa-lock"></i></a></li>
				</ul>
                                */
                                ?>
			</div>
		<?php
		}
		
	?>
        <h2 class="hstyl_1 sm <?=(empty($event['ticketcategories'])?'hide':'')?>"><i class="fa fa-ticket"></i>&nbsp;&nbsp;<?=(($event['skiptoleadcapture'])?$event['ticketcategories']['leadcapture']['name']:'Tickets')?></h2>
	</div>
	<ul class="eventtickets">
		
		<?php 
		if($event['ticketcategories'] && empty($event['skiptoleadcapture'])) 
		{
			foreach($event['ticketcategories'] as $ticketdetail)
			{
				?>
				<li>
				<div class="row">
					<?php
					if(false)
					{
					?>
					<div class="col-xs-6">
						<?=$ticketdetail['name']?>
					</div>
					<div class="col-xs-3 text-right">
						₹ <?=$ticketdetail['price']?> 
					</div>
					<div class="col-xs-3">
						<div class="input-group">
					          <span class="input-group-btn">
					              <button type="button" class="btn btn-default btn-number" data-type="minus" disabled="disabled">
					                  -
					              </button>
					          </span>
					          <input class="form-control input-number ticketselectsidebar" data-ticketid="<?=$ticketdetail['id']?>" value="0" min="<?=(($ticketdetail['iscombo'] && $ticketdetail['iscombo'] ==1)?0:(($ticketdetail['minbuy'])?$ticketdetail['minbuy']:0))?>" max="<?=(($ticketdetail['iscombo'] && $ticketdetail['iscombo'] ==1)?1:(($ticketdetail['maxbuy'])?$ticketdetail['maxbuy']:10))?>" type="text">
					          <span class="input-group-btn">
					              <button type="button" class="btn btn-default btn-number" data-type="plus" >
					                  +
					              </button>
					          </span>
					      </div>
					</div>
					<?php
					}
					else
					{
					?>
					<div class="col-xs-9">
						<?=$ticketdetail['name']?>
					</div>
					<div class="col-xs-3 text-right">
						₹ <?=$ticketdetail['price']?> 
					</div>
					<?php
					}
					?>
					
				</div>
				<?php
				if($ticketdetail['bookingprice'] != 0)
				{
					echo '<div class="row sm-text">';
					echo '<div class="col-xs-12 ">';
					echo 'Booking Amount: ₹ '.$ticketdetail['bookingprice'];
					echo '</div>';
					echo '</div>';
				}
				echo '</li>';
			} 
		} 
		?>
		
	   <li class="clearfix">
		<?php
		if($event['skiptoleadcapture']){
			echo '<button class="btn btn-primary pull-right" data-onclick="showleadcaptureform">'.$event['ticketcategories']['leadcapture']['name'].'</button>';
		}
		else if($event['ticketcategories'])
		{
			if($event['bookingenddate'] != '0000-00-00 00:00:00' && (date('Y-m-d H:i:s') > $event['bookingenddate']))
			echo '<button class="btn btn-primary pull-right" disabled>Booking Closed</button>';
			else
			{
			if($event['producttype'] == 'tour')
			echo '<button class="btn btn-primary pull-right" data-onclick="showcustomdateselector">Buy Ticket</button>';
			else
			echo '<button class="btn btn-primary pull-right" data-onclick="showdatetimeslots">Buy Ticket</button>';
			}
		}
		else
		echo '<button id="event_follow" class=" btn btn-primary ajax-btn pull-right" data-action="post" data-aj="true" data-p-str="eventid" data-t="event-follow" data-eventid="'.$event['event_id'].'">Follow Event</button>';
		?>
		<div class="socialbar_2 circle sm">
			<a data-type="fb" class="fb sharepage"><i class="fa fa-facebook"></i></a>
			<a data-type="tw" class="twtr sharepage"><i class="fa fa-twitter"></i></a>
			<a data-type="gg" class="gplus sharepage"><i class="fa fa-google-plus"></i></a>
		</div>
		
		<div class="rating" >
	   	
	   	<div class="ratingwrapper" data-id='<?=$event['event_id']?>' data-type='event'>
	   	<?php
	   	$eventrating = $event['rating'];
	   	if(empty($eventrating['users']))
	   	$eventrating['users'] = 0;
	   	if(empty($eventrating['totalscore']))
	   	$eventrating['totalscore'] = 0;
	   	if($eventrating['users'])
	   	$rating = round(($eventrating['totalscore'] / $eventrating['users']));
	   	else
	   	$rating = 0;
	   	for($i=1;$i<=5;$i++)
	   	{
	   	    if($rating>=$i)
	   	    echo '<span data-rating='.$i.' class="fill"></span>';
	   	    else
	   	    echo '<span data-rating='.$i.'></span>';
	   	}
                if($eventrating['users'])
	   	echo ' / '.$eventrating['users'].'';
                ?>
		</div>
		</div>
	   </li>
	   <li class="lst_dt hide"><div class="row">
		<div class="col-xs-7">
	        Ticket booking ends on:
	        </div>
	        <div class="col-xs-5 text-right">
	        </div>
	        </div>
	   </li>
	</ul>                 
</div>
<?php
if($event['bookingenddate'] != '0000-00-00 00:00:00' && $event['source'] !='aggregated')
{
$x = strtotime($event['bookingenddate']);
$countstartdate = strtotime('-2 week', $x);
$now = time();
if($now >= $countstartdate && $now<=$x)
{
$PSJsincludes['external'][] = "js/countdowntimer.js";
?>
<div class="widget countdowntimer hide" data-timeevent="<?=$event['datestart']?>" data-timerend="<?=$event['bookingenddate']?>">
    <!--span class='header'>Ticket booking ends in</span-->
    <div class='header'>Ticket booking ends in</div>
    <div class='tiles'></div>
    <div class="labels">
    <li>Days</li>
    <li>Hours</li>
    <li>Mins</li>
    <li>Secs</li>
    </div>
</div>
<?php
}
}
?>
<?php
if($event['donations'] && $event['donations']['allow'] && $event['donations']['enddate'] > date('Y/m/d H:i:s'))
{
$PSJavascript['donation'] = $event['donations'];
?>
<div class="widget donationwdgt "> 
	<div class="widget-heading">	
		<h2 class="hstyl_1 sm">Do you want to contribute to this charity?</h2>
	</div>             
	<div class="bx-styl event-meta">   
		<div class="donation-form form1 clearfix">
			<div class="row">
				<div class="col-sm-6 donationuserdata hide">
                      Donation Amount
                </div>
				<div class="col-sm-6">
                      <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" id="donateamount" placeholder="Enter Amount" class="form-control">
                </div>
				<div class="donationuserdata hide">
					<div class="col-sm-6">Name</div>
					<div class="col-sm-6"><input type="text" placeholder="Enter Full Name" id="donateename" class="form-control"></div>
					<div class="col-sm-6">Email</div>
					<div class="col-sm-6"><input type="text" placeholder="Enter Email" id="donateeemail" class="form-control"></div>
					<div class="col-sm-6">Contact Number</div>
					<div class="col-sm-6"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Enter Phone Number" id="donateemobile" class="form-control"></div>
				</div>
				<div class="donationuserdata hide col-sm-6">
                      &nbsp;
                </div><div class="col-sm-6">
                      <input type="button" id="donatebutton" class="form-control btn btn-primary" value="Contribute">
                </div>
			</div>
		</div>
	</div>	                       
</div>
<?php
}
if($event['source'] == 'organic')
{
?>
<div class="widget orgnzr "> 
<div class="widget-heading">	
		<h2 class="hstyl_1 sm">Organizer</h2>
	</div>             
		<div class="bx-styl event-meta">   
	        <ul>
	            <li class="clearfix">
			<i class="fa fa-briefcase"></i>
			<h6 class="hide">Organizer</h6>
			<?php
			if($event['organiser']['type'] != 'userprofile')
			echo $event['organiser']['name'];
			else
			echo "<a href='".ROOT_PATH.'profile/'.get_alphanumeric($event['organiser']['name']).'/'.$event['organiser']['id']."' target='_blank'>".$event['organiser']['name']."</a>";
			?>
	            </li>
	            <li class="clearfix">
			<i class="fa fa-map-marker"></i>
			<h6 class="hide">Address</h6>
			<?=(($event['organiser']['address'])?$event['organiser']['address']:'Not Provided')?>
	            </li>
	            <li class="clearfix">
			<?php
			if($event['organiser']['email'])
			{
				if($event['creator'] == 'PS')
				{
				$toname = $event['organiser']['name'];
				$toprofile = $event['organiser']['email'];
				}
				else
				{
				$toname = $event['organiser']['name'];
				$toprofile = $event['creator'];
				}
			}
			else
			{
			$toname = 'PASSIONSTREET';
			$toprofile = 'social@passionstreet.in';
			}
			?>
	                <a href="#" class="callmodaliframe btn btn-primary" data-targetsrc="module/contactform?toname=<?=$toname?>&toprofile=<?=$toprofile?>">Connect</a>
	            </li>
	        </ul>
		</div>	                       
	    </div>

<?php
}
?>
        <?php
        /*
        if($event['routeimage'])
        {
            ?>
            <div class="widget bx-styl">
        <div>
            <h2 class="hstyl_1 sm">Route Map</h2>  
            <div ><a href="#" class="callmodaliframe" data-targetsrc="module/maps/routemap?coordinates=<?=urlencode(serialize($event['routemap']))?>"><img src="<?=$event['routeimage']?>" style="width:100%"></a></div>
            </div>
</div>
            <?php
        }
        else if($event['mapimage'] && false)
        {
            ?>
             <div class="widget bx-styl">
        <div>
            <h2 class="hstyl_1 sm">Location Map</h2>  
            <div ><a href="#" class="callmodaliframe" data-targetsrc="module/maps/map?coordinate=<?=urlencode($event['maploc'])?>"><img src="<?=$event['mapimage']?>" style="width:100%"></a></div>
            </div>
</div>
            <?php
        }*/
        ?>
        
<div class="widget event-gallery jbox-gallery hide">
<div class="widget-heading"> <?php if(!empty($event['isAdmin']) && $_GET['layout']!='amp'){?>
    <span class="pull-right upload-btn" style="position:relative" href="#">
        <input type="file" name="eventpic[]" content_id="<?=$event['event_id']?>" content_type="event" class="filehidden ajaxImageUpload" multiple callback="" pictype="eventPics">
        Upload Pics <i class="fa fa-gear"></i>
    </span>
    <?php } ?>
    <!--<a class="more-lnk pull-right" href="#">View all <i class="fa fa-angle-double-right"></i></a>-->

    <h2 class="hstyl_1 sm">Photo Gallery</h2> 
    </div>
<div class=" bx-styl">
    <div class="inner">
        <ul class="clearfix img_set">
        </ul>
    </div>
</div>
</div>
<?php
/*
<div class="widget amazonads">
	<!--<script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=d8fe32cb-f38b-4873-9ac9-1f23fc18b2c4"></script>-->
	<!-- 
	<script type="text/javascript">
	amzn_assoc_placement = "adunit0";
	amzn_assoc_search_bar = "true";
	amzn_assoc_tracking_id = "jauharcorpora-20";
	amzn_assoc_ad_mode = "manual";
	amzn_assoc_ad_type = "smart";
	amzn_assoc_marketplace = "amazon";
	amzn_assoc_region = "US";
	amzn_assoc_title = "My Amazon Picks";
	amzn_assoc_linkid = "fdfa2070b4d64627057636863aae5ad8";
	amzn_assoc_asins = "B01K9S260E,B01J5E6IIY,B00HFPOXM4,B0177V0H7K";
	</script>
	<script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US"></script>
	
	-->
	
	<script type="text/javascript">
	amzn_assoc_ad_type ="responsive_search_widget"; 
	amzn_assoc_tracking_id ="jauharcorpora-21"; 
	amzn_assoc_marketplace ="amazon"; 
	amzn_assoc_region ="IN"; 
	amzn_assoc_placement =""; 
	amzn_assoc_search_type = "search_widget";
	amzn_assoc_width ="auto"; 
	amzn_assoc_height ="auto"; 
	amzn_assoc_default_search_category =""; 
	amzn_assoc_default_search_key ="";
	amzn_assoc_theme ="light"; 
	amzn_assoc_bg_color ="FFFFFF"; 
	</script>
	<script src="//z-in.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1&Marketplace=IN"></script>
</div>
*/
?>
<div class="widget eventanalytics hide">
	<div class="widget-heading">
		<h2 class="hstyl_1 sm">Event Analytics</h2> 
	</div>
	<div class=" bx-styl">
		<div class="inner" id="chart_div">
			
		</div>
	</div>
</div>
<?php
//$PSJsincludes['external'][] = "https://www.gstatic.com/charts/loader.js";
?>
<script>
$(document).ready(function(){
    $params = {id:'<?=$event['event_id']?>',added_to:'event'};
    $.get(root_path+'PSAjax.php?type=getphotoes',$params,function(data){
        $photoes = $.parseJSON(data);
        $photoeshtml = '';
        $.each($photoes['list']['photoes'],function(i,v){
            $photoeshtml += "<li><div><img class='jbox-img unveil' src='"+DEFAULT_IMG+"' data-src='"+v['imgurl']+"'></div></li>";
        });
	if($photoeshtml)
	{
        $(".event-gallery").find('ul').html($photoeshtml).removeClass('hide');
	delayfunction2(['jquery.unveil.js'],'jqueryunveil_callback','',1000);
        gallery = $(".event-gallery .jbox-img"),
        lastImg = gallery[gallery.length - 1].getAttribute("src"),
        firstImg = gallery[0].getAttribute("src");
        $(".jbox-img").click(function() {
            var n = $(this).attr("src");
            openJBox(n, "first")
        });
	}
    });
    
});
</script>