<div class="widget bx-styl event-meta" id="tck_dtl"> 
	
	<div class="widget-heading clearfix">	
		<?php
		if($event['isAdmin']){
		?>
			<div class="settings">
				<!--<span class="toggle-setting">Settings <i class="fa fa-cog"></i></span>-->
				<span class="toggle-setting"><a href="<?=ROOT_PATH.'events/publish/'.$event['event_id']?>" target="_blank">Settings <i class="fa fa-cog"></i></a></span>
				<span class="toggle-setting"><a href="<?=ROOT_PATH.'events/data/'.$event['event_id']?>" target="_blank">&nbsp;Attendees <i class="fa fa-user"></i></a></span>
                                <?php
                                /*
				<ul>
					<li><a href="#">Edit Event <i class="fa fa-pencil"></i></a> </li>
					<li><a href="#">Assign More Admin <i class="fa fa-pencil"></i></a> </li>
					<li><a href="#">Invite People <i class="fa fa-pencil"></i></a> </li>
					<li><a href="#">Delete Event <i class="fa fa-pencil"></i></a> </li>
					<li><a data-targetsrc="module/settingsevent/password/1234/sdfasd" class="callmodaliframe" href="#">Change Password <i class="fa fa-lock"></i></a></li>
				</ul>
                                */
                                ?>
			</div>
		<?php
		
	}
	?>
        <h2 class="hstyl_1 sm <?=(empty($event['ticketcategories'])?'hide':'')?>"><i class="fa fa-ticket"></i>&nbsp;&nbsp;Tickets</h2>
	</div>
	<ul class="eventtickets">
		
		<?php 
		if($event['ticketcategories']) 
		{
			foreach($event['ticketcategories'] as $ticketdetail)
			{
				?>
				<li>
				<div class="row">
					<?php
					if(false)
					{
					?>
					<div class="col-xs-6">
						<?=$ticketdetail['name']?>
					</div>
					<div class="col-xs-3 text-right">
						₹ <?=$ticketdetail['price']?> 
					</div>
					<div class="col-xs-3">
						<div class="input-group">
					          <span class="input-group-btn">
					              <button type="button" class="btn btn-default btn-number" data-type="minus" >
					                  -
					              </button>
					          </span>
					          <input class="form-control input-number ticketselectsidebar" data-ticketid="<?=$ticketdetail['id']?>" value="0" min="<?=(($ticketdetail['iscombo'] && $ticketdetail['iscombo'] ==1)?0:(($ticketdetail['minbuy'])?$ticketdetail['minbuy']:0))?>" max="<?=(($ticketdetail['iscombo'] && $ticketdetail['iscombo'] ==1)?1:(($ticketdetail['maxbuy'])?$ticketdetail['maxbuy']:10))?>" type="text">
					          <span class="input-group-btn">
					              <button type="button" class="btn btn-default btn-number" data-type="plus" >
					                  +
					              </button>
					          </span>
					      </div>
					</div>
					<?php
					}
					else
					{
					?>
					<div class="col-xs-9">
						<?=$ticketdetail['name']?>
					</div>
					<div class="col-xs-3 text-right">
						₹ <?=$ticketdetail['price']?> 
					</div>
					<?php
					}
					?>
					
				</div>
				<?php
				if($ticketdetail['bookingprice'] != 0)
				{
					echo '<div class="row sm-text">';
					echo '<div class="col-xs-12 ">';
					echo 'Booking Amount: ₹ '.$ticketdetail['bookingprice'];
					echo '</div>';
					echo '</div>';
				}
				echo '</li>';
			} 
		} 
		?>
		
	   <li class="clearfix">
		<?php
		if($event['ticketcategories'])
		{
		if($event['bookingenddate'] != '0000-00-00 00:00:00' && (date('Y-m-d H:i:s') > $event['bookingenddate']))
		echo '<a href="'.ROOT_PATH.'event/'.$event['seopath'].'" class="btn btn-primary pull-right" >Booking Closed</a>';
		else
		{
		if($event['producttype'] == 'tour')
		echo '<a href="'.ROOT_PATH.'event/'.$event['seopath'].'" class="btn btn-primary pull-right" data-onclick="showcustomdateselector">Buy Ticket</a>';
		else
		echo '<a href="'.ROOT_PATH.'event/'.$event['seopath'].'" class="btn btn-primary pull-right" data-onclick="showdatetimeslots">Buy Ticket</a>';
		}
		}
		else
		echo '<a href="'.ROOT_PATH.'event/'.$event['seopath'].'"  id="event_follow" class=" btn btn-primary ajax-btn pull-right" data-action="post" data-aj="true" data-p-str="eventid" data-t="event-follow" data-eventid="'.$event['event_id'].'">Follow Event</a>';
		?>
		<div class="socialbar_2 circle sm">
			<a data-type="fb" class="fb sharepage"><i class="fa fa-facebook"></i></a>
			<a data-type="tw" class="twtr sharepage"><i class="fa fa-twitter"></i></a>
			<a data-type="gg" class="gplus sharepage"><i class="fa fa-google-plus"></i></a>
		</div>
		
		<div class="rating" >
	   	
	   	<div class="ratingwrapper" data-id='<?=$event['event_id']?>' data-type='event'>
	   	<?php
	   	$eventrating = $event['rating'];
	   	if(empty($eventrating['users']))
	   	$eventrating['users'] = 0;
	   	if(empty($eventrating['totalscore']))
	   	$eventrating['totalscore'] = 0;
	   	if($eventrating['users'])
	   	$rating = round(($eventrating['totalscore'] / $eventrating['users']));
	   	else
	   	$rating = 0;
	   	for($i=1;$i<=5;$i++)
	   	{
	   	    if($rating>=$i)
	   	    echo '<span data-rating='.$i.' class="fill"></span>';
	   	    else
	   	    echo '<span data-rating='.$i.'></span>';
	   	}
                if($eventrating['users'])
	   	echo ' / '.$eventrating['users'].'';
                ?>
		</div>
		</div>
	   </li>
	   <li class="lst_dt hide"><div class="row">
		<div class="col-xs-7">
	        Ticket booking ends on:
	        </div>
	        <div class="col-xs-5 text-right">
	        </div>
	        </div>
	   </li>
	</ul>                 
</div>
<?php
if($event['bookingenddate'] != '0000-00-00 00:00:00')
{
$x = strtotime($event['bookingenddate']);
$countstartdate = strtotime('-2 week', $x);
$now = time();
if($now >= $countstartdate && $now<=$x)
{
$PSJsincludes['external'][] = "js/countdowntimer.js";
?>
<div class="widget countdowntimer hide" data-timeevent="<?=$event['datestart']?>" data-timerend="<?=$event['bookingenddate']?>">
    <!--span class='header'>Ticket booking ends in</span-->
    <div class='header'>Ticket booking ends in</div>
    <div class='tiles'></div>
    <div class="labels">
    <li>Days</li>
    <li>Hours</li>
    <li>Mins</li>
    <li>Secs</li>
    </div>
</div>
<?php
}
}
?>
<?php
if($event['donations'] && $event['donations']['allow'] && $event['donations']['enddate'] < date('Y/m/d H:i:s'))
{
?>
<div class="widget donationwdgt "> 
	<div class="widget-heading">	
		<h2 class="hstyl_1 sm"><a  href="<?=ROOT_PATH.'event/'.$event['seopath']?>" >Click here if you want to contribute to this charity?</a></h2>
	</div>
	<br />	
</div>
<?php
}
?>
<div class="widget orgnzr "> 
<div class="widget-heading">	
		<h2 class="hstyl_1 sm">Organizer</h2>
	</div>             
		<div class="bx-styl event-meta">   
	        <ul>
	            <li class="clearfix">
			<i class="fa fa-briefcase"></i>
			<h6 class="hide">Organizer</h6>
			<?php
			if($event['organiser']['type'] != 'userprofile')

			echo $event['organiser']['name'];
			else
			echo "<a href='".ROOT_PATH.'profile/'.get_alphanumeric($event['organiser']['name']).'/'.$event['organiser']['id']."' target='_blank'>".$event['organiser']['name']."</a>";
			?>
	            </li>
	            <li class="clearfix">
			<i class="fa fa-map-marker"></i>
			<h6 class="hide">Address</h6>
			<?=$event['organiser']['address']?>
	            </li>
	            <li class="clearfix">
	                <a href="<?=ROOT_PATH.'event/'.$event['seopath']?>" class="btn btn-primary pull-right" >Connect</a>
	            </li>
	        </ul>
		</div>	                       
	    </div>
	    <amp-analytics type="googleanalytics">
<script type="application/json">
    {
      "vars": {"account": "UA-75357427-1"},
      "triggers": {
	"defaultPageview": {
	    "on": "visible",
	    "request": "pageview"
	}}}
</script>
</amp-analytics>