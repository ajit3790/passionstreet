<div class="widget bx-styl event-meta">                
    <div class="">
        <ul>
            <li class="clearfix">
				<i class="fa fa-briefcase"></i>
				<h6>Organizer</h6>
				<?=$event['organiser']['name']?>
            </li>
            <li class="clearfix">
				<i class="fa fa-map-marker"></i>
				<h6>Address</h6>
				<?=$event['organiser']['address']?>
            </li>
            <li class="clearfix">
                <a href="#" class="callmodaliframe btn btn-primary" targetsrc="module/contactform?email=<?=$event['organiser']['email']?>">Connect</a>
            </li>
        </ul>
    </div>                    
</div>
<div class="widget bx-styl event-meta"> 
	<?php
	if($event['isAdmin']){
		?>
			<div class="settings">
				<span class="toggle-setting">Settings <i class="fa fa-cog"></i></span>

					<ul>
						<li><a href="#">Edit Event <i class="fa fa-pencil"></i></a> </li>
						<li><a href="#">Assign More Admin <i class="fa fa-pencil"></i></a> </li>
						<li><a href="#">Invite People <i class="fa fa-pencil"></i></a> </li>
						<li><a href="#">Delete Event <i class="fa fa-pencil"></i></a> </li>
						<li><a targetsrc="module/settingsevent/password/1234/sdfasd" class="callmodaliframe" href="#">Change Password <i class="fa fa-lock"></i></a></li>
					</ul>
			</div>
		<?php
	}
	?>
	<ul>
		<li class="clearfix">
			<i class="fa fa-clock-o"></i>
			<?php
				$datestr1 = '';
				$datestr2 = '';
				$eventdateData = getdate(strtotime($event['datestart']));
				$datestr1 = ucfirst($eventdateData['month']) .' '. $eventdateData['mday'];
				$datestr2 = ucfirst($PSParams['months'][$eventdateData['mon']]) .' '. $eventdateData['mday'] .' at '. (($eventdateData['hours'] <= 12)?($eventdateData['hours'] .' AM'):(($eventdateData['hours']-12) .' PM'));
				
				if($event['dateend'])
				{
					$eventdateData = getdate(strtotime($event['dateend']));
					$datestr1 .= ' &ndash; '. ucfirst($eventdateData['month']) .' '. $eventdateData['mday'];
					$datestr2 .= ' to '. ucfirst($PSParams['months'][$eventdateData['mon']]) .' '. $eventdateData['mday'] .' at '. (($eventdateData['hours'] <= 12)?($eventdateData['hours'] .' AM'):(($eventdateData['hours']-12) .' PM'));
				}
			?>
			<h6><?=$datestr1?></h6>
			<?=$datestr2?>
		</li>
		<li class="clearfix">
		   <i class="fa fa-map-marker"></i>
			<?php 
				$eventaddressData = explode(',',$event['address']);
				$eventlocation = $eventaddressData[0];
				$eventaddress = implode(',',$eventaddressData);
			?>
			<h6><?=$eventlocation?></h6>
			<?=$eventaddress?>
		</li>
		<?php 
		if($event['ticketcategories']) 
		{ ?>
		<li>
			<i class="fa fa-ticket"></i>
			<h6><span class="pull-right text-right">Rate / Ticket (INR)</span>Ticket Category</h6>
			<?php
			foreach($event['ticketcategories'] as $ticketdetail)
			{ ?>
			<div class="row"><div class="col-xs-8"><?=$ticketdetail['name']?></div><div class="col-xs-4 text-right"><?=$ticketdetail['price']?></div></div>
			<?php }  ?>   
		</li>
	   	<?php } ?>
		<li>
	   	<div class="rating" >
	   	Rating 
	   	<div class="ratingwrapper" data-id='<?=$event['event_id']?>' data-type='event'>
	   	<?php
	   	$eventrating = unserialize($event['rating']);
	   	if(empty($eventrating['users']))
	   	$eventrating['users'] = 0;
	   	if(empty($eventrating['totalscore']))
	   	$eventrating['totalscore'] = 0;
	   	if($eventrating['users'])
	   	$rating = round(($eventrating['totalscore'] / $eventrating['users']));
	   	else
	   	$rating = 0;
	   	for($i=1;$i<=5;$i++)
	   	{
	   	    if($rating>=$i)
	   	    echo '<span data-rating='.$i.' class="fill"></span>';
	   	    else
	   	    echo '<span data-rating='.$i.'></span>';
	   	}
                if($eventrating['users'])
	   	echo '('.$eventrating['users'].' users)';
                ?>
		</div>
		</div>
		<div class="socialbar_2 circle sm">
			<a href="javascript:void(0)" type="fb" class="fb sharepage"><i class="fa fa-facebook"></i></a>
			<a href="javascript:void(0)" type="tw" class="twtr sharepage"><i class="fa fa-twitter"></i></a>
			<a href="javascript:void(0)" type="gg" class="gplus sharepage"><i class="fa fa-google-plus"></i></a>
		</div>
			
	   </li>
	   <li>
		<button class="btn btn-primary" onclick="showquestionaire()">Sign Up Event</button>
	   </li>
	</ul>                 
</div>
<div class="widget bx-styl">
        <div>
        <?php
        if($event['routeimage'])
        {
            ?>
            <h2 class="hstyl_1 sm">Route Map</h2>  
            <div ><a href="#" class="callmodaliframe" targetsrc="module/maps/routemap?coordinates=<?=urlencode(serialize($event['routemap']))?>"><img src="<?=$event['routeimage']?>" style="width:100%"></a></div>
            <?php
        }
        else if($event['mapimage'])
        {
            ?>
            <h2 class="hstyl_1 sm">Location Map</h2>  
            <div ><a href="#" class="callmodaliframe" targetsrc="module/maps/map?coordinate=<?=urlencode($event['maploc'])?>"><img src="<?=$event['mapimage']?>" style="width:100%"></a></div>
            <?php
        }
        ?>
        </div>
</div>
<div class="widget event-gallery">
<div class="widget-heading"> <?php if(!empty($event['isAdmin'])){?>
    <span class="pull-right upload-btn" style="position:relative" href="#">
        <input type="file" name="eventpic[]" content_id="<?=$event['event_id']?>" content_type="event" class="filehidden ajaxImageUpload" multiple callback="" pictype="eventPics">
        Upload Pics <i class="fa fa-gear"></i>
    </span>
    <?php } ?>

    <h2 class="hstyl_1 sm">Events Photo Gallery</h2> 
    </div>
<div class=" bx-styl scroll-content">
    <div class="inner">
        <ul class="clearfix">
        </ul>
    </div>
</div>
</div>
    <script>
$(document).ready(function(){
    $params = {id:'<?=$event['event_id']?>',added_to:'event'};
    $.get(root_path+'PSAjax.php?type=getphotoes',$params,function(data){
        $photoes = $.parseJSON(data);
        $photoeshtml = '';
        $.each($photoes['list']['photoes'],function(i,v){
            $photoeshtml += "<li><div class='img-frame'><img class='jbox-img' src='"+v['imgurl']+"'></div></li>";
        });
        $(".event-gallery").find('ul').html($photoeshtml);
        gallery = $(".jbox-img"),
        lastImg = gallery[gallery.length - 1].getAttribute("src"),
        firstImg = gallery[0].getAttribute("src");
        $(".jbox-img").click(function() {
            var n = $(this).attr("src");
            openJBox(n, "first")
        })
    });
    
});
</script>