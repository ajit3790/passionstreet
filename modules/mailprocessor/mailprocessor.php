<?php
    $profile_id = (!empty($_GET['profile_id'])?$_GET['profile_id']:0);
    $profiledetails = user_profile(array("profile_id"=>$profile_id));
    $email_id = (!empty($profiledetails['email'])?$profiledetails['email']:(!empty($_GET['email_id'])?$_GET['email_id']:0));
    $token = strtolower(hash('sha512', implode("|",array($email_id,$profile_id,$profiledetails['id'],$_GET['t'],ENCRYPTION_KEY))));
    if($token == $_GET['mailtoken'])
    {
        //proceed implementation
        $action = $_GET['action'];
        switch ($action){
            case 'resetpassword':
                $defaultformFields['profile_id'] = $_GET['profile_id'];
                $defaultformFields['mailtoken'] = $_GET['mailtoken'];
                $PSModData['defaultformFields'] = $defaultformFields;
                $module_display_style = 'resetpassword';
                break;
            case 'activation':
                user_profile_update(array('profile_id'=>$_GET['profile_id'],'status'=>'1'));
                echo '<div class="msgbox1 text-center">
                        <h4>Profile Activation Successfull</h4>
                    </div>';
                break;
	    case 'verifypage':
		$extradata = base64_decode($_GET['ext']);
		parse_str($extradata,$extradataarray);
		if($extradataarray['usr'] == $PSData['user']['profile_id'] && $extradataarray['pg'])
		{
			$where_fields['page_id'] = $extradataarray['pg'];
			$where_fields['creator_id'] = $PSData['user']['profile_id'];
			$where_fields['page_email'] = $_GET['email_id'];
			$where_fields['status'] = 1;
			$update_fields['status'] = 2;
			$updateresponse = page_update($where_fields,$update_fields);
			if($updateresponse == 1)
			{
			    echo '<div class="msgbox1 text-center">
				<h4>Company Page is now verified</h4>
			    </div>';
		        }
			else
			{
				echo '<div class="msgbox1 text-center">
				<h4>Page is already verified</h4>
			    </div>';
			}
                }
		else
		{
		echo '<div class="msgbox1 text-center">
                        <h4>Login first and then retry the link</h4>
                    </div>';
                }
		break;
	    default :
                echo NULL;
        }
    }
    else{
        echo '<div class="msgbox1 text-center">
		<h3>Not authorized for this operation</h3>
        </div>';
    }
?>