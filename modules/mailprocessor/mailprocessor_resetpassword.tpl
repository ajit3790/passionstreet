<?php
if (!empty($_POST['resetpasswordsubmit'])) {
	$profile_id     = $_POST['profile_id'];
	$profiledetails = user_profile(array(
		"profile_id" => $profile_id
	));
	$token          = strtolower(hash('sha512', implode("|", array($profiledetails['email'],$profiledetails['profile_id'],$profiledetails['id'],$_GET['t'],ENCRYPTION_KEY))));
	if ($token == $_POST['mailtoken']) {
		if ($_POST['password'] == $_POST['re-password']) {
			if (user_credential_update(array('profile_id' => $_POST['profile_id'],'password' => $_POST['password']))) {
			} else {
				$credential['password']   = $_POST['password'];
				$credential['email']      = $profiledetails['email'];
				$credential['profile_id'] = $profiledetails['profile_id'];
				user_create_user_credential($credential);
			}
		?>

                 <div class="msgbox1 text-center">Password Reset Successfull</div>

                 <script>
		setTimeout(function(){
			parent.reloadwindow();
		},3000);
                 </script>

                 <?php
		 return;
		} else {
			echo '<div class="msgbox1 text-center">Password Reset Successfull</div>';
		}
	}
}
?>

<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="form-wrapper">

                <form class="form1" name="resetpassword-form" method="post">

                    <?php
foreach ($defaultformFields as $key => $value) {
	echo "<input type='hidden' name='" . $key . "' value='" . $value . "'/>";
}
?>

                    <div class="row form-group">

                        <div class="col-sm-6">

                          <input name="password"  class="form-control" placeholder="PASSWORD" type="password">

                        </div>

                        <div class="col-sm-6">

                          <input name="re-password"  class="form-control" placeholder="CONFIRM PASSWORD" type="password">

                        </div>

                    </div>

                    <div class="row form-group">

                          <div class="col-sm-12">

                            <input name="resetpasswordsubmit" class="form-control btn btn-yellow" value="Reset Password" type="Submit">

                          </div>

                    </div> 

                       

                </form>

            </div>

        </div>

    </div>

</div>