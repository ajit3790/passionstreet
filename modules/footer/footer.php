<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5">Copyright &copy; 2016-19 PASSIONSTREET</div>
            <div class="col-md-4 col-sm-3 text-center">
                <div class="socialbar_1 circle">
                    <a href="https://twitter.com/PassionStreetIN" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.facebook.com/PassionStreet.in" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://plus.google.com/103133684089210044221" target="_blank"><i class="fa fa-google-plus"></i></a>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul>
                    <li><a href="<?=ROOT_PATH.'faqs'?>">FAQs</a></li>
                    <li><a href="<?=ROOT_PATH.'terms'?>">Terms & Conditions</a></li>
                    <li><a href="<?=ROOT_PATH.'privacy'?>">Privacy</a></li>
                    <li><a href="<?=ROOT_PATH.'terms#disclaimer'?>">Disclaimer</a></li>
                </ul>                    
            </div>
        </div>
    </div>
</footer>