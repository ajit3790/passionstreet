<div class="row htmls">
<div class="col-sm-12 col-sm-12">                
    <div class="row">
        <div class="col-sm-12 text-center">
            <h3>Add /  Manage Display Elements</h3>
        </div>
    </div>
    <div class="row form-group hide">
        <div class="col-sm-12 ">
          <select id="displayelements" class="form-control">
		<option value=""> -- Select an optiopn -- </option>
		<option value="sponsorblock">Sponsors</option>
		<option value="itenararyblock">itenaray</option>
		<option value="customblock">Custom Block</option>
	  </select>
        </div>
     </div> 
     <br />
     <br />
     <br />
     <div class="row form-group sponsorblock displayblock hide">
        <div class="col-sm-12">
             Sponsor
        </div>
     </div>
     <div class="itenararyblock displayblock">
	<form class="form1 event-creation" action="<?=$_SERVER['PHP_BROWSER_URL']?>" method="post" enctype="multipart/form-data">
		<div class="row form-group ">
			<div class="col-sm-12">Itenarary</div>
			<div class="col-sm-12">
			     <input type="hidden" name="displayblock" value="itenarary">
			     <input type="hidden" name="blockid" value="<?=$htmlblocks['itenarary'][0]['blockid']?>">
			     <input type="text" class="form-control " name="displayblockname" value="" placeholder="Display name for Itenarary , Default is Itenaray">
			</div>
		</div>
		<div id="itenararycontainer">
		</div>
		<script id="daytemplate" type="text/x-jQuery-tmpl">
		<div class="row itenararydaybox form-group ">
			<div class="">
				<div class="col-sm-12">
					<div class="col-sm-3">
						Day ${dayno}
					</div>
					<div class="col-sm-9">
					&nbsp;
					<input type="button" class="btn btn-default pull-right addmoretmpl2" data-dayno="${dayno}" data-id="1" value="Add more items" data-target="itemlist_${dayno}"/>
					</div>
				</div>
			</div>
			<div id="itemlist_${dayno}">
				{{tmpl(itemlist) "#timetemplate"}}
			</div>
		</div>
		</script>
		<script id="timetemplate" type="text/x-jQuery-tmpl">
			<!--${JSON.stringify($data)}-->
				<div class="col-sm-12 timerow">
					<div class="col-sm-2">
						&nbsp;
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control " name="time[${dayno}][]" value="${time}" placeholder="8:00">
					</div>
					<div class="col-sm-6">
						<!--<input type="text" class="form-control " name="item[${dayno}][]" value="${item}" placeholder="What is planned for this time ?">-->
						<textarea placeholder="What is planned for this time ?" name="item[${dayno}][]" class="form-control simpleeditormin">${item}</textarea>
					</div>
					<div class="col-sm-1">
						<input type="button" class="btn btn-default removetime" value=" X "  placeholder="Remove">
					</div>
				</div>
		</script>
		<div class="row form-group">
		  <div class="col-sm-12 text-center">
		      <input type="button" data-id="<?=((count($htmlblocks['itenarary'][0]['data']))?count($htmlblocks['itenarary'][0]['data']):4)?>" class="btn btn-primary addmoretmpl" value="Add More Days" />
		      <input type="submit" name="submit" class="btn btn-primary" value="Submit" />
		  </div>
		</div>
	</form>
     </div>
     <div class="row form-group customblock displayblock hide">
        <div class="col-sm-12">
             displayblock
        </div>
     </div>
      
</div>
</div>
<?php
$datain = $htmlblocks['itenarary'][0]['data'];
if($datain)
{
	$data = $htmlblocks['itenarary'][0]['data'];
}
else
{
	$data = array();
	for($i=1;$i<=2;$i++)
	{
		$temp = array();
		$temp['dayno'] = $i;
		for($j=1;$j<=2;$j++)
		{
			$temp2 = array();
			$temp2['time'] = '';
			$temp2['item'] = '';
			$temp2['dayno'] = $i;
			$temp['itemlist'][] = $temp2;
		}
		$data[] = $temp;
	}
}
?>
<script>
$(document).ready(function(){
	$("#displayelements").on('change',function(){
		$(".displayblock").addClass('hide');
		if($(this).val() != '')
		{
			$("."+$(this).val()).removeClass('hide');
		}
	});
	// $("#daytemplate").tmpl([{dayno:1}]).appendTo("#itenararycontainer");
	$("#daytemplate").tmpl(<?=json_encode($data)?>).appendTo("#itenararycontainer");
	$(".addmoretmpl").on('click',function(){
		$currentid = $(this).attr('data-id');
		$newid = parseInt($currentid) + 1;
		$(this).attr('data-id',$newid);
		$("#daytemplate").tmpl([{dayno:$newid,itemlist:[{"time":"","item":"","dayno":$newid}]}]).appendTo("#itenararycontainer");
		summernote_callback();
	});
	$("body").on('click','.addmoretmpl2',function(){
		$("#timetemplate").tmpl([{dayno:$(this).data('dayno')}]).appendTo("#itemlist_"+$(this).data('dayno'));
		summernote_callback();
	});
	$("body").on("click",'.removetime',function(){
		$(this).parents('.timerow').remove();
	})
});
</script>
<!--<script type="text/javascript" src="https://passionstreet.ini/passionstreet3/js/jquery.tmpl.js"></script>-->
<?php
$PSJsincludes['internal'][] = 'js/jquery.tmpl.js';
$PSJsincludes['external'][] = 'js/summernote-master/dist/summernote.js';
$PSCssincludes['external'][] = "js/summernote-master/dist/summernote.css";
?>