<?php
if(!($_GET['entityType'] == 'event' || $_GET['entityType'] == 'tour'))
return;
$producttype = $_GET['entityType'];
$eventid = $_GET['entityId'];

if(!($eventid) ||  !($event = event_get_details($eventid,1,PROFILE_ID,$producttype,true))){
    return;
}
else if($event['creator'] != $_SESSION['user']['profile_id']){
    return;
}
if($_POST)
{
    $userlist = array();
	if($_REQUEST['detaileduserlist'])
	$userlist = json_decode($_REQUEST['detaileduserlist'],true);
	foreach($userlist['custom'] as $userdata)
	{
	$insertfields = array();
	$insertfields['invite_id'] = generate_id('evinv');
	$insertfields['profile_id'] = 'NA';
	$insertfields['email'] = $userdata['value'];
	$insertfields['productid'] = $_REQUEST['entityId'];
	$insertfields['producttype'] = $_REQUEST['entityType'];
	$insertfields['invitedby'] = $_SESSION['user']['profile_id'];
	$insertfields['dateadded'] = date("Y-m-d H:i:s");
	$insertfields['invitetype'] = 'eventmembership';
	$insertfields['status'] = 0;
	event_invite_add($insertfields);
	}
	foreach($userlist['autosuggest'] as $userdata)
	{
	$insertfields = array();
	$insertfields['invite_id'] = generate_id('evinv');
	$insertfields['profile_id'] = $userdata['value'];
	$insertfields['email'] = 'NA';
	$insertfields['productid'] = $_REQUEST['entityId'];
	$insertfields['producttype'] = $_REQUEST['entityType'];
	$insertfields['invitedby'] = $_SESSION['user']['profile_id'];
	$insertfields['dateadded'] = date("Y-m-d H:i:s");
	$insertfields['invitetype'] = 'eventmembership';
	$insertfields['status'] = 0;
	event_invite_add($insertfields);
	}
	//event_invite_add($insertfields);
}
$where = array();
$where['productid'] = $event['event_id'];
$where['producttype'] = $event['producttype'];

$invites = event_invite_get_list($where);
$PSModData['invites'] = $invites;
?>