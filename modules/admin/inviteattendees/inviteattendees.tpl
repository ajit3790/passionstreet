<form class="submit-invite" method="post" action="<?=$_SERVER['PHP_BROWSER_URL']?>">
<div class="row form-group customcheckbox1">
	<div class="col-xs-12 text-center">
		<h3>Select from your network or invite by email</h3>
	</div>
	<div class="tagbox">
		<input class="inviteelist as-original" name="inviteelist" type="text" value="" placeholder="Search your followers & following , invite by email" />
	</div>
	<input type="button" class="btn btn-default" value="Select from Network" onclick="autoFillInvitees(true)"/>
</div>
<div class="row form-group">
  <div class="col-sm-12 text-center">
	  <button type="submit" name="submit" class="btn btn-primary submitbtn" value="Invite" >Invite</button>
  </div>
</div>
</form>
<?php
if($invites)
{
?>
<div class="row">
<div class="col-xs-12 text-center">
<div class="invites">
<br />
<h4>Invited Members</h4>
<?php
foreach($invites as $invite)
{
	if($invite['profile_id'] == 'NA')
	$displaytext = $invite['email'];
	else
	$displaytext = $invite['fname'].' '.$invite['lname'];
	echo '<span class="invite" style="background-color:#ededed;color:#000;padding:5px;margin-right:5px;margin-bottom:5px;display:inline-block;">'.$displaytext.'</span>';
}
?>
</div>
</div>
</div>
<?php
}
?>
<script>
var $mynetwork = [];
$(document).ready(function(){
	delayfunction2(['jquery.autoSuggest.js'],'getUsersInMyNetwork',['mynetwork'],1000);
	$('.submitbtn').on('click',function(event){
		var $thisform = $('.submit-invite');
		$detaileduserlist = getAddedTagsv2($('.tagbox'));
		if($detaileduserlist['custom'].length ==0 && $detaileduserlist['autosuggest'].length ==0 )
		{
		showToolTipMsg($('.as-input'),"Please add a member to invite");
		event.preventDefault();
		return false;
		}
		if($("#detaileduserlist").length == 0)
		$thisform.append("<input id='detaileduserlist' type='hidden' name='detaileduserlist' value='"+JSON.stringify($detaileduserlist)+"'>");
		else
		$("#detaileduserlist").val(JSON.stringify($detaileduserlist));
		//$thisform.submit();
	});
});
function mynetwork($data){
	$mynetwork = $data;
	autoFillInvitees(false);
}
function autoFillInvitees($prefill){
	$(".tagbox").html('<input class="inviteelist as-original"  name="inviteelist" type="text" value="" placeholder="Search your followers & following , invite by email" />');
	if($prefill == true)
	{
		var $opt = {};
		$opt['preFill'] = $mynetwork;
		$opt['onSelection'] = function(data){
			return ((data['text'] == data['value'])?isValidEmailAddress(data['value']):true);
		}
		autoSuggestUser($('.inviteelist'),$mynetwork,$opt);
	}
	else
	{
		var $opt = {};
		$opt['onSelection'] = function(data){
			return ((data['text'] == data['value'])?isValidEmailAddress(data['value']):true);
		}
		autoSuggestUser($('.inviteelist'),$mynetwork,$opt);
	}
}

</script>
<?php
$PSJsincludes['external'][] = "js/jquery.autoSuggest.js";
$PSCssincludes['external'][] = "css/jquery.autoSuggest.css";
?>