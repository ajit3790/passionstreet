<?php
if(!($_GET['entityType'] == 'event' || $_GET['entityType'] == 'tour'))
return;
$producttype = $_GET['entityType'];
$eventid = $_GET['entityId'];

if(!($eventid) ||  !($event = event_get_details($eventid,1,PROFILE_ID,$producttype,true))){
    return;
}
else if($event['creator'] != $_SESSION['user']['profile_id']){
    return;
}
if($_POST)
{
	$inputParams['eventinfo']['donations']['allow'] = (($_POST['allowdonation'])?1:0);
	$inputParams['eventinfo']['donations']['enddate'] = ($inputParams['eventinfo']['donations']['allow'])?generate_standard_date($_POST['enddate']):'';
	event_update($eventid,$inputParams);
}
$event = event_get_details($eventid,1,PROFILE_ID,$producttype,true);
$PSModData['donation'] = $event['eventinfo']['donations'];
?>