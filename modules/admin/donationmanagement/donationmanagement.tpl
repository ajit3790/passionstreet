<form class="submit-invite" method="post" action="<?=$_SERVER['PHP_BROWSER_URL']?>">
<div class="row form-group customcheckbox1">
	<div class="col-xs-12 text-center">
		<h3>Choose below if a donation box need to appear on event page or not</h3>
		<br />
		<br />
	</div>
</div>
<div class="row form-group customcheckbox">
	<div class="col-xs-4">
		Allow Donations ? 
	</div>
	<div class="col-xs-8">
		<label class="switch"><input type="checkbox" name="allowdonation" id="adddonations" <?=(($donation['allow'])?'checked':'')?>><span class="switchslider round"></span></label>
	</div>
</div>
<div class="donationbox <?=(($donation['allow'])?'':'hide')?>">
<div class="row form-group customcheckbox">
	<div class="col-xs-4">
		Last date of accepting donations 
	</div>
	<div class="col-xs-8 date-element">
		<div class="form-group date">
			  <!--<i class="fa fa-calendar"></i>-->
			  <input type="text" name="enddate" value="<?=$donation['enddate']?>" placeholder="" id="donationcalender" data-type="datetimepicker" class="form-control date date-future datepicker">
		</div>
	</div>
</div>
</div>
<div class="row form-group">
  <div class="col-sm-12 text-center">
	  <button type="submit" name="submit" class="btn btn-primary submitbtn" value="Submit" >Submit</button>
  </div>
</div>
</form>
<script>
$(document).ready(function(){
	$("#adddonations").on('change',function(){
		if ($(this).is(':checked'))
		{
		$('.donationbox').removeClass('hide');
		$("#donationcalender").attr('required','true');
		}
		else
		{
		$('.donationbox').addClass('hide');
		$("#donationcalender").removeAttr('required');
		}
	});
});
</script>
<?php
$PSJsincludes['external'][] = "js/bootstrapdatetimepicker.min.js";
?>