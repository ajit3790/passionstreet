<?php
$ispaidevent = (($PSParams['eventdefault']['typesbasedonfees'][$event['eventtypebyfee']]['processingfee'] + $PSParams['eventdefault']['typesbasedonfees'][$event['eventtypebyfee']]['gatewayfee']) > 0);
?>

<div class="row"><div class="col-sm-12">
	<div class="help-text">
    	<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
	    <ul>
	    	<!--<li>select “Yes” if user is required to pay to participate</li>-->
	    	<li>define ticket category, price and allocate no of tickets you want to sell</li>
	    	<li>You may add more ticket categories if required</li>
	    	<!--<li>In case of “No” ticket buying, define last date of registration entry</li>-->
	    </ul>	
	</div>
	<br />
</div>
</div>
<div class="row"><div class="col-sm-12">

<form class="form2 event-manage clearfix" method="post"  action="<?=$_SERVER['PHP_BROWSER_URL']?>" id="form-step1">
<?php
/*
?>
<div class="row customcheckbox">
    <?php
    foreach($PSParams['eventdefault']['typesbasedonfees'] as $id=>$typedata)
    {
    $isPaidType = (($typedata['processingfee'] + $typedata['gatewayfee']) > 0)
    ?>
    <div class="col-sm-4 col-xs-4">
        <label>
            <input name="eventtypebyfees" class="eventtypebyfees" data-value="<?=(($isPaidType)?'paid':'free')?>" value="<?=$id?>" type="radio" <?=(($event['eventtypebyfee'])?(($id == $event['eventtypebyfee'])?'checked':''):(($typedata['name'] == 'PAID')?'checked':''))?>>
            <div class="tab-data">
                <?=$typedata['name']?>
            </div>
        </label>
    </div>
    <?php
    }
    ?>
</div>
<?php
*/
?>
<!-- Ticket - info -->
<div class="tickets-price <?=($ispaidevent)?'':'hide'?>" >
    <div class="ticket-price-row-box">
	<script id="tickettemplate" type="text/x-jQuery-tmpl">
	<!--${JSON.stringify($data)}-->
	<div class="ticket-price-row row1 clearfix" >
		<div class="panel-group" id="accordion">
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapse${seq}">
				<div class="row">
				<input type="hidden" class="ticketcodee" name="ticketcode[]" value="${code}">
				<div class="col-md-3 col-sm-3"><input type="text" name="ticketfeecategory[]" value="${name}" placeholder="Ticket Name" class="form-control" required></div>
				<div class="col-md-3 col-sm-3 ticketamountbox"><input type="text" name="ticketfeefee[]" value="${price}" placeholder="Ticket Price" class="form-control"  required></div>
				<div class="col-md-3 col-sm-3 bookingamountbox hide"><input type="text" name="ticketfeebookingfee[]" value="${bookingprice}" placeholder="Booking Amount" class="form-control"></div>
				<span class="pull-right">
					<i class="fa fa-cog" title="Settings" onclick="$(this).parents('.ticket-price-row').find('.extrafields').toggleClass('hide')"></i><i class="fa fa-trash" onclick="$(this).parents('.ticket-price-row').remove()"></i>
				</span>
				</div>
			</a>
		      </h4>
		    </div>
		    <div id="collapse${seq}" class="panel-collapse collapse">
		      <div class="panel-body">
			<div class="row">
				<label class="col-md-3 col-sm-3 control-label" for="inputWarning">
					Ticket Detail
				</label>
				<div class="col-md-9 col-sm-9">
					<input type="text" name="ticketfeedescription[]" value="${description}" placeholder="Define Ticket Description"  class="form-control">
				</div>
			</div>
			<div class="row">
				<label class="col-md-3 col-sm-3 control-label" for="inputWarning">
					Min tickets user can buy
				</label>
				<div class="col-md-2 col-sm-2">
					<input type="number" name="ticketfeeminbuycount[]" placeholder="Minimum buy count" value="${minbuy}"  class="form-control">
				</div>
				<div class="col-md-2 col-sm-2">&nbsp;</div>
			
				<label class="col-md-3 col-sm-3 control-label" for="inputWarning">
					Max tickets user can buy
				</label>
				<div class="col-md-2 col-sm-2">
					<input type="number" name="ticketfeemaxbuycount[]" placeholder="Maximum buy count" value="${maxbuy}" class="form-control">
				</div>
				
				<label class="col-md-3 col-sm-3 control-label" for="inputWarning">
					BIB / SR no. initial
				</label>
				<div class="col-md-2 col-sm-2">
					<input type="text" name="ticketfeebibintial[]" placeholder="Initials for SR no." value="${bibinitial}" class="form-control">
				</div>
				<div class="col-md-2 col-sm-2">&nbsp;</div>
				
				<label class="col-md-3 col-sm-3 control-label" for="inputWarning">
					BIB / SR no.  starts with
				</label>
				<div class="col-md-2 col-sm-2">
					<input type="number" name="ticketfeebibseqstrt[]" placeholder="Set start sequence for SR no." value="${bibstart}"  class="form-control">
				</div>
				
				<label class="col-md-3 col-sm-3 control-label" for="inputWarning">
					Enter total no.of Tickets
				</label>
				<div class="col-md-2 col-sm-2">
					<input type="number" name="ticketfeeticketss[]" value="${totalcount}" placeholder="Enter no.of Tickets"  class="form-control"  required>
				</div>
				<div class="col-md-2 col-sm-2">&nbsp;</div>
				<label class="col-md-3 col-sm-3 control-label" for="inputWarning">
					Minimum age of participation
				</label>
				<div class="col-md-2 col-sm-2">
					<input type="number" name="ticketfeeage[]" value="${minage}" placeholder="Minimum age of participation"  class="form-control"  required>
				</div>
			</div>
			
		      </div>
		    </div>
		  </div>
		</div>
	</div>
	</script>
    </div>
    <div class="row ">
        <div class="col-sm-12 form-group"> <a class="more-ticket-categories1 addmoretickets btn btn-default  pull-right"> + Add More Tickets</a> </div>
    </div>
    
    <div class="row lastprebookingdatebox">
        <div class="col-sm-12 form-group"> 
		Last date of Advance Booking closes 
		<select name="lastprebookingdate" class="form-control" style="display:inline;width:auto;">
			<option value="7" <?=($event['lastprebookingdate'] == 7)?'selected':''?>>7 days </option> 
			<option value="15" <?=($event['lastprebookingdate'] == 15)?'selected':''?>>15 days </option> 
			<option value="30" <?=($event['lastprebookingdate'] == 30)?'selected':''?>>30 days </option> 
		</select> 
		 before date of booking 
	</div>
	<div class="col-sm-12 form-group"> 
		&nbsp;
	</div>
    </div>
    
    <div class="row funkyradio">
	<div class="col-md-12 col-sm-12"><h5>Organiser's Service Tax</h5></div>
	<div class="col-sm-6 form-group ">
		<div class="funkyradio-success">
		    <input type="radio" name="servicetaxincluded" value="1" id="servicetaxincludedyes" required="" <?=(($allowedit == 0)?'onclick="javascript: return false;"':'')?> <?=((!isset($event['servicetaxincluded']) || $event['servicetaxincluded'] == 1)?'checked':'')?> />
		    <label for="servicetaxincludedyes">Ticket Price Includes Service Tax</label>
		</div>
	</div>
	<div class="col-sm-6 form-group ">
		<div class="funkyradio-success">
		    <input type="radio" name="servicetaxincluded" value="0" id="servicetaxincludedno" required=""  <?=(($allowedit == 0)?'onclick="javascript: return false;"':'onclick="toggleOrganiserServicetaxBox"')?> <?=((isset($event['servicetaxincluded']) && $event['servicetaxincluded'] == 0)?'checked':'')?> />
		    <label for="servicetaxincludedno">Charge service tax from user</label>
		    <div  id="organisersservicetaxbox" class="<?=((isset($event['servicetaxincluded']) && $event['servicetaxincluded'] == 0)?'':'hide')?>">
			@ <input type="text" class="form-control" id="organisersservicetax" name="organisersservicetax" style="max-width:55.5px;display:inline;" value="<?=(isset($event['organisersservicetax'])?$event['organisersservicetax']:0)?>" /> %
		    </div>
		</div>
	</div>
    </div>
    
    
    <div class="row funkyradio form-group">
        <div class="col-md-12 col-sm-12"><h5>Select Commission Payout Option</h5></div>
        <?php
        foreach($PSParams['eventdefault']['feepaymentoptions'] as $id=>$value){
            echo    '<div class="radio1 form-group1 col-sm-6 col-md-6 funkyradio-success">
                            <input id="payout'.$id.'"name="eventfeepaymentoptions" value="'.$id.'" required="" '.(($id == $event['feepaymentoption'])?'checked':(($id==1)?'checked="true"':'')).' type="radio" '.(($allowedit == 0)?'onclick="javascript: return false;"':'').'>
                            <label for="payout'.$id.'">'.$value["name"].'</label>
                    </div>';
        }
        ?>
    </div>
	
	<div class="row  form-group hide">
		<div class="col-md-12 col-sm-12"><h5>Addition Product</h5></div>
        <div class="col-sm-6 form-group ">
		  <input type="text" value="<?=$event['extrapricing']['name']?>" name="extrapricing[name]" placeholder="Is there any addition product to be added" class="form-control" <?=($editmode == 1 && $allowedit != 1)?'readonly':''?>>
	  </div>
	  <div class="col-sm-6 form-group ">
		  <input type="text" value="<?=$event['extrapricing']['price']?>" name="extrapricing[price]" placeholder="Price" class="form-control" <?=($editmode == 1 && $allowedit != 1)?'readonly':''?>>
	  </div>
	</div>
    
    <div class="row">
        <div class="col-sm-12 text-center">
            <button value="submit" name="submit" type="submit" class="btn btn-primary">Save & Proceed</button>
        </div>
    </div>
</div>
<!-- /Ticket -info -->
</form>
</div>
</div>
<?php
$PSJsincludes['internal'][] = "js/eventpublish.js";
//$PSJsincludes['external'][] = "js/jquery.datetimepicker.js";
//$PSCssincludes['external'][] = "css/jquery.datetimepicker.css";
$PSJsincludes['external'][] = "js/bootstrapdatetimepicker.min.js";
$PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
$currentstatus = 1;
if(strtolower($event['tickettype']) == 'paid' && empty($event['ticketcategories']))
$currentstatus = 0;
?>
<script>
$(document).ready(function(){
        executeFunctionByName('parent.togglePublishComponentStatus',['eventticketing',<?=$currentstatus?>]);
});
</script>
<style>
.funkyradio div {
  overflow: hidden;
}

.funkyradio label {
  width: 100%;
  border-radius: 3px;
  border: 1px solid #D1D3D4;
  font-weight: normal;
}

.funkyradio input[type="radio"]:empty,
.funkyradio input[type="checkbox"]:empty {
  display: none;
}

.funkyradio input[type="radio"]:empty ~ label,
.funkyradio input[type="checkbox"]:empty ~ label {
  position: relative;
  line-height: 16px;
  padding:10px;
  padding-left:45px;
  margin-top: 0.5em;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

.funkyradio input[type="radio"]:empty ~ label:before,
.funkyradio input[type="checkbox"]:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #D1D3D4;
  border-radius: 3px 0 0 3px;
  padding-top:10px;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
  color: #888;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;
}

.funkyradio input[type="radio"]:checked ~ label,
.funkyradio input[type="checkbox"]:checked ~ label {
  color: #777;
}

.funkyradio input[type="radio"]:checked ~ label:before,
.funkyradio input[type="checkbox"]:checked ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #333;
  background-color: #ccc;
}

.funkyradio input[type="radio"]:focus ~ label:before,
.funkyradio input[type="checkbox"]:focus ~ label:before {
  box-shadow: 0 0 0 3px #999;
}

.funkyradio-default input[type="radio"]:checked ~ label:before,
.funkyradio-default input[type="checkbox"]:checked ~ label:before {
  color: #333;
  background-color: #ccc;
}

.funkyradio-primary input[type="radio"]:checked ~ label:before,
.funkyradio-primary input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #337ab7;
}

.funkyradio-success input[type="radio"]:checked ~ label:before,
.funkyradio-success input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5cb85c;
}
.ticket-price-row-box label{font-size:11px;line-height:29px;}
.panel-collapse .control-label,.panel-collapse .form-control{margin-bottom:5px;}
</style>
<?php
$ticketcategories = $event['ticketcategories'];
$data = array();
$i=0;
$ticketcategoriestemp = $ticketcategories;
$ticketcategoriestemp[] = array();
foreach($ticketcategoriestemp as $tktcode=>$ticketdata)
{
$i++;
$dataticket = array();
$dataticket['seq'] 		= $i;
$dataticket['name'] 		= $ticketdata['name'];
$dataticket['totalcount'] 	= ($ticketdata['totalcount'])?$ticketdata['totalcount']:10000;
$dataticket['price'] 		= $ticketdata['price'];
$dataticket['code'] 		= $tktcode;
$dataticket['bookingprice'] 	= $ticketdata['bookingprice'];
$dataticket['minbuy'] 		= ($ticketdata['minbuy'])?$ticketdata['minbuy']:1;
$dataticket['maxbuy'] 		= ($ticketdata['maxbuy'])?$ticketdata['maxbuy']:10;
$dataticket['bibinitial'] 	= $ticketdata['bibinitial'];
$dataticket['bibstart'] 	= $ticketdata['bibstart'];
$dataticket['description'] 	= $ticketdata['description'];
$dataticket['minage'] 		= ($ticketdata['agerestrict'])?$ticketdata['agerestrict']:7;
$data[] = $dataticket;
}
$PSJavascript['addmoreticketvalues'] = $dataticket;
if($ticketcategories)
unset($data[count($data) - 1]);
?>
<script>
$(document).ready(function(){
	$('body').on('click','.panel-heading input',function(e){e.preventDefault();e.stopPropagation();});
	$('body').on('click','.fa-trash',function(e){e.preventDefault();e.stopPropagation();$(this).parents('.panel-group').eq(0).remove();});
	$("#tickettemplate").tmpl(<?=json_encode($data)?>).appendTo(".ticket-price-row-box");
	$(".addmoretickets").on('click',function(){
		addmoreticketvalues['seq']++;
		$("#tickettemplate").tmpl([addmoreticketvalues]).appendTo(".ticket-price-row-box");
	});
});
</script>
<?php
$PSJsincludes['internal'][] = 'js/jquery.tmpl.js';
?>