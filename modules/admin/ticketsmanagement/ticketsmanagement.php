<?php
if(!($_GET['entityType'] == 'event' || $_GET['entityType'] == 'tour'))
return;
$producttype = $_GET['entityType'];
$eventid = $_GET['entityId'];

if(!($eventid) ||  !($event = event_get_details($eventid,1,PROFILE_ID,$producttype,true))){
    return;
}
else if($event['creator'] != $_SESSION['user']['profile_id']){
    return;
}
$PSModData['allowedit'] = 0;
if($event['status'] != 'published'){
    $PSModData['allowedit'] = 1;
}

if($_POST['submit']){
    extract($_POST);
    $inputParams['tickettype'] = 'paid';
    $maxticketprice = -1;
    $minticketprice = -1;
    foreach($ticketfeecategory as $key=>$value)
    {
        if($ticketfeecategory[$key] && $ticketfeefee[$key] && $ticketfeeticketss[$key] )
        {
            $temp = array();
            $temp['tktcatname'] = $ticketfeecategory[$key];
            $temp['tktcatprice'] = $ticketfeefee[$key];

            if($temp['tktcatprice'] < $minticketprice)
            $minticketprice = $temp['tktcatprice'];
            else if($minticketprice == -1)
            $minticketprice = $temp['tktcatprice'];

            if($temp['tktcatprice'] > $maxticketprice)
            $maxticketprice = $temp['tktcatprice'];
            else if($maxticketprice == -1)
            $maxticketprice = $temp['tktcatprice'];

            $temp['tktcatcount'] = $ticketfeeticketss[$key];
            $temp['tktcatminbuy'] = ($ticketfeeminbuycount[$key])?$ticketfeeminbuycount[$key]:1;
            $temp['tktcatmaxbuy'] = ($ticketfeemaxbuycount[$key])?$ticketfeemaxbuycount[$key]:10;
            $temp['tktcatbibintial'] = $ticketfeebibintial[$key];
            $temp['tktcatbibstart'] = $ticketfeebibseqstrt[$key];
            $temp['tktcatbookamount'] = $ticketfeebookingfee[$key];
            $temp['tktcateventid'] = $eventid;
            $temp['description'] = $ticketfeedescription[$key];
            $temp['agerestrict'] = (int)$ticketfeeage[$key];
	    if($ticketcode[$key])
            ticketcategories_update($ticketcode[$key],$temp);
	    else
            ticketcategories_add($temp);
            $ticketCategories[] = $temp;
            $maxticketcount = $maxticketcount  +  $ticketfeeticketss[$key];
        }
        $maxticketprice = ($maxticketprice == -1)?0:$maxticketprice;
	$minticketprice = ($minticketprice == -1)?0:$minticketprice;
	$inputParams['ticketvaluerange'] = $minticketprice.','.$maxticketprice;
    
    }
    if($PSModData['allowedit'])
    {
	$inputParams['eventinfo']['feepaymentoption'] = $eventfeepaymentoptions;
	$inputParams['eventinfo']['servicetaxincluded'] = $servicetaxincluded;
	$inputParams['eventinfo']['organisersservicetax'] = $organisersservicetax;
    }
    $inputParams['eventinfo']['lastprebookingdate'] = $lastprebookingdate;
	if($extrapricing && $extrapricing['name'] && $extrapricing['price'])
	{
	$extrapricing['price'] =  (int)$extrapricing['price'];
    $inputParams['eventinfo']['extrapricing'] = $extrapricing;
	}
	event_update($eventid,$inputParams);
    $event = event_get_details($eventid,1,PROFILE_ID,$producttype,true);
}
unset($event['ticketcategories']['leadcapture']);
$PSModData['event'] = $event;
$PSModData['producttype'] = $producttype;
?>