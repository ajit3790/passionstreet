<?php
$ispaidevent = (($PSParams['eventdefault']['typesbasedonfees'][$event['eventtypebyfee']]['processingfee'] + $PSParams['eventdefault']['typesbasedonfees'][$event['eventtypebyfee']]['gatewayfee']) > 0);
?>
<form class="form1 event-manage clearfix" method="post"  action="<?=$_SERVER['PHP_BROWSER_URL']?>" id="form-step1">
<?php
/*
?>
<div class="row customcheckbox">
    <?php
    foreach($PSParams['eventdefault']['typesbasedonfees'] as $id=>$typedata)
    {
    $isPaidType = (($typedata['processingfee'] + $typedata['gatewayfee']) > 0)
    ?>
    <div class="col-sm-4 col-xs-4">
        <label>
            <input name="eventtypebyfees" class="eventtypebyfees" data-value="<?=(($isPaidType)?'paid':'free')?>" value="<?=$id?>" type="radio" <?=(($event['eventtypebyfee'])?(($id == $event['eventtypebyfee'])?'checked':''):(($typedata['name'] == 'PAID')?'checked':''))?>>
            <div class="tab-data">
                <?=$typedata['name']?>
            </div>
        </label>
    </div>
    <?php
    }
    ?>
</div>
<?php
*/
?>
<!-- Ticket - info -->
<div class="tickets-price <?=($ispaidevent)?'':'hide'?>" >
    <?php
    $ticketcategories = $event['ticketcategories'];
    if(empty($ticketcategories))
    {
        $ticketcategories[''] = array();
    }
    foreach($ticketcategories as $tktcode=>$ticketdata)
    {
    ?>
    <div class="ticket-price-row ">
        <div class="row">
            <div class="col-sm-3">
                <label class="form-group control-label">Ticket Type</label>
                <input type="hidden" name="ticketcode[]" value="<?=$ticketdata['name']?>">
                <input type="text" name="ticketfeecategory[]" value="<?=$ticketdata['name']?>" placeholder="Define Ticket Name"  class="form-control" required>
            </div>
            <div class="col-sm-3">
                <label class="form-group control-label">Rate per Ticket</label>
                <input type="text" name="ticketfeefee[]" value="<?=$ticketdata['price']?>" placeholder="Ticket Price In INR"  class="form-control"  required>
            </div>
            <div class="col-sm-3">
                <label class="form-group  control-label">Allocate Tickets</label>
                <input type="text" name="ticketfeeticketss[]" value="<?=$ticketdata['totalcount']?>" placeholder="Enter no.of Tickets"  class="form-control"  required>
            </div>
            <div class="col-sm-3 tools">
                <i class="fa fa-cog" title="Settings" onclick="$(this).parents('.ticket-price-row').find('.extrafields').toggleClass('hide')"></i><i class="fa fa-trash" onclick="$(this).parents('.ticket-price-row').remove()"></i>
            </div>
        </div>
        <div class="row extrafields hide clearfix" >
              <div class="col-sm-3">
                <label class="form-group control-label">Min Ticket user can buy</label>
                <input type="text" name="ticketfeeminbuycount[]" placeholder="Minimum buy count" value="<?=(($ticketdata['minbuy'])?$ticketdata['minbuy']:1)?>"  class="form-control">
              </div>
             <div class="col-sm-3">
                <label class="form-group control-label">Max Tickets user can buy</label>
                <input type="text" name="ticketfeemaxbuycount[]" placeholder="Maximum buy count" value="<?=(($ticketdata['maxbuy'])?$ticketdata['maxbuy']:10)?>" class="form-control">
             </div>
             <div class="col-sm-3">
                <label class="form-group  control-label">BIB / SR no. initial</label>
                <input type="text" name="ticketfeebibintial[]" placeholder="Initials for SR no." value="<?=$ticketdata['bibinitial']?>" class="form-control">
             </div>
             <div class="col-sm-3">
                <label class="form-group  control-label">BIB / SR no. Starts with</label>
                <input type="text" name="ticketfeebibseqstrt[]" placeholder="Set start sequence for SR no." value="<?=$ticketdata['bibstart']?>"  class="form-control">
             </div>
        </div>
    </div>
    <?php
    }
    ?>
    <div class="ticket-price-row-box"></div>
    <div class="row ">
        <div class="col-sm-12 form-group"> <a class="more-ticket-categories"> + Add More Tickets</a> </div>
    </div>
    <div class="row ">
        <div class="col-md-12 col-sm-12"><h5>Select Commission Payout Option</h5></div>
        <?php
        foreach($PSParams['eventdefault']['feepaymentoptions'] as $id=>$value){
            echo    '<div class="radio form-group col-sm-6 col-md-6">
                        <label>
                            <input name="eventfeepaymentoptions" value="'.$id.'" required="" '.(($id == $event['feepaymentoption'])?'checked':(($id==1)?'checked="true"':'')).' type="radio">
                           '.$value["name"].'</label>
                    </div>';
        }
        ?>
    </div>
    
    <div class="row">
        <div class="col-sm-12 text-center">
            <button value="submit" name="submit" type="submit" class="btn btn-primary">Save & Proceed</button>
        </div>
    </div>
</div>
<!-- /Ticket -info -->
</form>

<?php
$PSJsincludes['internal'][] = "js/eventpublish.js";
//$PSJsincludes['external'][] = "js/jquery.datetimepicker.js";
//$PSCssincludes['external'][] = "css/jquery.datetimepicker.css";
$PSJsincludes['external'][] = "js/bootstrapdatetimepicker.min.js";
$PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
$currentstatus = 1;
if(strtolower($event['tickettype']) == 'paid' && empty($event['ticketcategories']))
$currentstatus = 0;
?>
<script>
$(document).ready(function(){
        executeFunctionByName('togglePublishComponentStatus',['eventticketing',<?=$currentstatus?>]);
});
</script>
