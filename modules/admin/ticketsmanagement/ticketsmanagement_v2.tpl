<?php
$ispaidevent = (($PSParams['eventdefault']['typesbasedonfees'][$event['eventtypebyfee']]['processingfee'] + $PSParams['eventdefault']['typesbasedonfees'][$event['eventtypebyfee']]['gatewayfee']) > 0);
?>

<div class="row"><div class="col-sm-8">

<form class="form1 event-manage clearfix" method="post"  action="<?=$_SERVER['PHP_BROWSER_URL']?>" id="form-step1">
<?php
/*
?>
<div class="row customcheckbox">
    <?php
    foreach($PSParams['eventdefault']['typesbasedonfees'] as $id=>$typedata)
    {
    $isPaidType = (($typedata['processingfee'] + $typedata['gatewayfee']) > 0)
    ?>
    <div class="col-sm-4 col-xs-4">
        <label>
            <input name="eventtypebyfees" class="eventtypebyfees" data-value="<?=(($isPaidType)?'paid':'free')?>" value="<?=$id?>" type="radio" <?=(($event['eventtypebyfee'])?(($id == $event['eventtypebyfee'])?'checked':''):(($typedata['name'] == 'PAID')?'checked':''))?>>
            <div class="tab-data">
                <?=$typedata['name']?>
            </div>
        </label>
    </div>
    <?php
    }
    ?>
</div>
<?php
*/
?>
<!-- Ticket - info -->
<div class="tickets-price <?=($ispaidevent)?'':'hide'?>" >
    <div class="ticket-price-row-box">
    <?php
    $ticketcategories = $event['ticketcategories'];
    if(empty($ticketcategories))
    {
        $ticketcategories[''] = array();
    }
    foreach($ticketcategories as $tktcode=>$ticketdata)
    {
    ?>
    <div class="ticket-price-row row clearfix" >
        <div class="ticket-row-control">
		<i class="fa fa-cog" title="Settings" onclick="$(this).parents('.ticket-price-row').find('.extrafields').toggleClass('hide')"></i><i class="fa fa-trash" onclick="$(this).parents('.ticket-price-row').remove()"></i>
	</div>
	    <div class="col-sm-3">
                <label class="form-group control-label">Ticket Type</label>
                <input type="hidden" class="ticketcodee" name="ticketcode[]" value="<?=$tktcode?>">
                <input type="text" name="ticketfeecategory[]" value="<?=$ticketdata['name']?>" placeholder="Define Ticket Name"  class="form-control" required>
            </div>
            <div class="col-sm-3">
                <label class="form-group  control-label">Allocate Tickets</label>
                <input type="text" name="ticketfeeticketss[]" value="<?=$ticketdata['totalcount']?>" placeholder="Enter no.of Tickets"  class="form-control"  required>
            </div>
	    <div class="col-sm-3 ticketamountbox">
                <label class="form-group control-label">Ticket Price</label>
                <input type="text" name="ticketfeefee[]" value="<?=$ticketdata['price']?>" placeholder="Ticket Price In INR"  class="form-control"  required>
            </div>
	    <div class="col-sm-3 bookingamountbox" style="visibility:hidden">
                <label class="form-group control-label">Booking Amount</label>
                <input type="text" name="ticketfeebookingfee[]" value="<?=$ticketdata['bookingprice']?>" placeholder="Advance Booking Price In INR"  class="form-control">
            </div>
            
        <div class="extrafields hide" >
              <div class="col-sm-3">
                <label class="form-group control-label">Min Ticket user can buy</label>
                <input type="text" name="ticketfeeminbuycount[]" placeholder="Minimum buy count" value="<?=(($ticketdata['minbuy'])?$ticketdata['minbuy']:1)?>"  class="form-control">
              </div>
             <div class="col-sm-3">
                <label class="form-group control-label">Max Tickets user can buy</label>
                <input type="text" name="ticketfeemaxbuycount[]" placeholder="Maximum buy count" value="<?=(($ticketdata['maxbuy'])?$ticketdata['maxbuy']:10)?>" class="form-control">
             </div>
             <div class="col-sm-3">
                <label class="form-group  control-label">BIB / SR no. initial</label>
                <input type="text" name="ticketfeebibintial[]" placeholder="Initials for SR no." value="<?=$ticketdata['bibinitial']?>" class="form-control">
             </div>
             <div class="col-sm-3">
                <label class="form-group  control-label">BIB / SR no. Starts with</label>
                <input type="text" name="ticketfeebibseqstrt[]" placeholder="Set start sequence for SR no." value="<?=$ticketdata['bibstart']?>"  class="form-control">
             </div>
        </div>
    </div>
    <?php
    }
    ?>
    </div>
    <div class="row ">
        <div class="col-sm-12 form-group"> <a class="more-ticket-categories1 addmore" data-limit=10 data-callback="moreticketsadded" data-trgtelement="ticket-price-row"> + Add More Tickets</a> </div>
    </div>
    
    <div class="row lastprebookingdatebox">
        <div class="col-sm-12 form-group"> 
		Last date of Advance Booking closes 
		<select name="lastprebookingdate" class="form-control" style="display:inline;width:auto;">
			<option value="7" <?=($event['lastprebookingdate'] == 7)?'selected':''?>>7 days </option> 
			<option value="15" <?=($event['lastprebookingdate'] == 15)?'selected':''?>>15 days </option> 
			<option value="30" <?=($event['lastprebookingdate'] == 30)?'selected':''?>>30 days </option> 
		</select> 
		 before date of booking 
	</div>
	<div class="col-sm-12 form-group"> 
		&nbsp;
	</div>
    </div>
    
    <div class="row funkyradio">
	<div class="col-md-12 col-sm-12"><h5>Organiser's Service Tax</h5></div>
	<div class="col-sm-6 form-group ">
		<div class="funkyradio-success">
		    <input type="radio" name="servicetaxincluded" value="1" id="servicetaxincludedyes" required="" <?=(($allowedit == 0)?'onclick="javascript: return false;"':'')?> <?=((!isset($event['servicetaxincluded']) || $event['servicetaxincluded'] == 1)?'checked':'')?> />
		    <label for="servicetaxincludedyes">Ticket Price Includes Service Tax</label>
		</div>
	</div>
	<div class="col-sm-6 form-group ">
		<div class="funkyradio-success">
		    <input type="radio" name="servicetaxincluded" value="0" id="servicetaxincludedno" required=""  <?=(($allowedit == 0)?'onclick="javascript: return false;"':'onclick="toggleOrganiserServicetaxBox"')?> <?=((isset($event['servicetaxincluded']) && $event['servicetaxincluded'] == 0)?'checked':'')?> />
		    <label for="servicetaxincludedno">Charge service tax from user</label>
		    <div  id="organisersservicetaxbox" class="<?=((isset($event['servicetaxincluded']) && $event['servicetaxincluded'] == 0)?'':'hide')?>">
			@ <input type="text" class="form-control" id="organisersservicetax" name="organisersservicetax" style="max-width:55.5px;display:inline;" value="<?=(isset($event['organisersservicetax'])?$event['organisersservicetax']:0)?>" /> %
		    </div>
		</div>
	</div>
    </div>
    
    
    <div class="row funkyradio form-group">
        <div class="col-md-12 col-sm-12"><h5>Select Commission Payout Option</h5></div>
        <?php
        foreach($PSParams['eventdefault']['feepaymentoptions'] as $id=>$value){
            echo    '<div class="radio1 form-group1 col-sm-6 col-md-6 funkyradio-success">
                            <input id="payout'.$id.'"name="eventfeepaymentoptions" value="'.$id.'" required="" '.(($id == $event['feepaymentoption'])?'checked':(($id==1)?'checked="true"':'')).' type="radio" '.(($allowedit == 0)?'onclick="javascript: return false;"':'').'>
                            <label for="payout'.$id.'">'.$value["name"].'</label>
                    </div>';
        }
        ?>
    </div>
	
	<div class="row  form-group">
		<div class="col-md-12 col-sm-12"><h5>Addition Product</h5></div>
        <div class="col-sm-6 form-group ">
		  <input type="text" value="<?=$event['extrapricing']['name']?>" name="extrapricing[name]" placeholder="Is there any addition product to be added" class="form-control" <?=($editmode == 1 && $allowedit != 1)?'readonly':''?>>
	  </div>
	  <div class="col-sm-6 form-group ">
		  <input type="text" value="<?=$event['extrapricing']['price']?>" name="extrapricing[price]" placeholder="Price" class="form-control" <?=($editmode == 1 && $allowedit != 1)?'readonly':''?>>
	  </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12 text-center">
            <button value="submit" name="submit" type="submit" class="btn btn-primary">Save & Proceed</button>
        </div>
    </div>
</div>
<!-- /Ticket -info -->
</form>
</div>
<div class="col-sm-4">
	<div class="help-text">
    	<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
	    <ul>
	    	<li>select “Yes” if user is required to pay to participate</li>
	    	<li>define ticket category, price and allocate no of tickets you want to sell</li>
	    	<li>You may add more ticket categories if required</li>
	    	<li>In case of “No” ticket buying, define last date of registration entry</li>
	    </ul>	
	</div>
</div>
</div>
<?php
$PSJsincludes['internal'][] = "js/eventpublish.js";
//$PSJsincludes['external'][] = "js/jquery.datetimepicker.js";
//$PSCssincludes['external'][] = "css/jquery.datetimepicker.css";
$PSJsincludes['external'][] = "js/bootstrapdatetimepicker.min.js";
$PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
$currentstatus = 1;
if(strtolower($event['tickettype']) == 'paid' && empty($event['ticketcategories']))
$currentstatus = 0;
?>
<script>
$(document).ready(function(){
        executeFunctionByName('parent.togglePublishComponentStatus',['eventticketing',<?=$currentstatus?>]);
});
</script>
<style>
.funkyradio div {
  overflow: hidden;
}

.funkyradio label {
  width: 100%;
  border-radius: 3px;
  border: 1px solid #D1D3D4;
  font-weight: normal;
}

.funkyradio input[type="radio"]:empty,
.funkyradio input[type="checkbox"]:empty {
  display: none;
}

.funkyradio input[type="radio"]:empty ~ label,
.funkyradio input[type="checkbox"]:empty ~ label {
  position: relative;
  line-height: 16px;
  padding:10px;
  padding-left:45px;
  margin-top: 0.5em;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

.funkyradio input[type="radio"]:empty ~ label:before,
.funkyradio input[type="checkbox"]:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #D1D3D4;
  border-radius: 3px 0 0 3px;
  padding-top:10px;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label {
  color: #888;
}

.funkyradio input[type="radio"]:hover:not(:checked) ~ label:before,
.funkyradio input[type="checkbox"]:hover:not(:checked) ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;
}

.funkyradio input[type="radio"]:checked ~ label,
.funkyradio input[type="checkbox"]:checked ~ label {
  color: #777;
}

.funkyradio input[type="radio"]:checked ~ label:before,
.funkyradio input[type="checkbox"]:checked ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #333;
  background-color: #ccc;
}

.funkyradio input[type="radio"]:focus ~ label:before,
.funkyradio input[type="checkbox"]:focus ~ label:before {
  box-shadow: 0 0 0 3px #999;
}

.funkyradio-default input[type="radio"]:checked ~ label:before,
.funkyradio-default input[type="checkbox"]:checked ~ label:before {
  color: #333;
  background-color: #ccc;
}

.funkyradio-primary input[type="radio"]:checked ~ label:before,
.funkyradio-primary input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #337ab7;
}

.funkyradio-success input[type="radio"]:checked ~ label:before,
.funkyradio-success input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5cb85c;
}
</style>