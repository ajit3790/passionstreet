<?php
$fields = array();
$fields['name'] = array('Name',1,'text',1);
$fields['email'] = array('Email Id',1,'email',1);
$fields['gender'] = array('Gender',1,'text',1);
$fields['dob'] = array('Date Of Birth',1,'text',1);
$fields['mobile'] = array('Mobile',1,'number',1);
$fields['designation'] = array('Designation',0,'text',0);
$fields['company'] = array('Company',0,'text',0);
$fields['address'] = array('Address',0,'longtext',0);
$fields['city'] = array('City',0,'text',0);
$fields['country'] = array('Country',0,'text',0);
$fields['pin'] = array('PIN',0,'number',0);
$fields['guardian'] = array('Guardian\'s Name',0,'text',0);

$fields['name'] = array('Name',1,'text',1);
$fields['email'] = array('Email Id',1,'email',1);
$fields['gender'] = array('Gender',1,'text',1);
$fields['dob'] = array('Date Of Birth',1,'text',1);
$fields['mobile'] = array('Mobile',1,'number',1);
$fields['designation'] = array('Designation',1,'text',0);
$fields['company'] = array('Company',1,'text',0);
$fields['address'] = array('Address',1,'longtext',0);
$fields['city'] = array('City',1,'text',0);
$fields['country'] = array('Country',1,'text',0);
$fields['pin'] = array('PIN',1,'number',0);
$fields['guardian'] = array('Guardian\'s Name',1,'text',0);
$defaultFields = array('name','email');
$PSModData['fields'] = $fields;
$PSModData['defaultFields'] = $defaultFields;
$error = false;
if($_POST)
{
    $newfields = array();
    foreach($_POST['default'] as $fieldkey=>$field)
    {
        if(isset($field['include']))
        {
            $newfields[$fieldkey] = array($field['displayname'],(isset($field['mandatory'])?1:0),(($fields[$fieldkey][2])?$fields[$fieldkey][2]:$field['dtype']),1);
        }
        else if(startsWith($fieldkey,'custom'))
        {
            $newfields[$fieldkey] = array($field['displayname'],0,$field['dtype'],0);
        }
    }
    foreach($_POST['custom'] as $field)
    {
        if(!empty($field['name']))
        {
            $fieldkey = 'custom'.time().rand(1000,9999);
            $newfields[$fieldkey] = array($field['name'],(isset($field['mandatory'])?1:0),(!empty($field['type'])?$field['type']:'text'),1);
        }
    }
    $error = (!(attendeefields_manage(array('productid'=>$_GET['entityId'],'producttype'=>$_GET['entityType']),array('fields'=>$newfields))));
}
$data = attendeefields_get_details(array('productid'=>$_GET['entityId'],'producttype'=>$_GET['entityType']));
if($data['fields'])
{
    foreach($data['fields'] as $key=>$value){
        $fields[$key] = $value;
    }
}
if($error)
$PSModData['fields'] = $newfields;
else
$PSModData['fields'] = $fields;    
?>