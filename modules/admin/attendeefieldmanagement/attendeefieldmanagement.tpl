<div class="row">
<div class="col-sm-8">
<form method="post" action="<?=$_SERVER['PHP_BROWSER_URL']?>">
<div class="row">
    <div class="col-sm-12 col-md-12 col-xs-12">
            <table class="table table-hover attendeetable ">
                <thead>
                  <tr>
                    <th>Field Name</th>
                    <th>Include</th>
                    <th  class="hide">Mandatory Check</th>
                    <th >Drag</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                foreach($fields as $fieldkey=>$field){
                    echo '<tr>';
                        echo '<td><input type="hidden" name="default['.$fieldkey.'][displayname]" value="'.$field[0].'">'.$field[0].'<input type="hidden" name="default['.$fieldkey.'][dtype]" value="'.$field[2].'"></td>';
                        if(in_array($fieldkey,$defaultFields))
                        {
                        echo '<td><label class="switch"><input type="checkbox" name="default['.$fieldkey.'][include]" checked readonly ><span class="switchslider round" tabindex="0"></span></label></td>';
                        echo '<td class="hide"><label class="switch"><input type="checkbox" name="default['.$fieldkey.'][mandatory]" checked readonly><span class="switchslider round" tabindex="0"></span></label></td>';
			}
                        else
                        {
                        echo '<td><label class="switch"><input type="checkbox" name="default['.$fieldkey.'][include]" '.(($field[3])?'checked':'').'><span class="switchslider round" tabindex="0"></span></label></td>';
                        echo '<td class="hide"><label class="switch"><input type="checkbox" name="default['.$fieldkey.'][mandatory]" '.(($field[1])?'checked':'').'><span class="switchslider round" tabindex="0"></span></label></td>';
                        }
                        echo '<td><i class="fa fa-arrows"></i></td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
    </div>
</div>
<div class="row " id="attendeefieldcustomadd">
</div>
<div class="row ">
    <div class="col-sm-12 form-group text-right ">
      <span class="add-more">Add custom fields</span>
    </div>
 </div>
<div class="row">
    <div class="col-sm-12 text-center">
        <button value="submit" name="submit" type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
</form>
</div>
<div class="col-sm-4">
	<div class="help-text">
    	<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
	    <ul><li>You may select the fields, which are required</li><!--<li>Define which are mandatory information for user to enter</li>--><li>You may add more field if required</li><!--<li>Drag and drop to reorder</li>--></ul>	
	</div>
</div>
</div>


<script>
    $(document).ready(function(){
        $(".attendeefieldmanagement .add-more").on('click',function(){
            $("#attendeefieldcustomadd").append(addCustomField());
        });
	$('.attendeetable').sortable({
		  containerSelector: 'table',
		  itemPath: '> tbody',
		  itemSelector: 'tr',
		  placeholder: '<tr class="placeholder"/>',
		  handle: 'i.fa-arrows'
	});
    });
var $customFieldIndex = 0;
function addCustomField(){
    $x = '<div class="row form-group ">'+
        '<div class="col-sm-3">Field Name</div>'+
        '<div class="col-sm-9"><input type="text" name="custom['+$customFieldIndex+'][name]" value="" class="form-control"></div>'+
    '</div>'+
    '<div class="row form-group ">'+
        '<div class="col-sm-3">Input Type</div>'+
        '<div class="col-sm-9">'+
            '<label class="radio-inline"><input type="radio" name="custom['+$customFieldIndex+'][type]" value="text" checked>Short Text</label>'+
            '<label class="radio-inline"><input type="radio" name="custom['+$customFieldIndex+'][type]" value="longtext">Long Text</label>'+
            '<label class="radio-inline"><input type="radio" name="custom['+$customFieldIndex+'][type]" value="number">Number</label>'+
        '</div>'+
    '</div>'+
    '<div class="row form-group ">'+
        '<div class="col-sm-3">Mandatory</div>'+
        '<div class="col-sm-9"><label class="switch"><input type="checkbox" name="custom['+$customFieldIndex+'][mandatory]" checked value="1"><span class="switchslider round"></span></label></div>'+
    '</div>';
    $customFieldIndex++;
    return '<div class="col-sm-12 cstm_atnd_feld form1">'+$x+'</div>';
}
</script>
<?php
$PSJsincludes['internal'][] = "js/jquery-sortable.js";
?>