 
 <div class="row"><div class="col-sm-8">
    <?php
    if($currentSavedAccount)
    {
    ?>
    <div class="row">
        <div class="row">
            <div class="col-sm-12">
                <div class='row'>
                    <div class="col-sm-4">Account Holder </div>
                    <div class="col-sm-8"><?=$userbankaccounts[$currentSavedAccount]['accountholdername']?></div>
                </div>
            </div> 
            <div class="col-sm-12">
                <div class='row'>
                    <div class="col-sm-4">Bank</div>
                    <div class="col-sm-8"><?=$userbankaccounts[$currentSavedAccount]['bankname']?></div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class='row'>
                    <div class="col-sm-4">A/C Number </div>
                    <div class="col-sm-8"><?=$userbankaccounts[$currentSavedAccount]['acntnmbr']?></div>
                </div>
            </div>  
        </div>
        <a class="btn btn-default pull-right" onclick="$('.editbank').removeClass('hide')">Change Bank</a>
    </div>
    <?php } ?>
    <div class='editbank <?=(($currentSavedAccount)?"hide":"")?> '>
        
    <div class="row">
        <label class="control-label col-md-12  form-group">Kindly Provide your Bank Details to transfer event ticketing fee money</label>
    </div>
    
    <ul class="nav nav-tabs nav-justified" style="margin-bottom:-1px">
        <li class="active"><a data-toggle="tab" href="#savedaccounts">Saved Accounts</a></li>
        <li><a data-toggle="tab" href="#addaccount" id="addNewBackAccount">Add New Account</a></li>
    </ul>
    <div class="tab-content" style="border: 1px solid rgb(204, 204, 204); padding: 30px;background-color:#fff;margin-bottom:10px">
        <div id="savedaccounts" class="tab-pane fade in active">
            <div class="bank-account-list">
                <form method="post" action="<?=$_SERVER['PHP_BROWSER_URL']?>">
                <?php
                echo '<div class="row form-group"> <div class="col-sm-1"></div> <div class="col-sm-4">Account Name</div> <div class="col-sm-4">Bank Name</div> <div class="col-sm-3">A/C Number</div>  </div>';
                foreach($userbankaccounts as $bankdetail)
                {
                    echo '<div class="row form-group"><div class="col-sm-1"><input type="radio" name="selectedbankid" value="'.$bankdetail['account_id'].'" required '.(($currentSavedAccount == $bankdetail['account_id'])?'checked':'').'/></div> <div class="col-sm-4">'.$bankdetail['accountholdername'].'</div> <div class="col-sm-4">'.$bankdetail['bankname'].'</div> <div class="col-sm-3">'.$bankdetail['acntnmbr'].'</div>  </div>';
                }
                ?>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <input type="hidden" name="operationtype" value="attachbank" />
                        <button value="submit" name="submit" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div id="addaccount" class="tab-pane form1 fade addNewBackAccountForm">
            <form method="post" action="<?=$_SERVER['PHP_BROWSER_URL']?>">
            <div class="row">
                <label class="control-label col-md-12  form-group">Kindly Provide your Bank Details to transfer the payment</label>
            </div>
            <div class="row">
                <div class="col-sm-12 form-group "><input type="text" placeholder="Account Holder Name" required="" pattern="[a-zA-Z .]{1,}" name="accountholdername" class="form-control"></div>
            </div>
            <div class="row">
                <div class="col-sm-6  col-xs-6  form-group "><input type="text" placeholder="Bank Name" required="" pattern="[a-zA-Z .]{1,}" name="bankname" class="form-control"></div>
                <div class="col-sm-6  col-xs-6   form-group "><input type="text" placeholder="Account No" required="" pattern="[a-zA-Z0-9- .]{1,}" name="acntnmbr" class="form-control"></div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-6 form-group ">
                    <select required="" name="type" class="form-control" pattern="[a-zA-Z]{1,}">
                        <option value="" selected="">Account type</option>
                        <option value="saving">Saving Account</option>
                        <option value="current">Current Account</option>
                  </select>
                </div>
                <div class="col-sm-6 col-xs-6 form-group "><input type="text" name="ifsc" pattern="[a-zA-Z0-9- .]{1,}" placeholder="IFSC Code" required="" class="form-control"></div>
            </div>
            <div class="row">
                <div class="col-sm-12  form-group "><input type="text" name="address" pattern=".{1,}" placeholder="Branch Address" required="" class="form-control"></div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <input name="chequepic" id="chequepic" type="file" />
                </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
                <input type="hidden" name="operationtype" value="addbankaccount" />
                <button value="submit" name="submit" type="submit" class="btn btn-primary">Submit</button>
                </div>  
            </div>
            <script type="text/javascript">
            $('#chequepic').filestyle({
                'icon' : false,
                'buttonText' : ' Browse',
                'placeholder': 'Upload Cancelled Cheque'
            });
            </script>
            </form>
        </div>
        </div>
</div>
</div>
<div class="col-sm-4">
<div class="help-text">
<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
<ul><li>Adding your bank A/c will help us transfer your payment quickly</li><li>You may record multiple bank A/c</li><li>One event money can not be transferred to multiple A/c</li><li>You may add more field if required</li></ul>
</div>
</div>

</div>        
        
<?php
$currentstatus = 0;
if($currentSavedAccount)
$currentstatus = 1;
?>
<script>
$(document).ready(function(){
        executeFunctionByName('parent.togglePublishComponentStatus',['eventpaymentcollection',<?=$currentstatus?>]);
});
</script>