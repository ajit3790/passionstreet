<?php
if(!($_GET['entityType'] == 'event' || $_GET['entityType'] == 'tour'))
return;
$eventid = $_GET['entityId'];

if(!($eventid) ||  !($event = event_get_details($eventid,1,PROFILE_ID,true))){
    return;
}
else if($event['creator'] != $_SESSION['user']['profile_id']){
    return;
}
$userbankaccounts = bankaccount_get_list();
if($_POST)
{
    if($_POST['operationtype'] == 'attachbank')
    {
        $selectedbankid = $_POST['selectedbankid'];
        if($selectedbankid)
        {
            $inputParams = array();
            $inputParams['eventinfo']['bankaccount'] = $selectedbankid;
            event_update($eventid,$inputParams);
            $event = event_get_details($eventid,1,PROFILE_ID,true);
        }
    }
    else if($_POST['operationtype'] == 'addbankaccount')
    {
        extract($_POST);
        $insertData = array();
        $insertData['accountholdername'] = $accountholdername;
        $insertData['bankname'] = $bankname;
        $insertData['account_id'] = generate_id("bank");
        $insertData['acntnmbr'] = $acntnmbr;
        $insertData['type'] = $type;
        $insertData['ifsc'] = $ifsc;
        $insertData['address'] = $address;
        if(isset($_FILES['chequepic']) && $_FILES['chequepic']['error']==0)
        $insertData['chequepic'] = upload('chequepic','image',$_FILES['chequepic']);
        else
        $insertData['chequepic'] = '';
        $insertData['dateadded'] = date("Y/m/d h:i:s");
        $insertData['profile_id'] = $_SESSION['user']['profile_id'];
        $x = bankaccount_add($insertData);
        $inputParams = array();
        $inputParams['eventinfo']['bankaccount'] = $insertData['account_id'];
        event_update($eventid,$inputParams);
        $event = event_get_details($eventid,1,PROFILE_ID,true);
        $userbankaccounts = bankaccount_get_list();
    }
}
$PSModData['userbankaccounts'] = $userbankaccounts;
$PSModData['currentSavedAccount'] = $event['bankaccount'];

?>