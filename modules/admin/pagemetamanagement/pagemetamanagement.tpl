<div class="row"><div class="col-sm-8">
<form class="form1 event-creation" action="<?=$_SERVER['PHP_BROWSER_URL']?>" method="post" enctype="multipart/form-data">            
	<div class="row form-group ">
		<div class="col-sm-3">SEO Title</div>
		<div class="col-sm-9"><input type="text" name="seotitle" value="<?=$pagemeta['title']?>" class="form-control"></div>
	</div>
	<div class="row form-group ">
		<div class="col-sm-3">SEO Description</div>
		<div class="col-sm-9"><textarea name="seodescription" class="form-control"><?=$pagemeta['description']?></textarea></div>
	</div>
	<div class="row form-group ">
		<div class="col-sm-3">SEO Keywords</div>
		<div class="col-sm-9"><textarea name="seokeywords" class="form-control"><?=$pagemeta['keywords']?></textarea></div>
	</div>
	<div class="row form-group ">
	        <div class="col-sm-3"></div>
		<div class="col-sm-9">
		<?php
		echo '<input type="hidden" name="pagemetaid" value="'.$pagemeta['pagemetaid'].'" />';
		?>
		<input type="submit" name="submit" class="btn btn-primary" value="Submit"></div>
	</div>
</form>
</div>
<div class="col-sm-4">
<div class="help-text">
   <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
<ul><li>Use SEO friendly event title (not more that 80 characters)</li><li>Describe your event in not more than 140 characters</li><li>Use relevant keywords as possible, no limit in keywords</li></ul>
</div>   

</div>'
</div>