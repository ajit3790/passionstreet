<?php
if($serverresponse)
echo '<h5>'.$serverresponse.'</h5>';
?>
<div class="row"><div class="col-sm-8">
<form method="post" class="form1" action="<?=$_SERVER['PHP_BROWSER_URL']?>">
<div class="org-detail">
    <h4 class="hstyl_4">Organizer Contact details</h4>
        <div class="radio form-group col-sm-6 col-xs-12">
                <label>
                  <input type="radio" name="organiser[type]"  value="userprofile" id="userasorganiser" <?=(($event['organiser']['type'] == 'userprofile')?'checked':'')?>>
                  Add Self Profile as organiser
                </label>
        </div>
        <div class="radio form-group col-sm-6 col-xs-12">
                <label id="showpagelist">
                  <input type="radio" name="organiser[type]"  value="pageprofile" readonly="readonly" <?=(($event['organiser']['type'] == 'pageprofile')?'checked':'')?>>
                  Add Affiliate Page as organiser
                </label>
        </div>
    <div class="row">
        <div class="col-sm-12  form-group "><input type="text" id="organisername" name="organiser[name]" class="form-control" required placeholder="Organizer's Name" value="<?=$event['organiser']['name']?>"></div>
    </div>
    <div class="row">
        <div class="col-sm-6  form-group "><input type="email" id="organiseremail" name="organiser[email]" class="form-control" required placeholder="Email Id"  value="<?=$event['organiser']['email']?>"></div>
        <div class="col-sm-6  form-group "><input type="text" id="organisercontactno" name="organiser[cntnumber]" class="form-control" required placeholder="Contact Number"  value="<?=$event['organiser']['cntnumber']?>"></div>
    </div>

    <div class="row">
        <div class="col-sm-12  form-group ">
            <textarea class="form-control" id="organiseraddress" name="organiser[address]" required rows="5" cols="40" placeholder="Postal Address"><?=$event['organiser']['address']?></textarea>
            <input type="hidden" name="organiser[id]" id="organiserid" value="<?=$event['organiser']['id']?>" /> 
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12  form-group ">
            <button value="submit" name="submit" type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</div>          
</form>
</div>
<div class="col-sm-4">

</div>

</div>
<?php
$PSJsincludes['internal'][] = "js/eventpublish.js";
$currentstatus = 0;
if($event['organiser'])
$currentstatus = 1;
?>
<script>
$(document).ready(function(){
        executeFunctionByName('parent.togglePublishComponentStatus',['eventorganiser',<?=$currentstatus?>]);
});
</script>