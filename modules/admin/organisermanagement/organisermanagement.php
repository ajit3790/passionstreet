<?php
if(!($_GET['entityType'] == 'event' || $_GET['entityType'] == 'tour'))
return;
$producttype = $_GET['entityType'];
$eventid = $_GET['entityId'];

if(!($eventid) ||  !($event = event_get_details($eventid,1,PROFILE_ID,$producttype,true))){
    return;
}
else if($event['creator'] != $_SESSION['user']['profile_id']){
    return;
}
if($_POST){
    if(empty($event['organiser']) || true)
    {
            $inputParams = array();
	    $inputParams['organisertype'] = ($_POST['organiser']['type'] == 'userprofile' || empty($_POST['organiser']['type']))?'user':'page';;
            $inputParams['organiserid'] = $_POST['organiser']['id'];
            $inputParams['eventinfo']['organiser'] = $_POST['organiser'];
            $inputParams['eventinfo']['allowuserpost'] =  1;
            event_update($eventid,$inputParams);
            $event = event_get_details($eventid,1,PROFILE_ID,$producttype,true);
    }
    else
    {
        $PSModData['serverresponse'] = '<div class="alert-warning">Once Live , this organiser details cannot be changed</div>';
    }
}
$pages = page_get_list(array('creator_id'=>PROFILE_ID,'status'=>2));
$pageContact = array();
foreach($pages as $page)
{
	$temp = array();
	$temp['id'] = $page['page_id'];
	$temp['name'] = $page['page_name'];
	$temp['email'] = $page['page_email'];
	$temp['contact'] = ($page['page_contactno'])?$page['page_contactno']:'';
	$temp['address'] = ($page['page_address'])?$page['page_address']:'';
	$pageContact[$temp['id']] = $temp;
}
$userContact = array();
	$temp = array();
	$temp['id'] = $PSData['user']['profile_id'];
	$temp['name'] = $PSData['user']['fullname'];
	$temp['email'] = $PSData['user']['email'];
	$temp['contact'] = ($PSData['user']['contactno'])?$PSData['user']['contactno']:'';
	$temp['address'] = ($PSData['user']['city'])?$PSData['user']['city'].', '.$PSData['user']['country']:'';
	$userContact[$temp['id']] = $temp;

$PSJavascript['pageContact'] = $pageContact;
$PSJavascript['userContact'] = $userContact;
$PSModData['event'] = $event;
?>