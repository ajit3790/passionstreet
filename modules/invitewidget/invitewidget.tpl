<div class="invite-widget">
    <div class="flipper">
        <div class="front">
            <h2>Invite friends</h2>
            <i class="fa fa-facebook c-fb"></i> 
            <i class="fa fa-twitter c-twtr"></i>
            <i class="fa fa-google-plus c-ggl"></i>
            <i class="fa fa-envelope"></i>
        </div>
        <div class="back">
            <span class="close">X</span> 	
            <p>You may enter multiple email id's coma seperated</p>	
            <textarea></textarea>
            <button class="btn btn-primary">Invite</button>
        </div>
    </div> 
</div>
<script>
$(".fa-envelope").click(function(){
$(".invite-widget").addClass("flip");	
});
$(".back .close").click(function(){
$(".invite-widget").removeClass("flip");	
});
</script>

