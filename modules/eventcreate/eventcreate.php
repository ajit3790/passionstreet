<?php
$eventid = $_GET['event_id'];
if(!($eventid) ||  !($event = event_get_details($eventid,true))){
	$PSModData['editmode'] = 0;
}
else
{
	$PSModData['editmode'] = 1;
	$PSModData['event'] = $event;
	if($event['creator'] != $PSData['user']['profile_id'])
	{
		header('location:'.ROOT_PATH); 
		exit();
	}
}
if(isset($_POST['event-create']))
{
    usePlugin('htmlpurifier');
    if(!empty($_POST['description']) && !empty($PSParams['plugins']['htmlpurifier']))$_POST['description'] = $PSParams['plugins']['htmlpurifier']->purify($_POST['description']);
    if(!empty($_POST['terms']) && !empty($PSParams['plugins']['htmlpurifier']))$_POST['terms'] = $PSParams['plugins']['htmlpurifier']->purify($_POST['terms']);
    if(true){
        extract($_POST);
        $insertData = array();
        $insertData['event_id'] = generate_id("event");
        $insertData['creator'] = $_SESSION['user']['profile_id'];
        $insertData['eventtype'] = $eventType;
        $insertData['eventname'] = $name;
        $insertData['category'] = $category;
	$timestart = generate_standard_time($timestart);
	$timeend = generate_standard_time($timeend);
	$datestart = $datestart.' '.$timestart;
	$dateend = $dateend.' '.$timeend;
	$insertData['datestart'] = generate_standard_date($datestart);
        $insertData['dateend'] = generate_standard_date($dateend);
        $insertData['description'] = $description;
        $insertData['terms'] = $terms;
        $map_loc_components = json_decode($map_loc_components,true);
        $insertData['venue'] = $map_loc_components['name'];
        $insertData['city'] = $map_loc_components['city'];
        $insertData['state'] = $map_loc_components['state'];
        $insertData['address'] = strip_tags($address,'<br></br />');
        $insertData['maploc'] = $map_loc_coordinate;
        $insertData['mapimage'] = upload_from_url('eventmap',$map_loc_image);
        //$insertData['eventpic'] = upload_from_uri('eventbanner',$pic_post);
        if($_POST['multicropdata']['eventbanner'])
        $insertData['eventpic'] = upload_post_images_new2($_FILES,'eventbanner');
        else
        $insertData['eventpic'] = array();
        $insertData['eventpic'] = serialize($insertData['eventpic']);
        // $insertData['termsdoc'] = upload('eventtnc','file',$_FILES['termsdoc']);
        $insertData['termsdoc'] = '';
        $insertData['createdate'] = date("Y/m/d h:i:s");
        $insertData['status'] = 'unpublished';
	if(event_create($insertData))
        {
            $PSModData['server_response_text'] = 'Event Creation Successfull';
            $PSModData['server_response_status'] = 1;
            $PSModData['server_response_html'] = '<div class="msgbox1 text-center">
                    <p>Your Event / Workshop has been created successfully .<br /> Click <a href="'.ROOT_PATH.'events/publish/'.$insertData['event_id'].'">Here to publish it.</p>
                </div>';
        }
        else {
            $PSModData['params'] = $_POST;
            $PSJavascript['map_position']=$_POST['map_loc_coordinate'];
            $PSModData['server_response_html'] = '<div class="msgbox1 text-center"><p>Event Creation Failed</p></div>';
            $PSModData['server_response_status'] = 0;
        }
    }
    else
    {
        $PSModData['params'] = $_POST;
        $PSJavascript['map_position']=$_POST['map_loc_coordinate'];
        $PSModData['server_response_html'] = '<div class="msgbox1 text-center"><p>Event Creation Failed</p></div>';
        $PSModData['server_response_status'] = 0;
    }
}
if(isset($_POST['event-update']) && $_POST['event_id'])
{
    usePlugin('htmlpurifier');
    if(!empty($_POST['description']) && !empty($PSParams['plugins']['htmlpurifier']))$_POST['description'] = $PSParams['plugins']['htmlpurifier']->purify($_POST['description']);
    if(!empty($_POST['terms']) && !empty($PSParams['plugins']['htmlpurifier']))$_POST['terms'] = $PSParams['plugins']['htmlpurifier']->purify($_POST['terms']);
    if(true){
        extract($_POST);
        $insertData = array();
		$eventid = $_POST['event_id'];
        if($eventType)
		$insertData['eventtype'] = $eventType;
        if($category)
		$insertData['category'] = $category;
		//$timestart = generate_standard_time($timestart);
		//$timeend = generate_standard_time($timeend);
		//$datestart = $datestart.' '.$timestart;
		//$dateend = $dateend.' '.$timeend;
		//$insertData['datestart'] = generate_standard_date($datestart);
        //$insertData['dateend'] = generate_standard_date($dateend);
        $insertData['description'] = $description;
        $insertData['terms'] = $terms;
        //$map_loc_components = json_decode($map_loc_components,true);
        //$insertData['venue'] = $map_loc_components['name'];
        //$insertData['city'] = $map_loc_components['city'];
        //$insertData['state'] = $map_loc_components['state'];
        //$insertData['address'] = strip_tags($address,'<br></br />');
        //$insertData['maploc'] = $map_loc_coordinate;
        //$insertData['mapimage'] = upload_from_url('eventmap',$map_loc_image);
        //$insertData['eventpic'] = upload_from_uri('eventbanner',$pic_post);
        //if($_POST['multicropdata']['eventbanner'])
        //$insertData['eventpic'] = upload_post_images_new2($_FILES,'eventbanner');
        //else
        //$insertData['eventpic'] = array();
        //$insertData['eventpic'] = serialize($insertData['eventpic']);
        // $insertData['termsdoc'] = upload('eventtnc','file',$_FILES['termsdoc']);
        //$insertData['termsdoc'] = '';
		if(event_update($eventid,$insertData))
		{
			header("location:".ROOT_PATH.'events/edit/'.$eventid);
		}
    }
}
$PSModData['eventTypes'] = $PSParams['eventTypes'];
?>