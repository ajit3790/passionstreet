<div class="col-md-12">
<?php
if($editmode != 1)
{ 
?>
<div class="row">
    <div class="col-md-12"><h2 class="hstyl_2 mb-large"><span><i class="fa fa-pencil-square-o"></i>Create Event and Workshop</span></h2></div>
</div>
<?php
}
?>
<div class="row">
    <div class="col-md-12">
        <?=$server_response_html?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-wrapper">
	    <?php
	    //print_array($event);
	    if($editmode == 1)
	    echo '<form class="form1 event-creation" action="'.ROOT_PATH.'events/publish/'.$event['event_id'].'" method="post" enctype="multipart/form-data">';
	    else
	    echo '<form class="form1 event-creation" action="'.ROOT_PATH.'events/create'.'" method="post" enctype="multipart/form-data">';
	    ?>
            <div class="row ">
                  <div class="col-sm-12 form-group ">
                      <input type="text" value="<?=($editmode == 1)?$event['eventname']:$params['name']?>" name="name" placeholder="Event Title" required="" class="form-control" <?=($editmode == 1)?'disabled':''?>>
                  </div>
               </div> 
                <div class="row form-group eventTypeRow">
                    <!--<label class="col-sm-3 col-xs-12 col-md-2 form-group  control-label">TYPE</label> -->
                    <!--
                    <div class="radio form-group col-sm-6 col-md-2 ">
                        <label>
                          <input type="radio" value="event" id="eventtypeevent" required="" name="eventtype" <?=($params['eventtype']=='event')?'checked':''?>>
                          Event
                        </label>
                     </div>
                     <div class="radio form-group col-sm-6 col-md-2 ">
                        <label>
                          <input type="radio" value="workshop" id="eventtypeworkshop" required="" name="eventtype" <?=($params['eventtype']=='workshop')?'checked':''?>> 
                          Workshop
                        </label>
                     </div>
                    -->
                    <div class="col-sm-6 col-md-6 ">
                        <select name="eventType" id="eventType" class="form-control" required="" pattern=".{1,}">
                            <option value="" selected="">Select Event Type</option>
                            <?php
                            foreach($eventTypes as $eventType)
                            {
				$selected = (($params['eventType']==$eventType) || ($editmode == 1 && $eventType==$event['eventtype']))?'selected':'';
                                echo "<option value='".$eventType."' $selected >".$eventType."</option>";
                            }
                            ?>
                        </select>
                    </div>
                     <div class="col-sm-6 col-md-6 ">
                        <select name="category" id="category" class="form-control" required="" pattern=".{1,}">
                            <option value="" selected="">Select Category</option>
                            <?php
                            foreach($PSParams['PSCategories'] as $PSCategorykey=>$PSCategory)
                            {
                                $selected = (($params['category']==$PSCategorykey) || ($editmode == 1 && $PSCategorykey==$event['category']))?'selected':'';
                                echo "<option value='".$PSCategorykey."' $selected >".$PSCategory['name']."</option>";
                            }
                            echo '<option value="others">Category Not in List ?</option>';
                            ?>
                        </select>
                  </div>
                  <div class="col-sm-6 col-md-6 categorydiv hide">
                     <input type="text" name="category" id="othercat" disabled  placeholder="Define Category Name" class="form-control " />
                  </div>
               </div>
               <?php
		//if($editmode != 1)
		{
		//$datestart = 2017-01-22 09:29:00;
		//$dateend;
		if($editmode == 1)
		{
		$temp = getdate(strtotime($event['datestart']));
		$event['dsm'] = array(sprintf("%02d", $temp['mday']).'/'.sprintf("%02d", $temp['mon']).'/'.$temp['year'] , ((($temp['hours']>12)?sprintf("%02d", $temp['hours']-12):sprintf("%02d", $temp['hours'])) .':'.sprintf("%02d", $temp['minutes'])).' '.(($temp['hours']>12)?'PM':'AM'));
		$temp = getdate(strtotime($event['dateend']));
		$event['dem'] = array(sprintf("%02d", $temp['mday']).'/'.sprintf("%02d", $temp['mon']).'/'.$temp['year'] , ((($temp['hours']>12)?sprintf("%02d", $temp['hours']-12):sprintf("%02d", $temp['hours'])) .':'.sprintf("%02d", $temp['minutes'])).' '.(($temp['hours']>12)?'PM':'AM'));
		}
		?>
			<div class="row form-group event-date">
			  <div class="col-md-3 ">
				<!--<label class="control-label  form-group">From</label>-->
				<div class="form-group date">
				    <i class="fa fa-calendar"></i>
				    <input type="text" name="datestart" id="datestart" value="<?=($editmode == 1)?($event['dsm'][0]):$params['datestart']?>" required="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" title="Format = DD/MM/YYYY" placeholder="Start Date" data-type="datepicker" class="form-control date date-future datepicker" style="background-color: #fff" readonly="">
				</div>  
			  </div>
			  <div class="col-md-3 ">
				<!--<label class="control-label  form-group">From</label>-->
				<div class="form-group date">
				    <i class="fa fa-clock-o"></i>
				    <input type="text" name="timestart" id="timestart" value="<?=($editmode == 1)?($event['dsm'][1]):$params['timestart']?>" required="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" title="Format = hh:mm:ss" placeholder="Start Time" data-type="timepicker" class="form-control date date-future datepicker" style="background-color: #fff" readonly="">
				</div>  
			  </div> 
			  <div class="col-md-3 ">     
				<!--<label class="control-label form-group">To</label>-->
				<div class="form-group date">
				    <i class="fa fa-calendar"></i>
				    <input type="text" name="dateend" id="dateend" value="<?=($editmode == 1)?($event['dem'][0]):$params['dateend']?>" required="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" title="Format = DD/MM/YYYY" placeholder="End Date" data-type="datepicker" class="form-control date date-future datepicker" style="background-color: #fff" readonly="readonly">
				</div>  
			  </div>
			  <div class="col-md-3 ">     
				<!--<label class="control-label form-group">To</label>-->
				<div class="form-group date">
				    <i class="fa fa-clock-o"></i>
				    <input type="text" name="timeend" id="timeend" value="<?=($editmode == 1)?($event['dem'][1]):$params['timeend']?>" required="" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" title="Format = hh:mm:ss" placeholder="End Time" data-type="timepicker" class="form-control date date-future datepicker" style="background-color: #fff" readonly="readonly">
				</div>  
			  </div>

		       </div>  

		       <div class="row ">
			  <div class="col-sm-6">
			      <div class="form-group ">
				 <input type="text" name="event_venue" id="map-input" placeholder="Event Venue" class="form-control date" <?=($editmode == 1)?'disabled':''?>  value="<?=($editmode==1)?$event['venue']:''?>">
			      </div>
			      <div class="form-group ">
				 <textarea placeholder="Complete Address" name="address" id="address" required="" class="form-control"><?=($editmode == 1)?$event['address']:$params['address']?></textarea>
			      </div>
			      <div class="form-group">
				<input type="file" name="eventbanner[]" croptype="eventbanner" id="upload2" class="multifilecrop" multiple data-crpvrsn=2 >
				<!--<input type="file" pictype="eventbanner"  org-width=400 org-height=240 callback="filetopost" id="event-pic" name="pic"  style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" class="filecrop" tabindex="-1"><div class="bootstrap-filestyle input-group"><input type="text" disabled="" placeholder="If Event Banner is not uploaded, we will use default" class="form-control "> <span class="group-span-filestyle input-group-btn" tabindex="0"><label class="btn btn-default " for="event-pic"><span class="buttonText"> Browse</span></label></span></div>--> 
			      </div>
			  </div>
			  <div class="col-sm-6 form-group">
			      <div id="map" style="height:218px;width:100%"></div>
			  </div>
		       </div>
		       <?php 
		       if($editmode == 1)
		       {?>
			<div class="row ">
			  <div class="col-sm-12 form-group">
			      <img src="<?=$event['eventpic']?>" style="width:100%" />
			  </div>
			</div>	       
			<?php
			}
			?>

                <?php
		}
		?>
                <div class="form-group row">
                    <label class="col-sm-12 form-group  control-label">Brief Description of Event</label>	
                    <div class="col-sm-12 ">
                        <textarea placeholder="Brief Description of event" name="description" id="description" required="" class="form-control simpleeditor"><?=($editmode==1)?$event['description']:$params['description']?></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-12 form-group  control-label">Write Event Participation Terms &amp; Condition</label>	
                    <div class="col-sm-12 ">
                        <textarea placeholder="Write Event participation Terms &amp; Condition" name="terms" id="terms" required="" class="form-control simpleeditormin"><?=($editmode==1)?$event['terms']:$params['terms']?></textarea>
                    </div>
                </div>
                <div class="form-group row hide">
                    <div class="col-sm-12">
                        <input type="file" id="TnC" name="termsdoc" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" tabindex="-1"><div class="bootstrap-filestyle input-group"><input type="text" disabled="" placeholder="Upload T&amp;C doc file" class="form-control "> <span class="group-span-filestyle input-group-btn" tabindex="0"><label class="btn btn-default " for="TnC"><span class="buttonText"> Browse</span></label></span></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <?php 
				if($editmode == 1)
				{
				echo "<input type='hidden' name='event_id' value='".$event['event_id']."'>";
				echo "<input type='hidden' name='editmode' value='1'>";
				echo '<button class="btn btn-primary" type="submit" name="event-update" value="1">Update</button>';
				}
				else
				echo '<button class="btn btn-primary" type="submit" name="event-create" value="1">Create</button>';
			?>
                    </div>
                </div>
                <input type="hidden" id="event-pic_post" name="pic_post" value="<?=$params['pic_post']?>">
            </form>
        </div>
    </div>
</div>
</div>
<?php
$PSJsincludes['external'][] = "https://maps.googleapis.com/maps/api/js?key=".$PSParams['google_maps_key']."&libraries=places&callback=initGoogleMapsAutocomplete";
//$PSJsincludes['external'][] = "js/jquery.datetimepicker.js";
//$PSCssincludes['external'][] = "css/jquery.datetimepicker.css";
//$PSJsincludes['external'][] = "js/jquery.Jcrop.js";
//$PSCssincludes['external'][] = "css/jquery.Jcrop.css";
$PSJsincludes['internal'][] = "js/eventcreate.js";
$PSJsincludes['internal'][] = "js/bootstrap-filestyle.min.js";
$PSJsincludes['external'][] = "js/bootstrapdatetimepicker.min.js";
$PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
$PSJsincludes['external'][] = "js/fengyuanchencropper/croppermain2.js";
$PSJsincludes['external'][] = "js/fengyuanchencropper/cropper.js";
$PSCssincludes['external'][] = "js/fengyuanchencropper/cropper.css";
$PSJsincludes['external'][] = 'js/summernote-master/dist/summernote.js';
$PSCssincludes['external'][] = "js/summernote-master/dist/summernote.css";
?>