<?php
//generate long lived access token = https://graph.facebook.com/v2.8/oauth/access_token?access_token=EAACEdEose0cBADgeB0fLlp4tSQOiqNiMg0Lf80FMlvQVS7Nv2uo8qfRyGtcvKwngZADIKqE39jik25tSM1wMUXoZBAsnPGmIq2qgvhEZCakbd4OXyNA56EFUBm1NfPxZBMDApcVVjABdWKh1W15Y3IezGtMc6reZAkqEMNnngMgZDZD&debug=all&format=json&grant_type=fb_exchange_token&method=get&pretty=0&suppress_http_code=1&client_id=652032041525469&client_secret=e6cbe362f51e9934335f4c53727b6608&fb_exchange_token=EAAJRBQo3oN0BAEiHZC2Xw7L4moWyOJhNJE4si0ZCWSvfmojh2brr0PDExlZAzHeNZA6Uo4RK75JXU1nWGfCSItzjgB2vpYqxAxWP6DrEUC9ZAcvamSCOaXOKgXclygk2ZC2snEIXc74ZCOZCnuTqKv5MMRhNATUM8qGOdf8T0ateBgZDZD
?>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : clientid['facebook'],
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     if(server == 'local')
     js.src = "//connect.facebook.net/en_US/sdk/debug.js";
     else    
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<input type="button" class="sharebtn" value="Share"/>
<input type="button" class="loginbtn" value="Login"/>
<input type="button" class="getProfilebtn" value="Get Profile"/>
<script>
$(document).ready(function(){
    $(".sharebtn").on('click',function(){
        FB.ui(
        {
            method: 'share',
            href: 'https://developers.facebook.com/docs/'
        }, function(response){});
        /*
        FB.ui({
            method: 'share_open_graph',
            action_type: 'og.likes',
            action_properties: JSON.stringify({
                object:'https://developers.facebook.com/docs/',
            })
        }, function(response){
            // Debug response (optional)
            console.log(response);
        });
        */
    });
    $(".loginbtn").on('click',function(){
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                console.log('Logged in.');
                //FB.api('/me/feed', 'post', {message: 'Hello, world!'});
                /*FB.api('/361679933983641', function(response) { //read page info
                    console.log(response);
                });*/
                FB.api('/me', {fields: 'friends'}, function(response) {
                    console.log(response);
                });
            }
            else {
                //FB.login();
                FB.login(function(){
                    //FB.api('/me/feed', 'post', {message: 'Hello, world!'});
                    FB.api('/me', {fields: 'friends'}, function(response) {
                        console.log(response);
                    })
                }, {scope: 'publish_actions'});
            }
        });
    });
    $(".getProfilebtn").on('click',function(){
        //callfbapi('/me','',{fields:'email,likes,friends'},'','email,user_likes,user_friends');
        //callfbapi('/me','',{fields:'events'},'','user_events'); //permission required
        //callfbapi('/me/accounts','',{},'','manage_pages,pages_show_list'); //permission required
        //callfbapi('/me','',{fields:'posts'},'','user_posts'); //permission required
        callfbapi('/me/accounts','',{},showpagelist,'manage_pages');
        //callfbapi('/161669987213351','',{fields:'access_token'},managepage,'manage_pages,publish_pages,publish_actions'); //permission required
    });
});
function showpagelist(data){
    console.log('callback pagelist')
    console.log(data);
    if(data['data'])
    {
        $dropdownlist  = '<select id="selectpagetomanage">';
        $dropdownlist  += '<option value="">Select Page to Manage</option>';
        $.each(data['data'],function(i,v){
            console.log(i);
            console.log(v);
            FBObject['pages'][v['id']] = v;
            $dropdownlist  += '<option value="'+v['id']+'">'+v['name']+'</option>'; 
        });
        $dropdownlist  += '</select>'; 
        myModal("Manage Page","<div class='managepage'>"+$dropdownlist+"</div>",'');
        setTimeout(function(){
            $("#selectpagetomanage").on('change',function(){
                if(!($(this).val() == ''))
                {
                    $id = $(this).val();
                    FBObject['currentpage'] = FBObject['pages'][$id];
                    console.log(FBObject['pages'][$(this).val()]);
                    callfbapi('/'+FBObject['currentpage']['id'],'',{access_token:FBObject['currentpage']['access_token'],fields:'description,description_html,location,subscribed_apps,about,access_token,personal_info,talking_about_count,tabs,tagged,bio,display_subtext,category,emails,features,fan_count,feed,featured_video,founded,general_info,band_interests'},managepage,'manage_pages,publish_pages,publish_actions'); //permission required
                    //callfbapi('/'+FBObject['currentpage']['id'],'',{access_token:FBObject['currentpage']['access_token'],fields:'description,description_html,location,subscribed_apps,about,access_token,personal_info,talking_about_count,tabs,tagged,bio,display_subtext,category,emails,features,fan_count,feed,featured_video,founded,general_info,band_interests'},managepage,'manage_pages'); //permission required
                }
            });
        },1000);
    }
    else
    {
        alert('no page to manage');
    }
}
function managepage(data){
    if(data)
    {
	console.log('page data');
	$.extend(FBObject['currentpage'],data);
        console.log(FBObject['currentpage']);
	
        FB.ui({
            method: 'pagetab',
	    redirect_uri: 'https://www.facebook.com/dialog/pagetab?app_id=601029180034751',
	    //https://www.facebook.com/dialog/pagetab?app_id=601029180034751&next=http%3A%2F%2Fwww.facebook.com
        }, function(response){console.log(response)});
	
    }    
}
//public user_friends / scope , field : friends
//private user_events / scope , field : events
//private user_likes / scope , field : likes
var FBObject = {};
FBObject['permissions'] = {};
FBObject['pages'] = {};
FBObject['currentpage'] = {};
function callfbapifinal(path,method,params,callback){
    FB.api(path,params, function(response) {
        console.log('api called');
        callback(response);
    });
} 
function callfbapi(path,method,params,callback,extrapermissionsstr){
    console.log('calling fbapi');
    FB.getLoginStatus(function(response) {
        if(response.status === 'connected') {
            console.log('already logged in');
            console.log(FBObject['permissions']);
            console.log('***********************');
    
            if($.isEmptyObject(FBObject['permissions']))
            {
                console.log('loop1');
                fbgetpermissions(function(){
                    callfbapi(path,method,params,callback,extrapermissionsstr);
                });
            }
            else if(extrapermissionsstr)
            {
                console.log('loop2');
                extrapermissions = extrapermissionsstr.split(',');
                $.each(extrapermissions,function(i,v){
                    console.log(FBObject['permissions']);
                    console.log(FBObject['permissions']['email']);
                    if(!(v in FBObject['permissions']))
                    {
                        console.log('permission not available '+v);
                        console.log('attempting log in with desired permissions '+extrapermissionsstr);
                        /*fblogin(extrapermissionsstr,function(){
                            callfbapifinal(path,method,params,callback);
                        });
                        return*/;
                    }
                });
                callfbapifinal(path,method,params,callback);
            }
            else
            {
                console.log('loop3');
                callfbapifinal(path,method,params,callback);
            }
        }
        else
        {
            console.log('attempting log in with permissions '+extrapermissionsstr);
            fblogin(extrapermissionsstr);
        }
    });
}
function fbgetpermissions(callback){
    console.log('get the permissions');
    FB.api('/me', {fields: 'permissions'}, function(response) {
        $.each(response.permissions.data,function(i,v){
            FBObject['permissions'][v['permission']] = v['status'];
        });
        console.log(FBObject['permissions']);
        if(callback)
        callback();
    });
}
function fblogin(extrapermissions,callback){
    console.log('login function with '+extrapermissions)
    if(extrapermissions)
    {
        FB.login(function(){
            fbgetpermissions(callback);
        },{scope:extrapermissions});
    }
    else
    {
        FB.login(function(){
            fbgetpermissions(callback);
        });
    }
}
</script>