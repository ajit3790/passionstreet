<?php
if($transaction_response['status'] == 'complete|success')
{
	?>
	<h3>Thank You. Your order status is <span class="label label-success">Success</span></h3>
	<h4>Your Transaction ID for this transaction is <?=$transaction_response['txnid']?></h4>
	<h4>We have received a payment of Rs. <?=$transaction_response['amount']?></h4>
	<?php
	if($transaction_response['producturl'])
	echo '<br /><h4><a href="'.$transaction_response['producturl'].'" style="font-weight:bold;color:blue">Click Here for further processing</a></h4>';
	?>
	<script>
	$(document).ready(function(){
		setTimeout(function(){loadPage('<?=str_replace(ROOT_PATH,'',$transaction_response['producturl'])?>')},5000);
	});
	</script>
	<?php
}
else
{
	?>
	<h3>Your order status is <span class="label label-warning"><?=$transaction_response['status']?></span></h3>
	<h4>Your Transaction ID for this transaction is <?=$transaction_response['txnid']?></h4>
	<?php
	if($transaction_response['producturl'])
	echo '<br /><h4><a href="'.$transaction_response['producturl'].'" style="font-weight:bold;color:blue">Click Here for further processing</a></h4>';
	?>
	<script>
	$(document).ready(function(){
		setTimeout(function(){loadPage('<?=str_replace(ROOT_PATH,'',$transaction_response['producturl'])?>')},5000);
	});
	</script>
	<?php 
}
if($transaction_response['message'])
echo '<h4>'.$transaction_response['message'].'</h4>';
?>