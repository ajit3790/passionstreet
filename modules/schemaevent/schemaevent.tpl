<?php
//http://schema.org/Event
$schema = array();
$schema['@context'] 		= 'http://schema.org';
$schema['@type'] 		= 'Event';
$schema['name'] 		= $event['eventname'];
$schema['startDate'] 		= date('c',strtotime($event['datestart']));
$schema['endDate'] 		= date('c',strtotime($event['dateend']));
$schema['description'] 		= strip_tags($event['description']);
$schema['location'] 		= array(
					'@type'		=>'Place',
					'name'		=>$event['venue'],
					//'url'		=>'Place',
					'address'	=>array(
								'@type'			=>'PostalAddress',
								'addressLocality'	=>$event['city'],
								'addressRegion'		=>$event['state'],
								'streetAddress'		=>$event['address'],
								'addressCountry'	=>($event['country'])?$event['country']:'India',
								),
					'hasMap'	=>$event['mapimage']
					);
$schema['url']			= ROOT_PATH.'event/'.$event['seopath'];
$schema['image']		= $event['eventpic'];
$schema['aggregateRating']	= array(
					'@type'		=>'AggregateRating',
                                        'bestRating'    => 5,
                                        'worstRating'    => 1,
					'ratingValue'	=>($event['rating']['totalscore'])?$event['rating']['totalscore']:5,
					'ratingCount'	=>($event['rating']['users'])?$event['rating']['users']:1
					);
$schema['performer']	= array(
					'@type'		=>'Organization',
					'address'	=>array(
								'@type'			=>'PostalAddress',
								'addressLocality'	=>$event['city'],
								'addressRegion'		=>$event['state'],
								'streetAddress'		=>$event['organiser']['address'],
								'addressCountry'	=>($event['country'])?$event['country']:'India',
								),
					"email"=> $event['organiser']['email'],
					"name"=> $event['organiser']['name'],
					"telephone"=> $event['organiser']['cntnumber']
					);
$ticketValueRange = explode(',',$event['ticketvaluerange']);
if(count($ticketValueRange) > 1)
{
    $schema['offers']	= array(
					'@type'		=>'AggregateOffer',
					'highPrice'     =>$ticketValueRange[1],
                                        'lowPrice'      =>$ticketValueRange[0],
                                        'priceCurrency' => 'INR',
                                        'availabilityStarts'=>date('c',strtotime($event['datestart'])),
                                        'availabilityEnds'=>date('c',strtotime($event['dateend'])),
                                        'url'           =>$schema['url']
                                        );
}
else
{
    $schema['offers']	= array(
					'@type'		=>'Offer',
					'price'         =>($ticketValueRange[0])?$ticketValueRange[0]:0,
                                        'priceCurrency' => 'INR',
                                        'availabilityStarts'=>date('c',strtotime($event['datestart'])),
                                        'availabilityEnds'=>date('c',strtotime($event['dateend'])),
                                        'url'           =>$schema['url']
                                        );
}
echo '<script type="application/ld+json">';
echo json_encode($schema,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
echo '</script>';


$breadcrumbs = array();
$breadcrumbs[] = array('name'=>'Home','url'=>ROOT_PATH);
$breadcrumbs[] = array('name'=>'Events','url'=>ROOT_PATH.'events');
$breadcrumbs[] = array('name'=>$event['eventname'],'url'=>ROOT_PATH.'event/'.$event['seopath']);
$PSParams['breadcrumbs'] = $breadcrumbs;	

?>