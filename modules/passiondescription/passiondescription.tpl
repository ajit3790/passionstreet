<?php
$p = $PSParams['PSCategories'];
?>
<div class="row">
	<div class="col-md-12 no-padding">
	    <div class="passion-cat-banner clearfix">
	       
	       <picture>
		    <source srcset="images/<?=$passiontype.'-xl-mob.jpg'?>" media="(max-width: 479px)">
		    <source srcset="images/<?=$passiontype.'-xl-min.jpg'?>">
		    <img src="images/<?=$passiontype.'-xl-min.jpg'?>" alt="">
		</picture>
		   <div class="caption">
			<?php
			if(!in_array($passiontype,$PSData['user']['passions']))
			echo '<button id="passion_join" class=" btn btn-primary ajax-btn pull-right" data-action="post" data-aj="true" data-p-str="passion" data-t="passion-join" data-passion="'.$passiontype.'">Follow</button>';
			else
			echo '<button id="passion_join" class="btn btn-primary pull-right" >Following</button>';
			?>
			<h4 class="mb0"><?=$p[$passiontype]['name']?></h4>
					
			</div>
	    </div>
	</div>
</div>
<?php
$breadcrumbs = array();
$breadcrumbs[] = array('name'=>'Home','url'=>ROOT_PATH);
$breadcrumbs[] = array('name'=>'Passions','url'=>ROOT_PATH.'streetofpassion');
$breadcrumbs[] = array('name'=>$p[$passiontype]['name'],'url'=>ROOT_PATH.$p[$passiontype]['link']);
$PSParams['breadcrumbs'] = $breadcrumbs;
?>
