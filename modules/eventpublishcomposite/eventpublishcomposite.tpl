<?php
echo '<a class="pull-right btn btn-primary" onclick="$(\'.advancedoptions\').toggleClass(\'hide\')"><i class="fa fa-setting"> </i>Advanced Options</a>';
if($event['status'] == 'published')
echo '<span class="label btn label-success pull-right">Live</span>';
else
echo '<span class="label btn  label-success pull-right">Draft</span>';
?>
<h3><?=$event['eventname']?></h3>
<div class="panel-group accordion eventpublish" id="accordion">
    <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed alert-success" data-parent="#accordion" href="#eventdetails">
        Event Details - <small>Event details can be updated here</small>
        </a>
        <i class="fa fa-question-circle hide" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li></li></ul>"></i>
      </h4>
    </div>
      <!-- <div id="eventdetails" class="panel-collapse collapse" data-frame="module/eventcreatev2?event_id=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventdetailsdiv">
        </div>
    </div> -->
    
    <div id="eventdetails" class="panel-collapse collapse">
	<div class="panel-body form1  event-manage form-wrapper" id="eventdetaildiv">
	<?php
	render_module('eventcreatev2');
	?>
	</div>
    </div>
    </div>
    <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed <?=(($event['organiser'])?'alert-success':'alert-warning')?>" data-parent="#accordion" href="#eventorganiser">
        Organiser Contact - <small>Show case User / Page profile as organiser</small>
        </a>
        <i class="fa fa-question-circle hide" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li></li></ul>"></i>
      </h4>
    </div>
      <div id="eventorganiser" class="panel-collapse collapse" data-frame="module/admin/organisermanagement?entityType=<?=$producttype?>&entityId=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventorganiserdiv">
        </div>
    </div>
    </div>

  <div class="panel panel-default">
    <div class="panel-heading">	
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed <?=((($event['tickettype'] != 'free') && empty($event['ticketcategories']))?'alert-warning':'alert-success')?>" data-parent="#accordion" href="#eventticketing">
        Ticketing - <small>Define your event participation / attending fee</small>
        </a>
         <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>Select “Yes” if user is required to pay to participate</li><li>Define ticket category, price and allocate no of tickets you want to sell</li><li>You may add more ticket categories if required</li><li>In case of “No” ticket buying, define last date of registration entry</li></ul>"></i>
      </h4>
    </div>
      <?php if($event['tickettype'] == 'paid'){	?>
      <div id="eventticketing" class="panel-collapse collapse" data-frame="module/admin/ticketsmanagement?entityType=<?=$producttype?>&entityId=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventmanageticket">
        </div>
    </div>
    <?php }
    else{ ?>
	<div id="eventticketing" class="panel-collapse collapse">
        <div class="panel-body form1  event-manage form-wrapper" id="eventmanageticket">
		To Enable ticketing , change the event type to  "Paid" from Event Details
        </div>
    </div>
    <?php }?>
  </div>
<div class="advancedoptions hide">
<?php 
if($event['tickettype'] == 'paid')
{
?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed <?=((empty($event['bankaccount']))?'alert-warning':'alert-success')?>" data-parent="#accordion" href="#eventpaymentcollection">
        Payment Collection - <small>Define your bank account for payment transfer</small>
        </a> 
        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>Adding your bank A/c will help us transfer your payment quickly</li><li>You may record multiple bank A/c</li><li>One event money can not be transferred to multiple A/c</li><li>You may add more field if required</li></ul>"></i>
      </h4>
    </div>
      <div id="eventpaymentcollection" class="panel-collapse collapse" data-frame="module/admin/bankaccountmanagement?entityType=<?=$producttype?>&entityId=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventmanagebankaccount">
        </div>
    </div>
  </div>
<?php
}
?>
<?php 
if($event['tickettype'] == 'paid')
{
?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed alert-success" data-parent="#accordion" href="#eventdiscountmanagement">
        Coupon Management - <small>Define your coupons</small>
        </a>
        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>You can manage multiple coupons for same event</li><li>You may limit the discount on number of tickets</li><li>You may define the validity of the coupons</li><li>You may share the coupons with exclusive participants</li></ul>"></i>
      </h4>
    </div>
      <div id="eventdiscountmanagement" class="panel-collapse collapse" data-frame="module/admin/couponsmanagement?entityType=<?=$producttype?>&entityId=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventcoupons">
        </div>
    </div>
  </div>
<?php
}
?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed  alert-success" data-parent="#accordion" href="#eventatteneeregistration">
        Attendee / Participant registration - <small>Design user registration FORM</small>
        </a>
         <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>You may select the fields, which are required</li><li>Define which are mandatory information for user to enter</li><li>You may add more field if required</li></ul>"></i>
      </h4>
    </div>
      <div id="eventatteneeregistration" class="panel-collapse collapse" data-frame="module/admin/attendeefieldmanagement?entityType=<?=$producttype?>&entityId=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventattendee">
        </div>
    </div>
  </div>
  <?php
  if($event['scope'] != 'public')
  {
  ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed  alert-success" data-parent="#accordion" href="#eventinviteattendees">
        Invite Attendees - <small>Invite your friends and followers to join this event</small>
        </a>
         <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>You may select the fields, which are required</li><li>Define which are mandatory information for user to enter</li><li>You may add more field if required</li></ul>"></i>
      </h4>
    </div>
      <div id="eventinviteattendees" class="panel-collapse collapse" data-frame="module/admin/inviteattendees?entityType=<?=$producttype?>&entityId=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventattendee">
        </div>
    </div>
  </div>
  <?php
  }
  ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed alert-success" data-parent="#accordion" href="#eventquestionaire">
        Questionaire - <small>Define your question and ask user to answer to participate.</small>
        </a>
        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>You may define multiple question</li><li>Answer options could be simple text or multiple choice</li><li>You may also ask user to upload image / media file</li><li>You may share the coupons with exclusive participants</li></ul>"></i>
      </h4>
    </div>
      <div id="eventquestionaire" class="panel-collapse collapse" data-frame="module/admin/questionsmanagement?questiontype=eventquestionaire&relatedto=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventquestionairediv">
        </div>
    </div>
  </div>
  <?php
  if($event['eventtypebyfee'] == 2) //charity event
  {
  ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed  alert-success" data-parent="#accordion" href="#eventdonatiobox">
        Donations - <small>Configure here to accept donations</small>
        </a>
         <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>You may select the fields, which are required</li><li>Define which are mandatory information for user to enter</li><li>You may add more field if required</li></ul>"></i>
      </h4>
    </div>
      <div id="eventdonatiobox" class="panel-collapse collapse" data-frame="module/admin/donationmanagement?entityType=<?=$producttype?>&entityId=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventattendee">
        </div>
    </div>
  </div>
  <?php
  }
  ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed alert-success" data-parent="#accordion" href="#eventseo">
        SEO - <small>Make your event visible in online search</small>
        </a>
        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>Use SEO friendly event title (not more that 80 characters)</li><li>Describe your event in not more than 140 characters</li><li>Use relevant keywords as possible, no limit in keywords</li></ul>"></i>
      </h4>
    </div>
      <div id="eventseo" class="panel-collapse collapse" data-frame="module/admin/pagemetamanagement?pageType=eventDetail&pageId=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventseodiv">
        </div>
    </div>
  </div>
  <?php
  /*
  ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed alert-success" data-parent="#accordion" href="#eventseo">
        SEO - <small>Make your event visible in online search</small>
        </a>
        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>Use SEO friendly event title (not more that 80 characters)</li><li>Describe your event in not more than 140 characters</li><li>Use relevant keywords as possible, no limit in keywords</li></ul>"></i>
      </h4>
    </div>
      <div id="eventseo" class="panel-collapse collapse" >
        <?php
	render_module('admin/pagemetamanagement');
	?>
    </div>
  </div>
  <?php
  */
  ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed alert-success" data-parent="#accordion" href="#eventsocial">
        Social media Integration & Promotion - <small>You need a horse to ride; get the best out of it.</small>
        </a>
        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>Make it visible in all social media network</li><li>You have an option to share &amp; control on your other social media account</li><li>Engage with your participants / audience</li><li>Allow post on your event and make it viral</li></ul>"></i>
      </h4>
    </div>
    <div id="eventsocial" class="panel-collapse collapse">
        <div class="panel-body form1  event-manage form-wrapper" >
		<div class="row">
			<div class="col-md-6 col-sm-6">
				Integrate Facebook Page
			</div>
			<div class="col-md-6 col-sm-6">
				<label class="switch"><input type="checkbox" name="facebookpage" value="facebookpage"><span class="switchslider round"></span></label><br />
			</div>
		</div>
		<div class="row hide">
			<div class="col-md-6 col-sm-6">
				Auto Post on Twitter
			</div>
			<div class="col-md-6 col-sm-6">
				<label class="switch"><input type="checkbox" name="twitterpost" value="twitterpost" ><span class="switchslider round"></span></label><br />
			</div>
		</div>
		<div class="row hide">
			<div class="col-md-6 col-sm-6">
				Auto Post on Google Plus
			</div>
			<div class="col-md-6 col-sm-6">
				<label class="switch"><input type="checkbox" name="googlepost" ><span class="switchslider round"></span></label><br />
			</div>
		</div>
        </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed alert-success" data-parent="#accordion" href="#eventhtmlblocks">
        Display Blocks - <small>Add sponsors / itenaray and other display elements to your page</small>
        </a>
        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="popover" data-placement="bottom" data-trigger="hover" data-content="<ul><li>Make it visible in all social media network</li><li>You have an option to share &amp; control on your other social media account</li><li>Engage with your participants / audience</li><li>Allow post on your event and make it viral</li></ul>"></i>
      </h4>
    </div>
    <div id="eventhtmlblocks" class="panel-collapse collapse" data-frame="module/admin/htmlblocks?entityType=<?=$producttype?>&entityId=<?=$eventid?>">
        <div class="panel-body form1  event-manage form-wrapper" id="eventseodiv">
        </div>
    </div>
  </div>
  </div>
</div>
<?php
if($event['status'] == 'unpublished')
{ ?>
<div class="row hide publishbtn">
    <div class="col-sm-12  form-group ">
            <button value="submit" id="publishevent" type="submit" class="btn btn-primary">Publish</button>
    </div>
</div>
<?php } ?>
<div class="row">
    <div class="col-sm-12  form-group ">
        <a href="<?=ROOT_PATH.$producttype.'/'.$event['seopath']?>" target="_blank" class="btn btn-primary"><?=(($event['status'] == 'published')?'View Event':'Preview Event')?></a>
    </div>
</div>
<?php
$PSJsincludes['internal'][] = "js/eventpublish.js";
?>
<style>
    .form-wrapper{
        max-width:900px !important;
    }
</style>
<script>
   $("#publishevent").on('click',function(){
       myModal('Have you reviewed all fields ?','Once published , you will not be able to change certain fields . <div class="modal-footer" style="border:none;margin-top:15px;"><a href="javascript:void(0);" id="btnYes" class="btn danger alert-success" >Yes</a>      <a href="javascript:void(0);" data-dismiss="modal" aria-hidden="true" class="btn alert-warning">No</a>    </div>');
   });
   $("body").on('click',"#btnYes",function(){
       javascriptpost(orgurl,{"publish":1},"post");
   });
   $(document).ready(function(){
       unhidepublishbtn();
   });
</script>