<?php
$eventid = $_GET['event_id'];
$producttype = ($_GET['producttype'])?$_GET['producttype']:'event';
if(!($eventid) ||  !($event = event_get_details($eventid,1,PROFILE_ID,$producttype,true))){
    header("location:".ROOT_PATH.'events/create');
}
else if($event['creator'] != $_SESSION['user']['profile_id']){
    header("location:".ROOT_PATH.'events/create');
}
if($_POST){
    if($_POST['publish']){
            $inputParams = array();
            $inputParams['status'] = 'published';
            event_update($eventid,$inputParams);
            $event = event_get_details($eventid,1,PROFILE_ID,$producttype,true);
    }
}
unset($event['ticketcategories']['leadcapture']);
$PSParams['currenteventid'] = $eventid;
$PSParams['currentevent'] = $event;
$PSModData['eventid'] = $eventid;
$PSModData['event'] = $event;
$PSModData['producttype'] = $producttype;
?>