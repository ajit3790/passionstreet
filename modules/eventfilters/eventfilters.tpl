<div class="row <?=(($eventfilters['city'])?'':'hide')?>">
<div class="col-md-1">
Upcoming events by City
</div>
<div class="col-md-11">
<ul class = "nav nav-pills">
   <?php
   foreach($eventfilters['city'] as $i=>$data)
   {
   if($i>10)
   break;
   echo '<li class = "active"><a href = "'.ROOT_PATH."events/events-in-".strtolower(str_replace(' ','-',$data['city'])).'">'.$data['city'].'<span class ="badge">'.$data['count'].'</span></a></li>';
   }
   ?>
</ul>
<br />
</div>
</div>

<div class="row <?=(($eventfilters['category'])?'':'hide')?>">
<div class="col-md-1">
Upcoming events by Passion
</div>
<div class="col-md-11">
<ul class = "nav nav-pills">
   <?php
   foreach($eventfilters['category'] as $i=>$data)
   {
   if($i>10)
   break;
   echo '<li class = "active"><a href = "'.ROOT_PATH."events/events-in-".$data['category'].'-passion">'.ucwords(str_replace('-','',$data['category'])).'<span class ="badge">'.$data['count'].'</span></a></li>';
   }
   ?>
</ul>
<br />
</div>
</div>
<style>
.eventfilters{font-size:11px;}
.eventfilters .nav-pills li a{width:auto;padding:5px;background-color:#bbb;}
.eventfilters .nav-pills li{margin-bottom:2px;}
.eventfilters .badge{color:#000 !important;}
.eventfilters .col-centered{float: none;margin: 0 auto;}
</style>