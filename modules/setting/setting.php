<?php
$x['privacy'] = array(
    "userdp"=>"Who can see my Photo ?",
    "dob"=>"Who can see my DOB ?",
    "gender"=>"Who can see my Gender Orientation ?",
    "email"=>"My Email Id visible to ",
    "userdp"=>"My Contact No. visible to ",
    "occupation"=>"My Occupation visible to ",
    "citycountry"=>"My City & Country Information visible to ",
    "bio"=>"My Passion Profile statement visible to ",
    "timeline"=>"My Timeline visible to ",
    "rewardpoints"=>"My Rewards Point visible to "
);

$x['activity'] = array(
    "activtypost"=>"Who can see my activity post ?",
    "activitycomment"=>"Who can commenton my activity ?",
    "follow"=>"Who can follow me ?",
    "follower"=>"Who can see who all are my follower ?",
    "following"=>"Who can see who i am following ?",
    "message"=>"Who can message me ? "
);

$x['notification'] = array(
    "follow"=>"Follow Notification",
    "followeractivity"=>"Followers Activity Update",
    "eventcreation"=>"Event Creation Update",
    "eventactivityupdate"=>"Event's Activity Update",
    "expertadvise"=>"Expert Advise",
    "networkactivty"=>"Network Activity Update",
    "followingactivity"=>"Activity Update of whom i am following",
    "partneractivty"=>"Partner Activity Update"
);

$PSModData[$_GET['type']] = $x[$_GET['type']];
$PSModData['type'] = $_GET['type'];
$PSModData['options']=array(
    "Everyone"=>"Everyone",
    "Follower"=>"Follower",
    "FollowersFollower"=>"Follower's Follower",
    "Self"=>"Self"
);

$PSModData['options1']=array(
    "email"=>"Email",
    "onpassionstreet"=>"On Passion Street"
);

if($_GET['type'] == 'notification')
$module_display_style = "notification";

if($_POST['submit'])
{
    $PSJavascript['close_window'] = 1; 
    $module_display_style = "done";
}

?>