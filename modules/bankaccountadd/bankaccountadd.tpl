<form method="post" enctype="multipart/form-data">
<div class="row">
    <label class="control-label col-md-12  form-group">Kindly Provide your Bank Details to transfer the payment</label>
</div>
<div class="row">
    <div class="col-sm-12 form-group "><input type="text" placeholder="Account Holder Name" required="" pattern="[a-zA-Z .]{1,}" name="accountholdername" class="form-control"></div>
</div>
<div class="row">
    <div class="col-sm-6  col-xs-6  form-group "><input type="text" placeholder="Bank Name" required="" pattern="[a-zA-Z .]{1,}" name="bankname" class="form-control"></div>
    <div class="col-sm-6  col-xs-6   form-group "><input type="text" placeholder="Account No" required="" pattern="[a-zA-Z0-9- .]{1,}" name="acntnmbr" class="form-control"></div>
</div>
<div class="row">
    <div class="col-sm-6 col-xs-6 form-group ">
        <select required="" name="type" class="form-control" pattern="[a-zA-Z]{1,}">
            <option value="" selected="">Account type</option>
            <option value="saving">Saving Account</option>
            <option value="current">Current Account</option>
      </select>
    </div>
    <div class="col-sm-6 col-xs-6 form-group "><input type="text" name="ifsc" pattern="[a-zA-Z0-9- .]{1,}" placeholder="IFSC Code" required="" class="form-control"></div>
</div>
<div class="row">
    <div class="col-sm-12  form-group "><input type="text" name="address" pattern=".{1,}" placeholder="Branch Address" required="" class="form-control"></div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <input name="chequepic" id="chequepic" type="file"  required="" />
    </div>
</div>
<div class="row">
    <div class="col-sm-12  form-group "><input type="submit" name="submit" class="btn btn-primary" value="Save Account Details" /></div>
</div>
</form>
<script type="text/javascript">
$('#chequepic').filestyle({
    'icon' : false,
    'buttonText' : ' Browse',
    'placeholder': 'Upload Cancelled Cheque'
});
</script>