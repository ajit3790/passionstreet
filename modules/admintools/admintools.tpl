<style>
.toolsarea{display:none;}
.tools{cursor:pointer;}
.container .jumbotron, .container-fluid .jumbotron{border-radius:40px;min-height:140px;}
</style>
<script>
$(document).ready(function(){
    $(".tools").on('click',function(){
        $(".toolsarea").slideUp();
        $("#"+$(this).data('target')).slideDown();
    });
    $(".submitbtn").on('click',function(){
        var $params = {};
        var $currenttool = $(this).parents(".toolsarea");
        var $currenttoolid = $currenttool.attr('id');
        $currenttool.find(".param").each(function(){
            $params[$(this).data('type')] = $(this).val();
        });
        console.log($params);
        $.post(root_path+'admintoolsajax.php?type='+$currenttoolid,$params,function(data){
			var obj = typeof data != 'object' ? $.parseJSON(data) : data;
			if(obj['text'])
			$("#contentarea").html(obj['text']);
			else
            jsontotable(data,"#contentarea");
        });
    });
});
</script>
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12 tools jumbotron text-center" data-target="eventwiseticketdata">
        EventWise Ticketing Data
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 tools jumbotron text-center" data-target="eventwisetransactiondata">
        EventWise Transaction Data
    </div>
	<div class="col-md-3 col-sm-6 col-xs-12 tools jumbotron text-center" data-target="eventwiseuserdata">
        EventWise User Data
    </div>
	<div class="col-md-3 col-sm-6 col-xs-12 tools jumbotron text-center" data-target="ticketbytxnid">
        Resend Event Ticket (transaction id)
    </div>
	<div class="col-md-3 col-sm-6 col-xs-12 tools jumbotron text-center" data-target="ticketbyemail">
        Resend Event Ticket (email)
    </div>
	<div class="col-md-3 col-sm-6 col-xs-12 tools jumbotron text-center" data-target="loginasuser">
        Login as user(profile id)
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 tools jumbotron text-center" data-target="fbeventfetch">
        Fetch FB Event
    </div>
</div>
<div class="row">
    <div class="col-md-12 toolsarea" id="eventwiseticketdata">
        <h4>EventWise Ticketing Data</h4>
        <input type="text" class="param" data-type="event_id"  placeholder="event id"/>
	<select class="param" data-type="status">
		<option value="success">Successfull</option>
		<option value="failure">Failure</option>
		<option value="all">All</option>
	</select>
        <input type="button" class="submitbtn" value="Submit" />
    </div>
    <div class="col-md-12 toolsarea" id="eventwisetransactiondata">
        <h4>EventWise Transaction Data</h4>
        <input type="text" class="param" data-type="event_id" placeholder="event id" />
        <select class="param" data-type="status">
		<option value="success">Successfull</option>
		<option value="failure">Failure</option>
		<option value="all">All</option>
	</select>
        <input type="button" class="submitbtn" value="Submit" />
    </div>
	<div class="col-md-12 toolsarea" id="eventwiseuserdata">
        <h4>EventWise User Data</h4>
        <input type="text" class="param" data-type="event_id" placeholder="event id" />
        <select class="param" data-type="status">
		<option value="success">Successfull</option>
		<option value="failure">Failure</option>
		<option value="all">All</option>
	</select>
        <input type="button" class="submitbtn" value="Submit" />
    </div>
	<div class="col-md-12 toolsarea" id="ticketbytxnid">
        <h4>Generate & send event ticket by transaction id</h4>
        <input type="text" class="param" placeholder="txnid"  data-type="txnid" />
        <input type="button" class="submitbtn" value="Submit" />
    </div>	
	<div class="col-md-12 toolsarea" id="ticketbyemail">
        <h4>Generate & send event ticket by  email</h4>
        <input type="text" class="param" placeholder="email" data-type="email" />
        <input type="button" class="submitbtn" value="Submit" />
    </div>
	<div class="col-md-12 toolsarea" id="loginasuser">
        <h4>Login as user</h4>
        <input type="text" class="param" placeholder="profile_id" data-type="profile_id" />
        <input type="button" class="submitbtn" value="Submit" />
    </div>
    <div class="col-md-12 toolsarea" id="fbeventfetch">
        <h4>Fetch FB Events</h4>
        <input type="text" class="param" placeholder="comma separated fb event ids" data-type="eventids" />
        <input type="button" class="submitbtn" value="Submit" />
    </div>
</div>
<div class="row" id="contentarea">
<div class="col-md-12">

</div>
</div>
