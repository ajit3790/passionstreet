<?php
$producttype = (($_GET['producttype'] == 'tour')?'tour':'event');
if($PSParams['currenteventid'])
{
$eventid = $PSParams['currenteventid'];
$event = $PSParams['currentevent'];
}
else
{
$eventid = $_REQUEST['event_id'];
$event = event_get_details($eventid,1,PROFILE_ID,true);
}
if(!($eventid) ||  !($event)){
	$PSModData['editmode'] = 0;
    $PSModData['allowedit'] = 0;
	$PSModData['allowedit1'] = 1;
}
else
{
	$PSModData['editmode'] = 1;
	$PSModData['event'] = $event;
	if($event['creator'] != $PSData['user']['profile_id'])
	{
		header('location:'.ROOT_PATH); 
		exit();
	}
        if($event['status'] != 'published'){
            $PSModData['allowedit'] = 1;
			$PSModData['allowedit1'] = 1;
        }
        else
		{
        $PSModData['allowedit'] = 0;
		$PSModData['allowedit1'] = 0;
		}
}
if(isset($_POST['event-create']))
{
    usePlugin('htmlpurifier');
    if(!empty($_POST['description']) && !empty($PSParams['plugins']['htmlpurifier']))$_POST['description'] = $PSParams['plugins']['htmlpurifier']->purify($_POST['description']);
    if(!empty($_POST['terms']) && !empty($PSParams['plugins']['htmlpurifier']))$_POST['terms'] = $PSParams['plugins']['htmlpurifier']->purify($_POST['terms']);
    
    if(true){
        extract($_POST);
        $insertData = array();
        $insertData['event_id'] = generate_id("event");
		$insertData['producttype'] = $producttype;
        $insertData['creator'] = $_SESSION['user']['profile_id'];
		if($producttype  != 'event')
        $insertData['eventtypebyfee'] = 3;
        else
		$insertData['eventtypebyfee'] = (($_POST['eventtypebyfees'])?$_POST['eventtypebyfees']:3);
	
		$insertData['eventtype'] = $eventType;
		$insertData['scope'] = ($eventscope)?$eventscope:'public';
        $insertData['eventname'] = $name;
        $insertData['category'] = $category;
        if(($PSParams['eventdefault']['typesbasedonfees'][$insertData['eventtypebyfee']]['processing'] + $PSParams['eventdefault']['typesbasedonfees'][$insertData['eventtypebyfee']]['gatewayfee'])>0)
        $insertData['tickettype'] = 'paid';
        else
        $insertData['tickettype'] = 'free';
        $insertData['multislot'] = 0;
        if($_POST['timeslot'] != 'single')
        {
            $slot1 = array();
            $slot2 = array();
            foreach($datestart as $index=>$x){
                if($producttype != 'event')
				$datebookingends[$index] = $dateend[$index];
				$datestart[$index] = generate_standard_date($datestart[$index]);
                $dateend[$index] = generate_standard_date($dateend[$index]);
				$slot1[$datestart[$index]]['datestart'] = $datestart[$index];
                $slot1[$datestart[$index]]['dateend'] = $dateend[$index];
                $slot1[$datestart[$index]]['bookingenddate'] = generate_standard_date($datebookingends[$index]);
                $slot2[$dateend[$index]]['datestart'] = $dateend[$index];
                $slot2[$dateend[$index]]['dateend'] = $dateend[$index];
                $slot2[$dateend[$index]]['bookingenddate'] = generate_standard_date($datebookingends[$index]);
            }
            asort($slot1);
            arsort($slot2);
            $slots = array_values($slot1);
            if(count($slots) > 1)
            {
                $insertData['multislot'] = 1;
            }
            $insertData['datestart'] = generate_standard_date($slot1[key($slot1)]['datestart']);
            $insertData['dateend'] = generate_standard_date($slot2[key($slot2)]['dateend']);
            $insertData['bookingenddate'] = generate_standard_date($slot2[key($slot2)]['bookingenddate']);
        }
        else
        {
            $insertData['multislot'] = 0;
            $index = 0;
			if($producttype != 'event')
			$datebookingends[$index] = $dateend[$index];
		
            $insertData['datestart'] = generate_standard_date($datestart[$index]);
            $insertData['dateend'] = generate_standard_date($dateend[$index]);
            $insertData['bookingenddate'] = generate_standard_date($datebookingends[$index]);
            $slot1 = array();
            $x = array();
            $x['datestart'] = $insertData['datestart'];
            $x['dateend'] = $insertData['dateend'];
            $x['bookingenddate'] = $insertData['bookingenddate'];
            $slot1[] = $x;
        }
        $insertData['description'] = chop_string($description,300);
        $insertData['eventinfo']['description'] = $description;
        $insertData['eventinfo']['terms'] = $terms;
        
        $map_loc_components = json_decode($map_loc_components,true);
        $insertData['venue'] = ($map_loc_components['name'])?$map_loc_components['name']:'';
        $insertData['city'] = ($map_loc_components['city'])?$map_loc_components['city']:'';
        $insertData['state'] = ($map_loc_components['state'])?$map_loc_components['state']:'';
        $insertData['eventinfo']['address'] = strip_tags($address,'<br></br />');
        $insertData['eventinfo']['maploc'] = ($map_loc_coordinate)?$map_loc_coordinate:'';
        $insertData['eventinfo']['mapimage'] = upload_from_url('eventmap',$map_loc_image);
        
        //$insertData['eventpic'] = upload_from_uri('eventbanner',$pic_post);
        if($_POST['multicropdata']['eventbanner'])
        $insertData['eventpic'] = upload_post_images_new2($_FILES,'eventbanner');
        else
        $insertData['eventpic'] = array();
        $insertData['eventpic'] = serialize($insertData['eventpic']);
        // $insertData['termsdoc'] = upload('eventtnc','file',$_FILES['termsdoc']);
        $insertData['eventinfo']['termsdoc'] = '';
        $insertData['createdate'] = date("Y/m/d h:i:s");
        $insertData['status'] = 'unpublished';
        
        // if(!empty(event_create($insertData)))
        if(event_create($insertData))
        {
            $insertData2 = array();
            $insertData2['producttype'] = $producttype;
            $insertData2['productid'] = $insertData['event_id'];
            foreach($slot1 as $key=>$data){
                $insertData2['slotid'] = generate_id("slot");
                $insertData2['datestart'] = $slot1[$key]['datestart'];
                $insertData2['dateend'] = $slot1[$key]['dateend'];
                $insertData2['bookingenddate'] = $slot1[$key]['bookingenddate'];
                datetimeslot_add($insertData2);
            }
            
            $PSModData['server_response_text'] = 'Event Creation Successfull';
            $PSModData['server_response_status'] = 1;
            $PSModData['server_response_html'] = '<div class="msgbox1 text-center">
                    <p>Your Event / Workshop has been created successfully .<br /> Click <a href="'.ROOT_PATH.$producttype.'/publish/'.$insertData['event_id'].'">Here to publish it.</p>
                </div>';
		header('location:'.ROOT_PATH.$producttype.'/publish/'.$insertData['event_id']);
        }
        else {
            $PSModData['params'] = $_POST;
            $PSJavascript['map_position']=$_POST['map_loc_coordinate'];
            $PSModData['server_response_html'] = '<div class="msgbox1 text-center"><p>Event Creation Failed</p></div>';
            $PSModData['server_response_status'] = 0;
        }
    }
    else
    {
        $PSModData['params'] = $_POST;
        $PSJavascript['map_position']=$_POST['map_loc_coordinate'];
        $PSModData['server_response_html'] = '<div class="msgbox1 text-center"><p>Event Creation Failed</p></div>';
        $PSModData['server_response_status'] = 0;
    }
}
if(isset($_POST['event-update']) && $_POST['event_id'])
{
    usePlugin('htmlpurifier');
    if(!empty($_POST['description']) && !empty($PSParams['plugins']['htmlpurifier']))$_POST['description'] = $PSParams['plugins']['htmlpurifier']->purify($_POST['description']);
    if(!empty($_POST['terms']) && !empty($PSParams['plugins']['htmlpurifier']))$_POST['terms'] = $PSParams['plugins']['htmlpurifier']->purify($_POST['terms']);
    if(true){
        extract($_POST);
        $insertData = array();
		$insertData['eventinfo'] = $event['eventinfo'];
		//$insertData['producttype'] = $producttype;
		$eventid = $_POST['event_id'];
        if($eventType) $insertData['eventtype'] = $eventType;
        if($category) $insertData['category'] = $category;
        
        if($PSModData['allowedit'] == 1)
        {
			$insertData['scope'] = ($eventscope)?$eventscope:'public';
			if($producttype  != 'event')
            $insertData['eventtypebyfee'] = 3;
			else
            $insertData['eventtypebyfee'] = (($_POST['eventtypebyfees'])?$_POST['eventtypebyfees']:3);
            $insertData['eventname'] = $name;
        
            if($map_loc_components)
            {    
            $map_loc_components = json_decode($map_loc_components,true);
            $insertData['venue'] = $map_loc_components['name'];
            $insertData['city'] = $map_loc_components['city'];
            $insertData['state'] = $map_loc_components['state'];
            $insertData['eventinfo']['address'] = strip_tags($address,'<br></br />');
            $insertData['eventinfo']['maploc'] = $map_loc_coordinate;
            $insertData['eventinfo']['mapimage'] = upload_from_url('eventmap',$map_loc_image);
            }
            if(($PSParams['eventdefault']['typesbasedonfees'][$insertData['eventtypebyfee']]['processing'] + $PSParams['eventdefault']['typesbasedonfees'][$insertData['eventtypebyfee']]['gatewayfee'])>0)
            $insertData['tickettype'] = 'paid';
            else
            $insertData['tickettype'] = 'free';
            $insertData['multislot'] = 0;
			if($_POST['timeslot'] != 'single')
            {
                $slot1 = array();
                $slot2 = array();
				foreach($datestart as $index=>$x){
					if($producttype != 'event')
					$datebookingends[$index] = $dateend[$index];
                    $datestart[$index] = generate_standard_date($datestart[$index]);
                    $dateend[$index] = generate_standard_date($dateend[$index]);
                    $slot1[$datestart[$index]]['slotid'] = $slotid[$index];
                    $slot1[$datestart[$index]]['datestart'] = $datestart[$index];
                    $slot1[$datestart[$index]]['dateend'] = $dateend[$index];
                    $slot1[$datestart[$index]]['bookingenddate'] = generate_standard_date($datebookingends[$index]);
                    $slot1[$datestart[$index]]['slotid'] = $slotid[$index];
                    $slot2[$dateend[$index]]['datestart'] = $dateend[$index];
                    $slot2[$dateend[$index]]['dateend'] = $dateend[$index];
                    $slot2[$dateend[$index]]['bookingenddate'] = generate_standard_date($datebookingends[$index]);
                }
                asort($slot1);
                arsort($slot2);
                $slots = array_values($slot1);
                if(count($slots) > 1)
                {
                    $insertData['multislot'] = 1;
                }
                $insertData['datestart'] = generate_standard_date($slot1[key($slot1)]['datestart']);
                $insertData['dateend'] = generate_standard_date($slot2[key($slot2)]['dateend']);
                $insertData['bookingenddate'] = generate_standard_date($slot2[key($slot2)]['bookingenddate']);
            }
            else
            {
                $insertData['multislot'] = 0;
                $index = 0;
				if($producttype != 'event')
				$datebookingends[$index] = $dateend[$index];
                $insertData['datestart'] = generate_standard_date($datestart[$index]);
                $insertData['dateend'] = generate_standard_date($dateend[$index]);
                $insertData['bookingenddate'] = generate_standard_date($datebookingends[$index]);
                $slot1 = array();
                $x = array();
                $x['slotid'] = $slotid[0];
                $x['datestart'] = $insertData['datestart'];
                $x['dateend'] = $insertData['dateend'];
                $x['bookingenddate'] = $insertData['bookingenddate'];
                $slot1[] = $x;
            }
        }
		$insertData['description'] = chop_string($description,300);
		$insertData['eventinfo']['description'] = $description;
        $insertData['eventinfo']['terms'] = $terms;
	
		if($pic_post)
        $insertData['eventpic'] = upload_from_uri('eventbanner',$pic_post);
        else if($_POST['multicropdata']['eventbanner'])
        $insertData['eventpic'] = upload_post_images_new2($_FILES,'eventbanner');
        if(empty($insertData['eventpic']))
        unset($insertData['eventpic']);
        else
        $insertData['eventpic'] = serialize($insertData['eventpic']);
        
		// $insertData['termsdoc'] = upload('eventtnc','file',$_FILES['termsdoc']);
        //$insertData['termsdoc'] = '';
	
		event_update($eventid,$insertData);
	
            $insertData2 = array();
            $where = array();
            $insertData2['producttype'] = $producttype;
            $insertData2['productid'] = $eventid;
	    	
            foreach($slot1 as $key=>$data){
                $insertData2['datestart'] = $slot1[$key]['datestart'];
                $insertData2['dateend'] = $slot1[$key]['dateend'];
                $insertData2['bookingenddate'] = $slot1[$key]['bookingenddate'];
				if($slot1[$key]['slotid'])
                {
                $where['slotid'] = $slot1[$key]['slotid'];
                datetimeslot_update($insertData2,$where);
                }
                else
                {
                $insertData2['slotid'] = generate_id("slot");
                datetimeslot_add($insertData2);
                }

            }
	        
	    $PSJavascript['eventupdated'] = 1;
	    if($redirectpath)
		header("location:".$redirectpath);
	    exit();
    }
}
$PSJavascript['eventdefault'] = $PSParams['eventdefault'];
$PSModData['eventTypes'] = $PSParams['eventTypes'];
$PSModData['producttype'] = $producttype;
$PSModData['producttypestring'] = ucwords($PSModData['producttype']);
?>