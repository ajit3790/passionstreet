<div class="col-md-12">
<?php
if($editmode != 1)
{ 
?>
<div class="row">
    <div class="col-md-12"><h2 class="hstyl_2 mb-large"><span><i class="fa fa-pencil-square-o"></i><?=($producttype == 'tour')?'Create Tour':'Create Event'?></span><span class="hidden-xs" onclick="$('.prcng_optns').removeClass('hide');" style="float:right;font-size:14px; cursor:pointer;">Click for pricing option</span></h2></div>
</div>
<?php
}
?>
<div class="row">
    <div class="col-md-12">
        <?=$server_response_html?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-wrapper">
	    <?php
	    //print_array($event);
	    //if($editmode == 1)
	    //echo '<form class="form1 event-creation" action="'.ROOT_PATH.'events/publish/'.$event['event_id'].'" method="post" enctype="multipart/form-data">';
	    //else
	    //echo '<form class="form1 event-creation" action="'.ROOT_PATH.'events/create'.'" method="post" enctype="multipart/form-data">';
            echo '<form class="form1 event-creation" action="'.$_SERVER['PHP_BROWSER_URL'].'" method="post" enctype="multipart/form-data">'
	    ?>
	    <input name="redirectpath" type="hidden" value="<?=$_SERVER['PHP_BROWSER_URL']?>">
	    <?php
	    if($producttype != 'tour')
	    { ?>
            <div class="row customcheckbox">
                <?php
                foreach($PSParams['eventdefault']['typesbasedonfees'] as $id=>$typedata)
                {
                $isPaidType = (($typedata['processingfee'] + $typedata['gatewayfee']) > 0)
                ?>
                <div class="col-sm-4 <?=(($typedata['name'] == 'NON-PROFIT')?'col-xs-6':'col-xs-3')?>">
                    <label>
                        <input name="eventtypebyfees" class="eventtypebyfees" data-value="<?=(($isPaidType)?'paid':'free')?>" value="<?=$id?>" type="radio" <?=(($event['eventtypebyfee'])?(($id == $event['eventtypebyfee'])?'checked':''):(($typedata['name'] == 'PAID')?'checked':''))?>>
                        <div class="tab-data">
                            <?=$typedata['name']?>
                        </div>
                    </label>
                </div>
                <?php
                }
                ?>
            </div>
	    <?php 
	    }
	    ?>
			<?php 
			if($producttype != 'tour')
			{
			?>
			<div class="row  customcheckbox1 hide">
				<div class="col-xs-6">
					<label data-toggle="tooltip" data-placement="bottom" title="Anyone can join">
						<input name="eventscope" value="public" type="radio" <?=(($editmode == 1)?(($event['scope'] == 'public')?'checked':''):'checked')?>  <?=(($allowedit1 == 1)?'':' readonly onclick="javascript: return false;"')?>>
						Public
					</label>
				</div>
				<div class="col-xs-6">
					<label data-toggle="tooltip" data-placement="bottom" title="Invited can view ,Invited can only join">
						<input name="eventscope" value="private" type="radio" <?=(($editmode == 1)?(($event['scope'] == 'private')?'checked':''):'')?>  <?=(($allowedit1 == 1)?'':' readonly onclick="javascript: return false;"')?>>
						Private
					</label>
				</div>
			</div>
			<?php
			}
			?>
            <div class="row ">
                  <div class="col-sm-12 form-group ">
                      <input type="text" value="<?=($editmode == 1)?$event['eventname']:$params['name']?>" name="name" placeholder="<?=$producttypestring?> Title" required="" class="form-control" <?=($editmode == 1 && $allowedit != 1)?'disabled':''?>>
                  </div>
               </div> 
                <div class="row form-group eventTypeRow">
                    <div class="col-sm-6 col-xs-6 ">
                        <select name="eventType" id="eventType" class="form-control" required pattern=".{1,}">
                            <option value="" selected="">Select <?=$producttypestring?> Type</option>
                            <?php
                            foreach($eventTypes as $eventType)
                            {
				$selected = (($params['eventType']==$eventType) || ($editmode == 1 && $eventType==$event['eventtype']))?'selected':'';
                                echo "<option value='".$eventType."' $selected >".$eventType."</option>";
                            }
                            ?>
                        </select>
                    </div>
                     <div class="col-sm-6 col-xs-6 ">
                        <select name="category" id="category" class="form-control" required pattern=".{1,}">
                            <option value="" selected="">Select Category</option>
                            <?php
                            foreach($PSParams['PSCategories'] as $PSCategorykey=>$PSCategory)
                            {
                                $selected = (($params['category']==$PSCategorykey) || ($editmode == 1 && $PSCategorykey==$event['category']))?'selected':'';
                                echo "<option value='".$PSCategorykey."' $selected >".$PSCategory['name']."</option>";
                            }
                            echo '<option value="others">Category Not in List ?</option>';
                            ?>
                        </select>
                  </div>
                  <div class="col-sm-6 col-md-6 categorydiv hide">
                     <input type="text" name="category" id="othercat" disabled  placeholder="Define Category Name" class="form-control " />
                  </div>
               </div>
               <div class="row form-group hide">
                    <div class="col-md-6 col-sm-6">
                        <a href="javascript:void(0)" data-toggletype="date" data-toggle="collapse" class="togglediv" data-target=".event-date"><span> - Remove</span> Date & Time</a>
                        <input type="hidden" id="adddate" name="adddate" value="1">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <a href="javascript:void(0)" data-toggletype="location" data-toggle="collapse" class="togglediv" data-target=".event-location"><span> - Remove</span> Event location</a>
                        <input type="hidden" id="addlocation" name="addlocation" value="1">
                    </div>
                </div>
		<?php 
		if($producttype != 'tour')
		{
		?>
                <div class="row form-group">
                    <div class="col-xs-6">
                        <label>
                            <input name="timeslot" class="timeslotradio" value="single" type="radio" <?=(($editmode == 1)?((count($event['datetimeslots'])==1)?'checked':''):'checked')?>>
                            Single Session
                        </label>
                    </div>
                    <div class="col-xs-6">
                        <div style="position: relative" class="<?=(($editmode == 1)?((count($event['datetimeslots'])==1)?'hide':''):'hide')?> timeslotbtndiv">
			    <span class="btn1 btn-default addmore" data-limit="-1" data-callback="addMoreDates" data-trgtelement="event-date" style="position:absolute;right:0px;padding:3px 10px;">
				+
			    </span>
			</div>
			<label>
                            <input name="timeslot" class="timeslotradio" value="multiple" type="radio" <?=(($editmode == 1)?((count($event['datetimeslots'])==1)?'':'checked'):'')?>>
                            Multiple Sessions
                        </label>
			
                    </div>
                </div>
		<?php
		}
		?>
                <div class="event-date-wrapper">
                <?php
		if(empty($event['datetimeslots']) || $editmode != 1)
                $event['datetimeslots'][] = array();
		foreach($event['datetimeslots'] as $slot)
                {
                    $datestart = '';
                    $dateend = '';
                    $bookingenddate = '';

                    if($slot['datestart'])
                    $datestart = date("d/m/Y h:i:s",strtotime($slot['datestart']));
                    if($slot['dateend'])
                    $dateend = date("d/m/Y h:i:s",strtotime($slot['dateend']));
                    if($slot['bookingenddate'])
                    $bookingenddate = date("d/m/Y h:i:s",strtotime($slot['bookingenddate']));
                    if($slot['id'])
                    echo '<input type="hidden" name="slotid[]" value="'.$slot['slotid'].'">';
                    ?>
                    <div class="row event-date collapse in" aria-expanded="true">
                        <div class="<?=($producttype!='event')?'col-md-6 col-sm-6':'col-md-4 col-sm-4'?> date-element">
                              <!--<label class="control-label  form-group">From</label>-->
                              <div class="form-group date">
                                  <i class="fa fa-calendar"></i>
                                  <input type="text" name="datestart[]" value="<?=($editmode == 1)?($datestart):$params['datestart']?>" required=""  placeholder="<?=(($producttype!='event')?'Season starts':'Start Date')?>" data-type="<?=(($producttype!='event')?'datepicker':'datetimepicker')?>" class="form-control date date-future datepicker datestart" style="background-color: #fff">
                              </div>  
                        </div>
                        <div class="<?=($producttype!='event')?'col-md-6 col-sm-6':'col-md-4 col-sm-4'?> date-element">     
                              <!--<label class="control-label form-group">To</label>-->
                              <div class="form-group date">
                                  <i class="fa fa-calendar"></i>
                                  <input type="text" name="dateend[]" value="<?=($editmode == 1)?($dateend):$params['dateend']?>" required=""  placeholder="<?=(($producttype!='event')?'Season Ends':'End Date')?>" data-type="<?=(($producttype!='event')?'datepicker':'datetimepicker')?>" class="form-control date date-future datepicker dateend" style="background-color: #fff">
                              </div>  
                        </div>
                        <div class="<?=($producttype!='event')?'col-md-6 col-sm-6 hide':'col-md-4 col-sm-4'?> date-element">     
                              <!--<label class="control-label form-group">To</label>-->
                              <div class="form-group date">
                                  <i class="fa fa-calendar"></i>
                                  <input type="text" name="datebookingends[]" value="<?=($editmode == 1)?($bookingenddate):$params['dateend']?>" <?=(($producttype!='event')?'':'required=""')?> placeholder="Last date of booking" data-type="<?=(($producttype!='event')?'datepicker':'datetimepicker')?>" class="form-control date date-future datepicker datebookingends" style="background-color: #fff">
                              </div>  
                        </div>
                    </div>
                    <?php
                }
                ?>
                </div>  

               <div class="row  event-location collapse in" aria-expanded="true">
                  <div class="col-sm-6 col-md-6">
                      <div class="form-group ">
                         <input type="text" name="event_venue" id="map-input" placeholder="<?=$producttypestring?> Venue" class="form-control date" <?=($editmode == 1 && $allowedit!=1)?'disabled':''?>  value="<?=($editmode==1)?$event['venue']:''?>">
                      </div>
                      <div class="form-group ">
                         <textarea placeholder="Complete Address" name="address" id="address" required class="form-control"><?=($editmode == 1)?$event['address']:$params['address']?></textarea>
                      </div>

                  </div>
                  <div class="col-sm-6 col-md-6 form-group">
                      <div id="map" style="height:170px;width:100%"></div>
                  </div>
               </div>
                <div class="row ">
                    <div class="col-sm-12 col-md-12 form-group">
                        <input type="file" name="eventbanner[]" croptype="eventbanner" id="upload2" class="multifilecrop" multiple data-crpvrsn=2 >
                        <!--<input type="file" pictype="eventbanner"  org-width=400 org-height=240 callback="filetopost" id="event-pic" name="pic"  style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" class="filecrop" tabindex="-1"><div class="bootstrap-filestyle input-group"><input type="text" disabled="" placeholder="If Event Banner is not uploaded, we will use default" class="form-control "> <span class="group-span-filestyle input-group-btn" tabindex="0"><label class="btn btn-default " for="event-pic"><span class="buttonText"> Browse</span></label></span></div>--> 
                    </div>
                </div>
               <?php 
               if($editmode == 1)
               {?>
                <div class="row ">
                  <div class="col-sm-12 col-md-12 form-group">
                      <img src="<?=$event['eventpic']?>" style="width:100%" />
                  </div>
                </div>	       
                <?php
                }
                ?>
                <div class="form-group row">
                    <label class="col-sm-12  col-md-12 form-group  control-label">Brief Description of <?=$producttypestring?></label>	
                    <div class="col-sm-12  col-md-12">
                        <textarea placeholder="Brief Description of event" name="description" id="description" required class="form-control simpleeditor"><?=($editmode==1)?$event['description']:$params['description']?></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-12  col-md-12 form-group  control-label">Write <?=$producttypestring?> Participation Terms &amp; Condition</label>	
                    <div class="col-sm-12  col-md-12">
                        <textarea placeholder="Write Event participation Terms &amp; Condition" name="terms" id="terms" required class="form-control simpleeditormin"><?=($editmode==1)?$event['terms']:$params['terms']?></textarea>
                    </div>
                </div>
                <div class="form-group row hide">
                    <div class="col-sm-12  col-md-12">
                        <input type="file" id="TnC" name="termsdoc" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" tabindex="-1"><div class="bootstrap-filestyle input-group"><input type="text" disabled="" placeholder="Upload T&amp;C doc file" class="form-control "> <span class="group-span-filestyle input-group-btn" tabindex="0"><label class="btn btn-default " for="TnC"><span class="buttonText"> Browse</span></label></span></div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 checkbox text-center">
                        <label><input required="" type="checkbox"> I acknowledge &amp; accept all <a href="terms" style="color:blue">terms &amp; conditions</a> of PASSIONSTREET.IN</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12  col-md-12 text-center">
                        <?php 
				if($editmode == 1)
				{
				echo "<input type='hidden' name='event_id' value='".$event['event_id']."'>";
				echo "<input type='hidden' name='editmode' value='1'>";
				echo '<button class="btn btn-primary" type="submit" name="event-update" value="1">Update</button>';
				}
				else
				echo '<button class="btn btn-primary" type="submit" name="event-create" value="1">Create</button>';
			?>
                    </div>
                </div>
                <input type="hidden" id="event-pic_post" name="pic_post" value="<?=$params['pic_post']?>">
            </form>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function(){
        $('.timeslotradio[type="radio"]').click(function(){
            if ($(this).is(':checked'))
            {
              if($(this).val() == 'single')
              {
                  $(".timeslotbtndiv").addClass("hide");
                  $(".event-date.dynamicallyadded").addClass("hide");
              }
              else if($(this).val() == 'multiple')
              {
                    $(".timeslotbtndiv").removeClass("hide");
                    $(".event-date.dynamicallyadded").removeClass("hide");
                    if($(".event-date").length < 2)
                    $(".timeslotbtndiv .addmore").trigger("click");
              }
            }
        });
        $(".togglediv").on('click',function(e){
            e.preventDefault();
            $x = $(this).find('span').text();
            $y = $(this).attr('data-toggletype');
            $z = $(this).attr('data-target');
            if($x.indexOf("Add") >=0)
            {
                $("#add"+$y).val(1);
                $(this).find('span').text(' - Remove');
                $($z).find("input.form-control").prop('required', true);
            }
            else
            {
                $("#add"+$y).val(0);
                if($y == 'date')
                myModal('Alert','When Date & time is not set , the event page might not have certain time based call to action buttons like "Buy now"');
                else if($y == 'location')
                myModal('Alert','When location is not set , the event will be considered "Online" event and some features like Map & Routemap will not be available');
                $(this).find('span').text(' + Add');
                $($z).find("input.form-control").prop('required', false);
            }
        });
        if(typeof eventupdated != 'undefined' && eventupdated == 1 && parentwindowcontext == 'parent'){
            executeFunctionByName('reloadwindow');
        }
    });
</script>
<?php
$PSJsincludes['external'][] = "https://maps.googleapis.com/maps/api/js?key=".$PSParams['google_maps_key']."&libraries=places&callback=initGoogleMapsAutocomplete";
//$PSJsincludes['external'][] = "js/jquery.datetimepicker.js";
//$PSCssincludes['external'][] = "css/jquery.datetimepicker.css";
//$PSJsincludes['external'][] = "js/jquery.Jcrop.js";
//$PSCssincludes['external'][] = "css/jquery.Jcrop.css";
$PSJsincludes['internal'][] = "js/eventcreate.js";
$PSJsincludes['internal'][] = "js/bootstrap-filestyle.min.js";
$PSJsincludes['external'][] = "js/bootstrapdatetimepicker.min.js";
$PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
$PSJsincludes['external'][] = "js/fengyuanchencropper/croppermain2.js";
$PSJsincludes['external'][] = "js/fengyuanchencropper/cropper.js";
$PSCssincludes['external'][] = "js/fengyuanchencropper/cropper.css";
$PSJsincludes['external'][] = 'js/summernote-master/dist/summernote.js';
$PSCssincludes['external'][] = "js/summernote-master/dist/summernote.css";
?>