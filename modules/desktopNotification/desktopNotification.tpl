<style>
  label.error {color:red;}
</style>
<div class="col-md-12">
  <div class="row">
    <div class="col-md-10">
        <div class="form-wrapper">   
          <div class="row">
    	      <div class="col-md-12">
    	          <h2 class="hstyl_2 "><span><i class="fa fa-pencil-square-o"></i>Send Desktop Notification</span></h2>
    	      </div>
    	    </div>	   
          <form class="form1" action="" method="post">
          <?php
          	if(isset($_SESSION['flash']) === true ) {
          	   echo "<strong>Success !</strong> Notification has been sent.";
          	   unset($_SESSION['flash']);
          	}
          ?>
            <div class="row">
              <div class="col-sm-6 col-md-4">
                <label>Type</label>
              </div>              
              <div class="col-sm-6 col-md-8 form-group ">                
                <select name="type" class="form-control">
                   <option value=""> Notification Type </option>
                   <option value="topic"> Topic wise</option>
                </select>
                <?=isset($errors['type']) ? "<label class='error'>".$errors['type']."</label>": ''?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-md-4">
                <label>Title</label>
              </div>              
              <div class="col-sm-6 col-md-8 form-group ">
                <input type="text" name="title" class="form-control" placeholder="Notification Title">
                <?=isset($errors['title']) ? "<label class='error'>".$errors['title']."</label>": ''?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-md-4">
                <label>Url</label>
              </div>              
              <div class="col-sm-6 col-md-8 form-group ">
                <input type="text" name="url" class="form-control" placeholder="Notification Url">
                <?=isset($errors['url']) ? "<label class='error'>".$errors['url']."</label>": ''?>
              </div>
            </div>

            <div class="row form-group eventTypeRow">
              <div class="col-sm-6 col-md-4">
                <label>Description</label>
              </div>
              <div class="col-sm-6 col-md-8">
                <textarea placeholder="Notification description" name="description" id="description" class="form-control"></textarea>
                <?=isset($errors['description']) ? "<label class='error'>".$errors['description']."</label>": ''?>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-6 col-md-4"></div>
              <div class="col-sm-6 col-md-4">
                <button class="btn btn-primary" type="submit">Send Notification</button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
