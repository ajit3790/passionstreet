<?php
global $connection;
$error = array();
$fields = array('title','url', 'description', 'type');

if (empty($_POST) === false) {
    foreach ($fields as $key => $name) {
	if (isset($_POST[$name]) === false || $_POST[$name] === '') {
	   $error[$name] = 'Field '.$name.' can not be empty';
	}
    }
    if (empty($error) === true) {
        $title 		= $_POST['title'];
        $url 		= $_POST['url'];
        $description 	= $_POST['description'];
        $type 		= $_POST['type'];
        
        $apiKey = 'AAAANoWLoyc:APA91bGNuxm2NolehcBLxC0yrSvq4b7m2QcDzsm4dYOu4DOuBbzkcNH8A-bz03b9uc0-cfeCyKJ2H2-c5Z6n3gBPJdVxBgoNuMjuyeIDpxugvcBF3yl21XzJi8T82-caExcS1hKCYBsE';
	$headers = array('Authorization: key=' . $apiKey,'Content-Type: application/json');
	
	$topic = "/topics/passionst-topic";
	
	$data = array();
	$data['status'] = $title;
	$data['body'] 	= $description;
	$data['url'] 	= $url;
	$data['icon'] 	= 'favicon.ico';
	
	$post = array('to'=> $topic,'data'=> $data,'time_to_live' => 86400);
		
	$ch = curl_init();    
	curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
	curl_setopt($ch, CURLOPT_POST, true);     
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);    
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));  
	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		// echo 'GCM error: ' . curl_error($ch);
	}
	
	curl_close($ch);
	$response = json_decode($result,true);	
	
	if($response['message_id'] > 0){
	    echo "Notification has been sent successfully.";
	} else {
	    echo "Error occured while sending a notification";
	}	
	//print_array($response);
    }
}

$PSModData['errors'] = $error;
