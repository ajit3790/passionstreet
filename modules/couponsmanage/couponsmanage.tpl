<div class="row">
<div class="col-sm-4 col-md-4 col-xs-4">
    <ul class="list-group">
    <?php
    echo '<li class="list-group-item addnewcoupon">Add New Coupon</li>';
    foreach($couponlist as $couponId=>$coupon)
    {
        $timenow = date('Y-m-d h:i:s');
        if($coupon['couponStatus'] == 1 && ($coupon['couponCount'] == -1 || ($couponCount > -1 && $coupon['couponsRemaining' > 0])) && ($coupon['couponDateStart'] <= $timenow && $coupon['couponDateEnd']>=$timenow))
        $statusclass = 'alert-success';
        else
        $statusclass = 'alert-danger';
        echo '<li class="list-group-item editthiscoupon" data-couponid="'.$couponId.'">'.$coupon['couponName'].'<span class="badge pull-right '.$statusclass.'">&nbsp;</span></li>';
    }
    ?>
    </ul>
</div>
<div class="col-sm-8 col-md-8 col-xs-8">    
<div class='couponaddmode'>
<form class="form1 event-creation" action="<?=$_SERVER['PHP_BROWSER_URL']?>" method="post" enctype="multipart/form-data">            
    <div class="row">
        <div class="col-xs-12 text-center">
            Add a new coupon
            <br />
            <br />
        </div>
    </div>
    <div class="row form-group">
        <div class="col-xs-12 ">
          <input type="text" name="couponname" class="form-control" required placeholder="Coupon Name">
        </div>

     </div> 
     <div class="row form-group">
        <div class="col-xs-12">
             <select  name="couponapplieson" class="form-control" required="">
                <option selected value="">Coupon Applies On</option>
                <?php
                foreach($couponcanapplyon as $coupon)
                {
                    echo '<option value="'.$coupon[0].'">'.$coupon[1].'</option>';
                }
                ?>
            </select>
        </div>
     </div>
     <div class="row form-group">
        <div class="col-xs-12">
            <label class="control-label form-group">Coupon Type : Applies on per ticket basis</label>
            <div class="row tickettypebox">
                    <div class="tickettypeboxrow radio col-xs-4">
                        <label>
                            <input name="coupontype" required="" value="Percentage" type="radio">
                            Percentage (%)
                        </label>
                    </div> 
                    <div class="tickettypeboxrow radio col-xs-4">
                        <label>
                            <input name="coupontype" required="" value="Fixed" type="radio">
                            Fixed Value
                        </label>
                    </div>
                    <div class="tickettypeboxrow radio col-xs-4">
                        <input type="text" name="coupondiscountvalue" class="form-control" required placeholder="Coupon Value">
                    </div>
            </div>
        </div>
     </div>
     <div class="row form-group">
        <div class="col-xs-12">
            <label class="control-label form-group">No. of Coupons</label>
            <div class="row couponcountbox">
                <div class="couponcountboxrow radio col-xs-6">
                    <label>
                      <input name="couponcounttype" class="couponcounttype" required="" value="Unlimited" type="radio">
                      Unlimited
                    </label>
                </div> 
                <div class="couponcountboxrow radio col-xs-6">
                    <label>
                      <input name="couponcounttype" class="couponcounttype" required="" value="Limited" type="radio">
                      Limited
                    </label>
                </div>
                <div class="couponcountboxrow couponcountboxfield hide radio col-xs-6">
                      <input name="couponcount" id="couponcount"  class="form-control" value="" type="text" placeholder="Define coupon count">
                </div>
            </div>
        </div>
    </div>     
    <div class="row form-group">
        <div class="col-xs-12">
            <label class="control-label form-group">Coupon Validity</label>
          <div class="row">
              <div class=" col-xs-6">
              <div class="date">
                  <i class="fa fa-calendar icon-calendar"></i>
                  <input name="coupondatestart" id="coupondatestart"  title="Format = DD/MM/YYYY" value="<?=date('Y-m-d h:i:s')?>" placeholder="Start Date" data-type="datepicker" class="form-control date date-future datepicker" style="background-color: #fff"  type="text">
              </div>
          </div> 
         <div class="col-xs-6">
             <div class="date">
                  <i class="fa fa-calendar icon-calendar"></i>
                  <input name="coupondateend" id="coupondateend"  title="Format = DD/MM/YYYY" value="<?=date('Y-m-d h:i:s')?>" placeholder="End Date" data-type="datepicker" class="form-control date date-future datepicker" style="background-color: #fff"  type="text">
              </div>
         </div> 
          </div>
        </div>

     </div>     

     <div class="row form-group">
        <label class="control-label col-xs-4">Mmber List</label>
        <div class="radio col-xs-4">
          <label>
            <input name="couponparticipantstype" class="couponparticipantstype" required=""  value="all" type="radio">
            All
          </label>
        </div>
        <div class="radio col-xs-4">
          <label>
            <input name="couponparticipantstype" class="couponparticipantstype" required="" value="exclusive" type="radio">
            Exclusive
          </label>
        </div> 
        <div class="radio couponmemberlist col-xs-12 hide">
            <textarea class="form-control" id="couponmemberlistemaillist" name="couponparticipants" rows="2" cols="20" placeholder="You may enter multiple email id's coma seperated"></textarea>
        </div>
     </div>


      <div class="row form-group">
          <div class="col-sm-12 text-center">
              <input type="hidden" name="couponid" class="btn btn-primary" value="<?=$_GET['couponid']?>" />
              <input type="submit" name="submit" class="btn btn-primary" value="Generate Coupon" />
          </div>
      </div>
      <div class="row form-group">
        <div class="col-xs-12 ">
          <!--<input type="text" class="form-control" required placeholder="Coupon Code">-->
        </div>

     </div> 
</form>
</div>
<div class='couponeditmode hide'></div>
</div>
</div>
<script>
    $(document).ready(function(){
        $(".couponcounttype").on("select click",function(){
            if($(this).val() == 'Limited')
            {
                $(".couponcountboxrow").addClass('col-xs-4').removeClass('col-xs-6');
                $(".couponcountboxfield").removeClass('hide');
                $("#couponcount").attr('required', 'required');
            }
            else
            {
                $(".couponcountboxrow").addClass('col-xs-6').removeClass('col-xs-4');
                $(".couponcountboxfield").addClass('hide');
                $("#couponcount").removeAttr('required');
            }
        });
        $(".couponparticipantstype").on("select click",function(){
            if($(this).val() == 'all')
            {
                $(".couponmemberlist").addClass('hide');
                $("#couponmemberlistemaillist").removeAttr('required');
            }
            else
            {
                $(".couponmemberlist").removeClass('hide');
                $("#couponmemberlistemaillist").attr('required', 'required');
            }
        });
        $("#couponmemberlistemaillist").on('keyup',function(event){
            $temp = $(this).val();
            $temp = $temp.split(',').join('\n');
            $(this).val($temp);
        });
        $(".editthiscoupon").on('click',function(){
            $(".couponaddmode").addClass('hide');
            $(".couponeditmode").removeClass('hide');
        });
        $(".addnewcoupon").on('click',function(){
            $(".couponaddmode").removeClass('hide');
            $(".couponeditmode").addClass('hide');
        });
        $(".editthiscoupon").on('click',function(){
            $couponId = $(this).attr('data-couponid');
            $coupon = couponlist[$couponId];
            console.log($couponId);
            console.log($coupon);
            $editformhtml = '<div class="row">'+
                                '<div class="col-xs-12 text-center">Edit Coupon : '+$coupon['couponName']+'<br /><br /></div>'+
                            '</div>'+
                            '<div class="row"><form class="form1 event-creation" action="<?=$_SERVER['PHP_BROWSER_URL']?>" method="post">'+
                                '<div class="col-xs-6">Coupon Code</div><div class="col-xs-6">'+$coupon['couponCode']+'</div>'+
                                '<div class="col-xs-6">Type</div><div class="col-xs-6">'+$coupon['couponType']+'</div>'+
                                '<div class="col-xs-6">Discount</div><div class="col-xs-6">'+$coupon['couponDiscount']+'</div>'+
                                '<div class="col-xs-6">Count</div><div class="col-xs-6">'+(($coupon['couponCount']==-1)?'Unlimited':$coupon['couponCount'])+'</div>'+
                                '<div class="col-xs-6">Remaining</div><div class="col-xs-6">'+(($coupon['couponCount']==-1)?'Unlimited':$coupon['couponsRemaining'])+'</div>'+
                                '<div class="col-xs-6">Participation</div><div class="col-xs-6">'+(($coupon['couponParticipantsType'] == 'all')?'All':$coupon['couponParticipants'])+'</div>'+
                                '<div class="col-xs-6">DateStart</div><div class="col-xs-6"><input type="text" name="startdate" value="'+$coupon['couponDateStart']+'"></div>'+
                                '<div class="col-xs-6">DateEnd</div><div class="col-xs-6"><input type="text" name="enddate" value="'+$coupon['couponDateEnd']+'"></div>';
                        
            $editformhtml +='<div class="col-xs-6"><label><input type="radio" value="1" name="status" '+(($coupon['couponStatus']==1)?'checked':'')+'/>Enabled</label></div>';
            $editformhtml +='<div class="col-xs-6"><label><input type="radio" value="0" name="status" '+(($coupon['couponStatus']==0)?'checked':'')+'/>Disabled</label></div>';
            $editformhtml +='<div class="col-xs-6"></div><div class="col-xs-6"><input type="hidden" value="'+$coupon['couponId']+'" name="couponid"><input type="submit" class="btn" value="Update" name="updatecoupon"></div>';
            
            $editformhtml +='</form></div>';
            console.log($editformhtml);
            $(".couponeditmode").html($editformhtml);
        });
        //bootstrapdatetimepickermin_callback = parent.bootstrapdatetimepickermin_callback;
        //bootstrapdatetimepickermin_callback();
    });
</script>
<?php
$PSJsincludes['external2'][] = "js/bootstrapdatetimepicker.min.js";
?>