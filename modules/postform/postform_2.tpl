<script>window.FileAPI = { /* options */ };</script>
<script src="js/jquery.fileapi/FileAPI/FileAPI.min.js"></script>
<script src="js/jquery.fileapi/jquery.fileapi.min.js"></script>
<script src="js/jquery.fileapi/jcrop/jquery.Jcrop.min.js"></script>
<link href="js/jquery.fileapi/jcrop/jquery.Jcrop.min.css" rel="stylesheet" type="text/css"/>

<div id="userpic" class="userpic">
   <div class="js-preview userpic__preview"></div>
   <input name="filedata" type="file">
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Modal header</h3>
    </div>
    <div class="modal-body">
        <p>One fine body…</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button>
    </div>
</div>
<script>
$('#userpic').fileapi({
   url: 'http://rubaxa.org/FileAPI/server/ctrl.phpasd',
   accept: 'image/*',
   imageSize: { minWidth: 50, minHeight: 50 },
   elements: {
      //active: { show: '.js-upload', hide: '.js-browse' },
      preview: {
         el: '.js-preview',
         width: 200,
         height: 200
      }
      //progress: '.js-progress'
   }
});


</script>