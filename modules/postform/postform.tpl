<?php
if($_GET['layout'] == 'amp')
return;
?>
<div class="sidebar1">
<div class="tab-header mb0">
	<ul class="nav nav-pills full-width">
	    <li role="presentation" class="active"><a href="#activity" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Publish Activity</a></li>
	    <li role="presentation" class=""><a href="#url" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">URL Post</a></li>
        <?php
		if($showworkoutform){?>
		<li role="presentation" class=""><a href="#workout" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Workout Post</a></li>
	    <?php } ?>
	</ul>
</div>
<div class="bx-styl tab-content">
    <div id="activity" class="tab-pane fade active in" role="tabpanel">
        <form enctype="multipart/form-data" method="post" action="<?=$_SERVER['PHP_SELF_URL']?>" class="form1 submit-post">
        <div class="row postbox">
			<div class="col-sm-12 psr form-group">
				<textarea name="postsummary" placeholder="Write something" required="" class="form-control about"></textarea>
                                <span class="photo">Add photo <i class="fa fa-camera" aria-hidden="true"></i>
				<input class="showimgpreview" target=".imgpreviewbox" type="file" multiple/></span>
                        </div>
                        <div class="imgpreviewbox"></div>
                        
		</div>  
		<div class="row hide">
			<label class="control-label col-md-4 middle ">Give it a name</label>
			<div class="col-md-8 form-group">
				<input type="text" name="title" class="form-control">
			</div>
		</div>
	   
		<div class="row">
			<div class="col-sm-12 form-group">
				<div class="tagbox">
					<input class="inputtag1 typeahead passiontags" type="text" value="" placeholder="Tag posting keywords e.g streetfood, biking, running, wildlife, ..." />
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-sm-12 text-right">
                <button name="postsubmit" class="btn btn-primary btn-sm postsubmit">POST</button>
			</div>
		</div>	
			
	</form>
    </div>
    <div id="workout" class="tab-pane fade" role="tabpanel">
        <form enctype="multipart/form-data" method="post" action="<?=$_SERVER['PHP_SELF_URL']?>" class="form1 submit-post">
        <div class="row postbox">
			<div class="col-sm-12 psr form-group">
				<textarea name="postsummary" placeholder="Write something" required="" class="form-control about"></textarea>
                                <span class="photo">Add photo <i class="fa fa-camera" aria-hidden="true"></i>
				<input class="showimgpreview" target=".imgpreviewbox" type="file" multiple/></span>
                        </div>
                        <div class="imgpreviewbox"></div>
                        
		</div>                      
		
        <div class="post-log">
	   
	    <div class="row">
	    		
	    	   <div class="col-md-3 col-sm-3 col-xs-6 form-group">
	    	   <div class="row">
	    	        <label class="control-label col-xs-12 ">Workout Type</label>
	    	        <div class="col-xs-12">
			<?php
			if(count($workouttypes) == 1)
			{
				echo '<input type="text" class="form-control" required="" readonly="" value="'.$PSParams['PSCategories'][$workouttypes[0]]['name'].'">';
				echo '<input type="hidden" name="workouttype" class="workouttype form-control" required="" readonly="" value="'.$workouttypes[0].'">';
			}
			else
			{
			?>
			<select name="workouttype" class="workouttype form-control" required="">
				<option value=''>Select</option>
				<?php
				foreach($workouttypes as $passion)
				{
					
					echo '<option value="'.$passion.'">'.$PSParams['PSCategories'][$passion]['name'].'</option>';
				}
				?>
			</select>
			<?php
			}
			?>
			</div>
			</div>
		   </div>	
	    		
		   <div class="col-md-3 col-sm-3 col-xs-6 form-group ">
			 <div class="row">
					<label class="control-label col-xs-12 ">Date </label>
					<div class="col-xs-12">
						<i class="fa fa-calendar"></i>
                                                <input name="log[date]" required="" style="background-color:#fff" type="text" class="datepicker form-control date date-past"  placeholder="DD/MM/YYYY" readonly="">
					</div>  
				 </div>  
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6">
				<div class="row">
					<label class="control-label col-xs-12 ">Distance</label>
					<div class="col-xs-12 distance ">
						<input name="log[distance]" required="" id="logdistance" type="text" class="form-control logcfields" placeholder="">
					</div> 
					<!--<div class="col-md-4 col-sm-4"><span class="text"><input type="hidden" name="log[distanceunit]" value="KM" />KM</span></div>-->  
				 </div>
					  
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6 form-group ">
				<div class="row">
					<label class="control-label col-xs-12 ">Time taken</label>
					<div class="col-xs-12">
						<i class="fa fa-clock-o"></i>
						<input id="logtime" name="log[totaltimehh]" required="" style="background-color:#fff" data-type="hourrangeselector" data-options="0,100,2" type="text" class="datepicker form-control date date-past logcfields"  placeholder="HH:MM:SS" readonly="">
					</div>
				 </div> 
			</div>
	   </div>
	   <div class="row distroy-clear">
		   
			<div class="col-md-4 col-sm-4 col-xs-6">
				 <div class="row">
					<label class="control-label col-xs-12  ">Heart rate (BPM)</label>
					<div class="col-md-6 col-sm-6 col-xs-6 form-group">
						<input name="log[heartrateavg]" id="logheartrate" type="text" class="form-control logcfields logcfieldsoptional" placeholder="AVG">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 form-group">
						<input name="log[heartratemax]" type="text" class="form-control" placeholder="MAX">
					</div>  
				 </div>    
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="row">
					<label class="control-label col-xs-12 ">Altitude (MT)</label>
					<div class="col-md-6 col-sm-6 col-xs-6 date form-group">
						<input name="log[altitudelow]" type="text" class="form-control" placeholder="MIN">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 date form-group">
						<input name="log[altitudehigh]" type="text" class="form-control" placeholder="MAX">
					</div>  
				 </div>     
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6 ">                                 
				 <div class="row">
					<label class="control-label col-xs-12 ">Height &amp; Weight</label>
					
					<div class="col-xs-12 form-group update-box">
                                                <i class="fa fa-gear" onclick="javascript:customToggle('.update-popup')"></i>
						<input type="text" onfocus="javascript:customToggle('.update-popup')" id="userHeightWidth" readonly placeholder="0 CM, 0 KG" class="form-control" value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].' CM, ':''?><?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].' KG':''?>">
						<div class="update-popup hide">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="row">
										<label class="control-label col-xs-12 ">Height</label>
										<div class="col-xs-12 date">
                                                                                        <input value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].'':''?>" type="text" id="userheight" placeholder="In CM" class="form-control">
										</div>
										 
									 </div>     
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="row form-group">
										<label class="control-label col-xs-12 ">Weight</label>
										<div class="col-xs-12 date">
											<input value="<?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].'':''?>" type="text" id="userweight" placeholder="In KG" class="form-control">
										</div>
									 </div>   
								</div>
							</div>
							<div class="row">
								<input type='button' id='updateUserheightwidthbtn' class="btn btn-primary btn-sm" value="Update" />
							 </div> 
						</div>                                        
					</div>
				 </div>                                       
			</div>
	   </div>
	   
	   
	   <div class="row distroy-row">
			<div class="col-md-4 col-sm-4 col-xs-6">
				 <div class="row">
					<label class="control-label col-xs-12 ">Avg speed</label>
					<div class="col-xs-12 form-group">
						<i class="fa fa-refresh"></i>
						<input readonly="" id="avgspeed" type="text" name="log[avgspeed]" placeholder="0 KM/H" class="form-control">
					</div>
				 </div>    
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="row">
					<label class="control-label col-xs-12 ">Calories</label>
				   <div class="col-xs-12 form-group felt">
						<i class="fa fa-refresh"></i>
						<input readonly="" id="calories" type="text" name="log[calories]" placeholder="0 CALORIES" class="form-control">
					</div>                                    
				 </div>       
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="row">
					<label class="control-label col-xs-12  ">Felt</label>
					<div class="col-xs-12 form-group felt">
						<label class="icon" title="Awesome" data-placement="top" data-toggle="tooltip" data-original-title="smile"><input type="radio" name="log[felt]" value="awesome"><?=$GLOBALS['PSParams']['PSSmilies']['awesome']?>

						</label>
						<label class="icon" title="Good" data-placement="top" data-toggle="tooltip" data-original-title="excited"><input type="radio" name="log[felt]" value="good"><?=$GLOBALS['PSParams']['PSSmilies']['good']?>

						</label>
						
						<label class="icon" title="Ok" data-placement="top" data-toggle="tooltip" data-original-title="Ok"><input type="radio" name="log[felt]" value="okok"><?=$GLOBALS['PSParams']['PSSmilies']['okok']?>

						</label>
						<label class="icon" title="Awefull" data-placement="top" data-toggle="tooltip" data-original-title="sad"><input type="radio" name="log[felt]" value="awefull"><?=$GLOBALS['PSParams']['PSSmilies']['awefull']?>

						</label>
						
					</div>
				 </div>     
			</div>
	   </div>
	   </div>
	   <div class="row hide">
			<label class="control-label col-md-4 middle ">Give it a name</label>
			<div class="col-md-8 form-group">
				<input type="text" name="title" class="form-control">
			</div>
		</div>
	   
		<div class="row">
			<div class="col-sm-12 form-group">
				<div class="tagbox">
					<input class="inputtag1 typeahead workouttags" type="text" value="" placeholder="Tag posting keywords e.g cycling, biking, running, trekking, ..." />
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-sm-12 text-right">
                <button name="postsubmit" class="btn btn-primary btn-sm postsubmit">POST</button>
			</div>
		</div>
	</form>
    </div>
    <div id="url" class="tab-pane fade" role="tabpanel">
        <form enctype="multipart/form-data" method="post" action="<?=$_SERVER['PHP_SELF_URL']?>" class="form1 submit-post">
        <div class="row  postbox">
			<div class="col-md-12 form-group">
				<input type="url" name="url" id="fetchurl" onchange="fetchUrl()" class="form-control" placeholder="Add URL">
			</div>
			<div id="urlpreview" class="col-md-12"></div>
		</div>
        <div class="row">
			<div class="col-md-12 form-group">
				<textarea name="postsummary" placeholder="Write something" class="form-control about"></textarea>
                <textarea name="title" placeholder="Write something" id="urlpostsummary" class="form-control about hide"></textarea>
			</div>
		</div>
        <div class="row">
			<div class="col-sm-12 form-group">
				<div class="tagbox">
					<input class="inputtag1 typeahead passiontags" type="text" value="" placeholder="Tag posting keywords e.g streetfood, biking, running, wildlife, ..." />
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-sm-12 text-right">
                <button name="postsubmit" class="btn btn-primary btn-sm postsubmit">POST</button>
			</div>
		</div>
        </form>
    </div>
</div>
</div>
<script>


function fetchUrl(){
    fetchExtUrlMeta($("#fetchurl").val(),'fetchedUrl');
}
function fetchedUrl($metalist)
{
    if($metalist['videoembed']!== undefined)
    {
        $("#urlpreview").html('<div class="post external clearfix bx-styl"><div class="flexi-video unveil" vsrc="'+$metalist['videoembed']+'" src="<?=DEFAULT_IMG?>" data-src="'+$metalist['pageimage']+'" ></div><h3>'+$metalist['title']+'</h3><p class="clearfix">'+$metalist['description']+'</p><a class="hosturl"  rel="noindex,nofollow" href="'+$metalist['url']+'">'+$metalist['host']+'</a></div>');
        $("#urlpostsummary").html($("#urlpreview").html());
        prepare_youtube_frames();
        jqueryunveil_callback();
    }
    else{
        if($metalist['pageimage'])
        {
        var tmpImg = new Image();
        tmpImg.src=$metalist['pageimage'];
        $(tmpImg).one('load',function(){
            orgWidth = tmpImg.width;
            orgHeight = tmpImg.height;
            if(orgWidth<300 || ((orgWidth/orgHeight)<(1.5)))
            $imgtype = 'thumb';
            else
            $imgtype = 'fullwidth';
            $("#urlpreview").html('<div class="post external '+$imgtype+' clearfix bx-styl"><a href="'+$metalist['url']+'" target="_blank" rel="noindex,nofollow"><div class="img-frame"><img class="unveil" src="<?=DEFAULT_IMG?>" data-src="'+$metalist['pageimage']+'" /></div><h3>'+$metalist['title']+'</h3><p class="clearfix">'+$metalist['description']+'</p></a><a class="hosturl"  rel="noindex,nofollow" href="'+$metalist['url']+'">'+$metalist['host']+'</a></div>');
            $("#urlpostsummary").html($("#urlpreview").html());
            jqueryunveil_callback();
	});
        }
        else
        {
            $("#urlpreview").html('<div class="post external fullwidth clearfix bx-styl"><a href="'+$metalist['url']+'" target="_blank" rel="noindex,nofollow"><h3>'+$metalist['title']+'</h3><p class="clearfix">'+$metalist['description']+'</p></a><a class="hosturl"  rel="noindex,nofollow" href="'+$metalist['url']+'">'+$metalist['host']+'</a></div>');
            $("#urlpostsummary").html($("#urlpreview").html());
        }
        
    }    
}
$(document).ready(function(){
    $('.postform .nav-pills').click(function(){
            //$('.typeahead').tagsinput('removeAll');
	    //$('.typeaheadwc').tagsinput('removeAll');
    });
    
    $('.postimg').each(function(){
        $(this).filestyle({
            icon : false,
            buttonText : ' Browse',
            'placeholder': 'Upload photo'
        });
    });

    if(!fndefined('tooltip')){
        $temp2['tooltip'] = setInterval(function(){
            if(fndefined('tooltip')){
                clearInterval($temp2['tooltip']);
                $('[data-toggle="tooltip"]').tooltip();
           };
        },300);  
    }
    else{
		$('[data-toggle="tooltip"]').tooltip();
    }
            

    $(".categoryworkout").change(function(){
        $temp = $(this);
        $tempParent = $temp.parent();
        $temp.parents("form").find(".posttypeworkout").html('');
        $temp.parents("form").find(".posttypeworkout").append("<option> --Activity Type-- </option>");
        if(psworkout)
        {
            $.each(psworkout[$temp.val()],function(i,v){
                $temp.parents("form").find(".posttypeworkout").append("<option value='subcategory###"+i+"'>"+v['name']+"</option>");
            });
        }
    });

    $(".category").change(function(){
        $temp = $(this);
        $tempParent = $temp.parent();
        //$.get(root_path+"PSAjax.php",{"category":$temp.val() , "type":"categorysublist-posttype"},function(data){
        //});
            $temp.parents("form").find(".posttype").html('');
            $temp.parents("form").find(".posttype").append("<option value='posttype###random'>Random</option>");
            if(passionSubcategories)
            {
                $.each(passionSubcategories[$temp.val()],function(i,v){
                    $temp.parents("form").find(".posttype").append("<option value='subcategory###"+i+"'>"+v['name']+"</option>");
                });
            }
            
            
            if(passionPosttypes)
            {
                $.each(passionPosttypes[$temp.val()],function(i,v){
                    $temp.parents("form").find(".posttype").append("<option value='posttype###"+i+"'>"+v['name']+"</option>");
                });
            }
    });
    $(".submit-post").on("change",".posttype",function(){
        if($(this).val() == 'ridelog')
        $(".post-log").removeClass('hide');
        else
        $(".post-log").addClass('hide');
            
    });
    $passionType = '';
    if(q['pagetype'] == 'passion'){
        $passionType = q['type'];
    }else if(q['pagetype'] == 'eventDetail'){
        $passionType = passiontype;
    }
    if($passionType !== ''){
        $("#category").children().filter(function() {
            return $(this).val() == $passionType;
        }).prop('selected', true).parents("#category").change();
        //$('#category option:not(:selected)').prop('disabled', true);
        $('#category option:not(:selected)').remove();
    }
    
});
$("#updateUserheightwidthbtn").click(function(){
    $height = $("#userheight").val();
    $weight = $("#userweight").val();
    $.post(root_path+"PSAjax.php?type=updateUserheightwidth",{"height":$height ,"weight":$weight},function(data){
        $("#userHeightWidth").val($height+' CM, '+$weight+' KG');
        customToggle('.update-popup');
        calculateRidelogData();
    });
        
})
$(document).ready(function(){
    $(".logcfields").blur(function(){
        $temp = 1;
        $(".logcfields").each(function(i,v){
            if(!($(v).hasClass("logcfieldsoptional")) && $(v).val().length < 1)
            $temp = 0;
        });
        if($temp == 1)
        {
            if($("#userHeightWidth").val().length == 0)
            customToggle('.update-popup');
            else
            calculateRidelogData();
        }
    });
    
});
function calculateRidelogData(){
    $params = {};
    $params['time'] = $("#logtime").val();
    $params['distance'] = $("#logdistance").val();
    $params['heartRate'] = $("#logheartrate").val();
    $params['activitytype'] = $(".workouttype").val();
    $.post(root_path+"PSAjax.php?type=calculateRidelogData",$params,function(data){
        $x = $.parseJSON(data);
        $("#calories").val($x['list']['calories']);
        $("#avgspeed").val($x['list']['speed']);
    });
}

$(document).ready(function(){
	/*
    $('.postsubmit').on('click',function(event){
        event.preventDefault();
	$currentTab = $(this).parents('.tab-pane');
        var $tagss = $currentTab.find('.typeahead').tagsinput('items');
	if($currentTab.find(".workouttype").length > 0)
	{
		$tempfields = ['.workouttype','input[required]'];
		$temperror = 0;
		$.each($tempfields,function(i,v){
			$tempfield = $currentTab.find(v).each(function(){
				if($(this).val().length == 0)
				{
				showToolTipMsg($(this),"Required field");
				$temperror = 1;
				}
			});
			
		});
		if($temperror)
		return false;
	}
	if($tagss.length == 0)
	{
		showToolTipMsg($currentTab.find('.tt-input'),"Please add relevant tags!");
		$currentTab.find('.tt-input').focus();
		return;
	}
	var $thisform = $currentTab.find('.submit-post');
	console.log($tagss);
	$.each($tagss,function(i,v){
            //$.each(v1,function(i,v){
                if('value' in v)
                $thisform.append("<input type='hidden' name='tags["+v['value']+"]' value='"+v['passion']+"'>");
            //}); 
        });
	$thisform.submit();
   });
	*/
   $('.postsubmit').on('click',function(event){
        event.preventDefault();
	$currentTab = $(this).parents('.tab-pane');
        var $tagsList = getAddedTags($currentTab);
	var $tagss = $tagsList['list'];
	var $currentPostingType = $currentTab.attr('id');
	if($currentPostingType == 'workout')
	{
		if($currentTab.find(".workouttype").length > 0)
		{
			$tempfields = ['.workouttype','input[required]'];
			$temperror = 0;
			$.each($tempfields,function(i,v){
				$tempfield = $currentTab.find(v).each(function(){
					if($(this).val().length == 0)
					{
					showToolTipMsg($(this),"Required field");
					$temperror = 1;
					}
				});
				
			});
			if($temperror)
			return false;
		}
	}
	else if($currentPostingType == 'activity')
	{
		$temp = $currentTab.find('.about');
		if($.trim($temp.val()).length == 0 && $currentTab.find('.imgpreviewbox .img-frame img').length == 0)
		{
		showToolTipMsg($temp,"Please fill this");
		return false;
		}
	}
	else if($currentPostingType == 'url')
	{
		$temp = $currentTab.find('#urlpreview .external');
		if($.trim($temp.text()).length == 0)
		{
		showToolTipMsg($currentTab.find('#fetchurl'),"Please check this URL . It seems URL was not provided or proper information was not returned from the page");
		return false;
		}
	}
	if(Object.keys($tagss).length == 0)
	{
	showToolTipMsg($currentTab.find('.as-input'),"Please add a relevant tag");
	return false;
	}
	
	var $thisform = $currentTab.find('.submit-post');
	$.each($tagss,function(i,v){
            //$.each(v1,function(i,v){
                if('text' in v)
                $thisform.append("<input type='hidden' name='tags["+v['text']+"]' value='"+v['passion']+"'>");
            //}); 
        });
	$thisform.submit();
   });
		
  //delayfunction2(['bootstrap-tagsinput.js','typeahead.bundle.min.js'],'tagimplementation','',1000);
  delayfunction2(['jquery.autoSuggest.js'],'tagimplementation2','',1000);
});
function tagimplementation2(){
	var elt = $('.passiontags');
	var data = passiontags;
	elt.autoSuggest(data, {selectedItemProp: "text", searchObjProps: "text",usePlaceholder:true,startText:elt.attr('placeholder'),neverSubmit:true,selectionAdded:function(elem){
		//console.log(elem.data('value'));
	}});
	
	elt = $('.workouttags');
	data = workouttags;
	elt.autoSuggest(data, {selectedItemProp: "text", searchObjProps: "text",usePlaceholder:true,startText:elt.attr('placeholder'),neverSubmit:true,selectionAdded:function(elem){
		//console.log(elem.data('value'));
	}});
}
/*
function tagimplementation(){
	var tagengn = new Bloodhound({
	    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
	    queryTokenizer: Bloodhound.tokenizers.whitespace,
	    //prefetch: 'assets/cities.json',
	    local:  passiontags
	});
	tagengn.initialize();
	  
	var elt = $('.typeahead');
	$x = {
	    itemValue: 'value',
	    itemText: 'text',
	    typeaheadjs: {
	      name: 'tagengn',
	      displayKey: 'text',
	      source: tagengn.ttAdapter()
	    }
	};
	elt.tagsinput($x);
}*/

	


</script>
<?php
$PSJsincludes['external'][] = "js/stickykit.js";
$PSJsincludes['external'][] = "js/jquery.autoSuggest.js";
$PSJsincludes['external'][] = "js/jquery.Jcrop.js";
$PSJsincludes['external'][] = "js/bootstrap-filestyle.min.js";
$PSJsincludes['external'][] = "js/bootstrapdatetimepicker.min.js";
$PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
$PSCssincludes['external'][] = "css/jquery.autoSuggest.css";
//$PSCssincludes['external'][] = "css/jquery.Jcrop.css";
//$PSJsincludes['external'][] = "js/typeahead.bundle.min.js";
//$PSJsincludes['external'][] = "js/bootstrap-tagsinput.js";
//$PSJsincludes['external'][] = "js/jquery.datetimepicker.js";
//$PSCssincludes['external'][] = "css/jquery.datetimepicker.css";
//$PSCssincludes['external'][] = "css/bootstrap-tagsinput.css";
?>