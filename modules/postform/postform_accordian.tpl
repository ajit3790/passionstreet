<a name="addContributionForm"></a>
<div class="sidebar">
    <div class="widget bx-styl">
        <h2 class="hstyl_1 bx sm">Post your activities</h2>
	<form enctype="multipart/form-data" method="post" action="<?=$_SERVER['PHP_SELF_URL']?>" class="form1 submit-post">
            <div class="row ">
                <div class="col-md-6 col-sm-6 form-group ">
                   <div class="dropdown accordian">
                    <span class="form-control form-dropdown accordian-select">
                      <span class='accordian-selected-text'>Passion Category</span>
                      <span class="caret"></span>
                      <input type='hidden' class='accordian-value-layer1' id='category' name='category' />
                      <input type='hidden' class='accordian-value-layer2' id='subcategory' name='subcategory'/>
                    </span>
                    <div class="dropdown-menu accordian-dropdown">
                         <div id="accordion" aria-multiselectable="true" role="tablist" class="panel-group" style="position:relative">
                            <?php
                                if($_GET['pagetype']== 'passion')
                                $selectedPassion = $_GET['type'];
                                else if($_GET['pagetype']== 'eventDetail')
                                $selectedPassion = $_GET['passiontype'];
                                else
                                $selectedPassion = '';
$c = 0;
                                foreach($PSParams['PSCategories'] as $key=>$value)
                                {
$c++;
                                    if(!empty($selectedPassion) && ($key!=$selectedPassion))
                                    continue;
                                    ?>
                                    <div class="panel panel-default">
                                    <div id="<?=$key?>" val="<?=$key?>" role="tab" class="panel-heading">                                          
                                        <a val="<?=$key?>" class="panel-heading-a"aria-controls="<?=$key?>_collapse" aria-expanded="true" href="#<?=$key?>_collapse" data-parent="#accordion" data-toggle="collapse" role="button"><?=$value['name']?></a>
                                        
                                    </div>
                                    <div aria-labelledby="<?=$key?>" role="tabpanel" class="panel-collapse collapse order<?=$c?>" id="<?=$key?>_collapse">
                                        <div class="panel-body">
                                            <ul>
                                            <?php
                                            foreach($PSParams['PSSubCategoriesByCategory'][$key] as $key2=>$value2)
                                            {
                                                ?>
                                                <li val='<?=$key2?>'><?=$value2['name']?></li>
                                                <?php
                                            }
                                            ?>
                                            </ul>
                                        </div>
                                    </div>
                                    </div>
                                    <?php
                                }
                              ?>
                          </div>   
                    </div>
                  </div>
                </div>
                <!--<div class="row ">
                    <div class="col-md-12 col-sm-6 form-group ">
			<select name="category" id="category" class="form-control" required="">
				<option value=''> -- Select Passion -- </option>
				<?php
				foreach($PSParams['PSCategories'] as $PSCategorykey=>$PSCategory)
				{
					echo "<option value='".$PSCategorykey."'>".$PSCategory['name']."</option>";
				}
				?>
			</select>
		  </div>
		  <div class="col-md-6 col-sm-6 form-group hide">
			<select name="subcategory" id="subcategory"  class="form-control" required="">
			</select>
		  </div>
                </div>-->  
		
		   <div class="col-md-6 col-sm-6 form-group ">
			<select name="posttype" id="posttype"  class="form-control" required="">
				<option value=""> -- Posting Type -- </option>
			</select>
			</div>
		</div>
	   
		<div class="row">
			<div class="col-sm-12 form-group">
				<textarea name="postsummary" placeholder="Write something" required="" class="form-control about"></textarea>
			</div>
		</div>                      
		
           <div class="post-log hide">
	   <div class="row">
		   <div class="col-md-4 col-sm-4 form-group ">
			 <div class="row">
					<label class="control-label col-md-12 ">Ride date </label>
					<div class="col-md-12 date">
						<i class="fa fa-calendar"></i>
						<input name="log[date]" style="font-size: 10px" type="text" class="form-control date date-default"  placeholder="DD/MM/YYYY">
					</div>  
				 </div>  
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="row">
					<label class="control-label col-md-12  ">Distance</label>
					<div class="col-md-8 col-sm-8 date">
						<input name="log[distance]" id="logdistance" type="text" class="form-control logcfields" placeholder="">
					</div> 
					<div class="col-md-4 col-sm-4"><span class="text"><input type="hidden" name="log[distanceunit]" value="KM" />KM</span></div>  
				 </div>
					  
			</div>
			<div class="col-md-4 col-sm-4 form-group ">
				<div class="row">
					<label class="control-label col-md-12 ">Time taken</label>
					<div class="col-md-12 col-sm-12 time">
						<input name="log[totaltimehh]" id="logtime" type="text" class="form-control logcfields" placeholder="HH : MM">
					</div>  
				 </div> 
			</div>
	   </div>
	   <div class="row">
		   
			<div class="col-md-4 col-sm-4">
				 <div class="row">
					<label class="control-label col-md-12  ">Heart rate (BPM)</label>
					<div class="col-md-6 col-sm-6 form-group">
						<input name="log[heartrateavg]" id="logheartrate" type="text" class="form-control logcfields logcfieldsoptional" placeholder="Avg">
					</div>
					<div class="col-md-6 col-sm-6 form-group">
						<input name="log[heartratemax]" type="text" class="form-control" placeholder="Max">
					</div>  
				 </div>    
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="row">
					<label class="control-label col-md-12 ">Altitude (MT)</label>
					<div class="col-md-6 col-sm-6 date">
						<input name="log[altitudelow]" type="text" class="form-control" placeholder="Low">
					</div>
					<div class="col-md-6 col-sm-6 date">
						<input name="log[altitudehigh]" type="text" class="form-control" placeholder="High">
					</div>  
				 </div>     
			</div>
			<div class="col-md-4 col-sm-4">                                 
				 <div class="row">
					<label class="control-label col-md-12 ">Height &amp; Weight</label>
					
					<div class="col-md-12 form-group update-box">
                                                <i class="fa fa-gear" onclick="javascript:customToggle('.update-popup')"></i>
						<input type="text" onfocus="javascript:customToggle('.update-popup')" id="userHeightWidth" readonly placeholder="0 CM, 0 KG" class="form-control" value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].' CM, ':''?><?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].' KG':''?>">
						<div class="update-popup hide">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="row">
										<label class="control-label col-md-12 ">Height</label>
										<div class="col-md-12 date">
                                                                                        <input value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].'':''?>" type="text" id="userheight" placeholder="In CM" class="form-control">
										</div>
										 
									 </div>     
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="row form-group">
										<label class="control-label col-md-12 ">Weight</label>
										<div class="col-md-12 date">
											<input value="<?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].'':''?>" type="text" id="userweight" placeholder="In KG" class="form-control">
										</div>
									 </div>   
								</div>
							</div>
							<div class="row">
								<input type='button' id='updateUserheightwidthbtn' class="btn btn-primary btn-sm" value="Update" />
							 </div> 
						</div>                                        
					</div>
				 </div>                                       
			</div>
	   </div>
	   
	   
	   <div class="row">
			<div class="col-md-4 col-sm-4">
				 <div class="row">
					<label class="control-label col-md-12 ">Avg speed</label>
					<div class="col-md-12 form-group">
						<i class="fa fa-refresh"></i>
						<input readonly="" id="avgspeed" type="text" name="log[avgspeed]" placeholder="" class="form-control">
					</div>
				 </div>    
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="row">
					<label class="control-label col-md-12 ">Calories</label>
				   <div class="col-md-12 form-group">
						<i class="fa fa-refresh"></i>
						<input readonly="" id="calories" type="text" name="log[calories]" placeholder="" class="form-control">
					</div>                                    
				 </div>       
			</div>
			<div class="col-md-4 col-sm-4">
                            <div class="row">
                                <label class="control-label col-md-12  ">Felt</label>
                                <div class="col-md-12 col-sm-6 form-group felt">
                                    <label class="icon" title="Awesome" data-placement="top" data-toggle="tooltip" data-original-title="smile"><input type="radio" name="log[felt]" value="awesome"><img alt="Awesome" src="images/smile.png">

                                    </label>
                                    <label class="icon" title="Good" data-placement="top" data-toggle="tooltip" data-original-title="excited"><input type="radio" name="log[felt]" value="good"><img alt="Good" src="images/glad.png">

                                    </label>
                                    <label class="icon" title="Awefull" data-placement="top" data-toggle="tooltip" data-original-title="sad"><input type="radio" name="log[felt]" value="awefull"><img alt="Awefull" src="images/sad.png">

                                    </label>
                                </div>
                             </div>     
                        </div>
	   </div>
	   </div>
	   <div class=" row">
			<div class="col-sm-12 form-group">
				<input type="file" name="postimages[]" croptype="post" id="upload2" class="multifilecrop1" multiple>
			</div>
		</div>
	   <div class="row">
			<label class="control-label col-md-4 middle ">Give it a name</label>
			<div class="col-md-8 form-group">
				<input type="text" name="title" class="form-control">
			</div>
		</div>
	   
		<div class="row">
			<div class="col-sm-12 text-right">
                            <button name="postsubmit" class="btn btn-primary btn-sm">POST</button>
			</div>
		</div>
	</form>
</div>
</div>
<script>

$(document).ready(function(){
    $('#upload2').filestyle({
        icon : false,
        buttonText : ' Browse',
        'placeholder': 'Upload photo'
    });

    $(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
    
   $(function () {
    $('#accordion').on('shown.bs.collapse', function (e) {
        var offset = $('.panel.panel-default > .panel-collapse.in').position();
        if(offset) {
           $('.dropdown-menu').scrollTop(offset.top - 30);
        }
    }); 
});

    $("#category").change(function(){
        $("#subcategory").parent().addClass("hide");
        $("#subcategory").html('');
        $("#posttype").html('');
        $temp = $(this);
        $tempParent = $temp.parent();
        //$.get(root_path+"PSAjax.php",{"category":$temp.val() , "type":"categorysublist-posttype"},function(data){
        //});
        if(passionSubcategories)
            {
                $tempParent.removeClass("col-md-12");
                $tempParent.addClass("col-md-6");
                $("#subcategory").append("<option value=''> -- Select Sub Category -- </option>");
                $.each(passionSubcategories[$temp.val()],function(i,v){
                    $("#subcategory").append("<option value='"+i+"'>"+v['name']+"</option>");
                });
                $("#subcategory").parent().removeClass("hide");
            }
            else
            {
                $tempParent.removeClass("col-md-6");
                $tempParent.addClass("col-md-12");
            }
            
            if(passionPosttypes)
            {
                $("#posttype").append("<option value=''> -- Posting Type -- </option>");
                $("#posttype").append("<option value='Random'>Random</option>");
                $.each(passionPosttypes[$temp.val()],function(i,v){
                    $("#posttype").append("<option value='"+i+"'>"+v['name']+"</option>");
                });
            }
    });
    $(".submit-post").on("change","#posttype",function(){
        if($(this).val() == 'ridelog')
        $(".post-log").removeClass('hide');
        else
        $(".post-log").addClass('hide');
            
    });
    $passionType = '';
    if(q['pagetype'] == 'passion'){
        $passionType = q['type'];
    }else if(q['pagetype'] == 'eventDetail'){
        $passionType = passiontype;
    }
    if($passionType !== ''){
        $("#category").children().filter(function() {
            return $(this).val() == $passionType;
        }).prop('selected', true).parents("#category").change();
        //$('#category option:not(:selected)').prop('disabled', true);
        $('#category option:not(:selected)').remove();
    }
    
    $(".accordian-select").click(function(){
       customToggle($(this).parents(".accordian").find('.accordian-dropdown')); 
    });
    $(".accordian").on('click','.panel-heading,.panel-heading-a',function(){
        $temp = $(this).parents(".accordian");
        $temp.find(".accordian-value-layer1").val($(this).attr('val'));
        $("#category").change();
    });
    $(".accordian").on('click','li',function(){
        $temp = $(this).parents(".accordian");
        $temp.find(".accordian-value-layer2").val($(this).attr('val'));
        $temp.find(".accordian-selected-text").text($(this).html());
        customToggle($temp.find(".accordian-dropdown"));
        $("#category").change();
    });
    
});
$("#updateUserheightwidthbtn").click(function(){
    $height = $("#userheight").val();
    $weight = $("#userweight").val();
    $.post(root_path+"PSAjax.php?type=updateUserheightwidth",{"height":$height ,"weight":$weight},function(data){
        $("#userHeightWidth").val($height+' CM, '+$weight+' KG');
        customToggle('.update-popup');
        calculateRidelogData();
    });
        
})
$(document).ready(function(){
    $(".logcfields").blur(function(){
        $temp = 1;
        $(".logcfields").each(function(i,v){
            if(!($(v).hasClass("logcfieldsoptional")) && $(v).val().length < 1)
            $temp = 0;
        });
        if($temp == 1)
        {
            if($("#userHeightWidth").val().length == 0)
            customToggle('.update-popup');
            else
            calculateRidelogData();
        }
    });
    
});
function calculateRidelogData(){
    $params = {};
    $params['time'] = $("#logtime").val();
    $params['distance'] = $("#logdistance").val();
    $params['heartRate'] = $("#logheartrate").val();
    $.post(root_path+"PSAjax.php?type=calculateRidelogData",$params,function(data){
        $x = $.parseJSON(data);
        $("#calories").val($x['list']['calories']);
        $("#avgspeed").val($x['list']['speed']);
    });
}

</script>
<?php
$PSJsincludes['external'][] = "js/stickykit.js";
$PSJsincludes['external'][] = "js/jquery.datetimepicker.js";
$PSCssincludes['external'][] = "css/jquery.datetimepicker.css";
$PSJsincludes['external'][] = "js/jquery.Jcrop.js";
$PSJsincludes['internal'][] = "js/bootstrap-filestyle.min.js";
$PSCssincludes['external'][] = "css/jquery.Jcrop.css";
?>