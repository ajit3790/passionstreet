<?php
    $loggedInUserId = $_SESSION['user']['id'];
    $enableAddMember = false;
    $isGroupDetailAvailable = (isset($group['createdBy']) === true) ? true : false;
    
    if (isset($groupUserPermission['role']) === true && (($group['invitePermission'] == 3 && in_array($groupUserPermission['role'], array(1,2))) || in_array($group['invitePermission'], array(1,2)) ))  {
        $enableAddMember = true;
    }
?>
<div class="mb-small bx-styl"> 
    <div class="row">
        <div class="col-md-12">
            <h6>Description</h6> 
            <div class="group-desc">
            <?php
                if (isset($group['description']) === true ) {
                    echo $group['description'];
                } else {
                    echo '<p>Tell people what this group is about.</p> ';
                    echo '<i class="fa fa-pencil"></i>';
                }
            ?>
            </div>
        </div>
    </div>    
    
    <div class="row">
        <div class="col-md-12 members">
        <?php
            $countMemberUI = "<a class='more-lnk pull-right' href='community/$groupId/members'>";
            if ($totalMembers < 2) {
                $countMemberUI .= $totalMembers.' Member </a>';
            } else {
                $countMemberUI .= $totalMembers.' Members </a>';
            }

            $memberUI   = $countMemberUI.'<h6>Members Joined</h6><div class="inner clearfix follow_lst"><ul>';
            foreach ($members as $key => $member) {
                $profileId  = $member['profileId'];
                $userName   = $member['fname'].' '.$member['lname'];
                $profileUrl = ROOT_PATH.'profile/'.strtolower(str_replace(' ', '-', $userName)).'/'.$profileId;

                $defaultImg = ROOT_PATH.'profiledp';
                $profileImg = ROOT_PATH.'files/'.$member['userdp'];
                
                $memberUI  .= "<li>";
                $memberUI  .= "<a class='hovercard' id='$profileId' htype='profile' hentity='$profileId' href='$profileUrl'>";
                $memberUI  .= "<figure>";
                $memberUI  .= "<img class='unveil psimage' src='$defaultImg' data-src='$profileImg'>";
                $memberUI  .= "<figcaption>$userName</figcaption>";
                $memberUI  .= "<span class=''></span>";
                $memberUI  .= "<figure>";
                $memberUI  .= "</a>";
                $memberUI  .= "</li>";
            }
            $memberUI .= '</ul></div></div>';
            echo $memberUI;
        ?>
    </div>
    <?php
        if ($enableAddMember === true) {
    ?>
    <form name="addGroupMember" action="" method="POST">
        <div class="row">
            <div class="col-md-12">
                <h6>Invite Member</h6> 
                <div class="input-group add-member-form">
                    <span class="input-group-addon addGroupMember"><span class="glyphicon glyphicon-plus"></span></span>
                    <input class="form-control" placeholder="Invite Member" name="invite" type="text" id="invite-member">
                </div>
            </div>
        </div>
    </form>
    <?php
        }
    ?>
    
    <div class="row invtd-mbrs">
        <div class="col-md-12">
            <?php
                $totalInvited = $totalInvitedMembers;
                if (empty($invitedMembers) === false) {
                    $otherMembers = $totalInvited-3;
                    if ($totalInvited > 3) {
                        echo "<a class='more-lnk pull-right more-invites' href='javascript::void(0);'><span class='invited-member-count'>+$otherMembers</span> more</a>";
                    }
                    echo "<h6>Invited Members</h6>";
                    echo '<ul class="invited-list">';
                    foreach ($invitedMembers as $key => $invitedMember) {
                        $invitedName = $invitedMember['fname'].' '. $invitedMember['lname'];
                        if ($invitedMember['fname'] == '') {
                           $invitedName = $invitedMember['invitedToEmail'];
                        }
                        echo '<li><span class="glyphicon glyphicon-envelope"></span>';

                        echo $invitedName.'</li>';
                        if ($key == 4) {
                            break;
                        }
                    }
                    echo '</ul>';
                } else {
                    echo '<ul class="invited-list"></ul>';
                }
            ?>
        </div>
    </div>
</div>

<?php
    if (empty($suggestedGroups) === false) {    
        $suggestedGroupCount = count($suggestedGroups);
        $carouselClass = '';
        if ($suggestedGroupCount > 1) {
            $carouselClass = 'eventcarousel';
        }
?>
<div class="widget-heading mb-small">  
    <a class="more-lnk pull-right" href="<?=ROOT_PATH.'communities/discover'?>">View all <i class="fa fa-angle-double-right"></i>
    </a>
    <h2 class="hstyl_1 sm">Suggested Community</h2>
</div>

<div class="mb-small bx-styl">  
    <div class="row">
        <div class="col-md-12 card-styl_1 sm carousel-wrapper">
            <!-- Uncomment if more than one group -->
            <!-- <i class="fa fa-angle-left"></i>
            <i class="fa fa-angle-right"></i> -->
            <div class="inner">
                <ul class="single <?=$carouselClass?>">
                    <?php
                        foreach ($suggestedGroups as $key => $list) {
                        $groupPhoto = ROOT_PATH.'files/'.$list['photo'];
                    ?>
                    <li class="card">
                        <figure>
                            <a href="<?=ROOT_PATH.'community/'.$list['groupId']?>">
                                <img alt="" src="<?=DEFAULT_IMG_HORIZONTAL?>" class="unveil" data-src="<?=$groupPhoto?>">
                            </a>
                        </figure>
                        <figcaption class='clearfix sg-gp'>                        
                            <a href="<?=ROOT_PATH.'community/'.$list['groupId']?>" class='title'><?php echo $list['name'];?></a>
                            <div class="join-sg-gp">
                                <button class="btn btn-primary join-group" data-id="<?php echo $list['groupId'];?>">Be A Member</button>
                            </div>
                            <div class="sg-members">
                                <span>Members - <?php if ($list['totalMembers'] > 1) { echo $list['totalMembers']. ' Members';} else { echo $list['totalMembers'].' Member';} ?></span> 
                            </div>
                            <div class="sg-cat">
                                <span>Category - <?php echo $list['category'];?></span> 
                            </div>
                        </figcaption>
                    </li>
                    <?php
                        }
                    ?>
                </ul>

            </div>
        </div>
    </div>    
</div>
<?php
    }
?>

<?php
    if($enableAddMember === true) {
?>
<script type="text/javascript">
    var groupId = "<?php echo $groupId;?>";
    var loggedInUserId = "<?php echo $loggedInUserId;?>";
    var invitedMembers = '<?php echo $totalInvited;?>';

    // Invite member
    $("#invite-member").autocomplete({
        source: function (request, response)  {
          $.ajax({
            url: root_path+"PSAutocomplete.php?type=autocomplete",
            dataType: "json",
            data:{
              term: request.term
            },
            success: function (data) {
              response(data);
            }
          });
        },
        minLength: 2,
        select: function (event, ui) {
            var optedId = ui.item ? ui.item.id : 0;
            if (optedId ==  0) {
                if( !isValidEmailAddress(ui.item.email)) {
                  showToolTipMsg($('#invite-member'),"Please enter valid email.");
                  return false;  
                } else {
                    var activitySuccess = false;
                    $.ajax({
                        url: root_path+"AjaxGroup.php",
                        method:"POST",
                        data: "userId="+loggedInUserId+"&memberId="+optedId+"&requestType=inviteMember&groupId="+groupId+'&email='+ui.item.email,
                        success: function (data) {
                          //response(data);
                            var res = $.parseJSON(data);
                            if (res.responseCode == '2006') {
                                if (invitedMembers == 0) {
                                    $('.invited-list').before('<h6>Invited Members</h6>');
                                }
                                var member   = '<li><span class="glyphicon glyphicon-envelope"></span>';
                                    member  += '<label>'+ui.item.email+'</label></li>';
                                $('.invited-list').append(member);
                                
                                invitedMembers++;
                                $('.invited-member-count').text(invitedMembers);
                                if (invitedMembers > 0) {
                                    $('.more-invites').show();
                                }
                            } else if (res.responseCode == '5001') {
                                showToolTipMsg($('#invite-member'), res.result);
                            }
                        }
                    });
                }
            } else {

                $.ajax({
                    url: root_path+"AjaxGroup.php",
                    method:"POST",
                    data: "userId="+loggedInUserId+"&memberId="+optedId+"&requestType=inviteMember&groupId="+groupId+'&emid='+btoa(ui.item.email),
                    success: function (data) {
                      //response(data);
                    }
                });
                
                if (invitedMembers == 0) {
                    $('.invited-list').before('<h6>Invited Members</h6>');
                }
                invitedMembers++;

                var addedMemberName = ui.item.name;

                var inviteId = $("input[name='invite']").val();
                var member   = '<li><span class="glyphicon glyphicon-envelope"></span>';
                    member  += addedMemberName+'</li>';
                $('.invited-list').append(member);
            }
        }    
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li></li>" )
        .data( "item.autocomplete", item)
        .append( "<a class='myclass' customattribute='" + item.name + "'> <img src='"+root_path+"profiledp' width='40px' height='40px'> " + item.name + "</a>" )
        .appendTo(ul);
    };

    function isValidEmailAddress(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( email );
    };

</script>
<?php
    }
?>