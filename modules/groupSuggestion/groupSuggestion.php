<?php
global $groupId;
global $groupDetail;
global $groupUserPermission;

$userId = $_SESSION['user']['id'];
$groupMembers = $connection->fetchAll("SELECT pr.id AS userId, pr.profile_id AS profileId, pr.fname, pr.lname, pr.userbg, pr.userdp FROM group_members AS gm JOIN profile AS pr ON pr.id = gm.userId WHERE gm.groupId = ? AND gm.isMember = ? LIMIT ?", array($groupId, '1', 4));

$totalGroupMembers = count($groupMembers);

if ($totalGroupMembers >= 4) {
    $totalUsers = $connection->fetchAssoc("SELECT count(1) AS totalMembers FROM group_members WHERE groupId = ? AND isMember = ? ", array($groupId, '1'));
    $totalGroupMembers = $totalUsers['totalMembers'];
}


//$invitedMembers = $connection->fetchAll("SELECT gi.*, p.fname, p.lname FROM group_invites AS gi JOIN profile AS p ON p.id = gi.invitedTo  WHERE groupId = ? ORDER BY id DESC LIMIT ?", array($groupId, 6));

$invitedMembers = $connection->fetchAll("SELECT gi.*, p.fname, p.lname FROM group_invites AS gi LEFT JOIN profile AS p ON p.id = gi.invitedTo  WHERE groupId = ? ORDER BY id DESC LIMIT ?", array($groupId, 3));

if (is_array($invitedMembers) === true) {
	$invitedMembers = array_reverse($invitedMembers);
}

$totalInvitedMembers = count($invitedMembers);
if ($totalInvitedMembers >= 3) {
    $totalInvited = $connection->fetchAssoc("SELECT count(1) AS totalInvited FROM group_invites WHERE groupId = ?", array($groupId));
    $totalInvitedMembers = $totalInvited['totalInvited'];
}

//$suggestedGroups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gm.isMember, gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId GROUP BY gm.groupId) AS temp_1 JOIN (SELECT gm.isMember, gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId != ? GROUP BY gm.groupId) AS temp_2 ON temp_2.totalMembers = temp_1.totalMembers AND temp_1.groupId = temp_2.groupId WHERE temp_1.groupId != ? LIMIT ?", array($_SESSION['user']['id'], $groupId, 6));

$suggestedGroups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId WHERE gm.isMember = '1' GROUP BY gm.groupId ) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId != ? WHERE isMember = '1' GROUP BY gm.groupId) AS temp_2 ON temp_2.totalMembers = temp_1.totalMembers AND temp_1.groupId = temp_2.groupId WHERE temp_1.groupId != ? LIMIT ?", array($_SESSION['user']['id'], $groupId, 6));

foreach ($suggestedGroups as $key => $group) {
	$suggestedGroups[$key]['follow'] = false;
	$userData = $connection->fetchAssoc("SELECT * FROM group_members WHERE groupId =? AND userId =?", array($group['groupId'], $userId));
	if (isset($userData['isMember']) === true) {
		$suggestedGroups[$key]['follow'] = true;
	}
}


$PSModData['group'] = $groupDetail;
$PSModData['totalMembers'] = $totalGroupMembers;
$PSModData['members'] = $groupMembers;
$PSModData['suggestedGroups'] = $suggestedGroups;

$PSModData['groupUserPermission'] = $groupUserPermission;
$PSModData['invitedMembers'] 	  = $invitedMembers;
$PSModData['totalInvitedMembers'] = $totalInvitedMembers;



