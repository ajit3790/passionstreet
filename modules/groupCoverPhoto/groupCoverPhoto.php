<?php
global $groupId;
global $groupDetail;
global $groupUserPermission;

$PSModData['action'] 	= isset($_REQUEST['action']) ? $_REQUEST['action'] : 'home';
$PSModData['groupId'] 	= $groupId;
$PSModData['group'] 	= $groupDetail;
$PSModData['eventTypes']= $PSParams['eventTypes'];

$PSModData['groupUserPermission'] = $groupUserPermission;