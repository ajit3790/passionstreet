<?php
    $loggedInUserId = $_SESSION['user']['id'];
    $groupCategoryData  = PSUtils::passionCategory($group['category']);
    $groupId = isset($group['groupId']) ? $group['groupId'] : '';
    
    $groupCategory  = isset($groupCategoryData['name']) ? $groupCategoryData['name'] : '';
?>
<div class="row">
	<div class="col-md-12 clearfix">
	   	<div class="image_section">
		   	<figure>
          <?php
            if ($group['photo'] != '') {
          ?>
          <img src="<?php echo ROOT_PATH.'coverbg';?>" class="covr_phto unveil" data-src="<?= ROOT_PATH.'files/'.$group['photo']?>" id="gc-photo">
          <?php
            } else {
          ?>
            <img src="<?php echo ROOT_PATH.'coverbg';?>" class="covr_phto" id="gc-photo">
          <?php
            }
          ?>
        </figure>
        <?php
          if (isset($groupUserPermission['role']) === true && ($groupUserPermission['role'] == '1' || $groupUserPermission['role'] == '2' )) {
        ?>        
		   	<span class="change_covr">
			    	<input id="image_file" type="file" name="userbg" callback="changeGroupCoverPhoto" pictype="userbg" croptype="userbg" id="upload3" class="multifilecrop filehidden" data-crpvrsn=2 org-selector="gc-photo"  style="position:absolute;" /><i class="fa fa-camera"></i> <small>Change photo</small>
			 </span>
       <?php
        }
       ?>
		   	<div class="layer">
			    <div class="profile-pic-outer">			      
			      <span class="name"> <?php echo $group['name'];?></span>
            <div  class="comm-info">Created in <?php echo $groupCategory;?> | 
            <?php
              if($group['privacyType'] == 'public') {
                echo '<i class="fa fa-globe"></i> Public</div>';
              } else {
                echo '<i class="fa fa-lock"></i> '.ucfirst($group['privacyType']).'</div>';
              }
            ?>
			      <div class="group-controls">                   		
                <?php
                  if (isset($groupUserPermission['role']) === true && $groupUserPermission['isMember'] == '1') {
                ?>
                <div class="btn-group pull-right">
                  <button type="button" class="btn btn-default"><i class="fa fa-share"></i> <!-- Share --> Action</button>
              	  <!-- <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Share</button> -->
                  <!-- <div class="btn-group">
                  	<button type="button" class="btn btn-default dropdown-toggle" type="button" id="groupnotify" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-check"></i> Notifications</button>
                      <ul class="dropdown-menu" aria-labelledby="groupnotify">
                    	  <li><a href="#">All Posts</a></li>
                        <li><a href="#">Highlights</a></li>
                        <li><a href="#">Friends Posts</a></li>
                        <li><a href="#">Off</a></li>
                      </ul>
                  </div> -->

                  <div class="btn-group">
                  	<button type="button" class="btn btn-default dropdown-toggle" type="button" id="groupsettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-ellipsis-h"></i></button>
                      <ul class="dropdown-menu" aria-labelledby="groupsettings">
                          <li><a href="communities">Manage Community</a></li>
                          <?php
                            if (isset($groupUserPermission['role']) === true && ($groupUserPermission['role'] == '1' || $groupUserPermission['role'] == '2')) {
                          ?>
                          <li><a href="community/<?php echo $group['groupId'].'/edit';?>">Edit Community Setting</a></li>
                          <?php
                            }
                          ?>
                          <li><a href="community/create">Create New Community</a></li>
                      </ul>
                  </div>
                </div>
                
                <div class="btn-group pull-right">
                  <button type="button" class="btn btn-default"><i class="fa fa-user"></i> Joined</button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" type="button" id="groupsettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-caret caret"></i>
                    </button>
                      <ul class="dropdown-menu" aria-labelledby="groupsettings">
                        <li><a href="#" class="exit-group" data-id="<?php echo $groupId;?>">Leave Community</a></li>
                      </ul>
                  </div>
                </div>
                <?php
                  } else if (isset($groupUserPermission['role']) === true && $groupUserPermission['isMember'] == '0') {
                ?>
                <div class="btn-group pull-right">
                  <?php                    
                    if ($group['joiningPermission'] > 0) {
                  ?>
                  <button type="button" class="btn btn-success join-group" data-id="<?php echo $group['groupId'];?>"><i class="fa fa-plus"></i> Join Community</button>
                  <?php
                    } else {
                      echo '<button type="button" class="btn btn-default"><i class="fa fa-share"></i> Action</button>';
                    }
                  ?>

                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" type="button" id="groupsettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-ellipsis-h"></i></button>
                      <ul class="dropdown-menu" aria-labelledby="groupsettings">
                          <li><a href="communities">Manage Community</a></li>
                          <li><a href="community/create">Create New Community</a></li>
                      </ul>
                  </div>
                </div>
                <?php                  
                  } else {
                ?>
                <div class="btn-group pull-right">
                  <?php
                    if ($group['joiningPermission'] > 0) {
                  ?>
                    <button type="button" class="btn btn-success join-group" data-id="<?php echo $group['groupId'];?>"><i class="fa fa-plus"></i> Join Community</button>
                  <?php
                    } else {
                      echo '<button type="button" class="btn btn-default"><i class="fa fa-share"></i> Action</button>';
                    }
                  ?>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" type="button" id="groupsettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-ellipsis-h"></i></button>
                      <ul class="dropdown-menu" aria-labelledby="groupsettings">
                          <li><a href="communities">Manage Community</a></li>
                          <li><a href="community/create">Create New Community</a></li>
                      </ul>
                  </div>
                </div>
                <?php
                  }
                ?>
            </div>		       
			    </div>  
		   </div>
	   </div>	   
	</div>
</div>

<div class="row">
  <div class="col-md-12 ">
    <?php
      if (isset($groupUserPermission['role']) === true && $groupUserPermission['isMember'] == '1') {
    ?>
    <div class="mob-group-controls clearfix">
    	<span class="joined">
      	<i class="fa fa-check" aria-hidden="true"></i>
		    Joined	
      </span> 
      <span>
      	<a href="community/<?php echo $group['groupId'];?>?#invite-member"><i class="fa fa-user-plus" aria-hidden="true"></i>
		    Add Members	</a>
      </span>
      <span class="gp_search">
      	<i class="fa fa-search" aria-hidden="true"></i>
		    Search	
      </span>
      <span class="gp_info">
      	<i class="fa fa-info-circle" aria-hidden="true"></i>
		    Info	
      </span>
    </div>
    <?php
      } else if ($group['joiningPermission'] > 0) {
    ?>
    <div class="mob-group-controls memb-inactive clearfix">
    	<span class="join-group" data-id="<?php echo $group['groupId'];?>">
        <i class="fa fa-plus" aria-hidden="true"></i>
			  Join Community
      </span>
      <span class="gp_info">
        <i class="fa fa-info-circle" aria-hidden="true"></i>
			  Info	
      </span>
    </div>
    <?php
      }
    ?>
  	<div class="group-nav">
  		  <?php
          if (isset($groupUserPermission['role']) === false) {
            echo "<button class='btn btn-primary group-follow follow-action' data-id='$groupId' data-action='1' type='button'>Follow</button>";
          } elseif (isset($groupUserPermission['role']) === true && $groupUserPermission['isMember'] == '0') {
            echo "<button class='btn btn-primary group-follow follow-action' data-id='$groupId' data-action='0' type='button'>Unfollow</button>";
          }

          $activeMembersTab = true;
          if (isset($groupUserPermission['role']) === true && (($groupUserPermission['role'] == 1 || $groupUserPermission['role'] == 2) || ($group['privacyType'] == 'public')) ) {            
            $activeMembersTab = false;
          }
          //if ($activeMembersTab === true || $action == 'members') {
        ?>
        <div class="group-search-form">
           <form class="form" role="search" method="GET">
              <div class="input-group add-on">
                <input class="form-control searchCommunityMember" placeholder="Search" name="search" id="srch-term" type="text">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
              </div>
            </form>
            <span class="form-close">X</span>
        </div> 
        <?php
         // }
        ?>
        <ul class="nav nav-pills full-width V2">
          <?php
          $activeMembersTab = true;

          if (isset($groupUserPermission['role']) === true) {
            $activeMembersTab = false;
          ?>
          <li class="<?php if ($action == 'home'){ echo 'active';}?>">
              <a href='<?php echo "community/".$group["groupId"] ?>' data-toggle="<?php if ($action == 'home'){ echo 'tab';}?>">Discussion</a>
          </li>
          <?php } ?>
          <li class="<?php if ($action == 'members' || $activeMembersTab === true){ echo 'active';}?>" >
              <a href='<?php echo "community/".$group["groupId"]."/members"; ?>' data-toggle="<?php if ($action == 'members' || $activeMembersTab === true) { echo 'tab';}?>">Members</a>
          </li>
          <?php
            if (isset($groupUserPermission['role']) === true && ( ($group['eventPermission'] == 1 && in_array($groupUserPermission['role'], array(1,2))) || ($group['eventPermission'] == 2) ) ) {
              echo "<li><a href='events'>Events</a></li>";
            }
          ?>   
        </ul>
  </div>
  
  <div class="group-info-box" >
  	<section class="group-info-hd">
    	<i class="fa fa-angle-left gp_info-close"></i> Community Info
    </section>
  	<section>
    	<figure>
        <?php
          if ($group['photo'] != '') {
        ?>
        <img src="<?php echo ROOT_PATH.'coverbg';?>" class="covr_phto unveil" data-src="<?= ROOT_PATH.'files/'.$group['photo']?>" id="gc-photo">
        <?php
          } else {
        ?>
          <img src="<?php echo ROOT_PATH.'coverbg';?>" class="covr_phto" id="gc-photo">
        <?php
          }
        ?>
      </figure>
    	<h2><?php echo $group['name'];?></h2>
        <span class="type">
          <?php
            if($group['privacyType'] == 'public') {
              echo '<i class="fa fa-globe"></i> Public';
            } else {
              echo '<i class="fa fa-lock"></i> '.ucfirst($group['privacyType']);
            }
          ?>
        </span>
     </section> 
     <section class="comm_desc">
     	<p>
        <?php echo $group['description'];?>
      </p>
     </section>  
     <section>
     	<a href='<?php echo "community/".$group["groupId"]."/members"; ?>'>Members</a>
     </section>
      <?php
        if (isset($groupUserPermission['role']) === true && $groupUserPermission['role'] == '1'){
      ?>
      <section>
        <a href='<?php echo "community/".$group["groupId"]."/edit"; ?>'>Edit Community Setting</a>
       </section>
      <?php
        }
      ?>
     <section>
     	<a href='<?php echo "communities"; ?>'>Manage community</a>
     </section>
     <section>
      <a href='groups/create'>Create your community</a>
     </section>
     <?php
        if (isset($groupUserPermission['role']) === true && $groupUserPermission['isMember'] == '1'){
      ?>
      <section>
        <a class="exit-group" data-id="<?php echo $group['groupId'];?>">Leave Community</a>
       </section>
      <?php
        }
      ?>
  </div>  
</div>

<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="js/jquery-ui.js" ></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#savebio").click(function(){
	    $temp = $(this);
	    $.post(root_path+'PSAjax.php?type=updateprofile', {bio:$("#userbio").val()}, function(data){ 
	        $res = $.parseJSON(data);
	        $temp.html($res['data']);
	    });    
	});
});

function changeGroupCoverPhoto($selector){  
  var $selectorObj = $('#'+$selector);
  $pictype = $selectorObj.attr('pictype');
  formdata = new FormData();
  var $cropData = {};
  $selectorObj.parent().find('.multicropdata input').each(function(){
    formdata.append($(this).attr('name'),$(this).attr('value'));
  });
  var filedata = $selectorObj[0];
  var i = 0, len = filedata.files.length, img, reader, file;
  for (; i < len; i++) {
    file = filedata.files[i];

    if (window.FileReader) {
        reader = new FileReader();
        reader.onloadend = function(e) {
        };
        reader.readAsDataURL(file);
    }
    formdata.append($pictype+"[]", file);
  }
  $boxid = $selectorObj.attr('org-selector');
  formdata.append('pictype',$pictype);
  $selectorObj.replaceWith( $selectorObj = $selectorObj.clone( true ) );
  if (formdata) {
    $.ajax({
        url: appendAjaxDefaultParameters(root_path+'AjaxGroup.php?requestType=changeGroupCoverPhoto&boxid='+$boxid+'&groupId='+groupId),
        type: "POST",
        data: formdata,
        processData: false,
        contentType: false,
        success: function(data) {
          $res = $.parseJSON(data);
          $('#gc-photo').attr('src',upload_path+$res['data']);
        },       
        error: function(res) {

        }       
      });
    };
  }
  
  //Open/close search form Mobile
  $('.gp_search').click(function(){
	  $('.group-search-form').show();
	  });
  $('.form-close').click(function(){
	  $('.group-search-form').hide();
	  });
	  
  //Open/close Group Info form Mobile	  
	  
	  
  $('.gp_info').click(function(){
  		$('.group-info-box').show("slide", { direction: "left" }, 500);
  });
  $('.gp_info-close').click(function(){
	  $('.group-info-box').hide("slide", { direction: "left" }, 500);
  });
  
  // Invite member
    $(".searchCommunityMember").autocomplete({
        source: function (request, response)  {
          $.ajax({
            url: root_path+"PSAutocomplete.php?type=searchCommunityMember",
            dataType: "json",
            data:{
              term: request.term
            },
            success: function (data) {
              response(data);
            }
          });
        },
        minLength: 2,
        select: function (event, ui) {
            var optedId = ui.item ? ui.item.id : 0;
            var addedMemberName = ui.item.name;
            var profile_id = ui.item.profile_id;
            
            var profileName = addedMemberName.toLowerCase()
              .replace(/ /g,'-')
              .replace(/[^\w-]+/g,'');
            window.location.replace(root_path+'profile/'+profileName+'/'+profile_id);
        }

    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li></li>" )
        .data( "item.autocomplete", item)
        .append( "<a class='myclass' customattribute='" + item.name + "'> <img unveil src='"+root_path+"profiledp' data-src='"+root_path+item.userdp+"' width='40px' height='40px'> " + item.name + "</a>" )
        .appendTo(ul);
    };
</script>
<?php
$PSJsincludes['external'][] = "js/bootbox.min.js";
$PSJsincludes['external'][] = "js/groups.js";
$PSJsincludes['external'][] = "js/fengyuanchencropper/croppermain2.js";
$PSJsincludes['external'][] = "js/fengyuanchencropper/cropper.js";
$PSCssincludes['external'][] = "js/fengyuanchencropper/cropper.css";
?>
