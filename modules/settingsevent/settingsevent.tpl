<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="settings-box">
                <span class="error-text">
                    <?=$server_response_text?>
                </span>
                <form class="form1" name="registration-form" method="post">
                   <input type="hidden" name="type" value="<?=$type?>">
                    <?php
                    foreach($$type as $key=>$setting)
                    {
                    ?>
                   <div class="row form-group">
                      <div class="col-xs-6">
                          <span class="title"><?=$setting?></span>
                      </div>
                      <div class="col-xs-6">
                          <select class="form-control" name="<?=$key?>">
                              <?php
                              if($key == 'rewardpoints')
                              $options1 = array_reverse($options);
                              else
                              $options1 = $options;
                              foreach($options1 as $key2=>$option)
                              {
                                echo '<option value="'.$key2.'">'.$option.'</option>';
                              }
                              ?>
                          </select>
                      </div>
                   </div>
                    <?php
                    }
                    ?>
                   <div class="row form-group">
                      <div class="col-xs-4 col-xs-offset-4">
                        <input name="submit" class="form-control btn btn-primary" value="Update" type="Submit">
                      </div>
                   </div> 

                </form>
            </div>
        </div>
    </div>
</div>