<?php
$_GET['page'] 	= (!empty($_GET['page'])?$_GET['page']:1);
$moduleName 		= isset($_GET['pagetype']) ? $_GET['pagetype'] : '';
$moduleId = '';

if ($moduleName == 'community') {
	$moduleId = isset($_GET['communityId']) ? $_GET['communityId'] : '';
} elseif (isset($_GET['producttype']) === true && $_GET['producttype'] == 'event') {
	$moduleName = $_GET['producttype'];
	$moduleId = isset($_GET['event_id']) ? $_GET['event_id'] : '';
} elseif ($moduleName == 'passion') {
	$moduleId = isset($_GET['type']) ? $_GET['type'] : '';
} elseif ($moduleName == 'userprofile') {
	$moduleId = isset($_GET['profile_id']) ? $_GET['profile_id'] : '';
	$moduleId = ($moduleId == '' && isset($_GET['moduleId']) === true) ? $_GET['moduleId'] : $moduleId;
}

if ($moduleId == '') {
	$moduleId = ($moduleId == '') ? $_SESSION['user']['profile_id'] : $moduleId;
}

$page = isset($_GET['page']) ? $_GET['page'] : 1;
if (isset($_GET['page']) === false && $_GET['page'] < 1) {
	$page = 1;
}


$post_list = group_posts(array('moduleName' => $moduleName, 'moduleId' => $moduleId), $_GET['page']);

$PSModData['nextlink'] = ROOT_PATH.'moduleAJX/wall?page='.($page+1).'&pagetype='.$moduleName.'&moduleName='.$moduleName.'&moduleId='.$moduleId;

$PSModData['post_list'] = $post_list;
$PSModData['new_post'] = $post_list;
?>