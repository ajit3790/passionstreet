<?php
//print_array($event);
?>
<div class="event-banner">
	<?php
	if($event['source'] == 'aggregated' && empty($PSParams['amp']))
	{ ?>
	<span class="source">
		<a href="https://facebook.com/events/<?=$event['event_id']?>" rel="noindex,nofollow" target="_blank">Source: <i class="fa fa-facebook-square"></i></a>
	</span>
	<?php } ?>
	<img class="eventbannerimg1 unveil" src="<?=DEFAULT_IMG_HORIZONTAL?>" data-src="<?=$event['eventpic']?>">
	<div class="hdng">
		<?php /* <a href="#" class="btn btn-primary follow ">Follow</a> */ ?>
		<h1 class="event-title"><?=$event['eventname']?></h1>
        <div class="event-lcn">
        <i class="fa fa-map-marker"></i> <?=$event['venue'].', '.$event['city']?> 
	<?php
	if(empty($PSParams['amp']))
	{ ?>
        <a href="#" class="callmodaliframe view-map" data-targetsrc="module/maps/map?coordinate=<?=urlencode($event['maploc'])?>" title="View on map">
           View on map
        </a>
	<?php } ?>
	</div>
	</div>
	<div class="date-stamp">
			<?php
				//$eventdate = date_parse_from_format("Y-m-d", $event['datestart']);
				$eventdateData = getdate(strtotime($event['datestart']));
				$eventMonth = strtoupper($PSParams['months'][$eventdateData['mon']]);
				$eventDate = $eventdateData['mday'];
			?>
			<!-- <span><?=$eventMonth?></span>
			<span><?=$eventDate?></span> -->
		</div>
</div>
<div class="event-info clearfix">
	<div class="clm inlinewidth">
		<section class="clearfix">
			
			<i class="fa fa-calendar"></i>	
			<?php
				$datestr = '';
				if($event['producttype'] == 'tour'){
					$datestr = 'Season <br /> '. date('jS M',strtotime($event['datestart'])).' to '.date('jS M',strtotime($event['dateend']));
				}
				else if($event['onedayEvent'] == 1)
				{
					$eventdatestart = getdate(strtotime($event['datestart']));
					$eventdateend = getdate(strtotime($event['dateend']));
					$datestr = $eventdatestart['weekday'].', '.$eventdatestart['mday'].' '.$eventdatestart['month'].' '.$eventdatestart['year'].' '; 
					$datestr .= ' <br />'.($eventdatestart['hours']<12)?($eventdatestart['hours'].':'.$eventdatestart['minutes'].' AM'):($eventdatestart['hours']-12 .':'.$eventdatestart['minutes'].' PM');
					$datestr .= ' to '. (($eventdateend['hours']<12)?($eventdateend['hours'].':'.$eventdateend['minutes'].' AM'):($eventdateend['hours']-12 .':'.$eventdateend['minutes'].' PM'));
				}
				else
				{
					/*
					$eventdatestart = getdate(strtotime($event['datestart']));
					$eventdateend = getdate(strtotime($event['dateend']));
					print_array($eventdateend);
					print_array($event['dateend']);
					print_array(date('j F Y, h:i A, l',strtotime($event['dateend'])));
					$datestr1 = sprintf("%02d",$eventdatestart['mday']).' '.$eventdatestart['month'].' '.$eventdatestart['year'] .', '.$eventdatestart['hours'].':'.$eventdatestart['minutes']; 
					$datestr2 = sprintf("%02d",$eventdateend['mday']).' '.$eventdateend['month'].' '.$eventdateend['year'] .', '.$eventdateend['hours'].':'.$eventdateend['minutes'] ; 
					*/
					// $datestr1 = ''.date('j F Y, h:i A, l',strtotime($event['datestart'])); 
                                        if($event['multislot'] == 0)
                                        {
					$datestr = ''.date('j M y, h:i A',strtotime($event['datestart'])); 
					$datestr .= ' To - '.date('j M y, h:i A',strtotime($event['dateend']));
                                        }
                                        else
                                        {
                                            foreach($event['upcomingdatetimeslots'] as $slot){
                                                $datestr .= '<div class="datetimeslots">'.date('M j, H:i',strtotime($slot['datestart'])).' -> '.date('M j, H:i',strtotime($slot['dateend'])).'</div>';
                                            }
                                            foreach($event['endeddatetimeslots'] as $slot){
                                                $datestr .= '<div class="datetimeslots strikethrough">'.date('M j, H:i',strtotime($slot['datestart'])).' -> '.date('M j, H:i',strtotime($slot['dateend'])).'</div>';
                                            }
                                        }
				}
			?>
			
			<?=$datestr?>
			
			<?php
                        /*
			<span class="addto-calendar  ">
				<span class="atcb-link">
				Add to calendar
				</span>
				<ul class="addEvent">
                                <li class="addEventli" data-type="google">Google</li>
                                <li class="addEventli" data-type="outlook">Outlook</li>
                                <li class="addEventli" data-type="yahoo">Yahoo</li>
                                <li class="addEventli" data-type="appleical">Apple</li>
                                </ul>
			</span> 
                        */
                        ?>
		</section>	
		
		
	</div>	
	
        <div class="clm inlinewidth" id="eventdata" >
        
            <!--<span alt="Views" title="Views"><i class="fa fa-eye" aria-hidden="true"></i></span>
            <span alt="Participation" title="Participation"><i class="fa fa-users" aria-hidden="true"></i></span>-->
            
	</div>
</div>
<script>


$(document).ready(function(){
	$(".atcb-link").click(function(event){
	event.stopPropagation();	
	$(".addEvent").slideToggle();
	});
    
	$("body, .addEvent li").click(function(){	  
	  if($('.addEvent').css('display') == 'block')
		{
			$(".addEvent").hide();
		}
	});
	
	$t = {};
	$t['service'] = 'google';
	$t['dstart'] = '<?=$event["datestart"]?>';   
	$t['dend'] = '<?=$event["dateend"]?>';
	$t['dallday'] = 'true';
	$t['dsum'] = '<?=$event["eventname"]?>';
	$t['ddesc'] = 'Join event';
	$t['calname'] = '<?=$event["eventname"]?>';
	$t['dateformat'] = 'YYYY/MM/DD hh:mm:ss';
	$t['reference'] = orgurl;
	console.log($t);
	
	$(".addEventli").on('click',function(e){
	$t['service'] = $(this).attr('type');
	$tquery = '';
	$.each($t,function(i,v){
	$tquery = $tquery +"&"+i+"="+encodeURI(v);
	});
	$turl = 'https://addevent.com/create/?'+$tquery;
	console.log($turl);
	window.open($turl, '_blank');
	});
        var templink;
        try{
          templink = $('link[rel=canonical]').attr('href') || orgurl;
        }
        catch(e){
          templink = orgurl;
        }
		templink = q['event_id'];
        $.get( root_path+'PSAjax.php?type=eventpagedata',{ page: templink, startdate: "<?=$event['createdate']?>"}, function( data ) {
            console.log( data );
            $("#eventdata").html(data['data']);
        },'json');
});
</script>

