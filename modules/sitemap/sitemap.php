<section id="sitemap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <ul class="clearfix">
                    <?php
                    foreach($PSParams['PSCategories'] as $PSCategorykey=>$PSCategory)
                    {
                        ?>
                        <li><a href="<?=$PSCategory['link']?>"><?=$PSCategory['name']?></a></li>
                        <?php
                    }
                    ?>
                </ul>
                <div class="contact">
                <h3>Connect with us : </h3>
                <ul>
                    <!--li class="hide">
                        <i class="fa fa-map-marker"></i>
                        Passion Streets Pvt Ltd..<br>
                        912, LS Road,<br>
                        Noida, Uttar Pradesh 201301
                    </li>
                    <li class="mob"><i class="fa fa-mobile"></i>(XXX) 9990-254-044</li-->
                    <li><i class="fa fa-envelope"></i><a href="mailto:social@passionstreet.in">social@passionstreet.in</a></li>
                </ul>
                </div>
            </div>
            
           <!--
                <h3>Latest Tweets</h3>
                <ul>
                    <li>
                        <i class="fa fa-twitter"></i>
                        For those who have not seen our invitation to to the world. Here is our Tourisim Commercial. <a href="#">http://fb.me/27xZxXzVv
@passionstreet</a> <span>8 mins ago</span>
                    </li>
                    <li>
                        <i class="fa fa-twitter"></i>
                        Today, we flagged off our Cycle Rally from Jantar Mantar, Delhi.
                        @passionstreet  <span>52 mins ago</span>
                    </li>
                </ul> -->
            
        </div>
    </div>
</section>