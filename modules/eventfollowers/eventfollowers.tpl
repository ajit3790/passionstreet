<div class="col-md-12">
    <h2 class="hstyl_1 sm">Who else have signed up for the event</h2>
</div>
<div class="row">
    <div class="col-md-12 carousel-wrapper">
        <i class="fa fa-angle-left"></i>
        <i class="fa fa-angle-right"></i>
        <div class="inner">
            <ul style="width:2000px;">
                <li>
                    <div class="card-styl_2 clearfix">
            <figure>
                <img alt="" src="images/user-thumb.jpg">
                <figcaption>Saumitra Day</figcaption>
            </figure>
            <div class="desc">
                <h5>Passionate About</h5>
                <div class="passions">
                    <img alt="" src="images/cycling.jpg">
                    <img alt="" src="images/dancing.jpg">
                    <img alt="" src="images/culinary.jpg">
                </div>
                <div class="stat clearfix">
                    <section>
                        <strong>322</strong> FOLLOWERS
                    </section>
                    <section>
                        <strong>102</strong> FOLLOWING
                    </section>
                </div>
                <button class="btn btn-primary">FOLLOW HIM</button>
                <span class="following">FOLLOWING YOU</span>
            </div>
        </div>
                </li>
                <li>
                    <div class="card-styl_2 clearfix">
            <figure>
                <img alt="" src="images/user-thumb.jpg">
                <figcaption>Saumitra Day</figcaption>
            </figure>
            <div class="desc">
                <h5>Passionate About</h5>
                <div class="passions">
                    <img alt="" src="images/cycling.jpg">
                    <img alt="" src="images/dancing.jpg">
                    <img alt="" src="images/culinary.jpg">
                </div>
                <div class="stat clearfix">
                    <section>
                        <strong>322</strong> FOLLOWERS
                    </section>
                    <section>
                        <strong>102</strong> FOLLOWING
                    </section>
                </div>
                <button class="btn btn-primary">FOLLOW HIM</button>
                <span class="following">FOLLOWING YOU</span>
            </div>
        </div>
                </li>
                <li>
                    <div class="card-styl_2 expert clearfix">
            <span class="expert-flag"></span>
            <figure>
                <img alt="" src="images/user-thumb.jpg">
                <figcaption>Saumitra Day</figcaption>
            </figure>
            <div class="desc">
                <h5>Expert In</h5>
                <div class="passions">
                    <img alt="" src="images/cycling.jpg">
                    <img alt="" src="images/dancing.jpg">
                    <img alt="" src="images/culinary.jpg">
                </div>
                <div class="stat clearfix">
                    <section>
                        <strong>322</strong> FOLLOWERS
                    </section>
                    <section>
                        <strong>102</strong> FOLLOWING
                    </section>
                </div>
                <button class="btn btn-primary">FOLLOW HIM</button>
            </div>
        </div>
                </li>
                <li>
                    <div class="card-styl_2 clearfix">
            <figure>
                <img alt="" src="images/user-thumb.jpg">
                <figcaption>Saumitra Day</figcaption>
            </figure>
            <div class="desc">
                <h5>Passionate About</h5>
                <div class="passions">
                    <img alt="" src="images/cycling.jpg">
                    <img alt="" src="images/dancing.jpg">
                    <img alt="" src="images/culinary.jpg">
                </div>
                <div class="stat clearfix">
                    <section>
                        <strong>322</strong> FOLLOWERS
                    </section>
                    <section>
                        <strong>102</strong> FOLLOWING
                    </section>
                </div>
                <button class="btn btn-primary">FOLLOW HIM</button>
                <span class="following">FOLLOWING YOU</span>
            </div>
        </div>
                </li>
            </ul>
        </div>
    </div>

</div>