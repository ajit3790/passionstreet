<div class="tab-header mb0">
	<ul class="nav nav-pills full-width">
		<li role="presentation" class="active"><a href="#pagesimanage" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">pages I Manage</a></li>
		<li role="presentation" class=""><a href="#mypages" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">pages Joined / Following</a></li>
        	<li role="presentation" class=""><a href="#createpage" id="createpagetab" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Create a page</a></li>
	</ul>
</div>
<div class="bx-styl tab-content">
	<div id="pagesimanage" class="tab-pane fade active in" role="tabpanel">
		<ul class="list-group">
		<?php
		foreach($pagelist['mypages'] as $page)
		{
			echo '<li class="list-group-item"><a href="'.ROOT_PATH.'page/'.get_alphanumeric($page['page_name']).'/'.$page['page_id'].'" style="display:block">'.$page['page_name'].(($page['status'] == 1)?'<span class="alert-danger badge pull-right">Pending Email Verification</span>':'<span class="alert-success badge pull-right">Live</span>').'</a></li>';
		}
		?>
	</div>
	<div id="mypages" class="tab-pane fade in" role="tabpanel">
		pages i Follow
	</div>
	<div id="createpage" class="tab-pane fade in" role="tabpanel">
		<div class="row">
		    <div class="col-md-12"><h2 class="hstyl_2"><span>Add a new page</span></h2></div>
		</div>
		
		<div class="row">
		    <div class="col-md-12">
			<div class="form-wrapper">
			    <form class="pagecreationform" action="<?=$_SERVER['PHP_SELF_URL']?>" method="post" enctype="multipart/form-data">
				<div class="row ">
				  <div class="col-sm-12 form-group ">
				      <input type="text" value="<?=$params['name']?>" name="name" placeholder="page Name" required="" class="form-control name">
				  </div>
			        </div>
				<div class="row ">
				  <div class="col-sm-12 form-group ">
				      <input type="email" value="<?=$params['email']?>" name="email" placeholder="page Email" required="" class="form-control email">
				  </div>
			        </div>
				<div class="row form-group">
				    <div class="col-sm-12">
				        <input name="submit" id="createpagesubmitbtn" class="form-control btn btn-yellow" value="Create Page" type="Submit">
				    </div>
			        </div>
				<div class="row form-group">
				    <div class="col-sm-12 pagecreationmessage hide text-center">
				        
				    </div>
			        </div>
			    </form>
			</div>
		    </div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$("#createpagesubmitbtn").on('click',function(event){
		event.preventDefault();
		var $form = $(".pagecreationform");
		$temp = {};
		$temp['name'] = $form.find('.name').val();
		$temp['email'] = $form.find('.email').val();
		$.post(root_path+'PSAjax.php?type=create-page', $temp, function(data){ 
			$res = $.parseJSON(data);
			$('.pagecreationmessage').removeClass('hide');
			$('.pagecreationmessage').html($res['data']);
			if($res['status'] == "200")
			{
			$(".pagecreationform").find('.name').val('');
			$(".pagecreationform").find('.email').val('');
			}
		});
	});
});
</script>
