<div class="tab-header mb0">
	<ul class="nav nav-pills full-width">
		<li role="presentation" class="active"><a href="#pageeditdetail" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Edit Details</a></li>
		<li role="presentation" class=""><a href="#pagemembers" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Members</a></li>
        	<li role="presentation" class=""><a href="#pageadmins" id="createpagetab" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Admins</a></li>
        	<li role="presentation" class=""><a href="#pageevents" id="createpagetab" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Events</a></li>
	</ul>
</div>
<div class="bx-styl tab-content">
	<div id="pageeditdetail" class="tab-pane fade active in" role="tabpanel">
		tab 1
	</div>
	<div id="pagemembers" class="tab-pane fade in" role="tabpanel">
		tab 2
	</div>
	<div id="pageadmins" class="tab-pane fade in" role="tabpanel">
		tab 3 
	</div>
	<div id="pageevents" class="tab-pane fade in" role="tabpanel">
		tab 4
	</div>
</div>
<script>
$(document).ready(function(){
	
});
</script>
