<?php
$eventid = $_GET['event_id'];
if(!($eventid) ||  !($event = event_get_details($eventid,1))){
    header("location:".ROOT_PATH.'events/create');
}
else if($event['creator'] != $_SESSION['user']['profile_id']){
    header("location:".ROOT_PATH.'events/create');
}
if(!($_GET['step']))
{
    if(!isset($_SESSION['eventcreation'][$eventid]['step1']))
    $_GET['step'] = 'step1';
    else if(!isset($_SESSION['eventcreation'][$eventid]['step2']))
    $_GET['step'] = 'step2';
    else if(!isset($_SESSION['eventcreation'][$eventid]['step3']))
    $_GET['step'] = 'step3';
}
$PSModData['showform'][$_GET['step']] = 'show';
if($_POST['event-update'])
{
    extract($_POST);
    $timestart = generate_standard_time($timestart);
    $timeend = generate_standard_time($timeend);
    $datestart = $datestart.' '.$timestart;
    $dateend = $dateend.' '.$timeend;
    $inputParams['eventtype'] = $eventType;
    $inputParams['eventname'] = $name;
    $inputParams['category'] = $category;
    $inputParams['datestart'] = generate_standard_date($datestart);
    $inputParams['dateend'] = generate_standard_date($dateend);
    $inputParams['description'] = $description;
    $inputParams['terms'] = $terms;
    $map_loc_components = json_decode($map_loc_components,true);
    $inputParams['venue'] = $map_loc_components['name'];
    $inputParams['city'] = $map_loc_components['city'];
    $inputParams['state'] = $map_loc_components['state'];
    $inputParams['address'] = strip_tags($address,'<br></br />');
    if($map_loc_coordinate)
    $inputParams['maploc'] = $map_loc_coordinate;
    if($map_loc_image)
    $inputParams['mapimage'] = upload_from_url('eventmap',$map_loc_image);
    //$inputParams['eventpic'] = upload_from_uri('eventbanner',$pic_post);
    if($_POST['multicropdata']['eventbanner'])
    {
    $inputParams['eventpic'] = upload_post_images_new2($_FILES,'eventbanner');
    $inputParams['eventpic'] = serialize($inputParams['eventpic']);
    }
    //disallow edit
    unset($inputParams['eventname']);
    unset($inputParams['venue']);
    unset($inputParams['city']);
    unset($inputParams['state']);
    
    event_update($eventid,$inputParams);
    if($editmode == 1)
    header("location:".ROOT_PATH.'events/manage/'.$eventid.'#editevent-eventdetails');
    else
    header("location:".ROOT_PATH.'events/publish/'.$eventid);
    exit();
}
else if($_POST['step1'])
{
    extract($_POST);
    $inputParams['bankaccount'] = $ticket_bank_account;
    if(strtolower($ticketfeeradio) != 'yes')
    $inputParams['tickettype'] = 'free';
    else
    {
	$inputParams['tickettype'] = 'paid';
	$maxticketprice = -1;
	$minticketprice = -1;
	foreach($ticketfeecategory as $key=>$value)
        {
            if($ticketfeecategory[$key] && $ticketfeefee[$key] && $ticketfeeticketss[$key] )
            {
		$temp = array();
		$temp['tktcatname'] = $ticketfeecategory[$key];
		$temp['tktcatprice'] = $ticketfeefee[$key];
		
		if($temp['tktcatprice'] < $minticketprice)
		$minticketprice = $temp['tktcatprice'];
		else if($minticketprice == -1)
		$minticketprice = $temp['tktcatprice'];
		
		if($temp['tktcatprice'] > $maxticketprice)
		$maxticketprice = $temp['tktcatprice'];
		else if($maxticketprice == -1)
		$maxticketprice = $temp['tktcatprice'];
		
		$temp['tktcatcount'] = $ticketfeeticketss[$key];
		$temp['tktcatminbuy'] = $ticketfeeminbuycount[$key];
		$temp['tktcatmaxbuy'] = $ticketfeemaxbuycount[$key];
		$temp['tktcatbibintial'] = $ticketfeebibintial[$key];
		$temp['tktcatbibstart'] = $ticketfeebibseqstrt[$key];
		$temp['tktcateventid'] = $eventid;
		ticketcategories_add($temp);
                $ticketCategories[] = $temp;
                $maxticketcount = $maxticketcount  +  $ticketfeeticketss[$key];
            }
        }
	$maxticketprice = ($maxticketprice == -1)?0:$maxticketprice;
	$minticketprice = ($minticketprice == -1)?0:$minticketprice;
	$inputParams['ticketvaluerange'] = $minticketprice.','.$maxticketprice;
    
    }
    
    $inputParams['extra']['organiser'] = $organiser;
    $inputParams['extra']['allowuserpost'] =  $event['allowuserpost'];
    $inputParams['extra'] = serialize($inputParams['extra']);
    $_SESSION['eventcreation'][$eventid]['step1'] = $inputParams;
    event_update($eventid,$inputParams);
    header("location:".ROOT_PATH.'events/publish/'.$eventid);
    exit();
}
else if($_POST['step2'])
{
    extract($_POST);
    if(strtolower($event_visibility) == 'open-to-all')
    $inputParams['visibility'] = 'open-to-all';
    else
    $inputParams['visibility'] = 'invite-only';
    
    if(strtolower($event_precondition) == 'yes')
    $inputParams['precondition'] = 'yes';
    else
    $inputParams['precondition'] = 'yes';
    
    $_SESSION['eventcreation'][$eventid]['step2'] = $inputParams;
    event_update($eventid,$inputParams);
    header("location:".ROOT_PATH.'events/publish/'.$eventid);
    exit();
}
else if($_POST['step3'])
{
    extract($_POST);
    $route_map = $event_include_routemap;
    if($route_map == 'Yes')
    {
        foreach($waypoints as $stop)
        {
            $x[] = json_decode($stop, true);
        }
        $route_map_stops = serialize($x);
        if($route_pic)
        $routeimage = upload_from_url('eventroutemap',$route_pic);
    }
    $inputParams['extra'] = $_SESSION['eventcreation'][$eventid]['step1']['extra'];
    $inputParams['extra']['allowuserpost'] = $allowuserpost ;

    $inputParams = array();
    if(strtolower($allowuserpost) == 'allow')
    $inputParams['extra']['allowuserpost'] =  'allow';
    else
    $inputParams['extra']['allowuserpost'] =  'moderate';
    
    $inputParams['extra']['organiser'] =  $event['organiser'];
    $inputParams['extra'] = serialize($inputParams['extra']);
    $inputParams['routemap'] =  $route_map_stops;
    $inputParams['routeimage'] =  $routeimage;
    $inputParams['status'] =  'published';
    $_SESSION['eventcreation'][$eventid]['step3'] = $inputParams;
    event_update($eventid,$inputParams);
    $module_display_style = "done";
    
}
$pages = page_get_list(array('creator_id'=>$PSData['user']['profile_id'],'status'=>2));
$pageContact = array();
foreach($pages as $page)
{
	$temp = array();
	$temp['id'] = $page['page_id'];
	$temp['name'] = $page['page_name'];
	$temp['email'] = $page['page_email'];
	$temp['contact'] = ($page['page_contactno'])?$page['page_contactno']:'';
	$temp['address'] = ($page['page_address'])?$page['page_address']:'';
	$pageContact[$temp['id']] = $temp;
}
$userContact = array();
	$temp = array();
	$temp['id'] = $PSData['user']['profile_id'];
	$temp['name'] = $PSData['user']['fullname'];
	$temp['email'] = $PSData['user']['email'];
	$temp['contact'] = ($PSData['user']['contactno'])?$PSData['user']['contactno']:'';
	$temp['address'] = ($PSData['user']['city'])?$PSData['user']['city'].', '.$PSData['user']['country']:'';
	$userContact[$temp['id']] = $temp;

$PSJavascript['pageContact'] = $pageContact;
$PSJavascript['userContact'] = $userContact;
/*$PSJavascript['dateRange'] = array(
    "start"=>date("d/m/Y",strtotime(generate_standard_date($event["datestart"]))),
    "end"=>date("d/m/Y",strtotime(generate_standard_date($event["dateend"])))
);*/
$PSJavascript['dateRange'] = array(
    "start"=>generate_standard_date($event["datestart"]),
    "end"=>generate_standard_date($event["dateend"])
);
$PSModData['event'] = $event;
?>