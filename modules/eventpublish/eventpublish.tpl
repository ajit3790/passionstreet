<?php
$orgevent = $event;
if($editmode != 1)
{
?>
<div class="col-md-12"><h2 class="hstyl_2 mb-large"><span><i class="fa fa-pencil-square-o"></i>Manage Your Event / Workshop</span></h2></div>
<div class="steps">
    <div class="row">
        <div class="col-md-4 <?=(($_GET['step']=='step1')?'active':'')?>">
            <a href="events/publish/<?=$event['event_id']?>/step1">
                <img  alt="" src="images/ticketing.png"><span class="title">Ticketing</span>
            </a>
        </div>
        <div class="col-md-4 <?=(($_GET['step']=='step2')?'active':'')?>">
            <a href="events/publish/<?=$event['event_id']?>/step2">
                <img alt="" src="images/selection.png"><span class="title">Selection Criteria</span>
            </a>
        </div>
        <div class="col-md-4 <?=(($_GET['step']=='step3')?'active':'')?>">
            <a href="events/publish/<?=$event['event_id']?>/step3">
                <img alt="" src="images/ticketing.png"><span class="title">Add Some Spice</span>
            </a>
        </div>
    </div>   
</div>
<?php
}
?>
<div class="row tab-content">
    <?php if($editmode!=1 || ($editmode==1 && $showform['step1']))
    {?>
    <div  role="tabpanel" class="col-md-12 row tab-pane <?=($showform['step1'])?$showform['step1'].' fade active in':'hide'?>" id="step1">
        <div class="form-wrapper">
            <form class="form1 event-manage" method="post" id="form-step1" action="<?=ROOT_PATH.'events/publish/'.$event['event_id']?>">
               <div class=" row">
                    <label class="col-sm-7 col-xs-12 form-group  control-label">
                    	<h4 class="hstyl_4">Is there any participation fee?</h4>
                    </label>
                    <div class="radio form-group col-sm-2 col-xs-2">
                        <label>
                          <input type="radio" value="Yes" class="ticketfeeradio" name="ticketfeeradio" <?=($event['tickettype']=='Paid')?'checked':''?>>
                          Yes
                        </label>
                     </div>
                     <div class="radio form-group col-sm-2 col-xs-2">
                        <label>
                          <input type="radio" value="No" class="ticketfeeradio" name="ticketfeeradio" <?=($event['tickettype']=='Free')?'checked':''?>>
                          No
                        </label>
                     </div>
               </div>
	       
               <div class="tickets-price <?=($event['tickettype']=='Paid')?'':'hide'?>" style="padding-top:20px; padding-bottom:20px;"> 
                
                <div class="row">
                    <label class="col-sm-4  form-group  control-label">Ticket Type</label>
                    <label class="col-sm-3  form-group  control-label">Rate per Ticket</label>
                    <label class="col-sm-3  form-group  control-label">Allocate Tickets</label>
                    <label class="col-sm-2  form-group  control-label text-center">&nbsp;</label>
               </div>
               <div class="ticket-price-row-box">   

		<?php
		if(empty($event['ticketcategories']))
		{
			$temp = array();
			$temp['name'] = '';
			$temp['price'] = '';
			$temp['minbuy'] = 1;
			$temp['maxbuy'] = 10;
			$temp['bibinitial'] = '';
			$temp['bibstart'] = '';
			$temp['totalcount'] = 0;
			$event['ticketcategories'][] = $temp;
		}
		foreach($event['ticketcategories'] as $ticketid=>$ticketdata)
		{
		?>
	                <div class="ticket-price-row row" >
			<div class="row1" style="background-color:#f4f4f4">
	                    <div class="col-sm-4  form-group  "><input type="text" name="ticketfeecategory[]" value="<?=$ticketdata['name']?>" placeholder="Define Ticket Name"  class="form-control"></div>
	                    <div class="col-sm-3  form-group  "><input type="text" name="ticketfeefee[]" value="<?=$ticketdata['price']?>" placeholder="Ticket Price In INR"  class="form-control"></div>
	                    <div class="col-sm-3  form-group  "><input type="text" name="ticketfeeticketss[]" value="<?=$ticketdata['totalcount']?>" placeholder="Enter no.of Tickets"  class="form-control"></div>
	                    <div class="col-sm-2 tools"><i class="fa fa-cog" onclick="$(this).parents('.ticket-price-row').find('.extrafields').toggleClass('hide')"></i><i class="fa fa-trash" onclick="$(this).parents('.ticket-price-row').remove()"></i></div>
	                </div>
			<div class="row1 extrafields hide clearfix" style="background-color:#f4f4f4">    
			    <div class="col-sm-2  form-group">Min Ticket user can buy</div>
	                    <div class="col-sm-2  form-group">Max Tickets user can buy</div>
	                    <div class="col-sm-3  form-group">BIB / SR no. initial</div>
	                    <div class="col-sm-3  form-group">BIB / SR no. Starts with</div>
	                    <div class="col-sm-2  form-group">&nbsp;</div>
			</div>
			<div class="row1 extrafields hide clearfix" style="background-color:#f4f4f4;border-bottom:1px solid #eeeeee;">    
			    <div class="col-sm-2  form-group"><input type="text" name="ticketfeeminbuycount[]" placeholder="Minimum buy count" value="<?=$ticketdata['minbuy']?>"  class="form-control"></div>
	                    <div class="col-sm-2  form-group"><input type="text" name="ticketfeemaxbuycount[]" placeholder="Maximum buy count" value="<?=$ticketdata['maxbuy']?>" class="form-control"></div>
	                    <div class="col-sm-3  form-group"><input type="text" name="ticketfeebibintial[]" placeholder="Initials for SR no." value="<?=$ticketdata['bibinitial']?>" class="form-control"></div>
	                    <div class="col-sm-3  form-group"><input type="text" name="ticketfeebibseqstrt[]" placeholder="Set start sequence for SR no." value="<?=$ticketdata['bibstart']?>"  class="form-control"></div>
	                    <div class="col-sm-2  form-group">&nbsp;</div>
	               </div>
	               </div>
		       
	       <?php
	       }
	       ?>
              </div>

               <div class="row ">
                  <div class="col-sm-12 form-group text-right ">
                    <span class="add-more more-ticket-categories">add more ticket type</span>
                  </div>
               </div>
               </div>
                <div class="tickets-total-count hide"> 
                
                    <div class="row">
                        <label class="col-sm-6  form-group  control-label">Total Ticket Count</label>
                        <label class="col-sm-6  form-group  control-label"><input type="text" name="totalticketcount" placeholder="1000" class="form-control"></label>
                   </div>
               </div>
               <div class="bookinglastday"> 
                   <div class="row">
                        <div class="col-sm-6  form-group " style="line-height:30px;">Last date of Registering</div>
                        <div class="col-sm-6  form-group "><i class="fa fa-calendar" style="margin-right:10px"></i><input type="text" placeholder="DD/MM/YYYY" data-type="datetimepicker" class="form-control date datepicker date-range" style="background-color: #fff" readonly="" value="<?=(($event['createdate'])?date('d/m/Y h:i:s',strtotime($event['createdate'])):'')?>" name="ticketbookingends"></div>
                    </div>
               </div>
               <div class="bank-account hide"> 
               <div class="row">
                    <label class="control-label col-md-12  form-group">Kindly Provide your Bank Details to transfer event ticketing fee money</label>
                </div>
               
                <ul class="nav nav-tabs nav-justified" style="margin-bottom:-1px">
                    <li class="active"><a data-toggle="tab" href="#savedaccounts">Saved Accounts</a></li>
                    <li><a data-toggle="tab" href="#addaccount" id="addNewBackAccount">Add New Account</a></li>
                </ul>
                <div class="tab-content" style="border: 1px solid rgb(204, 204, 204); padding: 30px;background-color:#fff;margin-bottom:10px">
                    <div id="savedaccounts" class="tab-pane fade in active">
                        <div class="row bank-account-list">
                            
                        </div>
                    </div>
                    <div id="addaccount" class="tab-pane fade addNewBackAccountForm">
                        
                    </div>
                </div>
              </div>
                <div class="org-detail">
                    <h4 class="hstyl_4">Organizer Contact details</h4>
			<div class="radio form-group col-sm-6 col-xs-12">
				<label>
				  <input type="radio" name="organiser[type]"  value="userprofile" id="userasorganiser" <?=(($event['organiser']['type'] == 'userprofile')?'checked':'')?>>
				  Add Self Profile as organiser
				</label>
			</div>
			<div class="radio form-group col-sm-6 col-xs-12">
				<label id="showpagelist">
				  <input type="radio" name="organiser[type]"  value="pageprofile" readonly="readonly" <?=(($event['organiser']['type'] == 'pageprofile')?'checked':'')?>>
				  Add Affiliate Page as organiser
				</label>
			</div>
                    <div class="row">
                        <div class="col-sm-12  form-group "><input type="text" id="organisername" name="organiser[name]" class="form-control" required placeholder="Organizer's Name" value="<?=$event['organiser']['name']?>"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6  form-group "><input type="email" id="organiseremail" name="organiser[email]" class="form-control" required placeholder="Email Id"  value="<?=$event['organiser']['email']?>"></div>
                        <div class="col-sm-6  form-group "><input type="text" id="organisercontactno" name="organiser[cntnumber]" class="form-control" required placeholder="Contact Number"  value="<?=$event['organiser']['cntnumber']?>"></div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12  form-group ">
                            <textarea class="form-control" id="organiseraddress" name="organiser[address]"  rows="5" cols="40" placeholder="Postal Address"><?=$event['organiser']['address']?></textarea>
			    <input type="hidden" name="organiser[id]" id="organiserid" value="<?=$event['organiser']['id']?>" /> 
			</div>
                    </div>

               </div>          
              
              <div class="submitform hide" <?=(!empty($orgevent)?'style="display:block !important"':'')?>>
                <div class="row hide terms <?=(!empty($orgevent)?'hide':'')?>">
                    <div class="col-sm-12 checkbox">
                        <label><input type="checkbox" name="tncPayment" <?=(!empty($orgevent)?'checked readonly':'')?>>I accept <a href="terms#payment" style="color:#007ea4" target="_blank">terms &amp; conditions</a> of Passionstreet.in payment gateway usage.</label>
                    </div>
                </div>
				
                <div class="row">
                    <div class="col-sm-12 text-center">
			 <button class="btn btn-primary"  type="submit" name="step1" value="step1">NEXT</button>
                    </div>
                </div>

                
               </div>
            </form>
        </div>
    </div>
    <?php
    }
    if($editmode!=1 || ($editmode==1 && $showform['step2']))
    {?>
    <div  role="tabpanel" class="col-md-12 row tab-pane <?=($showform['step2'])?$showform['step2'].' fade active in':'hide'?> " id="step2">
        <div class="form-wrapper">
            <form class="form1 event-manage" method="post" id="form-step2"  action="<?=ROOT_PATH.'events/publish/'.$event['event_id']?>">
                <div class=" row hide">
                    <label class="col-sm-6 col-xs-12 form-group  control-label"><h4 class="hstyl_4">Event participation</h4></label>
                    <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_visibility" value="open-to-all" class="event_visibility" <?=(($event['visibility'] == 'open-to-all')?'checked':'checked')?>>
                          Open to all
                        </label>
                     </div>
		    <?php
		    /* ?><div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_visibility" value="open-to-all" class="event_visibility">
                          Open to all
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_visibility" value="invite-only" id="invitebtn" class="event_visibility">
                          By invitation
                        </label>
                     </div>
                    <div class="form-group row hide" id="invitebox">
                        <div class="col-sm-12 ">
                            <div id="inviteboxinner"></div>
                        </div>
                    </div>
		    <?php */ ?>
               </div>
                
               <div class=" row">
                    <label class="col-sm-6 col-xs-12 form-group  control-label"><h4 class="hstyl_4">Is there any selection criteria ?</h4></label>
                    <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_precondition"  value="Yes" id="event_precondition" <?=(($event['precondition'] == 'yes')?'checked':'')?>>
                          Yes
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_precondition"  value="No">
                          No
                        </label>
                     </div>
               </div>
	       <div class="form-group row hide" id="preconditionbox">
			<div class="col-sm-12 ">
				<div id="preconditioninner"></div>
			</div>	
		</div>
               
                <div class="row">
                    <div class="col-sm-12 text-center">
					    <button class="btn btn-primary"  type="submit" name="step2" value="step2">NEXT</button> <a class="btn " href="<?=(str_replace('step2','step1',$_SERVER['PHP_BROWSER_URL']))?>">BACK</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
    }
    if($editmode!=1 || ($editmode==1 && $showform['step3']))
    {?>
    <div  role="tabpanel" class="col-md-12 row tab-pane <?=($showform['step3'])?$showform['step3'].' fade active in':'hide'?>" id="step3">
        <div class="form-wrapper">
            <form class="form1 event-manage"  method="post" id="form-step3"  action="<?=ROOT_PATH.'events/publish/'.$event['event_id']?>">
                <div class=" row">
                    <label class="col-sm-6 col-xs-12 form-group  control-label"><strong>Do you want to include route map?</strong></label>
                    <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" value="Yes" class="event_include_routemap"  name="event_include_routemap">
                          Yes
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" value="No" class="event_include_routemap"  name="event_include_routemap">
                          No
                        </label>
                     </div>
               </div>
                <div class="row event_routemap hide">
                    <div class="col-sm-12 form-group">
                        <?php
			/* ?>
			<div class="form-group row ">
                        	<div class="col-sm-4">
                        	     <input type="text" id="map-input" placeholder="Add Point Of Interest 1 (POI)" class="form-control">
                        	</div>
                        	<div class="col-sm-4">
                        	     <input type="text" id="map-input2" placeholder="Add Point Of Interest 2 (POI)" class="form-control">
                        	</div>
                        	<div class="col-sm-4">
                        	     <input type="text" id="map-input3" placeholder="Add Point Of Interest 3 (POI)" class="form-control">
                        	</div>
                        </div>
                        <div class="form-group row ">
                             <div class="col-sm-12 text-right">
                              <span class="add-more">Add more POI</span>
                            </div>
                        </div>
			<?php */ ?>
			<div class="form-group row ">
                        	<div class="col-sm-12">
                        	     <input type="text" id="routemapinput" placeholder="Search Point of Interest" class="form-control">
                        	</div>
			</div>
                        <div class="row " id="waypoints">
                        </div>
                        <div class="row form-group">
                        	<div class="col-sm-12"><input type="button" style="margin:0;" class="btn btn-primary" id="btn_gen_route" value="Generate Route" /></div>
                        </div>
                        <input type="hidden" class="form-group " id="route_pic" name="route_pic" value="" />
                        <div id="routeMap" style="height:350px;width:100%; border:1px solid #ccc;">
                        </div>
                    </div>
                </div>
                <!-- <div class="row publish">
                    <label class="col-sm-6 col-xs-12 form-group  control-label">Do you want to Publish this event in</label>
                    <div class="col-sm-6 col-xs-12 socialbar_2 circle">                                   
                        <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="twtr"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="gplus"><i class="fa fa-google-plus"></i></a>
                     </div>                            
               </div> -->                     
                
                <div class=" row">
                    <label class="col-sm-6 col-xs-12 form-group  control-label">User engagement</label>
                    <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="allowuserpost"  value="Allow">
                          Allowed
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="allowuserpost"  value="Moderate">
                          Moderated
                        </label>
                     </div>
               </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button class="btn btn-primary" type="submit" name="step3" value="step3">PUBLISH</button> <a class="btn " href="<?=(str_replace('step3','step2',$_SERVER['PHP_BROWSER_URL']))?>">BACK</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
    }
    ?>
</div>
<?php
if($showform['step3'] && $editmode != 1)
{
// $PSJsincludes['external'][] = "https://maps.googleapis.com/maps/api/js?key=".$PSParams['google_maps_key']."&libraries=places&callback=initGoogleMapsRouteMapCreate";
$PSJsincludes['external'][] = "https://maps.googleapis.com/maps/api/js?key=".$PSParams['google_maps_key']."&libraries=places";
}
?>
<?php
$PSJsincludes['internal'][] = "js/eventpublish.js";
//$PSJsincludes['external'][] = "js/jquery.datetimepicker.js";
//$PSCssincludes['external'][] = "css/jquery.datetimepicker.css";
$PSJsincludes['external'][] = "js/bootstrapdatetimepicker.min.js";
$PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
?>