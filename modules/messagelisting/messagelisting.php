<?php
if(!function_exists('messages')){
	function messages_get_list($fields,$refreshType = false , $cacheMeta = array('expiry'=>180)){
		global $connection,$PSData;
		
		if($refreshType !== true)
		return preprocessApi('messages_get_list', $fields , $refreshType,$cacheMeta);

		if($fields['extraquery'])
		$fields['extraquery'] .= '';
		else
		$fields['extraquery'] = ' sender = "'.$PSData['user']['profile_id'].'" OR recipient = "'.$PSData['user']['profile_id'].'" OR sender = "'.$PSData['user']['email'].'" OR recipient = "'.$PSData['user']['email'].'"';
		$queryParams = construct_list_params($fields);
		$messages = $connection->fetchAll("select * from messages ".$queryParams[0]." group by parent_message_id  order by auto desc",$queryParams[1]);
		return $messages;
	}
}
$messages = messages_get_list();
foreach($messages as $message){
	if($message['sender'] && $message['recipient'])
	{
		((validate_email($message['sender']))?($email_ids[get_standard_email($message['sender'])] = 1):($profile_ids[$message['sender']] = 1));
		((validate_email($message['recipient']))?($email_ids[get_standard_email($message['recipient'])] = 1):($profile_ids[$message['recipient']] = 1));
	}
}
$profilebyprofileid = array();
$profiles = $connection->fetchAll("select profile_id,fname,lname,email,userdp from profile where profile_id in ('".implode("','",array_keys($profile_ids))."')");
foreach($profiles as $profile)
{
	$profilebyprofileid[$profile['profile_id']] = $profile;
}
$profilebyemail = array();
$profiles = $connection->fetchAll("select profile_id,fname,lname,email,userdp from profile where email in ('".implode("','",array_keys($email_ids))."')");
foreach($profiles as $profile)
{
	$profilebyemailid[$profile['email']] = $profile;
}
$PSModData['messages'] = $messages;
$PSModData['profiles'] = array('byprofile'=>$profilebyprofileid,'byemail'=>$profilebyemailid);
?>