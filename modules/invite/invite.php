<?php
if(array_key_exists('invitetype',$_POST))
{
    extract($_POST);
    if($relatedtotype == 'event')
    $details = event_get_details($relatedto);
    if(empty($relatedtotype) || $details['isAdmin'] == 1)
    {
        if($invitetype == 'InviteByEmail')
        {
            $insertData['emaillist'] = preg_split('/\r\n|[\r\n]/', $emaillist);
            foreach($insertData['emaillist'] as $email){
                if(validate_email($email))
                {
                    $insertData = array();
                    $insertData['inviteid'] = generate_id("invite");
                    $insertData['invitee'] = $_SESSION['user']['profile_id'];
                    $insertData['invited'] = $email;
                    $insertData['relatedtotype'] = (empty($relatedtotype)?'':$relatedtotype);
                    $insertData['relatedto'] = (empty($relatedto)?'':$relatedto);
                    $insertData['dateadded'] = date("Y-m-d H:i:s");
                    invite_add($insertData);
                }
            }
            $module_display_style = "done";
        }
    }
    else
    $module_display_style = "not_authorized";
    
}
else if($_GET['relatedto'] && $_GET['relatedtotype'])
{
    if($_GET['relatedtotype'] == 'event')
    $details = event_get_details ($_GET['relatedto']);
    if($details['isAdmin'] != 1)
    {
        $module_display_style = "not_authorized";
    }
}
?>