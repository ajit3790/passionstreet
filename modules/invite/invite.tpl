<div class="widget invite_wdgt">
<!--<div class="widget-heading">
	<h2 class="hstyl_1 sm">Invite</h2>
</div> -->
<div class="bx-styl">
	
	<h3>Invite by adding the e-mail ids or connecting to your other social connect</h3>
	<div class="socialbar_1 inverse text-center circle clearfix">
		<a href="#" class="invite email"><i class="fa fa-envelope"></i></a>
		<a href="#" class="invite twtr inviteshare" data-type="tw"><i class="fa fa-twitter"></i></a>
		<a href="#" class="invite fb inviteshare"  data-type="fb"><i class="fa fa-facebook"></i></a>
		<a href="#" class="invite gplus inviteshare"  data-type="gg"><i class="fa fa-google-plus"></i></a>
	</div>
	<form id="email-share" class="form1" method="post">                       
             <div class="row form-group">
                <div class="col-md-12">
                  <input type="hidden" value="InviteByEmail" name="invitetype" />
                  <input type="hidden" name="relatedto" value="<?=$_GET['relatedto']?>" /><input type="hidden" name="relatedtotype" value="<?=$_GET['relatedtotype']?>" />
                  <textarea class="form-control" id="emaillist" name="emaillist" rows="2" cols="20" placeholder="You may enter multiple email id's coma seperated"></textarea>
              </div>
              
              </div>
              <div class="row">
                  <div class="col-md-12 text-right">
                  <button type="submit" name="invitesubmit" id="invitesubmit" class="btn btn-primary"  aria-expanded="false">Invite </button>
                  </div>
              </div>
	
	</form>
 
	
</div>
</div>
<script>
$(document).ready(function(){
    $("#emaillist").on('keyup',function(event){
        $temp = $(this).val();
        $temp = $temp.split(',').join('\n');
        $(this).val($temp);
    });
    $("#invitesubmit").on('click',function(event){
    	event.preventDefault();
    	if($.trim($("#emaillist").val()) != "")	
    	$(this).parents('form').submit();
    	else
    	alert('Please provide email ids');
    });
    $(".inviteshare").on('click',function(e){
        $images = ['invite1.jpg','invite2.jpg','invite3.jpg','invite4.jpg'];
        $metaobj = {};
        $metaobj['title'] = 'I am inviting you to join me on PASSIONSTREET';
        $metaobj['description'] = 'PASSIONSTREET is an online activity based networking community for collaborating with like minded passionate individuals, experts, groups and organizations, who share a common goal. It is also a place to host and participate in events & workshops, wellness programs, adventure tourism, sports activity campaign and much more.';
        $metaobj['image'] = root_path+'images/invite/'+$images[getRandomInt(0,3)];
        $metaobj['url'] = root_path;
        $metaobj['tags'] = [];
        $metaobj['tags'][0] = 'Invite';
        sharepage($(this).data('type'),$metaobj,true);
	e.preventDefault();
	e.stopPropagation();
    });
});
</script>