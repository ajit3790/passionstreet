<form method="post" enctype="multipart/form-data">
<div class="row">
    <label class="control-label col-md-12  form-group">Choose Invitation Type</label>
</div>
<div class="row">
    <div class="col-sm-6  col-xs-6  form-group "><label><input type="radio" required="" value="AllMyFollowers" name="invitetype" class="form-control">Invite All My Follower</label></div>
    <div class="col-sm-6  col-xs-6  form-group "><label><input type="radio" required="" value="InviteByEmail" name="invitetype" id="InviteByEmailBtn" class="form-control ">Invite By Email</label></div>
</div>
<div class="row hide" id="InviteByEmailBox">
    <div class="col-sm-12  form-group "><textarea name="emaillist" id="emaillist" placeholder="Add Multiple email on individual lines" required="" class="form-control"></textarea></div>
</div>
<div class="row">
    <div class="col-sm-12  form-group "><input type="hidden" name="relatedto" value="<?=$_GET['relatedto']?>" /><input type="hidden" name="relatedtotype" value="<?=$_GET['relatedtotype']?>" /><input type="submit" name="submit" class="form-control" id="invitesubmit" value="Send Invitation Mailer" /></div>
</div>
</form>
<script>
$("#InviteByEmailBtn").on("click",function(){
    $("#InviteByEmailBox").removeClass('hide');
});
$("#emaillist").on('keyup',function(event){
    $temp = $(this).val();
    $temp = $temp.split(',').join('\n');
    $(this).val($temp);
});
</script>