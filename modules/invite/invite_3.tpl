<div class="invite-widget">
    <div class="flipper">
        <div class="front">
            <h2>Invite friends</h2>
            <a href="#" class="invite fb inviteshare" data-type="fb"><i class="fa fa-facebook c-fb"></i></a>
            <a href="#" class="invite twtr inviteshare" data-type="tw"><i class="fa fa-twitter c-twtr"></i></a>
            <a href="#" class="invite gplus inviteshare" data-type="gg"><i class="fa fa-google-plus c-ggl"></i></a>
            <a href="#" class="invite email"><i class="fa fa-envelope"></i></a>
        </div>
        <div class="back">
            <span class="close">X</span> 
	    <form id="email-share" class="form1" method="post">
	    <input type="hidden" value="InviteByEmail" name="invitetype" />
	    <input type="hidden" name="relatedto" value="<?=$_GET['relatedto']?>" /><input type="hidden" name="relatedtotype" value="<?=$_GET['relatedtotype']?>" />
            <p>You may enter multiple email id's coma seperated</p>	
            <textarea class="form-control" id="emaillist2" name="emaillist" rows="2" cols="20" placeholder="You may enter multiple email id's coma seperated"></textarea>
            <button type="submit" name="invitesubmit" id="invitesubmit2" class="btn btn-primary"  aria-expanded="false">Invite </button>
	    </form>
        </div>
    </div> 
</div>
<script>
$(document).ready(function(){
	$(".module.invite .fa-envelope").click(function(event){
		event.preventDefault();
		$(".invite-widget").addClass("flip");	
	});
	$(".module.invite .back .close").click(function(event){
		event.preventDefault();
		$(".invite-widget").removeClass("flip");	
	});
	$(".inviteshare").on('click',function(e){
		$images = ['invite1.jpg','invite2.jpg','invite3.jpg','invite4.jpg'];
		$metaobj = {};
		$metaobj['title'] = 'Join me on PASSIONSTREET';
		$metaobj['description'] = 'PASSIONSTREET is an online activity based networking community for collaborating with like minded passionate individuals, experts, groups and organizations, who share a common goal. It is also a place to host and participate in events & workshops, wellness programs, adventure tourism, sports activity campaign and much more.';
		$metaobj['image'] = root_path+'images/invite/'+$images[getRandomInt(0,3)];
		$metaobj['url'] = root_path;
		$metaobj['tags'] = [];
		$metaobj['tags'][0] = 'Invite';
		sharepage($(this).data('type'),$metaobj,true);
		e.preventDefault();
		e.stopPropagation();
	});
	$("#emaillist2").on('keyup',function(event){
		$temp = $(this).val();
		$temp = $temp.split(',').join('\n');
		$(this).val($temp);
	});
	$("#invitesubmit2").on('click',function(event){
		event.preventDefault();
		if($.trim($("#emaillist2").val()) != "")	
		$(this).parents('form').submit();
		else
		alert('Please provide email ids');
	});
});
</script>

