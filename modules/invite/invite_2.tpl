<div class="bx-styl invite_wdgt">
    <div class="inner">
    <h3><i class="fa fa-plus"></i> Invite by adding the E-mail id's</h3>
    <form class="form1" method="post">
        <div class="row form-group">
            <div class="col-sm-12 ">
                <textarea id="emaillist" name="emaillist" placeholder="Add Multiple email on individual lines" required="" class="form-control"></textarea>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12 text-right">
                <input type="hidden" value="InviteByEmail" name="invitetype" />
                <input type="hidden" name="relatedto" value="<?=$_GET['relatedto']?>" /><input type="hidden" name="relatedtotype" value="<?=$_GET['relatedtotype']?>" />
                <input type="submit" name="submit" class="btn btn-primary btn-sm" id="invitesubmit" value="SEND INVITATION" />
            </div>
        </div>
    </form>

    <!--h3 class="text-center">Or Invite from your other social connect</h3>
    <div class="socialbar_1 text-center circle">
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-google-plus"></i></a>
    </div-->
    </div>
</div>
<script>
$(document).ready(function(){
    $("#emaillist").on('keyup',function(event){
        $temp = $(this).val();
        $temp = $temp.split(',').join('\n');
        $(this).val($temp);
    });
});
</script>