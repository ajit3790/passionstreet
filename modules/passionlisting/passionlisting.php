<?php
$temp['all'] = array();
$passionlist = passion_get_list($temp['all']);
$passionsrendered = array_keys($passionlist);
$passionsAll = array_keys($PSParams['PSCategories']);
$passionsyettorender = array_diff($passionsAll,$passionsrendered);
foreach($passionsyettorender as $value)
{
    $temp = array();
    $temp['passion'] = $value;
    $temp['eventcount'] = 0;
    $temp['amifollowingthis'] = '';
    $temp['totalmember'] = 0;
    $temp['follows'] = 0;
    $temp['isExpert'] = 0;
    $passionlist[$value] = $temp; 
}
$PSModData['passionlist'] = $passionlist;
$PSModData['headerlink'] = ROOT_PATH.'/streetofpassion';
$PSModData['headertext'] = 'Follow your passion';
?>