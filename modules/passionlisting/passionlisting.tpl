<?php
$p = $PSParams['PSCategories'];
?>
<div class="col-md-12">
     <h3>Explore Your Passion</h3> 
     	
<div class="container passion-list">
    <div class="card-styl_1">
        <?php 
        $i=0;
        foreach($passionlist as $passion)
        {
        $i++;
        if ($i%2==0)
        $alternateClass = 'alt';
        else
        $alternateClass = '';
        ?>
        <div class=" branch card-styl_1 clearfix <?=$alternateClass?>">
        <div class="branch-inner">	
            <figure>
                <a href="<?=$p[$passion['passion']]['link']?>">
                    <img class="unveil" alt="<?=$p[$passion['passion']]['name']?>" src="<?=DEFAULT_IMG?>" data-src="images/<?=$passion['passion'].'-sm.jpg'?>" >
                </a>
                <figcaption >
                    <span><a href="<?=$p[$passion['passion']]['link']?>"><?=$p[$passion['passion']]['name']?></a></span> 
                    <a href="<?=$p[$passion['passion']]['link']?>" class="btn-primary btn-sm pull-right"><?=(!empty($passion['amifollowingthis']))?'Following':'Follow'?></a>
                </figcaption>
            </figure>
                          
            <div class="data">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6"><span>Members: <strong><?=($passion['totalmember'])?$passion['totalmember']:0?></strong></span></div>
                    <div class="col-md-6 col-sm-6 col-xs-6"><span>Posts: <strong><?=($passion['postcount'])?$passion['postcount']:0?></strong></span></div>
                </div>    
                <div class="row">

                    <div class="col-md-6 col-sm-6 col-xs-6"><span>Experts: <strong><?=($passion['isExpert'])?$passion['isExpert']:0?></strong></span></div>
                    <div class="col-md-6 col-sm-6 col-xs-6"><span>Events &amp; Workshops: <strong><?=($passion['eventcount'])?$passion['eventcount']:0?></strong>     </span></div>
                </div>
                
            </div>
           </div>
        </div>
        <?php
        }
        ?>
    </div>
    
</div>
<h3>More to come</h3>	
</div>

<!--a href="<?=ROOT_PATH.'moduleAJX/'.$moduleName?>" id="next">next</a-->
<script>
$(document).ready(function(){
    if(!fndefined('cleverInfiniteScroll')){
        $temp2['cleverInfiniteScroll'] = setInterval(function(){
            if(fndefined('cleverInfiniteScroll')){
                clearInterval($temp2['cleverInfiniteScroll']);
                $('.passion-list').cleverInfiniteScroll({
                    contentsWrapperSelector: '.card-styl_1',
                    contentSelector: '.card-styl_1>div',
                    nextSelector: '#next',
                    loadImage: 'ajax-loader.gif'
                });
           };
        },300);  
    }
    else{
        $('.passion-list').cleverInfiniteScroll({
            contentsWrapperSelector: '.card-styl_1',
            contentSelector: '.card-styl_1>div',
            nextSelector: '#next',
            loadImage: 'ajax-loader.gif'
        });
    }
}); 
</script>
<?php
if($_GET['pagetype'] == 'passionlisting')
{
$breadcrumbs = array();
$breadcrumbs[] = array('name'=>'Home','url'=>ROOT_PATH);
$breadcrumbs[] = array('name'=>'Passions','url'=>ROOT_PATH.'streetofpassion');
$PSParams['breadcrumbs'] = $breadcrumbs;
}
?>