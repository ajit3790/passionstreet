<?php
$p = $PSParams['PSCategories'];
if(!empty($headertext))
{
	?>
	<div class="row">
	<div class="col-md-12">
	    <?php 
	    if($headerlink)
	    {
	    $h1 = '<a href="'.$headerlink.'">';
	    $h2 = '</a>';
	    }
	    echo '<h2 class="hstyl_1">'.$h1.$headertext.$h2.'</h2>';
	    ?>
	</div>
	</div>
	<?php
}
?>
<div class="row ">
<div class="col-md-12 carousel-wrapper ">
    <i class="fa fa-angle-left"></i>
    <i class="fa fa-angle-right"></i>
    <div class="inner" >
        <ul class="passioncarousel" >
        <?php 
        $i=0;
        foreach($passionlist as $passion)
        {
        $i++;
        if ($i%2==0)
        $alternateClass = 'alt';
        else
        $alternateClass = '';
        ?>
        <li class="col-md-4">
        <div class=" branch card-styl_1 clearfix <?=$alternateClass?>">
        <div class="branch-inner">	
            <figure>
                <a href="<?=$p[$passion['passion']]['link']?>">
                    <img class="unveil" alt="<?=$p[$passion['passion']]['name']?>" src="<?=DEFAULT_IMG?>" data-src="images/<?=$passion['passion'].'-sm.jpg'?>" style="margin-bottom:15px">
                </a>
                <figcaption >
                    <span><a href="<?=$p[$passion['passion']]['link']?>"><?=$p[$passion['passion']]['name']?></a></span> 
                    <a href="<?=$p[$passion['passion']]['link']?>" class="btn-primary btn-sm pull-right"><?=(!empty($passion['amifollowingthis']))?'Following':'Follow'?></a>
                </figcaption>
            </figure>
            <?php
	    /*
	    ?>
            <div class="data">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6"><span>Members: <strong><?=($passion['totalmember'])?$passion['totalmember']:0?></strong></span></div>
                    <div class="col-md-6 col-sm-6 col-xs-6"><span>Posts: <strong><?=($passion['postcount'])?$passion['postcount']:0?></strong></span></div>
                </div>    
                <div class="row">

                    <div class="col-md-6 col-sm-6 col-xs-6"><span>Experts: <strong><?=($passion['isExpert'])?$passion['isExpert']:0?></strong></span></div>
                    <div class="col-md-6 col-sm-6 col-xs-6"><span>Events &amp; Workshops: <strong><?=($passion['eventcount'])?$passion['eventcount']:0?></strong>     </span></div>
                </div>
                
            </div>
           </div>
	   <?php
	   */
	   ?>
        </div>
        </li>
        <?php
        }
        ?>
        </ul>
    </div>
</div>

</div>