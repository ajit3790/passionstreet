<style>
.alert-message
{
    margin: 20px 0;
    padding: 20px;
    border-left: 3px solid #eee;
    background-color: #F4FDF0;
    border-color: #3C763D;
}

.alert-message h4
{
    margin-top: 0;
    margin-bottom: 5px;
    color: #3C763D;
}
.alert-message p:last-child
{
    margin-bottom: 0;
}
.alert-message code
{
    background-color: #fff;
    border-radius: 3px;
}
.alert-message-default
{
    background-color: #EEE;
    border-color: #B4B4B4;
}
.alert-message-default h4
{
    color: #000;
}
</style>
<?php
foreach($messages as $message){
?>
<div class="row">
	<div class="col-sm-10 col-md-10 <?=(!($message['sender'] == $PSData['user']['profile_id'] || $message['sender'] == $PSData['user']['email'])?'pull-right':'')?>">
	    <div class="alert-message ">
		<h4>
		<?php
		if(validate_email($message['sender']))
		{
		$message['sender'] = get_standard_email($message['sender']);
		}
		echo ($profiles['byprofile'][$message['sender']])?($profiles['byprofile'][$message['sender']]['fname']):($profiles['byemail'][$message['sender']]?$profiles['byemail'][$message['sender']]['fname']:$message['sender']);
		?>
		</h4>
		<p>
		<?=$message['message']?>
		<?php
		if($message['attachments'])
		echo '<br /><img src="'.UPLOAD_PATH.$message['attachments'].'" /><br />';
		?>
		<span class="pull-right time reltime" data-time="<?=strtotime($message['dateadded'])?>"></span>
		</p>
	    </div>
	</div>
</div>
<?php
}
render_module('contactform');
?>