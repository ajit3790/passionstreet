<?php
//<script src='https://www.google.com/recaptcha/api.js'></script>
?>
<script src="js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<form method="post" enctype="multipart/form-data">
<div class="row">
    <div class="col-sm-12 form-group ">
	<input type="text" placeholder="Recipient" value="<?=($_GET['toname'])?$_GET['toname']:'PASSIONSTREET'?>" readonly="" required=""  name="recipientname" class="form-control">
	<input type="hidden" placeholder="Recipient" value="<?=(($_GET['toprofile'] == 'PS' || empty($_GET['toprofile']))?'social@passionstreet.in':$_GET['toprofile'])?>" readonly="" required=""  name="recipientprofile" class="form-control">
    </div>
</div>
<div class="row">
    <div class="col-sm-12 form-group ">
	<input type="text" placeholder="Sender" value="<?=$PSData['user']['fullname']?>" readonly="" required=""  name="sendername" class="form-control">
	<input type="hidden" placeholder="Sender" value="<?=$PSData['user']['profile_id']?>" readonly="" required=""  name="senderprofile" class="form-control">
    </div>
</div>
<div class="row">
    <div class="col-sm-12 form-group "><textarea class="form-control" required="" name="messagebox" placeholder="Write Message Here"></textarea></div>
</div>
<div class="row">
    <div class="col-sm-12 form-group ">
        <input name="attachments" id="attachments" type="file" />
    </div>
</div>
<div class="row hide">
    <div class="col-sm-12 form-group ">
        <div class="g-recaptcha" style="height:80px" data-sitekey="6Le0CRATAAAAACFI2qNFo0hn9uUR3lzMXmTyNkBh"></div>
    </div>
</div>    
<div class="row">
    <div class="col-sm-12  form-group text-center">
	<input type="hidden" name="parent_message_id" value="<?=$_GET['parent_message_id']?>" />
	<input type="submit" name="submit" class="btn btn-primary" value="Send Message" />
    </div>
</div>
</form>
<script type="text/javascript">
$('#attachments').filestyle({
    'icon' : false,
    'buttonText' : ' Browse',
    'placeholder': 'Upload Image as Attachment If Any'
});
</script>