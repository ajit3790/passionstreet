<?php
    $loggedInUserId = $_SESSION['user']['id'];
?>
<div class="row tab-header">
    <div class="col-md-12">           
        <?php
            $categories = PSUtils::passionCategory();
            if ($action == 'discover') {
                echo '<select class="filter form-control options discover-gy-cat">';
                echo '<option value="all" selected="">All Community</option>';
                foreach ($categories as $categoryId => $category) {
                    echo "<option value='$categoryId'>".$category["name"]."</option>";
                }
                echo '</select>';
            }
        ?>
        
        <div class="group-nav">
        <ul class="nav nav-pills full-width V2">
            <!-- <li class="<?php if ($action == 'joined'){ echo 'active';}?>">
                <a href="groups" data-toggle="<?php if ($action == 'home'){ echo 'tab';}?>">Your Groups</a>
            </li>
            <li class="<?php if ($action == 'manage'){ echo 'active';}?>" >
                <a href="groups/manage" data-toggle="<?php if ($action == 'manage'){ echo 'tab';}?>">Groups you Manage</a>
            </li> -->
            <?php
                if ($loggedInUserId > 0){
            ?>
            <li class="<?php if ($action == 'manage' || $action == 'joined'){ echo 'active';}?>" >
                <a href="communities/manage" data-toggle="<?php if ($action == 'manage'){ echo 'tab';}?>">My Community</a>
            </li>
            <?php
                }
            ?>
            <li class="<?php if ($action == 'discover'){ echo 'active';}?>">
                <a href="communities/discover" data-toggle="<?php if ($action == 'discover'){ echo 'tab';}?>">All Community</a>
            </li>

            <li role="presentation"><a href="community/create">Create New Community</a></li>
        </ul>
        </div>
    </div>
</div>
<?php
    if ($loggedInUserId >0 ){
?>
<div class="row">
  <div class="col-md-12">
    <div class="tab-content">
        <div role="tabpanel" class="row tab-pane fade active in" id="all">
            <div class="col-md-12 gp-scroll-box">                
                <div class="row card-styl_3 ajax-post-response">
                    <?php
                    $recordsCount = 0;
                    if (empty($groups) === false) {
                        $recordsCount = count($groups);
                        foreach ($groups as $key => $groupDetail) {
                            $groupIcon = ROOT_PATH.'files/'.$groupDetail['photo'];
                            $categoryId = $groupDetail['category'];
                            
                    ?>
                    <div class="col-md-4">
                        <div class="bx-styl clearfix">
                            <figure>
                                <a href="community/<?=$groupDetail['groupId']?>" title="">
                                    <img class="unveil" alt="<?php echo $groupDetail['name'];?>" src="<?=DEFAULT_IMG_HORIZONTAL?>" data-src="<?=$groupIcon?>">
                                </a>
                            </figure>
                            <div class="description">
                                <h4><a href="community/<?=$groupDetail['groupId']?>" title="<?php echo $groupDetail['name'];?>">
                                <?php echo $groupDetail['name'];?></a></h4>
                                <p> Category : <?php echo $categories[$categoryId]['name'];?></p>
                                <div class="group-stat"><span class="pull-right">Followers : <?=$groupDetail['followers']?></span>Members: <?=$groupDetail['totalMembers']?></div>
                            </div>
                            <?php
                                if ($action == 'discover') {
                            ?>
							<div class="footer">
                            	<div class="btn-group">
                                	<button class="btn btn-primary join-group" data-id="<?php echo $groupDetail['groupId'];?>">Be A Member</button>
                                </div>
                                <?php
                                    if($groupDetail['follow'] === true) {
                                ?>
                                <div class="btn-group pull-left">
                                    <button class="btn btn-primary follow-action" data-action="0" data-id="<?php echo $groupDetail['groupId'];?>">Unfollow</button>
                                </div>
                                <?php
                                    } else {
                                ?>
                                <div class="btn-group pull-left">
                                    <button class="btn btn-primary follow-action" data-action="1" data-id="<?php echo $groupDetail['groupId'];?>">Follow</button>
                                </div>
                                <?php
                                    }
                                echo '</div>';
                                } else {
                                ?>
                                <div class="footer">
                                    <div class="btn-group">
                                        <a href="community/<?=$groupDetail['groupId']?>" class="btn btn-primary">Visit Community</a>
                                        <?php
                                            if ($groupDetail['hasEditPermission'] === true) {
                                        ?>
                                        <a href="community/<?=$groupDetail['groupId'];?>/edit" class="btn btn-primary" style="margin-left:5px;">Edit</a>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            <?php 
                                }
                            ?>
                        </div>
                    </div>
                    <?php
                        }
                        if ($recordsCount == 6) {
                    ?>
                    <span style="text-align:center"><a href="javascript:void(0);" data-nextlink="<?=$nextlink?>" id="nextGroupList" class="viewmorebtn">&nbsp;</a></span>
                    <?php
                        }
                    } else if($page == 1) {
                        echo '<div class="col-md-12"><div class="no-group-created">No community found <a href="community/create">Create your own Community</a></div></div>';
                    }
                    ?>
                </div>
            </div>
        </div>    
    </div>
  </div>
</div>
<?php
    }
?>
<script type="text/javascript">
    $(document).on('change', '.discover-gy-cat', function(){
        var categoryId = $(this).val();
        var userId = '<?php echo $loggedInUserId;?>';
        $.ajax({
            url: root_path+'AjaxGroup.php',
            type: "POST",
            data: 'requestType=groupDiscoverList&category='+categoryId+'&user='+userId,
            success: function(data) {
                var discoveredGroup = jQuery.parseJSON(data);
                if (discoveredGroup.result == '') {
                    var noData = '<div class="col-md-12"><div class="no-group-created">No community found <a href="community/create">Create your own Community</a></div></div>';
                    $('.ajax-post-response').html(noData);    
                } else {
                    $('.ajax-post-response').html(discoveredGroup.result);
                }
            },
            error: function(res) {
            }
        });
    });
</script>
<script>
$(document).ready(function(){
    delayfunction2(['groupwall.js','cleverinfinitescroll.js'],'groupsPaging','',1000);
});
</script>
<?php
$PSJsincludes['external'][] = "js/bootbox.min.js";
$PSJsincludes['external'][] = "js/groups.js";
$PSJsincludes['external'][] = "js/groupwall.js";
$PSJsincludes['external'][] = "js/cleverinfinitescroll.js";
?>
