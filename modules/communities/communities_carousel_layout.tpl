<div class="event-listing-v2">
    <div class="widget-heading clearfix">
       <h2 class="hstyl_1 sm">My Communities</h2>    
       <button class="btn btn-primary">Create Community</button>    
       <select class="filter form-control options">
            <option value="all" selected="">All</option>
            <?php
                foreach($PSParams['PSCategories'] as $PSCategorykey=>$PSCategory) {
            ?>
                <option value="<?=$PSCategorykey?>"><?=$PSCategory['name']?></option>
            <?php 
                }
            ?>
        </select>
        <form class="form search-form" role="search" method="GET">
            <div class="input-group add-on">
                 <input class="form-control" placeholder="Search" name="search" id="" type="text">
                 <div class="input-group-btn">
                 <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                 </div>
            </div>
        </form>
    </div>

    <!-- <div class="bx-styl"> </div> -->
    <!-- Start Previous list -->
    <div class="bx-styl">
        <div class="row card-styl_3">
            <div class="col-md-12 card-styl_1 sm carousel-wrapper">
                <i class="fa fa-angle-left"></i>
                <i class="fa fa-angle-right"></i>
                <div class="inner">
                    <ul class="eventcarousel" data-maxslides=3 data-slidewidth=<?=($PSParams['isMobile'])?320:350?>>
                        <?php                        
                        $recordsCount = 0;
                        if (empty($groups) === false) {
                            $recordsCount = count($groups);
                            foreach ($groups as $key => $groupDetail) {
                                $groupIcon = ($groupDetail['photo'] != '') ? ROOT_PATH.'files/'.$groupDetail['photo'] : DEFAULT_IMG_HORIZONTAL;
                                $categoryId = $groupDetail['category'];
                                $groupUrl = ROOT_PATH.'community/'.$groupDetail['groupId'];
                        ?>
                        <div class="col-md-4">
                            <div class="bx-styl clearfix" >
                                <figure>
                                    <a href="<?=$groupUrl?>" title="<?=$groupDetail['name']?>">
                                        <img class="unveil" alt="<?=$groupDetail['name']?>" src="<?=DEFAULT_IMG_HORIZONTAL?>" data-src="<?=$groupIcon?>">
                                    </a>                                    
                                    <div class="socialbar_1 circle"></div>
                                </figure>                                
                                <div class="description">
                                    <mark>Members <?=$groupDetail['totalMembers']?></mark>
                                    <mark>Followers <?=$groupDetail['followers']?></mark>
                                    <mark>Category <?=$categories[$categoryId]['name']?></mark>
                                    <h4>
                                        <a href="<?=$groupUrl?>" title="<?=$groupDetail['name']?>">
                                            <?=$groupDetail['name']?>
                                        </a>
                                    </h4>
                                    <p><?=$groupDetail['description']?></p>
                                </div>                                
                                <div class="footer">
                                    <div class="btn-group">
                                        <a href="<?=$groupUrl?>" class="btn btn-primary">Visit Community</a>
                                        <?php
                                            if ($groupDetail['hasEditPermission'] === true) {
                                        ?>
                                        <a href="<?=$groupUrl?>/edit" class="btn btn-primary" style="margin-left:5px;">Edit</a>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="event-listing-v2">
    <div class="widget-heading clearfix">
       <h2 class="hstyl_1 sm">All Communities</h2>    
       
    </div>

    <!-- <div class="bx-styl"> </div> -->
    <!-- Start Previous list -->
    <div class="bx-styl">
        <div class="row card-styl_3">
            <div class="col-md-12 card-styl_1 sm carousel-wrapper">
                <i class="fa fa-angle-left"></i>
                <i class="fa fa-angle-right"></i>
                <div class="inner">
                    <ul class="" data-maxslides=3 data-slidewidth=<?=($PSParams['isMobile'])?320:350?>>
                        <?php                        
                        $recordsCount = 0;
                        if (empty($groups) === false) {
                            $recordsCount = count($groups);
                            foreach ($groups as $key => $groupDetail) {
                                $groupIcon = ($groupDetail['photo'] != '') ? ROOT_PATH.'files/'.$groupDetail['photo'] : DEFAULT_IMG_HORIZONTAL;
                                $categoryId = $groupDetail['category'];
                                $groupUrl = ROOT_PATH.'community/'.$groupDetail['groupId'];
                        ?>
                        <div class="col-md-4">
                            <div class="bx-styl clearfix" >
                                <figure>
                                    <a href="<?=$groupUrl?>" title="<?=$groupDetail['name']?>">
                                        <img class="unveil" alt="<?=$groupDetail['name']?>" src="<?=DEFAULT_IMG_HORIZONTAL?>" data-src="<?=$groupIcon?>">
                                    </a>                                    
                                    <div class="socialbar_1 circle"></div>
                                </figure>                                
                                <div class="description">
                                    <mark>Members <?=$groupDetail['totalMembers']?></mark>
                                    <mark>Followers <?=$groupDetail['followers']?></mark>
                                    <mark>Category <?=$categories[$categoryId]['name']?></mark>
                                    <h4>
                                        <a href="<?=$groupUrl?>" title="<?=$groupDetail['name']?>">
                                            <?=$groupDetail['name']?>
                                        </a>
                                    </h4>
                                    <p><?=$groupDetail['description']?></p>
                                </div>                                
                                <div class="footer">
                                    <div class="btn-group">
                                        <button class="btn btn-primary join-group" data-id="<?php echo $groupDetail['groupId'];?>">Be A Member</button>
                                    </div>
                                    <?php
                                        if($groupDetail['follow'] === true) {
                                    ?>
                                    <div class="btn-group pull-left">
                                        <button class="btn btn-primary follow-action" data-action="0" data-id="<?php echo $groupDetail['groupId'];?>">Unfollow</button>
                                    </div>
                                    <?php
                                        } else {
                                    ?>
                                    <div class="btn-group pull-left">
                                        <button class="btn btn-primary follow-action" data-action="1" data-id="<?php echo $groupDetail['groupId'];?>">Follow</button>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>