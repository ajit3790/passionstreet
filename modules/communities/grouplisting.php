<?php
global $connection;

$userId = $_SESSION['user']['id'];
$actionName = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'joined';

$_REQUEST['page'] = isset($_REQUEST['page']) === true ? $_REQUEST['page'] : 1;
$perpage= 6;
$offset = ($_REQUEST['page']-1) * $perpage;

if ($actionName == 'discover') {
	// find group by gooup's category
	//$groups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId GROUP BY gm.groupId ) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId != ? GROUP BY gm.groupId) AS temp_2 ON temp_2.totalMembers = temp_1.totalMembers AND temp_1.groupId = temp_2.groupId  LIMIT ?, ?", array($_SESSION['user']['id'], $offset, $perpage));

	$groups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId WHERE gm.isMember = '1' GROUP BY gm.groupId ) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId != ? WHERE isMember = '1' GROUP BY gm.groupId) AS temp_2 ON temp_2.totalMembers = temp_1.totalMembers AND temp_1.groupId = temp_2.groupId  LIMIT ?, ?", array($_SESSION['user']['id'], $offset, $perpage));
	
	foreach ($groups as $key => $group) {
		$groups[$key]['hasEditPermission'] = false;
		$groups[$key]['follow'] = false;

		$followers = $connection->fetchAssoc("SELECT count(1) AS followers FROM group_members WHERE groupId =?", array($group['groupId']));
		$groups[$key]['followers'] = isset($followers['followers']) ? $followers['followers'] : 0;

		$userData = $connection->fetchAssoc("SELECT * FROM group_members WHERE groupId =? AND userId =?", array($group['groupId'], $userId));
		if (isset($userData['isMember']) === true) {
			$groups[$key]['follow'] = true;
		}
	}
} else {
	// $groups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId GROUP BY gm.groupId) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId = ? GROUP BY gm.groupId) AS temp_2 ON temp_1.groupId = temp_2.groupId LIMIT ?, ?", array($userId, $offset, $perpage));

	$groups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId WHERE gm.isMember = ? GROUP BY gm.groupId) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId = ? WHERE gm.isMember = ? GROUP BY gm.groupId) AS temp_2 ON temp_1.groupId = temp_2.groupId LIMIT ?, ?", array('1', $userId, '1',$offset, $perpage));

	foreach ($groups as $key => $group) {
		$followers = $connection->fetchAssoc("SELECT count(1) AS followers FROM group_members WHERE groupId =?", array($group['groupId']));
		$groups[$key]['followers'] = $followers['followers'];
		$groups[$key]['follow'] = true;

		$userData = $connection->fetchAll("SELECT * FROM group_members WHERE groupId =? AND userId =?", array($group['groupId'], $userId));
		$groups[$key]['hasEditPermission'] = false;
		if (isset($userData['role']) === true && ($userData['role'] == '1' || $userData['role'] == '2')) {
			$groups[$key]['hasEditPermission'] = true;
		}
	}
}

$PSModData['nextlink'] = ROOT_PATH.'moduleAJX/grouplisting?page='.($_REQUEST['page']+1).'&action='.$actionName;

$PSModData['page'] 	 = $_REQUEST['page'];
$PSModData['groups'] = $groups;
$PSModData['action'] = $actionName;