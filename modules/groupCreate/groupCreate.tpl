<?php
  if (isset($group['groupId']) === true) {
    $groupId = $group['groupId'];
  } else {
    $groupId = 0;
  }
  $showJoiningRequestTab = true;
  if ($groupId > 0 && $group['privacyType'] == 'closed') {
    $showJoiningRequestTab = false;
  }
?>
<div class="col-md-12">
  <div class="row">
    <div class="col-md-12">
        <div class="form-wrapper">   
          <div class="row">
	      <div class="col-md-12">
	          <h2 class="hstyl_2 "><span><i class="fa fa-pencil-square-o"></i><?php if ($groupId > 0) { echo 'Edit Community Permission';} else { echo 'Create New Community';}?></span></h2>
	      </div>
	  </div>	   
          <form class="form1 group-creation" action="" method="post" enctype="multipart/form-data">
            <div class="row">
              <div class="col-sm-6 col-md-4">
                <label>Community Name</label>
              </div>              
              <div class="col-sm-6 col-md-8 form-group ">
                <input type="text" name="name" value="<?=isset($group['name']) ? $group['name'] : ''?>" class="form-control" id="groupName" placeholder="Community Name">
              </div>
            </div>            

            <div class="row form-group">
              <div class="col-sm-6 col-md-4">
                <label>Community Category</label>
              </div>
              <div class="col-sm-6 col-md-8 ">
                <?php
                  $category = (isset($group['category']) === true) ? $group['category'] :'';
                  if ($category != '') {
                    $categoryData = $psCategories[$category];
                ?>
                <label class="group-category">
                  <input name="groupType" checked="true" value="<?php echo $category;?>" type="radio">
                  <img src="images/<?php echo $categoryData['icon'];?>" id="categoryImage" alt="<?php echo $categoryData['label'];?>" width="45px;">
                  <span><?php echo $categoryData['name'];?></span>
                </label>
                <button class="btn btn-xs changeGroupCategory" type="button" data-action="1">Pick a Community Category</button>
                <?php
                  } else {
                ?>
                  <button class="btn btn-xs changeGroupCategory" type="button" data-action="0">Pick a Community Category</button>
                <?php
                  }
                ?>
                <label class="required-error category-error"></label>
              </div>
            </div>

            <div class="row form-group eventTypeRow">
              <div class="col-sm-6 col-md-4">
                <label>Privacy Settings</label>
              </div>
              <div class="col-sm-6 col-md-8">
                <select name="privacyType" id="groupPrivacy" class="form-control">
                  <option value="">Privacy Settings</option>
                  <option value="public" <?php if($group['privacyType'] == 'public') { echo 'selected';} ?> >Public Community</option>
                  <option value="closed" <?php if($group['privacyType'] == 'closed') { echo 'selected';} ?>>Closed Community</option>
                </select>
              </div>
            </div>

            <div class="row form-group eventTypeRow">
              <div class="col-sm-6 col-md-4">
                <label>Community Photo</label>
              </div>
              <div class="col-sm-6 col-md-8">
                <input type="file" name="photo" croptype="eventbanner" class="multifilecrop">
              </div>
            </div>

            <div class="row form-group eventTypeRow">
              <div class="col-sm-6 col-md-4">
                <label>Community Description</label>
              </div>
              <div class="col-sm-6 col-md-8">
                <textarea placeholder="Brief description of community" name="description" id="description" class="form-control"><?=isset($group['description']) ? $group['description'] : ''?></textarea>
              </div>
            </div>
            
            <!-- <input type="text" class="input-ui"> -->

            <?php if (isset($group['groupId']) === false) { ?>
            <div class="row">
              <div class="col-sm-6 col-md-4">
                <label>Tag Members</label>
              </div>              
              <div class="col-sm-6 col-md-8 form-group ">
                <input type="text" name="members" id="auto-add-member" class="auto-add-member-1 form-control" placeholder="Tag Members">
                <input type="hidden" name="membersId" id="auto-add-member-id">
                <input type="hidden" name="membersEmid" id="auto-add-member-emid">
              </div>
            </div>
            <?php } ?>

            <div class="row form-group">
              <div class="col-sm-6 col-md-4">
                <label>Posting Permission</label>
              </div>
              <div class="col-sm-6 col-md-8">                
                <?php

                  foreach ($postingSettings as $keyId => $label) {
                    $checked = '';
                    if (isset($group['postingPermission']) === true && $keyId == $group['postingPermission']) {
                      $checked = 'checked';
                    } elseif ($keyId == 1) {
                      $checked = 'checked';
                    }
                    echo "<div class='radio'><label><input type='radio' name='postingPermission' value='$keyId' $checked > $label</label></div>";  
                  }
                ?>
              </div>
            </div>

            <div class="row form-group <?=($showJoiningRequestTab=== false)? 'hide': ''?> joining-sec">
              <div class="col-sm-6 col-md-4">
                <label>Joining Request</label>
              </div>
              <div class="col-sm-6 col-md-8">                
                <?php
                  foreach ($joiningSettings as $keyId => $label) {
                    $checked = '';
                    if (isset($group['joiningPermission']) === true && $keyId == $group['joiningPermission']) {
                      $checked = 'checked';
                    } elseif ($keyId == 1) {
                      $checked = 'checked';
                    }
                    $disabled = ($showJoiningRequestTab=== false)? 'disabled': '';
                    echo "<div class='radio'><label><input $disabled type='radio' name='joiningPermission' value='$keyId' $checked class='joiningPermission'> $label</label></div>";  
                  }                  
                ?>
              </div>
            </div>

            <div class="row form-group">
              <div class="col-sm-6 col-md-4">
                <label>Add / invite members</label>
              </div>
              <div class="col-sm-6 col-md-8">                
                <?php
                  foreach ($inviteSettings as $keyId => $label) {
                    $checked = '';
                    if (isset($group['invitePermission']) === true && $keyId == $group['invitePermission']) {
                      $checked = 'checked';
                    } elseif ($keyId == 1) {
                      $checked = 'checked';
                    }
                    echo "<div class='radio'><label><input type='radio' name='invitePermission' value='$keyId' $checked > $label</label></div>";  
                  }                  
                ?>
              </div>
            </div>

            <div class="row form-group">
              <div class="col-sm-6 col-md-4">
                <label>Event creation</label>
              </div>
              <div class="col-sm-6 col-md-8">                
                <?php
                  foreach ($eventCreationSettings as $keyId => $label) {
                    $checked = '';
                    if (isset($group['eventPermission']) === true && $keyId == $group['eventPermission']) {
                      $checked = 'checked';
                    } elseif ($keyId == 1) {
                      $checked = 'checked';
                    }
                    echo "<div class='radio'><label><input type='radio' name='eventPermission' value='$keyId' $checked > $label</label></div>";  
                  }                  
                ?>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6 col-md-4"></div>
              <div class="col-sm-6 col-md-4">
                <?php
                  if (isset($group['groupId']) === true) {
                     $groupId = isset($group['groupId']) ? $group['groupId'] : 0;
                     echo "<input type='hidden' name='groupId' value='$groupId' >";
                    echo '<button class="btn btn-primary update-group-btn" type="submit">Save Changes</button>';
                  } else {
                    echo '<button class="btn btn-primary create-group-btn" type="submit">Create Community</button>';
                  }
                ?>
              </div>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).on('click', '.changeGroupCategory', function(){
    // Change group category
    $("#groupCategoryPopup").modal({backdrop: 'static',keyboard: false});
  });

  $(document).on('click', '.category-confirm', function() {
    var optedCategory = $('input[name=optedCategory]:checked').val();
    
    if (optedCategory== undefined || optedCategory == '') {
      $('.categoryError').html('Please select a category');
      return false;
    }
    var categoryName = $('input[name=optedCategory]:checked').data('name');
    var categoryIcon = $('input[name=optedCategory]:checked').data('icon');

    $('.categoryError').html('');
    $('.category-error').html('');

    var isCategoryFilled = $('.changeGroupCategory').attr('data-action');
    $('.changeGroupCategory').attr('data-action', '1');
    if (isCategoryFilled == 0) {
      var categoryHtml = '<label class="group-category"><input name="groupType" checked="true" value="'+optedCategory+'" type="radio"><img src="images/'+categoryIcon+'" id="categoryImage" alt="'+optedCategory+'" width="45px;"><span></span></label>';
      $('.changeGroupCategory').before(categoryHtml);
    } else {
      $('#categoryImage').attr('src','images/'+categoryIcon);
      $('input[name=groupType]').val(optedCategory);
    }
    
    $('.changeGroupCategory').html('Change Community Category');
    $('.group-category span').html(categoryName);
    $('#groupCategoryPopup').modal('toggle');
  });
</script>

<!-- Select Category -->
<div id="groupCategoryPopup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Pick Community Category</h4>
      </div>
      <div class="modal-body">
        <div class="row experties">
          <?php
            foreach($psCategories as $PSCategorykey => $PSCategory) {
              $extraAttr = '';
              if($PSCategorykey == $category)
                $extraAttr = ' checked = "checked" readonly="readonly" ';
          ?>
          <div class="col-sm-2 col-xs-2">
            <label >
              <input name="optedCategory" data-icon="<?=$PSCategory['icon']?>" data-name="<?=$PSCategory['name']?>" class="optedCategory" <?=$extraAttr?> value="<?=$PSCategorykey?>" type="radio" />
              <img src="images/<?=$PSCategory['icon']?>" alt="<?=$PSCategory['name']?>" />
            </label>
          </div>
          <?php
            }
          ?>          
        </div>
        <label class='categoryError required-error'></label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary category-confirm">Confirm</button>
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="js/jquery-ui.js" ></script>

<script type="text/javascript">
  $(document).on('click', '.update-group-btn, .create-group-btn', function(){
    var isvalid = true;
    var groupId = "<?php echo $groupId;?>";
    if ($('#groupName').val() == '') {
      showToolTipMsg($('#groupName'),"Please fill in this field.");
      isvalid = false;
    }
    var isCategoryFilled = $('.changeGroupCategory').attr('data-action');
    if (isCategoryFilled == 0) {
      showToolTipMsg($('.changeGroupCategory'),"Please pick a group category.");
      isvalid = false;
    }
    if ($('#groupPrivacy').val() == '') {
      showToolTipMsg($('#groupPrivacy'),"Please select group privacy.");
      isvalid = false;
    }
    if ($('#description').val() == '') {
      showToolTipMsg($('#description'),"Please fill in this field.");
      isvalid = false;
    }

    if (groupId == 0 && $('#auto-add-member-id').val() == '') {
      showToolTipMsg($('#auto-add-member'),"Please tag members to join group.");
      isvalid = false;
    }
    return isvalid;
  });

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }

    $("#auto-add-member").autocomplete({
        source: function (request, response)  {
          $.ajax({
            url: root_path+"PSAutocomplete.php?type=tagMember",
            dataType: "json",
            data:{
              term: extractLast(request.term),
              taggedIds: $('#auto-add-member-id').val()
            },
            success: function (data) {
              response(data);
            }
          });
        },
        minLength: 1,
        select: function (event, ui) {
          var selectedId = $('#auto-add-member-id').val();
          var selectedEmid = $('#auto-add-member-emid').val();
          if (selectedId == '') {
            $('#auto-add-member-id').val(ui.item.id);
            $('#auto-add-member-emid').val(ui.item.email);
          } else {
            $('#auto-add-member-id').val(selectedId+','+ui.item.id);
            $('#auto-add-member-emid').val(selectedEmid+','+ui.item.email);
          }

          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }    
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<li></li>" )
        .data( "item.autocomplete", item)
        .append( "<a class='myclass' customattribute='" + item.value + "'> <img src='"+root_path+"profiledp' width='40px' height='40px'> " + item.value + "</a>" )
        .appendTo(ul);
    };
</script>
<script type="text/javascript">
  $(document).on('change', '#groupPrivacy', function(){
    var typeId = $(this).val();
    if (typeId == 'closed') {
      $('.joining-sec').hide();
      $('.joiningPermission').attr('disabled', false);
    } else {
      $('.joining-sec').show();
    }
  });
  
</script>
<?php
  $PSJsincludes['external'][] = "js/fengyuanchencropper/croppermain2.js";
  $PSJsincludes['external'][] = "js/fengyuanchencropper/cropper.js";
  $PSCssincludes['external'][] = "js/fengyuanchencropper/cropper.css";
?>