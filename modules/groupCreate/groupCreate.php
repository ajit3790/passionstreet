<?php
global $connection;
global $groupId;
global $groupDetail;

if (empty($_POST) === false) {
	$creatorId = isset($_SESSION['user']['id']) ? $_SESSION['user']['id'] : 0;

	$isNewGroup = true;
	if (isset($_POST['groupId']) === true && $_POST['groupId'] > 0) {
		$groupId 	= $_POST['groupId'];
		$isNewGroup = false;
	} else {
		$groupId = PSUtils::uuid();
	}
	$postParams = array(		
		isset($_POST['groupType']) ? $_POST['groupType'] : 'public',
		isset($_POST['name']) ? $_POST['name'] : '',
		'',
		isset($_POST['description']) ? $_POST['description'] : '',
		isset($_POST['privacyType']) ? $_POST['privacyType'] : 'public',
		isset($_POST['postingPermission']) ? $_POST['postingPermission'] : 4,
		isset($_POST['joiningPermission']) ? $_POST['joiningPermission'] : 0,
		isset($_POST['invitePermission']) ? $_POST['invitePermission'] : 3,
		isset($_POST['eventPermission']) ? $_POST['eventPermission'] : 1,
		$groupId,
	);
	
	if ($isNewGroup === true) {
		$postParams[] = $creatorId;
		$postParams[] = date('Y-m-d H:i:s');
		$isCreated = $connection->executeUpdate("INSERT INTO groups(category, name, photo, description, privacyType, postingPermission, joiningPermission, invitePermission, eventPermission, groupId, createdBy, createdAt) VALUES(?, ? ,? ,? ,? ,? ,? ,? ,?, ?, ?, ?)", $postParams);
		if ($isCreated === 1) {
			$membersId = explode(',', $_REQUEST['membersId']);
			$memberIds = array_unique($membersId);
			$connection->executeUpdate("INSERT INTO group_members(groupId, userId, role, isMember, isFollower, status) VALUES(?, ? ,? ,?, ?, ?)", array($groupId, $creatorId, '1', '1', '1', '1'));
			
			$sql = sprintf("SELECT concat(fname, ' ', lname) AS name, email FROM profile WHERE id IN(%s)", $_REQUEST['membersId']);
			$users = $connection->fetchAll($sql, array());

			foreach ($memberIds as $key => $memberId) {
				$connection->executeUpdate("INSERT INTO group_members(groupId, userId, role, isMember, isFollower, status) VALUES(?, ? ,? ,?, ?, ?)", array($groupId, $memberId, '0', '1', '1', '1'));
				
				$requesterName = $_SESSION['user']['fname'].' '.$_SESSION['user']['lname'];        
				$title = $requesterName.' has added you in a community:: '.$_POST['name'].' on PASSIONSTREET';
	            $desc = $requesterName.' has added you in a community <b>'.$_POST['name'].'</b> on PASSIONSTREET.';
	            
	            $img = ROOT_PATH.'userbg';

	            $desc .= "<tr><td style='padding-top:10px; padding-bottom:15px;line-height:22px'><img src='$img' width='100%'></td></tr>";
	            if ($_POST['description'] != '') {
	                $desc .= '<tr><td style="padding-bottom:8px;line-height:22px">'.$_POST['description'].'</td></tr>';
	            }
	            $groupUrl = ROOT_PATH."community/".$groupId;
	            $desc .= "<tr><td style='padding-bottom:10px;line-height:22px'><a href='$groupUrl' target='_blank' style='color:#1e7cdc;text-decoration:underline'> <span class='il'>Click here</span></a> to leave the community.</td></tr>";

	            $invitedToEmail = $users[$key]['email'];
	            mailer(array("profile_id"=>$memberId,"email_id"=>$invitedToEmail),'default', array('data'=>$desc,'title'=>$title));

			}
		}
	} else {
		unset($postParams[2]);
		$connection->executeUpdate("UPDATE groups SET category = ?, name = ?, description = ?, privacyType = ?, postingPermission = ?, joiningPermission = ?, invitePermission = ?, eventPermission = ? WHERE groupId = ? ", $postParams);
	}
	header('Location:'.ROOT_PATH.'community/'.$groupId);die;
}

$PSModData['psCategories'] = PSUtils::passionCategory();
$PSModData['group'] = $groupDetail;

$postingSettings = array(
	'1' => 'Anyone can post',
	'2' => 'Only members can post',
	'3' => 'Members can post but admin / moderators have to approve the post',
	'4' => 'Only Admin or moderator can post'
);

$joiningSettings = array(
	'1' => 'Anyone can join community',
	'2' => 'Anyone can request for joining for admin / moderator approval'
);

$inviteSettings = array(
	'1' => 'Any member can add other members',
	'2' => 'Any members can invite other members through an appoval by an admin or a moderator',
	'3' => 'Only admin / moderator can add / invite members',
);

$eventCreationSettings = array(
	'1' => 'Only admin / moderator can create event',
	'2' => 'Any member can create event but admin / moderators have to approve it'
);

$PSModData['postingSettings'] = $postingSettings;
$PSModData['joiningSettings'] = $joiningSettings;
$PSModData['inviteSettings'] = $inviteSettings;
$PSModData['eventCreationSettings'] = $eventCreationSettings;
?>