<div class="widget-heading mb-small">	
<a class="more-lnk pull-right" href="<?=$headerlink?>">
	View all <i class="fa fa-angle-double-right"></i>
</a>
<h2 class="hstyl_1 sm">
	<?=(!empty($headertext))?$headertext:'Events'?>
</h2>
</div>
<div class="tab-content mb-small bx-styl">
<div class="row card-styl_3">
<div class="col-md-12 card-styl_1 sm carousel-wrapper">
    <i class="fa fa-angle-left"></i>
    <i class="fa fa-angle-right"></i>
    <div class="inner">
	<ul class="eventcarousel" data-maxslides=4 data-slidewidth=<?=($PSParams['isMobile'])?320:350?>>
	    <?php
           foreach($eventslist['all'] as $event)
           {
		$datestr1 = '';
		$datestr2 = '';
		$eventdateData = getdate(strtotime($event['datestart']));
		?>
	    <div class="col-md-4">
                <div class="bx-styl clearfix" style="padding-bottom:50px;height:auto;">
                    <figure>
                            <a href="<?=ROOT_PATH?>event/<?=$event['seopath']?>"  title="<?=$event['eventname']?>"><img class="unveil" alt="<?=$event['eventname']?>" src="<?=DEFAULT_IMG_HORIZONTAL?>" data-src="<?=$event['eventpic']?>"></a>
                            
                            <div class="socialbar_1 circle">
			      <?php
			      $event['eventname'] = strip_tags($event['eventname']);
			      $event['description'] = strip_tags($event['description']);
			      ?>
	                      <a class="customshare fb" data-type="fb" data-title="<?=$event['eventname']?>" data-description="<?=$event['description']?>" data-image="<?=$event['eventpic']?>" data-shareurl="<?=ROOT_PATH?>event/<?=$event['seopath']?>"><i class="fa fa-facebook"></i></a>
	                      <a class="customshare twtr" data-type="tw"  data-title="<?=$event['eventname']?>" data-description="<?=$event['description']?>" data-image="<?=$event['eventpic']?>" data-shareurl="<?=ROOT_PATH?>event/<?=$event['seopath']?>"><i class="fa fa-twitter"></i></a>
	                      <a class="customshare gplus" data-type="gg" data-title="<?=$event['eventname']?>" data-description="<?=$event['description']?>" data-image="<?=$event['eventpic']?>"  data-shareurl="<?=ROOT_PATH?>event/<?=$event['seopath']?>"><i class="fa fa-google-plus"></i></a>
	                   </div>
                            
                    </figure>
                    <div class="date">
                        <span><?=$eventdateData['mday']?></span>
                        <span><?=ucfirst($PSParams['months'][$eventdateData['mon']])?></span>
                    </div>
                    <div class="description">
                    	<mark>EVENT</mark>
                        <h4><a href="event/<?=$event['seopath']?>" title="<?=$event['eventname']?>"><?=$event['eventname']?></a></h4>
                        <p><?=$event['description']?></p>
                    </div>
                    <div class="footer" >
                        <?php
                        $tdate = date("Y-m-d H:i:s");
                        ?>
                    	<a href="event/<?=$event['seopath']?>"  title="<?=$event['eventname']?>" class="btn btn-primary"><?=($event['source'] == 'aggregated')?'View Event':(($event['tickettype'] == 'paid' && $event['bookingenddate'] >=  $tdate)?'Join Now':'View Event')?></a>
                    	<?php
                        if($event['source'] == 'aggregated')
			echo 'Paid';
			else if($event['bookingenddate'] <  $tdate)
                        {
                        }
			else if($event['tickettype'] == 'paid')
			{
				$ticketvaluerangeArray = explode(',',$event['ticketvaluerange']);
				if($ticketvaluerangeArray[0] == $ticketvaluerangeArray[1])
				echo '&#8377 '.$ticketvaluerangeArray[0];
				else 
				echo '&#8377 '.$ticketvaluerangeArray[0].' onwards';
			}
			else
			{
				echo 'Free';
			}
			?>
                    </div> 
                </div>
            </div>
           <?php
           }
           ?>
	</ul>
    </div>
</div>
</div>
</div>