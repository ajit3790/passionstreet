
<div class="event-listing-v2">
    <div class="widget-heading clearfix">
       <h2 class="hstyl_1 sm">My Events</h2>	
       <button class="btn btn-primary">Create Event</button>	
       <select class="filter form-control options">
            <option value="all" selected="">All</option>
            <?php
            foreach($PSParams['PSCategories'] as $PSCategorykey=>$PSCategory)
            {?>
            <option value="<?=$PSCategorykey?>"><?=$PSCategory['name']?></option>
            <?php }
            ?>
        </select>	
        <form class="form search-form" role="search" method="GET">
            <div class="input-group add-on">
                 <input class="form-control" placeholder="Search" name="search" id="" type="text">
                 <div class="input-group-btn">
                 <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                 </div>
            </div>
        </form>
    </div>
    <div class="bx-styl">
    
    </div> 
 
 <div class="load-more">
 	<button class="btn btn-primary">View More Events</button>
 </div>
    
</div>
