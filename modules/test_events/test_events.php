<?php
$_GET['eventlistpage'] = (isset($_GET['eventlistpage']))?$_GET['eventlistpage']:1;
if($_GET['pagetype'] == 'eventListing')
{
    $temp['all'] = array();
    $temp['myevents'] = array("creator"=>$_SESSION['user']['profile_id'],"status"=>"all");

    $x = array();
    $PSModData['heading1'] = 'Upcoming events';
    $PSModData['heading2'] = 'My events';
	    
    if($_GET['eventlistby'] == 'passion')
    {
	    if($_GET['passion'] && $_GET['passion']!='all')
	    {
	    $x = array('category'=>$_GET['passion']);
	    $PSModData['heading1'] = 'Upcoming events in '.$PSParams['PSCategories'][$_GET['passion']]['name'].' category';
	    $PSModData['heading2'] = 'My events in '.$PSParams['PSCategories'][$_GET['passion']]['name'].' category';
	    $GLOBALS['page_meta']['title'] = $PSModData['heading1'];
	    }
    }
    else if($_GET['eventlistby'] == 'city')
    {
	    if($_GET['city'])
	    {
	    $_GET['city'] = ucwords(strtolower(trim($_GET['city'])));
	    $x = array('city'=>$_GET['city']);
	    $PSModData['heading1'] = 'Upcoming events in '.$_GET['city'];
	    $PSModData['heading2'] = 'My events in '.$_GET['city'];
	    $GLOBALS['page_meta']['title'] = $PSModData['heading1'];
	    }
    }
    else if($_GET['eventlistby'] == 'citypassion')
    {
	    if($_GET['city'] && $_GET['passion'])
	    {
	    $_GET['city'] = ucwords(strtolower(trim($_GET['city'])));
	    $x = array('city'=>$_GET['city'],'category'=>$_GET['passion']);
	    $PSModData['heading1'] = 'Upcoming '.$PSParams['PSCategories'][$_GET['passion']]['name'].' events in '.$_GET['city'];
	    $PSModData['heading2'] = 'My '.$PSParams['PSCategories'][$_GET['passion']]['name'].' events in '.$_GET['city'];
	    $GLOBALS['page_meta']['title'] = $PSModData['heading1'];
	    }
    }
    $temp['all'] = array_merge($temp['all'],$x);
    $temp['myevents'] = array_merge($temp['myevents'],$x);
    
    $fields = $temp['all'];
    $fields['paging'] = array('page'=>$_GET['eventlistpage'],'show'=>90);
    $eventslist['all'] = event_get_list(array('fields'=>$fields));
    
    $fields = $temp['myevents'];
    $fields['paging'] = array('page'=>$_GET['eventlistpage'],'show'=>90);
    $eventslist['myevents'] = event_get_list(array('fields'=>$fields));
    
    $PSModData['eventslist'] = $eventslist;
    //if(count($eventslist['all'])==0)
    //header('location:'.ROOT_PATH.'events/create');
    
}
else if($_GET['pagetype'] == 'passion'){
    // $fields = array('category'=>$_GET['type'],'extraquery'=>'datestart > "'.date("Y-m-d h:i:s").'"','paging'=>array('page'=>$_GET['eventlistpage'],'show'=>10));
    $fields = array('category'=>$_GET['type'],'upcoming'=>1,'paging'=>array('page'=>$_GET['eventlistpage'],'show'=>9));
    $eventslist['all'] = event_get_list(array('fields'=>$fields));
    $module_display_style = "boxed_coresel";
    $PSModData['headertext'] = "Events in this passion category";
    $PSModData['viewalllink'] = ROOT_PATH.'events/events-in-'.strtolower($_GET['type']).'-passion';
}
else if($_GET['pagetype'] == 'index'){
    if($eventlistby && $eventlistby == 'featured')
    {
    $eventslist['all'] = event_get_list(array('fields'=>array('source'=>'organic','upcoming'=>1,'paging'=>array('page'=>$_GET['eventlistpage'],'show'=>20))));
    $PSModData['headertext'] = "Featured Events";
    $PSModData['headerlink'] = ROOT_PATH."events";
    }
    else if($eventlistby && $city)
    {
    $eventslist['all'] = event_get_list(array('fields'=>array('city'=>$city,'upcoming'=>1,'paging'=>array('page'=>$_GET['eventlistpage'],'show'=>20))));
    $PSModData['headertext'] = "Events in ".ucwords(strtolower($city));
    $PSModData['headerlink'] = ROOT_PATH."events/events-in-".strtolower($city);
    }
    else if($eventlistby && $passion)
    {
    $eventslist['all'] = event_get_list(array('fields'=>array('category'=>$passion,'upcoming'=>1,'paging'=>array('page'=>$_GET['eventlistpage'],'show'=>20))));
    $PSModData['headertext'] = "Events in ".$passion.' category';
    $PSModData['headerlink'] = ROOT_PATH."events/events-in-".$passion.'-passion';
    }
    else
    {
    $eventslist['all'] = event_get_list(array('fields'=>array('upcoming'=>1,'paging'=>array('page'=>$_GET['eventlistpage'],'show'=>20))));
    $PSModData['headertext'] = "Explore & Join Upcoming Events";
    $PSModData['headerlink'] = ROOT_PATH."events";
    }
    $module_display_style = "boxed_coresel_2";
    
}
else if($_GET['pagetype'] == 'home'){
    // $eventslist['all'] = event_get_list(array('extraquery'=>'dateend > "'.date("Y-m-d").'"'));
    //$eventslist['data'] = event_get_list(array('extraquery'=>'dateend > "'.date("Y-m-d").'"'));
    // $eventslist['all'] = event_get_list(array('extraquery'=>'dateend > "'.date("Y-m-d").'"'));
    $eventslist['all'] = event_get_list(array('fields'=>array('upcoming'=>1,'paging'=>array('page'=>$_GET['eventlistpage'],'show'=>20))));
    //$module_display_style = "boxed_coresel_fullwidth";
    $PSModData['headertext'] = "Events";
}
else if($_GET['pagetype'] == 'userprofile' || $_GET['pagetype'] == 'postDetail'){
    $profile_id = ($_GET['profile_id'])?$_GET['profile_id']:$_SESSION['user']['profile_id'];
    $eventids = array();
    //$successfulltransactions = transaction_get_list(array('extraquery'=>'userid = "'.$profile_id.'" and status = "complete|success" and producttype="event" '));
    //foreach($successfulltransactions as $trans)
    //$eventids[] = $trans['productid'];   
    $createdevents = event_get_list(array('fields'=>array('creator'=>$profile_id,'paging'=>array('page'=>1,'show'=>9))));
    if($createdevents)
    {
	$allevents = $createdevents;
	$PSModData['headertext'] = "Organised Events";
    }
    else
    {
	$joinedEvents = relation_list(array('profile_id'=>$profile_id,'relation'=>'joinedEvent'));
	foreach($joinedEvents as $relation)
	$eventids[] = $relation['object'];   
	    
	$allevents = event_get_list(array('fields'=>array('extraquery'=>'event_id IN ("'.implode('","',$eventids).'")','paging'=>array('page'=>1,'show'=>9))));
	$PSModData['headertext'] = "Participating in Events";
    }
    $eventslist['all'] = $allevents;
    $module_display_style = "boxed_coresel";
}
 else {
     $module_display_style = "boxed_coresel";
}
$PSModData['eventslist'] = $eventslist;
?>