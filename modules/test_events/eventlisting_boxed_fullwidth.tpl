<?php
if(count($eventslist['all'])==0)
return ;
?>
<div class="widget">
<div class="widget-heading">	
    <h2 class="hstyl_1 sm"><?=$headertext?></h2>
</div>
<div class="card-styl_1 events">
<?php
foreach($eventslist['all'] as $event)
{
    echo '<div class="card">';
    renderer_event($event);
    echo '</div>';
}
?>
</div>
</div>
