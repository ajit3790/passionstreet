<?php
if(count($eventslist['all'])<3)
return ;
?>
<div class="tab-content mb-small bx-styl">
    <?php
    if(!empty($eventslist['all']))
    {
    ?>
    <div role="tabpanel" class="row tab-pane fade <?=(empty($eventslist['all'])?'':'active in')?>" id="all-events">
        <div class="col-md-12 card-styl_1 sm carousel-wrapper">
            <i class="fa fa-angle-left"></i>
            <i class="fa fa-angle-right"></i>
            <div class="inner">
                <ul class="eventcarousel" style="width:2000px;">
                    <?php
		    $PSParams['eventfullwidthlistingcalled'] = true ;
                    foreach($eventslist['all'] as $event)
                    {
			echo '<li class="card">';
                        renderer_event($event);
                        echo '</li>';
                    }
                    ?>
                </ul>
            </div>
        </div> 
    </div>
    <?php
    }
    ?>
</div>