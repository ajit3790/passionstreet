<?php
$pageid = $_GET['page_id'];
if($pageid)
{
	$page = $PSParams['currentpage'];
	if(empty($page))
	{
	$page = page_get_details($pageid);
	$PSParams['currentpage'] = $page;
	}
	if(empty($page))
	{
		header("location:".ROOT_PATH.'page/manage');
	}
}
else
header("location:".ROOT_PATH.'page/manage');
$PSModData['page'] = $page;

$GLOBALS['page_meta']['title'] = (($page['pagename'])?$page['pagename']:'');
$GLOBALS['page_meta']['description'] = 'Join the page | '.$page['description'];
$temp[0] = $page['pagepic'];
if($page['mapimage'])
$temp[1] = $page['mapimage'];
if($page['routeimage'])
$temp[2] = $page['routeimage'];
$GLOBALS['page_meta']['url'] = $_SERVER['PHP_SELF_URL'];
$GLOBALS['page_meta']['image'] = $temp;
$PSJavascript['passiontype'] = $page['category'];
?>
