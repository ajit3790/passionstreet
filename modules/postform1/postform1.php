<?php
if(count($_POST)>0 || array_key_exists('postsubmit',$_POST))
{
    if($_GET['pagetype'] == 'eventDetail')
    {
        $fields['postedontype'] = 'event';
        $fields['postedon'] = $_GET['event_id'];
    }
    else if($_GET['pagetype'] == 'pageDetail')
    {
        $fields['postedontype'] = 'page';
        $fields['postedon'] = $_GET['page_id'];
    }
    $fields['post_id'] = generate_id("post");    
    $fields['post_type'] = '';
    $fields['title'] = $_POST['title'];
    $fields['content'] = $_POST['postsummary'];
    //$fields['images'] = upload_post_images($_FILES['postimages']);
    //$fields['images'] = upload_post_images_new($_FILES,'posts','required');
    $imageArray = $_POST['photoes'];
    //$fields['images'] = get_image_ratio(move_images_from_temp($imageArray,'posts'));
    $orgImages = move_images_from_temp($imageArray,'posts');
    $thumbImages = create_thumbnail($orgImages);
    $fields['images'] = get_image_ratio($orgImages);
    
    //print_array(array($orgImages,$thumbImages,$fields['images']));
    
    $temp = array();
    if(!empty($_POST['log']))
    {
    $fields['log'] = serialize($_POST['log']);
    $temp = explode('###',$_POST['workouttype']);
    $category = end($temp);
    }
    else
    {
	    foreach($_POST['tags'] as $tag=>$categorytemp)
	    {
		if(empty($categorytemp))
		$categorytemp = 'random';
		$temp[$categorytemp]++;
	    }
	    arsort($temp);
	    unset($temp['undefined']);
	    $key = key($temp);
	    $key2 = key($_POST['tags']);
	    $key3 = $_POST['tags'][$key2];
	    if($key)
	    {
		$category = $key;
	    }
	    else
	    {
		$category = '';    
	    }
    }
    $fields['category'] = $category;
    $fields['subcategory'] = implode(',',array_keys($_POST['tags']));
    // print_array($fields);
    // exit();
    $exturls = parse_urls_from_text($fields['title']); //for images
    foreach($exturls as $url){
        if(startsWith($url, ROOT_PATH.'ps_proxy.php?type=image'))
        {
            $extimg = str_replace(ROOT_PATH.'ps_proxy.php?type=image&psurl=','',$url);
            $uploadedimgurl = (upload_from_url('extimg', $extimg));
            if($uploadedimgurl)
            {
                $uploadedimgurl = UPLOAD_PATH.$uploadedimgurl;
                $fields['title'] = str_replace($url,$uploadedimgurl,$fields['title']);
            }
        }
    }
    tag_update(array_keys($_POST['tags']),$fields['post_id']);
    post_create($fields);
    //exit();
    if($_GET['pagetype'] == 'eventDetail')
    $post_list = post_get_list(array('category'=>$fields['category'],'postedontype'=>'event','postedon'=>$_GET['event_id']),1,'delete');
    else
    {
    $post_list = post_get_list(array('category'=>$fields['category']),1,'delete');
    $post_list = post_get_list(array(),1,'delete');
    }
    header('location:'.$_SERVER['PHP_SELF_URL'].'?share_post_id='.$fields['post_id']);
}
$currentPassionType = NULL;
if(!empty($_GET['type']))
$currentPassionType = $_GET['type'];
else if(empty($currentPassionType))
$currentPassionType = $PSParams['currentEvent']['category'];
$PSModData['currentPassionType'] = $currentPassionType;

foreach($PSParams['PSCategories'] as $temp1=>$temp2){
    if(empty($currentPassionType) || (!empty($currentPassionType) && $currentPassionType == $temp1) || (!empty($currentPassionType) && $PSParams['currentEvent']['category']))
    {
	    foreach($temp2['posttype'] as $temp3=>$temp4)
	    {
		$PSJavascript['passiontags'][] = array("value"=>$temp3,"text"=>$temp4,"passion"=>$temp1);
	    }
	    foreach($temp2['workouttype'] as $temp3=>$temp4)
	    {
		$PSJavascript['workouttags'][] = array("value"=>$temp3,"text"=>$temp4,"passion"=>$temp1);
	    }
    }
}
/*foreach($PSParams['PSCategoriesWorkout'] as $temp1=>$temp2){
    foreach($temp2['workouttype'] as $temp3=>$temp4)
    {
        $PSJavascript['passiontagsworkout'][] = array("value"=>$temp3,"text"=>$temp4,"passion"=>$temp1);
    }
}*/
//print_array($PSJavascript['passiontagsworkout']);
//$PSJavascript['passionSubcategories'] = $PSParams['PSSubCategoriesByCategory'];
//$PSJavascript['psworkout'] = $PSParams['PSSubCategoriesByCategoryWorkout'];

if($_GET['pagetype'] == 'passion')
$PSModData['showworkoutform'] = isset($PSParams['PSCategoriesWorkout'][$_GET['type']]);
else if($_GET['pagetype'] == 'eventDetail')
$PSModData['showworkoutform'] = isset($PSParams['PSCategoriesWorkout'][$PSParams['currentEvent']['category']]);
else
$PSModData['showworkoutform'] = true;

if($currentPassionType)
{
	$PSModData['workouttypes'][] = $currentPassionType;
}
else
{
	$workouttypearray = array_keys($PSParams['PSCategoriesWorkout']);
	$PSModData['workouttypes'] = $workouttypearray;
}
?>