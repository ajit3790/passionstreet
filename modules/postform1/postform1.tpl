<style>
.postform1 .about {position:relative;}
.postform1 .about .form-control{font-size:14px; color:#888; height:70px; margin-bottom:0;}
.postform1 .controls span{display:block; width:100%; line-height:34px; text-align:center; background:#f6f7f9; font-size:12px; color:#000; font-weight:bold; transition:all .25s ease; cursor:pointer; border-radius:16px; position:relative;}
.postform1 .controls span small{font-size:100%;}
.postform1 .controls span:hover{background:#dddfe2;}
.postform1 .controls .fa{margin-right:5px; font-size:18px; vertical-align:bottom; line-height:inherit;}
.postform1 .controls .fa-picture-o{color:#89BE4C;}
.postform1 .controls .fa-tags{color:#01c7f1;}
.postform1 .controls .fa-film{color:#F7CC06;}
.postform1 .controls .fa-cogs{color:#FD2A6A;}
.postform1 .controls span.photo input{ opacity:0; position:absolute;left: 0;top: 0;}
.activity_lst, .tag-memb_lst {position:absolute; top:100%; left:10px; right:10px;  background:#fff; border:1px solid #ccc; border-width:0 1px 1px; padding:10px 0; z-index:10; display:none; }
.activity_lst{padding-top:40px;}
.activity_lst h3{background:#ccc; font-size:15px; font-weight:normal; width:100%; padding:0 10px 5px; position:absolute; left:0; top:0; line-height:32px; color:#000;}
.activity_lst ul, .tag-memb_lst ul{padding:0;max-height:225px; overflow:auto; margin:0;}
.activity_lst ul li, .tag-memb_lst ul li{list-style:none; cursor:pointer; padding:5px 10px; line-height:36px; font-size:16px; color:#000; font-weight:bold;}
.activity_lst ul li img, .tag-memb_lst ul li img{float:left; height:36px; margin-right:10px;}
.activity_lst ul li:hover, .tag-memb_lst ul li:hover{background:#232f3e; color:#fff;}
#workout, #tag-member{display:none;}
#tag-member .col-md-12 .inner{position:relative; border-bottom:1px solid #ccc;}
#tag-member .col-md-12 .form-control{border:0;}
#tag-member .tag-memb_lst{left:0; right:0;}
/*#tag-member .col-md-12 .inner:before{position:absolute; content:"With"; font-size:12px; height:32px; line-height:32px; left:10px; bottom:0; background:#ccc; text-align:center; display:block; width:40px;}*/
.tag_lst{margin-bottom:15px;}
.tag_lst .item{display:inline-block; position:relative; background:#f6f7f7; line-height:24px; height:24px; padding:0 20px 0 8px; color:#000; font-size:12px; margin-right:5px; vertical-align:middle;}
.tag_lst .item a.close { position: absolute; font-size: 12px;color: #999; z-index: 1; right: 0;top: 0; cursor: pointer;opacity: .5; background: #c9c9c9; line-height:22px; width: 15px; text-align:center; height: 24px;}
.tag_lst .item:hover a.close{color:#333;}
.tag_lst .item.add{border:2px dashed #eee; padding:0 8px; color:#ccc; font-size:14px; background:none; cursor:pointer; height:26px;}


.photo_lst .item{display:inline-block; width:100px; height:100px; overflow:hidden; position:relative; margin:0 10px 10px 0;}
.photo_lst .item img{min-width:100%; min-height:100%; max-height:100%;}
.photo_lst .item .overlay{position:absolute; width:100%; height:100%; z-index:10; background:rgba(0,0,0,.5); display:none; left:0; top:0;}
.photo_lst .item a.close{color:#fff; font-size:18px; position:absolute; z-index:11; right:5px; top:5px; display:none; opacity:.8; font-weight:normal;}
.photo_lst .item:hover .overlay, .photo_lst .item:hover a.close{display:block;}
.photo_lst .item a.close:hover{opacity:1;}
.photo_lst .item.add{background:none; border:3px dashed #eee; text-align:center; line-height:90px; font-size:35px; color:#eee; font-weight:bold;  cursor:pointer;}
.photo_lst .item.add:hover, .tag_lst .item.add:hover{border-color:#ccc; color:#ccc;}
.photo_lst .item.add input{position:absolute;opacity:0; height:100px; right:0; bottom:0; display:block; cursor:pointer;}
@media screen and (max-width:767px){
	.postform1 .controls span{background:none;}
	.postform1 .controls span small{display:none;}
	.ps-act-ftr{margin-left:-10px; margin-right:-10px;}
	.postform1 .about .form-control{margin-bottom:0;}
}
</style>



<div class="widget-heading">	
    <h2 class="hstyl_1 sm">Publish Activity</h2>
</div>
<form class="form1" method="post">
    <div class="bx-styl">
        <div class="row form-group">
            <div class="col-md-12 about">
                <textarea name="postsummary" placeholder="Write something" required class="form-control" onClick="showactivities();"></textarea>
                <div class="activity_lst">
                	<h3>Select activity type</h3>
                	<ul>
                    	<li><img src="images/motorbiking.jpg" alt="Motorbiking"> Motorbiking</li>
                        <li><img src="images/music.jpg" alt="Music"> Music</li>
                        <li><img src="images/rafting.jpg" alt="Rafting"> Rafting</li>
                        <li><img src="images/dancing.jpg" alt="Dancing"> Dancing</li>
                        <li><img src="images/voyage.jpg" alt="Voyage"> Voyage</li>
                        <li><img src="images/running.jpg" alt="Running"> Running</li>
                        <li><img src="images/painting.jpg" alt="Painting"> Painting</li>
                        <li><img src="images/treknmount.jpg" alt="Trekking"> Trekking</li>
                        <li><img src="images/yoga.jpg" alt="Yoga"> Yoga</li>
                        <li><img src="images/culinary.jpg" alt="Food Safari &amp; Culinary"> Food Safari &amp; Culinary</li>
                        <li><img src="images/cycling.jpg" alt="Cycling"> Cycling</li>
                        <li><img src="images/photography.jpg" alt="Photography"> Photography</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tag_lst">
        	<div class="item">Cycling <a class="close">x</a></div>
            <div class="item add">+ Add more categories</div>
        </div>
        <div class="photo_lst">
        	<div class="item"><img src="https://passionstreet.in/files/extimg/extimg_091049400-1487641155.jpg" />
            	<div class="overlay"></div>
                <a href="#" class="close">X</a>
            </div>
            <div class="item add">+<input type="file" /></div>
        </div>
        
        <div id="workout"  >
        
        <div class="post-log">
	   
	    <div class="row">
	    		
	    	   <div class="col-md-3 col-sm-3 col-xs-6 form-group">
	    	   <div class="row">
	    	        <label class="control-label col-xs-12 ">Workout Type</label>
	    	        <div class="col-xs-12">
			<?php
			if(count($workouttypes) == 1)
			{
				echo '<input type="text" class="form-control" required="" readonly="" value="'.$PSParams['PSCategories'][$workouttypes[0]]['name'].'">';
				echo '<input type="hidden" name="workouttype" class="workouttype form-control" required="" readonly="" value="'.$workouttypes[0].'">';
			}
			else
			{
			?>
			<select name="workouttype" class="workouttype form-control" required>
				<option value=''>Select</option>
				<?php
				foreach($workouttypes as $passion)
				{
					
					echo '<option value="'.$passion.'">'.$PSParams['PSCategories'][$passion]['name'].'</option>';
				}
				?>
			</select>
			<?php
			}
			?>
			</div>
			</div>
		   </div>	
	    		
		   <div class="col-md-3 col-sm-3 col-xs-6 form-group ">
			 <div class="row">
					<label class="control-label col-xs-12 ">Date </label>
					<div class="col-xs-12">
						<i class="fa fa-calendar"></i>
                                                <input name="log[date]" required="" style="background-color:#fff" type="text" class="datepicker form-control date date-past"  placeholder="DD/MM/YYYY" readonly>
					</div>  
				 </div>  
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6">
				<div class="row">
					<label class="control-label col-xs-12 ">Distance</label>
					<div class="col-xs-12 distance ">
						<input name="log[distance]" required="" id="logdistance" type="text" class="form-control logcfields" placeholder="">
					</div> 
					<!--<div class="col-md-4 col-sm-4"><span class="text"><input type="hidden" name="log[distanceunit]" value="KM" />KM</span></div>-->  
				 </div>
					  
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6 form-group ">
				<div class="row">
					<label class="control-label col-xs-12 ">Time taken</label>
					<div class="col-xs-12">
						<i class="fa fa-clock-o"></i>
						<input id="logtime" name="log[totaltimehh]" required="" style="background-color:#fff" data-type="hourrangeselector" data-options="0,100,2" type="text" class="datepicker form-control date date-past logcfields"  placeholder="HH:MM:SS" readonly>
					</div>
				 </div> 
			</div>
	   </div>
	   <div class="row distroy-clear">
		   
			<div class="col-md-4 col-sm-4 col-xs-6">
				 <div class="row">
					<label class="control-label col-xs-12  ">Heart rate (BPM)</label>
					<div class="col-md-6 col-sm-6 col-xs-6 form-group">
						<input name="log[heartrateavg]" id="logheartrate" type="text" class="form-control logcfields logcfieldsoptional" placeholder="AVG">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 form-group">
						<input name="log[heartratemax]" type="text" class="form-control" placeholder="MAX">
					</div>  
				 </div>    
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="row">
					<label class="control-label col-xs-12 ">Altitude (MT)</label>
					<div class="col-md-6 col-sm-6 col-xs-6 date form-group">
						<input name="log[altitudelow]" type="text" class="form-control" placeholder="MIN">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 date form-group">
						<input name="log[altitudehigh]" type="text" class="form-control" placeholder="MAX">
					</div>  
				 </div>     
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6 ">                                 
				 <div class="row">
					<label class="control-label col-xs-12 ">Height &amp; Weight</label>
					
					<div class="col-xs-12 form-group update-box">
                                                <i class="fa fa-gear" onclick="javascript:customToggle('.update-popup')"></i>
						<input type="text" onfocus="javascript:customToggle('.update-popup')" id="userHeightWidth" readonly placeholder="0 CM, 0 KG" class="form-control" value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].' CM, ':''?><?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].' KG':''?>">
						<div class="update-popup hide">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="row">
										<label class="control-label col-xs-12 ">Height</label>
										<div class="col-xs-12 date">
                                                                                        <input value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].'':''?>" type="text" id="userheight" placeholder="In CM" class="form-control">
										</div>
										 
									 </div>     
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="row form-group">
										<label class="control-label col-xs-12 ">Weight</label>
										<div class="col-xs-12 date">
											<input value="<?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].'':''?>" type="text" id="userweight" placeholder="In KG" class="form-control">
										</div>
									 </div>   
								</div>
							</div>
							<div class="row">
								<input type='button' id='updateUserheightwidthbtn' class="btn btn-primary btn-sm" value="Update" />
							 </div> 
						</div>                                        
					</div>
				 </div>                                       
			</div>
	   </div>
	   
	   
	   <div class="row distroy-row">
			<div class="col-md-4 col-sm-4 col-xs-6">
				 <div class="row">
					<label class="control-label col-xs-12 ">Avg speed</label>
					<div class="col-xs-12 form-group">
						<i class="fa fa-refresh"></i>
						<input readonly id="avgspeed" type="text" name="log[avgspeed]" placeholder="0 KM/H" class="form-control">
					</div>
				 </div>    
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="row">
					<label class="control-label col-xs-12 ">Calories</label>
				   <div class="col-xs-12 form-group felt">
						<i class="fa fa-refresh"></i>
						<input readonly id="calories" type="text" name="log[calories]" placeholder="0 CALORIES" class="form-control">
					</div>                                    
				 </div>       
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="row">
					<label class="control-label col-xs-12  ">Felt</label>
					<div class="col-xs-12 form-group felt">
						<label class="icon" title="Awesome" data-placement="top" data-toggle="tooltip" data-original-title="smile"><input type="radio" name="log[felt]" value="awesome"><?=$GLOBALS['PSParams']['PSSmilies']['awesome']?>

						</label>
						<label class="icon" title="Good" data-placement="top" data-toggle="tooltip" data-original-title="excited"><input type="radio" name="log[felt]" value="good"><?=$GLOBALS['PSParams']['PSSmilies']['good']?>

						</label>
						
						<label class="icon" title="Ok" data-placement="top" data-toggle="tooltip" data-original-title="Ok"><input type="radio" name="log[felt]" value="okok"><?=$GLOBALS['PSParams']['PSSmilies']['okok']?>

						</label>
						<label class="icon" title="Awefull" data-placement="top" data-toggle="tooltip" data-original-title="sad"><input type="radio" name="log[felt]" value="awefull"><?=$GLOBALS['PSParams']['PSSmilies']['awefull']?>

						</label>
						
					</div>
				 </div>     
			</div>
	   </div>
	   </div>
	   <div class="row hide">
			<label class="control-label col-md-4 middle ">Give it a name</label>
			<div class="col-md-8 form-group">
				<input type="text" name="title" class="form-control">
			</div>
		</div>
    </div>
        <!--/Workout -->
        <div id="tag-member" class="row form-group">
        	<div class="col-md-12">
            	<div class="inner"><input type="text" class="form-control" placeholder="Whom are you with?" />
                	<div class="tag-memb_lst">
                        <ul>
                            <li><img src="https://passionstreet.in/files/userdp/userdp_0638840001456728613.jpg" alt="" /> Saumitra Dey </li>
                            <li><img src="https://passionstreet.in/files/userdp/userdp_0596308001456709758.jpg" alt="" /> Paromita Dey </li>
                            <li><img src="https://passionstreet.in/files/userdp/userdp_005096900-1490201006.jpg" alt="" /> Jauhar Amir</li>
                            <li><img src="https://passionstreet.in/files/userdp/userdp_0638840001456728613.jpg" alt="" /> Saumitra Dey </li>
                            <li><img src="https://passionstreet.in/files/userdp/userdp_0596308001456709758.jpg" alt="" /> Paromita Dey </li>
                            <li><img src="https://passionstreet.in/files/userdp/userdp_005096900-1490201006.jpg" alt="" /> Jauhar Amir</li>
                        </ul>
                    </div>
                </div>
                
         </div>
        </div>   
    
        <div class="row controls">
        	<div class="col-xs-3 col-sm-3"><span class="photo"><i class="fa fa-picture-o" aria-hidden="true"></i> <small>Add Photo</small> <input class="showimgpreview" target=".imgpreviewbox" multiple type="file"></span></div>
            <div class="col-xs-3 col-sm-3"><span><i class="fa fa-film" aria-hidden="true"></i> <small>Add Video</small></span></div>
            <div class="col-xs-3 col-sm-3"><span class="workout"><i class="fa fa-cogs" aria-hidden="true"></i> <small>Add Workout</small></span></div>
            <div class="col-xs-3 col-sm-3"><span class="tag-member"><i class="fa fa-tags" aria-hidden="true"></i> <small>Tag member</small></span></div>
        </div>
    </div>
</form>

<script>
function showactivities()
    {
        $( ".activity_lst" ).fadeIn();
    }
$(document).ready(function(e) {
    $(".workout").click(function(){
		$("#workout").slideDown();
	});
	 $(".tag-member").click(function(){
		$("#tag-member").slideDown();
	});
	
		
});

</script>

