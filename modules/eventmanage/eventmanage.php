<?php
$eventid = $_GET['event_id'];
if($eventid)
{
	$event = $PSParams['currentEvent'];
	if(empty($event))
	{
	$event = event_get_details($eventid,true,PROFILE_ID,true);
	$PSParams['currentEvent'] = $event;
	}
	if(empty($event) || ($event['creator'] != PROFILE_ID))
	{
		header("location:".ROOT_PATH.'events');
	}
}
else
header("location:".ROOT_PATH.'events');
$joinedMemberList = event_get_attending_members($eventid);
$pendingMemberList = event_get_pending_members($eventid);
$PSModData['event'] = $event;
$PSModData['joinedMemberList'] = $joinedMemberList;
$PSModData['pendingMemberList'] = $pendingMemberList;
$PSModData['eventTypes'] = $PSParams['eventTypes'];

$pages = page_get_list(array('creator_id'=>$PSData['user']['profile_id'],'status'=>2));
$pageContact = array();
foreach($pages as $page)
{
	$temp = array();
	$temp['id'] = $page['page_id'];
	$temp['name'] = $page['page_name'];
	$temp['email'] = $page['page_email'];
	$temp['contact'] = ($page['page_contactno'])?$page['page_contactno']:'';
	$temp['address'] = ($page['page_address'])?$page['page_address']:'';
	$pageContact[$temp['id']] = $temp;
}
$userContact = array();
	$temp = array();
	$temp['id'] = $PSData['user']['profile_id'];
	$temp['name'] = $PSData['user']['fullname'];
	$temp['email'] = $PSData['user']['email'];
	$temp['contact'] = ($PSData['user']['contactno'])?$PSData['user']['contactno']:'';
	$temp['address'] = ($PSData['user']['city'])?$PSData['user']['city'].', '.$PSData['user']['country']:'';
	$userContact[$temp['id']] = $temp;

$PSJavascript['map_position'] = $event['maploc'];
$PSJavascript['pageContact'] = $pageContact;
$PSJavascript['userContact'] = $userContact;
$PSJavascript['dateRange'] = array(
    "start"=>generate_standard_date($event["datestart"]),
    "end"=>generate_standard_date($event["dateend"])
);
?>