<div class="tab-header mb0">
	<ul class="nav nav-pills full-width">
		<li role="presentation" class="active"><a href="#members" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Attending Members</a></li>
		<li role="presentation" class=""><a href="#pendingmembers" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Ticketing Failed</a></li>
		<!--<li role="presentation" class=""><a href="#roles" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Roles</a></li>
		<li role="presentation" class=""><a href="#messages" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Messages</a></li>-->
		<li role="presentation" class=""><a href="#editevent" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Edit Event</a></li>
                <li role="presentation" class=""><a href="#eventdata" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Data</a></li>
	</ul>
</div>
<div class="bx-styl tab-content">
        <div id="eventdata" class="tab-pane fade in" role="tabpanel">
		<div class="inner clearfix ">
			<ul> 
                            <li id="attending-members" class="ajax-btn" data-c="jsontotable,#databox,xxx" data-trgtid="databox" data-action="get" data-aj="true" data-p-str="type,eventid" data-t="extract-members" data-eventid="<?=$event['event_id']?>" data-type="attending-members">Extract attending member list</li>
                            <li id="failed-members" class="ajax-btn" data-c="jsontotable,#databox,xxx" data-trgtid="databox" data-action="get" data-aj="true" data-p-str="type,eventid" data-t="extract-members" data-eventid="<?=$event['event_id']?>" data-type="failed-members">Extract members with failed transaction</li>
                        </ul>
                        <div id="databox" style="width:100%;overflow-x: scroll">
                                
                        </div>
		</div>
	</div>
	<div id="members" class="tab-pane fade active in" role="tabpanel">
		<div class="inner clearfix follow_lst">
			<ul> 
			<?php
			foreach($joinedMemberList as $user)
			{
				echo '<li>';
				renderer_member($user);
				echo '</l1>';
			}
			?>
			</ul>
		</div>
	</div>
	<div id="pendingmembers" class="tab-pane fade" role="tabpanel">
		<div class="inner clearfix follow_lst">
			<ul> 
			<?php
			foreach($pendingMemberList as $user)
			{
				echo '<li>';
				renderer_member($user);
				echo '</l1>';
			}
			?>
			</ul>
		</div>
	</div>
	<div id="roles" class="tab-pane fade" role="tabpanel">
		<div class="inner clearfix follow_lst">
			<ul> 
			<?php
			foreach($roles as $user)
			{
				echo '<li>';
				renderer_member($user);
				echo '</l1>';
			}
			?>
			</ul>
		</div>
	</div>
	<div id="messages" class="tab-pane fade" role="tabpanel">
		<div class="inner clearfix follow_lst">
			<ul> 
			<?php
			foreach($roles as $user)
			{
				echo '<li>';
				renderer_member($user);
				echo '</l1>';
			}
			?>
			</ul>
		</div>
	</div>
	<div id="editevent" class="tab-pane fade" role="tabpanel">
		<div class="inner clearfix">
			<div class="tab-header mb0">
				<ul class="nav nav-pills full-width">
					<li role="presentation" class="active"><a href="#editevent-eventdetails" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Event Details</a></li>
					<li role="presentation" class=""><a href="#editevent-eventticketing" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Ticketing Details</a></li>
					<li role="presentation" class=""><a href="#editevent-eventselectionq" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Selection Criteria</a></li>
					<li role="presentation" class=""><a href="#editevent-eventspice" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Route Maps / Spice</a></li>
					<li role="presentation" class=""><a href="#editevent-eventseo" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">SEO</a></li>
					<li role="presentation" class=""><a href="#editevent-eventcoupons" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Coupons</a></li>
				</ul>
			</div>
			<div class="bx-styl tab-content">
				<div id="editevent-eventdetails" class="tab-pane fade active in" role="tabpanel">
					<div class="inner clearfix ">
					<?php
						$editmode = 1;
						include(MODULE_DIR.DIRECTORY_SEPARATOR.'eventcreatev2'.DIRECTORY_SEPARATOR.'eventcreatev2.tpl');
					?>
					</div>
				</div>
				<div id="editevent-eventticketing" class="tab-pane fade in" role="tabpanel">
					<div class="inner clearfix ">
					<?php
						$_GET['step'] = 'step1';
						$showform = array();
						$showform['step1'] = 'show';
						include(MODULE_DIR.DIRECTORY_SEPARATOR.'eventpublish'.DIRECTORY_SEPARATOR.'eventpublish.tpl');
					?>
					</div>
				</div>
				<div id="editevent-eventselectionq" class="tab-pane fade in" role="tabpanel">
					<div class="inner clearfix ">
					<?php
						$_GET['step'] = 'step2';
						$showform = array();
						$showform['step2'] = 'show';
						include(MODULE_DIR.DIRECTORY_SEPARATOR.'eventpublish'.DIRECTORY_SEPARATOR.'eventpublish.tpl');
					?>
					</div>
				</div>
				<div id="editevent-eventspice" class="tab-pane fade in" role="tabpanel">
					<div class="inner clearfix ">
					<?php
						$_GET['step'] = 'step3';
						$showform = array();
						$showform['step3'] = 'show';
						include(MODULE_DIR.DIRECTORY_SEPARATOR.'eventpublish'.DIRECTORY_SEPARATOR.'eventpublish.tpl');
					?>
					</div>
				</div>
				<div id="editevent-eventseo" class="tab-pane fade active in" role="tabpanel">
					<div class="inner clearfix ">
						<div class="col-md-12">
							<div class="form-wrapper" id="metainfo">
								<script>
								$(document).ready(function(){
									$("#metainfo").html(calliframe("module/pagemetaadd?pageType=eventDetail&pageId="+q['event_id']));
								});
								</script>
							</div>
						</div>
					</div>
				</div>
				<div id="editevent-eventcoupons" class="tab-pane fade active in" role="tabpanel">
					<div class="inner clearfix ">
						<div class="col-md-12">
							<div class="form-wrapper1" id="eventcoupons">
								<script>
								$(document).ready(function(){
									$("#eventcoupons").html(calliframe("module/couponsmanage?entityType=event&entityId="+q['event_id']));
								});
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    function rendertable(data){
        console.log('render table');
        console.log(data);
    }
</script>