<?php
$eventid = $_GET['event_id'];
if(!($eventid) ||  !($event = event_get_details($eventid))){
    header("location:".ROOT_PATH.'events/create');
}
else if($event['creator'] != $_SESSION['user']['profile_id']){
    header("location:".ROOT_PATH.'events/create');
}
if(!($_GET['step']))
{
    if(!isset($_SESSION['eventcreation'][$eventid]['step1']))
    $_GET['step'] = 'step1';
    else if(!isset($_SESSION['eventcreation'][$eventid]['step2']))
    $_GET['step'] = 'step2';
    else if(!isset($_SESSION['eventcreation'][$eventid]['step3']))
    $_GET['step'] = 'step3';
}
$PSModData['showform'][$_GET['step']] = 'show';
if($_POST['step1'])
{
    extract($_POST);
    $inputParams['maxuserticketcount'] = $ticketcount;
    $inputParams['ticketpricing']=$ticketfeeradio;
    $inputParams['ticketbookingends']=generate_standard_date($ticketbookingends);
    if($ticketfeeradio == 'No')
    {
        $inputParams['maxticketcount'] = $totalticketcount;
        $inputParams['accountId'] = '';
        $inputParams['ticketcategories'] = '';
    }
    else
    {
        $inputParams['accountId'] = $ticket_bank_account;
        $maxticketcount = 0;
        $ticketCategories = array();
        foreach($ticketfeecategory as $key=>$value)
        {
            if($ticketfeecategory[$key] && $ticketfeefee[$key] && $ticketfeeticketss[$key] )
            {
                $ticketCategories[] = array("name"=>$ticketfeecategory[$key],"price"=>$ticketfeefee[$key],"maxticketcount"=>$ticketfeeticketss[$key]);
                $maxticketcount = $maxticketcount  +  $ticketfeeticketss[$key];
            }
        }
        $inputParams['maxticketcount'] = $maxticketcount;
        $inputParams['ticketcategories'] = serialize($ticketCategories);
        
    }
    $inputParams['extra']['organiser'] = $organiser;
    $_SESSION['eventcreation'][$eventid]['step1'] = $inputParams; 
    header("location:".ROOT_PATH.'events/publish/'.$eventid);
}
else if($_POST['step2'])
{
    extract($_POST);
    $inputParams['visibility'] = $event_visibility;
    $inputParams['precondition'] = $event_precondition;
    if($inputParams['precondition'] == 'Yes')
    {
        $inputParams['precondition_type'] = $event_precondition_type;
        if($inputParams['precondition_type'] == 'question')
        {
            $inputParams['precondition_param']['question'] = array();
            $inputParams['precondition_param']['question']['text'] = $question;
            $inputParams['precondition_param']['question']['type'] = $event_participation_question_type;
            if($inputParams['precondition_param']['question']['type'] == 'objective')
            {
                $inputParams['precondition_param']['question']['options'] = $options;
            }
            $inputParams['precondition_param'] = serialize($inputParams['precondition_param']['question']);
        }
    }
    $_SESSION['eventcreation'][$eventid]['step2'] = $inputParams;
    header("location:".ROOT_PATH.'events/publish/'.$eventid);
}
else if($_POST['step3'])
{
    extract($_POST);
    $inputParams['route_map'] = $event_include_routemap;
    if($inputParams['route_map'] == 'Yes')
    {
        foreach($waypoints as $stop)
        {
            $x[] = json_decode($stop, true);
        }
        $inputParams['route_map_stops'] = serialize($x);
        if($route_pic)
        $inputParams['routeimage'] = upload_from_url('eventroutemap',$route_pic);
    }
    $inputParams['extra'] = $_SESSION['eventcreation'][$eventid]['step1']['extra'];
    $inputParams['extra']['allowuserpost'] = $allowuserpost ;
    $_SESSION['eventcreation'][$eventid]['step3'] = $inputParams;

    $inputParams = array();
    $inputParams['ticketcount'] =  $_SESSION['eventcreation'][$eventid]['step1']['maxticketcount'];
    $inputParams['peruserticketcount'] =  $_SESSION['eventcreation'][$eventid]['step1']['maxuserticketcount'];
    $inputParams['bankaccount'] =  $_SESSION['eventcreation'][$eventid]['step1']['accountId'];
    $inputParams['ticketcategories'] =  $_SESSION['eventcreation'][$eventid]['step1']['ticketcategories'];
    $inputParams['visibility'] =  $_SESSION['eventcreation'][$eventid]['step2']['visibility'];
    $inputParams['precondition'] =  $_SESSION['eventcreation'][$eventid]['step2']['precondition_type'];
    $inputParams['preconditionOption'] =  $_SESSION['eventcreation'][$eventid]['step2']['precondition_param'];
    $inputParams['routemap'] =  $_SESSION['eventcreation'][$eventid]['step3']['route_map_stops'];
    $inputParams['routeimage'] =  $_SESSION['eventcreation'][$eventid]['step3']['routeimage'];
    $inputParams['status'] =  'published';
    $inputParams['extra'] = serialize($_SESSION['eventcreation'][$eventid]['step3']['extra']);
    
    event_update($eventid,$inputParams);
    $module_display_style = "done";
    
}
$PSModData['event'] = $event;
?>