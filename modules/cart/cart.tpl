<?php
$totalCartValue = 0;
foreach($products as $type=>$productlist){
    echo "<div class='cart-type-".$type."'>".ucfirst($type)."<br />";
    foreach($productlist as $product)
    {
        echo "<div class='row'>";
            echo "<div class='col-md-3'>".$product['productid']."</div>";
            echo "<div class='col-md-3'>".$product['ticketname']."</div>";
            echo "<div class='col-md-3'>".$product['ticketprice']."</div>";
            echo "<div class='col-md-3'>".$product['buycount']."</div>";
        echo "</div>";
    $totalCartValue = $totalCartValue + ($product['ticketprice'] * $product['buycount']);
    }
    echo "</div>";
}

echo "<div class='cart-value'>".$totalCartValue."</div>";
echo "<a class='btn btn-primary' onclick=\"javascript:parent.callmodaliframe('Payment','payumoney/PayUMoney_form.php')\" > Move To Payment </a>";
?>