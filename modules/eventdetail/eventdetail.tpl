<div class="widget hide eventbooking">
    <div class="eventbookingsteps datetimeslots">
        <div class="widget-heading"><h2 class="hstyl_1 sm">Select Date / Time Slot</h2></div>
        <div class="bx-styl form1 datetimeslotdiv clearfix">
        <?php
        foreach($event['upcomingdatetimeslots'] as $i=>$data){
            ?>
            <label style="width:100%"><div class="col-xs-3"><div class='pull-right'><input type='radio' name='slotid' value='<?=$data["slotid"]?>' class='datetimeradio' <?=(($i==0)?'checked':'')?> ></div></div><div class='col-xs-9'><?=(date('j M y, h:i A',strtotime($data['datestart'])).' -> '.date('j M y, h:i A',strtotime($data['dateend'])))?></div></label>
            <?php
        }
        ?>
        <button class="btn btn-primary pull-right" data-onclick="showcart">Proceed</button>
        </div>
    </div>
    <div class="eventbookingsteps hide customdateselector">
	<div class="widget-heading"><h2 class="hstyl_1 sm">Select Date</h2></div>
	<div class="bx-styl form1 clearfix">
		<div class="customdateselectordiv">
		</div>
		<input type="hidden" class="customdates">
		<button class="btn btn-primary pull-right hide" data-onclick="showcart">Proceed</button>
	</div>
    </div>
    <div class="eventbookingsteps hide cart"></div>
    <div class="eventbookingsteps hide participants"></div>
    <div class="eventbookingsteps hide questionaire"></div>
</div>

<div class="tab-header mb0">
	<ul class="nav nav-pills full-width">
	    <li role="presentation" class="active"><a href="#evnt_dtl" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Event Details </a></li>
	    <li role="presentation" class="<?=(($event['terms'])?'':'hide')?>"><a href="#tmc" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Terms &amp; Conditions</a></li>
	    <li role="presentation" class="<?=(($event['routeimage'])?'':'hide')?>"><a href="#evnt_routemap" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Route Map</a></li>
	</ul>
</div>
<div class="post bx-styl tab-content">
		<div id="evnt_dtl" class="tab-pane fade active in">
			<div class="post-desc event-desc scroll-content clearfix">                           
			<div class="post-content collapsed"><p><?=$event['description']?></p></div> </div>  
		</div>  
		<div id="tmc" class="tab-pane fade">
			<div class="post-desc event-desc scroll-content  clearfix">                           
				<div class="post-content collapsed"><p><?=$event['terms']?></p></div>
			</div>
		</div>
		<?php
		if($event['routeimage'])
		{ ?>
		<div id="evnt_routemap" class="tab-pane fade" data-frame="module/maps/routemap?coordinates=<?=urlencode(serialize($event['routemap']))?>">
			<!--<a href="#" class="callmodaliframe" data-targetsrc="module/maps/routemap?coordinates=<?=urlencode(serialize($event['routemap']))?>">
				<span class="hide">Route Map</span><img src="<?=$event['routeimage']?>" style="width:100%">
			</a>-->
				<div class="panel-body form1  form-wrapper" id="eventroutemap">
				</div>
		</div>
		<?php } 
		?>
		
		                 
</div>
<?php
$displayblockconfig = array();
$displayblockconfig['itenarary'] = array('name'=>'Itenaray','id'=>'event_itenary');
if($event['displayblocks']){
?>
<div class="tab-header mb0">
	<ul class="nav nav-pills full-width">
	    <?php
	    $i = 0;
	    foreach($event['displayblocks'] as $displayblockkey=>$displayblock){
		echo '<li role="presentation" class="'.(($i == 0)?'active':'').'"><a href="#'.$displayblockconfig[$displayblockkey]['id'].'" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">'.(($displayblock[0]['title'])?$displayblock[0]['title']:$displayblockconfig[$displayblockkey]['name']).'</a></li>';
	    $i++;
	    }
	    ?>
	</ul>
</div>
<div class="post bx-styl tab-content">
	<?php
	$i = 0;
	foreach($event['displayblocks'] as $displayblockkey=>$displayblock){
		?>
		<div id="<?=$displayblockconfig[$displayblockkey]['id']?>" class="tab-pane fade <?=(($i==0)?'active':'')?> in">
			<div class="post-desc event-desc scroll-content clearfix">                           
			<div class="post-content collapsed"><p>
			<?php
			if($displayblockkey == 'itenarary')
			{
				$itenararyitems = $displayblock[0]['data'];
				foreach($itenararyitems as $itenarary)
				{
					echo '<ul class="itenararyday">Day '.$itenarary['dayno'].'<br />';
					foreach($itenarary['itemlist'] as $item)
					{
						echo '<li>';
						if($item['time'])
						{
						echo '<span class="time">'.$item['time'].'</span>';
						echo '<span class="item">'.$item['item'].'</span>';
						}
						else
						echo '<span class="itemfull">'.$item['item'].'</span>';
						echo '</li>';
					}
					echo '</ul>';
				}
			}
			?>
			</p></div> </div>  
		</div>
		<?php
		$i++;
	}
	?>
</div>
<?php
}
$PSJsincludes['external'][] = "js/event.js";
?>