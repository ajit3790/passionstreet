<div class="post bx-styl1 tab-content">
		<div id="evnt_dtl" class="tab-pane fade active in">
			<h5 >Event Details</h5>
			<div class="post-desc event-desc scroll-content clearfix">                           
			<div class="post-content collapsed"><p><?=strip_only($event['description'],array('font','span'))?></p></div> </div>  
		</div>  
		<?php
		if($event['terms'])
		{
		?>
		<div id="tmc" class="tab-pane fade">
			<h5 >Terms & Condition</h5>
			<div class="post-desc event-desc scroll-content  clearfix">                           
				<div class="post-content collapsed"><p><?=strip_only($event['terms'],array('font','span'))?></p></div>
			</div>
		</div>
		<?php
		}
		if($event['routeimage'] && false)
		{ ?>
		<h5 >Route Map</h5>
		<div id="evnt_routemap" class="tab-pane fade" data-frame="module/maps/routemap?coordinates=<?=urlencode(serialize($event['routemap']))?>">
			<!--<a href="#" class="callmodaliframe" data-targetsrc="module/maps/routemap?coordinates=<?=urlencode(serialize($event['routemap']))?>">
				<span class="hide">Route Map</span><img src="<?=$event['routeimage']?>" style="width:100%">
			</a>-->
				<div class="panel-body form1  form-wrapper" id="eventroutemap">
				</div>
		</div>
		<?php } 
		?>
		
		                 
</div>
<?php
$displayblockconfig = array();
$displayblockconfig['itenarary'] = array('name'=>'Itenaray','id'=>'event_itenary');
if($event['displayblocks']){
?>
<div class="tab-header mb0">
	<ul class="nav nav-pills full-width">
	    <?php
	    $i = 0;
	    foreach($event['displayblocks'] as $displayblockkey=>$displayblock){
		echo '<li role="presentation" class="'.(($i == 0)?'active':'').'"><a href="#'.$displayblockconfig[$displayblockkey]['id'].'" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">'.(($displayblock[0]['title'])?$displayblock[0]['title']:$displayblockconfig[$displayblockkey]['name']).'</a></li>';
	    $i++;
	    }
	    ?>
	</ul>
</div>
<div class="post bx-styl1 tab-content">
	<?php
	$i = 0;
	echo '<h5 >Itenarary</h5>';
	foreach($event['displayblocks'] as $displayblockkey=>$displayblock){
		?>
		<div id="<?=$displayblockconfig[$displayblockkey]['id']?>" class="tab-pane fade <?=(($i==0)?'active':'')?> in">
			<div class="post-desc event-desc scroll-content clearfix">                           
			<div class="post-content collapsed"><p>
			<?php
			if($displayblockkey == 'itenarary')
			{
				$itenararyitems = $displayblock[0]['data'];
				foreach($itenararyitems as $itenarary)
				{
					echo '<ul class="itenararyday">Day '.$itenarary['dayno'].'<br />';
					foreach($itenarary['itemlist'] as $item)
					{
						echo '<li>';
						if($item['time'])
						{
						echo '<span class="time">'.$item['time'].'</span>';
						echo '<span class="item">'.$item['item'].'</span>';
						}
						else
						echo '<span class="itemfull">'.$item['item'].'</span>';
						echo '</li>';
					}
					echo '</ul>';
				}
			}
			?>
			</p></div> </div>  
		</div>
		<?php
		$i++;
	}
	?>
</div>
<?php
}
$PSJsincludes['external'][] = "js/event.js";
?>