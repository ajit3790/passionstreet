<div class="container">
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-wrapper">
                	<div class="row">
		            <div class="col-md-12"><h2 class="hstyl_2"><span>HERE YOU GO!!!</span></h2></div>
		        </div>
                    <form class="form1" name="registration-form" method="post">
                       <span class="error-text">
                            <?=$server_response_text?>
                        </span>

                       <div class="row form-group">
                          <div class="col-sm-8">
                            <input class="form-control" placeholder="NAME" value="<?=$params['fname']?>" id="fname" name="fname" type="text">
                          </div>
                          <div class="col-sm-4">
                            <input class="form-control" placeholder="SURNAME" value="<?=$params['lname']?>" id="lname" name="lname" type="text">
                          </div>
                       </div> 
                        <div class="row form-group">
                          <div class="col-sm-12">
                            <input class="form-control" placeholder="EMAIL ID" value="<?=$params['email']?>" id="email" name="email" type="text">
                          </div>
                         
                       </div>
                       <div class="form-group row">
                            <label class="col-sm-3 control-label">GENDER</label>
                            <div class="radio col-sm-3">
                                <label>
                                  <input  name="gender" id="gender" value="male" <?=($params['gender']=='Male')?'checked="checked"':''?> type="radio">
                                  MALE
                                </label>
                             </div>
                             <div class="radio col-sm-3">
                                <label>
                                  <input  name="gender" id="gender" value="female" <?=($params['gender']=='Female')?'checked="checked"':''?> type="radio">
                                  FEMALE
                                </label>
                             </div>
                             <div class="radio col-sm-3">
                                <label>
                                  <input  name="gender" id="gender" value="others" <?=($params['gender']=='Others')?'checked="checked"':''?> type="radio">
                                  OTHERS
                                </label>
                             </div>   
                       </div>  
                       <div class="form-group row">
                            <label class="col-sm-3 control-label">DATE OF BIRTH</label>
                            <div class="radio col-sm-3">
                                <select name="dob_day" id="dob_day" class="form-control">
                                    <option value="-1">DAY</option>
                                </select>
                             </div>
                             <div class="radio col-sm-3">
                                <select name="dob_month" id="dob_month" class="form-control">
                                    <option value="-1">MONTH</option>
                                </select>
                             </div>
                             <div class="radio col-sm-3">
                                <select name="dob_year" id="dob_year" class="form-control">
                                    <option value="-1">YEAR</option>
                                </select>
                             </div>   
                       </div>
                       <div class="row form-group">
                          <div class="col-sm-6">
                            <input name="password" id="password" class="form-control" placeholder="PASSWORD" type="password">
                          </div>
                          <div class="col-sm-6">
                            <input name="re-password" id="re-password" class="form-control" placeholder="CONFIRM PASSWORD" type="password">
                          </div>
                       </div> 
                       <div class="row form-group">
                          <div class="col-sm-6">
                             <select name="country" id="country" class="form-control">
                                    <option value="-1">COUNTRY</option>
                              </select>
                          </div>
                          <div class="col-sm-6">
                            <input value="<?=$params['city']?>" name="city" id="city" class="form-control" placeholder="CITY" type="text">
                          </div>
                       </div> 
                       <div class="row form-group">
                          <div class="col-sm-12">
                            <input name="submit" class="form-control btn btn-yellow" value="Register" type="Submit">
                          </div>
                       </div> 
                          
                    </form>
                </div>
            </div>
        </div>
    </div>
<script>
$(document).ready(function(){
    var $temp = ''
    for(var $i=1;$i<=31;$i++){
        if( $i!=<?php echo ($params['dob_day'])?$params['dob_day']:-1 ; ?>)
        $temp += '<option value="'+$i+'">'+$i+'</option>';
        else
        $temp += '<option value="'+$i+'" selected>'+$i+'</option>';
        
    }
    $("#dob_day").append($temp);
    
    $temp = '';
    $monthObj = <?=json_encode($PSParams['months'])?>;
    $.each($monthObj, function($key, $value) {
        if( $key=='<?php echo (array_key_exists($params['dob_month'],$PSParams['months']))?$params['dob_month']:-1 ; ?>')
        $temp += '<option value="'+$key+'" selected>'+$value+'</option>';
        else
        $temp += '<option value="'+$key+'">'+$value+'</option>';
    });
    $("#dob_month").append($temp);
    
    $temp = ''
    for(var $i= (new Date()).getFullYear() ;$i>= ((new Date()).getFullYear()-100)  ;$i--){
        if( $i!=<?php echo ($params['dob_year'])?$params['dob_year']:-1 ; ?>)
        $temp += '<option value="'+$i+'">'+$i+'</option>';
        else
        $temp += '<option value="'+$i+'" selected>'+$i+'</option>';
        
    }
    $("#dob_year").append($temp);
    
    
    $temp = '';
    $countryObj = <?=json_encode($PSParams['country'])?>;
    $.each($countryObj, function($key, $value) {
        if( $key=='<?php echo (array_key_exists($params['country'],$PSParams['country']))?$params['country']:-1 ; ?>')
        $temp += '<option value="'+$key+'" selected>'+$value+'</option>';
        else
        $temp += '<option value="'+$key+'">'+$value+'</option>';
    });
    $("#country").append($temp);
    var validator = new FormValidator('registration-form', [{
        name: 'fname',
        display: 'fname',
        rules: 'required'
    },{
        name: 'lname',
        display: 'lname',
        rules: 'required'
    },{
        name: 'gender',
        rules: 'required'
    }, {
        name: 'password',
        rules: 'required|min-length[6]'
    }, {
        name: 're-password',
        rules: 'required|matches[password]|min-length[6]'
    }, {
        name: 'email',
        rules: 'required|valid_email'
    }, {
        name: 'city',
        rules: 'required'
    }, {
        name: 'dob_day',
        rules: 'required|numeric'
    }, {
        name: 'dob_month',
        rules: 'required|numeric'
    }, {
        name: 'dob_year',
        rules: 'required|numeric'
    }, {
        name: 'country',
        rules: 'required|alpha'
    }], function(errors, event) {
        
        for (var key in validator.fields) {
            if (validator.fields.hasOwnProperty(key)) {
              $("#" + validator.fields[key].id ).css({'background-color':'#ffffff'});
            }
        }
        if (errors.length > 0) {
            for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
                $("#"+ errors[i].id ).css({'background-color':'#ffb380'});
            }
        }
    });
    
})
    

</script>