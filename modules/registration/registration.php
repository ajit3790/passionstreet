<?php
if(!empty($_SESSION['user']['profile_id']) && strpos($_SERVER['REQUEST_URI'], '/register'))
header('location:'.ROOT_PATH);
$module_display_style = 2;
$PSModData['usertype']='';
if(isset($_GET['usertype']) && $_GET['usertype']=='expert')
$PSModData['usertype']='expert';
if(!empty($_SESSION['user']['profile_id'])){
$PSModData['editmode']=true;
$PSModData['params'] = $PSData['user'];
}
else{
$PSModData['editmode']=false;
$PSModData['params'] = $PSData['user'];
}
if(isset($_POST['submit']))
{
    $inData = $_POST;
    extract($_POST);
    if(!empty($_SESSION['user']['profile_id']) && $_POST['submit']=='completeprofile'){
        if(!empty($_POST['dob_year']))
        {
            $_POST['dob'] = $dob_year . '-' . $dob_month . '-' .$dob_day;
            $_POST['dob'] = date('Y-m-d',strtotime($_POST['dob'])).'';
            unset($_POST['dob_year']);
            unset($_POST['dob_month']);
            unset($_POST['dob_day']);
        }
        unset($_POST['category']);
        unset($_POST['submit']);
        unset($_POST['g-recaptcha-response']);
        $temp = $_POST['fulllocation'];
		unset($_POST['fulllocation']);
        $updateData = $_POST;
		$updateData['extra'] = array('fulllocation'=>$temp); 
        if(user_profile_update($updateData)){
            foreach($category as $cat)
            {
                relation_register($_SESSION['user']['profile_id'],'joinedPassion',$cat);
            }
            $PSJavascript['close_window'] = 1;
            $PSModData['server_response_html'] = '<div class="msgbox1 text-center"><p>Update Successful</p></div>';
            $PSModData['server_response_status'] = 1;
            $module_display_style = 'done';

        }
    }
    else if($_POST['submit']=='register'){
        $email = get_standard_email($email); 
	$user_count = user_get_count(array("email"=>$email));
	if($user_count == 0)
        {
            $insertData['email'] = $email;
            $insertData['fname'] = $fname;
            $insertData['lname'] = $lname;
            $insertData['gender'] = $gender;
            $insertData['country'] = $country;
            $insertData['city'] = $city;
            $insertData['password'] = $password;
            $insertData['dob'] = $dob_year . '-' . $dob_month . '-' .$dob_day;
            $insertData['dob'] = date('Y-m-d',strtotime($insertData['dob'])).'';
            $insertData['profile_id'] = generate_id("profile");
            
            if($PSModData['usertype']=='expert')
            {
                //$insertData['userdp'] = upload('userdp','image',$_FILES['userdp']);
                $insertData['bio'] = $bio;
                if(isset($_FILES['userdp']) && $_FILES['userdp']['error']==0)
                $insertData['userdp'] = imageresizecrop($_FILES['userdp'],'userdp',true,array('width'=>$PSJavascript['croptype']['dp']['sizes'][0][0],'height'=>$PSJavascript['croptype']['dp']['sizes'][0][1]),1);
                else
                $insertData['userdp'] = '';
                foreach($achievement['title'] as $key=>$achieved){
                    if($achieved && !empty($achievement['year'][$key]))
                    $achievementlist[] = array('title'=>$achieved,'year'=>$achievement['year'][$key]);
                }
                if(!empty($achievementlist))
                    $insertData['extra']['achievements'] = $achievementlist;
            }
            $insertData['extra']['fulllocation'] = $fulllocation;
            $insertData['extra'] = serialize($insertData['extra']);
            $profile_id = user_create_profile($insertData);
            if($profile_id)
            {
                mailer(array("profile_id"=>$insertData['profile_id']),'activation');
                $PSModData['server_response_text'] = 'Registration Successful';
                $module_display_style = 'done';
                $PSModData['server_response_status'] = 1;
                $PSModData['server_response_html'] = '<div class="msgbox1 text-center">
                        <h3>Thank you for registering with us.</h3>
                        <p>Your profile is created and a verification mail is sent to your Email Id<br><br>If you don\'t see the mail on your inbox, kindly check your spam folder too !<span>Thank You!!!</span></p>
                    </div>';
                if($PSModData['usertype']=='expert')
                {
                    foreach($category as $cat)
                    {
                        relation_register($profile_id,'isExpert',$cat);
                    }
                    $PSModData['server_response_html'] = '<div class="msgbox1 text-center">
                        <h3>Thank you for submitting an expert registration form!</h3>
                        <p>Expert registration requires an ADMIN approval . We are reviewing your registration form and once approved you shall be notified on your registered email id .This may take upto maximum 48 hours.Please keep checking registered mail id.<br><br>If you dont see the mail on your inbox, kindly check your spam folder too !<span>Thank You!!!</span></p>
                    </div>';
                }else
                {
                    foreach($category as $cat)
                    {
                        relation_register($profile_id,'joinedPassion',$cat);
                    }
                }

            }
            else
            {
                $PSModData['params'] = $_POST;
                $PSModData['server_response_text'] = 'Registration Failed <br>We are working to fix this ';
                $PSModData['server_response_status'] = 0;
            }
        }
        else
        {
            $PSModData['params'] = $_POST;
            $PSModData['server_response_text'] = 'Registration Failed <br>User already exists with this email id';
            $PSModData['server_response_status'] = 0;
        }
    }
}
?>