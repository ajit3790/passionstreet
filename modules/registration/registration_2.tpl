<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<?php
if($editmode == false)
{
?>

<?php
}
if($usertype != 'expert' && $editmode == false && false)
{
    ?>
    <div class="row form-group ">
        <div class="col-sm-12 text-right expert-reg-btn">
           <a class="btn btn-yellow" href="register/expert">If you are an Expert <strong>CLICK HERE</strong></a>
        </div>
     </div> 
    <?php 
} 
?>
<?php
if($server_response_html)
echo '<div class="row"><div class="col-md-12">'.$server_response_html.'</div></div>';
?>
<div class="row">
    <div class="col-md-12">
        <div class="form-wrapper">
        
        	<div class="row">
		    <div class="col-md-12"><h2 class="hstyl_2 hstyl_3 <?php echo ($usertype=='expert')?'mb-large':''; ?>"><small>*All fields are mandatory.</small><span><i class="fa fa-pencil-square-o"></i>Here You Go!!!</span></h2></div>
		</div>
        
            <form class="form1 clearfix" action="<?=$_SERVER['PHP_SELF_URL']?>" method="post" enctype="multipart/form-data">
                <span class="error-text">
                    <?=(!($server_response_html))?$server_response_text:''?>
                </span>
                <?php
                if($editmode == false || ($editmode == true && (empty($params['fname']) || empty($params['lname']))))
                { ?>
                <div class="row ">
                  <div class="col-sm-6 form-group has-error">
                    <input type="text" placeholder="First Name" value="<?=$params['fname']?>" id="fname" name="fname" required="" class="form-control">
                  </div>
                  <div class="col-sm-6 form-group has-error">
                    <input type="text" placeholder="Last Name" value="<?=$params['lname']?>" id="lname" name="lname" required="" class="form-control">
                  </div>
               </div> 
                <?php }
                if($editmode == false || ($editmode == true && empty($params['email'])))
                { ?>
                
                <div class="row form-group has-error">
                  <div class="col-sm-12 has-error">
                    <input type="email" placeholder="Email id" value="<?=$params['email']?>" id="email" name="email" required="" class="form-control">
                  </div>

               </div>
               <?php }
                if($editmode == false || ($editmode == true && empty($params['gender'])))
                { ?>
                <div class="gender row">
                    <label class="col-sm-3 col-xs-12 form-group  control-label">Gender</label>
                    <div class="radio form-group col-sm-3 col-xs-4">
                        <label>
                          <input  name="gender" id="gender" value="male" <?=($params['gender']=='Male')?'checked="checked"':''?> required="" type="radio">
                          Male
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-4">
                        <label>
                          <input  name="gender" id="gender" value="female" <?=($params['gender']=='Female')?'checked="checked"':''?> required="" type="radio">
                          Female
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-4">
                        <label>
                          <input  name="gender" id="gender" value="others" <?=($params['gender']=='Others')?'checked="checked"':''?> required="" type="radio">
                          Others
                        </label>
                     </div>   
               </div>  
               <?php }
                if($editmode == false || ($editmode == true && empty($params['dob'])))
                { ?>
                <div class=" birth row  dob dobdropdown">
                    <label class="col-sm-3 control-label form-group col-xs-12">Date of Birth</label>
                    <div class="radio form-group col-sm-3 col-xs-4">
                        <select name="dob_day" id="dob_day" class="dobday form-control" required="">
                        </select>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-4">
                        <select name="dob_month" id="dob_month" class="dobmonth form-control" required="">
                        </select>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-4">
                        <select name="dob_year" id="dob_year" class="dobyear form-control" required="">
                        </select>
                     </div>   
               </div>
               <?php }
               if($editmode == false)
               { ?>
               <div class="row ">
                  <div class="col-sm-6 form-group has-error">
                    <input name="password" id="password" type="password" placeholder="Password" required="" class="form-control" pattern=".{8,}" title="At least 8 or more characters">
                  </div>
                  <div class="col-sm-6 form-group has-error">
                    <input name="re-password" id="re-password" type="password" placeholder="Confirm Password" required="" class="form-control" pattern=".{8,}" title="Must contain at least 8 or more characters">
                  </div>
               </div>
               <?php } ?>
                <div class="row ">
                  <?php
                  if($editmode == false || ($editmode == true &&  (empty($params['country']) || empty($params['city']))))
                  { ?> 
                  <div class="col-sm-12 form-group has-error">
                     <input value="" id="placeAutocomplete" name="fulllocation" type="text" placeholder="Locality / Street Name" class="form-control">
                  </div>
                  <div class="col-sm-6 form-group has-error">
                     <input value="<?=$params['country']?>" name="country" id="country" type="text" placeholder="Country" required="" class="form-control">
                  </div>
                  <div class="col-sm-6 form-group has-error">
                    <input value="<?=$params['city']?>" name="city" id="locality" type="text" placeholder="City" required="" class="form-control">
                  </div>
                  <?php } ?>
               </div>
                <h4 class="hstyl_3"><span><?=($usertype=='expert')?'Expert in':'Select Your Passion'?></span></h4>  
                <div class="row experties">
                    <?php
                    foreach($PSParams['PSCategories'] as $PSCategorykey=>$PSCategory)
                    {
                    $extraAttr = '';
                    if(in_array($PSCategorykey,$params['passions']))
                    $extraAttr = ' checked = "checked" readonly="readonly" ';
                    ?>
                    <div class="col-sm-3 col-xs-3">
                        <label >
                            <input name="category[]" <?=$extraAttr?> value="<?=$PSCategorykey?>" type="checkbox" />
                            <img src="images/<?=$PSCategory['icon']?>" alt="<?=$PSCategory['name']?>" />
                        </label>
                    </div>

                    <?php
                    }
                    ?>
                </div>
                <?php
                if($usertype=='expert')
                {
                ?>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input name="userdp" id="userdp" type="file"  required="" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 has-error">
                        <textarea name="bio" id="bio" class="form-control" required="" placeholder="Say something about you"><?=$params['bio']?></textarea>
                        <!--textarea class="simpleeditor"><?=$params['bio']?></textarea-->
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12 hstyl_4">
                        Achievements & Recognitions
                    </div>
                    <div class="col-md-12">
                    <div class="achievementboxouter">
	                    <div class="row form-group achievementbox">   
	                    	<div class="col-sm-9">
	                        	<input placeholder="Award &amp; Achievement Title" name="achievement[title][]" class="form-control" type="text"  required="" />
	                    	</div>
		                    <div class="col-sm-3">
		                        <select name="achievement[year][]" class="form-control achievementyear" required="">
                                            <option>Year</option>
                                        </select>
		                    </div>
	                    </div>
                    </div>
                    </div>
                    <div class="col-sm-12">
                        <a class="pull-right addmore" trgtelement="achievementbox" href="javascript:void(0)">add more +</a>
                    </div>
                </div>
                <?php
                }
                ?>
                <div class="form-group  row">
                    <div class="col-sm-12">
                        <div class="g-recaptcha" style="height:80px" data-sitekey="6Le0CRATAAAAACFI2qNFo0hn9uUR3lzMXmTyNkBh"></div>
                    </div>
               </div>
                <?php
               if($editmode == false)
               { ?>
               <div class="form-group row">
                    <div class="col-sm-12 checkbox text-center">
                        <label><input type="checkbox" required=""> I acknowledge &amp; accept all <a href="terms" style="color:blue">terms &amp; conditions</a> of PASSIONSTREET.IN</label>
                    </div>
                </div>
               <?php } ?>
               <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" name="submit" class="btn btn-primary" value="<?=($editmode == true)?'completeprofile':'register'?>"><?=($editmode == true)?'Update Profile':'Create My Account'?></button> 
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
        
<script>
function add_select_fields()
{
    addDobDropdowns(".dobdropdown");
    /*var $temp = ''
    for(var $i=1;$i<=31;$i++){
        if( $i!=<?php echo ($params['dob_day'])?$params['dob_day']:-1 ; ?>)
        $temp += '<option value="'+$i+'">'+$i+'</option>';
        else
        $temp += '<option value="'+$i+'" selected>'+$i+'</option>';
        
    }
    $("#dob_day").append($temp);
    
    $temp = '';
    $monthObj = <?=json_encode($PSParams['months'])?>;
    $.each($monthObj, function($key, $value) {
        if( $key=='<?php echo (array_key_exists($params['dob_month'],$PSParams['months']))?$params['dob_month']:-1 ; ?>')
        $temp += '<option value="'+$key+'" selected>'+$value+'</option>';
        else
        $temp += '<option value="'+$key+'">'+$value+'</option>';
    });
    $("#dob_month").append($temp);
    
    $temp = ''
    for(var $i= ((new Date()).getFullYear() - 13)  ;$i>= ((new Date()).getFullYear()-100)  ;$i--){
        if( $i!=<?php echo ($params['dob_year'])?$params['dob_year']:-1 ; ?>)
        $temp += '<option value="'+$i+'">'+$i+'</option>';
        else
        $temp += '<option value="'+$i+'" selected>'+$i+'</option>';
        
    }
    $(".achievementyear").append($temp);
    $("#dob_year").append($temp);
    */
    
    $temp = '';
    $countryObj = <?=json_encode($PSParams['country'])?>;
    $.each($countryObj, function($key, $value) {
        if( $key=='<?php echo (array_key_exists($params['country'],$PSParams['country']))?$params['country']:-1 ; ?>')
        $temp += '<option value="'+$key+'" selected>'+$value+'</option>';
        else
        $temp += '<option value="'+$key+'">'+$value+'</option>';
    });
    $("#country").append($temp);
}
function add_pass_repass_check()
{
    if($("#password").val() != $("#re-password").val())
        $("#re-password").setCustomValidity('');	 
}
$(document).ready(function(){
    add_select_fields();
    add_pass_repass_check();
})    
</script>
<script type="text/javascript">
var placeSearch, autocomplete;
var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };


function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('placeAutocomplete')),
      {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  
  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      if(document.getElementById(addressType) !== null)
      document.getElementById(addressType).value = val;

    }
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
</script>
<?php
if($usertype=='expert')
{
?>
<script type="text/javascript">
$('#userdp').filestyle({
    'icon' : false,
    'buttonText' : ' Browse',
    'placeholder': 'Upload account profile photo'
});
</script>
<?php
}
$PSJsincludes['external'][] = "https://maps.googleapis.com/maps/api/js?key=".$PSParams['google_maps_key']."&libraries=places&callback=initAutocomplete";
?>