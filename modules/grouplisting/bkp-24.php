<?php
global $connection;

$userId = $_SESSION['user']['id'];
$actionName = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'joined';

$_REQUEST['page'] = isset($_REQUEST['page']) === true ? $_REQUEST['page'] : 1;
$perpage= 6;
$offset = ($_REQUEST['page']-1) * $perpage;

if ($actionName == 'manage') {
	$groups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId GROUP BY gm.groupId) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId = ? GROUP BY gm.groupId) AS temp_2 ON temp_1.groupId = temp_2.groupId AND createdBy = ? LIMIT ?, ?", array($userId, $userId, $offset, $perpage));

} elseif ($actionName == 'discover') {
	// find group by gooup's category
	$groups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId GROUP BY gm.groupId) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId != ? GROUP BY gm.groupId) AS temp_2 ON temp_2.totalMembers = temp_1.totalMembers AND temp_1.groupId = temp_2.groupId  LIMIT ?, ?", array($_SESSION['user']['id'], $offset, $perpage));
} else {
	// Your groups (joined groups)
	//$groups = $connection->fetchAll("SELECT gp_gm.* FROM (SELECT count(gm.groupId) AS totalMembers, gp.* FROM groups AS gp RIGHT JOIN group_members AS gm ON gp.groupId = gm.groupId WHERE gp.createdBy <> ? AND gm.userId = ? ) AS gp_gm WHERE totalMembers > 0 LIMIT ?", array($userId1, $userId, 6));
	$groups = $connection->fetchAll("SELECT temp_1.* FROM (SELECT gp.*, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId GROUP BY gm.groupId) AS temp_1 JOIN (SELECT gp.groupId, count(gm.userId) AS totalMembers FROM groups AS gp JOIN group_members AS gm ON gm.groupId = gp.groupId AND gm.userId = ? GROUP BY gm.groupId) AS temp_2 ON temp_1.groupId = temp_2.groupId AND createdBy <> ? LIMIT ?, ?", array($userId, $userId, $offset, $perpage));
}

$PSModData['nextlink'] = ROOT_PATH.'moduleAJX/grouplisting?page='.($_REQUEST['page']+1).'&action='.$actionName;

$PSModData['page'] 	 = $_REQUEST['page'];
$PSModData['groups'] = $groups;
$PSModData['action'] = $actionName;