<?php
if(empty($userslist['all']))
return;
?>
<div class="row tab-header">
    <div class="col-md-12">
        <select class="filter form-control options">
            <option value="all" selected="">All</option>
            <option value="member">Member</option>
            <option value="isexpert">Expert</option>
        </select>
        <ul class="nav nav-pills full-width">
          <li class="active" role="presentation"><a onclick="triggerunveil()" data-toggle="tab" role="tab" aria-controls="home" href="#all"><?=$heading?></a></li>
		  <?php
		  if($_SESSION['user']['profile_id'])
		  {
          echo '<li role="presentation"><a onclick="triggerunveil()" data-toggle="tab" role="tab" aria-controls="home" href="#following">I am Following</a></li>';
          echo '<li role="presentation"><a onclick="triggerunveil()" data-toggle="tab" role="tab" aria-controls="home" href="#follower">My Follower</a></li>';
		  }
		  ?>
	</ul>
    </div>
</div>
<div class="tab-content">
        <div role="tabpanel" class="row member-list tab-pane fade active in" id="all">
            <?php
            foreach($userslist['all'] as $user)
            {
                echo '<div class="col-md-3 col-sm-3 col-xs-6">';
                renderer_member($user,1);
                echo '</div>';
            }
            ?>
        </div>
        
		<?php 
		if($_SESSION['user']['profile_id'])
		{
			echo '<div role="tabpanel" class="row member-list tab-pane " id="following">';
            foreach($userslist['following'] as $user)
            {
                echo '<div class="col-md-3 col-sm-3 col-xs-6">';
                renderer_member($user,1);
                echo '</div>';
            }
			echo '</div>';
			
			echo '<div role="tabpanel" class="row member-list tab-pane " id="follower">';
            foreach($userslist['follower'] as $user)
            {
                echo '<div class="col-md-3 col-sm-3 col-xs-6">';
                renderer_member($user,1);
                echo '</div>';
            }
			echo '</div>';
		}
		?>
</div>
<script>
var $tempparams={};
$(".options").change(function(event){
        $tempparams['passionlevel']=$(this).val();
        /*$querystring='?';
        $.each($tempparams,function(i,v){
                $querystring+="&"+i+"="+v;
        });*/
        if($tempparams['passionlevel'] == 'all')
        $querystring = '';
        else
        $querystring = $tempparams['passionlevel'];
        location.assign(root_path+'follow/'+$querystring);
});
$(document).ready(function(){
    $(".options").children().filter(function() {
        return $(this).val() == q['passionlevel'];
    }).prop('selected', true);
})
</script>
<a href="<?=ROOT_PATH.'moduleAJX/'.$moduleName.'?pagetype=follow&memberlistpage='.($_GET['memberlistpage']+1)?>" id="memberlistnext">&nbsp;</a>
<script>
activeTab = "all";
$(document).ready(function(){
    delayfunction2(['cleverinfinitescroll.js'],'memberlistpaging','',1000);
});
function memberlistpaging(){
    $temp = ['all','following','follower'];
    $.each($temp,function(i,v){
        $('#'+v).cleverInfiniteScroll({
            contentsWrapperSelector: '#'+v,
            contentSelector: '#'+v+'>div',
            nextSelector: '#memberlistnext',
            loadImage: 'ajax-loader.gif',

            tab:v
        });
    });
}
    /*
activeTab = "all";
$(document).ready(function(){
        $temp = ['all','following','follower'];
        if(!fndefined('cleverInfiniteScroll')){
            $temp2['cleverInfiniteScroll'] = setInterval(function(){
                if(fndefined('cleverInfiniteScroll')){
                    clearInterval($temp2['cleverInfiniteScroll']);
                    $.each($temp,function(i,v){
                        $('#'+v).cleverInfiniteScroll({
                            contentsWrapperSelector: '#'+v,
                            contentSelector: '#'+v+'>div',
                            nextSelector: '#next',
                            loadImage: 'ajax-loader.gif',

                            tab:v
                        });
                    });
               };
            },300);  
        }
        else{
            $.each($temp,function(i,v){
                $('#'+v).cleverInfiniteScroll({
                    contentsWrapperSelector: '#'+v,
                    contentSelector: '#'+v+'>div',
                    nextSelector: '#next',
                    loadImage: 'ajax-loader.gif',
                    tab:v
                });
            });

        }
        
        
        
}); 
*/
</script>
<?php
if($_GET['pagetype'] == 'follow' || $_GET['pagetype'] == 'memberlisting')
{
$breadcrumbs = array();
$breadcrumbs[] = array('name'=>'Home','url'=>ROOT_PATH);
$breadcrumbs[] = array('name'=>'Members','url'=>ROOT_PATH.'follow');
$PSParams['breadcrumbs'] = $breadcrumbs;
}
?>