<?php
if(count($userslist['all'])==0)
return ;
?>
<div class="col-md-12 no-padding">
    <?php
    if($url)
    echo '<a class="more-lnk pull-right" href="'.$url.'">View all <i class="fa fa-angle-double-right"></i></a>';
    ?>
    <h2 class="hstyl_1 sm"><?=$heading?></h2>
</div>
<div class="row ">
<div class="col-md-12 carousel-wrapper">
    <i class="fa fa-angle-left"></i>
    <i class="fa fa-angle-right"></i>
    <div class="inner follow_lst" >
        <ul class="membercarousel" >
            <?php
                foreach($userslist['all'] as $user)
                {
                    echo '<li>';
                    renderer_member($user,1);
                     echo '</li>';
                }
            ?>
        </ul>
    </div>
</div>

</div>