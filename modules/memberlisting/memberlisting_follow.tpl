<?php
if(empty($userslist['all']))
{
    return;
}
?>
<div class="main-content">
<div class="inner row">
<?php
foreach($userslist['all'] as $profiledetails)
{
//print_array($profiledetails);
?>
<div class="col-md-4 col-sm-6">

<div class="hover_menu1 V2 show_nub white_bg" >
    <div class="hover_menu_contents" >
        <div class="ObjectCard">
            <div class="OC_hdr clearfix">
		<?php if($profiledetails['profile_id'] != $_SESSION['user']['profile_id'])
                echo '<button class="followbtn btn btn-primary btn-sm ajax-btn" data-action="post" data-aj="true" data-p-str="profile_id" data-t="user-follow" data-profile_id="'.$profiledetails['profile_id'].'">'.((empty($profiledetails['amifollowinghim']))?'Follow':'Unfollow').'</button>';
		?>
                <a class="name" href="<?=ROOT_PATH.'profile/'.$profiledetails['seopath']?>"><img class="pic unveil" alt="" src="<?=DEFAULT_IMG?>" data-src="<?=$profiledetails['userdp']?>"><?=$profiledetails['fullname']?></a><?php /*<small>Following you</small>*/ ?>
            </div>
            <div class="OC_body">
                <a class="name" href="<?=ROOT_PATH.'profile/'.$profiledetails['seopath']?>">
                    <img class="cvr_phto unveil " src="<?=DEFAULT_IMG?>"  alt="" data-src="<?=$profiledetails['userbg']?>" >
                    <div class="overlay">
			<?php
			
			foreach($profiledetails['passion']['joinedPassion'] as $key=>$passion)
			{
			    if($key>3)
			    {
				echo '+';
				break;
			    }
			    $imgsrc = 'images/'.$PSParams['PSCategories'][$passion]['icon'];
			    $imgalt = $PSParams['PSCategories'][$passion]['name'];
			    echo '<a href="'.ROOT_PATH.$PSParams['PSCategories'][$passion]['link'].'"><img alt="'.$imgalt.'" class="unveil" src="'.DEFAULT_IMG.'" data-src="'.$imgsrc.'"></a>';
			}
			?>
		    </div>
                </a>
            </div>
	    <div class="OC_footer"><a class="name" href="<?=ROOT_PATH.'profile/'.$profiledetails['seopath']?>"><div class="stat clearfix"><section>FOLLOWERS  <strong>(<?=$profiledetails['follower']?>)</strong></section><section>FOLLOWING <strong>(<?=$profiledetails['following']?>)</strong></section></div></a></div>
	</div>
    </div>
</div>

</div>
<?php
}
?>
<a href="<?=ROOT_PATH.'moduleAJX/'.$moduleName.'?pagetype=follow&memberlistpage='.($_GET['memberlistpage']+1)?>" id="nextmemberlist">next</a>
</div>
</div>
<script>
$(document).ready(function(){
    delayfunction2(['cleverinfinitescroll.js'],'memberlistpaging','',1000);
});
function memberlistpaging(){
	$('.memberlisting').cleverInfiniteScroll({
	    contentsWrapperSelector: '.memberlisting .main-content',
	    contentSelector: '.inner',
	    nextSelector: '#nextmemberlist',
	});
}
</script>
<?php
$PSJsincludes['external'][] = "js/cleverinfinitescroll.js";
?>