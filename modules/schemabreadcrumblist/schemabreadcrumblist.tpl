<?php
if($breadcrumbs['schema'])
{
	echo '<script type="application/ld+json">';
	echo json_encode($breadcrumbs['schema'],JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
	echo '</script>';
}
if($breadcrumbs['navigation'])
{
	echo $breadcrumbs['navigation'];
}
?>