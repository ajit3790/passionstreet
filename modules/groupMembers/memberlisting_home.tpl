<?php
if(empty($userslist['follower']) && empty($userslist['following']))
{
    return;
}
?>
<div class="row tab-header mb0">
        <div class="col-md-12">
        <?php
        if($profiledetails['profile_id'] != $_SESSION['user']['profile_id'])
        {
        	//echo '<h4 class="text-center ">'.$profiledetails['fname'].'\'s </h4>';
        	?>
		<ul class="nav nav-pills full-width">
			<li class="<?=($userslist['following'])?'active':''?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#following">Following</a></li>
			<li class="<?=(empty($userslist['following']) && $userslist['follower'])?'active':''?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#follower">Follower</a></li>
		</ul>
        	<?php
        }
        else
        {
        	?>
		<ul class="nav nav-pills full-width">
			<li class="<?=($userslist['following'])?'active':''?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#following">I am Following</a></li>
			<li class="<?=(empty($userslist['following']) && $userslist['follower'])?'active':''?>" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#follower">My Follower</a></li>
		</ul>
        	<?php
        }
        ?>
        </div>
    </div>
<div class="tab-content  bx-styl">
    <div role="tabpanel" class="member-list tab-pane  <?=($userslist['following'])?'active':''?>" id="following">
        <div class="inner clearfix follow_lst">
           <ul> 
            <?php
            foreach($userslist['following'] as $user)
            {
                echo '<li>';
                renderer_member($user);
                echo '</l1>';
            }
            ?>
            </ul>
        </div>
    </div>
    <div role="tabpanel" class="member-list tab-pane <?=(empty($userslist['following']) && $userslist['follower'])?'active':''?>" id="follower">
        <div class="inner clearfix follow_lst">
            <ul>
            <?php
            foreach($userslist['follower'] as $user)
            {
                echo '<li>';
                renderer_member($user);
                echo '</l1>';
            }
            ?>
            </ul>
        </div>
    </div>
</div>
<script>
var $tempparams={};
$(".options").change(function(event){
        $tempparams['passionlevel']=$(this).val();
        /*$querystring='?';
        $.each($tempparams,function(i,v){
                $querystring+="&"+i+"="+v;
        });*/
        if($tempparams['passionlevel'] == 'all')
        $querystring = '';
        else
        $querystring = $tempparams['passionlevel'];
        location.assign(root_path+'follow/'+$querystring);
});
$(document).ready(function(){
    $(".options").children().filter(function() {
        return $(this).val() == q['passionlevel'];
    }).prop('selected', true);
    
     
})
</script>
<!--a href="<?=ROOT_PATH.'moduleAJX/'.$moduleName.'?pagetype=userprofile'?>" id="next">next</a-->
<script>
activeTab = "following";
$(document).ready(function(){	/* 
        $temp = ['following','follower'];
        if(!fndefined('cleverInfiniteScroll')){
            $temp2['cleverInfiniteScroll'] = setInterval(function(){
                if(fndefined('cleverInfiniteScroll')){
                    clearInterval($temp2['cleverInfiniteScroll']);
                    $.each($temp,function(i,v){
                        $('#'+v).cleverInfiniteScroll({
                            contentsWrapperSelector: '#'+v,
                            contentSelector: '#'+v+'>div',
                            nextSelector: '#next',
                            loadImage: 'ajax-loader.gif',

                            tab:v
                        });
                    });
               };
            },300);  
        }
        else{
            $.each($temp,function(i,v){
                $('#'+v).cleverInfiniteScroll({
                    contentsWrapperSelector: '#'+v,
                    contentSelector: '#'+v+'>div',
                    nextSelector: '#next',
                    loadImage: 'ajax-loader.gif',
                    tab:v
                });
            });

        }
        
        
        */
        
        
}); 
</script>