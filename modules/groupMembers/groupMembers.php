<?php
global $groupId;
global $groupUserPermission;

if ($groupId == '') {
	$groupId = isset($_REQUEST['groupId']) ? $_REQUEST['groupId'] : '';
}

$_REQUEST['page'] = isset($_REQUEST['page']) === true ? $_REQUEST['page'] : 1;
$perpage 	= 12;
$offset 	= ($_REQUEST['page']-1) * $perpage;

$groupMembers = $connection->fetchAll("SELECT pr.id AS userId, pr.profile_id AS profileId, pr.fname, pr.lname, gm.role, pr.userdp FROM group_members AS gm JOIN profile AS pr ON pr.id = gm.userId WHERE gm.groupId = ? AND gm.isMember = ? LIMIT ?, ?", array($groupId, '1', $offset, $perpage));

$PSModData['nextlink'] = ROOT_PATH.'moduleAJX/groupMembers?page='.($_REQUEST['page']+1).'&groupId='.$groupId;

$PSModData['perpage']  = $perpage;
$PSModData['members']  = $groupMembers;
$PSModData['groupUserPermission']  = $groupUserPermission;

?>