<?php
    $membersCount = count($members);
    if($membersCount == 0) {
        return;
    }
?>
<div class="main-content">
    <div class="row member-list">
        <?php
            foreach ($members as $key => $member) {                
                $profileUID     = $member['userId'];
                $memberName     = ucfirst($member['fname']).' '.ucfirst($member['lname']);                
                $profileId      = $member['profileId'];
                $profileLink    = 'profile/'.str_replace(' ', '-', strtolower(trim($memberName))).'/'.$profileId;
        ?>
        <div class="col-md-3 active">        
            <div class="card-styl_2  clearfix">
                <?php
                if (isset($groupUserPermission['role']) === true ) {
                    $allEdit = false;
                    if ($groupUserPermission['role'] == 1) {
                        $allEdit = true;    
                    }
                    if ($allEdit === true) {
                        if ($member['role'] == 0) {
                            $adminText = 'Make Admin';
                            $moderatorText = 'Make Moderator';
                            $adminRole = 1;
                            $moderatorRole = 2;
                        } elseif ($member['role'] == 1){
                            $adminText = 'Remove as Admin';
                            $moderatorText = 'Change to Moderator';
                            $adminRole = 0;
                            $moderatorRole = 2;
                        } elseif ($member['role'] == 2){
                            $adminText = 'Make Admin';
                            $moderatorText = 'Remove as Moderator';
                            $adminRole = 1;
                            $moderatorRole = 0;
                        }
                ?>
                    <div class="btn-group mem-stng">
                    	<button type="button" class="btn btn-default dropdown-toggle" id="groupsettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="groupsettings">
                            <li><a href="#" class="member-admin-action" data-id="<?php echo $groupId?>" data-member="<?php echo $profileUID;?>" data-action="<?php echo $adminRole;?>"><?php echo $adminText;?></a></li>
                            <li><a href="#" class="member-moderator-action" data-id="<?php echo $groupId?>" data-member="<?php echo $profileUID;?>" data-action="<?php echo $moderatorRole;?>"><?php echo $moderatorText;?></a></li>
                            <li><a href="#" class="exit-group" data-id="<?php echo $groupId?>" data-member="<?php echo $profileUID;?>">Remove from Group</a></li>
                          </ul>
                    </div>
                <?php
                    } else if($groupUserPermission['userId'] == $member['userId']){
                ?>
                    <div class="btn-group mem-stng">
                        <button type="button" class="btn btn-default dropdown-toggle" id="groupsettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="groupsettings">
                            <li><a href="#" class="exit-group" data-id="<?php echo $groupId?>" data-member="<?php echo $profileUID;?>">Leave Group</a></li>
                          </ul>
                    </div>
                <?php
                        }
                    }
                ?>
                
                <figure>
                    <img alt="<?php echo $memberName;?>" class="unviel" src="<?=ROOT_PATH.'profiledp'?>" data-src="<?=ROOT_PATH.'files/'.$member['userdp']?>">
                    <figcaption><?php echo $memberName;?></figcaption>
                </figure>
                <div class="desc">
                    <span class="name" onclick="loadPage('<?php echo $profileLink;?>')"><?php echo $memberName;?></span>
                    <h5 onclick="loadPage('<?php echo $profileLink;?>')">Passion</h5>
                    <div onclick="loadPage('<?php echo $profileLink;?>')" class="passions"></div>
                    <div onclick="loadPage('<?php echo $profileLink;?>')" class="stat clearfix">
                        <section class="sectionpaddingadjusted">
                            <strong>0</strong> Followers
                        </section>
                        <section class="sectionpaddingadjusted">
                            <strong>0</strong> Following
                        </section>
                    </div>
                    <button id="profile_follow_" class="btn btn-primary ajax-btn " style="width:80px; left:50%; right:auto; margin-left:-40px;" data-profile_id="<?php echo  $profileId;?>" data-t="user-follow" data-p-str="profile_id" data-aj="true" data-action="post">Follow</button>             
                </div>
            </div>
        </div>
        <?php
            }
            if ($membersCount == $perpage) {
        ?>
            <span style="text-align:center"><a href="javascript:void(0);" data-nextlink="<?=$nextlink?>" id="nextMemberList" class="viewmorebtn">&nbsp;</a></span>
        <?php
            }
        ?>
    </div>
</div>
<script>
$(document).ready(function() {
    $("img").unveil();
    delayfunction2(['groupwall.js','cleverinfinitescroll.js'],'groupMembersPaging','',1000);
    
});
</script>
<?php
$PSJsincludes['external'][] = "js/groupwall.js";
$PSJsincludes['external'][] = "js/cleverinfinitescroll.js";
?>