<?php
if(!empty($_GET['type']))
{
    $snstype = $_GET['type'];
    if(empty($_GET['loggedin']))
    header('location:'.ROOT_PATH.'oauth/login'.$snstype.'.php');
    else
    {
        $crop = array("height"=>200,"width"=>200);
        $x = (array)$_SESSION['socialLogin'][$snstype];
        $insertData = array();
        if($snstype == 'fb')
        {
        $insertData['email'] = $x['email'];
        $insertData['fname'] = $x['first_name'];
        $insertData['lname'] = $x['last_name'];
        $insertData['gender'] = $x['gender'];
        }
        if($snstype == 'tw')
        {
        //$insertData['email'] = $x['email'];
        $temp = explode(" ",$x['name']);
        $insertData['email'] = $x['screen_name']."@twitter.com";
        $insertData['fname'] = $temp[0];
        $insertData['lname'] = $temp[1]?$temp[1]:'';
        $insertData['country'] = $x['location'];
        $insertData['bio'] = $x['description'];
        }
        if($snstype == 'gg')
        {
        $temp = explode(" ",$x['name']);
        $insertData['email'] = $x['email'];
        $insertData['fname'] = $temp[0];
        $insertData['lname'] = $temp[1]?$temp[1]:'';
        $insertData['gender'] = $x['gender'];
        }
        $insertData[$snstype.'id'] = $x['id'];
        $insertData['extra'] = serialize(array($snstype=>$x));
        
        //$user_count = user_get_count(array("email"=>$insertData['email'],$snstype."id"=>$insertData[$snstype.'id']));
        $user_count = user_get_count(array("email"=>$insertData['email']));
        if($user_count == 0){
            $insertData['profile_id'] = generate_id("profile");
            if($snstype == 'tw' && !empty($x['profile_image_url']))
            $insertData['userdp'] = upload_from_url('userdp',$x['profile_image_url'],$crop);    

            if($snstype == 'gg' && !empty($x['picture']))
            $insertData['userdp'] = upload_from_url('userdp',$x['picture'],$crop);
            
            if($snstype == 'fb' && !empty($x['picture']->data->url))
            $insertData['userdp'] = upload_from_url('userdp',$x['picture']->data->url,$crop);
            
            if($snstype == 'fb' && !empty($x['cover']->source))
            $insertData['userbg'] = upload_from_url('userbg',$x['cover']->source);

            $profile_id = user_create_profile_sns($insertData);
            @session_start();
            $PSData['user'] = user_profile(array("profile_id" => $insertData['profile_id']));
            $_SESSION['user']['profile_id'] = $PSData['user']['profile_id'];
            $PSJavascript['profile_id'] = $PSData['user']['profile_id'];
            $PSJavascript['login_status'] = true;
            $PSJavascript['close_window'] = 1; 
            $module_display_style = "done";
            mailer(array("profile_id"=>$_SESSION['user']['profile_id']),'welcome');
            exit();
        }
        else{
            @session_start();
            $PSData['user'] = $temp = user_profile(array("email"=>$insertData['email']));
            $_SESSION['user']['profile_id'] = $PSData['user']['profile_id'];
            $PSJavascript['profile_id'] = $PSData['user']['profile_id'];
            $PSJavascript['login_status'] = true;
            $PSJavascript['close_window'] = 1; 
            $module_display_style = "done";
            
            foreach($insertData as $key=>$value){
                if(array_key_exists($key, $temp) && empty($temp[$key])){
                    $PSUpdateArray[$key] = $value; 
                }
            }
            user_profile_update($PSUpdateArray);
            //print_array();
        }
        
    }
}
?>