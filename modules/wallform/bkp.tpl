<?php
	if (isset($groupDetail['privacyType']) === true && ($groupDetail['privacyType'] == 'public' || ($groupDetail['privacyType'] == 'private')) ) {

?>
<div class="widget-heading">	
    <h2 class="hstyl_1 sm">Publish Activity</h2>
</div>
<form class="form1" id="wall-form" method="post" enctype="multipart/form-data">
    <div class="bx-styl">
        <div class="row form-group">
            <div class="col-md-12 about">
                <textarea name="postsummary" placeholder="Write something" required class="form-control"></textarea>
            </div>
        </div>
        <div class="photo_lst wp-form-box">
        	<div class="img-preview"></div>
            <div class="item add">
            	+<input type="file" class="upload-img" target=".img-preview" multiple="true" />
            </div>
        </div>

        <!-- Start workout-->
        <div id="workout">        
        	<div class="post-log">
	    		<div class="row">
		    	   <div class="col-md-3 col-sm-3 col-xs-6 form-group">
			    	   	<div class="row">
			    	        <label class="control-label col-xs-12 ">Workout Type</label>
			    	        <div class="col-xs-12">
								<?php
								if(count($workouttypes) == 1) {
									echo '<input type="text" class="form-control" required="" readonly="" value="'.$PSParams['PSCategories'][$workouttypes[0]]['name'].'">';
									echo '<input type="hidden" name="workouttype" class="workouttype form-control" required="" readonly="" value="'.$workouttypes[0].'">';
								} else {
								?>
								<select name="workouttype" class="workouttype form-control" required>
									<option value=''>Select</option>
									<?php
									foreach($workouttypes as $passion) {
										
										echo '<option value="'.$passion.'">'.$PSParams['PSCategories'][$passion]['name'].'</option>';
									}
									?>
								</select>
								<?php
								}
								?>
							</div>
						</div>
				    </div>	
	    		
				    <div class="col-md-3 col-sm-3 col-xs-6 form-group ">
					 	<div class="row">
							<label class="control-label col-xs-12 ">Date </label>
							<div class="col-xs-12">
								<i class="fa fa-calendar"></i>
		                        <input name="log[date]" required="" style="background-color:#fff" type="text" class="datepicker form-control date date-past"  placeholder="DD/MM/YYYY" readonly>

							</div>  
						 </div>  
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="row">
							<label class="control-label col-xs-12 ">Distance</label>
							<div class="col-xs-12 distance ">
								<input name="log[distance]" required="" id="logdistance" type="text" class="form-control logcfields" placeholder="">
							</div> 
							<!--<div class="col-md-4 col-sm-4"><span class="text"><input type="hidden" name="log[distanceunit]" value="KM" />KM</span></div>-->  
						 </div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6 form-group ">
						<div class="row">
							<label class="control-label col-xs-12 ">Time taken</label>
							<div class="col-xs-12">
								<i class="fa fa-clock-o"></i>
								<input id="logtime" name="log[totaltimehh]" required="" style="background-color:#fff" data-type="hourrangeselector" data-options="0,100,2" type="text" class="datepicker form-control date date-past logcfields"  placeholder="HH:MM:SS" readonly>
							</div>
						 </div> 
					</div>
	   			</div>
	   			<div class="row distroy-clear">
					<div class="col-md-4 col-sm-4 col-xs-6">
						 <div class="row">
							<label class="control-label col-xs-12  ">Heart rate (BPM)</label>
							<div class="col-md-6 col-sm-6 col-xs-6 form-group">
								<input name="log[heartrateavg]" id="logheartrate" type="text" class="form-control logcfields logcfieldsoptional" placeholder="AVG">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6 form-group">
								<input name="log[heartratemax]" type="text" class="form-control" placeholder="MAX">
							</div>  
						 </div>    
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="row">
							<label class="control-label col-xs-12 ">Altitude (MT)</label>
							<div class="col-md-6 col-sm-6 col-xs-6 date form-group">
								<input name="log[altitudelow]" type="text" class="form-control" placeholder="MIN">
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6 date form-group">
								<input name="log[altitudehigh]" type="text" class="form-control" placeholder="MAX">
							</div>  
						 </div>     
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6 ">                                 
				 		<div class="row">
							<label class="control-label col-xs-12 ">Height &amp; Weight</label>
							<div class="col-xs-12 form-group update-box">
                                <i class="fa fa-gear" onclick="javascript:customToggle('.update-popup')"></i>
								<input type="text" onfocus="javascript:customToggle('.update-popup')" id="userHeightWidth" readonly placeholder="0 CM, 0 KG" class="form-control" value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].' CM, ':''?><?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].' KG':''?>">
								<div class="update-popup hide">
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<div class="row">
												<label class="control-label col-xs-12 ">Height</label>
												<div class="col-xs-12 date">
                                                	<input value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].'':''?>" type="text" id="userheight" placeholder="In CM" class="form-control">
												</div>
									 		</div>     
										</div>
										<div class="col-md-6 col-sm-6">
											<div class="row form-group">
												<label class="control-label col-xs-12 ">Weight</label>
												<div class="col-xs-12 date">
													<input value="<?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].'':''?>" type="text" id="userweight" placeholder="In KG" class="form-control">
												</div>
											 </div>   
										</div>
									</div>
									<div class="row">
										<input type='button' id='updateUserheightwidthbtn' class="btn btn-primary btn-sm" value="Update" />
									</div> 
								</div>                                        
							</div>
						</div>                                       
					</div>
			    </div>
	   
			   	<div class="row distroy-row">
					<div class="col-md-4 col-sm-4 col-xs-6">
						 <div class="row">
							<label class="control-label col-xs-12 ">Avg speed</label>
							<div class="col-xs-12 form-group">
								<i class="fa fa-refresh"></i>
								<input readonly id="avgspeed" type="text" name="log[avgspeed]" placeholder="0 KM/H" class="form-control">
							</div>
						 </div>    
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="row">
							<label class="control-label col-xs-12 ">Calories</label>
						   <div class="col-xs-12 form-group felt">
								<i class="fa fa-refresh"></i>
								<input readonly id="calories" type="text" name="log[calories]" placeholder="0 CALORIES" class="form-control">
							</div>                                    
						 </div>       
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div class="row">
							<label class="control-label col-xs-12">Felt</label>
							<div class="col-xs-12 form-group felt">
								<label class="icon" title="Awesome" data-placement="top" data-toggle="tooltip" data-original-title="smile"><input type="radio" name="log[felt]" value="awesome"><?=$GLOBALS['PSParams']['PSSmilies']['awesome']?>

								</label>
								<label class="icon" title="Good" data-placement="top" data-toggle="tooltip" data-original-title="excited"><input type="radio" name="log[felt]" value="good"><?=$GLOBALS['PSParams']['PSSmilies']['good']?>

								</label>
								
								<label class="icon" title="Ok" data-placement="top" data-toggle="tooltip" data-original-title="Ok"><input type="radio" name="log[felt]" value="okok"><?=$GLOBALS['PSParams']['PSSmilies']['okok']?>

								</label>
								<label class="icon" title="Awefull" data-placement="top" data-toggle="tooltip" data-original-title="sad"><input type="radio" name="log[felt]" value="awefull"><?=$GLOBALS['PSParams']['PSSmilies']['awefull']?>
								</label>
							</div>
						 </div>     
					</div>
			   	</div>
			</div>
    	</div>
        <!-- End Workout -->
        <div id="add-video" class="row form-group">
            <div class="col-md-12">
                <div class="inner">
                    <input type="url" onchange="fetchUrl()" id="fetchurl" name="video" class="form-control" placeholder="Enter url" />
                </div>
            </div>
            <div id="urlpreview" class="col-md-12"></div>
        </div>

        <div id="tag-member" class="row form-group">
        	<div class="col-md-12">
            	<div class="inner">
            		<input type="text" name="tagged_members" class="form-control input-tag-members" placeholder="Whom are you with?" />
                </div>
         	</div>
        </div>

        <!-- <div class="row controls postbox">
            <div class="imgpreviewbox"></div>
            <div class="col-xs-6 col-sm-3">
                <span class="photo"><i class="fa fa-picture-o" aria-hidden="true"></i> Add photo
                <input class="showimgpreview" target=".imgpreviewbox" type="file" multiple/></span>
            </div>
        </div> --> 

        <div class="row controls postbox">
            <div class="imgpreviewbox"></div>
            <div class="col-xs-6 col-sm-3">
                <span class="photo"><i class="fa fa-picture-o" aria-hidden="true"></i> Add photo
                <input class="showimgpreview" target=".imgpreviewbox" type="file" multiple/></span>
            </div>
        
        	<!-- <div class="col-xs-6 col-sm-3">
        		<span class="photo"><i class="fa fa-picture-o" aria-hidden="true"></i> Add Photo <input class="showimgpreview" target=".imgpreviewbox" multiple type="file"></span>
        	</div> -->
            <div class="col-xs-6 col-sm-3">
            	<span class="add-video"><i class="fa fa-film" aria-hidden="true"></i> Add Video</span>
            </div>
            <div class="col-xs-6 col-sm-3">
            	<span class="workout"><i class="fa fa-cogs" aria-hidden="true"></i> Add Workout</span>
            </div>
            <div class="col-xs-6 col-sm-3">
            	<span class="tag-member"><i class="fa fa-tags" aria-hidden="true"></i> Tag member</span>
            </div>
        </div>

        <div class="row">
        	<div class="col-sm-12 ps-act-ftr">
        		<button class="btn-primary postWall pull-right" type="button" data-id="<?php echo $groupId;?>"> POST</button>
        		<select class="form-control" name="category">
        			<option value=""> Select Categories</option>
        			<?php
        				foreach($psCategory as $key => $value) {
        					echo "<option value='$key'> $value </option>";
        				}
        			?>
        		</select>
        	</div>
        </div>
    </div>
</form>
<span class="ajax-post-response"></span>

<script type="text/javascript">
$(document).ready(function(e) {
    $(".workout").click(function(){
		$("#workout").slideDown();
	});

	$(".tag-member").click(function(){
		$("#tag-member").slideDown();
	});
    $(".add-video").click(function(){
        $("#add-video").slideDown();
    });

});
</script>

<script type="text/javascript">    
    $(".upload-img").on('change',function(){
    	console.log('ssss');
        var $temp = $(this).get(0).files;
        $target = $(this).parents('.wp-form-box').find($(this).attr('target'));       
        //$target = 'img-preview'//$(this).attr('target');       
        
        
        for($i=0;$i<$temp.length;$i++) {
        	console
            var $file = $temp[$i];
            $file['identifier'] = 'preview_'+Math.floor((Math.random() * 10) + 1);
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    $tempid = theFile['identifier'];
                    $('<img />',{'id':$tempid}).css({}).appendTo($target);
                    $("#"+$tempid).wrap("<div class='item' ></div>");
                    $temp3 = $("#"+$tempid).parent();
                    $temp3.append('<div class="overlay"></div><a href="#" class="close">X</a>');
                    //console.log(theFile);
                    $temp3.append('<input type="hidden" name="photos[]" id="field_'+$tempid+'"/>');
                    
                    var $temp2 = $("#"+$tempid).get(0);
                    $temp2.src = e.target.result;
                    $temp2.onload = function () {
                        
                        formdata = false;
                        if (window.FormData) {
                            formdata = new FormData();
                        }
                        formdata.append("file[]", theFile);
                        if (formdata) {
                            $.ajax({
                                url: root_path+'AjaxGroup.php',
                                type: "POST",
                                data: formdata,
                                processData: false,
                                contentType: false,
                                success: function(data) {
                                    $res = $.parseJSON(data);
                                    if($res.status == 405)
                                        loginModal('');
                                    else if($res.status == 200) {
                                        $x = $('body').find('#'+$res['boxid']);
                                        $x.val($res['list']['photoes'][0]);
                                    }
                                },       
                                error: function(res) {
                                }
                            });
                        }
                    }
                };
            }) ($file);

            if ($file) {
              reader.readAsDataURL($file);
            }
        }
    });

    $(document).on('click', '.postWall', function(){
    	var formdata = $("#wall-form").serialize();
    	var groupId  = $(this).attr('data-id');
    	$.ajax({
            url: root_path+'AjaxGroup.php',
            type: "POST",
            data: formdata+'&ps_uid='+ps_uid+'&groupId='+groupId+'&requestType=gwp',
            success: function(data) {
                $('.ajax-post-response').after(data);
            },
            error: function(res) {
            }       
        });
    });
</script>


<script>


function fetchUrl(){
    fetchExtUrlMeta($("#fetchurl").val(),'fetchedUrl');
}
function fetchedUrl($metalist)
{
    if($metalist['videoembed']!== undefined)
    {
        $("#urlpreview").html('<div class="post external clearfix bx-styl"><div class="flexi-video unveil" vsrc="'+$metalist['videoembed']+'" src="<?=DEFAULT_IMG?>" data-src="'+$metalist['pageimage']+'" ></div><h3>'+$metalist['title']+'</h3><p class="clearfix">'+$metalist['description']+'</p><a class="hosturl"  rel="noindex,nofollow" href="'+$metalist['url']+'">'+$metalist['host']+'</a></div>');
        $("#urlpostsummary").html($("#urlpreview").html());
        prepare_youtube_frames();
        jqueryunveil_callback();
    }
    else{
        if($metalist['pageimage'])
        {
        var tmpImg = new Image();
        tmpImg.src=$metalist['pageimage'];
        $(tmpImg).one('load',function(){
            orgWidth = tmpImg.width;
            orgHeight = tmpImg.height;
            if(orgWidth<300 || ((orgWidth/orgHeight)<(1.5)))
            $imgtype = 'thumb';
            else
            $imgtype = 'fullwidth';
            $("#urlpreview").html('<div class="post external '+$imgtype+' clearfix bx-styl"><a href="'+$metalist['url']+'" target="_blank" rel="noindex,nofollow"><div class="img-frame"><img class="unveil" src="<?=DEFAULT_IMG?>" data-src="'+$metalist['pageimage']+'" /></div><h3>'+$metalist['title']+'</h3><p class="clearfix">'+$metalist['description']+'</p></a><a class="hosturl"  rel="noindex,nofollow" href="'+$metalist['url']+'">'+$metalist['host']+'</a></div>');
            $("#urlpostsummary").html($("#urlpreview").html());
            jqueryunveil_callback();
	});
        }
        else
        {
            $("#urlpreview").html('<div class="post external fullwidth clearfix bx-styl"><a href="'+$metalist['url']+'" target="_blank" rel="noindex,nofollow"><h3>'+$metalist['title']+'</h3><p class="clearfix">'+$metalist['description']+'</p></a><a class="hosturl"  rel="noindex,nofollow" href="'+$metalist['url']+'">'+$metalist['host']+'</a></div>');
            $("#urlpostsummary").html($("#urlpreview").html());
        }
        
    }    
}
$(document).ready(function(){
    $('.postform .nav-pills').click(function(){
            //$('.typeahead').tagsinput('removeAll');
	    //$('.typeaheadwc').tagsinput('removeAll');
    });
    
    $('.postimg').each(function(){
        $(this).filestyle({
            icon : false,
            buttonText : ' Browse',
            'placeholder': 'Upload photo'
        });
    });

    if(!fndefined('tooltip')){
        $temp2['tooltip'] = setInterval(function(){
            if(fndefined('tooltip')){
                clearInterval($temp2['tooltip']);
                $('[data-toggle="tooltip"]').tooltip();
           };
        },300);  
    }
    else{
		$('[data-toggle="tooltip"]').tooltip();
    }
            

    $(".categoryworkout").change(function(){
        $temp = $(this);
        $tempParent = $temp.parent();
        $temp.parents("form").find(".posttypeworkout").html('');
        $temp.parents("form").find(".posttypeworkout").append("<option> --Activity Type-- </option>");
        if(psworkout)
        {
            $.each(psworkout[$temp.val()],function(i,v){
                $temp.parents("form").find(".posttypeworkout").append("<option value='subcategory###"+i+"'>"+v['name']+"</option>");
            });
        }
    });

    $(".category").change(function(){
        $temp = $(this);
        $tempParent = $temp.parent();
        //$.get(root_path+"PSAjax.php",{"category":$temp.val() , "type":"categorysublist-posttype"},function(data){
        //});
            $temp.parents("form").find(".posttype").html('');
            $temp.parents("form").find(".posttype").append("<option value='posttype###random'>Random</option>");
            if(passionSubcategories)
            {
                $.each(passionSubcategories[$temp.val()],function(i,v){
                    $temp.parents("form").find(".posttype").append("<option value='subcategory###"+i+"'>"+v['name']+"</option>");
                });
            }
            
            
            if(passionPosttypes)
            {
                $.each(passionPosttypes[$temp.val()],function(i,v){
                    $temp.parents("form").find(".posttype").append("<option value='posttype###"+i+"'>"+v['name']+"</option>");
                });
            }
    });
    $(".submit-post").on("change",".posttype",function(){
        if($(this).val() == 'ridelog')
        $(".post-log").removeClass('hide');
        else
        $(".post-log").addClass('hide');
            
    });
    $passionType = '';
    if(q['pagetype'] == 'passion'){
        $passionType = q['type'];
    }else if(q['pagetype'] == 'eventDetail'){
        $passionType = passiontype;
    }
    if($passionType !== ''){
        $("#category").children().filter(function() {
            return $(this).val() == $passionType;
        }).prop('selected', true).parents("#category").change();
        //$('#category option:not(:selected)').prop('disabled', true);
        $('#category option:not(:selected)').remove();
    }
    
});
$("#updateUserheightwidthbtn").click(function(){
    $height = $("#userheight").val();
    $weight = $("#userweight").val();
    $.post(root_path+"PSAjax.php?type=updateUserheightwidth",{"height":$height ,"weight":$weight},function(data){
        $("#userHeightWidth").val($height+' CM, '+$weight+' KG');
        customToggle('.update-popup');
        calculateRidelogData();
    });
        
})
$(document).ready(function(){
    $(".logcfields").blur(function(){
        $temp = 1;
        $(".logcfields").each(function(i,v){
            if(!($(v).hasClass("logcfieldsoptional")) && $(v).val().length < 1)
            $temp = 0;
        });
        if($temp == 1)
        {
            if($("#userHeightWidth").val().length == 0)
            customToggle('.update-popup');
            else
            calculateRidelogData();
        }
    });
    
});
function calculateRidelogData(){
    $params = {};
    $params['time'] = $("#logtime").val();
    $params['distance'] = $("#logdistance").val();
    $params['heartRate'] = $("#logheartrate").val();
    $params['activitytype'] = $(".workouttype").val();
    $.post(root_path+"PSAjax.php?type=calculateRidelogData",$params,function(data){
        $x = $.parseJSON(data);
        $("#calories").val($x['list']['calories']);
        $("#avgspeed").val($x['list']['speed']);
    });
}

function tagimplementation2(){
	var elt = $('.passiontags');
	var data = passiontags;
	elt.autoSuggest(data, {selectedItemProp: "text", searchObjProps: "text",usePlaceholder:true,startText:elt.attr('placeholder'),neverSubmit:true,selectionAdded:function(elem){
		//console.log(elem.data('value'));
	}});
	
	elt = $('.workouttags');
	data = workouttags;
	elt.autoSuggest(data, {selectedItemProp: "text", searchObjProps: "text",usePlaceholder:true,startText:elt.attr('placeholder'),neverSubmit:true,selectionAdded:function(elem){
		//console.log(elem.data('value'));
	}});
}


</script>

<?php
$PSJsincludes['external'][]  = "js/stickykit.js";
$PSJsincludes['external'][]  = "js/jquery.autoSuggest.js";
$PSJsincludes['external'][]  = "js/jquery.Jcrop.js";
$PSJsincludes['external'][]  = "js/bootstrap-filestyle.min.js";
$PSJsincludes['external'][]  = "js/bootstrapdatetimepicker.min.js";
$PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
$PSCssincludes['external'][] = "css/jquery.autoSuggest.css";

?>

<style>
.postform1 .about {position:relative;}
.postform1 .about .form-control{font-size:14px; color:#888; height:70px;}
.postform1 .controls span{display:block; width:100%; line-height:34px; text-align:center; background:#f6f7f9; font-size:12px; color:#000; font-weight:bold; transition:all .25s ease; cursor:pointer; border-radius:16px; position:relative;}
.postform1 .controls span:hover{background:#dddfe2;}
.postform1 .controls .fa{margin-right:5px; font-size:18px; vertical-align:bottom; line-height:inherit;}
.postform1 .controls .fa-picture-o{color:#89BE4C;}
.postform1 .controls .fa-tags{color:#01c7f1;}
.postform1 .controls .fa-film{color:#F7CC06;}
.postform1 .controls .fa-cogs{color:#FD2A6A;}
.postform1 .controls span.photo input{ opacity:0; position:absolute;left: 0;top: 0;}
.activity_lst, .tag-memb_lst {position:absolute; top:100%; left:10px; right:10px;  background:#fff; border:1px solid #ccc; border-width:0 1px 1px; padding:10px 0; z-index:10; display:none; }
.activity_lst{padding-top:40px;}
.activity_lst h3{background:#ccc; font-size:15px; font-weight:normal; width:100%; padding:0 10px 5px; position:absolute; left:0; top:0; line-height:32px; color:#000;}
.activity_lst ul, .tag-memb_lst ul{padding:0;max-height:225px; overflow:auto; margin:0;}
.activity_lst ul li, .tag-memb_lst ul li{list-style:none; cursor:pointer; padding:5px 10px; line-height:36px; font-size:16px; color:#000; font-weight:bold;}
.activity_lst ul li img, .tag-memb_lst ul li img{float:left; height:36px; margin-right:10px;}
.activity_lst ul li:hover, .tag-memb_lst ul li:hover{background:#232f3e; color:#fff;}
#workout, #tag-member, #add-video{display:none;}
#tag-member .col-md-12 .inner{position:relative; border-bottom:1px solid #ccc;}
#tag-member .col-md-12 .form-control{border:0;}
#tag-member .tag-memb_lst{left:0; right:0;}
/*#tag-member .col-md-12 .inner:before{position:absolute; content:"With"; font-size:12px; height:32px; line-height:32px; left:10px; bottom:0; background:#ccc; text-align:center; display:block; width:40px;}*/
.tag_lst{margin-bottom:15px;}
.tag_lst .item{display:inline-block; position:relative; background:#f6f7f7; line-height:24px; height:24px; padding:0 20px 0 8px; color:#000; font-size:12px; margin-right:5px; vertical-align:middle;}
.tag_lst .item a.close { position: absolute; font-size: 12px;color: #999; z-index: 1; right: 0;top: 0; cursor: pointer;opacity: .5; background: #c9c9c9; line-height:22px; width: 15px; text-align:center; height: 24px;}
.tag_lst .item:hover a.close{color:#333;}
.tag_lst .item.add{border:2px dashed #eee; padding:0 8px; color:#ccc; font-size:14px; background:none; cursor:pointer; height:26px;}


.photo_lst .item{display:inline-block; width:100px; height:100px; overflow:hidden; position:relative; margin:0 10px 10px 0;}
.photo_lst .item img{min-width:100%; min-height:100%; max-height:100%;}
.photo_lst .item .overlay{position:absolute; width:100%; height:100%; z-index:10; background:rgba(0,0,0,.5); display:none; left:0; top:0;}
.photo_lst .item a.close{color:#fff; font-size:18px; position:absolute; z-index:11; right:5px; top:5px; display:none; opacity:.8; font-weight:normal;}
.photo_lst .item:hover .overlay, .photo_lst .item:hover a.close{display:block;}
.photo_lst .item a.close:hover{opacity:1;}
.photo_lst .item.add{background:none; border:3px dashed #eee; text-align:center; line-height:90px; font-size:35px; color:#eee; font-weight:bold;  cursor:pointer;}
.photo_lst .item.add:hover, .tag_lst .item.add:hover{border-color:#ccc; color:#ccc;}
.photo_lst .item.add input{position:absolute;opacity:0; height:100px; right:0; bottom:0; display:block; cursor:pointer;}

</style>
<?php
	}
?>

