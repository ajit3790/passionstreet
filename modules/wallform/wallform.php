<?php
$workOutCategory = PSUtils::workoutCategory();
$postModule 	= '';
$postModuleId 	= '';
$postCategory 	= '';

if (isset($_REQUEST['pagetype']) === true && $_REQUEST['pagetype'] == 'community') {
	global $groupDetail;
	$postModule 	= 'community';
	$postModuleId 	= isset($_REQUEST['communityId']) === true? $_REQUEST['communityId'] : '';
	$postCategory 	= isset($groupDetail['category']) === true? $groupDetail['category'] : '';
} elseif (isset($_REQUEST['pagetype']) === true && $_REQUEST['pagetype'] == 'passion') {
	$postModule 	= 'passion';
	$postModuleId 	= isset($_REQUEST['type']) === true ? $_REQUEST['type'] : '';
	$postCategory 	= isset($_REQUEST['type']) === true ? $_REQUEST['type'] : '';
} elseif (isset($_REQUEST['pagetype']) === true && $_REQUEST['pagetype'] == 'eventDetail') {
	$postModule 	= 'event';
	$postModuleId 	= isset($_REQUEST['event_id']) === true? $_REQUEST['event_id'] : '';
	$postCategory 	= isset($PSParams['currentEvent']['category']) === true ? $PSParams['currentEvent']['category'] : '';
} elseif (isset($_REQUEST['pagetype']) === true && $_REQUEST['pagetype'] == 'home') {
	$postModule 	= 'home';
	$postModuleId 	= '';
	$postCategory 	= isset($_REQUEST['type']) === true ? $_REQUEST['type'] : '';
} elseif (isset($_REQUEST['pagetype']) === true && $_REQUEST['pagetype'] == 'userprofile') {
	$postModule 	= 'userprofile';
	$postModuleId 	= isset($_REQUEST['profile_id']) ? $_REQUEST['profile_id'] : '';
	$postCategory 	= isset($_REQUEST['type']) === true ? $_REQUEST['type'] : '';
}

$PSModData['workoutTypes']  = $workOutCategory;
$PSModData['psCategory']    = PSUtils::passionCategory();
$PSModData['postModule']  	= $postModule;
$PSModData['postCategory']  = $postCategory;
$PSModData['postModuleId']  = $postModuleId;

if ($postCategory == '' || ($postCategory != '' && array_key_exists($postCategory, $workOutCategory))) {
	$PSModData['showWorkout'] = true;
} else{
	$PSModData['showWorkout'] = false;
}

$PSModData['showPostCategory']  = false;
if ($postCategory == '') {
	$PSModData['showPostCategory']  = true;
}
?>