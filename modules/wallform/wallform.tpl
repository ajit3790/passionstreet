<div class="widget-heading"><h2 class="hstyl_1 sm">Publish Activity</h2></div>
<form class="form1" id="wall-form" method="post" enctype="multipart/form-data">
    <div class="bx-styl">
        <div class="row form-group">
            <div class="col-md-12 about">
                <textarea name="postsummary" placeholder="Write something" required class="form-control about-post"></textarea>
            </div>
        </div>
        
        <!-- Start workout-->
        <?php if ($showWorkout === true) { ?>
        <div id="workout" class="workout-panel">        
            <div class="post-log">
                <div class="row">
                   <div class="col-md-3 col-sm-3 col-xs-6 form-group">
                        <div class="row">
                            <label class="control-label col-xs-12 ">Workout Type</label>
                            <div class="col-xs-12">
                                <?php
                                    if(isset($workoutTypes[$postCategory]['name']) === true) {
                                ?>
                                <input name="workoutCat" required type="text" class="form-control" value="<?=$workoutTypes[$postCategory]['name']?>" readonly>
                                <input name="workouttype" class="workouttype" type="hidden" value="<?=$postCategory?>">
                                <?php
                                    } else {
                                ?>
                                <select name="workouttype" class="workouttype form-control" id="workouttype" required>
                                    <option value=''>Select</option>
                                    <?php
                                        foreach($workoutTypes as $key => $passion) {
                                            $passionName = $passion['name'];
                                            $selected = '';
                                            if ($key == $postCategory) {
                                                $selected = "selected='true'";
                                            }
                                            echo "<option value='$key' $selected>$passionName</option>";
                                        }
                                    ?>
                                </select>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>  
                
                    <div class="col-md-3 col-sm-3 col-xs-6 form-group ">
                        <div class="row">
                            <label class="control-label col-xs-12 ">Date </label>
                            <div class="col-xs-12">
                                <i class="fa fa-calendar"></i>
                                <input name="log[date]" required="" style="background-color:#fff" type="text" class="datepicker form-control date date-past"  placeholder="DD/MM/YYYY" readonly>

                            </div>  
                         </div>  
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="row">
                            <label class="control-label col-xs-12 ">Distance</label>
                            <div class="col-xs-12 distance ">
                                <input name="log[distance]" required="" id="logdistance" type="text" class="form-control logcfields" placeholder="">
                            </div>
                         </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 form-group ">
                        <div class="row">
                            <label class="control-label col-xs-12 ">Time taken</label>
                            <div class="col-xs-12">
                                <i class="fa fa-clock-o"></i>
                                <input id="logtime" name="log[totaltimehh]" required="" style="background-color:#fff" data-type="hourrangeselector" data-options="0,100,2" type="text" class="datepicker form-control date date-past logcfields"  placeholder="HH:MM:SS" readonly>
                            </div>
                         </div> 
                    </div>
                </div>
                <div class="row distroy-clear">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                         <div class="row">
                            <label class="control-label col-xs-12">Heart rate (BPM)</label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                                <input name="log[heartrateavg]" id="logheartrate" type="text" class="form-control logcfields logcfieldsoptional" placeholder="AVG">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                                <input name="log[heartratemax]" type="text" class="form-control" placeholder="MAX">
                            </div>  
                         </div>    
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="row">
                            <label class="control-label col-xs-12">Altitude (MT)</label>
                            <div class="col-md-6 col-sm-6 col-xs-6 date form-group">
                                <input name="log[altitudelow]" type="text" class="form-control" placeholder="MIN">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 date form-group">
                                <input name="log[altitudehigh]" type="text" class="form-control" placeholder="MAX">
                            </div>  
                         </div>     
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 ">                                 
                        <div class="row">
                            <label class="control-label col-xs-12 ">Height &amp; Weight</label>
                            <div class="col-xs-12 form-group update-box">
                                <i class="fa fa-gear" onclick="javascript:customToggle('.update-popup')"></i>
                                <input type="text" onfocus="javascript:customToggle('.update-popup')" id="userHeightWidth" readonly placeholder="0 CM, 0 KG" class="form-control" value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].' CM, ':''?><?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].' KG':''?>">
                                <div class="update-popup hide">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="row">
                                                <label class="control-label col-xs-12 ">Height</label>
                                                <div class="col-xs-12 date">
                                                    <input value="<?=($PSData['user']['extra']['userprofile']['height'])?$PSData['user']['extra']['userprofile']['height'].'':''?>" type="text" id="userheight" placeholder="In CM" class="form-control">
                                                </div>
                                            </div>     
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="row form-group">
                                                <label class="control-label col-xs-12 ">Weight</label>
                                                <div class="col-xs-12 date">
                                                    <input value="<?=($PSData['user']['extra']['userprofile']['weight'])?$PSData['user']['extra']['userprofile']['weight'].'':''?>" type="text" id="userweight" placeholder="In KG" class="form-control">
                                                </div>
                                             </div>   
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input type='button' id='updateUserheightwidthbtn' class="btn btn-primary btn-sm" value="Update" />
                                    </div> 
                                </div>                                        
                            </div>
                        </div>                                       
                    </div>
                </div>
       
                <div class="row distroy-row">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                         <div class="row">
                            <label class="control-label col-xs-12 ">Avg speed</label>
                            <div class="col-xs-12 form-group">
                                <i class="fa fa-refresh"></i>
                                <input readonly id="avgspeed" type="text" name="log[avgspeed]" placeholder="0 KM/H" class="form-control">
                            </div>
                         </div>    
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="row">
                            <label class="control-label col-xs-12 ">Calories</label>
                           <div class="col-xs-12 form-group felt">
                                <i class="fa fa-refresh"></i>
                                <input readonly id="calories" type="text" name="log[calories]" placeholder="0 CALORIES" class="form-control">
                            </div>                                    
                         </div>       
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="row">
                            <label class="control-label col-xs-12">Felt</label>
                            <div class="col-xs-12 form-group felt">
                                <label class="icon" title="Awesome" data-placement="top" data-toggle="tooltip" data-original-title="smile"><input type="radio" name="log[felt]" value="awesome"><?=$GLOBALS['PSParams']['PSSmilies']['awesome']?>

                                </label>
                                <label class="icon" title="Good" data-placement="top" data-toggle="tooltip" data-original-title="excited"><input type="radio" name="log[felt]" value="good"><?=$GLOBALS['PSParams']['PSSmilies']['good']?>

                                </label>
                                
                                <label class="icon" title="Ok" data-placement="top" data-toggle="tooltip" data-original-title="Ok"><input type="radio" name="log[felt]" value="okok"><?=$GLOBALS['PSParams']['PSSmilies']['okok']?>

                                </label>
                                <label class="icon" title="Awefull" data-placement="top" data-toggle="tooltip" data-original-title="sad"><input type="radio" name="log[felt]" value="awefull"><?=$GLOBALS['PSParams']['PSSmilies']['awefull']?>
                                </label>
                            </div>
                         </div>     
                    </div>
                </div>
            </div>
        </div>
        <!-- End Workout -->
        <?php } ?>        
        
        <!-- End Video -->
        <div id="add-video" class="row form-group">
            <div class="col-md-12">
                <div class="inner">
                    <input type="url" id="fetchurl" name="video" class="form-control" placeholder="Enter url" />
                    <input type="hidden" name="videoMeta" class="post-url-meta">
                </div>
            </div>
            <div id="urlpreview" class="col-md-12"></div>
        </div>
        <!-- End Video -->

        <!-- Start member tagging -->
        <div id="tag-member" class="row form-group">
            <div class="col-md-12">
                <div class="inner">
                    <input type="text" name="tagged_members" id="pf-tag-member" class="form-control" placeholder="Whom are you with ?" />
                    <input type="hidden" name="membersId" id="pf-tag-member-id">
                    <input type="hidden" name="membersPID" id="pf-tag-member-pid">
                </div>
            </div>
        </div>
        <!-- End member tagging -->

        <!-- Start action buttons -->
        <div class="row controls postbox">
            <div class="col-xs-12 imgpreviewbox photo_lst"></div>
            <div class="col-xs-3">
                <span class="photo"><i class="fa fa-picture-o" aria-hidden="true"></i> <small>Add photo</small>
                <input class="form-upload-preview" target=".imgpreviewbox" type="file" multiple/></span>
            </div>
        
            <div class="col-xs-3">
                <span class="add-video"><i class="fa fa-film" aria-hidden="true"></i><small> Add Video</small></span>
            </div>
            <?php if ($showWorkout === true) { ?>
            <div class="col-xs-3">
                <span class="workout"><i class="fa fa-cogs" aria-hidden="true"></i> <small>Add Workout</small></span>
            </div>
            <?php }?>
            <div class="col-xs-3">
                <span class="tag-member"><i class="fa fa-tags" aria-hidden="true"></i> <small>Tag member</small></span>
            </div>
        </div>
        <!-- End action buttons -->

        <!-- Start post buttons -->
        <div class="row ps-act-ftr">
            <div class="col-sm-12">
                <button class="btn btn-primary wall-post-btn pull-right" type="button" data-id="<?php echo $postModuleId;?>" data-entity="<?php echo $postModule;?>" data-init='false'> POST</button>
                <?php
                    if ($showPostCategory === false) {
                        echo "<input type='hidden' name='category' value='$postCategory'>";
                    } else {
                        $categogyDropDown  = '';
                        $categogyDropDown .= '<select name="category" class="form-control wall-category">';
                        $categogyDropDown .= '<option value="">Select Activity</option>';
                        foreach ($psCategory as $key => $categoryData) {
                            $categogyDropDown .= '<option value="'.$key.'">'.$categoryData['name'].'</option>';
                        }
                        $categogyDropDown .= '</select>';
                        echo $categogyDropDown;
                    }
                ?>
            </div>
        </div>
        <!-- End post buttons -->
    </div>
</form>
<span class="ajax-post-response"></span>


<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="js/jquery-ui.js" ></script>
<?php
    //$PSJsincludes['external'][]  = "js/jquery.autoSuggest.js";
    $PSJsincludes['external'][]  = "js/bootstrap-filestyle.min.js";
    $PSJsincludes['external'][]  = "js/bootstrapdatetimepicker.min.js";
    $PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
?>

<script type="text/javascript">
function postFormValidate() {
    var isValid = true;
    var aboutPost = $('.about-post').val();
    var aboutMandatory = true;

    var isWorkoutAvailable = $('#workout').hasClass('workout-panel');
    if (isWorkoutAvailable === true) {
        // Workout validation
        if ($("#workout").is(":hidden") === false) {
            aboutMandatory = false;
            if ($('.workouttype').val() == '') {
                isValid = false;
                showToolTipMsg($('.workouttype'),"Please fill this");
            }
            if ($('.datepicker').val() == '') {
                isValid = false;
                showToolTipMsg($('.datepicker'),"Please fill this");
            }
            if ($('#logdistance').val() == '') {
                isValid = false;
                showToolTipMsg($('#logdistance'),"Please fill this");
            }
            if ($('#logtime').val() == '') {
                isValid = false;
                showToolTipMsg($('#logtime'),"Please fill this");
            }
        } else {
            aboutMandatory = true;
        }
    }
    // Video validation
    if ($("#add-video").is(":hidden") === false) {
        if ($('#fetchurl').val() == '') {
            isValid = false;
            showToolTipMsg($('#fetchurl'),"Please fill this");
        }
        aboutMandatory = false;
    }

    if (aboutPost == '' && aboutMandatory === true) {
        isValid = false;
        showToolTipMsg($('.about-post'),"Please fill this");
    }

    // Member Tagging validation
    if ($("#tag-member").is(":hidden") === false) {
        if ($('.input-tag-members').val() == '') {
            isValid = false;
            showToolTipMsg($('.input-tag-members'),"Please fill this");
        }
    }
    
    if (isWorkoutAvailable === true) {
        if($("#workout").is(":hidden") === true) {
            if ($('.wall-category').val() == '') {
                //isValid = false;
                $('.wall-category').show();
                //showToolTipMsg($('.wall-category'),"Please fill this");
            }
        } else {
            var category = $('.workouttype').val();
            $('.wall-category').val(category);
            $('.wall-category').hide();
        }
    }
    
    return isValid;
}


function validateUrl(url) {
  return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(url);
}

function fetchedUrl($metalist) {
    var urlMetaString = JSON.stringify($metalist);
    
    $('.post-url-meta').val(urlMetaString);
    if($metalist['videoembed']!== undefined) {
        var url_string = $metalist['pageimage'];
        var urlData = new URL(url_string);
        var thumbnail = urlData.searchParams.get("psurl");
        
        $("#urlpreview").html('<div class="post external clearfix bx-styl"><div class="flexi-video unveil" vsrc="'+$metalist['videoembed']+'" src="<?=DEFAULT_IMG?>" data-src="'+thumbnail+'" ></div><h3>'+$metalist['title']+'</h3><p class="clearfix">'+$metalist['description']+'</p><a class="hosturl"  rel="noindex,nofollow" href="'+$metalist['url']+'">'+$metalist['host']+'</a></div>');
        $("#urlpostsummary").html($("#urlpreview").html());
        prepare_youtube_frames();
        jqueryunveil_callback();
    } else {
        
        if($metalist['pageimage']) {
            var url_string = $metalist['pageimage'];
            var urlData = new URL(url_string);
            var thumbnail = urlData.searchParams.get("psurl");

            var tmpImg = new Image();
            tmpImg.src = thumbnail;//$metalist['pageimage'];
            $(tmpImg).one('load',function(){
                orgWidth = tmpImg.width;
                orgHeight = tmpImg.height;
                if(orgWidth<300 || ((orgWidth/orgHeight)<(1.5)))
                $imgtype = 'thumb';
                else
                $imgtype = 'fullwidth';
                $("#urlpreview").html('<div class="post external '+$imgtype+' clearfix bx-styl"><a href="'+$metalist['url']+'" target="_blank" rel="noindex,nofollow"><div class="img-frame"><img class="unveil" src="<?=DEFAULT_IMG?>" data-src="'+thumbnail+'" /></div><h3>'+$metalist['title']+'</h3><p class="clearfix">'+$metalist['description']+'</p></a><a class="hosturl"  rel="noindex,nofollow" href="'+$metalist['url']+'">'+$metalist['host']+'</a></div>');
                $("#urlpostsummary").html($("#urlpreview").html());
                jqueryunveil_callback();
            });
        } else {
            $("#urlpreview").html('<div class="post external fullwidth clearfix bx-styl"><a href="'+$metalist['url']+'" target="_blank" rel="noindex,nofollow"><h3>'+$metalist['title']+'</h3><p class="clearfix">'+$metalist['description']+'</p></a><a class="hosturl"  rel="noindex,nofollow" href="'+$metalist['url']+'">'+$metalist['host']+'</a></div>');
            $("#urlpostsummary").html($("#urlpreview").html());
        }
    }    
}

function fetchUrl() {
    var url = $('#fetchurl').val();
    var isValidUrl = validateUrl(url);   
    if (isValidUrl === true) {
        $("#urlpreview").html('<i class="fa fa-spinner fa-spin" style="margin-top:-35px;"></i>');
        fetchExtUrlMeta(url,'fetchedUrl');
    } else {
        showToolTipMsg($('#fetchurl'),"Please enter valid url");
    }
}

var timeoutId = 0;
$(document).on('keyup', '#fetchurl', function(){
    clearTimeout(timeoutId); // doesn't matter if it's 0
    timeoutId = setTimeout(fetchUrl, 2000);
});

$(document).ready(function(e) {
    $(document).on('focus', '.about-post', function(){
        $('.ps-act-ftr').show();
    });

    $(".workout").click(function() {
        if($("#workout").is(":hidden")) {
            $("#workout").slideDown();
            $('.ps-act-ftr').show();
            $('.wall-category').hide();
        } else {
            var workoutType = $('.workouttype').val();
            var logdistance = $('#logdistance').val();

            var date = $("input[name='log[date]']").val();
            var time = $("input[name='log[totaltimehh]']").val();
            
            if (workoutType !='' && logdistance != '' && date != '' && time != '') {
                $('.wall-category').val(workoutType);
                $('.ps-act-ftr').show();
            } else {
                var about = $('.about-post').val();
                if (about == '') {
                    $('.wall-category').show();
                    $('.ps-act-ftr').hide();
                }
            }
            $("#workout").slideUp();
        }
    });

    $(".tag-member").click(function(){
        if($("#tag-member").is(":hidden")) {
            $("#tag-member").slideDown();
            $('.ps-act-ftr').show();
        } else {
            $("#tag-member").slideUp();
        }
    });

    $(".add-video").click(function(){
        if($("#add-video").is(":hidden")) {
            $("#add-video").slideDown();
            $('.ps-act-ftr').show();
        } else{
            $("#add-video").slideUp();    
        }
    });
});

$(document).on('click', '.wall-post-btn', function(){
    var isPendingPost = $('.wall-post-btn').attr('data-init');
    if (isPendingPost == 'true') {
        return false;
    }

    var isValid  = postFormValidate();
    if (isValid === false) {
        return false;
    }
    $('.wall-post-btn').attr('data-init', 'true');

    var fdata = $("#wall-form").serialize();
    var fmId  = $(this).attr('data-id');
    var fmodule = $(this).attr('data-entity');
    $.ajax({
        url: root_path+'AjaxGroup.php?requestType=publishActivity',
        type: "POST",
        data: fdata+'&ps_uid='+ps_uid+'&fmId='+fmId+'&fm='+fmodule,
        beforeSend: function() {
            $('.wall-post-btn').html('<i class="fa fa-refresh fa-spin"></i> Processing');
            $('#wall-form')[0].reset();
            $('#pf-tag-member-id').val('');
            $('#pf-tag-member-pid').val('');
            $('#fetchurl').val('');
            $('.imgpreviewbox .item').remove();

            $("#tag-member").slideUp();
            $("#add-video").slideUp();
            var isWorkoutAvailable = $('#workout').hasClass('workout-panel');
            if (isWorkoutAvailable === true) {
                $("#workout").slideUp();
            }
        },
        success: function(data) {
            $('.ps-act-ftr').hide();
            $('.wall-post-btn').attr('data-init', 'false');
            $('.wall-post-btn').html('POST');
            $('.ajax-post-response').after(data);
            $('[data-toggle="tooltip"]').tooltip();
            onContentAdd();
        },
        error: function(res) {
        }
    });
});

$(document).ready(function(){
    if(!fndefined('tooltip')){
        $temp2['tooltip'] = setInterval(function(){
            if(fndefined('tooltip')){
                clearInterval($temp2['tooltip']);
                $('[data-toggle="tooltip"]').tooltip();
           };
        },300);  
    } else{
        $('[data-toggle="tooltip"]').tooltip();
    }

    $(".categoryworkout").change(function(){
        $temp = $(this);
        $tempParent = $temp.parent();
        $temp.parents("form").find(".posttypeworkout").html('');
        $temp.parents("form").find(".posttypeworkout").append("<option> --Activity Type-- </option>");
        if(psworkout)
        {
            $.each(psworkout[$temp.val()],function(i,v){
                $temp.parents("form").find(".posttypeworkout").append("<option value='subcategory###"+i+"'>"+v['name']+"</option>");
            });
        }
    });
});

$("#updateUserheightwidthbtn").click(function(){
    $height = $("#userheight").val();
    $weight = $("#userweight").val();
    $.post(root_path+"PSAjax.php?type=updateUserheightwidth",{"height":$height ,"weight":$weight},function(data){
        $("#userHeightWidth").val($height+' CM, '+$weight+' KG');
        customToggle('.update-popup');
        calculateRidelogData();
    });
});

$(document).ready(function(){
    $(".logcfields").blur(function(){
        $temp = 1;
        $(".logcfields").each(function(i,v){
            if(!($(v).hasClass("logcfieldsoptional")) && $(v).val().length < 1)
            $temp = 0;
        });
        if($temp == 1)
        {
            if($("#userHeightWidth").val().length == 0)
            customToggle('.update-popup');
            else
            calculateRidelogData();
        }
    });
});

function calculateRidelogData(){
    $params = {};
    $params['time'] = $("#logtime").val();
    $params['distance'] = $("#logdistance").val();
    $params['heartRate'] = $("#logheartrate").val();
    $params['activitytype'] = $(".workouttype").val();
    $.post(root_path+"PSAjax.php?type=calculateRidelogData",$params,function(data){
        $x = $.parseJSON(data);
        $("#calories").val($x['list']['calories']);
        $("#avgspeed").val($x['list']['speed']);
    });
}

function split( val ) {
  return val.split( /,\s*/ );
}
function extractLast( term ) {
  return split( term ).pop();
}

$("#pf-tag-member").autocomplete({
    source: function (request, response)  {
      $.ajax({
        url: root_path+"PSAutocomplete.php?type=tagMember",
        dataType: "json",
        data:{
          term: extractLast(request.term),
          taggedIds: $('#pf-tag-member-id').val()
        },
        success: function (data) {
          response(data);
        }
      });
    },
    minLength: 1,
    select: function (event, ui) {
      var selectedId    = $('#pf-tag-member-id').val();
      var selectedPId   = $('#pf-tag-member-pid').val();
      if (selectedId == '') {
        $('#pf-tag-member-id').val(ui.item.id);  
        $('#pf-tag-member-pid').val(ui.item.profile_id);
      } else {
        $('#pf-tag-member-id').val(selectedId+','+ui.item.id);
        $('#pf-tag-member-pid').val(selectedPId+','+ui.item.profile_id);
      }

      var terms = split( this.value );
      // remove the current input
      terms.pop();
      // add the selected item
      terms.push( ui.item.value );
      // add placeholder to get the comma-and-space at the end
      terms.push( "" );
      this.value = terms.join( ", " );
      return false;
    }    
}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
return $( "<li></li>" )
    .data( "item.autocomplete", item)
    .append( "<a class='myclass' customattribute='" + item.value + "'> <img src='"+root_path+"profiledp' width='40px' height='40px'> " + item.value + "</a>" )
    .appendTo(ul);
}
</script>
<style>
.wallform .about {position:relative;}
.wallform .about .form-control{font-size:14px; color:#888; height:70px;}
.wallform .controls{margin-bottom:15px;}
.wallform .controls span{display:block; width:100%; line-height:34px; text-align:center; background:#f6f7f9; font-size:12px; color:#000; font-weight:bold; transition:all .25s ease; cursor:pointer;  position:relative; border-radius:16px;}
.wallform .controls span:hover{background:#dddfe2;}
.wallform .controls .fa{margin-right:5px; font-size:18px; vertical-align:bottom; line-height:inherit;}
.wallform .controls .fa-picture-o{color:#89BE4C;}
.wallform .controls .fa-tags{color:#01c7f1;}
.wallform .controls .fa-film{color:#F7CC06;}
.wallform .controls .fa-cogs{color:#FD2A6A;}
.wallform .controls span.photo input{ opacity:0; position:absolute;left: 0;top: 0;}
.activity_lst, .tag-memb_lst {position:absolute; top:100%; left:10px; right:10px;  background:#fff; border:1px solid #ccc; border-width:0 1px 1px; padding:10px 0; z-index:10; display:none; }
.activity_lst{padding-top:40px;}
.activity_lst h3{background:#ccc; font-size:15px; font-weight:normal; width:100%; padding:0 10px 5px; position:absolute; left:0; top:0; line-height:32px; color:#000;}
.activity_lst ul, .tag-memb_lst ul{padding:0;max-height:225px; overflow:auto; margin:0;}
.activity_lst ul li, .tag-memb_lst ul li{list-style:none; cursor:pointer; padding:5px 10px; line-height:36px; font-size:16px; color:#000; font-weight:bold;}
.activity_lst ul li img, .tag-memb_lst ul li img{float:left; height:36px; margin-right:10px;}
.activity_lst ul li:hover, .tag-memb_lst ul li:hover{background:#232f3e; color:#fff;}
#workout, #tag-member, #add-video{display:none;}
#tag-member .col-md-12 .inner{position:relative; border-bottom:1px solid #ccc;}
#tag-member .col-md-12 .form-control{border:0;}
#tag-member .tag-memb_lst{left:0; right:0;}

#add-video .col-md-12 .inner{position:relative; border-bottom:1px solid #ccc;}
#add-video .col-md-12 .form-control{border:0;}

/*#tag-member .col-md-12 .inner:before{position:absolute; content:"With"; font-size:12px; height:32px; line-height:32px; left:10px; bottom:0; background:#ccc; text-align:center; display:block; width:40px;}*/
.tag_lst{margin-bottom:15px;}
.tag_lst .item{display:inline-block; position:relative; background:#f6f7f7; line-height:24px; height:24px; padding:0 20px 0 8px; color:#000; font-size:12px; margin-right:5px; vertical-align:middle;}
.tag_lst .item a.close { position: absolute; font-size: 12px;color: #999; z-index: 1; right: 0;top: 0; cursor: pointer;opacity: .5; background: #c9c9c9; line-height:22px; width: 15px; text-align:center; height: 24px;}
.tag_lst .item:hover a.close{color:#333;}
.tag_lst .item.add{border:2px dashed #eee; padding:0 8px; color:#ccc; font-size:14px; background:none; cursor:pointer; height:26px;}


.photo_lst .item{display:inline-block; width:100px; height:100px; overflow:hidden; position:relative; margin:0 10px 10px 0;}
.photo_lst .item img{min-width:100%; min-height:100%; max-height:100%;}
.photo_lst .item .overlay{position:absolute; width:100%; height:100%; z-index:10; background:rgba(0,0,0,.5); display:none; left:0; top:0;}
.photo_lst .item .close{color:#fff; font-size:18px; position:absolute; z-index:11; right:5px; top:5px; display:none; opacity:.8; font-weight:normal;}
.photo_lst .item:hover .overlay, .photo_lst .item:hover .close{display:block;}
.photo_lst .item .close:hover{opacity:1;}
.photo_lst .item.add{background:none; border:3px dashed #eee; text-align:center; line-height:90px; font-size:35px; color:#eee; font-weight:bold;  cursor:pointer;}
.photo_lst .item.add:hover, .tag_lst .item.add:hover{border-color:#ccc; color:#ccc;}
.photo_lst .item.add input{position:absolute;opacity:0; height:100px; right:0; bottom:0; display:block; cursor:pointer;}
.wall-category{float:right; width:180px; margin-right:10px;}
.ps-act-ftr{margin:0 -20px -20px; position:relative; padding:10px; background:#fafafa; border-top:1px solid #d5d5d5; display: none;}
/*.ps-act-ftr select{float: right; width: 200px; margin-right: 10px;}*/
@media screen and (max-width:767px){
    .wallform .controls span{margin-bottom:10px;}
    .ps-act-ftr{margin:0 -10px -20px;}
    .wallform .controls span, .wallform .controls span:hover{background:none; line-height:inherit; margin-bottom:0; text-align:left;}
    .wallform .controls span small{display:none;}
    .wallform .about .form-control{margin-bottom:0;}
}
</style>