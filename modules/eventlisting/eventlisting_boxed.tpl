<?php
if(count($eventslist['upcoming'])==0)
return ;
?>
<div class="row">
<div class="col-md-12">
    <h1 class="hstyl_1"><?=$headertext?></h1>
</div>
</div>

<div class="row">
<?php
foreach($eventslist['upcoming'] as $event){
    echo '<div class="card col-md-4 col-sm-6">';
    renderer_event($event);
    echo '</div>';
}
?>
</div>