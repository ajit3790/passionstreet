<?php
if(!empty($headertext))
{
	?>
	<div class="row hide">
	<div class="col-md-12">
	    <a href="https://passionstreet.in/events/create" class="create-evnt btn">Create Event</a>	
	    <?php 
	    if($headerlink)
	    {
	    $h1 = '<a href="'.$headerlink.'">';
	    $h2 = '</a>';
	    }
	    echo '<h1 class="hstyl_1">'.$h1.$headertext.$h2.'</h1>';
	    ?>
	</div>
	</div>
	<?php
}
if(count($eventslist) >1)
{
?>
<div class="row">
<div class="col-md-12">
<div class="tab-header">
    <select class="filter form-control options">
        <option value="all" selected="">All</option>
        <?php
        foreach($PSParams['PSCategories'] as $PSCategorykey=>$PSCategory)
        {?>
        <option value="<?=$PSCategorykey?>"><?=$PSCategory['name']?></option>
        <?php }
        ?>
    </select>
    <ul class="nav nav-pills full-width">
      <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#all"><?=$heading1?></a></li>
      <li role="presentation" class="<?=(($eventslist['myevents'])?'':'hide')?>"><a data-toggle="tab" role="tab" aria-controls="home" href="#myevents"><?=$heading2?></a></li>
      <li role="presentation"><a  href="events/create">Create Event</a></li>
    </ul>
</div>
<?php
}
?>
<div class="tab-content">
    <div role="tabpanel" class="row tab-pane fade active in" id="all">
       <div class="col-md-12">
       	<div class="row  card-styl_3">
           <?php
           foreach($eventslist['all'] as $event)
           {
		$datestr1 = '';
		$datestr2 = '';
		$eventdateData = getdate(strtotime($event['datestart']));
		?>
	    <div class="col-md-4">
                <div class="bx-styl clearfix">
                    <figure>
                            <a href="<?=ROOT_PATH?>event/<?=$event['seopath']?>"  title="<?=$event['eventname']?>"><img class="unveil" alt="<?=$event['eventname']?>" src="<?=DEFAULT_IMG_HORIZONTAL?>" data-src="<?=$event['eventpic']?>"></a>
                            
                            <div class="socialbar_1 circle">
			      <?php
			      $event['eventname'] = strip_tags($event['eventname']);
			      $event['description'] = strip_tags($event['description']);
			      ?>
	                      <a class="customshare fb" data-type="fb" data-title="<?=$event['eventname']?>" data-description="<?=htmlentities(strip_tags($event['description']),ENT_QUOTES)?>" data-image="<?=$event['eventpic']?>" data-shareurl="<?=ROOT_PATH?>event/<?=$event['seopath']?>"><i class="fa fa-facebook"></i></a>
	                      <a class="customshare twtr" data-type="tw"  data-title="<?=$event['eventname']?>" data-description="<?=htmlentities(strip_tags($event['description']),ENT_QUOTES)?>" data-image="<?=$event['eventpic']?>" data-shareurl="<?=ROOT_PATH?>event/<?=$event['seopath']?>"><i class="fa fa-twitter"></i></a>
	                      <a class="customshare gplus" data-type="gg" data-title="<?=$event['eventname']?>" data-description="<?=htmlentities(strip_tags($event['description']),ENT_QUOTES)?>" data-image="<?=$event['eventpic']?>"  data-shareurl="<?=ROOT_PATH?>event/<?=$event['seopath']?>"><i class="fa fa-google-plus"></i></a>
	                   </div>
                            
                    </figure>
                    <div class="date">
                        <span><?=$eventdateData['mday']?></span>
                        <span><?=ucfirst($PSParams['months'][$eventdateData['mon']])?></span>
                    </div>
                    <div class="description">
                    	<mark>EVENT</mark>
                        <h4><a href="event/<?=$event['seopath']?>" title="<?=$event['eventname']?>"><?=$event['eventname']?></a></h4>
                        <p><?=$event['description']?></p>
                    </div>
                    <div class="footer" >
                        <?php
                        $tdate = date("Y-m-d H:i:s");
                        ?>
                    	<a href="event/<?=$event['seopath']?>"  title="<?=$event['eventname']?>" class="btn btn-primary"><?=($event['source'] == 'aggregated')?'View Event':(($event['tickettype'] == 'paid' && $event['bookingenddate'] >=  $tdate)?'Join Now':'View Event')?></a>
                    	<?php
                        if($event['bookingenddate'] <  $tdate)
                        {
                        }
			else if($event['tickettype'] == 'paid')
			{
				$ticketvaluerangeArray = explode(',',$event['ticketvaluerange']);
				if($ticketvaluerangeArray[0] == $ticketvaluerangeArray[1])
				echo '₹ '.$ticketvaluerangeArray[0];
				else 
				echo '₹ '.$ticketvaluerangeArray[0].' onwards';
			}
			else
			{
				echo 'Free';
			}
			?>
                    </div> 
                </div>
            </div>
           <?php
           }
           ?>
           </div>
       </div>
    </div>
    <?php
    if($eventslist['myevents'])
    { 
    ?>
    <div role="tabpanel" class="row member-list tab-pane" id="myevents">
       <div class="col-md-12">
       	   <div class="row  card-styl_3">
           <?php
           foreach($eventslist['myevents'] as $event)
           
           {
		    $datestr1 = '';
			$datestr2 = '';
			$eventdateData = getdate(strtotime($event['datestart']));
			?>
		   <div class="col-md-4">
                <div class="bx-styl clearfix">
                    <figure>
                            <a href="event/<?=$event['seopath']?>" title="<?=$event['eventname']?>"><img class="unveil" alt="<?=$event['eventname']?>" src="<?=DEFAULT_IMG?>" data-src="<?=$event['eventpic']?>"></a>
                    </figure>
                    <div class="date">
                        <span><?=$eventdateData['mday']?></span>
                        <span><?=ucfirst($PSParams['months'][$eventdateData['mon']])?></span>
                    </div>
                    <div class="description">
                        <h4><a href="event/<?=$event['seopath']?>"  title="<?=$event['eventname']?>"><?=$event['eventname']?></a></h4>
                        <p><?=strip_tags($event['description'])?></p>
                    </div>
					<div class="footer" style="width:calc(100% - 30px)">
                    	<a href="events/publish/<?=$event['event_id']?>" class="btn btn-primary pull-left">Edit Event</a>
                    	<a href="event/<?=$event['seopath']?>" class="btn btn-primary">View Event</a>
                    	
                    </div> 
                </div>
            </div>
           <?php
           }
           ?>
           </div>
       </div>
    </div>
    <?php
    }
    ?>
</div>
</div>
</div>
<?php if($_GET['pagetype'] == 'eventListing'){ ?>
<ul class="pager">
        <li class="<?=(($_GET['eventlistpage'] >= 2)?'':'hide')?>"><a href="<?=$headerlink?>/<?=($_GET['eventlistpage'] -1)?>">&larr; Previous</a></li>
        <li class=""><a href="<?=$headerlink?>/<?=($_GET['eventlistpage'] +1)?>">Next &rarr;</a></li>
</ul>
<?php } ?>
<script>
var $tempparams={};
$(".options").change(function(event){
        $temp=$(this).val();
	if(typeof q['eventlistby'] !='undefined')
	{
		if(q['eventlistby'] == 'passion')
		location.assign(root_path+'events/events-in-'+$temp+'-passion');
		else if(q['eventlistby'] == 'city')
		location.assign(root_path+'events/'+$temp+'-events-in-'+q['city']);
		else if(q['eventlistby'] == 'citypassion')
		location.assign(root_path+'events/'+$temp+'-events-in-'+q['city']);
	}
        else if($temp == 'all')
        location.assign(root_path+'events');
        else
        location.assign(root_path+'events/events-in-'+$temp+'-passion');
        // location.assign(root_path+'passion/'+$temp+'/events');
        
});
$(document).ready(function(){
    $(".options").children().filter(function() {
        return $(this).val() == q['passion'];
    }).prop('selected', true);
})
</script>
<?php
/*
?>
<a href="<?=ROOT_PATH.'moduleAJX/'.$moduleName.'?pagetype=eventListing'?>" id="next">next</a>
<script>
activeTab = "all";
$(document).ready(function(){
        $temp = ['all','myevents'];
        if(!fndefined('cleverInfiniteScroll')){
            $temp2['cleverInfiniteScroll'] = setInterval(function(){
                if(fndefined('cleverInfiniteScroll')){
                    clearInterval($temp2['cleverInfiniteScroll']);
                    $.each($temp,function(i,v){
                        $('#'+v).cleverInfiniteScroll({
                            contentsWrapperSelector: '#'+v,
                            contentSelector: '#'+v+'>div',
                            nextSelector: '#next',
                            tab:v
                        });
                    });
               };
            },300);  
        }
        else{
            $.each($temp,function(i,v){
                $('#'+v).cleverInfiniteScroll({
                    contentsWrapperSelector: '#'+v,
                    contentSelector: '#'+v+'>div',
                    nextSelector: '#next',
                    tab:v
                });
            });
        }
        
}); 
</script>
<?php
*/
?>
<?php
if($_GET['pagetype'] == 'eventListing')
{
$breadcrumbs = array();
$breadcrumbs[] = array('name'=>'Home','url'=>ROOT_PATH);
$breadcrumbs[] = array('name'=>'Events','url'=>ROOT_PATH.'events');
$PSParams['breadcrumbs'] = $breadcrumbs;
}
?>