<div class="widget-heading mb-small">    
        <a class="more-lnk pull-right" href="<?=$headerlink?>">View all <i class="fa fa-angle-double-right"></i>
    </a>
    <h2 class="hstyl_1 sm"><?=(!empty($headertext))?$headertext:'Events'?></h2>
</div>
<div class="bx-styl">
    <ul class="rltd_evnts" style="padding-left:0px;">
    <?php
    foreach($eventslist['all'] as $event)
    {
    $event['eventname'] = strip_tags($event['eventname']);
    $event['description'] = strip_tags($event['description']);
    ?>
     <li>
        <a href="<?=ROOT_PATH?>event/<?=$event['seopath']?>"  title="<?=$event['eventname']?>"><img  class="unveil" alt="<?=$event['eventname']?>" src="<?=DEFAULT_IMG_HORIZONTAL?>" data-src="<?=$event['eventpic']?>"/></a>
        <div class="desc" style="min-height:60px;">
            <h3><a href="<?=ROOT_PATH?>event/<?=$event['seopath']?>"><?=$event['eventname']?></a></h3>
            <p><?=$event['description']?></p>
        </div>
     </li>
     <?php
     }
     ?>
     </ul>
</div>