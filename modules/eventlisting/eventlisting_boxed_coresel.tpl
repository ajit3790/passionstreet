<?php
if(count($eventslist['all'])==0 || $PSParams['eventfullwidthlistingcalled'] == true)
return ;
?>

<div class="widget-heading mb-small">	
   	 <a class="more-lnk pull-right" href="<?=(($viewalllink)?$viewalllink:(ROOT_PATH.'events'))?>">View all <i class="fa fa-angle-double-right"></i>
	</a>
    <h2 class="hstyl_1 sm"><?=(!empty($headertext))?$headertext:'Events'?></h2>
</div>
<!--
    <div class="col-md-8">
        <h2 class="hstyl_1 sm">Events &amp; Workshops</h2>
    </div>
    
    
    
    
    <div class="col-md-4">
        <ul class="nav nav-pills pull-right">
          <li class="<?=(empty($eventslist['upcoming'])?'':'active')?>" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="home" href="#upcoming">Upcoming</a></li>
          <li class="<?=(empty($eventslist['all'])?'':'active')?>" role="presentation"><a aria-expanded="false" data-toggle="tab" role="tab" aria-controls="home" href="#all-events">View All</a></li>
        </ul>
    </div> -->

<div class="tab-content mb-small bx-styl">
    <?php 
    if(!empty($eventslist['upcoming']))
    {
    ?>
    <div role="tabpanel" class="row tab-pane fade <?=(empty($eventslist['upcoming'])?'':'active in')?>" id="upcoming">
        <div class="col-md-12 card-styl_1 sm carousel-wrapper">
            <i class="fa fa-angle-left"></i>
            <i class="fa fa-angle-right"></i>
            <div class="inner">
                <ul class="eventcarousel" style="width:2000px;">
                    <?php
                    foreach($eventslist['upcoming'] as $event)
                    {
			if(in_array($event['event_id'],$eventrendered))
			continue;
                        echo '<li class="card">';
                        renderer_event($event);
                        echo '</li>';
			$eventrendered[] = $event['event_id'];
                    }
                    ?>
                </ul>
            </div>
        </div>    
    </div>
    <?php
    }
    if(!empty($eventslist['all']))
    {
    ?>
    <div role="tabpanel" class="row tab-pane fade <?=(empty($eventslist['all'])?'':'active in')?>" id="all-events">
        <div class="col-md-12 card-styl_1 sm carousel-wrapper">
            <i class="fa fa-angle-left"></i>
            <i class="fa fa-angle-right"></i>
            <div class="inner">
                <ul class="eventcarousel" style="width:2000px;">
                    <?php
		    $eventrendered = array();
                    foreach($eventslist['all'] as $event)
                    {
			if(in_array($event['event_id'],$eventrendered))
			continue;
                        echo '<li class="card">';
                        renderer_event($event);
                        echo '</li>';
			$eventrendered[] = $event['event_id'];
                    }
                    ?>
                </ul>
            </div>
        </div> 
    </div>
    <?php
    }
    ?>
</div>