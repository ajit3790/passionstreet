<?php
$eventid = $_GET['event_id'];
if($eventid)
{
	$event = $PSParams['currentEvent'];
	if(empty($event))
	{
	$event = event_get_details($eventid,true,PROFILE_ID,true);
	$PSParams['currentEvent'] = $event;
	}
	if(empty($event) || ($event['creator'] != PROFILE_ID))
	{
		header("location:".ROOT_PATH.'events');
	}
}
else
header("location:".ROOT_PATH.'events');
$where = array();
$extraquery .= ' transactions.status="complete|success" AND transactions.txnhtype = "parent" AND transactions.txntype = "main" ';
$fields = array();
$fields['eventid'] = $eventid;
$fields['extraquery'] = $extraquery;

$PSJavascript['userdata']['list'] = event_data($fields);

$where = array();
$extraquery .= ' transactions.status="complete|success" ';
$fields = array();
$fields['productid'] = $eventid;
$fields['txnhtype'] = 'all';
$fields['txntype'] = 'contribution';
$fields['status'] = 'complete|success';

$contrbutiondata = transaction_get_list($fields);
$contrbutiondata2 = array();
foreach($contrbutiondata as $contribution)
{
	$temp = array();
	$temp['transactionid'] = $contribution['transactionid'];
	$temp['amount'] = $contribution['amount'];
	$temp['date'] = $contribution['initialisedate'];
	$x = unserialize($contribution['productdetails']);
	if($x['user'])
	$temp = array_merge($temp,$x['user']);
	else
	$temp = array_merge($temp,$x);
	unset($temp['parenttransactionid']);
	$contrbutiondata2[] = $temp;
}

$PSJavascript['charitydata']['list'] = $contrbutiondata2;
$offlinedata = array();
	if(filemtime(UPLOAD_DIR.DIRECTORY_SEPARATOR.'eventdata'.DIRECTORY_SEPARATOR.$eventid.DIRECTORY_SEPARATOR.$eventid.'-offlinedata.csv'))
	{
		$file = fopen(UPLOAD_DIR.DIRECTORY_SEPARATOR.'eventdata'.DIRECTORY_SEPARATOR.$eventid.DIRECTORY_SEPARATOR.$eventid.'-offlinedata.csv', 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
		  $offlinedata[] = $line;
		}
		fclose($file);
	}
	
$PSJavascript['offline']['list'] = $offlinedata;
?>