<div class="prcng_optns row hide">
    <div class="container sm">
      <div class="row">
        <div class="col-md-12">
          <h2 class="hstyl_1">Event Pricing Options</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="pricng_bx free"> <i class="fa fa-star-o icon"></i>
            <h4><?=$eventsetting['typesbasedonfees'][1]['name']?></h4>
            <h5>No CHARGE!</h5>
            <br>
            <p>In PASSIONSTREET we don't charge for free event hosting.</p>
            <div class="btn_wrap"> <a href="<?=ROOT_PATH.'events/create?type=free'?>" class="eventintrobtnfree" data-eventtypebyfee="free" data-eventtypeidbyfee='1'>CREATE FREE EVENT</a> </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="pricng_bx non-profit"><i class="fa fa-star-half-o icon"></i>
            <h4><?=$eventsetting['typesbasedonfees'][2]['name']?></h4>
            <p>PASSIONSTREET PROCESSING FEE</p>
            <h5>ZERO</h5>
            <h5>+</h5>
            <p>payment gateway fee</p>
            <h5><?=$eventsetting['paymentgatewayfee'][PAYMENTGATEWAY]['relative'].'%'.(!empty($eventsetting['paymentgatewayfee'][PAYMENTGATEWAY]['absolute'])?' + Rs '.$eventsetting['paymentgatewayfee'][PAYMENTGATEWAY]['absolute'].' / ticket':'')?></h5>
            <p><small>* GST applicable</small>.</p>
            <div class="btn_wrap"> <a href="javascript:void(0);" class="eventintrobtn" data-eventtypebyfee="non-profit"  data-eventtypeidbyfee='2'>CREATE NON-PROFIT EVENT</a> </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="pricng_bx paid"> <i class="fa fa-money icon"></i>
            <h4>PAID</h4>
            <p class="hide"><?=$eventsetting['typesbasedonfees'][3]['name']?></p>
            <h5><?=$eventsetting['processingfee']['relative'].'%'.(!empty($eventsetting['processingfee']['absolute'])?' OR Rs '.$eventsetting['processingfee']['absolute'].' / ticket <p>whichever is higher</p>':'')?></h5>
            <h5>+</h5>
            <p>payment gateway fee</p>
            <h5><?=$eventsetting['paymentgatewayfee'][PAYMENTGATEWAY]['relative'].'%'.(!empty($eventsetting['paymentgatewayfee'][PAYMENTGATEWAY]['absolute'])?' + Rs '.$eventsetting['paymentgatewayfee'][PAYMENTGATEWAY]['absolute'].' / ticket':'')?></h5>
            <p><small>* GST applicable</small>.</p>
            <div class="btn_wrap"> <a href="javascript:void(0);" class="eventintrobtn" data-eventtypebyfee="paid" data-eventtypeidbyfee='3'>CREATE PAID EVENT</a> </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="pricng_bx premium"> <i class="fa fa-diamond icon"></i>
            <h4>PREMIUM</h4>
            <p>CUSTOMIZED SOLUTION AND PREMIUM EVENTS</p>
            <br>
            <p>Get in touch with us!
              Write to social@passionstreet.in</p>
            <div class="btn_wrap"> <a href="javascript:void(0);" class="eventintrobtn2" data-eventtypebyfee="premium">GET IN TOUCH</a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>



<div id="eventcreatefree" class="prce_calcltr row">
    <div class="container sm">
      <div class="row">
        <div class="col-md-12">
          <h2 class="hstyl_1">See How Much You Earn</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
           <p class="message"> Hurray! You can list your event completely free and share it on other social media <br>
            We don’t charge unless you charge your customer. </p>
        </div>
        
      </div>
    </div>
  </div>


<div id="eventcreatepaid" class="prce_calcltr row">
    <div class="container sm">
      <div class="row">
        <div class="col-md-12">
          <h2 class="hstyl_1">See How Much You Earn</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="row">
            <div class="col-sm-6 multiply_bx">
              <div class="row">
                <div class="col-sm-5">
                  <label for="ticketPriceInput">TICKET PRICE</label>
                  <input class="form-control input-lg" id="eventTypeIdByFees" value="0" type="hidden">
                  <input class="form-control input-lg" id="ticketPriceInput" value="100" type="number">
                </div>
                <div class="col-sm-2"> <span class="glyphicon glyphicon-remove org-multiply"> </span> </div>
                <div class="col-sm-5">
                  <label for="ticketSoldInput">TICKET SOLD</label>
                  <input class="form-control input-lg" id="ticketSoldInput" value="50" type="number">
                </div>
              </div>
            </div>
            <div class="col-sm-1 text-center equal"> = </div>
            <div class="col-sm-5 equals-div">
              <div class="estimate"> <small class="fees-title">ESTIMATED REVENUE</small>
                  <div id="estimatedRevenue">₹ <span>-</span></div>
              </div>
            </div>
          </div>
          <div class="row" style="margin-top: 20px;">
            <div class="col-sm-7">
              <p>CHOOSE YOUR OPTION </p>
              <div class="radio-options">
                <?php
                foreach($eventsetting['feepaymentoptions'] as $id=>$value){
                    echo    '<div class="radio form-group ">
                                <label>
                                    <input name="fees-optn" value="'.$id.'" required="" '.(($id==1)?'checked="true"':'').' type="radio">
                                   '.$value["name"].'</label>
                            </div>';
                }
                ?>
              </div>
            </div>
            <div class="col-sm-5 ">
              <h5 class="fees-title">PER TICKET</h5>
              <div class="row">
                <div class="col-xs-7 fees-sub-title"> Ticket Price </div>
                <div class="col-xs-1 "> : </div>
                <div class="col-xs-3 price-data" id="ticketPrice">₹ <span>-</span></div>
              </div>
              <div class="row">
                <div class="col-xs-7 fees-sub-title"> Fee Organizer pays </div>
                <div class="col-xs-1 "> : </div>
                <div class="col-xs-3 price-data"  id="organizerFee"> ₹ <span>-</span> </div>
              </div>
              <div class="row">
                <div class="col-xs-7 fees-sub-title"> Fee Ticket Buyer pays </div>
                <div class="col-xs-1 padd0"> : </div>
                <div class="col-xs-3 price-data" id="buyerFee">₹ <span>-</span></div>
              </div>
              <div class="row">
              	<div class="col-xs-12 "><hr></div>
              </div>
              <div class="row orgnzr_rvnw">
                <div class="col-xs-7 fees-title"> ORGANIZER REVENUE </div>
                <div class="col-xs-1"> : </div>
                <div class="col-xs-3 price-data" id="organiserRevenue">₹ <span >178.40</span></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 non-profit-tips">
        <ul>
            <li>No Charges for Non-profit events</li>
            <li>Plug-in your event to other social Media </li>
            <li>Organizer Page</li>
            <li>Attendee Management </li>
            <li>Engage participants in event page</li>
            <li>Event analytics</li>
            <li>SEO &amp; Promotion </li>
          </ul>
        </div>
        <div class="col-md-4 paid-event-tips">
        	<ul>
            <li>social Media plug-in including facebook widget </li>
            <li>Listing in featured Events</li>
            <li>Attendee Management</li>
            <li>Payment, Collection and Tax Management</li>
            <li>Sponsors management</li>
            <li>Multi-level Discount Management</li>
            <li>Pre and post event user engagement</li>
            <li>Event analytics</li>
            <li>Customised notification and display element</li>
            <li>SEO &amp; Promotion </li>
          </ul>
        </div>
        
      </div>
    </div>
  </div>
  
  <div id="premium" class="prce_calcltr" >
    <div class="container">
      <div class="row">
        <div class="col-md-7">
          <p class="message"> Get in touch with us to make it a wonderful experience for your customers. </p>
        </div>
        <div class="col-md-5">
          <div class="form1">
            <div class="row ">
              <div class="col-sm-6 form-group ">
                <input placeholder="Name" value="" id="fname" name="fname" required="" class="form-control" type="text">
              </div>
              <div class="col-sm-6 form-group ">
                <input placeholder="Organization" value="" id="orgname" name="orgname" required="" class="form-control" type="text">
              </div>
            </div>
            <div class="row ">
              <div class="col-sm-6 form-group ">
                <input placeholder="E-mail" value="" id="e-mail" name="e-mail" required="" class="form-control" type="text">
              </div>
              <div class="col-sm-6 form-group ">
                <input placeholder="Phone Number" value="" id="mob-num" name="mob-num" required="" class="form-control" type="text">
              </div>
            </div>
            <div class="row ">
              <div class="col-sm-12 form-group ">
                <textarea name="postsummary" placeholder="Message" required="" class="form-control"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 text-right">
                <button name="postsubmit" class="btn btn-primary btn-sm postsubmit">SUBMIT</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
<script>
    $(document).ready(function(){
        $(".eventintrobtnfree").on('click',function(e){
            e.preventDefault();
			$("#eventcreatepaid").hide();
            $("#eventcreatefree").slideDown(function(){scrollTo("#eventcreatepaid");});
        });
		$(".eventintrobtn").on('click',function(e){
            e.preventDefault();
			$("#eventcreatefree").hide();
            $("#eventcreatepaid").slideUp();
            $temp = $(this);
            $eventidbyfee = $temp.attr('data-eventtypeidbyfee');
			$eventbyfee = $temp.attr('data-eventtypebyfee');
			if($eventbyfee == 'non-profit')
			{
				$(".non-profit-tips").show();
				$(".paid-event-tips").hide();
			}
			else if($eventbyfee == 'paid')
			{
				$(".non-profit-tips").hide();
				$(".paid-event-tips").show();
			}
            $("#eventTypeIdByFees").val($eventidbyfee);
            getdemoticketprice();
            $("#eventcreatepaid").slideDown(function(){scrollTo("#eventcreatepaid");});
        });
        $("#eventTypeIdByFees,input[name=fees-optn]").on('change',function(){
            getdemoticketprice();
        });
        $("#ticketPriceInput").donetyping(function(){
            getdemoticketprice();
        });
        $("#ticketSoldInput").donetyping(function(){
            getdemoticketprice();
        });
    });
    function getdemoticketprice(){
        $data = {};
        $data['eventTypeIdbyFees'] = $("#eventTypeIdByFees").val();
        $data['eventTicketPrice'] = $("#ticketPriceInput").val();
        $data['eventTicketSold'] = $("#ticketSoldInput").val();
        $data['eventFeepaymentoptions'] = $('input[name=fees-optn]:checked').val();
        $.post(root_path+'PSAjax.php?type=eventticketdemoeffectiveprice',$data, function(data){ 
            $res = $.parseJSON(data);
            $temp = $res['list'];
            $("#estimatedRevenue span").text($temp['totalPrice']);
            $("#ticketPrice span").text($temp['initialPrice']);
            $("#organiserRevenue span").text($temp['organiserrevenue']);
            $("#organizerFee span").text($temp['organiserpays']);
            $("#buyerFee span").text($temp['userpays']);
        });
    }
</script>