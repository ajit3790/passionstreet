<?php
$eventid = $_GET['event_id'];
if(!($eventid) ||  !($event = event_get_details($eventid))){
    header("location:".ROOT_PATH.'events');
}
$PSParams['currentEvent'] = $event;
$PSModData['event'] = $event;
?>
