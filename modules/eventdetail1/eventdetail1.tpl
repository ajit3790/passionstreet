<div class="col-md-8 main-content">
    <div class="inner">
        <h2 class="hstyl_1 sm bx"><?=$event['eventname']?></h2>                
        <div class="post bx-styl">

            <h3>Details of event</h3>
            <div class="post-desc event-desc clearfix">                           
                <?=$event['description']?>
            </div>                        
        </div>
        <!--/ post -->

    </div>      
</div>
<div class="col-md-4 sidebar">
    <div class="widget bx-styl">
    <?php if(!empty($event['isAdmin'])){?>
    <div class="event-settings">
        <a href="#">Settings <i class="fa fa-gear"></i></a>
    </div>
    <?php } ?>
    
        <div class="event-meta">
            <ul>
                <li>
                    <h6>Date</h6>
                    <strong><?=date("d/m/Y",strtotime($event['datestart']))?></strong>
                </li>
                <li>
                    <h6>Address</h6>
                    <?=$event['address']?>
                </li>
                <li>
                    <h6>Event Fee</h6>
                    <?=$event['tickettype']?>
                </li>
                <li class="clearfix">
                    <?php
                        if(!empty($event['precondition'])){
                            if($event['precondition'] == 'question')
                            {
                                echo ($event['isJoinedEvent'])?'<a class="btn btn-primary btn-large" onclick="javascript:return(0);">Already Joined</a>':'<a class="btn btn-primary btn-large" onclick="javascript:question_popup();">Sign Up Event</a>'; 
                            }
                        }
                    ?>
                    
                </li>
            </ul>
        </div>                    
    </div>
</div>
<script>
<?php
if(!empty($event['precondition'])){
    if($event['precondition'] == 'question')
    {   
        echo "var questions = $.parseJSON('".json_encode($event['preconditionOption'])."');\r\n";
        ?>
        function question_popup(){
            $quesHtml = '<div class="question">'+questions['text']+'</div>';
            if(questions['type'] == 'objective')
            {
                $.each(questions['options'],function(i,v){
                    $quesHtml += '<div class="options"><label><input type="radio" name="answer" value="'+v+'"/>'+v+'</label></div>';
                });
                $quesHtml += '<a class="btn btn-primary btn-large" onclick="javascript:ticketing_popup();">Submit & Move to Ticketing</a>';
            }
            myModal("Event Precondition Question",$quesHtml);
        } 
        <?php
     }
}
echo "var ticketing = $.parseJSON('".json_encode($event['ticketcategories'])."');\r\n"; 
?>
function ticketing_popup(){
    $ticketingHtml = "<div class='ticketing'>";
    $.each(ticketing , function(i,v){
        $ticketingHtml +=   "<div class='row ticketdetails'>"+
                                "<div class='tname'>"+v['name']+"</div>"+
                                "<div class='tprice'>"+v['price']+"</div>"+
                                "<div class='tcount'><input type='text' value='<?=$event['peruserticketcount']?>' class='ticketbuycount' /></div>"+
                                "<div class='tadd'><a class='btn btn-primary ticketbuy' event='<?=$event['event_id']?>'> Add </a></div>"+
                            "</div>";
    });
    $ticketingHtml += "</div>";
    $ticketingHtml += "<div class=''><a class='btn btn-primary viewcart callmodaliframe' targetsrc='module/cart' > View Cart </a></div>";
    myModal("Event Ticketing",$ticketingHtml);
}
$("body").on("click",".ticketbuy",function(){
    $temp = $(this);
    $temp2 = $temp.parents(".ticketdetails");
    $teventid = $temp.attr("event");
    $tname = $temp2.find(".tname").text();
    $tprice = $temp2.find(".tprice").text();
    $tcount = $temp2.find(".ticketbuycount").val();
    $params = {productType:'event',productid:$teventid,ticketname:$tname,ticketprice:$tprice,buycount:$tcount};
    $.post(root_path+'PSAjax.php?type=addToCart', $params, function(data){ 
        $temp.html("Added To Cart");
    });
})
</script>