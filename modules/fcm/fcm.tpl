<?php
/*
<div class="col-md-12 no-padding">
    <div class="banner-cells">
        <img class='unveil1' src1="<?=DEFAULT_IMG?>" src="<?=ROOT_PATH?>images/indexbanner.png" alt="" />
        <div class="hide">Banner</div>
    </div>
    <div class='show-mob'><img class='unveil1' src1='<?=DEFAULT_IMG?>' src='<?=ROOT_PATH?>images/indexbanner/banner-small.jpg'></div>
</div>
*/
?>
<?php
$images = array();
while(count($images) < 4)
{
    $x = rand(1,22);
    if(!in_array($x,$images))
    {
        $images[] = $x;
    }
}
?>
<div class="split-banner row clearfix">
<div class="col-8">
<div id="carousel-one" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
        <div class="tint"></div>
        <picture>
          <source srcset="images/indexbanner/banner_<?=$images[0]?>_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_<?=$images[0]?>.jpg">
          <img src="images/indexbanner/banner_<?=$images[0]?>.jpg" alt="...">
        </picture>
      
      <div class="carousel-caption text-btm">
          <strong>#LiveWhatYouLove!</strong>Follow Your Passion - Join - Connect - Exhibit
      </div>
    </div>

    <div class="item">
    <div class="tint"></div>
    <picture>
          <source srcset="images/indexbanner/banner_<?=$images[1]?>_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_<?=$images[1]?>.jpg">
      <img src="images/indexbanner/banner_<?=$images[1]?>.jpg" alt="...">
      </picture>
      <div class="carousel-caption text-right">
        <strong>LET YOUR<br/>PASSION<br/> FLOW</strong>Track &amp; Record Your Activities<br/><a href="register" class="join">Join Now</a>
      </div>
    </div>
    <div class="item">
    <div class="tint"></div>
    <picture>
          <source srcset="images/indexbanner/banner_<?=$images[2]?>_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_<?=$images[2]?>.jpg">
      <img src="images/indexbanner/banner_<?=$images[2]?>.jpg" alt="...">
      </picture>
      <div class="carousel-caption text-right-top">
        <strong class="f35">Follow Your<br/><span class="f43">PASSION</span></strong>Invite Friends &amp; Share Experience<br/><button class="login-btn" class="join">Invite</button>
      </div>
    </div>
     <div class="item">
     <div class="tint"></div>
     <picture>
          <source srcset="images/indexbanner/banner_<?=$images[3]?>_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_<?=$images[3]?>.jpg">
      <img src="images/indexbanner/banner_<?=$images[3]?>.jpg" alt="...">
      </picture>
      <div class="carousel-caption text-right">
        <strong class="f35"><span class="f68">PASSION</span><br/>Drives Perfection</strong>Connect & Interact with Experts<br/><button class="login-btn" class="join">Start Following</button>
      </div>
    </div>
    <?php
    $x = rand(1,3);
    ?>
    <div class="item">
    <div class="tint"></div>
    <picture>
          <source srcset="images/indexbanner/banner_event<?=$x?>_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_event<?=$x?>.jpg">
      <img src="images/indexbanner/banner_event<?=$x?>.jpg" alt="...">
      </picture>
      <div class="carousel-caption text-btm">
        <strong>Create &amp; Manage Events</strong>Single window for Ticketing, Analytics & Social Interaction!<br/>
        <button class="login-btn" class="join">Create Event Now</button>
      </div>
    </div>
    
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-one" role="button" data-slide="prev">
   
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-one" role="button" data-slide="next">
    
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<div class="division">
	<h2>Create or join communities to connect around your passions </h2>
</div>



<!--
<div class="col-4">
<div id="carousel-two" class="carousel slide" data-ride="carousel">
  
  <div class="carousel-inner" role="listbox">
    <div class="item active">
    <picture>
          <source srcset="images/indexbanner/banner_14_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_14.jpg">
      <img src="images/indexbanner/banner_14.jpg" alt="...">
      </picture>
      <div class="carousel-caption">
        Live for what you love to do!
      </div>
    </div>
    <div class="item">
    <picture>
          <source srcset="images/indexbanner/banner_15_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_15.jpg">
      <img src="images/indexbanner/banner_15.jpg" alt="...">
      </picture>

      <div class="carousel-caption">
        Live for what you love to do!
      </div>
    </div>
    <div class="item">
    <picture>
          <source srcset="images/indexbanner/banner_17_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_17.jpg">
      <img src="images/indexbanner/banner_17.jpg" alt="...">
      </picture>
      <div class="carousel-caption">
        Live for what you love to do!
      </div>
    </div>
    
    
  </div>


  <a class="left carousel-control" href="#carousel-two" role="button" data-slide="prev">
   
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-two" role="button" data-slide="next">
    
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<div class="col-4">
<div class="video-box flexi-video unveil" vsrc="https://www.youtube.com/embed/9z8b9h52TPs/?rel=0&autoplay=1&autohide=1" src="<?=DEFAULT_IMG?>" data-src="http://i.ytimg.com/vi/9z8b9h52TPs/sddefault.jpg"></div>
</div>


<div class="col-4">
<div id="carousel-three" class="carousel slide" data-ride="carousel">
  

  
  <div class="carousel-inner" role="listbox">
    <div class="item active">
    <picture>
          <source srcset="images/indexbanner/banner_7_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_7.jpg">
      <img src="images/indexbanner/banner_7.jpg" alt="...">
      </picture>
      <div class="carousel-caption">
        Participate in Events &amp; Share Experience
      </div>
    </div>
    <div class="item">
    <picture>
          <source srcset="images/indexbanner/banner_10_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_10.jpg">
      <img src="images/indexbanner/banner_10.jpg" alt="...">
      </picture>
      <div class="carousel-caption">
			Track &amp; Record Your Activities
      </div>
    </div>
    <div class="item">
    <picture>
          <source srcset="images/indexbanner/banner_18_mob.jpg" media="(max-width: 450px)">
          <source srcset="images/indexbanner/banner_18.jpg">
      <img src="images/indexbanner/banner_18.jpg" alt="...">
      </picture>
      <div class="carousel-caption">
        Live for what you love to do!
      </div>
    </div>
    
    
  </div>


  <a class="left carousel-control" href="#carousel-three" role="button" data-slide="prev">
   
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-three" role="button" data-slide="next">
    
    <span class="sr-only">Next</span>
  </a>
</div>

</div>
-->
</div>
<div class="video-area1 row" id="promotionalvideo">
<!--<iframe id='promotionalvideo' data-src="https://www.youtube.com/embed/WfiUAvfb5Go?controls=0&showinfo=0&autoplay=1&modestbranding=1&loop=1&disablekb=1&rel=0&theme=light&playsinline=1&enablejsapi=1" frameBorder="0">
</iframe>-->
<!--<video style="width:100%;" controls loop id="promotionalvideo" onloadeddata="alert(this.currentSrc)">
  <source src="videos/Passionstreet.mp4"  type='video/mp4'>
  <source src="videos/Passionstreet.webm" type='video/ogg'>
  <source src="videos/Passionstreet.ogv" type='video/webm'>
</video>-->
<div class="vdo_ovly1"></div>
</div>

<style>
body{background-image:none;}
.navbar .header {background:none; transition:background .2s ease;}
.navbar.sticky .header{background:rgba(19,26,34,.85);}
.spacer{display:none;}
.overlay_nav {top:0; padding-top:45px;}
.navbar-brand img{max-height:40px;}
.carousel-wrapper .bx-prev.disabled, .carousel-wrapper .bx-next.disabled, .image-gallery .bx-next.disabled, .image-gallery .bx-prev.disabled{background:#fff;}
.navbar.sticky .navbar-brand img{max-height:27px;}
.carousel-fade  .carousel-inner .item { transition-property: opacity; }
        
.carousel-fade  .carousel-inner .item, .carousel-fade  .carousel-inner .item.active.left, .carousel-fade  .carousel-inner .item.active.right {
 opacity: 0; }
 .carousel-fade  .carousel-inner .item.active,  .carousel-fade  .carousel-inner .item.next.left, .carousel-fade  .carousel-inner .item.prev.right {
 opacity: 1; }
.carousel-fade  .carousel-inner .item.next, .carousel-fade  .carousel-inner .item.prev, .carousel-fade  .carousel-inner .item.active.left, .carousel-fade .carousel-inner .item.active.right {left: 0; transform: translate3d(0, 0, 0);}
.carousel-control { z-index: 2;}


@media screen and (max-width:990px){ .navbar .header{background:rgba(19,26,34,.85);}}
</style>
<script>

$(document).ready(function(){
 	$("#main-nav").addClass("slide-out");
 	$(".nav-open").addClass("rotate");

 	//var stickyNavTop = $('.navbar ').offset().top; 
 	var stickyNavTop = $('.navbar ').height();
	var stickyNav = function(){
	var scrollTop = $(window).scrollTop();
        console.log(scrollTop );
	if (scrollTop > stickyNavTop) { 
	    $('.navbar ').addClass('sticky');
	} else {
	    $('.navbar ').removeClass('sticky'); 
	}
	};
	stickyNav();
	$(window).scroll(function() {
	  stickyNav();
	});
 
});

</script>
<script src="https://www.gstatic.com/firebasejs/4.0.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBTRfaFHOhA-bzhhkiZwEwLz7drvv4lx5U",
    authDomain: "passionstreet-15874.firebaseapp.com",
    databaseURL: "https://passionstreet-15874.firebaseio.com",
    projectId: "passionstreet-15874",
    storageBucket: "passionstreet-15874.appspot.com",
    messagingSenderId: "234168754983"
  };
  firebase.initializeApp(config);
  // Retrieve Firebase Messaging object.
const messaging = firebase.messaging();

messaging.requestPermission()
.then(function() {
  console.log('Notification permission granted.');
  return messaging.getToken();
  // TODO(developer): Retrieve an Instance ID token for use with FCM.
  // ...
})
.then(function(token) {
  console.log(token);
})
.catch(function(err) {
  console.log('Unable to get permission to notify.', err);
});
</script>