<?php
function fbpagetab_manage($wherefields = array(),$updatefields=array()){
    global $connection;
    if(!($wherefields['fbpageid'] && $updatefields['creator']))
    return false;
    $return = $connection->executeUpdate("insert into fbpagetab (fbpageid,defaultpage,creator,log) values ('".$wherefields['fbpageid']."',".$updatefields['defaultpage'].",'".$updatefields['creator']."','".$updatefields['log']."') ON DUPLICATE KEY UPDATE defaultpage = '".$updatefields['defaultpage']."',log = CONCAT(log,'|\n".$updatefields['log']."') ",array());
    return true;
}
function fbpagetab_get_details($fields = array()){
    global $connection;
    if(!($fields['fbpageid']))
    return false;
    $queryParams = construct_list_params($fields);
    $data = $connection->fetchAssoc("select defaultpage,creator from fbpagetab ".$queryParams[0]." ",$queryParams[1]);
    return $data;
}


if($PSParams['server'] == 'live')
$secret = "4c0360dbf3b7c382b89a365ea2d66dfb"; // Use your app secret here
else if($PSParams['server'] == 'local')
$secret = "e6cbe362f51e9934335f4c53727b6608"; // Use your app secret here
$PSModData['secret'] = $secret;
if($_POST && $_POST['setuppage'])
{
	$fbappdatadecoded = decodesignedrequest($_POST['app_data']);
	if($fbappdatadecoded && $fbappdatadecoded['user'] == $_SESSION['user']['profile_id'])
	{
		$where = array();
		$where['fbpageid'] = $_POST['fbpageid'];
		$update = array();
		$update['defaultpage'] = $_POST['defaultpage'];
		$update['creator'] = $_SESSION['user']['profile_id'];
		$update['log'] = addslashes('updated by fb user :'.$_POST['fbuserid'].' & '.$_SESSION['user']['profile_id'].' on '.date("Y-m-d H:i:s"));
		fbpagetab_manage($where,$update);
		echo "<div class='jumbotron text-center'>Update Successfull</div>";
	}	
}
if(count($_GET) == 1 && empty($_POST) && $_GET['pagetype']) //redirect to tab add page on fb
{
	$fburl = ROOT_PATH.'open/fbtab?';
	header('location:https://www.facebook.com/dialog/pagetab?app_id='.$PSParams['clientid']['facebook'].'&redirect_uri='.$fburl);
}
else if($_GET['tabs_added']) //after successfull add , get the pageid and redirect to fb page for edit
{
	$key = key($_GET['tabs_added']);
	$param = array();
	$param['user'] = $_SESSION['user']['profile_id'];
	$param['initialsetup'] = 1;
	header('location:https://www.facebook.com/'.$key.'/app/'.$PSParams['clientid']['facebook'].'?app_data='.createsignedrequest($param));
}
else if(!empty($_POST) && isset($_POST['signed_request'])) //this is called for every fb page tab call
{
	$fbdata = decodesignedrequest($_POST['signed_request'],$secret);
	if($fbdata)
	{
		$_SESSION['fbtaballow'] = 1;
		$_SESSION['fbsigneddata'] = $_POST['signed_request'];
		$fbpageid = $fbdata['page']['id'];
		$fbpageadmin = $fbdata['page']['admin'];
		$fbappdata = $fbdata['app_data'];
		if($fbpageadmin)
		{
		$_SESSION['fbpageadmin'] = 1;
		}
		unset($fbdata['app_data']);
		$fbappdatadecoded = decodesignedrequest($fbappdata);
		if($fbpageadmin && $fbappdatadecoded && $fbappdatadecoded['initialsetup'] == 1 && $fbappdatadecoded['user'] == $_SESSION['user']['profile_id'])
		{
			$PSModData['editpage'] = 1;
			$PSModData['fbappdata'] =  $fbappdata;
			$PSModData['fbpageid'] =  $fbpageid;
			$PSModData['fbdata'] =  $fbdata;
		}
		else //if normal user
		{
			$fbpagedata = fbpagetab_get_details(array('fbpageid'=>$fbpageid));
			if($fbpagedata['defaultpage'] == 1)
			{
				header('location:'.ROOT_PATH.'profile/p/'.$fbpagedata['creator']);
			}
			else if($fbpagedata['defaultpage'] == 1)
			{
				header('location:'.ROOT_PATH.'profile/p/'.$fbpagedata['creator']);
			}
			else if($fbpagedata['defaultpage'] == 2)
			{
				header('location:'.ROOT_PATH.'profile/p/'.$fbpagedata['creator'].'#eventlist');
			}
			else if($fbpagedata['defaultpage'] == 3)
			{
				$where = array();
				$where['organiserid'] = $fbpagedata['creator'];
				$where['upcoming'] = 1;
				$eventdata = event_get_list($where);
				if($eventdata[0])
				{
				$event = $eventdata[0];
				header('location:'.ROOT_PATH.$event['producttype'].'/'.$event['seopath']);
				}
				else
				echo "<div class='jumbotron text-center'>Configuration Error</div>";
			}
		}
	}
}
else if(isset($_GET['editfbtabsetting'])) //this is called for every fb page tab call
{
	$fbdata = decodesignedrequest($_SESSION['fbsigneddata'],$secret);
	if($fbdata)
	{
		$fbpageid = $fbdata['page']['id'];
		$fbpageadmin = $fbdata['page']['admin'];
		$fbappdata = $_GET['app_data'];
		unset($fbdata['app_data']);
		$fbappdatadecoded = decodesignedrequest($fbappdata);
		if($fbpageadmin && $fbappdatadecoded && $fbappdatadecoded['user'] == $_SESSION['user']['profile_id'])
		{
			$PSModData['editpage'] = 1;
			$PSModData['fbappdata'] =  $fbappdata;
			$PSModData['fbpageid'] =  $fbpageid;
			$PSModData['fbdata'] =  $fbdata;
		}
		else
		echo "<div class='jumbotron text-center'>Not Allowed</div>";
	}
}
?>