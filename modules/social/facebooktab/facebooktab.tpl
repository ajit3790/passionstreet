<?php
if($editpage ==1)
{
?>
<div class="form-wrapper">
	<br />
	<br />
	<form class="form1" action="" method="post" >
		<div class="col-md-12 col-sm-12"><h5>Select default page to display to user</h5></div>
		<div class="row form-group">
			<?php
			/*
			?>
		    <div class="col-md-4 col-sm-4">
			<label>
			    <input name="defaultpage" class="defaultpage" value="1" type="radio" required="">
			    Profile Page 
			</label>
		    </div>
			<?php
			*/
			?>
		    <div class="col-md-6 col-sm-6">
			<label>
			    <input name="defaultpage" class="defaultpage" value="2" type="radio" required="">
			    Upcoming Events List
			</label>
		    </div>
		    <div class="col-md-6 col-sm-6">
			<label>
			    <input name="defaultpage" class="defaultpage" value="3" type="radio" required="">
			    Latest Scheduled Event
			</label>
		    </div>
		</div>
		<div class="col-sm-12  col-md-12 text-center">
			<input type="hidden" name="app_data" value="<?=$fbappdata?>">
			<input type="hidden" name="fbpageid" value="<?=$fbpageid?>">
			<input type="hidden" name="fbuserid" value="<?=$fbdata['user_id']?>">
			<input type="hidden" name="setuppage" value="1">
			<button class="btn btn-primary" type="submit" name="event-create" value="1">Set Default Page</button>                    
		</div>
		<br />
		<br />
		<br />
		<br />
	
	</form>
</div>
<?php
}
?>