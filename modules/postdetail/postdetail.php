<?php
$PSParams['sharedpost'] = ($_GET['sharedpost'])?$_GET['sharedpost']:$_GET['post_id'];
$post_list = post_get_list(array('post_id'=>$PSParams['sharedpost']),true);
$post = $post_list[0];
if($post)
{
if($_GET['pagetype'] == 'postDetail')
{
	$_GET['profile_id'] = $post['creator'];

	$seotitle = ''; 
	$seodesc = '';
	$seoimage = ''; 

	if($post['title'])
	{
		usePlugin('DomXPath',array('html'=>$post['title']));
		$xpath = $PSParams['plugins']['DomXPath'];

		$nodes = $xpath->query("//img[@data-src]");
		if($nodes->item(0))
		{
		$img = $nodes->item(0)->getAttribute('data-src');
		$seoimage = $img;
		}
		if(empty($seoimage))
		{
		$nodes = $xpath->query("//div[contains(@class, 'unveil')]");
		if($nodes->item(0))
		{
		$img = $nodes->item(0)->getAttribute('data-src');
		$seoimage = $img;
		}
		}
		
		$nodes = $xpath->query("//h3");
		if($nodes->item(0))
		{
		$title = $nodes->item(0)->nodeValue;
		$seotitle = $title;
		}
		$nodes = $xpath->query("//p");
		if($nodes->item(0))
		{
		$desc = $nodes->item(0)->nodeValue;
		$seodesc = $desc;
		}
	}
	if(empty($seotitle))
	$seotitle = ($post['content'])?$post['content']:($post['title']);
	$seodesc = (($seodesc)?($seodesc.' - '):'').'Post by '.$post['creatorname'].(($PSParams['PSCategories'][$post['category']]['name'])?' in '.$PSParams['PSCategories'][$post['category']]['name'].' passion':' something').'. Express your own views,experiences with like minded people in this passion';
	if(empty($seoimage))
	{
		foreach($post['images'] as $temp){
		    foreach($temp as $temp2)
		    {
		    $seoimage[] = str_ireplace("/thumb","",$temp2); 
		    }
		}
	}
	$PSModData['postedonobject'] = array();
	if($post['postedontype'] == 'event' && $post['postedon'])
	{
	$temp = event_get_details($post['postedon']);
	$PSModData['postedonobject'] = $temp;
	if(empty($seoimage) && $PSModData['postedonobject']['eventpic'])
	$seoimage[] = $PSModData['postedonobject']['eventpic'];
	if($PSModData['postedonobject']['eventname'])
	$seodesc = $post['creatorname'].' posted in event - '.$PSModData['postedonobject']['eventname'].'. You can also share your views,experiences about this event';
	}
	
	$GLOBALS['page_meta']['title'] = $seotitle;
	$GLOBALS['page_meta']['description'] = $seodesc.' '.$post['title'];
	if(!empty($seoimage))
	$GLOBALS['page_meta']['image'] = $seoimage;
	$GLOBALS['page_meta']['url'] = $_SERVER['PHP_SELF_URL'];
}
else
$GLOBALS['page_meta']['url'] = ROOT_PATH.'post/'.$PSParams['sharedpost'];
}
$PSModData['post'] = $post;
?>