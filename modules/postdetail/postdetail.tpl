<?php
if(empty($post))
return;
?>
<div class="main-content">
<div class="inner">
<?php
renderer_post($post);
?>
</div>
</div>
<script>
$(document).ready(function(){    
	delayfunction2(['wall.js'],'initialisewall','',1000);    
	delayfunction2(['wall.js','cleverinfinitescroll.js'],'wallpaging','',1000);
});
</script>
<?php
$PSJsincludes['external'][] = "js/wall.js";
$PSJsincludes['external'][] = "js/cleverinfinitescroll.js";
?>