<div class="col-md-12"><h2 class="hstyl_2 mb-large"><span><i class="fa fa-pencil-square-o"></i>Manage Your Event / Workshop</span></h2></div>
<div class="steps">
    <div class="row">
        <div class="col-md-4 <?=(($_GET['step']=='step1')?'active':'')?>">
            <a href="events/publish/<?=$event['event_id']?>/step1">
                <img  alt="" src="images/ticketing.png"><span class="title">Ticketing</span>
            </a>
        </div>
        <div class="col-md-4 <?=(($_GET['step']=='step2')?'active':'')?>">
            <a href="events/publish/<?=$event['event_id']?>/step2">
                <img alt="" src="images/selection.png"><span class="title">Selection Criteria</span>
            </a>
        </div>
        <div class="col-md-4 <?=(($_GET['step']=='step3')?'active':'')?>">
            <a href="events/publish/<?=$event['event_id']?>/step3">
                <img alt="" src="images/ticketing.png"><span class="title">Add Some Spice</span>
            </a>
        </div>
    </div>   
</div>
<div class="row tab-content">
    <div  role="tabpanel" class="col-md-12 row tab-pane <?=($showform['step1'])?$showform['step1'].' fade active in':'hide'?>" id="step1">
        <div class="form-wrapper">
            <form class="form1 event-manage" method="post" id="form-step1">
               <div class=" row">
                    <label class="col-sm-7 col-xs-12 form-group  control-label">
                    	<h4 class="hstyl_4">Is there any participation fee?</h4>
                    </label>
                    <div class="radio form-group col-sm-2 col-xs-2">
                        <label>
                          <input type="radio" value="Yes" class="ticketfeeradio" name="ticketfeeradio">
                          Yes
                        </label>
                     </div>
                     <div class="radio form-group col-sm-2 col-xs-2">
                        <label>
                          <input type="radio" value="No" class="ticketfeeradio" name="ticketfeeradio">
                          No
                        </label>
                     </div>
               </div>
	       
               <div class="tickets-price hide" style="padding-top:20px; padding-bottom:20px;"> 
                
                <div class="row">
                    <label class="col-sm-4  form-group  control-label">Ticket Type</label>
                    <label class="col-sm-3  form-group  control-label">Rate per Ticket</label>
                    <label class="col-sm-3  form-group  control-label">Allocate Tickets</label>
                    <label class="col-sm-2  form-group  control-label text-center">&nbsp;</label>
               </div>
                <div class="ticket-price-row-box">   
	                <div class="ticket-price-row row" >
			<div class="row1" style="background-color:#f4f4f4">
	                    <div class="col-sm-4  form-group  "><input type="text" name="ticketfeecategory[]" placeholder="Define Ticket Name"  class="form-control"></div>
	                    <div class="col-sm-3  form-group  "><input type="text" name="ticketfeefee[]" placeholder="Ticket Price In INR"  class="form-control"></div>
	                    <div class="col-sm-3  form-group  "><input type="text" name="ticketfeeticketss[]" placeholder="Enter no.of Tickets"  class="form-control"></div>
	                    <div class="col-sm-2 tools"><i class="fa fa-cog" onclick="$(this).parents('.ticket-price-row').find('.extrafields').toggleClass('hide')"></i><i class="fa fa-trash" onclick="$(this).parents('.ticket-price-row').remove()"></i></div>
	                </div>
			<div class="row1 extrafields hide clearfix" style="background-color:#f4f4f4">    
			    <div class="col-sm-2  form-group">Min Ticket user can buy</div>
	                    <div class="col-sm-2  form-group">Max Tickets user can buy</div>
	                    <div class="col-sm-3  form-group">BIB / SR no. initial</div>
	                    <div class="col-sm-3  form-group">BIB / SR no. Starts with</div>
	                    <div class="col-sm-2  form-group">&nbsp;</div>
			</div>
			<div class="row1 extrafields hide clearfix" style="background-color:#f4f4f4;border-bottom:1px solid #eeeeee;">    
			    <div class="col-sm-2  form-group"><input type="text" name="ticketfeeminbuycount[]" placeholder="Minimum buy count" value="1"  class="form-control"></div>
	                    <div class="col-sm-2  form-group"><input type="text" name="ticketfeemaxbuycount[]" placeholder="Maximum buy count" value="10" class="form-control"></div>
	                    <div class="col-sm-3  form-group"><input type="text" name="ticketfeebibintial[]" placeholder="Initials for SR no." value="" class="form-control"></div>
	                    <div class="col-sm-3  form-group"><input type="text" name="ticketfeebibseqstrt[]" placeholder="Set start sequence for SR no."  class="form-control"></div>
	                    <div class="col-sm-2  form-group">&nbsp;</div>
	               </div>
	               </div>
		       
               </div>
               <div class="row ">
                  <div class="col-sm-12 form-group text-right ">
                    <span class="add-more more-ticket-categories">add more ticket type</span>
                  </div>
               </div>
               </div>
                <div class="tickets-total-count hide"> 
                
                    <div class="row">
                        <label class="col-sm-6  form-group  control-label">Total Ticket Count</label>
                        <label class="col-sm-6  form-group  control-label"><input type="text" name="totalticketcount" placeholder="1000" class="form-control"></label>
                   </div>
               </div>
               <div class="bookinglastday"> 
                   <div class="row">
                        <div class="col-sm-6  form-group " style="line-height:30px;">Last date of Registering</div>
                        <div class="col-sm-6  form-group "><i class="fa fa-calendar" style="margin-right:10px"></i><input type="text" placeholder="DD/MM/YYYY" data-type="datetimepicker" class="form-control date datepicker date-range" style="background-color: #fff" readonly="" name="ticketbookingends"></div>
                    </div>
               </div>
               <div class="bank-account hide"> 
               <div class="row">
                    <label class="control-label col-md-12  form-group">Kindly Provide your Bank Details to transfer event ticketing fee money</label>
                </div>
               
                <ul class="nav nav-tabs nav-justified" style="margin-bottom:-1px">
                    <li class="active"><a data-toggle="tab" href="#savedaccounts">Saved Accounts</a></li>
                    <li><a data-toggle="tab" href="#addaccount" id="addNewBackAccount">Add New Account</a></li>
                </ul>
                <div class="tab-content" style="border: 1px solid rgb(204, 204, 204); padding: 30px;background-color:#fff;margin-bottom:10px">
                    <div id="savedaccounts" class="tab-pane fade in active">
                        <div class="row bank-account-list">
                            
                        </div>
                    </div>
                    <div id="addaccount" class="tab-pane fade addNewBackAccountForm">
                        
                    </div>
                </div>
              </div>
                <div class="org-detail">
                    <h4 class="hstyl_4">Organizer Contact details</h4>
			<div class="radio form-group col-sm-6 col-xs-12">
				<label>
				  <input type="radio" name="organiser[type]"  value="userprofile" id="userasorganiser" >
				  Add Self Profile as organiser
				</label>
			</div>
			<div class="radio form-group col-sm-6 col-xs-12">
				<label id="showpagelist">
				  <input type="radio" name="organiser[type]"  value="pageprofile" readonly="readonly">
				  Add Affiliate Page as organiser
				</label>
			</div>
                    <div class="row">
                        <div class="col-sm-12  form-group "><input type="text" id="organisername" name="organiser[name]" class="form-control" required placeholder="Organizer's Name"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6  form-group "><input type="email" id="organiseremail" name="organiser[email]" class="form-control" required placeholder="Email Id"></div>
                        <div class="col-sm-6  form-group "><input type="text" id="organisercontactno" name="organiser[cntnumber]" class="form-control" required placeholder="Contact Number"></div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12  form-group ">
                            <textarea class="form-control" id="organiseraddress" name="organiser[address]"  rows="5" cols="40" placeholder="Postal Address"></textarea>
			    <input type="hidden" name="organiser[id]" id="organiserid" /> 
			</div>
                    </div>

               </div>          
              
              <div class="submitform hide">
                <div class="row hide terms">
                    <div class="col-sm-12 checkbox">
                        <label><input type="checkbox" name="tncPayment" >I accept <a href="terms#payment" style="color:#007ea4" target="_blank">terms &amp; conditions</a> of Passionstreet.in payment gateway usage.</label>
                    </div>
                </div>
				
                <div class="row">
                    <div class="col-sm-12 text-center">
			 <button class="btn btn-primary"  type="submit" name="step1" value="step1">NEXT</button>
                    </div>
                </div>

                
               </div>
            </form>
        </div>
    </div>
    <div  role="tabpanel" class="col-md-12 row tab-pane <?=($showform['step2'])?$showform['step2'].' fade active in':'hide'?> " id="step2">
        <div class="form-wrapper">
            <form class="form1 event-manage" method="post" id="form-step2">
                
                <div class=" row hide">
                    <label class="col-sm-6 col-xs-12 form-group  control-label"><h4 class="hstyl_4">Event participation</h4></label>
                    <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_visibility" value="open-to-all" class="event_visibility" checked="checked">
                          Open to all
                        </label>
                     </div>
		    <?php
		    /* ?><div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_visibility" value="open-to-all" class="event_visibility">
                          Open to all
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_visibility" value="invite-only" id="invitebtn" class="event_visibility">
                          By invitation
                        </label>
                     </div>
                    <div class="form-group row hide" id="invitebox">
                        <div class="col-sm-12 ">
                            <div id="inviteboxinner"></div>
                        </div>
                    </div>
		    <?php */ ?>
               </div>
                
               <div class=" row">
                    <label class="col-sm-6 col-xs-12 form-group  control-label"><h4 class="hstyl_4">Is there any selection criteria ?</h4></label>
                    <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_precondition"  value="Yes" id="event_precondition">
                          Yes
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="event_precondition"  value="No">
                          No
                        </label>
                     </div>
               </div>
	       <div class="form-group row hide" id="preconditionbox">
			<div class="col-sm-12 ">
				<div id="preconditioninner"></div>
			</div>	
		</div>
               
                <div class="row">
                    <div class="col-sm-12 text-center">
					    <button class="btn btn-primary"  type="submit" name="step2" value="step2">NEXT</button> <a class="btn " href="<?=(str_replace('step2','step1',$_SERVER['PHP_BROWSER_URL']))?>">BACK</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div  role="tabpanel" class="col-md-12 row tab-pane <?=($showform['step3'])?$showform['step3'].' fade active in':'hide'?>" id="step3">
        <div class="form-wrapper">
            <form class="form1 event-manage"  method="post" id="form-step3">
                <div class=" row">
                    <label class="col-sm-6 col-xs-12 form-group  control-label"><strong>Do you want to include route map?</strong></label>
                    <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" value="Yes" class="event_include_routemap"  name="event_include_routemap">
                          Yes
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" value="No" class="event_include_routemap"  name="event_include_routemap">
                          No
                        </label>
                     </div>
               </div>
                <div class="row event_routemap hide">
                    <div class="col-sm-12 form-group">
                        <?php
			/* ?>
			<div class="form-group row ">
                        	<div class="col-sm-4">
                        	     <input type="text" id="map-input" placeholder="Add Point Of Interest 1 (POI)" class="form-control">
                        	</div>
                        	<div class="col-sm-4">
                        	     <input type="text" id="map-input2" placeholder="Add Point Of Interest 2 (POI)" class="form-control">
                        	</div>
                        	<div class="col-sm-4">
                        	     <input type="text" id="map-input3" placeholder="Add Point Of Interest 3 (POI)" class="form-control">
                        	</div>
                        </div>
                        <div class="form-group row ">
                             <div class="col-sm-12 text-right">
                              <span class="add-more">Add more POI</span>
                            </div>
                        </div>
			<?php */ ?>
			<div class="form-group row ">
                        	<div class="col-sm-12">
                        	     <input type="text" id="map-input" placeholder="Search Point of Interest" class="form-control">
                        	</div>
			</div>
                        <div class="row " id="waypoints">
                        </div>
                        <div class="row form-group">
                        	<div class="col-sm-12"><input type="button" style="margin:0;" class="btn btn-primary" id="btn_gen_route" value="Generate Route" /></div>
                        </div>
                        <input type="hidden" class="form-group " id="route_pic" name="route_pic" value="" />
                        <div id="routeMap" style="height:350px;width:100%; border:1px solid #ccc;">
                        </div>
                    </div>
                </div>
                <!-- <div class="row publish">
                    <label class="col-sm-6 col-xs-12 form-group  control-label">Do you want to Publish this event in</label>
                    <div class="col-sm-6 col-xs-12 socialbar_2 circle">                                   
                        <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="twtr"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="gplus"><i class="fa fa-google-plus"></i></a>
                     </div>                            
               </div> -->                     
                
                <div class=" row">
                    <label class="col-sm-6 col-xs-12 form-group  control-label">User engagement</label>
                    <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="allowuserpost"  value="Allow">
                          Allowed
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-3">
                        <label>
                          <input type="radio" name="allowuserpost"  value="Moderate">
                          Moderated
                        </label>
                     </div>
               </div>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button class="btn btn-primary" type="submit" name="step3" value="step3">PUBLISH</button> <a class="btn " href="<?=(str_replace('step3','step2',$_SERVER['PHP_BROWSER_URL']))?>">BACK</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
if($showform['step3'])
// $PSJsincludes['external'][] = "https://maps.googleapis.com/maps/api/js?key=".$PSParams['google_maps_key']."&libraries=places&callback=initGoogleMapsRouteMapCreate";
$PSJsincludes['external'][] = "https://maps.googleapis.com/maps/api/js?key=".$PSParams['google_maps_key']."&libraries=places";
?>
<script>
$(document).ready(function(){


    $("#invitebtn").on('click',function(){
        $("#invitebox").removeClass('hide');
        $("#inviteboxinner").html(calliframe("module/invite?relatedto=<?=$event['event_id']?>&relatedtotype=event"));
    });
	
	$("#event_precondition").on('click',function(){
		if($(this).val() == 'Yes'){
			$("#preconditionbox").removeClass('hide');
			$("#preconditioninner").html(calliframe("module/questionsadd?questiontype=eventquestionaire&relatedto="+q['event_id']));
		}
    });

    $(".ticketcountradio").on('click',function(){
        if($(this).val() == 'multi'){
            $(".ticketcountdefine").removeClass('hide');
            $("#ticketcount").val('1');
            $(".ticketcounttext").val('1');
        }
        else{
            $(".ticketcountdefine").addClass('hide');
            $("#ticketcount").val('1');  
            $(".ticketcounttext").val('1');
        }
    })
    $(".ticketcounttext").on('keyup keypress blur change',function(){
        if($.trim($(this).val())!='')
            $("#ticketcount").val($.trim($(this).val())); 
        else{
            $("#ticketcount").val('0');
        }
    });
    
    $(".ticketfeeradio").on('click',function(){
        if($(this).val() == 'Yes'){
            getBankAccountsList();
            $(".bank-account").removeClass('hide');
            //$(".tickets-total-count").addClass('hide');
            $(".tickets-price").removeClass('hide');
            $(".terms").removeClass('hide');
            $(".submitform").removeClass('hide');
        }
        else{
            $(".bank-account").addClass('hide');
            //$(".tickets-total-count").removeClass('hide');
            $(".tickets-price").addClass('hide');
            $(".terms").addClass('hide');
            $(".submitform").removeClass('hide');
        }
    });
    
    $(".more-ticket-categories").on('click',function(){
        $(".ticket-price-row-box").append($(".ticket-price-row").eq(0).wrapAll('<div>').parent().html());
    });
    
    $(".event_include_routemap").click(function(){
        
        if($(this).val() == 'Yes'){
            $(".event_routemap").removeClass("hide");
	    $("#waypoints").html('');
	    initGoogleMapsRouteMapCreate();
        }
        else{
            $(".event_routemap").addClass("hide");
        }
    });
    
    $("#addNewBackAccount").click(function(){
    	$(".addNewBackAccountForm").html(calliframe('module/bankaccountadd'));
    });
    $('#showpagelist :radio').click(function(){
	    return false;
    });
    $('#showpagelist').popover({title: "Select from My Active Pages", content: populatePageList(), html: true, placement: "auto"}).on("show.bs.popover", function () { $(this).data("bs.popover").tip().css("max-width", "600px"); });;
    $('body').on('click','.selectpageasorganiser',function(event){
	$pageId = $(this).attr('id').replace('pg_','');
	populateOrganiserForm(pageContact[$pageId]);
	$('#showpagelist :radio').prop("checked","checked");
	$('#showpagelist').trigger('click');
	event.preventDefault();
    });
    $("#userasorganiser").click(function(){
	populateOrganiserForm(userContact[profile_id]);
    });
});
function populateOrganiserForm($data){
	$("#organiserid").val($data['id']);
	$("#organisername").val($data['name']);
	$("#organiseremail").val($data['email']);
	$("#organisercontactno").val($data['contact']);
	$("#organiseraddress").val($data['address']);
}
function populatePageList(){
	$x = '<ul class="list-group">';
	$.each(pageContact,function(i,v){
		$x += '<li class="list-group-item"><a href="#" class="selectpageasorganiser" id="pg_'+v['id']+'">'+v['name'] +'('+v['email']+')'+'</a></li>';
	});
	$x += '</ul>';
	return $x;
}
function getBankAccountsList(){

	$.get("PSAjax.php", {type:'getBankAccountList'}, function(data){
	    if(data['list']['accounts'].length == 0)
	    {
		 $("#addNewBackAccount").tab('show');
		 $(".addNewBackAccountForm").html(calliframe('module/bankaccountadd'));
	    }
	    else
	    {
		 $bankliststr = '<div class="row"> <div class="col-sm-1"></div> <div class="col-sm-4"><strong>Account Name</strong></div> <div class="col-sm-4"><strong>Bank Name</strong></div> <div class="col-sm-3"><strong>Account Number</strong></div>  </div>';
		$.each(data['list']['accounts'],function(i,v){
		    $bankliststr = $bankliststr + '<div class="row"> <div class="col-sm-1"><label><input type="radio" name="ticket_bank_account" value="'+v['account_id']+'" ></label></div> <div class="col-sm-4">'+v['accountholdername']+'</div> <div class="col-sm-4">'+v['bankname']+'</div> <div class="col-sm-3">'+v['acntnmbr']+'</div> </div>'; 
		});
		$(".bank-account-list").html($bankliststr);
	    }
	}, 'json');
}
    
</script>
<script>
function initGoogleMapsRouteMapCreate() {
  map_position_obj = $.parseJSON(map_position);
  marker_obj = [];
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;
  map = new google.maps.Map(document.getElementById('routeMap'), {
    zoom: 6,
    center: map_position_obj
  });
  directionsDisplay.setMap(map);
  
  var input = document.getElementById('map-input');
  google.maps.event.addDomListener(input, 'keydown', function(e) { 
	    if (e.keyCode == 13) { 
		e.preventDefault(); 
	    }
  });
  var searchBox = new google.maps.places.SearchBox(input);
  
  map.addListener('bounds_changed', function() {
	console.log('bounds changed');
    searchBox.setBounds(map.getBounds());
  });
  
  var markers = [];
  var bounds = new google.maps.LatLngBounds();
    
  searchBox.addListener('places_changed', function() {
    document.getElementById('map-input').value = '';
    console.log('places changed');
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    //markers.forEach(function(marker) {
     // marker.setMap(null);
    //});
   // markers = [];

    // For each place, get the icon, name and location.
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      marker = new google.maps.Marker({
        map: map,
        title: place.name,
        position: place.geometry.location,
        draggable:true
      });
      markers.push(marker);
      marker_obj = {"title":marker.title, "position":{'lat':marker.position.lat(),'lng':marker.position.lng()}};
      addMarkerToRoute(marker_obj);
      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  document.getElementById('btn_gen_route').addEventListener('click', function() {
    var checkboxArray = $('#waypoints input:checked');
    var start = $.parseJSON(checkboxArray.eq(0).attr('pos'));
    var stop = $.parseJSON(checkboxArray.eq(checkboxArray.length-1).attr('pos'));
    var waypts = [];
    for(i=1;i<(checkboxArray.length-1);i++)
    {
      waypts.push({
        location: $.parseJSON(checkboxArray.eq(i).attr('pos')),
        stopover: true
      });
    }
    calculateAndDisplayRoute(directionsService, directionsDisplay,start , stop , waypts);
  });
  })
}
function addMarkerToRoute(obj){
    var newobj = {};
    newobj['title'] = obj.title;
    newobj['position'] = obj.position;
    $("#waypoints").append("<div class='col-sm-4 col-xs-2 checkbox'><label class='form-group control-label'><input type='checkbox' checked name='waypoints[]' value='"+JSON.stringify(newobj)+"' pos='"+JSON.stringify(obj.position)+"' />"+obj.title+"</label></div>");
}

function routemaptoimage(){
    var currentPosition=map.getCenter();
    var checkboxArray = $('#waypoints input:checked');
    $path = "";
    $markers = "";
    for(i=0;i<(checkboxArray.length);i++)
    {
        $x = $.parseJSON(checkboxArray.eq(i).attr('pos'));
        $path = $path+"|"+$x['lat']+","+$x['lng'];
        $markers = $markers +"&markers=color:red|label:"+encodeURI((i+1)+"-"+checkboxArray.eq(i).parent().text())+"|"+$x['lat']+","+$x['lng'];
    }
    $str =  "http://maps.google.com/maps/api/staticmap?key="+google_maps_key+"&sensor=false&center=" +
        currentPosition.lat() + "," + currentPosition.lng() +
        "&zoom="+map.getZoom()+"&size=300x200&path=color:0xff0000ff|weight:5"+$path+$markers;
    return $str;
}

</script>
<?php
//$PSJsincludes['external'][] = "js/jquery.datetimepicker.js";
//$PSCssincludes['external'][] = "css/jquery.datetimepicker.css";
$PSJsincludes['external'][] = "js/bootstrapdatetimepicker.min.js";
$PSCssincludes['external'][] = "css/bootstrap-datetimepicker.min.css";
?>