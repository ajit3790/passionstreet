<div id="map" style="width:100%;min-height:400px"></div>
<script async src="https://maps.googleapis.com/maps/api/js?key=<?=$PSParams['google_maps_key']?>&libraries=places&callback=initGoogleMaps"></script>
<script>
function initGoogleMaps() {
  map_position_obj = $.parseJSON(map_position);
  map = new google.maps.Map(document.getElementById('map'), {
    center: map_position_obj,
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var markers = [];
  marker = new google.maps.Marker({
    map: map,
    title: 'Connaught Place, New Delhi, India',
    position: map_position_obj
  });
}
</script>