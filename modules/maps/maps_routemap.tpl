<div id="routeMap" style="width:100%;min-height:400px"></div>
<script async src="https://maps.googleapis.com/maps/api/js?key=<?=$PSParams['google_maps_key']?>&libraries=places&callback=initGoogleMapsRouteMap"></script>
<script>
function initGoogleMapsRouteMap() {
  map_position_obj = $.parseJSON(map_position);
  coordinates = $.parseJSON('<?=$coordinates?>');
  console.log(coordinates);
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;
  map = new google.maps.Map(document.getElementById('routeMap'), {
    zoom: 6,
    center: map_position_obj
  });
  directionsDisplay.setMap(map);
  
  
  var markers = [];
  var bounds = new google.maps.LatLngBounds();
  var waypts = [];
  var start = coordinates[0]['position'];
  var stop = coordinates[coordinates.length-1]['position'];
    
  for(i=0;i<(coordinates.length);i++)
  {
    if(!(i==0 || i==(coordinates.length-1)))
    {
       waypts.push({
        location: coordinates[i]['position'],
        stopover: true
      });
    }
    marker = new google.maps.Marker({
      map: map,
      title: coordinates[i]['title'],
      position: coordinates[i]['position']
    });
    markers.push(marker);
    
  }
    for(i=0;i<markers.length;i++) {
        bounds.extend(markers[i].getPosition());
    }

    map.fitBounds(bounds);
    
    calculateAndDisplayRoute(directionsService, directionsDisplay,start,stop,waypts);
      
}
function calculateAndDisplayRoute(directionsService, directionsDisplay,start , stop , waypts) {
  
  directionsService.route({
    origin: start,
    destination: stop,
    waypoints: waypts,
    optimizeWaypoints: true,
    travelMode: google.maps.TravelMode.DRIVING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}
</script>