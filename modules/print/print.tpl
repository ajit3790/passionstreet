<style>.print .content{display:none;}</style>
<?php
//https://passionstreet.ini/passionstreet3/event/winter-delhi-half-marathon/event-8604225488?printticket=1&txnid=transaction-2903533027
if($server_response_html)
echo '<div class="row"><div class="col-md-12">'.$server_response_html.'</div></div>';
if($ticketstatus)
{
?>
<div class="text-center">
	<br>
	<br>
	<h2>Congratulations!  </h2>
	<br>
	You have successfully booked your ticket, please share on your Facebook timeline / twitter / google plus and encourage your friends and followers to join this event and have fun.
	<br>
	<br>
	<?php
	$sharetitle = 'I am going to '.$eventmeta['title'];
	$sharedescription = 'Do join me at '.$eventmeta['title'].' and we can have fun together';
	$shareimage = $eventmeta['image'];
	$shareurl = $eventmeta['url']
	?>
	<div class="socialbar_2 circle sm">
		<a data-type="fb" class="fb customshare" data-title="<?=$sharetitle?>" data-description="<?=$sharedescription?>" data-image="<?=$shareimage?>" data-shareurl="<?=$shareurl?>" data-dialog="1" target="_parent"><i class="fa fa-facebook"></i></a>
		<a data-type="tw" class="twtr customshare" data-title="<?=$sharetitle?>" data-description="<?=$sharedescription?>" data-image="<?=$shareimage?>" data-shareurl="<?=$shareurl?>" data-dialog="1" target="_parent"><i class="fa fa-twitter"></i></a>
		<a data-type="gg" class="gplus customshare" data-title="<?=$sharetitle?>" data-description="<?=$sharedescription?>" data-image="<?=$shareimage?>" data-shareurl="<?=$shareurl?>" data-dialog="1" target="_parent"><i class="fa fa-google-plus"></i></a>
	</div>
</div>
<?php
}
?>
<script>
meta = {};
meta['keywords'] = 'Event,JoinMe';
$(document).ready(function(){
	if($('.printtoolbar .ticketprintbtn').length > 0 )
	$('.printtoolbar .ticketprintbtn').trigger('click');
})
</script>