<?php
/*if(!loggedId())
{
header('location:'.ROOT_PATH); 
exit();
}*/
if($_GET['type'] == 'event')
{
	if($_GET['subtype'] == 'ticket')
	{
		if($_GET['txnid'])
		{
			$txnid = $_GET['txnid'];
			$txndetail = transaction_get_details($txnid);
			if(!($txndetail['userid'] == $PSData['user']['profile_id'] || $txndetail['userid']=='unknown'))
			{
				header('location:'.ROOT_PATH); 
				exit();
			}
			$eventdetail = event_get_details($txndetail['productid'],1);
			//$creatordetail = user_profile(array('profile_id'=>$txndetail['userid']));
			$ticketdetails = unserialize($txndetail['productdetails']);
			$ticketarray = ticket_get_list(array('tkttransactionid'=>$txnid));
			if($eventdetail && $txndetail['status'] == 'complete|success')
			{
				$ticketnewarray = array();
				foreach($ticketarray as $ticketdata)
				{
					$ticketnewarray[$ticketdata['tktcode']][] = $ticketdata;
				}
				$ticketdetails['ticketing'] = $ticketnewarray;
				
				//print_array(array($txndetail,$eventdetail,$ticketnewarray));
				
                                $eventstartdate = $eventdetail['datestart'];
				$eventenddate = $eventdetail['dateend'];
				if($eventenddate == $eventstartdate)
				{
					$eventdatetxt = date('jS M,Y',strtotime($eventstartdate));
					$eventdatedetailedtxt = date('l, F j Y h:i a',strtotime($eventstartdate));
				}
				else
				{
					$eventdatetxt = date('jS M,Y',strtotime($eventstartdate)).' - '.date('jS M,Y',strtotime($eventenddate));
					$eventdatedetailedtxt = date('l, F j Y h:i a',strtotime($eventstartdate)).' To '.date('l, F j Y h:i a',strtotime($eventenddate));
				}
				$eventlocationtext = $eventdetail['location'];
				if(strlen($eventlocationtext)>40)
				$eventsubtitle = $eventdatetxt.'<br>'.$eventlocationtext;
				else
				$eventsubtitle = $eventdatetxt.$eventlocationtext;
				$title2 = 'Time &amp; Venue';
				$tables = '';
                                
				if($eventdetail['description'])
				{
					/*preg_match_all('(<table.*?>(.*?)<\/table>)', $eventdetail['description'], $matches);
					if($matches && $matches[0][0])
					{
					$tables .= str_replace('<table','<table width=100% border=1 cellspacing=0 cellpadding=5 ',$matches[0][0]).'</table>';
					$title2 .= ' And Description';
					}*/
					//preg_match_all('/(<table[^>]*>(?:.|\n)*?<\/table>)/', $eventdetail['description'], $matches);
					preg_match_all('(<table.*?>(.*?)<\/table>)', $eventdetail['description'], $matches);
                                        if($matches)
					{
						foreach($matches[0] as $key=>$data)
						{
							$tables .= str_replace('<table','<br /><table style="margin-left:2%;margin-right:2%;width:96%;margin-top:5px;margin-bottom:5px;" border=1 cellspacing=0 cellpadding=5 ',$matches[0][$key]);
						}
						$title2 .= ' And Description';
					}
				}
				$ticketextra = '';
				
				$html = file_get_contents(CORE_DIR.'/template/print/eventticketcomplete.tpl');
				$html = str_replace('<!--##eventtitle##-->',$eventdetail['eventname'],$html);
				$html = str_replace('<!--##eventsubtitle##-->',$eventsubtitle,$html);
				$html = str_replace('<!--##transactionid##-->',$txndetail['transactionid'],$html);
				$html = str_replace('<!--##transactionamount##-->',$txndetail['amount'],$html);
				$html = str_replace('<!--##eventvenue##-->',$eventdetail['address'],$html);
				$html = str_replace('<!--##eventtimedetailed##-->',$eventdatedetailedtxt,$html);
				$html = str_replace('<!--##eventtables##-->',$tables,$html);
				$html = str_replace('<!--##eventtitle2##-->',$title2,$html);
				//$html = str_replace('<!--##eventcreator##-->',ucwords($creatordetail['fname'].' '.$creatordetail['fname']),$html);
				$html = str_replace('<!--##eventorganiser##-->',$eventdetail['organiser']['name'],$html);
				$html = str_replace('<!--##eventorganiseremail##-->',$eventdetail['organiser']['email'],$html);
				
				$alltickethtml = '';
				$tickettemplate = file_get_contents(CORE_DIR.'/template/print/eventticketindividual.tpl');
				//print_array($ticketdetails);
				//print_array($eventdetail);
				$ticketcategoriesdetail = array();
				foreach($eventdetail['ticketcategories'] as $temp)
				{
					$ticketcategoriesdetail[$temp['name']] = $temp;
				}
				$ccusers = array();
				foreach($ticketdetails['ticketing'] as $tickettype=>$ticket)
				{
					foreach($ticket as $index=>$ticketdata)
					{
						if($ticketdata['tktbibno'])
						$ticketextra = '<br />Bib No.&nbsp;<strong>'.$ticketdata['tktbibno'].'</strong>';
						$ccusers[] = array($ticketdata['tktemail'],$ticketdata['tktname']);
						$temp = $tickettemplate;
						$temp = str_replace('<!--##ticketuser##-->',$ticketdata['tktname'].' ('.$ticketdata['tktemail'].')',$temp);
						$temp = str_replace('<!--##ticketnumber##-->',$ticketdata['tktid'],$temp);
						$temp = str_replace('<!--##tickettype##-->',$eventdetail['ticketcategories'][$tickettype]['name'],$temp);
						$temp = str_replace('<!--##ticketprice##-->',$eventdetail['ticketcategories'][$tickettype]['price'],$temp);
						$temp = str_replace('<!--##ticketextra##-->',$ticketextra,$temp);
						$barcodedata = urlencode('Event:'.$eventdetail['eventname']);
						$barcodedata .= '%0A'.urlencode('Event Link:'.ROOT_PATH.'event/'.$eventdetail['seopath']);
						$barcodedata .= '%0A'.urlencode('Attendee:'.$ticketdata['tktname']);
						$barcodedata .= '%0A'.urlencode('Ticket Number:'.$ticketdata['tktid']);
						$barcodedata .= '%0A'.urlencode('Ticket Type:'.$tickettype);
						$barcodedata .= '%0A'.urlencode('Ticket Price:'.$ticketcategoriesdetail[$tickettype]['price']);
						$barcodedata .= '%0A'.urlencode('Order Id:'.$txndetail['transactionid']);
						$barcodedataimg = 'https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl='.$barcodedata.'&choe=UTF-8';
						$temp = str_replace('<!--##ticketbarcode##-->',$barcodedataimg,$temp);
						$alltickethtml .= $temp;
					}
				}
				$html = str_replace('<!--##transactiontickets##-->',$alltickethtml,$html);
				$html = str_replace('<!--##root_path##-->',ROOT_PATH,$html);
				
				$filename = get_alphanumeric($txndetail['userid'].'-'.$txndetail['transactionid']).".html";
				$file_to_save = UPLOAD_DIR.DIRECTORY_SEPARATOR.'tickets'.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.$filename;
				file_put_contents($file_to_save, $html); 

				//echo $html;
				/*
				set_include_path(get_include_path() . PATH_SEPARATOR . CORE_DIR."/dompdf");
				require_once "dompdf_config.inc.php";

				$dompdf = new DOMPDF();
				
				$dompdf->load_html($html);
				$dompdf->render();
				
				$filename = get_alphanumeric($eventdetail['eventname'].'-'.$txnid).".pdf";
				
				$output = $dompdf->output();
				$file_to_save = UPLOAD_DIR.DIRECTORY_SEPARATOR.'tickets'.DIRECTORY_SEPARATOR.'pdf'.DIRECTORY_SEPARATOR.$filename;
				file_put_contents($file_to_save, $output); 
				mailer(array(
					"profile_id" => $txndetail['userid'],
					"eventdetail" => $eventdetail,
					"txndetail" => $txndetail,
					"tickets" => $ticketnewarray,
					"html"=>$html
				), 'ticketonmail');
				$output = $dompdf->output();
				$dompdf->stream(get_alphanumeric($eventdetail['eventname'].'-'.$txnid).".pdf");
                                */
				//$PSModData['server_response_html'] = '<div class="msgbox1 text-center"><p>Ticket generation is complete . Check your downloads folder </p></div>';
				if($txndetail['userid'] == 'unknown')
				{
				$productdetails = (unserialize($txndetail['productdetails']));
				$key = key($productdetails['ticketing']);
				$email_id = $productdetails['ticketing'][$key][0]['email'];
				}
				else
				$email_id = '';
                                mailer(array(
					"profile_id" => ($txndetail['userid'] != 'unknown')?$txndetail['userid']:0,
					"email_id" => $email_id,
					"ccusers"=> $ccusers,
					"eventdetail" => $eventdetail,
					"txndetail" => $txndetail,
					"tickets" => $ticketnewarray,
					"html"=>$html
				), 'ticketonmail',NULL,'blank.tpl');
				$PSModData['eventmeta'] = array();
				$PSModData['eventmeta']['title'] = $eventdetail['eventname'];
				$PSModData['eventmeta']['description'] = chop_string(strip_tags($eventdetail['description']),250);
				$PSModData['eventmeta']['image'] = $eventdetail['eventpic'];
				$PSModData['eventmeta']['url'] = ROOT_PATH.'event/'.$eventdetail['seopath'];
				$PSModData['eventmeta']['venue'] = $eventdetail['venue'];
				$PSModData['ticketstatus'] = 1;
                                $PSModData['server_response_html'] = '<div class="printtoolbar"><a class="btn btn-primary ticketprintbtn" href="javascript:void(0);" onclick="window.print()">Click to print Ticket</a></div><div class="content">'.$html.'</div>';
				
			}
			else if($txndetail['status'] == 'complete|failure')
			{
				$PSModData['ticketstatus'] = 0;
				$PSModData['server_response_html'] = '<div class="msgbox1 text-center"><p>The transaction has failed , thus ticket cannot be generated. </p></div>';
			}
			else if(empty($eventdetail))
			{
				$PSModData['ticketstatus'] = 0;
				$PSModData['server_response_html'] = '<div class="msgbox1 text-center"><p>Some error occured, please contact site admin</p></div>';
			}
			else
			{
				$PSModData['ticketstatus'] = 0;
				$PSModData['server_response_html'] = '<div class="msgbox1 text-center"><p>Some error occured, please contact site admin</p></div>';
			}
		}
	}
}
?>