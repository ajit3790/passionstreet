<?php
$profiletype = (!empty($_GET['profile_id']))?'user':'self';
if($profiletype == 'self')
{
    $PSModData['profiledetails'] = $PSData['user'];
    $_GET['profile_id'] = $PSData['user']['profile_id'];
}
else if($_GET['profile_id'] == $PSData['user']['profile_id'])
{
    $profiletype = 'self';
    $PSModData['profiledetails'] = $PSData['user'];
    $_GET['profile_id'] = $PSData['user']['profile_id'];
}
else
{
    $profile_id = $_GET['profile_id'];
    $PSModData['profiledetails'] = user_profile(array("profile_id"=>$profile_id));
}
if(empty($PSModData['profiledetails']['profile_id']))
header('location:'.ROOT_PATH);
$PSParams['profiledetails'] = $PSModData['profiledetails'];

$PSModData['profiledetails']['passions'] = user_passions($PSModData['profiledetails']['profile_id']);
$PSModData['profiledetails']['type'] = $profiletype;
$metadescriptionstr = '';
$fname =ucwords($PSModData['profiledetails']['fname'].' '.$PSModData['profiledetails']['lname']);
if(!empty($PSModData['profiledetails']['passions']['isExpert']))
{
    $metadescriptionstr = $fname. ' is expert in ';
    $temp = array();
    foreach($PSModData['profiledetails']['passions']['isExpert'] as $key=>$passion)
    {
        if($key>2)
            break;
        $temp[] = $passion;
    }
    $metadescriptionstr .= implode(',',$temp);
    $temp = count($PSModData['profiledetails']['passions']['isExpert']);
    if($temp > 3)
    {
        $metadescriptionstr .= ' and '.($temp-3).' other passions';
    }
}
else if(!empty($PSModData['profiledetails']['passions']['joinedPassion']))
{
    $metadescriptionstr = $fname.' is Passionate about ';
    $temp = array();
    foreach($PSModData['profiledetails']['passions']['joinedPassion'] as $key=>$passion)
    {
        if($key>2)
            break;
        $temp[] = $passion;
    }
    $metadescriptionstr .= implode(',',$temp);
    $temp = count($PSModData['profiledetails']['passions']['joinedPassion']);
    if($temp > 3)
    {
        $metadescriptionstr .= ' and '.($temp-3).' other passions';
    }
}
if($_GET['pagetype'] == 'userprofile')
{
$GLOBALS['page_meta']['title'] = 'Connect with '.$fname.' on PASSIONSTREET';
$GLOBALS['page_meta']['description'] = (!empty($PSModData['profiledetails']['bio'])?$PSModData['profiledetails']['bio'].'\n':'').$metadescriptionstr;
$GLOBALS['page_meta']['image'] = ($PSModData['profiledetails']['userdp'] != 'profiledp')?$PSModData['profiledetails']['userdp']:$PSModData['profiledetails']['userbg'];
}
?>