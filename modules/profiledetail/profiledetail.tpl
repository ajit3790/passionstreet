<div class="row">
	<div class="col-md-12 no-padding clearfix">
	   <?php
		if($profiledetails['type'] == 'self')
		{
		?>
		<div class="settings">
			<span class="toggle-setting">Settings <i class="fa fa-cog"></i></span>

			<ul>
			    <li><a href="#" >Edit Profile <i class="fa fa-pencil"></i></a> </li>
			    <li><a href="#" class="callmodaliframe" data-targetsrc="module/setting/activity">Activity Setting <i class="fa fa-user-secret"></i></a> </li>
			    <li><a href="#" class="callmodaliframe" data-targetsrc="module/setting/privacy">Privacy Setting <i class="fa fa-gear"></i> </a></li>
			    <li><a href="#" class="callmodaliframe" data-targetsrc="module/setting/notification">Notification Setting <i class="fa fa-bell"></i></a></li>
			    <li><a href="#" class="callmodaliframe" data-targetsrc="module/setting/password">Change Password <i class="fa fa-lock"></i></a></li>
			</ul>
	       
		
		</div>
		<?php
		}
		if($profiledetails['type'] == 'self')
		{
		?>
	       <!-- <a href="#" class="invite">Invite Friends</a> -->
		<?php
		}
		?>
	   <div class="image_section" style="background-image1:url('<?=$profiledetails['userbg']?>')">
	   <img src="<?=$profiledetails['userbg']?>" class="covr_phto" id="covr_phto1">
	   <div class="layer">

		    <div class="profile-pic-outer">
		       <div class="profile_pic">
			    <img class="dp_phto" id="dp_phto" alt="<?=$profiledetails['fname']?>" src="<?=$profiledetails['userdp']?>"> 
			    <?php 
			    if($profiledetails['
			    s']['isExpert'])
			    echo '<span class="expert-flag"></span>'; 
			    if($profiledetails['type'] == 'self')
			    {
			    //echo '<input type="file" name="userdp" croptype="dp" id="upload2" class="multifilecrop" data-crpvrsn=2 >';
			    echo '<span class="change" href="#"><input type="file" name="userdp" pictype="userdp" croptype="userdp" callback="ajaxUpload2" id="upload2" class="multifilecrop filehidden" data-crpvrsn=2 org-selector="dp_phto" style="position:absolute;" /><i class="fa fa-camera"></i> Edit</span>';
			    //echo '<span class="change" href="#"><input type="file" pictype="userdp" callback="ajaxUpload" org-width='.$PSJavascript['croptype']['dp']['sizes'][0][0].' org-height='.$PSJavascript['croptype']['dp']['sizes'][0][1].' org-selector=".dp_phto" id="imgg" class="filehidden filecrop"/><i class="fa fa-camera"></i> Edit</span>';
			    }
			    ?>
		       </div>
		       <span class="name"><?php echo ucwords($profiledetails['fname'].' '.$profiledetails['lname']);?></span>
		       Age <?=$profiledetails['age']?> years<br>
		       From <?php echo ucwords($profiledetails['city'].' , '.$profiledetails['country']);?>
		    </div>
		    
		    
		    <?php
		    if($profiledetails['type'] == 'self')
		    {
		    echo '<span class="change_covr" href="#"><input id="image_file" type="file" name="userbg" callback="ajaxUpload2" pictype="userbg" croptype="userbg" id="upload3" class="multifilecrop filehidden" data-crpvrsn=2 org-selector="covr_phto1"  style="position:absolute;" /><i class="fa fa-camera"></i> Change background photo</span>';
		    //echo '<span class="change_covr" href="#"><input id="image_file" pictype="userbg" callback="ajaxUpload" org-width='.$PSJavascript['croptype']['bg']['sizes'][0][0].' org-height='.$PSJavascript['croptype']['bg']['sizes'][0][1].' org-selector=".covr_phto1" type="file" class="filehidden filecrop"/><i class="fa fa-camera"></i>Change background photo</span>';
		    }
		    ?>

	   </div>
	   </div>
	   <div class="desc_section">
		
		 <div class="desc">
			<div class="clearfix">
			<?php
			$temp = array();
			$temp['isExpert']['title'] = 'Expert In';
			$temp['joinedPassion']['title'] = 'Passion';
			foreach($temp as $passionType => $pdata)
			{
			    if($profiledetails['passions'][$passionType])
			    {
				echo '<div class="pull-left">';
				echo '<h5>'.$pdata['title'].'</h5>';
				echo '<div class="passions">';
				foreach($profiledetails['passions'][$passionType] as $key=>$passion)
				{
				    if($key>3)
				    {
					echo '+';
					break;
				    }
				    $imgsrc = 'images/'.$PSParams['PSCategories'][$passion]['icon'];
				    $imgalt = $PSParams['PSCategories'][$passion]['name'];
				    echo '<a href="'.ROOT_PATH.$PSParams['PSCategories'][$passion]['link'].'"><img alt="'.$imgalt.'" class="unveil" src="'.DEFAULT_IMG.'" data-src="'.$imgsrc.'"></a>';
				}
				echo '</div>';
				echo '</div>';
			    }
			}
		       ?>
		       </div>
		       
		    
		       <?php
		       if($profiledetails['type'] == 'self')
		       {
		       ?>
		       <div class='clear'>
		       <textarea class='editbio hide' id="userbio" placeholder="Say something about you!" cols="40" rows="5"><?=ucfirst($profiledetails['bio'])?></textarea>
		       <button class="btn btn-primary btn-sm editbio hide" id="savebio" >Save</button>
		       <p class="bio editbio"><?=ucfirst($profiledetails['bio'])?></p>
		       <!--<button class="btn btn-primary btn-sm editbio" >EDIT</button>--></div>
		       <?php
		       }
		       else
		       {
		       ?>
		       <p><?=ucfirst($profiledetails['bio'])?></p>
		       
		    </div>
	   </div>
	    <?php
		       if($profiledetails['amIfollowinghim'] == 0)
		       echo '<button class="followbtn btn btn-primary btn-sm ajax-btn"  data-action="post" data-aj="true" data-p-str="profile_id" data-t="user-follow" data-profile_id="'.$profiledetails['profile_id'].'">FOLLOW</button>';
		       else
		       echo '<button class="followbtn btn btn-primary btn-sm" >UNFOLLOW</button>';
		       ?>
		       <?php
		       }
		       ?>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#savebio").click(function(){
	    $temp = $(this);
	    $.post(root_path+'PSAjax.php?type=updateprofile', {bio:$("#userbio").val()}, function(data){ 
	        $res = $.parseJSON(data);
	        $temp.html($res['data']);
	    });    
	});
	
	
});
</script>
<?php
//$PSJsincludes['external'][] = "js/jquery.Jcrop.js";
//$PSCssincludes['external'][] = "css/jquery.Jcrop.css";
$PSJsincludes['external'][] = "js/fengyuanchencropper/croppermain2.js";
$PSJsincludes['external'][] = "js/fengyuanchencropper/cropper.js";
$PSCssincludes['external'][] = "js/fengyuanchencropper/cropper.css";
?>
<?php
$breadcrumbs = array();
$breadcrumbs[] = array('name'=>'Home','url'=>ROOT_PATH);
$breadcrumbs[] = array('name'=>'Members','url'=>ROOT_PATH.'follow');
$breadcrumbs[] = array('name'=>$profiledetails['fullname'],'url'=>ROOT_PATH.'profile/'.$profiledetails['seopath']);
$PSParams['breadcrumbs'] = $breadcrumbs;
?>