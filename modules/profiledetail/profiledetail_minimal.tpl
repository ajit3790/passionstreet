<div class="bx-styl">
<div class="hover_menu1 show_nub white_bg" style="">
    <div class="hover_menu_contents" style="border:1px solid #eee;border-radius:4px;">
        <div class="ObjectCard">
            <div class="OC_hdr clearfix">
		<?php if($profiledetails['profile_id'] != $_SESSION['user']['profile_id'])
                echo '<button class="followbtn btn btn-primary btn-sm ajax-btn" data-action="post" data-aj="true" data-p-str="profile_id" data-t="user-follow" data-profile_id="'.$profiledetails['profile_id'].'">'.((empty($profiledetails['amIfollowinghim']))?'Follow':'Unfollow').'</button>';
		?>
                <a class="name" href="<?=ROOT_PATH.'profile/'.$profiledetails['seopath']?>"><img class="pic unveil" alt="" src="<?=DEFAULT_IMG?>" data-src="<?=$profiledetails['userdp']?>"><?=$profiledetails['fullname']?></a><?php /*<small>Following you</small>*/ ?>
            </div>
            <div class="OC_body" style="min-height:130px">
                <a class="name" href="<?=ROOT_PATH.'profile/'.$profiledetails['seopath']?>">
                    <img class="cvr_phto unveil " src="<?=DEFAULT_IMG?>"  alt="" data-src="<?=$profiledetails['userbg']?>" >
                    <div class="overlay">
			<?php
			
			foreach($profiledetails['passions']['joinedPassion'] as $key=>$passion)
			{
			    if($key>3)
			    {
				echo '+';
				break;
			    }
			    $imgsrc = 'images/'.$PSParams['PSCategories'][$passion]['icon'];
			    $imgalt = $PSParams['PSCategories'][$passion]['name'];
			    echo '<a href="'.ROOT_PATH.$PSParams['PSCategories'][$passion]['link'].'"><img alt="'.$imgalt.'" class="unveil" src="'.DEFAULT_IMG.'" data-src="'.$imgsrc.'"></a>';
			}
			?>
		    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>