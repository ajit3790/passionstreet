<div class="col-md-6 col-sm-6 col-lg-6 pull-right login_outer">               
    <div class="intro">
         <p>
             <span style="display:block;margin-bottom:18px">
                 PASSIONSTREET advocates action that will evoke passion for doing things you are passionate about and enjoy doing, consciously & responsibly! Here you can join and share a bond with individuals having similar passion, explore and exhibit the activities, which you are passionate about. You can also connect and consult with subject-matter experts, join relevant events & workshops, record & keep track of your activities, explore the ecosystem and choose to rediscover yourself.
             </span>
             PASSIONSTREET helps your desire to live for what you love! Explore many possibilities and above all, challenge yourself for bigger achievements. Share your views and help others to achieve their goals. It is also a platform for self managed event & workshop ticketing, guided adventure tourism, corporate engagement and much more.
         </p>
         <!--
         <div class="form-group">
            <label>Preview File Icon</label>
            <input id="file-3" name="filenew[]" type="file" multiple=true>
            <script>
                if(callbackselector['fileinput'] === undefined)
                callbackselector['fileinput'] = {};
                callbackselector['fileinput'] = "#file-3"; 
            </script>
         </div>-->
         <?php
         if(empty($_SESSION['user']['profile_id']))
         {?>
          <div class="text-center">
            <a class="btn btn-primary" href="register">Sign Up</a>
          </div>
          <?php
          }
          ?>  
    </div>
    <!--/login box -->

</div>
<div class="col-md-6 col-sm-6 col-lg-6 intro">
   <div class="video"> 
    <div class="video-box flexi-video unveil" vsrc="https://www.youtube.com/embed/9z8b9h52TPs/?rel=0&autoplay=1&autohide=1" src="<?=DEFAULT_IMG?>" data-src="http://i.ytimg.com/vi/9z8b9h52TPs/sddefault.jpg">
    </div>

     <div class="tagline clearfix">
         <span class="expert">BE AN EXPERT</span>
         <span class="partner">BE A PARTNER</span>
         <span>BE PASSIONATE</span>
     </div> 

   </div>
</div>