<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
<script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
<?php
/*
<style amp-boilerplate>
body {
	-webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
	-moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
	-ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
	animation: -amp-start 8s steps(1, end) 0s 1 normal both
}
@-webkit-keyframes -amp-start {
from {
visibility:hidden
}
to {
visibility:visible
}
}
@-moz-keyframes -amp-start {
from {
visibility:hidden
}
to {
visibility:visible
}
}
@-ms-keyframes -amp-start {
from {
visibility:hidden
}
to {
visibility:visible
}
}
@-o-keyframes -amp-start {
from {
visibility:hidden
}
to {
visibility:visible
}
}
@keyframes -amp-start {
from {
visibility:hidden
}
to {
visibility:visible
}
}
</style>
<noscript>
<style amp-boilerplate>
body {
	-webkit-animation: none;
	-moz-animation: none;
	-ms-animation: none;
	animation: none
}
</style>
</noscript>
*/
?>
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style  amp-custom>
blockquote, h1 {
	line-height: 1.35
}
address, h5 {
	line-height: 1
}
html {
	color: rgba(0, 0, 0, .87)
}
::-moz-selection {
 background: #b3d4fc;
 text-shadow: none
}
::selection {
	background: #b3d4fc;
	text-shadow: none
}
hr {
	display: block;
	height: 1px;
	border: 0;
	border-top: 1px solid #ccc;
	margin: 1em 0;
	padding: 0
}
audio, canvas, iframe, img, svg, video {
	vertical-align: middle
}
fieldset {
	border: 0;
	margin: 0;
	padding: 0
}
textarea {
	resize: vertical
}
.browserupgrade {
	margin: .2em 0;
	background: #ccc;
	color: #000;
	padding: .2em 0
}
.hidden {
	display: none
}
.visuallyhidden {
	border: 0;
	clip: rect(0 0 0 0);
	height: 1px;
	margin: -1px;
	overflow: hidden;
	padding: 0;
	position: absolute;
	width: 1px
}
.visuallyhidden.focusable:active, .visuallyhidden.focusable:focus {
	clip: auto;
	height: auto;
	margin: 0;
	overflow: visible;
	position: static;
	width: auto
}
body, html {
	width: 100%
}
.invisible {
	visibility: hidden
}
.clearfix:after, .clearfix:before {
	content: " ";
	display: table
}
.clearfix:after {
	clear: both
}

@media print {
blockquote,  img,  pre,  tr {
	page-break-inside: avoid
}
*,     :after,     :before,     :first-letter,     :first-line {
	background: 0 0;
	color: #000;
	box-shadow: none;
	text-shadow: none
}
a,  a:visited {
	text-decoration: underline
}
a[href]:after {
	content: " (" attr(href) ")"
}
abbr[title]:after {
	content: " (" attr(title) ")"
}
a[href^="#"]:after,  a[href^="javascript:"]:after {
	content: ""
}
blockquote,  pre {
	border: 1px solid #999
}
thead {
	display: table-header-group
}
img {
	max-width: 100%
}
h2,  h3,  p {
	orphans: 3;
	widows: 3
}
h2,  h3 {
	page-break-after: avoid
}
}
.mdl-accordion, .mdl-button, .mdl-card, .mdl-checkbox, .mdl-dropdown-menu, .mdl-icon-toggle, .mdl-item, .mdl-radio, .mdl-slider, .mdl-switch, .mdl-tabs__tab, a {
	-webkit-tap-highlight-color: transparent;
	-webkit-tap-highlight-color: rgba(255, 255, 255, 0)
}
html {
	height: 100%;
	-ms-touch-action: manipulation;
	touch-action: manipulation
}
body {
	min-height: 100%;
	margin: 0
}
main {
	display: block
}
[hidden] {
	display: none
}
body, html {
	font-family: Helvetica, Arial, sans-serif;
	font-size: 14px;
	font-weight: 400;
	line-height: 20px
}
h1, h2, h3, h4, h5, h6, p {
	margin: 0;
	padding: 0
}
h1, h2, h3 {
	margin-bottom: 24px;
	font-weight: 400
}
h4, h5, h6 {
	margin-top: 24px;
	margin-bottom: 16px;
	font-family: Roboto, Helvetica, Arial, sans-serif
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small {
	font-family: Roboto, Helvetica, Arial, sans-serif;
	font-weight: 400;
	line-height: 1.35;
	letter-spacing: -.02em;
	opacity: .54;
	font-size: .6em
}
h1 {
	font-family: Roboto, Helvetica, Arial, sans-serif;
	font-size: 56px;
	letter-spacing: -.02em;
	margin-top: 24px
}
h2 {
	font-family: Roboto, Helvetica, Arial, sans-serif;
	font-size: 45px;
	line-height: 48px;
	margin-top: 24px
}
h3 {
	font-family: Roboto, Helvetica, Arial, sans-serif;
	font-size: 34px;
	line-height: 40px;
	margin-top: 24px
}
h4 {
	font-size: 24px;
	font-weight: 400;
	line-height: 32px;
	-moz-osx-font-smoothing: grayscale
}
h5 {
	font-size: 20px;
	font-weight: 500;
	letter-spacing: .02em
}
h6, p {
	font-weight: 400;
	line-height: 24px
}
h6 {
	font-size: 16px;
	letter-spacing: .04em
}
p {
	font-size: 14px;
	letter-spacing: 0;
	margin-bottom: 8px
}
a {
	color: #365899;
	font-weight: 400
}
address, ol, ul {
	font-weight: 400;
	letter-spacing: 0
}
address {
	font-size: 12px;
	font-style: normal
}
ol, ul {
	font-size: 14px;
	line-height: 24px;
	list-style:none; padding:0;
}
.box, body {
	display: flex
}
.button, body a, button {
	cursor: pointer;
	text-decoration: none
}
.anchor, .button, button, .btn {
	position: relative;
	vertical-align: middle
}
figure, ol, ul {
	margin: 0;
	padding: 0
}
body {
	background: #fafafa;
	font-family: Roboto, Helvetica, Arial, sans-serif;
	-moz-osx-font-smoothing: grayscale;
	-webkit-font-smoothing: antialiased;
	min-height: 100vh;
	flex-direction: column
}
article, main {
	flex: 1;
	width: 100%
}
div.preview>a, figcaption, h1, h2, h3, h4, h5, h6, p {
	padding: 0 16px
}
ol, ul {
	margin: 0 16px;
	padding: 16px
}
article {
	padding: 0;
	max-width: 2400px;
	margin: auto
}
article #title {
	text-align: center;
	margin: 0;
	padding-top: 24px;
	padding-bottom: 24px
}
code, pre {
	background: #ECEFF1
}
.doc, .doc pre, .doc pre>code {
	background-color: #fff
}
.box {
	clear: both;
	min-height: -webkit-min-content;
	min-height: -moz-min-content;
	min-height: min-content
}
.anchor-trigger:hover+.anchor, .doc amp-img {
	display: block
}
.column {
	flex: 1;
	max-width: 800px;
	margin: 0 auto;
	width: 0
}
.doc code {
	padding: 1px 5px
}
.doc a {
	text-decoration: none
}
.code, code {
	background-color: #ECEFF1
}
.doc pre {
	padding: 16px;
	margin: 0
}
.doc amp-img {
	margin: 0 auto
}
.anchor-trigger {
	float: left
}
.anchor-target {
	margin: 0;
	padding: 0
}
.anchor {
	left: -16px;
	top: 30px;
	display: none
}
.anchor:hover {
	display: block
}
.anchor-img {
	background-image: -webkit-image-set(url(/img/ic_link_black_1x_web_18dp.png) 1x, url(/img/ic_link_black_2x_web_18dp.png) 2x);
	width: 18px;
	height: 18px
}
.code {
	padding-left: 8px;
	padding-right: 8px;
	padding-bottom: 8px
}
pre {
	white-space: pre-wrap;
	margin: 0;
	width: auto;
	line-height: 20px;
	overflow-wrap: break-word
}
code {
	font-size: 12px;
	font-family: Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, sans-serif
}
.preview {
	background-color: #CFD8DC
}
.button, button, .btn {
	background: 0 0;
	border: none;
	border-radius: 2px;
	height: 36px;
	margin: 0;
	min-width: 64px;
	padding: 0 16px;
	display: inline-block;
	font-family: Roboto, Helvetica, Arial, sans-serif;
	font-size: 14px;
	font-weight: 500;
	text-transform: uppercase;
	letter-spacing: 0;
	will-change: box-shadow;
	transition: box-shadow .2s cubic-bezier(.4, 0, 1, 1), background-color .2s cubic-bezier(.4, 0, .2, 1), color .2s cubic-bezier(.4, 0, .2, 1);
	outline: 0;
	text-align: center;
	line-height: 36px
}
.amp-experiment, .amp-experiment-large, .amp-experiment-medium {
	background-position: 100% center
}
.button-primary {
	color: #fff;
	background-color: #eb407a;
	box-shadow: 0 2px 2px 0 rgba(0, 0, 0, .14), 0 3px 1px -2px rgba(0, 0, 0, .2), 0 1px 5px 0 rgba(0, 0, 0, .12)
}
.button-inactive {
	color: #666;
	background-color: #aaa;
	cursor: default
}
#experiment-container {
	display: flex;
	justify-content: space-between;
	flex-wrap: wrap
}
#canary-toggle, #experiment-toggle {
	padding: 0;
	margin: 0 auto;
	display: inline-block;
	width: 160px
}
.amp-experiment-list span:not(:last-of-type)::after {
	content: ", "
}
.amp-experiment {
	padding-right: 24px;
	background-repeat: no-repeat;
	background-image: -webkit-image-set(url(/img/ic_experiment_black_1x_web_18dp.png) 1x, url(/img/ic_experiment_black_2x_web_18dp.png) 2x)
}
.amp-experiment-medium {
	padding-right: 36px;
	background-repeat: no-repeat;
	background-image: -webkit-image-set(url(/img/ic_experiment_black_1x_web_24dp.png) 1x, url(/img/ic_experiment_black_2x_web_24dp.png) 2x)
}
.amp-experiment-large {
	padding-right: 44px;
	background-repeat: no-repeat;
	background-image: -webkit-image-set(url(/img/ic_experiment_black_1x_web_36dp.png) 1x, url(/img/ic_experiment_black_2x_web_36dp.png) 2x)
}
.gist, .show-preview {
	background-position: right 8px center;
	background-repeat: no-repeat
}
.gist {
	background-image: url(/img/gist.png)
}
.show-preview {
	background-image: -webkit-image-set(url(/img/ic_play_circle_filled_black_1x_web_24dp.png) 1x, url(/img/ic_play_circle_filled_black_2x_web_24dp.png) 2x)
}
header {
	background-color: #000;
	box-shadow: 0 2px 2px 0 rgba(0, 0, 0, .14), 0 3px 1px -2px rgba(0, 0, 0, .2), 0 1px 5px 0 rgba(0, 0, 0, .12);
	display: block;
	flex-direction: column;
	align-items: center;
	justify-content: space-between
}
header #logo {
	align-self: flex-start;
	margin: 0 auto;
	background-position: left center;
	background-repeat: no-repeat;
	height: 62px;
	width: 500px;
}
header #logo img {
	min-width: inherit;
	min-height: inherit;
	max-width: 100%;
	height: auto;
	width: auto;
	margin:auto 0;
	max-width:90%
}
header .button {
	margin-left: 16px;
	padding: 0;
	height: 40px;
	min-width: 40px;
	display: block;
	float: left
}
header .button>amp-img {
	margin: 8px;
	float: left
}
header .button>span {
	display: inline-block;
	font-weight: 300;
	line-height: 40px;
	color: #fff;
	text-transform: uppercase
}
header #etb2bheader {
	align-self: flex-start;
	padding: 8px;
	margin-left: 8px;
	margin-top: 4px
}
header #actions {
	max-width: 50%;
	padding: 0 16px;
	align-self: flex-start;
	margin-left: auto;
	margin-right: 12px
}
#drawermenu {
	width: 320px;
	background-color: #fff
}

#drawermenu amp-accordion>* {
	-webkit-tap-highlight-color: #e5e5e5
}
#drawermenu .close {
	float: right;
	top: 16px;
	margin-right: 16px;
	cursor: pointer
}
#drawermenu .close:hover {
	background-color: #ccc
}
#drawermenu .expanded, #drawermenu .item a:active {
	background-color: #eee
}
#drawermenu .item a {
	color: #333;
	display: block;
	font-weight: 400;
	letter-spacing: .1px;
	padding: 10px 24px 10px 32px;
	white-space: nowrap;
	font-size: 13px;
	-webkit-tap-highlight-color: #e5e5e5
}
.amp-mode-touch #drawermenu .item a {
	padding-top: 18px;
	padding-bottom: 17px
}
#drawermenu .item amp-img {
	margin-left: 4px;
	vertical-align: text-bottom
}
#drawermenu .item .selected {
	font-weight: 700;
	color: #eb407a
}
#drawermenu .category {
	padding-left: 16px
}
#drawermenu .category a {
	color: #333
}
#drawermenu h4 {
	font-weight: 700;
	font-size: 13px;
	padding: 0;
	border: 0;
	background-color: #fff;
	width: 100%;
	line-height: 40px;
	margin:0;
	
}
#drawermenu .topheader {
	margin-top: 16px;
	margin-left: 16px
}
#drawermenu .home {
	text-align: left;
	font-size: 24px;
	line-height: 36px;
	color: rgba(0, 0, 0, .87);
	padding-left: 0
}
#drawermenu section:not([expanded]) .show-less, #drawermenu section[expanded] .show-more {
	display: none
}
footer {
	background:#131a22;
	padding:10px 15px;
	font-size: 13px;
	color: #ccc;
	line-height: 28px;
	font-weight: 300;
	text-align:center
}
footer ul{margin:0; padding:0;}
footer ul li{display:inline-block; margin:0 5px; }
footer ul li a{color:#ccc;}
.fab {
	position: fixed;
	bottom: 5%;
	right: 5%;
	z-index: 1000;
	background: #eb407a;
	line-height: 24px;
	border-radius: 50%;
	height: 56px;
	margin: auto;
	min-width: 56px;
	width: 56px;
	box-shadow: 0 1px 1.5px 0 rgba(0, 0, 0, .12), 0 1px 1px 0 rgba(0, 0, 0, .24);
	outline: 0
}
.fab amp-img {
	vertical-align: middle;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-12px, -12px);
	line-height: 24px;
	width: 24px
}
.card {
	box-shadow: 0 2px 2px 0 rgba(0, 0, 0, .14), 0 3px 1px -2px rgba(0, 0, 0, .2), 0 1px 5px 0 rgba(0, 0, 0, .12);
	float: none;
	overflow: hidden;
	padding: 8px;
	margin: 16px;
	border-radius: 2px
}
.card h4 {
	margin-top: 0;
	padding: 0
}
.card p {
	margin-top: 8px;
	padding: 0
}
.info {
	background: #ccc
}
.important {
	background: #FFF9C4
}
.show-on-mobile {
	display: none
}
.hide-on-mobile {
	display: block
}
.hide {
	display: none
}

@media (max-width: 700px) {
.hljs {
	padding: 0 16px
}
article {
	padding: 0
}
}

@media (max-width: 1024px) {
header .button>span,  header .hide-on-mobile {
	display: none
}
.anchor-trigger,  .doc {
	float: none
}
body {
	background: #fff
}
article #title {
	margin-top: 56px;
	padding-bottom: 0
}
header {
	height: 56px;
	flex-direction: row;
	position: fixed;
	top: 0;
	width: 100%;
	z-index: 1000
}
header #actions {
	padding: 8px 16px;
	margin-right: 0
}
header #etb2bheader {
	padding: 8px;
	margin: 0
}
header #logo {
	width: 280px;
	margin: 0;
	padding: 0;
	align-self: center
}
.card {
	margin: 16px
}
.column {
	flex: none;
	width: auto;
	max-width: 1280px
}
.box {
	display: block;
	margin: 0
}
.doc {
	padding: 0;
	margin: 0
}
.code {
	padding: 8px;
	margin: 16px
}
.preview {
	padding: 0;
	margin: 16px
}
.anchor-trigger:hover+.anchor,  .hide-on-mobile {
	display: none
}
.show-on-mobile {
	display: block
}
}

/*---Adding quotes to blockquote -----*/
.post, .post-content p{padding:0 15px; font-size:14px; font-weight:300; line-height:1.4em;}
.post-content p{padding:0;}
.post b{font-weight:700;}
.post blockquote, .post p blockquote {
	margin: 20px 20px 40px 0;
	font-weight: 700;
	position: relative;
	padding-left: 18px;
	line-height: 30px;
}
.post blockquote {
	quotes: "\"" "\"" "\'" "\'";
}
.post blockquote:before {
	content: open-quote;
	vertical-align: -28px;
	position: absolute;
	top: 27px;
	left: 0;
}
.post blockquote:after {
	content: close-quote;
	vertical-align: -27px;
}
blockquote::after, blockquote::before {
	font-size: 45px;
	color: #ed1c24;
	line-height: 0;
	font-weight: normal;
}
.post .quoteembed .quoteby {
	position: absolute;
	bottom: -28px;
	right: 0;
	font-size: 18px;
	color: #ed1c24;
	font-family: sans-serif;
}
/*----- Grid ---*/
.row{margin-left:-15px;margin-right:-15px}.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12{position:relative;min-height:1px;padding-left:15px;padding-right:15px}.col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12{float:left; box-sizing:border-box; -moz-box-sizing:border-box;}.col-xs-12{width:100%}.col-xs-11{width:91.66666667%}.col-xs-10{width:83.33333333%}.col-xs-9{width:75%}.col-xs-8{width:66.66666667%}.col-xs-7{width:58.33333333%}.col-xs-6{width:50%}.col-xs-5{width:41.66666667%}.col-xs-4{width:33.33333333%}.col-xs-3{width:25%}.col-xs-2{width:16.66666667%}.col-xs-1{width:8.33333333%}.col-xs-pull-12{right:100%}.col-xs-pull-11{right:91.66666667%}.col-xs-pull-10{right:83.33333333%}.col-xs-pull-9{right:75%}.col-xs-pull-8{right:66.66666667%}.col-xs-pull-7{right:58.33333333%}.col-xs-pull-6{right:50%}.col-xs-pull-5{right:41.66666667%}.col-xs-pull-4{right:33.33333333%}.col-xs-pull-3{right:25%}.col-xs-pull-2{right:16.66666667%}.col-xs-pull-1{right:8.33333333%}.col-xs-pull-0{right:auto}.col-xs-push-12{left:100%}.col-xs-push-11{left:91.66666667%}.col-xs-push-10{left:83.33333333%}.col-xs-push-9{left:75%}.col-xs-push-8{left:66.66666667%}.col-xs-push-7{left:58.33333333%}.col-xs-push-6{left:50%}.col-xs-push-5{left:41.66666667%}.col-xs-push-4{left:33.33333333%}.col-xs-push-3{left:25%}.col-xs-push-2{left:16.66666667%}.col-xs-push-1{left:8.33333333%}.col-xs-push-0{left:auto}.col-xs-offset-12{margin-left:100%}.col-xs-offset-11{margin-left:91.66666667%}.col-xs-offset-10{margin-left:83.33333333%}.col-xs-offset-9{margin-left:75%}.col-xs-offset-8{margin-left:66.66666667%}.col-xs-offset-7{margin-left:58.33333333%}.col-xs-offset-6{margin-left:50%}.col-xs-offset-5{margin-left:41.66666667%}.col-xs-offset-4{margin-left:33.33333333%}.col-xs-offset-3{margin-left:25%}.col-xs-offset-2{margin-left:16.66666667%}.col-xs-offset-1{margin-left:8.33333333%}.col-xs-offset-0{margin-left:0;  }
/*----- Widget-heading -----*/
.eventsidebar{padding:0 15px;}
h1, h2, h3, h4, h5, h6{margin-top:0;}
.widget-heading{padding:12px 15px 11px; background:#e7e7e7; border-bottom:3px solid #ffd200; line-height:18px;}
.hstyl_1{font-size:22px; position:relative; padding-bottom:13px; line-height:1em; }
.hstyl_1:after{position:absolute; display:block; height:3px; width:80px; bottom:0px; display:block; left:0; background:#ffd200;content:" ";}
.widget-heading .hstyl_1{padding-bottom:0;}
.widget-heading .hstyl_1:after{display:none;}
.hstyl_1.sm{font-size:15px; margin:0; padding:0;}
.hstyl_1.bx{ padding:0 0 8px;box-shadow:0 1px 0px rgba(0,0,0,.2);  font-size:17px;  }
.bx-styl{box-shadow:0 2px 1px rgba(0,0,0,.1); padding:20px; margin-bottom:20px; background:#fff;}
table { border-collapse: collapse; border-spacing: 0;}
<?php /*.table-bordered th,.table-bordered td{border:1px solid #ddd !important; padding:7px;} */?>
.table-bordered th,.table-bordered td{border:1px solid #ddd; padding:7px;}
.event-banner{margin:55px 0 15px;}
.event-banner img{margin-bottom:10px;}
.event-info{padding:0 15px; line-height:24px; color:#555; font-size:14px; font-weight:300;}
.event-title, h1.event-title{font-size:20px; display:block; padding:0 16px; font-weight:bold; padding-top:10px; margin-bottom:0;}
.btn-primary {box-sizing: content-box;-webkit-box-sizing: content-box;height: 40px;line-height: 40px;box-sizing: border-box;border: 0;
font-weight: 500;border-radius: 0;background: #ffd200;border: 0; color: #444;}

.row:before,.row:after{content:" ";display:table; clear:both;}

/*----- Event detail ---*/
/*.event-desc{max-height:200px; overflow:auto; padding-right:15px; }*/
.event-desc.post-desc{margin-bottom:0;}

.event-meta {padding:0;}
.event-meta ul{padding:0; list-style:none; margin:0;}
.event-meta ul li{list-style:none; padding:10px; border-bottom:1px solid #f2f2f2; font-size:14px; color:#555; line-height:18px; padding-left:40px; position:relative; font-weight:400;}
.event-meta ul li .text-right{font-weight:500; color:#333;}
.event-meta .eventtickets li{padding:10px;}
.rating{display:none}
.pull-right{float:right;}
.event-meta ul li > .fa{position:absolute; top:12px; left:15px; color:#4b4f56; font-size:14px; z-index:2; }
.event-meta ul li h6{ font-size:16px; color:#4b4f56; margin-bottom:4px; font-weight:normal;}

.event-meta ul li h6 span{font-size:14px; width:auto;}
.event-meta ul li:last-child{border:0;}
.event-meta ul li .date{float:right;}
.event-meta ul li span.pull-right{width:50%;}
.event-meta ul li span, .event-meta ul li strong, .event-meta .col-xs-6.text-right{font-size:12px;}
.event-meta ul li .row{margin-top:10px; margin-bottom:10px; }
.event-meta ul li .row:last-of-type{margin-bottom:0;}
.event-meta ul li.lst_dt .row:last-of-type{margin-top:0; margin-bottom:10px;}
.widget-heading .settings{ color:#888; font-size:13px; line-height:20px; float:right; position:relative; text-align:right; cursor:pointer; z-index:9;}
.widget-heading .settings ul{position:absolute; display:block; width:150px; background:#fff; right:-10px; top:25px; z-index:99; box-shadow:1px 1px 1px rgba(0,0,0,.1); display:none; padding:10px;}
.widget-heading .settings ul li{line-height: 24px;font-size: 12px; padding:0; border:0; }
.widget-heading .settings ul li a{color:#888;}
.widget-heading .settings ul li .fa{margin-left: 3px;margin-right: 0; position:static;}
.event-settings .socialbar_2{float:right;}
.ratingwrapper{display:inline-block;}
<?php /*.eventdetail .post-desc{padding-bottom:0; width:100% !important;}*/ ?>
.eventdetail .post-desc{padding-bottom:0; width:100%;}
.event-meta .socialbar_2 .fb{margin-left:0;}
.event-meta .rating{padding-top:10px;}
.hdng{padding: 0 15px;}
.hdng h1,.post h5{padding: 0;}
.event-info{margin-bottom:10px;}


</style>
