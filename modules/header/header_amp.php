<amp-sidebar id='drawermenu' layout="nodisplay">
    <amp-img class='close' src="https://st.etb2bimg.com/Themes/Release/images/responsive/amp-menu-close.png" width="32" height="32" alt="Close" on="tap:drawermenu.close" role="button" tabindex="0"></amp-img>
    <div class="topheader">
        <a href="<?=ROOT_PATH?>" class="home">PASSIONSTREET</a>
    </div>
    <hr />
    <section>
        <h4>
            <div class="show-more category "><a href="<?=ROOT_PATH?>"><i class="sprite-bg home"></i>Home</a></div>
        </h4>
    </section>
    <amp-accordion>
        <section>
            <h4>
                <div class="show-more category ">Passion<i class="arrow"></i></div>
                <div class="show-less category expanded ">Passion</div>
            </h4>
            <div>
                <?php
                foreach($PSParams['PSCategories'] as $passionkey=>$passionData)
                {
                    ?>
                    <div class="item ">
                        <a class='' href="<?=ROOT_PATH.$passionData['link']?>"><?=$passionData['name']?></a>
                    </div>
                    <?php
                }
                ?>
            </div>
        </section>
    </amp-accordion>
    <section>
        <h4>
            <div class="show-more category "><a href="<?=ROOT_PATH.'follow'?>">Follow</a></div>
        </h4>
    </section>
    <amp-accordion>
        <section>
            <h4>
                <div class="show-more category ">Events<i class="arrow"></i></div>
                <div class="show-less category expanded ">Events</div>
            </h4>
            <div>
                <div class="item ">
                    <a class='' href="<?=ROOT_PATH.'events'?>">View All Events</a>
                </div>
                <?php
                $temp = event_get_list();
                foreach($temp as $event)
                {
                    ?>
                    <div class="item ">
                        <a class='' href="<?=ROOT_PATH.'event/'.$event['seopath']?>"><?=$event['eventname']?></a>
                    </div>
                    <?php
                }
                ?>
                <div class="item ">
                    <a class='' href="<?=ROOT_PATH.'events/create'?>">Create Event</a>
                </div>
            </div>
        </section>
    </amp-accordion>
</amp-sidebar>
<header>
      <a id="etb2bheader" class="button" on="tap:drawermenu.toggle">
        <amp-img srcset="https://st.etb2bimg.com/Themes/Release/images/responsive/amp-menu.png" width="24" height="24" alt="navigation"></amp-img>
      </a>
      <a id="logo" href="<?=ROOT_PATH?>">
	  <amp-img src="<?=ROOT_PATH?>images/logo.png" alt="" height="50" layout="fixed-height"></amp-img>      
	  </a>
      <div id="actions">
        <a href="<?=ROOT_PATH?>" title="Passionstreet" class="button" target="_blank">
          <span class="hide-on-mobile">View Site</span>
        </a>
      </div>
</header>