<header class="navbar navbar-fixed-top">
    <div class="header">
        <div class="container">
            <div class="user-area">
                <?php 
                if($PSData['user']['profile_id'])
                {
			if(isset($_SESSION['fbpageadmin']) && $_SESSION['fbpageadmin'] == 1)
			{
				$param = array();
				$param['user'] = $_SESSION['user']['profile_id'];
				$param['setup'] = 1;
				?>
				<a class="notify" href="<?=ROOT_PATH.'open/fbtab?editfbtabsetting=1&app_data='.createsignedrequest($param)?>">
				    <i class="fa fa-cog"></i> 
				</a>
				<?php
			}
			?>
                        <a class="notify" href="<?=ROOT_PATH.'messages'?>">
                            <i class="fa fa-envelope-o"></i> 
                            <span class="count"><?=user_notification()?></span>
                        </a>
                        <span class="user-name">
                            <!--svg>
                            <image width="40" height="40" preserveAspectRatio="none" xlink:href="<?=$PSData['user']['userdp']?>"></image>
                            </svg-->
                            <img class="unveil" style="max-height:100%" src="<?=DEFAULT_IMG?>" data-src="<?=$PSData['user']['userdp']?>" class="nav-user-icon" /><strong><?=$PSData['user']['fname']?></strong> <i class="fa fa-angle-down"></i>
                            <div class="dropdown">
                                <ul>
                                <li><a href="#">Settings</a></li>
                                <li><a href="profile/">Profile</a></li>
                                <li><a href="logout">Log out</a></li>
                                </ul>
                            </div>
                        </span>
                    <?php
                }
                else
                {
                    ?>
                    <a href="register" class="signup-btn">SIGN UP</a>
                    <button class="btn btn-primary btn-sm login-btn">LOGIN</button>                    
                    <?php
                }
                ?>
                </div>
             	
            <div class="navbar-header">
                <?php
                $link = ROOT_PATH;
                if(!empty($PSData['user']['profile_id']))
                $link = ROOT_PATH.'home';
                ?>
                <span  class="nav-open"><i class="fa fa-bars"></i></span>
                <a href="<?=$link?>" class="navbar-brand"><img src="images/logo.png" alt="PASSIONSTREET" /></a>
            </div>
            
        </div>
    </div>
    <?php
    if($page_includes['navigation']==1)
    {
    ?>
    <div id="main-nav" class="overlay_nav">
    	<div class="mask"></div>
    	<?php if($PSData['user']['profile_id']){?>
		<div class="user_control">
    		<a href="<?=ROOT_PATH?>profile"><img class="unveil" src="<?=DEFAULT_IMG?>" data-src="<?=$PSData['user']['userdp']?>"  /></a>
    		<a href="<?=ROOT_PATH?>profile" class="name"><?=$PSData['user']['fullname']?></a>
    		<a href="<?=ROOT_PATH.'profile'?>" class="edit"><i class="fa fa-pencil"></i> Edit profile</a>	
    		<!--<a href="" class="invite"><i class="fa fa-user-plus" ></i> Invite friends</a>-->
		<a href="#" class="callmodaliframe invite" data-targetsrc="module/invite"><i class="fa fa-user-plus" ></i> Invite friends</a>
		</div>
	<?php } ?>
	<ul>
        	<?php if($PSData['user']['profile_id']) echo '<li><a href="'.ROOT_PATH.'home">HOME</a></li>';?>
            <li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i>PASSION ZONE</a>
            	<div class="subnav scroll-content">
                	<ul>
			<?php
				//print_array($PSParams['PSCategories']);
				foreach($PSParams['PSCategories'] as $passionkey=>$passionData)
				{
					echo '<li><a href="'.ROOT_PATH.$passionData['link'].'">'.$passionData['name'].'</a></li>';
                        	}
			?>
                    </ul>
                </div>
            </li>
            <li><a href="<?=ROOT_PATH.'follow'?>">FOLLOW </a></li>
            <li ><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i>EVENTS</a>
            	<div class="subnav wide">
                	<div class="head">
                    	<a href="<?=ROOT_PATH.'events'?>">View All Events <i class="fa fa-angle-right"></i> </a>
                    </div>
                    <div class="thumbnails scroll-content">
                    	<?php
			$fields = array();
			$fields['paging'] = array('page'=>1,'show'=>9);
			$temp = event_get_list(array('fields'=>$fields));
			foreach($temp as $event)
			{
				?>
				<figure>
					<a href="<?=ROOT_PATH.'event/'.$event['seopath']?>"><img class="unveil1" src1="<?=DEFAULT_IMG_HORIZONTAL?>" src="<?=$event['eventpic']?>" /></a>
					<figcaption><a href="<?=ROOT_PATH.'event/'.$event['seopath']?>"><?=$event['eventname']?></a></figcaption>
				</figure> 
				<?php
			}
			?>
			</div>
                    <div class="foot">
                    	<a href="<?=ROOT_PATH.'events/create'?>">CREATE EVENT</a>
                    </div>
                </div>
            </li>
            <li><a href="<?=ROOT_PATH.'communities'?>">COMMUNITY </a></li>
	    <?php /*<li><a href="#">GROUPS</a></li>
            <li><a href="#">AFFILIATE CIRCLE</a></li>
            <li><a href="#">CORPORATE CIRCLE</a></li>
            <li><a href="<?=ROOT_PATH?>page/manage">Manage Pages</a></li> */ ?>
	    
            
        </ul>
    </div>
    <?php
    }
    ?>

</header>
<div class="spacer">&nbsp;</div>