<form method="post" action="<?=$_SERVER['PHP_BROWSER_URL']?>">
    <div id="questions-accordian" class="form1">
        <?php 
        foreach($questions as $key=>$question)
        {
        $questiontypechecked = array();
        $questiontypechecked[$question['questiontype']] = ' checked';
        ?>
        <div class="questionsbox">
            <div class="questionsbox2">
                <div class="row">
                    <div class="col-sm-9  col-xs-9">
                        <input type="hidden" value="<?=$question['questionid']?>" required=""  name="question[<?=$key?>][questionid]" <?=$questiontypechecked['text']?> >
                        <textarea style="height:110px" placeholder="Define question" name="question[<?=$key?>][name]" required="" class="form-control sm"><?=$question['question']?></textarea>
                    </div>
		    <div class="col-sm-3  col-xs-3">
			    <label class="form-group  control-label"><strong>Answer type</strong></label>
			    <label>
				  <input type="radio" value="text" required="" class="anwsertype" name="question[<?=$key?>][type]" <?=$questiontypechecked['text']?> >
				  Text
				</label>
			   	<label>
				  <input type="radio" value="multipleoption" required="" class="anwsertype multiplechoicequestion" name="question[<?=$key?>][type]" <?=$questiontypechecked['multipleoption']?> >
				  Multiple Choice
				</label>
			   	<label>
				  <input type="radio" value="uploadimage" required=""  class="anwsertype" name="question[<?=$key?>][type]"  <?=$questiontypechecked['uploadimage']?>>
				  Image Upload
				</label>
		    </div>
		</div>
		<div class="row multiplechoicebox <?=($question['questiontype']=='multipleoption')?'':'hide'?>">
			<?php
			if(count($question['questionoptions'])>0)
			{
				foreach($question['questionoptions'] as $option)
				{
				    echo '<div class="col-sm-3  col-xs-3  form-group "><input type="text" name="question['.$key.'][options][]" value="'.$option.'" placeholder="Option"  class="form-control"></div>';
				}
			}
			else
			{
				?>
				<div class="col-sm-3  col-xs-3  form-group "><input type="text" name="question[<?=$key?>][options][]" placeholder="Option"  class="form-control"></div>
				<div class="col-sm-3  col-xs-3  form-group "><input type="text" name="question[<?=$key?>][options][]" placeholder="Option"  class="form-control"></div>
				<div class="col-sm-3  col-xs-3  form-group "><input type="text" name="question[<?=$key?>][options][]" placeholder="Option"  class="form-control"></div>
				<?php
			}
			?>
			<div class="col-sm-3 col-xs-3 form-group text-right "><span index="<?=$key?>" class="add-more">Add more Options</span></div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <button value="addMore" name="step2" type="submit" class="btn btn-default add-more-question">Add More Question</button>
            <button value="submit" name="submit" type="submit" class="btn btn-default">Save Questions</button>
        </div>
    </div>
</form>
<script>
$("document").ready(function(){
    
	$("body").on('click','.add-more',function(){
		$index = $(this).attr('index');
		$(this).parents('.multiplechoicebox').find('div:last').before('<div class="col-sm-3  col-xs-3  form-group "><input type="text" name="question['+$index+'][options][]" placeholder="Option"  class="form-control"></div>')
	});
	
	$("body").on('click focus','.multiplechoicequestion',function(event){
	    $(this).parents(".questionsbox").find(".multiplechoicebox").removeClass("hide");
	});
	
	$("body").on('click focus','.anwsertype',function(event){
	    $(this).parents(".questionsbox").find(".multiplechoicebox").addClass("hide");
	});
	
	$("body").on('click','.add-more-question',function(event){
	    event.preventDefault();
	    var $parentId = $(".questionsbox").length;
	    $("#questions-accordian").append('<div class="questionsbox">'+ addQuestion($parentId)+'</div>');
	});

});
function addQuestion($parentId){
    return '<div class="questionsbox2">                <div class="row">                    <div class="col-sm-9  col-xs-9">                        <input type="hidden" value="" required="" name="question['+$parentId+'][questionid]" checked="">                        <textarea style="height:110px" placeholder="Define question" name="question['+$parentId+'][name]" required="" class="form-control sm"></textarea>                    </div>		    <div class="col-sm-3  col-xs-3">			    <label class="form-group  control-label"><strong>Answer type</strong></label>			    <label>				  <input type="radio" value="text" required="" name="question['+$parentId+'][type]" checked="">				  Text				</label>			   	<label>				  <input type="radio" value="multipleoption" required="" class="multiplechoicequestion" name="question['+$parentId+'][type]">				  Multiple Choice				</label>			   	<label>				  <input type="radio" value="uploadimage" required="" name="question['+$parentId+'][type]">				  Image Upload				</label>		    </div>		</div>		<div class="row multiplechoicebox hide">							<div class="col-sm-3  col-xs-3  form-group "><input type="text" name="question['+$parentId+'][options][]" placeholder="Option" class="form-control"></div>				<div class="col-sm-3  col-xs-3  form-group "><input type="text" name="question['+$parentId+'][options][]" placeholder="Option" class="form-control"></div>				<div class="col-sm-3  col-xs-3  form-group "><input type="text" name="question['+$parentId+'][options][]" placeholder="Option" class="form-control"></div>							<div class="col-sm-3 col-xs-3 form-group text-right "><span index="'+$parentId+'" class="add-more">Add more Options</span></div>                </div>            </div>';
}
</script>