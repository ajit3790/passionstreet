<form method="post" action="<?=$_SERVER['PHP_BROWSER_URL']?>">
    <div id="questions-accordian" class="form1">
        <?php 
        foreach($questions as $key=>$question)
        {
        $questiontypechecked = array();
        $questiontypechecked[$question['questiontype']] = ' checked';
        ?>
        <div class="questionsbox">
            <div class="questionsbox2">
                <div class="row">
                    <div class="col-sm-12 ">
                        <input type="hidden" value="<?=$question['questionid']?>" required=""  name="question[<?=$key?>][questionid]" <?=$questiontypechecked['text']?> >
                        <textarea placeholder="Define question" name="question[<?=$key?>][name]" required="" class="form-control sm"><?=$question['question']?></textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-sm-3 col-xs-12 form-group  control-label"><strong>Answer type</strong></label>
                    <div class="radio form-group col-sm-3 col-xs-4">
                        <label>
                          <input type="radio" value="text" required=""  name="question[<?=$key?>][type]" <?=$questiontypechecked['text']?> >
                          Text
                        </label>
                     </div>
                     <div class="radio form-group col-sm-3 col-xs-4">
                        <label>
                          <input type="radio" value="multipleoption" required="" class="multiplechoicequestion" name="question[<?=$key?>][type]" <?=$questiontypechecked['multipleoption']?> >
                          Multiple Choice
                        </label>
                     </div>
                    <div class="radio form-group col-sm-3 col-xs-4">
                        <label>
                          <input type="radio" value="uploadimage" required=""  name="question[<?=$key?>][type]"  <?=$questiontypechecked['uploadimage']?>>
                          Image Upload
                        </label>
                     </div>
                </div>
            </div>
            <div class="multiplechoicebox <?=($question['questiontype']=='multipleoption')?'':'hide'?>">
                <div class="row">
                    <?php
                    if(count($question['questionoptions'])>0)
                    {
                        foreach($question['questionoptions'] as $option)
                        {
                            echo '<div class="col-sm-12  form-group "><input type="text" name="question['.$key.'][options][]" value="'.$option.'" placeholder="Option"  class="form-control"></div>';
                        }
                    }
                    else
                    {
                    ?>
                    <div class="col-sm-12  form-group "><input type="text" name="question[<?=$key?>][options][]" placeholder="Option"  class="form-control"></div>
                    <div class="col-sm-12  form-group "><input type="text" name="question[<?=$key?>][options][]" placeholder="Option"  class="form-control"></div>
                    <div class="col-sm-12  form-group "><input type="text" name="question[<?=$key?>][options][]" placeholder="Option"  class="form-control"></div>
                    <?php
                    }
                    ?>
                </div>
                <div class="row ">
                    <div class="col-sm-12 form-group text-right ">
                        <span class="add-more">Add more Options</span>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <button value="addMore" name="step2" type="submit" class="btn btn-default add-more-question">Add More Question</button>
            <button value="submit" name="submit" type="submit" class="btn btn-default">Save</button>
        </div>
    </div>
</form>
<script>
$("document").ready(function(){
    //$("#questions-accordian").accordion();
});
$("body").on('click','.multiplechoicequestion',function(event){
    $(this).parents(".questionsbox").find(".multiplechoicebox").toggleClass("hide");
});
$("body").on('click','.add-more-question',function(event){
    event.preventDefault();
    var $parentId = $(".questionsbox").length;
    $("#questions-accordian").append('<div class="questionsbox">'+ addQuestion($parentId) + addOptions($parentId) +'</div>');
    
});
function addQuestion($parentId){
    return '<div class="questionsbox2">            <div class="form-group row">                <div class="col-sm-12 ">                    <textarea class="form-control sm" required="" name="question['+$parentId+'][name]" placeholder="Define question"></textarea>                </div>            </div>            <div class=" row">                <label class="col-sm-3 col-xs-12 form-group  control-label">Answer type</label>                <div class="radio form-group col-sm-3 col-xs-4">                    <label>                      <input type="radio" name="question['+$parentId+'][type]" required="" value="text">                      Text                    </label>                 </div>                 <div class="radio form-group col-sm-3 col-xs-4">                    <label>                      <input type="radio" name="question['+$parentId+'][type]" class="multiplechoicequestion" required="" value="multipleoption">                      Multiple Choice                    </label>                 </div>                <div class="radio form-group col-sm-3 col-xs-4">                    <label>                      <input type="radio" name="question['+$parentId+'][type]" required="" value="uploadimage">                      Image Upload                    </label>                 </div>            </div>        </div>';
}
function addOptions($parentId){
    return '<div class="multiplechoicebox hide"><div class="row"><div class="col-sm-12  form-group "><input type="text" name="question['+$parentId+'][options][]"class="form-control" placeholder="Option"></div><div class="col-sm-12  form-group "><input type="text" name="question['+$parentId+'][options][]"class="form-control" placeholder="Option"></div><div class="col-sm-12  form-group "><input type="text" name="question['+$parentId+'][options][]"class="form-control" placeholder="Option"></div></div><div class="row "><div class="col-sm-12 form-group text-right "><span class="add-more">Add more Options</span></div></div></div>';
}
$("body").on('click','.add-more',function(){
    $temp = $(this).parents('.multiplechoicebox').find('.row').eq(0);
    $temp2 = $(this).parents('.multiplechoicebox').find('input').eq(0).clone().attr('value','');
    $temp.append($temp2.wrap("<div class='col-sm-12  form-group '></div>").parent());
});
</script>