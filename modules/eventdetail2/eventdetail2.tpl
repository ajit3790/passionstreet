<style>
.filehidden {
    cursor: pointer;
    opacity: 0;
    padding: 2px;
    position: absolute;
    width: 100%;
    z-index: 2;
    filter: alpha(opacity=0);
}
.cframe .modal-dialog{
    width:auto;
    margin:3% 18%;
    background:rgb(253, 253, 253) none repeat scroll 0 0;
} 
.cframe .modal-dialog{
    width:auto;
    margin:3% 18%;
    background:rgb(253, 253, 253) none repeat scroll 0 0;
}
</style>
<div class="col-md-8 main-content mb-large">
    <div class="inner">

        <div class="post bx-styl">
            <h3>Participation Terms &amp; Conditions</h3>
            <div class="post-desc event-desc clearfix">                           
               <?=$event['terms']?>     
            </div>                        
        </div>
        <!--/ post -->

       <!-- Followers -->
        <div class="row">
            <div class="col-md-12">
                <h2 class="hstyl_1 sm">Comments(<span class="comment_count_box">0</span>)</h2>
            </div>
        </div>
        <div class="share_cmt_box one-clm commentbox bx-styl" >
             <div class="share_cmt_box_inner">                                
                <div class="all_comments ps-container">
                    <div class="all_comments_container">
                    </div>
                </div>
                <div class="text-right view-all"><a href="#">+View all</a></div>

                <div>
                    <h5>Post a comment</h5>
                    <textarea class="comment_tarea" post_id="<?=$event['event_id']?>" comment_on="event" placeholder="Leave your reply here..." cols="" rows=""></textarea>
                    <input type="button" value="Post" class="btn btn-primary submit-comment" style="margin:0">
                </div>
                <div class="clearfix"></div>
            </div>
       </div>

    </div>      
</div>
<div class="col-md-4 sidebar">                
    <div class="widget bx-styl">
        <?php
        if($event['routeimage'])
        {
            ?>
            <h2 class="hstyl_1 sm">Route Map</h2>  
            <div ><a href="#" class="callmodaliframe" targetsrc="module/maps/routemap?coordinates=<?=urlencode(serialize($event['routemap']))?>"><img src="<?=$event['routeimage']?>" style="width:100%"></a></div>
            <?php
        }
        else if($event['mapimage'])
        {
            ?>
            <h2 class="hstyl_1 sm">Location Map</h2>  
            <div ><a href="#" class="callmodaliframe" targetsrc="module/maps/map?coordinate=<?=urlencode($event['maploc'])?>"><img src="<?=$event['mapimage']?>" style="width:100%"></a></div>
            <?php
        }
        ?>
    </div>
    <div class="widget event-gallery bx-styl">
            <?php if(!empty($event['isAdmin'])){?>
            <div class="event-settings">
                <span href="#">
                <input type="file" name="eventpic[]" content_id="<?=$event['event_id']?>" content_type="event" class="filehidden ajaxImageUpload" multiple callback="" pictype="eventPics">
                Upload Pics <i class="fa fa-gear"></i>
                </span>
            </div>
            <?php } ?>

         <h2 class="hstyl_1 sm">Events Photo Gallery</h2> 
         <ul class="clearfix">
         </ul>
    </div>
</div>
<script>
$(document).ready(function(){
    $(".commentbox").on("click",".submit-comment",function(){
        $temp = $(this);
        $tarea = $temp.parents(".commentbox").find('.comment_tarea');
        $id = $tarea.attr("post_id");
        $comment_on = $tarea.attr("comment_on");
        $comment = $tarea.val();
        $params = {id:$id,comment_on:$comment_on,comment:$comment};
        $.post(root_path+'PSAjax.php?type=addcomment', $params, function(data){ 
            $comment = $.parseJSON(data);
            $commentshtml = '';
            var v = $comment['list']['comment'];
            $commentshtml += '<div class="notif_row nobg_border"> ' + 
                                '<div class="usr_dp"><img src="'+v['userdp']+'"></div> ' +
                                '<div class="user_comment"><span class="gen_usr_nm">'+v['name']+'</span> '+v['comment']+' </div>' +
                            '</div>';
            $(".commentbox").find(".all_comments_container").append($commentshtml);
        });
    });
    $params = {id:'<?=$event['event_id']?>',comment_on:'event'};
    $.get(root_path+'PSAjax.php?type=getcomments',$params,function(data){
        $comments = $.parseJSON(data);
        $commentshtml = '';
        $.each($comments['list']['comments'],function(i,v){
            $commentshtml += '<div class="notif_row nobg_border"> ' + 
                    '<div class="usr_dp"><img src="'+v['userdp']+'"></div> ' +
                    '<div class="user_comment"><span class="gen_usr_nm">'+v['name']+'</span> '+v['comment']+' </div>' +
                '</div>';
        });
        $(".commentbox").find(".all_comments_container").append($commentshtml);
        $(".comment_count_box").html($comments['list']['comments'].length);
    });
    
    $params = {id:'<?=$event['event_id']?>',added_to:'event'};
    $.get(root_path+'PSAjax.php?type=getphotoes',$params,function(data){
        $photoes = $.parseJSON(data);
        $photoeshtml = '';
        $.each($photoes['list']['photoes'],function(i,v){
            $photoeshtml += "<li><div class='img-frame'><img class='jbox-img' src='"+v['imgurl']+"'></div></li>";
        });
        $(".event-gallery").find('ul').html($photoeshtml);
        gallery = $(".jbox-img"),
        lastImg = gallery[gallery.length - 1].getAttribute("src"),
        firstImg = gallery[0].getAttribute("src");
        $(".jbox-img").click(function() {
            var n = $(this).attr("src");
            openJBox(n, "first")
        })
    });
    
});
</script>