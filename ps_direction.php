<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple markers</title>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
    <script src='js/jquery-1.11.3.min.js' type='text/javascript'></script>
  </head>
  <body>
    <div id="map"></div>
    <script>
      //21.4225239,39.8239876
      function initMap() {
        var myLatLng = {lat: 21.4225239, lng: 39.8239876};
        //var myLatLng = {lat: 0, lng: 0};
        var otherposition = [{lat: -25.363, lng: 131.044},{lat: 30.363, lng: 39.8239876},{lat: 0, lng: 0}] 
        //var otherposition = [{lat:30, lng: 0},{lat:0, lng: 30},{lat:20, lng: 20},{lat: 10, lng: 5},{lat: 5, lng:10},{lat: -20, lng:20},{lat: -20, lng:-20},{lat: 20, lng:-20}]; 
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 3,
          center: myLatLng
        });
        
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
        
        $.each(otherposition,function(i,v){
            angle = Math.round(360 / (Math.PI  / ((Math.atan((2*(parseFloat(otherposition[i]['lat']) - parseFloat(myLatLng['lat'])))/(2*(parseFloat(otherposition[i]['lng']) - parseFloat(myLatLng['lng'])))))/2))*100)/100;
            new google.maps.Marker({
                position: otherposition[i],
                map: map,
                title: 'Hello World!',
                icon:'http://passionstreet.in/imagetotext.php?text='+angle
            });
            flightPath = new google.maps.Polyline({
                path: [otherposition[i],myLatLng],
                strokeColor: '#FF0000',
                map: map,
                strokeOpacity: 1.0,
                strokeWeight: 1
            });
            
            /*flightPath = new google.maps.Polyline({
                path: [{'lat':(otherposition[i]['lat']),'lng':(otherposition[i]['lng']+40)},{'lat':(otherposition[i]['lat']),'lng':(otherposition[i]['lng']-40)}],
                strokeColor: '#FF0000',
                map: map,
                strokeOpacity: 1.0,
                strokeWeight: 1
            });*/
        });
        
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsbAJL6QlioClOYVf8Gjfa8TrR3wmJ8aY&callback=initMap">
    </script>
  </body>
</html>