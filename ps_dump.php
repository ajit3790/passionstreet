<?php
require_once('initialise.php');

$list = $connection->fetchAll("SELECT p.id as profileId, ps.* FROM posts as ps JOIN profile AS p ON p.profile_id = ps.creator");

$count = 0;
foreach ($list as $key => $data) {
	$postId 	= $data['post_id'];//mt_rand(10000000, 99999999);
	$moduleId 	=  isset($data['postedon']) ? $data['postedon'] : '';
	$moduleName =  isset($data['postedontype']) === true && $data['postedontype']!= '' ? $data['postedontype'] : 'home';

	$videoUrl = '';
	$videoMeta = '';
	if ($data['title'] != '') {
		$videoData = getUrlData($data);
		if ($videoData === false) {
			//
		} else {
			$videoUrl  = $videoData['url'];
			$videoUrl  = ($videoUrl == null || $videoUrl == 'null') ? '' :$videoUrl;
			$videoMeta = json_encode($videoData);	
		}
	}

	$params = array(
		$postId,
		$moduleId,
		$moduleName,
		$data['category'],
		$data['images'],
		$data['content'],
		$data['log'],
		$videoUrl,
		$videoMeta,
		'a:0:{}',
		$data['extra'],
		$data['profileId'],
		date('Y-m-d H:i:s', strtotime($data['create_date']))
	);
	
	$connection->executeUpdate("INSERT INTO ps_posts(id, moduleId, module, category, images, about, workout, video, videoMeta, tagged_members, extra, createdBy, createdAt) VALUES(?, ?, ? ,? ,?, ?, ?, ?, ?, ?, ?, ?, ?)", $params);
	
	$likes = $connection->fetchAll("SELECT p.id as profileId, ls.* FROM likes as ls JOIN profile AS p ON p.profile_id = ls.user WHERE post_id = ?", array($data['post_id']));
	if (empty($likes) === false) {
		foreach ($likes as $key => $likeData) {
			$connection->executeUpdate("INSERT INTO group_likes(postId, userId) VALUES(?, ?)", array($postId, $likeData['profileId']));
		}
	}

	$comments = $connection->fetchAll("SELECT p.id as profileId, cs.* FROM comments as cs JOIN profile AS p ON p.profile_id = cs.commentor WHERE commentedon = ?", array($data['post_id']));
	if (empty($comments) === false) {
		foreach ($comments as $key => $commentData) {
			$createdAt = date('Y-m-d', strtotime($commentData['date']));
			$connection->executeUpdate("INSERT INTO group_comments(postId, userId, comment, createdAt) VALUES(?, ?, ?, ?)", array($postId, $commentData['profileId'], $commentData['comment'], $createdAt));
		}
	}
	$count++;
}

echo $count."--- executed";die;

function getUrlData($post = null) {
	global $PSParams;
	usePlugin('DomXPath',array('html'=>$post['title']));
	$xpath = $PSParams['plugins']['DomXPath'];

	$titleNodes = $xpath->query("//h3");
	
	$response = array();
	if ($titleNodes->item(0) != '') {
		$response['title'] = strip_tags(trim($titleNodes->item(0)->nodeValue));

		$nodes = $xpath->query("//img[@data-src]");		
		if ($nodes->item(0)) {
			$img = $nodes->item(0)->getAttribute('data-src');
			if ($img != '') {
				$img = 'https://passionstreet.in/ps_proxy.php?type=image&psurl='.$img;
				$nodes = $xpath->query("//a[contains(@class, 'hosturl')]");
				if($nodes->item(0)) {
					$response['url'] = $nodes->item(0)->getAttribute('href');
					$response['host'] = $nodes->item(0)->nodeValue;
				}
			}
			$response['pageimage']   = $img;
		} else {
			$response['pageimage']   = "";
		}

		if ($response['pageimage']== '') {
			$nodes = $xpath->query("//div[contains(@class, 'flexi-video')]");
			if($nodes->item(0)) {
				$response['pageimage'] = '';
				if ($nodes->item(0)->getAttribute('data-src')) {
					$response['pageimage'] = 'https://passionstreet.in/ps_proxy.php?type=image&psurl='.$nodes->item(0)->getAttribute('data-src');
				}
			}
		}

		$nodes = $xpath->query("//div[contains(@class, 'flexi-video')]");
		if($nodes->item(0)) {
			$embed = $nodes->item(0)->getAttribute('vsrc');
			$ytvideoid = substr(strstr($embed, 'embed/'), 6, 11);
			$response['videoembed'] = $embed;
			$response['ytvideoid']  = $ytvideoid;
			$response['host']  		= 'www.youtube.com';
		}

		$nodes = $xpath->query("//p");
		if($nodes->item(0)) {
			$desc = $nodes->item(0)->nodeValue;
			$response['description'] = $desc;
		}

		$nodes = $xpath->query("//a[contains(@class, 'hosturl')]");
		if($nodes->item(0)) {
			$url = $nodes->item(0)->getAttribute('href');
			$response['url'] = $url;
		}
		return $response;
	}
	return false;
}
	
