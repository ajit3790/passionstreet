<?php
require_once('initialise.php');
$header = getallheaders();
if ($header['X-Requested-With'] != 'XMLHttpRequest') {
    $response['status'] = '403';
    //preventing ajax file call from script
} else if (empty($_GET['type'])) {
    $response['status'] = '406';
} else {
    $actionsNotRequiringLogin          = array(
        "forgot_pswd",
        "login_process",
        "loginsns",
        "getphotoes",
        "eventbooking",
	"eventbookingguestcheckout",
	"startdonation",
        "validateeventbooking",
        "minimal_registration_process",
        "logactivity",
        "applycoupon",
        "eventpagedata",
        "getcomments",
        "eventticketdemoeffectiveprice",
        "eventticketeffectiveprice",
	"claimeventfromemail",
	"claimevent"
    );
    $actionsRequiringMinimalLogin      = array(
        "eventbooking",
	"startdonation",
	"claimeventfromemail"
    );
    $currentActionRequiresLogin        = !in_array($_GET['type'], $actionsNotRequiringLogin);
    $currentActionRequiresMinimalLogin = in_array($_GET['type'], $actionsRequiringMinimalLogin);
    //print_array(array('JMA',$currentActionRequiresLogin && ($_REQUEST['login_status'] == 'false' || empty($_REQUEST['login_status']) || ($_REQUEST['login_status'] == 'true' && !loggedId()))));
    //exit();
	if ($currentActionRequiresMinimalLogin && empty($_SESSION['user']['profile_id'])) {
        $response['status'] = '4051';
    } else if ($currentActionRequiresLogin && ($_REQUEST['login_status'] != 'true' || ($_REQUEST['login_status'] == 'true' && !loggedId()))) {
        $response['status'] = '405';
    } else if (!authorized()) {
        $response['status'] = '403';
    } else {
        $type               = $_GET['type'];
        $profile_id         = $_SESSION['user']['profile_id'];
        $response['status'] = '200';
        ob_start();
        unset($_POST['login_status']);
        unset($_POST['token']);
        unset($_GET['login_status']);
        unset($_GET['token']);
        switch ($type) {
            case 'passion-join':
                $x = relation_register($profile_id, 'joinedPassion', $_POST['passion']);
                if ($x) {
                    $response['status'] = '200';
                    echo "Joined Now";
                    $_SESSION['user']['passions'][] = $_POST['passion'];
                } else {
                    $response['status'] = '304';
                    echo "Already Joined";
                }
                break;
            case 'loginsns':
                $network = $_POST['network'];
                if ($network == 'facebook')
                    $snstype = 'fb';
                else if ($network == 'twitter')
                    $snstype = 'tw';
                else if ($network == 'google')
                    $snstype = 'gg';
                $x          = $_POST['data'];
                $insertData = array();
                if (!empty($x['email']))
                    $x['email'] = get_standard_email($x['email']);
                if ($snstype == 'fb') {
                    $insertData['email']  = $x['email'];
                    $insertData['fname']  = $x['first_name'];
                    $insertData['lname']  = $x['last_name'];
                    $insertData['gender'] = $x['gender'];
                }
                if ($snstype == 'tw') {
                    //$insertData['email'] = $x['email'];
                    $temp                  = explode(" ", $x['name']);
                    $insertData['email']   = $x['screen_name'] . "@twitter.com";
                    $insertData['fname']   = $temp[0];
                    $insertData['lname']   = $temp[1] ? $temp[1] : '';
                    $insertData['country'] = $x['location'];
                    $insertData['bio']     = $x['description'];
                }
                if ($snstype == 'gg') {
                    $temp                 = explode(" ", $x['name']);
                    $insertData['email']  = $x['email'];
                    $insertData['fname']  = $temp[0];
                    $insertData['lname']  = $temp[1] ? $temp[1] : '';
                    $insertData['gender'] = $x['gender'];
                }
                $insertData[$snstype . 'id'] = $x['id'];
                $insertData['extra']         = serialize(array(
                    $snstype => $x
                ));
                $user_profile_db             = user_profile(array(
                    'email' => $insertData['email']
                ));
                if (empty($user_profile_db)) {
                    $insertData['profile_id'] = generate_id("profile");
                    if ($snstype == 'tw' && !empty($x['profile_image_url']))
                        $insertData['userdp'] = upload_from_url('userdp', $x['profile_image_url'], $crop);
                    if ($snstype == 'gg' && !empty($x['picture']))
                        $insertData['userdp'] = upload_from_url('userdp', $x['picture'], $crop);
                    if ($snstype == 'fb' && !empty($x['picture']->data->url))
                        $insertData['userdp'] = upload_from_url('userdp', $x['picture']->data->url, $crop);
                    if ($snstype == 'fb' && !empty($x['cover']->source))
                        $insertData['userbg'] = upload_from_url('userbg', $x['cover']->source);
                    $profile_id = user_create_profile_sns($insertData);
                    @session_start();
                    $PSData['user']                 = user_profile(array(
                        "profile_id" => $insertData['profile_id']
                    ));
                    $_SESSION['sns']                = $network;
                    $_SESSION['snsid']                = $x['id'];
                    $_SESSION['user']['profile_id'] = $PSData['user']['profile_id'];
                    $PSJavascript['profile_id']     = $PSData['user']['profile_id'];
                    $PSJavascript['login_status']   = true;
                    $PSJavascript['close_window']   = 1;
                    $module_display_style           = "done";
                    mailer(array(
                        "profile_id" => $_SESSION['user']['profile_id']
                    ), 'welcome');
                    $response['login']      = 1;
                    $response['profile_id'] = $PSData['user']['profile_id'];
                    $response['sns'] = $snstype;
                    $response['snsid'] = $x['id'];
                } else {
                    @session_start();
                    $PSData['user']                 = $temp = user_profile(array(
                        "email" => $insertData['email']
                    ));
                    $_SESSION['sns']                = $network;
                    $_SESSION['snsid']              = $x['id'];
                    $_SESSION['user']['profile_id'] = $PSData['user']['profile_id'];
                    $PSJavascript['profile_id']     = $PSData['user']['profile_id'];
                    $PSJavascript['login_status']   = true;
                    $PSJavascript['close_window']   = 1;
                    $module_display_style           = "done";
                    foreach ($insertData as $key => $value) {
                        if (array_key_exists($key, $temp) && empty($temp[$key])) {
                            $PSUpdateArray[$key] = $value;
                        }
                    }
                    if (!empty($PSUpdateArray))
                        user_profile_update($PSUpdateArray);
                    //print_array();
                    $response['login']      = 1;
                    $response['profile_id'] = $PSData['user']['profile_id'];
		    $response['sns'] = $snstype;
                    $response['snsid'] = $x['id'];
                }
                break;
            case 'user-follow':
                $x = relation_register($profile_id, 'followsUser', $_POST['profile_id']);
                if ($x) {
                    $response['status'] = '200';
                    echo "Following Now";
                } else {
                    $response['status'] = '304';
                    echo "Already Following";
                }
                break;
            case 'categorysublist-posttype':
                $response['list']['subcategory'] = $PSParams['PSSubCategoriesByCategory'][$_GET['category']];
                $temp1                           = array(
                    "random" => "Random"
                );
                $temp2                           = $PSParams['PSCategories'][$_GET['category']]['posttype'];
                if (empty($temp2))
                    $temp2 = array();
                $temp1                        = array_merge($temp1, $temp2);
                $response['list']['posttype'] = $temp1;
                break;
            case 'imageuploadajax':
                $img        = $_POST['imgBase64'];
                $targetPath = $_POST['pictype'];
                $imgpath    = upload_from_uri($targetPath, $img);
                user_profile_update(array(
                    $targetPath => $imgpath
                ));
                echo $imgpath;
                break;
            case 'imageuploadajax2':
                $files = reArrayFiles($_FILES['file']);
                foreach ($files as $file) {
                    $temp['photo_id']    = generate_id("photo");
                    $temp['addedtotype'] = $_POST['content_type'];
                    $temp['addedto']     = $_POST['content_id'];
                    if (empty($_POST['content_type'])) {
                        $temp['path']        = upload('temp', 'image', $file);
                        $temp['addedtotype'] = 'temp';
                        $temp['addedto']     = 'temp';
                    } else if ($_POST['content_type'] == 'event')
                        $temp['path'] = upload('eventGallery', 'image', $file);
                    else
                        $temp['path'] = upload($_POST['content_type'], 'image', $file);
                    
                    $temp['addedby'] = $_SESSION['user']['profile_id'];
                    $temp['date']    = date("Y-m-d H:i:s");
                    if (photoes_add($temp)) {
                        $imgurl                        = get_upload_path($temp['path']);
                        $response['list']['photoes'][] = $imgurl;
                        echo "<li><div class='img-frame'><a href='#'><img src='$imgurl'></a></div></li>";
                    }
                    $temp = array();
                }
                break;
            case 'imageuploadajax3':
                $files = reArrayFiles($_FILES['file']);
                foreach ($files as $file) {
                    $temp['photo_id'] = generate_id("photo");
                    $temp['path']     = upload('eventGallery', 'image', $file);
                    $temp['addedby']  = $_SESSION['user']['profile_id'];
                    ;
                    $temp['addedtotype'] = $_POST['content_type'];
                    $temp['addedto']     = $_POST['content_id'];
                    $temp['date']        = date("Y-m-d H:i:s");
                    if (photoes_add($temp)) {
                        $imgurl                        = get_upload_path($temp['path']);
                        $response['list']['photoes'][] = $imgurl;
                        echo "<li><div class='img-frame'><a href='#'><img src='$imgurl'></a></div></li>";
                    }
                    $temp = array();
                }
                break;
            case 'imageuploadajax4':
                $targetPath = $_POST['pictype'];
                if ($_POST['multicropdata'][$targetPath]) {
                    $imgpaths = upload_post_images_new2($_FILES, $targetPath);
                    $imgpath  = $imgpaths[0];
                    if ($imgpath) {
                        user_profile_update(array(
                            $targetPath => $imgpath
                        ));
                        echo $imgpath;
                    }
                }
                break;
            case 'getphotoes':
                $response['list']['photoes'] = photoes_get_list(array(
                    "addedtotype" => $_GET['added_to'],
                    "addedto" => $_GET['id']
                ));
                break;
            case 'updateprofile':
                if (user_profile_update($_POST))
                    echo "Saved";
                else {
                    echo "Failed";
                    $response['status'] = '406';
                }
                break;
            case 'updateUserheightwidth':
                if ($PSData['user']['extra'] && false) {
                    $temp                          = $PSData['user']['extra'];
                    $temp['userprofile']['height'] = $_POST['height'];
                    $temp['userprofile']['weight'] = $_POST['weight'];
                } else {
                    $temp                          = array();
                    $temp['userprofile']['height'] = $_POST['height'];
                    $temp['userprofile']['weight'] = $_POST['weight'];
                }
                if (user_profile_update(array(
                    'extra' => $temp
                )))
                    echo "Saved";
                else {
                    echo "Failed";
                    $response['status'] = '406';
                }
                break;
            case 'getBankAccountList':
                $response['list']['accounts'] = bankaccount_get_list();
                break;
            case 'addcomment':
                $fields['comment_id'] = generate_id("comment");
                $fields['comment']    = $_POST['comment'];
                $fields['commentor']  = $_SESSION['user']['profile_id'];;
                $fields['commentedontype'] = $_POST['comment_on'];
                $fields['commentedon']     = $_POST['id'];
                $fields['date']            = date("Y-m-d H:i:s");
                if (comment_add($fields)) {
                    $response['list']['comment'] = array(
                        "name" => ucwords($PSData['user']['fname'] . ' ' . $PSData['user']['lname']),
                        "userdp" => $PSData['user']['userdp'],
                        "comment" => $fields['comment']
                    );
                    echo "Saved";
                } else {
                    echo "Failed";
                    $response['status'] = '406';
                }
                break;
            case 'getcomments':
                //print_array(getallheaders());
                $response['list']['comments'] = comment_get_list(array(
                    "commentedontype" => $_GET['comment_on'],
                    "commentedon" => $_GET['id']
                ));
                break;
            case 'addToCart':
                $_SESSION['ticketing'][$_POST['productType']][$_POST['productid']] = $_POST;
                break;
            case 'validateeventbooking':
                $response1 = validate_event_form();
                if ($response1);
                $response = array_merge($response, $response1);
                break;
            case 'eventbooking':
            case 'eventbookingguestcheckout':
                $response1 = validate_event_form();
                if ($response1);
                $response = array_merge($response, $response1);
                if ($response['status'] != 406) {
                    $participantindex = 0;
                    $ticketcount      = array();
                    foreach ($tickettemp as $ticketcode => $ticketdata1) {
                        foreach ($ticketdata1 as $ticketindex => $ticketdata) {
                            $ticket            = array();
                            $ticketid          = '';
                            $ticket['tktcode'] = $ticketcode;
                            if (empty($ticketcount[$ticket['tktcode']]))
                                $ticketcount[$ticket['tktcode']] = 1;
                            else
                                $ticketcount[$ticket['tktcode']]++;
                            
                            //$newbib                     = $eventdata['ticketcategories'][$ticket['tktcode']]['lastbib'] + $ticketcount[$ticket['tktcode']];
                            //$newbibcode                 = $eventdata['ticketcategories'][$ticket['tktcode']]['bibinitial'] . $newbib;
                            $ticket['tkteventid']       = $transactionfields['productid'];
                            $ticket['tktemail']         = get_standard_email($ticketdata['email']);
                            $ticket['tktname']          = $ticketdata['name'];
                            $ticket['tktdob']           = ($ticketdata['dob']) ? $ticketdata['dob'] : '';
                            $ticket['tktmobile']        = ($ticketdata['mobile']) ? $ticketdata['mobile'] : '';
                            $ticket['tktgender']        = strtolower($ticketdata['gender']);
                            $ticket['tkttransactionid'] = $transactionfields['transactionid'];
                            $ticket['tktdatetimeslot']  = $_POST['timeslotid'];
                            $ticket['tktcustomdates']   = $_POST['customdates'];
                            //$ticket['tkttransactionstatus'] = 'pending';
                            //$ticket['tktbibno']         = $newbibcode;
                            $ticket['tktextra']         = serialize($ticketanswers[$ticketcode][$ticketindex]);
                            $ticket['tktstatus']        = 'complete';
                            
                            if ($_POST['event']['ticketpricetype'][$ticketcode] == 'bookingprice')
                                $ticket['tktstatus'] = 'bookingamount';
                            else
                                $ticket['tktstatus'] = 'fullamount';
                            
                            $ticketid = ticket_add($ticket);
                            
                            /*
                            ticketcategories_update($ticketcode, array(
                            'tktcatlastbibno' => $newbib
                            ));
                            */
                            /*{
                            $user = array();
                            $temp = explode(' ',$ticket['tktname']);
                            $user['fname'] = $temp[0];
                            $user['lname'] = $temp[1];
                            $user['email'] = $ticket['tktemail'];
                            $user['gender'] = ($ticket['tktgender'] == 'f')?'female':'male';
                            $user['password'] = getRandomString();
                            $user['profile_id'] = generate_id('profile');
                            $users[$user['email']] = $user;
                            }*/
                            $_POST['event']['tickets'][$ticketid] = $ticket['tktcode'];
                        }
                        $participantindex++;
                    }
                }
                if ($response['status'] != 406) {
                    
                    $_POST['eventId'] = $_POST['event']['productid'];
                    if ($_POST['couponcode']) {
                        $fields                     = array();
                        $fields['couponCode']       = $_POST['couponcode'];
                        $fields['couponEntityId']   = $_POST['event']['productid'];
                        $fields['couponEntityType'] = 'event'; //$_POST['event']['producttype'];
                        $fields['extraquery']       = '(couponsRemaining > 0 OR couponCount = -1)';
                        $fields['extraquery']       = $fields['extraquery'] . ' AND couponStatus = 1';
                        $fields['extraquery']       = $fields['extraquery'] . ' AND couponStatus = 1';
                        $date                       = date("Y-m-d h:i:s");
                        $fields['extraquery']       = $fields['extraquery'] . ' AND couponDateStart < "' . $date . '"';
                        $fields['extraquery']       = $fields['extraquery'] . ' AND couponDateEnd > "' . $date . '"';
                        $coupons                    = coupon_get_details($fields);
                        $coupons2                   = array();
                        foreach ($coupons as $coupon) {
                            $coupons2[$coupon['couponAppliesOn']] = array(
                                'couponType' => $coupon['couponType'],
                                'couponDiscount' => $coupon['couponDiscount']
                            );
                        }
                        $fields = array();
                    }
                    
                    $ticketcategories              = array();
                    $temp                          = $eventdata['ticketcategories'];
                    $amout                         = 0;
                    $ticketcount                   = 0;
                    $couponmeta['couponcode']      = $_POST['couponcode'];
                    $couponmeta['couponusercount'] = 0;
                    $selectedTicketArray           = array();
                    
                    if (isset($_POST['eventId'])) {
                        $organisersservicetax      = (float) $eventdata['organisersservicetax'];
                        $PSParams['psproducttype'] = $eventdata['producttype'];
                        if (empty($organisersservicetax))
                            $organisersservicetax = 0;
                    }
                    $leadcaptureform = array();
                    foreach ($_POST['event']['tickets'] as $ticketid => $ticketcode) {
                        if (array_key_exists($ticketcode, $eventdata['ticketcategories'])) {
			    if($ticketcode == 'leadcapture')
			    $leadcaptureform[] = 1;
			    else
			    $leadcaptureform[] = 0;
                            if ($_POST['event']['ticketpricetype'][$ticketcode] == 'bookingprice') //saumitra 24/4/2017 fb
                            {
                                $ticketprice1                 = $eventdata['ticketcategories'][$ticketcode]['bookingprice'];
                                $organisersservicetaxabsolute = 0;
                            } else {
                                $ticketprice1                 = $eventdata['ticketcategories'][$ticketcode]['price'];
                                $organisersservicetaxabsolute = ($organisersservicetax * $ticketprice1 / 100);
                            }
                            $selectedTicketArray[$ticketcode]['initialprice'] = $ticketprice1;
                            $ticketprice1                                     = $ticketprice1 + $organisersservicetaxabsolute;
                            
                            $discount     = 0;
                            /*
                            if($coupons2['All'])
                            $coupons2[$ticketcode] = $coupons2['All'];
                            if($coupons2[$ticketcode])
                            {
                            if($coupons2[$ticketcode]['couponType'] == 'Percentage')
                            $discount =  round($ticketprice1 * ($coupons2[$ticketcode]['couponDiscount']/100),0,PHP_ROUND_HALF_UP);
                            else if($coupons2[$ticketcode]['couponType'] == 'Fixed')
                            $discount = round($coupons2[$ticketcode]['couponDiscount'],0,PHP_ROUND_HALF_UP);
                            $couponmeta['couponusercount'] = $couponmeta['couponusercount'] + 1;
                            
                            }
                            */
                            $ticketprice1 = $ticketprice1 - $discount;
                            
                            //if ($eventdata['ticketcategories'][$ticketcode]['iscombo'] == 1)
                            //$ticketprice1 = $eventdata['ticketcategories'][$ticketcode]['price'] / $eventdata['ticketcategories'][$ticketcode]['combosize'];
                            $amount = $amount + $ticketprice1;
                            if ($eventdata['ticketcategories'][$ticketcode]['iscombo'] == 1 && !empty($selectedTicketArray[$ticketcode]['price'])) //prevent price of combo to be added again
                                {
                                continue;
                            }
                            $selectedTicketArray[$ticketcode]['price'] = $ticketprice1;
                            $selectedTicketArray[$ticketcode]['count']++;
                            $selectedTicketArray[$ticketcode]['organiserservicetax'] = $organisersservicetaxabsolute;
                            $ticketcount++;
                        }
                    }
                    // $ticketconvprice = round((3.5 / 100) * $amount ,2);
                    // $ticketservprice = round((10 / 100) * $ticketconvprice ,2);
                    //$totalamount = $amount + $ticketconvprice + $ticketservprice;
                    $ticketconvprice = $eventdata['convenience'] * $ticketcount;
                    $ticketconvprice = round($ticketconvprice + (($eventdata['servicetax']) * $ticketconvprice / 100), 0, PHP_ROUND_HALF_UP);
                    $totalamount     = $amount + $ticketconvprice;
                    
                    foreach ($selectedTicketArray as $ticketcode => $ticketdata) {
                        $discount = 0;
                        if ($coupons2['All'])
                            $coupons2[$ticketcode] = $coupons2['All'];
                        if ($coupons2[$ticketcode]) {
                            if ($coupons2[$ticketcode]['couponType'] == 'Percentage')
                                $discount = round($ticketprice1 * ($coupons2[$ticketcode]['couponDiscount'] / 100), 0, PHP_ROUND_HALF_UP);
                            else if ($coupons2[$ticketcode]['couponType'] == 'Fixed')
                                $discount = round($coupons2[$ticketcode]['couponDiscount'], 0, PHP_ROUND_HALF_UP);
                            $couponmeta['couponusercount'] = $couponmeta['couponusercount'] + 1;
                            
                        }
                        
                        $data                                  = geteventticketprice($ticketdata['price'], $eventdata['eventtypebyfee'], $eventdata['feepaymentoption']);
                        $effectiveprice['displaycost']         = $effectiveprice['displaycost'] + (($data['displaycost'] - $discount) * $ticketdata['count']);
                        $effectiveprice['organiserservicetax'] = $ticketdata['organiserservicetax'] * $ticketdata['count'];
                        $effectiveprice['initialcost']         = $effectiveprice['initialcost'] + $ticketdata['initialprice'];
                        $effectiveprice['paymentgatewayfee']   = $effectiveprice['paymentgatewayfee'] + ($data['paymentgatewayfee'] * $ticketdata['count']);
                        $effectiveprice['processingfee']       = $effectiveprice['processingfee'] + ($data['processingfee'] * $ticketdata['count']);
                        $effectiveprice['totaltaxfee']         = $effectiveprice['totaltaxfee'] + ($data['totaltaxfee'] * $ticketdata['count']);
                        $effectiveprice['organiserrevenue']    = $effectiveprice['organiserrevenue'] + ($data['organiserrevenue'] * $ticketdata['count']);
                    }
                    $couponmeta['pricedistribution']     = 'gateway:' . $effectiveprice['paymentgatewayfee'] . '|processing:' . $effectiveprice['processingfee'] . '|tax:' . $effectiveprice['totaltaxfee'] . '|organiserrevenue:' . $effectiveprice['organiserrevenue'] . '|organiserservicetax:' . $effectiveprice['organiserservicetax'];
                    $transactionfields['amount']         = $totalamount;
                    $transactionfields['productdetails'] = $_POST['event'];
                    $transactionfields['couponsapplied'] = $couponmeta;
                    //$eventeffectiveprice  = geteventticketprice($amount,3,1);
                    $transactionfields['amount']         = round($effectiveprice['displaycost'], 0);
                    
                    //autoregisterusers 
                    foreach ($users as $useremail => $userdata) {
                        if (user_get_count(array(
                            "email" => $userdata['email']
                        )) == 0) {
                            /*$temp = user_create_profile($userdata);
                            if($temp)
                            {
                            $profile_ids[] = $temp;
                            mailer(array("profile_id"=>$temp),'activation');
                            }*/
                        } else {
                            $temp = user_profile(array(
                                'email' => $userdata['email']
                            ));
                            if ($temp)
                                $profile_ids[] = $temp['profile_id'];
                        }
                    }
                    $transactionfields['userid'] = ($_SESSION['user']['profile_id']) ? $_SESSION['user']['profile_id'] : $profile_ids[0];
                    
			$subtransactionamount = 0;
			if(count($leadcaptureform) == 1 && array_sum($leadcaptureform) == 1)
			$leadcaptureflag = 1;
			else
			$leadcaptureflag = 0;
                        if($_POST['donationamount'] && $leadcaptureflag == 0)
			{
				$subtransactionFields = array();
				$subtransactionFields['amount'] 		= $_POST['donationamount'];
				$subtransactionamount = $subtransactionamount + $_POST['donationamount'];
				$subtransactionFields['couponsapplied'] = array();
				reset($_POST['event']['ticketing']);
				$key = key($_POST['event']['ticketing']);
				$subtransactionFields['productdetails']['user']['name'] = $_POST['event']['ticketing'][$key][0]['name'];
				$subtransactionFields['productdetails']['user']['email'] = $_POST['event']['ticketing'][$key][0]['name'];
				$subtransactionFields['productdetails']['user']['mobile'] = $_POST['event']['ticketing'][$key][0]['mobile'];
				$subtransactionFields['productdetails']['parenttransactionid'] = $transactionfields['transactionid'];
				$subtransactionFields['productid'] = $transactionfields['productid'];
				$subtransactionFields['producttype'] 	= $transactionfields['producttype'];
				$subtransactionFields['producturl'] 		= $transactionfields['producturl'];
				$subtransactionFields['transactionid'] 		= generate_id('txn-d');
				$subtransactionFields['userid'] 		= $transactionfields['userid'];
				$subtransactionFields['txnhtype']= 'child';
				$subtransactionFields['txntype']= 'contribution';
				$subtransactionFields['parenttxnid']= $transactionfields['transactionid'];
				$subtransactionFields['paymentdata'] = transaction_start($subtransactionFields);
				$transactionfields['productdetails']['subtransactions'][$subtransactionFields['transactionid']] = $subtransactionFields['amount'];
				$transactionfields['extraamount'][] = $subtransactionFields['amount'];
				$couponmeta['pricedistribution'] = $couponmeta['pricedistribution'].'|contribution:'.$subtransactionFields['amount'];
			}
			if($leadcaptureflag == 1)
			{
				$transactionfields['status'] = 'complete|success';
				$tempresponse = transaction_start($transactionfields);
				$response['leadcaptureflag']    = 1;
				if($eventdata['source'] != 'aggregated')
				$response['txnid'] = $transactionfields['transactionid'];
			}
			else
			{
				$response['paymentdata'] = transaction_start($transactionfields);
				$response['pricedistribution'] = $couponmeta['pricedistribution'];
				$response['effectiveprice']    = $effectiveprice;
			}
                }
                break;
            case 'forgot_pswd':
                $temp = user_profile(array(
                    "email" => $_REQUEST['email']
                ));
                if (!empty($temp['profile_id'])) {
                    mailer(array(
                        "profile_id" => $temp['profile_id']
                    ), 'resetpassword');
                    echo "Password Reset Link Sent";
                } else {
                    echo "No User Found with this email id";
                }
                break;
            case 'minimal_registration_process':
                if ($_POST['password'] || $_POST['email']) {
                    $userdata          = array();
                    $userdata['email'] = get_standard_email(trim($_POST['email']));
                    
                    if (user_get_count(array(
                        "email" => $userdata['email']
                    )) == 0) {
                        $userdata['profile_id'] = generate_id("profile");
                        $userdata['password']   = $_POST['password'];
                        $x                      = $_POST['name'];
                        $y                      = explode(' ', $x);
                        $userdata['fname']      = $y[0];
                        $userdata['lname']      = trim(str_ireplace($y[0], '', $x));
                        $temp                   = user_create_profile($userdata);
                        if (!empty($_POST['passions'])) {
                            $category = explode(',', $_POST['passions']);
                            foreach ($category as $cat) {
                                relation_register($userdata['profile_id'], 'joinedPassion', $cat);
                            }
                        }
                        if ($temp) {
                            $profile_ids[] = $temp;
                            mailer(array(
                                "profile_id" => $temp
                            ), 'activation');
                        }
                        $temp                   = user_log_in(array(
                            'email' => $userdata['email'],
                            'password' => $userdata['password']
                        ));
                        $response['login']      = 1;
                        $response['profile_id'] = $temp;
                    }
		    else
		    {
			$response['login']      = 0;
			$response['profile_id'] = 0;
			echo "User Already Exists";
		    }
                }
                break;
            case 'sharepostonemail':
                if ($_REQUEST['emails'] && $_REQUEST['post_id']) {
                    $emails                = explode(',', $_REQUEST['emails']);
                    $temp                  = user_profile(array(
                        "profile_id" => $_SESSION['user']['profile_id']
                    ));
                    $insertData['sender']  = $temp['email'];
                    $post                  = post_details(array(
                        'post_id' => $_REQUEST['post_id']
                    ));
                    $insertData['message'] = '<h3>' . htmlspecialchars_decode($post['title']) . '</h3>';
                    $insertData['message'] .= '<div class="post-content collapsed">' . nl2br(htmlspecialchars_decode($post['content'])) . '</div>';
                    $insertData['message']           = '<a href="' . ROOT_PATH . 'post/' . $post['post_id'] . '">' . $insertData['message'] . '</a>';
                    $insertData['status']            = 1;
                    $insertData['parent_message_id'] = 0;
                    if (isset($_FILES['attachments']) && $_FILES['attachments']['error'] == 0)
                        $insertData['attachments'] = upload('attachments', 'image', $_FILES['attachments']);
                    else
                        $insertData['attachments'] = '';
                    $insertData['dateadded'] = date("Y/m/d h:i:s");
                    foreach ($emails as $recipientmail) {
                        $insertData['recipient']  = $recipientmail;
                        $insertData['message_id'] = generate_id("message");
                        $x                        = messages_add($insertData);
                        if ($x) {
                            $mailer_id = mailer(array(
                                "email_id" => $insertData['recipient']
                            ), 'message', array(
                                "data" => $insertData['sender'] . ' has shared a post with you .<br /><br /><blockquote>' . $insertData['message'] . '<blockqoute>',
                                "title" => "Message from PASSIONSTREET",
                                "altmsg" => $insertData['sender'] . " sent you a message"
                            ));
                        }
                    }
                    echo "Shared on email";
                } else
                    echo "Failed";
                break;
            case 'login_process':
                if (!empty($_POST['email']) && !empty($_POST['password'])) {
                    if (!empty($_POST['email']))
                        $_POST['email'] = get_standard_email($_POST['email']);
                    $temp = user_log_in($_POST);
                }
                if (!empty($temp)) {
                    $response['login']      = 1;
                    $response['profile_id'] = $temp;
                    echo "";
                } else {
                    $response['login']      = 0;
                    $response['profile_id'] = 0;
                    echo "User Credentails do not match";
                }
                break;
            case 'post-like':
                if (!empty($_POST['post_id'])) {
                    $x = like_update($_POST['post_id'], 'post');
                    if ($x == 1) {
                        echo "<i class='fa fa-thumbs-up'></i> Like (" . ($_POST['post-c'] + 1) . ")";
                        $response['class']['add'] = 'active';
                    } else if ($x == 0) {
                        echo "<i class='fa fa-thumbs-up'></i> Like (" . ($_POST['post-c'] - 1) . ")";
                        $response['class']['remove'] = 'active';
                    }
                }
                break;
            case 'post-share':
                if (!empty($_POST['post_id'])) {
                    $type                   = $_POST['type'];
                    $post_details           = post_details(array(
                        'post_id' => $_POST['post_id']
                    ));
                    $temp                   = $post_details['extra'];
                    $temp['tshares']        = (($temp['tshares']) ? $temp['tshares'] : 0) + 1;
                    $temp[$type . 'shares'] = (($temp[$type . 'shares']) ? $temp[$type . 'shares'] : 0) + 1;
                    if (post_update($_POST['post_id'], array(
                        "extra" => $temp
                    )))
                        echo "Shared (" . $temp['tshares'] . ")";
                    else {
                        echo "Failed";
                        $response['status'] = '406';
                    }
                }
                break;
            case 'post-delete':
                if (!empty($_POST['post_id'])) {
                    $type = $_POST['type'];
                    if (post_update($_POST['post_id'], array(
                        "status" => 0
                    )))
                        echo "Removed Post";
                    else {
                        echo "Failed To Remove Post";
                        $response['status'] = '406';
                    }
                }
                break;
            case 'post-edit':
                if (!empty($_POST['post_id'])) {
                    $type = $_POST['type'];
                    if (post_update($_POST['post_id'], array(
                        "content" => $_POST['content']
                    )))
                        echo nl2br($_POST['content']);
                    else {
                        echo "Failed To Update Post";
                        $response['status'] = '406';
                    }
                }
                break;
            case 'submitrating':
                if ($_POST['id'] && $_POST['type'] && $_POST['rating'] && $_POST['rating'] >= 0 && $_POST['rating'] <= 5) {
                    if ($_POST['type'] == 'event') {
                        $eventid = $_POST['id'];
                        $temp    = user_profile();
                        $tempp   = $temp['extra'];
                        if (empty($tempp['eventratingsubmitted'][$eventid])) {
                            $temp = event_get_details($eventid);
                            if (empty($temp)) {
                                echo 'Error';
                                break;
                            }
                            $eventrating               = unserialize($temp['rating']);
                            $eventrating['users']      = $eventrating['users'] + 1;
                            $eventrating['totalscore'] = $eventrating['totalscore'] + $_POST['rating'];
                            event_update($eventid, array(
                                'rating' => serialize($eventrating)
                            ));
                            if (empty($eventrating['users']))
                                $eventrating['users'] = 0;
                            if (empty($eventrating['totalscore']))
                                $eventrating['totalscore'] = 0;
                            if ($eventrating['users'])
                                $rating = round(($eventrating['totalscore'] / $eventrating['users']));
                            else
                                $rating = 0;
                            for ($i = 1; $i <= 5; $i++) {
                                if ($rating >= $i)
                                    echo '<span data-rating=' . $i . ' class="fill"></span>';
                                else
                                    echo '<span data-rating=' . $i . '></span>';
                            }
                            if ($eventrating['users'])
                                echo '(' . $eventrating['users'] . ' users)';
                            $tempp['eventratingsubmitted'][$eventid] = 1;
                            user_profile_update(array(
                                'extra' => $tempp
                            ));
                        } else
                            echo 'Not allowed';
                    }
                }
                break;
            case 'calculateRidelogData':
                $activityTypecomposite = explode('###', $_POST['activitytype']);
                $activityType          = end($activityTypecomposite);
                if (empty($activityType)) {
                    echo "Failed To Remove Post";
                    $response['status'] = '406';
                } else {
                    $temp                     = user_profile();
                    $height                   = $temp['extra']['userprofile']['height'];
                    $weight                   = $temp['extra']['userprofile']['weight'];
                    $gender                   = $temp['gender'];
                    $age                      = $temp['age'];
                    $intime                   = $_POST['time']; //In HH:MM
                    $temp                     = explode(":", $intime);
                    $time                     = $temp[0];
                    $time                     = $time + round(($temp[1] / 60), 2);
                    $response['list']['time'] = $time;
                    $distance                 = $_POST['distance']; //In Kms
                    $heartRate                = $_POST['heartRate'];
                    $speed                    = $distance / $time;
                    $speedmileperhour         = $speed * 0.621371;
                    //$METArray = $METmin + (($speed - $METminspeed)*$METincrementperspeed);
                    $METArray                 = $PSParams['MET'][$activityType];
                    //krsort($METArray, SORT_NUMERIC);
                    $METArrayKeys             = array_keys($METArray);
                    foreach ($METArrayKeys as $key => $value) {
                        $inKey = (int) $METArray[$value];
                        if ($speedmileperhour <= $value) {
                            $diff1 = ($METArray[$METArrayKeys[$key - 1]] - $METArray[$METArrayKeys[$key - 2]]) / 100;
                            $MET   = $METArray[$METArrayKeys[$key - 2]] + (($speedmileperhour - $METArray[$METArrayKeys[$key - 2]]) * $diff1);
                            break;
                        }
                    }
                    if (empty($heartRate)) {
                        $BMR['male']   = (13.75 * $weight) + (5 * $height) - (6.76 * $age) + 66;
                        $BMR['female'] = (9.56 * $weight) + (1.85 * $height) - (4.68 * $age) + 655;
                        $calories      = ($BMR[$gender] / 24) * $MET * $time;
                    } else {
                        $cal['male']   = ((-55.0969 + (0.6309 * $heartRate) + (0.1988 * $weight) + (0.2017 * $age)) / 4.184) * 60 * $time;
                        $cal['female'] = ((-20.4022 + (0.4472 * $heartRate) - (0.1263 * $weight) + (0.074 * $age)) / 4.184) * 60 * $time;
                        $calories      = $cal[$gender];
                    }
                    $response['list']['calories'] = round($calories) . ' CAL';
                    $response['list']['speed']    = round($speed, 2) . ' KM/H';
                }
                break;
            case 'create-page':
                if ($_POST['name'] && $_POST['email'] && validate_email($_POST['email'])) {
                    $fields               = array();
                    $fields['page_name']  = $_POST['name'];
                    $fields['page_email'] = $_POST['email'];
                    $pageinsertid         = page_add($fields);
                    if ($pageinsertid) {
                        mailer(array(
                            "email_id" => $fields['page_email'],
                            "page_name" => $fields['page_name'],
                            "page_id" => $pageinsertid,
                            "user_name" => $PSData['user']['fname'],
                            "user_email" => $PSData['user']['email'],
                            "user_id" => $PSData['user']['profile_id']
                        ), 'verifycompany');
                        echo "Page verification link has been sent .Please verify ";
                    } else {
                        echo "Page name is already taken , please try a different name";
                        $response['status'] = '406';
                    }
                } else {
                    echo "Validation Failed , Please check you input";
                    $response['status'] = '406';
                }
                break;
            case 'logactivity':
                if (!empty($_GET['email']) && !empty($_GET['eventid'])) {
                    $logstr = date("Y-m-d h:i:s") . ',' . $_GET['email'] . ',' . $_GET['eventid'] . ',' . serialize($_REQUEST) . ',' . serialize(htmlspecialchars($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8')) . "\r\n";
                    file_put_contents(ROOT_DIR . DIRECTORY_SEPARATOR . 'activitylog.csv', $logstr, FILE_APPEND);
                    $response['status'] = '200';
                }
                break;
            case 'applycoupon':
                if (!empty($_POST['entityId']) && !empty($_POST['entityType']) && !empty($_POST['couponCode'])) {
                    $fields['couponCode']       = $_POST['couponCode'];
                    $fields['couponEntityId']   = $_POST['entityId'];
                    $fields['couponEntityType'] = $_POST['entityType'];
                    $fields['extraquery']       = '(couponsRemaining > 0  OR couponCount = -1)';
                    $fields['extraquery']       = $fields['extraquery'] . ' AND couponStatus = 1';
                    $fields['extraquery']       = $fields['extraquery'] . ' AND couponStatus = 1';
                    $date                       = date("Y-m-d h:i:s");
                    $fields['extraquery']       = $fields['extraquery'] . ' AND couponDateStart < "' . $date . '"';
                    $fields['extraquery']       = $fields['extraquery'] . ' AND couponDateEnd > "' . $date . '"';
                    $coupons                    = coupon_get_details($fields);
                    $coupons2                   = array();
                    foreach ($coupons as $coupon) {
                        $coupons2[$coupon['couponAppliesOn']] = array(
                            'couponType' => $coupon['couponType'],
                            'couponDiscount' => $coupon['couponDiscount']
                        );
                    }
                    $response['list'] = $coupons2;
                }
                break;
            case 'extract-members':
                if (!empty($_REQUEST['eventid'])) {
                    if ($_REQUEST['type'] == 'attending-members')
                        $response['list']['members'] = event_data(array(
                            'eventid' => $_REQUEST['eventid'],
                            'extraquery' => ' transactions.status = "complete|success" '
                        ));
                    else if ($_REQUEST['type'] == 'failed-members')
                        $response['list']['members'] = event_data(array(
                            'eventid' => $_REQUEST['eventid'],
                            'extraquery' => ' transactions.status <> "complete|success" '
                        ));
                }
                break;
            case 'eventpagedata':
                //$_GET['page'] = 'https://passionstreet.in/event/winter-delhi-half-marathon/event-8604225488';
                $pageviews = getpageviews($_GET['page'], date("Y-m-d", strtotime($_GET['startdate'])));
                if ($pageviews)
                    echo '<span alt="Views" title="Views" style="margin:auto"><i class="fa fa-eye" aria-hidden="true" style="margin-left:5px;"></i>&nbsp;' . $pageviews . ' Views</span>';
                break;
            case 'fetchExtUrl':
                $response['list'] = getUrlMeta(urldecode($_GET['url']));
                break;
            case 'eventticketdemoeffectiveprice':
                $data             = geteventticketprice($_POST['eventTicketPrice'], $_POST['eventTypeIdbyFees'], $_POST['eventFeepaymentoptions']);
                //$userpays = round($userpays);
                //$organiserpays = round($organiserpays);
                $response['list'] = array(
                    'initialPrice' => $data['initialcost'],
                    'organiserpays' => $data['organiserpaysps'],
                    'userpays' => $data['displaycost'],
                    'organiserrevenue' => $data['organiserrevenue'],
                    'totalPrice' => $data['displaycost'] * $_POST['eventTicketSold']
                );
                
                break;
            case 'eventticketeffectiveprice':
                $effectiveprice            = array();
                $PSParams['psproducttype'] = 'event';
                if (isset($_POST['eventId'])) {
                    $event                     = event_get_details($_POST['eventId'], 1);
                    $PSParams['psproducttype'] = $event['producttype'];
                    $organisersservicetax      = (float) $event['organisersservicetax'];
                    if (empty($organisersservicetax))
                        $organisersservicetax = 0;
                }
                foreach ($_POST['eventSelectedTickets'] as $tickettype => $ticketdata) {
                    if ($ticketdata['pricetype'] == 'bookingprice') //saumitra 24/4/2017 fb
                        $organisersservicetaxabsolute = 0;
                    else
                        $organisersservicetaxabsolute = ($organisersservicetax * $ticketdata['price'] / 100);
                    $data                                  = geteventticketprice($ticketdata['price'] + $organisersservicetaxabsolute, $_POST['eventTypeIdbyFees'], $_POST['eventFeepaymentoptions']);
                    $effectiveprice['displaycost']         = $effectiveprice['displaycost'] + ($data['displaycost'] * $ticketdata['count']);
                    $effectiveprice['organiserservicetax'] = $organisersservicetaxabsolute * $ticketdata['count'];
                    $effectiveprice['initialcost']         = $effectiveprice['initialcost'] + ($ticketdata['price'] * $ticketdata['count']);
                    $effectiveprice['paymentgatewayfee']   = $effectiveprice['paymentgatewayfee'] + ($data['paymentgatewayfee'] * $ticketdata['count']);
                    $effectiveprice['processingfee']       = $effectiveprice['processingfee'] + ($data['processingfee'] * $ticketdata['count']);
                    $effectiveprice['totaltaxfee']         = $effectiveprice['totaltaxfee'] + ($data['totaltaxfee'] * $ticketdata['count']);
                    $effectiveprice['organiserrevenue']    = $effectiveprice['organiserrevenue'] + ($data['organiserrevenue'] * $ticketdata['count']);
                }
                $effectiveprice['distribution'] = 'gateway:' . $effectiveprice['paymentgatewayfee'] . '|processing:' . $effectiveprice['processingfee'] . '|tax:' . $effectiveprice['totaltaxfee'] . '|organiserrevenue:' . $effectiveprice['organiserrevenue'] . '|organiserservicetax:' . $effectiveprice['organiserservicetax'] . '|organisertaxpercentage:' . $organisersservicetax;
                /*
                unset($effectiveprice['paymentgatewayfee']);
                unset($effectiveprice['processingfee']);
                unset($effectiveprice['totaltaxfee']);
                unset($effectiveprice['organiserrevenue']);
                unset($effectiveprice['organiserservicetax']);
                */
                $response['list']               = $effectiveprice;
                break;
            case 'event-follow':
                if ($_POST['eventid']) {
                    $x = relation_register($profile_id, 'followsEvent', $_POST['eventid']);
                    if ($x) {
                        $response['status'] = '200';
                        echo "Following Now";
                    } else {
                        $response['status'] = '304';
                        echo "Already Following";
                    }
                }
                break;
            case 'getmynetwork':
				$user = user_get_list_network();
				if ($user) {
					$response['list']   = $user;
					$response['status'] = '200';
				} else {
					$response['status'] = '403';
				}
                break;
		case 'startdonation':
			if ($_POST['amount']) {
				if(!($_POST['name'] && $_POST['email'] && $_POST['mobile']))
				return false;
				$transactionFields = array();
				$transactionFields['amount'] = $_POST['amount'];
				$transactionFields['couponsapplied'] = array();
				$transactionFields['productid'] = $_POST['productid'];
				$transactionFields['producttype'] = $_POST['producttype'];
				$transactionFields['producturl'] = $_POST['producturl'];
				$transactionFields['productdetails']['user']['name'] = $_POST['name'];
				$transactionFields['productdetails']['user']['email'] = $_POST['email'];
				$transactionFields['productdetails']['user']['mobile'] = $_POST['mobile'];
				$transactionFields['transactionid'] = generate_id("txn2");
				$transactionFields['txntype'] = 'contribution';
				$transactionFields['userid'] = ($_SESSION['user']['profile_id']) ? $_SESSION['user']['profile_id'] : $profile_ids[0];
				$response['paymentdata'] = transaction_start($transactionFields);
				$response['status'] = '200';
			} else {
				$response['status'] = '403';
			}
		break;
		case 'claimeventfromemail':
			// $token = strtolower(hash('sha512', implode("|",array($_POST['email_id'],$_POST['event_id'],$_POST['time'],ENCRYPTION_KEY))));
			$token = decodesignedrequest($_POST['mailtoken']);
			// if($token != $_POST['mailtoken'])
			$_POST = $token;
			if(empty($token['event_id']) || empty($token['email_id']))
			{
				$response['status'] = '403';
				$response['responsearray']['title'] = 'Alert';
				$response['responsearray']['content'] = 'Token Mismatch';
			}
			else if(empty($_SESSION['user']['profile_id']))
			{
			$response['status'] = '403';
			$response['responsearray']['title'] = 'Alert';
			$response['responsearray']['content'] = 'User needs to be logged in';
			}
			else if($_POST['event_id'])
			{
				$eventid = $_POST['event_id'];
				$event = event_get_details($eventid,1);
				$eventmanagelink = ROOT_PATH.'events/publish/'.$eventid;
				$eventlink = ROOT_PATH.'event/'.get_alphanumeric($event['eventname']).'/'.$eventid;
				if($event['creator'] == 'PS')
				{
					$adminemail = $event['organiser']['email'];
					$useremail = $_SESSION['user']['email'];
					if($useremail == $adminemail) //registered through same email 
					{
						event_update($event['event_id'], array(
							'creator' => $_SESSION['user']['profile_id'],
							'source'=>'converted',
							'status'=>'unpublished'
						));
						$response['status'] = '200';
						$response['responsearray']['title'] = 'Successfully Authorized';
						$response['responsearray']['content'] = 'Click below to edit / manage event .<br /><br /><a class="btn btn-primary" href="'.$eventmanagelink.'" target="_blank" >Manage</a> &nbsp;<a class="btn btn-primary" href="'.$eventlink .'">Reload</a>';
					}
					else
					{
						mailer(array('email_id'=>$adminemail,'authorizeemailid'=>$_SESSION['user']['email'],'authorizeprofileid'=>$_SESSION['user']['profile_id'],'event_id'=>$eventid,'eventname'=>$event['eventname'],'eventurl'=>ROOT_PATH.'event/'.get_alphanumeric($event['eventname']).'/'.$event['event_id']),'eventauthorizeotherprofile',array(),'blank.tpl');
						$response['status'] = '200';
						$response['responsearray']['title'] = 'Authorization request raised';
						$response['responsearray']['content'] = 'Since the email you used to login ie '.$useremail.' is not the same as event admins ('.$adminemail.'), we have sent an authorization request to '.$adminemail.' . Click on authorization link to authorize this user';
					}
				}
				else if($event['creator'] == $_SESSION['user']['profile_id'])
				{
					$response['status'] = '200';
					$response['responsearray']['title'] = 'Alert';
					$response['responsearray']['content'] = 'You are already the admin for this event .<br /><br /><a class="btn btn-primary" href="'.$eventmanagelink.'" target="_blank">Manage</a>  &nbsp;<a class="btn btn-primary" href="'.$eventlink .'">Reload</a>';
				
				}
				else
				{
					$response['status'] = '200';
					$response['responsearray']['title'] = 'Alert';
					$response['responsearray']['content'] = 'An admin has already been assigned to this event .<!--<br /><br /><a href="#" class="callmodaliframe btn btn-primary" data-targetsrc="module/contactform?email=social@passionstreet.in">Contact PASSIONSTREET</a>-->';
				}
			}
		break;
		case 'claimevent':
			global $connection;
			$eventid = $_POST['event_id'];
			$response['sss'] = 'aa'; 
			$event = event_get_details($eventid,1);
			if($event)
			{
				$email = $event['organiser']['email'];
				$mailsentcount = $connection->fetchAssoc("select count(email) as count from mailer_track where email = ? and mailertype = ? and initiatedon > '".date("Y-m-d H:i:s",time() - 3*60*60)."' limit 1",array($email,'eventclaiminvite'));
				$response['mailsenttoday'] = $mailsentcount; 
				if($mailsentcount['count'] == 0)
				{
					mailer(array('email_id'=>$email,'event_id'=>$event['event_id'],'eventname'=>$event['eventname'],'eventurl'=>ROOT_PATH.'event/'.get_alphanumeric($event['eventname']).'/'.$event['event_id']),'eventclaiminvite',array(),'blank.tpl');
				}
				$response['status'] = '200';
				$response['responsearray']['title'] = 'Request Raised';
				$response['responsearray']['content'] = 'We have sent a mail to <b>'.$email.'</b> . Click on the Claim / Manage button to claim .';
			}
			else
			{
			}
		break;
		case 'eventauthorizeotherprofile':
			// $token = strtolower(hash('sha512', implode("|",array($_POST['email_id'],$_POST['event_id'],$_POST['authorizeemailid'],$_POST['authorizeprofileid'],$_POST['time'],ENCRYPTION_KEY))));
			// $token = strtolower(hash('sha512', implode("|",array($_POST['email_id'],$_POST['event_id'],$_POST['time'],ENCRYPTION_KEY))));
			$token = decodesignedrequest($_POST['mailtoken']);
			// if($token != $_POST['mailtoken'])
			$_POST = $token;
			if(empty($token['event_id']) || empty($token['email_id']) || empty($token['authorizeemailid']))
			{
				$response['status'] = '403';
				$response['responsearray']['title'] = 'Alert';
				$response['responsearray']['content'] = 'Token Mismatch';
			}
			else if(empty($_SESSION['user']['profile_id']))
			{
			$response['status'] = '403';
			$response['responsearray']['title'] = 'Alert';
			$response['responsearray']['content'] = 'User needs to be logged in';
			}
			else if($_POST['event_id'])
			{
				$eventid = $_POST['event_id'];
				$event = event_get_details($eventid,1);
				$eventmanagelink = ROOT_PATH.'events/publish/'.$eventid;
				$eventlink = ROOT_PATH.'event/'.get_alphanumeric($event['eventname']).'/'.$eventid;
				if($event['creator'] == 'PS')
				{
					$adminemail = $event['organiser']['email'];
					$useremail = $_SESSION['user']['email'];
					$authorizeemailid = $_POST['authorizeemailid'];
					$authorizeprofileid = $_POST['authorizeprofileid'];
					if($authorizeprofileid) //registered through same email 
					{
						event_update($event['event_id'], array(
							'creator' => $authorizeprofileid,
							'source'=>'converted',
							'status'=>'unpublished'
						));
						$response['status'] = '200';
						$response['responsearray']['title'] = 'Successfully Authorized '.$authorizeemailid;
						$response['responsearray']['content'] = 'Click below to reload this event .<br /><br /><a class="btn btn-primary" href="'.$eventlink .'">Reload</a>';
					}
					else
					{
						mailer(array('email_id'=>$insertData['eventinfo']['organiser']['email'],'authorizeemailid'=>$_SESSION['user']['email'],'event_id'=>$insertData['event_id'],'eventname'=>$insertData['eventname'],'eventurl'=>ROOT_PATH.'event/'.get_alphanumeric($insertData['eventname']).'/'.$insertData['event_id']),'eventauthorizeotherprofile',array(),'blank.tpl');
						$response['status'] = '200';
						$response['responsearray']['title'] = 'Authorization request raised';
						$response['responsearray']['content'] = 'Since the email you used to login ie '.$useremail.' is not the same as event admins ('.$adminemail.'), we have sent an authorization request to '.$adminemail.' . Click on authorization link to authorize this user';
					}
				}
				else if($event['creator'] == $_SESSION['user']['profile_id'])
				{
					$response['status'] = '200';
					$response['responsearray']['title'] = 'Alert';
					$response['responsearray']['content'] = 'You are already the admin for this event .<br /><br /><a class="btn btn-primary" href="'.$eventmanagelink.'" target="_blank">Manage</a>  &nbsp;<a class="btn btn-primary" href="'.$eventlink .'">Reload</a>';
				
				}
				else
				{
					$response['status'] = '200';
					$response['responsearray']['title'] = 'Alert';
					$response['responsearray']['content'] = 'An admin has already been assigned to this event .<!--<br /><br /><a href="#" class="callmodaliframe btn btn-primary" data-targetsrc="module/contactform?email=social@passionstreet.in">Contact PASSIONSTREET</a>-->';
				}
			}
		break;
	    default:
                echo NULL;
        }
        $responsedata = ob_get_contents();
        ob_end_clean();
        $response['data']  = $responsedata;
        $response['boxid'] = ($_REQUEST['boxid']) ? $_REQUEST['boxid'] : '';
    }
}
echo json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
//print_array($response);
?>