<?php
require_once('initialise.php');
$PSJavascript['login_required'] = false;
$rows = array(
    /*"row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
             ),
             'rowWrapper'=>array(),
             'modules'=>array(
                'homebanner'=>array(
                    'modulesParams'=>array()
                )
            )
        ),*/
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'rowWrapper'=>array("<div class='container sm'>","</div>"),
             'modules'=>array(
                'eventfilters'=>array(
                    'modulesParams'=>array()
                ),
		'eventlisting'=>array(
                    'modulesParams'=>array()
                )
            )
        )
);

$page_meta['title'] = 'Events on PASSIONSTREET';$page_meta['description'] = 'Create, Manage and Join Events - Single Window for Ticketing, Social Interactions, Promotions and Analytics';$page_meta['keywords'] = 'PASSIONSTREET,Events,Featured Events,Workshops,Seminar,Conference,Exhibitions,Charity,Sports and Fitness,Award Programs,Competitions,Reunion,Networking,Concerts';
echo render_modules();

?>