<?php
require_once('initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
            ),
            'rowWrapper'=>array(),
            'modules'=>array(
                'homebanner'=>array(
                    'modulesParams'=>array("class"=>"banner")
                )
            )
        ),
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
            'modules'=>array(
                'registration'=>array(
                    'modulesParams'=>array(
                        'moduleDisplayStyle'=>2
                    )
                )
            )
        )
);

$page_meta = array(
	'title'=>'Registration'
);

$PSJavascript['login_required'] = false;
if($_SESSION['user']['profile_id'])
header('location:'.ROOT_PATH.'/home');
echo render_modules();

?>