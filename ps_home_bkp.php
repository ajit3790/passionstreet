<?php
require_once('initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
             ),
             'rowWrapper'=>array(),
             'modules'=>array(
                'homebanner'=>array(
                    'modulesParams'=>array()
                )
            )
        ),
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'columnStructure' =>true,
             'columns'=>array(
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-3 sidebar-left sidebar'
                     ),
                    'modules'=>array(
                         'memberlisting'=>array(
                            'modulesParams'=>array()
                          ),                         
                         'invite'=>array(
                            'modulesParams'=>array()
                          ) ,
                          'adbanner'=>array(
                            'modulesParams'=>array("adtype"=>"event")
                          )                        
                     )
                     
                 ),
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-5'
                     ),
                    'modules'=>array(
                         'postform'=>array(
                            'modulesParams'=>array()
                          ),
			 'wall'=>array(
                            'modulesParams'=>array()
                          )
                     )
                     
                 ),
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-4 sidebar-right sidebar'
                     ),
                    'modules'=>array(
                         'eventlisting'=>array(
                            'modulesParams'=>array()
                          ),
                          'adbanner'=>array(
                            'modulesParams'=>array("adtype"=>"event")
                          )
                     )
                     
                 )
            )
        )
);

$page_meta = array(
	'title'=>'PASSIONSTREET- Home'
);
//if(!loggedId())
//header('location:'.ROOT_PATH); 
echo render_modules();


?>