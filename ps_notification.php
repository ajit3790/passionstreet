<?php
require_once('initialise.php');
$rows = array(
    "row_2"=>array(
        'sectionParams'=>array(
            'class'=>'section'
         ),
        'rowWrapper'=>array("<div class='container sm'>","</div>"),
        'columnStructure' =>true,
        'columns'=>array(
            array(
                'columnParams'=>array(
                    'class'=>'col-md-7'
                ),
                'modules'=>array(
                    'desktopNotification'=>array(
                        'modulesParams'=>array()
                    )
		        )
	        )
	    )
	)
);

$PSParams['blockbots'] = 1;

$page_meta = array(
	'title' =>' Desktop Notification'
);
echo render_modules();
?>