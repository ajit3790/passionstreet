<?php
require_once('initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
            ), 
            'rowWrapper'=>array(),
            'modules'=>array(
                'indexbanner'=>array(
                    'modulesParams'=>array("class"=>"banner")
                )
            )
        ),
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>''
             ),
            'modules'=>array(
                'logout'=>array(
                    'modulesParams'=>array()
                )
            )
        )
);

$page_meta = array(
	'title'=>'Logout'
);

echo render_modules();

?>