<?php
require_once('initialise.php');
$PSJavascript['login_required'] = true;
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
             ),
             'rowWrapper'=>array(),
             'modules'=>array(
                'passiondescription'=>array(
                    'modulesParams'=>array()
                )
            )
        ),
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'columnStructure' =>true,
             'rowWrapper'=>array("<div class='container sm'>","</div>"),
             
             'columns'=>array(
                 /*array(
                     'columnParams'=>array(
                        'class'=>'col-md-3  sidebar-left sidebar'
                     ),
                    'modules'=>array(
                    	'memberlisting'=>array(
                            'modulesParams'=>array()
                          ),
                        'eventlisting'=>array(
                               'modulesParams'=>array()
                         )
                     )
                     
                 ),*/
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-7'
                     ),
                    'modules'=>array(
			'wallform'=>array(
                            'modulesParams'=>array()
                          ),
                         'wall'=>array(
                            'modulesParams'=>array()
                          )
                     )
                     
                 ),
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-5 sidebar-right sidebar'
                     ),
                    'modules'=>array(
                    	'eventlisting'=>array(
                               'modulesParams'=>array()
                         ),
			'memberlisting'=>array(
                            'modulesParams'=>array()
                          ),
                         'adbanner'=>array(
				'modulesParams'=>array("adtype"=>"event")
			)
                     )
                     
                 )
            )
        )
);
$temp = $PSParams['PSCategories'][$_GET['type']];
$page_meta['title'] = $temp['name'];
$page_meta['image'] = ROOT_PATH.'images/'.$temp['banner'];
$page_meta['keywords'] = implode(',',$temp['posttype']);
$page_meta['description'] = 'Follow your passion for '.$page_meta['title'].' & share your experiences about '.$page_meta['keywords'];

echo render_modules();

?>