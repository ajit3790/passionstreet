<?php
//groups/?category=discover

require_once('initialise.php');
$PSJavascript['login_required'] = true;

$rows = array(
    "row_1"=>array(
        'sectionParams'=>array(
            'class'=>'container-fluid'
         ),
         'rowWrapper'=>array(),
         'modules'=>array(
            'homebanner'=>array(
                'modulesParams'=>array()
            )
        )
    ),
    "row_2"=>array(
        'sectionParams'=>array(
            'class'=>'section'
         ),
         'rowWrapper'=>array("<div class='container sm'>","</div>"),
         'modules'=>array(
            'grouplisting'=>array(
                'modulesParams'=>array()
            )
        )
    )
);
echo render_modules();

?>