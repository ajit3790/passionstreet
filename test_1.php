<?php
error_reporting(E_ALL);
require('vendor/autoload.php');
use Aws\Common\Aws;
use Aws\S3\Exception\S3Exception;

function print_array($x)
{
	echo '<pre>';
	print_r($x);
	echo '</pre>';
}

$bucketName = 'testbucketjma';

$aws = Aws::factory('awsscripts/config.php');
$s3 = $aws->get('s3', array('region' => 'us-west-2'));

$s3->uploadDirectory('images',$bucketName.'/images');
?>