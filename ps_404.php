<?php
require_once('initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'pt-medium section',
                'attrs'=>'style="background:#3E6184;"'
            ),
            'modules'=>array(
                '404'=>array(
                    'modulesParams'=>array()
                )
            )
        )
);

$page_meta = array(
	'title'=>'Passion Street'
);

$page_includes = array(
	"header"=>1,
	"sitemap"=>1,
	"footer"=>1,
        "navigation"=>1
);	

echo render_modules();

?>