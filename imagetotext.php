<?php
$im = imagecreatetruecolor(strlen($_GET['text'])*10, 16);

$black = imagecolorallocate($im, 0, 0, 0);
imagecolortransparent($im,$black);

    // White background and blue text
    //$bg = imagecolorallocate($im, 255, 255, 255);
    $textcolor = imagecolorallocate($im, 0, 0, 255);

    // Write the string at the top left
    imagestring($im, 5, 0, 0, $_GET['text'], $textcolor);

    // Output the image
    header('Content-type: image/png');

    imagepng($im);
    imagedestroy($im);
?>