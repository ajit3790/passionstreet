<?php
// File Wrapper (Php type file functions);
//Reference https://docs.aws.amazon.com/aws-sdk-php/v3/guide/service/s3-stream-wrapper.html

error_reporting(E_ALL);
require('vendor/autoload.php');
function print_array($x)
{
	echo '<pre>';
	print_r($x);
	echo '</pre>';
}

$bucketName = 'testbucketjma';
use Aws\Common\Aws;
use Aws\S3\Exception\S3Exception;


$aws = Aws::factory('config.php');
$s3 = $aws->get('s3', array('region' => 'us-west-2'));

// Register the stream wrapper from a client object
$s3->registerStreamWrapper();

//Private File Upload
file_put_contents('s3://'.$bucketName.'/photos/testttt.png', 'asdsadsad');

//Public File Upload
$context = stream_context_create(array(
    's3' => array(
        'ACL' => 'public-read'
    )
));
file_put_contents('s3://'.$bucketName.'/photos/testttt2.png', 'asdsadsad' ,0 ,$context);

/*Note : file_get_contents and file_put_contents loads data in memory ,
SO not a better solution : https://docs.aws.amazon.com/aws-sdk-php/v3/guide/service/s3-stream-wrapper.html
instead use 
if ($stream = fopen('s3://bucket/key', 'r')) {
    // While the stream is still open
    while (!feof($stream)) {
        // Read 1024 bytes from the stream
        echo fread($stream, 1024);
    }
    // Be sure to close the stream resource when you're done with it
    fclose($stream);
}
*/
?>