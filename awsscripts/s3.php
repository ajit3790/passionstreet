<?php
error_reporting(E_ALL);
require('vendor/autoload.php');
use Aws\Common\Aws;
use Aws\S3\Exception\S3Exception;

function print_array($x)
{
	echo '<pre>';
	print_r($x);
	echo '</pre>';
}

$bucketName = 'testbucketjma';

$aws = Aws::factory('config.php');
$s3 = $aws->get('s3', array('region' => 'us-west-2'));
/*
//Creating S3 Bucket
try{
	$result = $s3->createBucket([
		'Bucket' => $bucketName,
		'LocationConstraint' => 'us-west-2',
	]);
	$s3->waitUntilBucketExists([
		'Bucket' => $bucketName,
		'LocationConstraint' => 'us-west-2',
	]);
}catch(S3Exception $e){
	echo $e->getMessage();
}
*/

/*
//Get Bucket List
$result = $s3->listBuckets();
$result['Buckets'];
print_array($result->toArray());
*/

/*
//Upload to s3
$result = array();
$result = $s3->putObject([
	'Bucket'	=>$bucketName,
	'Key'		=>'photos/photo01.jpg',
	'Body'		=>fopen('img-etauto-logo.png','r'),
	'ACL'		=>'public-read',
]);
$url = $result->get('ObjectURL');
echo $url;
*/


/*
//Upload Multiple to s3
$command[] = $s3->getCommand('PutObject',[
	'Bucket'	=>$bucketName,
	'Key'		=>'photos/abc1.png',
	'Body'		=>fopen('img-etr-sprites.png','r'),
	'ACL'		=>'public-read',
]);
$command[] = $s3->getCommand('PutObject',[
	'Bucket'	=>$bucketName,
	'Key'		=>'photos/abc2.png',
	'Body'		=>fopen('img-ettech-logo.png','r')
]);
$s3->execute($command);
foreach($command as $cmd)
{
	$response = $cmd->getResult();
	$url[] = $response->get('ObjectURL');
}
print_array($url);
*/


//List All Files
$result = $s3->listObjects([
	'Bucket'	=>$bucketName,
]);
print_array($result);

//upload directory
//$s3->uploadDirectory('images',$bucketName);
?>