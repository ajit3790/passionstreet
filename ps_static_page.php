<?php
require_once('initialise.php');
$PSJavascript['login_required'] = false;
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'container-fluid'
            ),
            'rowWrapper'=>array(),
            'modules'=>array(
                'homebanner'=>array(
                    'modulesParams'=>array("class"=>"banner")
                )
            )
        ),
    "row_2"=>array(
            'sectionParams'=>array(
             ),	    'rowWrapper'=>array("<div class='container sm'>","</div>"),            'modules'=>array(
                'static'=>array(
                    'modulesParams'=>array("class"=>"section")
                )
            )
        )
);

$page_meta = array(
	'title'=>'Passion Street'
);

$page_includes = array(
	"header"=>1,
	"sitemap"=>1,
	"footer"=>1,
        "navigation"=>1
);	

$PSJavascript['login_required'] = false;

echo render_modules();

?>