<?php
require_once('initialise.php');
$rows = array(    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'rowWrapper'=>array("<div class='container sm'>","</div>"),
             'columnStructure' =>true,
             'columns'=>array(
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-12'
                     ),
                    'modules'=>array(	
			'eventmanage'=>array(
                            'modulesParams'=>array()
                          )
		     )
                 )/*,
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-4'
                     ),
                    'modules'=>array(
			'eventbanner'=>array(
				'modulesParams'=>array('')
			),
			'eventcard'=>array(
				'modulesParams'=>array()							
			)
                     )
                 )*/
             )
        )
);

$page_meta = array(
	'title'=>'Event Manage'
);
$PSParams['blockbots'] = 1;

if(!loggedId())
header('location:'.ROOT_PATH); 

echo render_modules();

?>