<?php
$PSParams = array(
	'gender' => array('male' => 'Male' , 'female' => 'Female' , 'others' => 'Others'),
	'country' => array('india' => 'India' , 'USA' => 'United States' , 'UK' => 'United Kingdom'),
        'months' =>array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec'),
        'monthsfull' =>array('1'=>'January','2'=>'February','3'=>'March','4'=>'April','5'=>'May','6'=>'June','7'=>'July','8'=>'August','9'=>'September','10'=>'October','11'=>'November','12'=>'December')
);

$PSCategories = array(
    "culinary"=>array(
        "name"=>"Food Safari & Culinary",
        "icon"=>"culinary.jpg",
        "banner"=>"banner-culinary.jpg",
        "link"=>"passion/culinary",
        //"description"=>"There is a dramatic connection between your diet and your emotions.  It affects the body’s metabolism, hormones and neurotransmitters (mood chemicals that are produced in the brain), and these in turn influence our emotions, concentration and energy. Nothing makes one more happier than being on a Culinary & Food Safari.  Come and learn a great deal about different communities through sampling their local cuisine.",
        "expert"=>array("foodcolumnist","dietician","nutritionist","chef"),
        "posttype1"=>array(
		"culinary"=>"Culinary",
		"dietplan"=>"Diet Plan",
		"homemmade"=>"Home Made",
		"patisserie"=>"Patisserie",
		"hotelmenu"=>"Hotel & Restaurant Menu",
		"orientalcuisine"=>"Oriental Cuisine",
		"italiancuisine"=>"Italian Cuisine",
		"continenetalcuisine"=>"Continental Cuisine",
		"indianmughlaicuisine"=>"Indian & Mughlai Cuisine",
		"traditionalfood"=>"Traditional Food",
		"exoticfood"=>"Exotic Food",
		"healthyfood"=>"Healthy Food",
		"streetfood"=>"Street Food",
		"recipe"=>"Recipe"
        ),
	"posttype"=>array(
		'foodsafari'	=>'Food Safari',
		'culinary'	=>'Culinary',
		'traditionalfood'=>'Traditional Food',
		'dietplan'	=>'Diet Plan',
		'homemade'	=>'Home Made Food',
		'exoticfood'	=>'Exotic Food',
		'healthyfood'	=>'Healthy food',
		'streetfood'	=>'Street Food',
		'recipe'	=>'Recipe',
		'patisserie'	=>'Patisserie',
		//'hotelnrestaurant'	=>'Hotel & Restaurant',
		'orientalcuisine'=>'Oriental Cuisine',
		'italiancuisine'=>'Italian Cuisine',
		'continentalcuisine'=>'Continental Cuisine',
		'mughlaicuisine'=>'Indian & Mughlai Cuisine',
		'foodmenu'=>'Food Menu'
	)
    ),
    "cycling"=>array(
        "name"=>"Cycling",
        "icon"=>"cycling.jpg",
        "banner"=>"banner-cycling.jpg",
        "link"=>"passion/cycling",
        //"description"=>"Of all the happiest days of your life, the day you learnt to cycle must be amongst the top.  The thrill to balance yourself on those two wheels, the energy you feel as you speed up are one of the purest forms of joy you'd have ever experienced.  Get that thrill back in your life and enjoy its inherent benefits as riding your bicycle regularly is one of the best ways to reduce your risk of health problems associated with a sedentary lifestyle. If you are an ardent cyclist already, join in and share your experiences with others.  Mentor others, show them the way and spread happiness.",
        "expert"=>array("cyclist"),
        "posttype1"=>array(
		"bikefit"=>"Bike Fit",
		"cyclingtechnique"=>"Cycling Technique",
		"injuryremedy"=>"Injury & Remedy",
		"mountainbiking"=>"Mountain Biking",
		"roadbiking"=>"Road Biking",
		"cyclingtransport"=>"Cycling Transport"
        ),
	"posttype"=>array(
		"cycling"	=>"Cycling",
		"mountainbiking"=>"Mountain Biking",
		"roadbiking"	=>"Road Biking",
		"cyclingtransport"=>"Cycling Transport",
		//"bikefit"	=>"Bike Fit",
		//"cyclingtechnique"=>"Cycling Technique & safety",
		//"cyclingremedy"=>"Cycling - Injury & Remedy",
		"cyclingtour"=>"Cycling Tour",
		"offroad"=>"Off Road Cycling"
        ),
	"workouttype"=>array(
		"cyclinglog"=>"Cycling",
		"mountainbikinglog"=>"Mountain Biking",
		"offroadcyclinglog"=>"Off Road Cycling",
		"roadbikinglog"=>"Road Biking",
		"mtb"=>"MTB"
	)
    ),
    "music"=>array(
        "name"=>"Music",
        "icon"=>"music.jpg",
        "banner"=>"banner-music.jpg",
        "link"=>"passion/music",
        //"description"=>"Friedrich Nietzsche had said, \"Without music, life would be a mistake\".  The fact is, there isn’t a single human culture on earth that has lived without music!  For example, native Africans and Americans both used music in the form of chanting for their rituals and healing ceremonies.  In Ancient Greece music was used to ease stress, soothe pain and help with sleeping patterns.  Many have said music heals the soul, and it appears that they were right!  Music invariably leads to dance.  There is nothing more liberating than swaying to good music.  Nothing makes one more 'happy' than swinging to good music. Come and celebrate life with us!",
        "expert"=>array("singer","musicdirector","musician","musiccomposer"),
        "posttype1"=>array(
		"lyrics"=>"Lyrics",
		"composition"=>"Composition",
		"vocal"=>"Vocal",
		"instrumentalmusic"=>"Instrumental Music",
		"indianmusic"=>"Indian Music",
		"classicalmusic"=>"Classical Music",
		"westernmusic"=>"Western Music",
		"folkmusic"=>"Folk Music"
        ),
	"posttype"=>array(
		"music"		=>"Music",
		"indianmusic"	=>"Indian Music",
		"classicalmusic"=>"Classical Music",
		"westernmusic"=>"Western Music",
		"folkmusic"=>"Folk Music",
		"lyrics"=>"Lyrics",
		"composition"=>"Composition",
		"vocal"=>"Vocal",
		"instrumental"=>"Instrumental"
        )
    ),
    "painting"=>array(
        "name"=>"Painting",
        "icon"=>"painting.jpg",
        "banner"=>"banner-painting.jpg",
        "link"=>"passion/painting",
        //"description"=>"Art makes us more human; it helps us to communicate in a different, personal language.  No wonder the first thing a child learns, is to draw. Within this chaotic world we live, the visualization and relaxation that we obtain through painting are tools that in the long run, benefit our emotional, organic, energetic and spiritual being.  Come, let's paint!",
        "expert"=>array("painter"),
        "posttype1"=>array(
		"contemporarypaint"=>"Contemporary Painting",
		"modernart"=>"Modern Art",
		"abstractpainting"=>"Abstract Painting",
		"oilpainting"=>"oil Painting",
		"watercolor"=>"Water Color",
		"acrylicpainting"=>"Acrylic painting",
		"naturaldyepainting"=>"Natural Dye Painting"
        ),
	"posttype"=>array(
		"painting"	=>"Painting",
		"oilpainting"	=>"Oil Painting",
		"watercolor"	=>"Water Color",
		"acrylicpainting"=>"Acrylic Painting",
		"naturaldyes"	=>"Natural Dyes",
		"modernart"	=>"Modern Art",
		"abstractpainting"	=>"Abstract Painting"
		//"contemporary"	=>"Contemporary "
        )
    ),
    "yoga"=>array(
        "name"=>"Yoga",
        "icon"=>"yoga.jpg",
        "banner"=>"banner-yoga.jpg",
        "link"=>"passion/yoga",
        //"description"=>"Human beings are made up of three components—body, mind and soul corresponding these there are three needs—health, knowledge and inner peace.  Health is physical need, knowledge is our psychological needs and inner peace is spiritual need when all three are present then there is harmony. Yoga gives us relief from countless ailments at the physical level.  The practice of the postures (asans) strengthens the body and creates a feeling of well being.  From the psychological view point, yoga sharpens the intellect and aid in concentration; it steadies the emotions and encourages a caring for others. Yoga helps to discipline our sense of power with the power of our own.",
        "expert"=>array("yogaguru"),
        "posttype1"=>array(
            "yogatechnique"=>"Yoga Technique",
            "yogaposture"=>"Yoga Posture",
            "poweryoga"=>"Power Yoga",
            "traditionalyoga"=>"Traditional Yoga",
            "modernformyoga"=>"Modern-form Yoga"
        ),
	"posttype"=>array(
            "yoga"=>"Yoga",
            "poweryoga"=>"Power Yoga",
            "traditionalyoga"=>"Traditional Yoga",
            "modernyoga"=>"Modern Yoga Form",
            "yogatechnique"=>"Yoga Technique",
            "yogabenefits"=>"Yoga Benefits",
            "mudras"=>"Mudras",
            "meditation"=>"Meditation",
            "yogaasana"=>"Yoga Asana",
            "pranayam"=>"Pranayam",
            "chanting"=>"Chanting"
        )
    ),
    "running"=>array(
        "name"=>"Running",
        "icon"=>"running.jpg",
        "banner"=>"banner-running.jpg",
        "link"=>"passion/running",
        //"description"=>"Fun fact: There are few things in the world that can better or more rapidly treat depression than running.Research shows that running can raise your levels of good cholesterol while also helping you increase lung function and use.  In addition, running can also boost your immune system and lower your risk of developing blood clots.  Running helps the arteries retain their elasticity and strengthening the heart, your chances of suffering a heart attack can be significantly reduced.  Running is the second most effective exercise in terms of calories burned per minute.  Running also helps to reduce your chances of developing tension headaches.  Common now, you need anymore reason to get up & go for a run?",
        "expert"=>array("runner"),
        "posttype1"=>array(
		"runninglog"=>"Running Log",
		"runningtechnique"=>"Running Technique",
		"injuryremedy"=>"Injury & Remedy",
		"trailrunning"=>"Trail Running",
		"marathonrunning"=>"Marathon Running",
		"sprint"=>"Sprint",
		"barefootrunning"=>"Bare-foot Running",
		"ultrarunning"=>"Ultra Running"
        ),
	"posttype"=>array(
		"running"=>"Running",
		"trailrunning"=>"Trail Running",
		"marathonrunning"=>"Marathon",
		"sprint"=>"Sprint",
		"barefootrunning"=>"Bare-foot Running",
		"ultramarathon"=>"Ultra Marathon",
		//"runningtechnique"=>"Technique & Safety",
		//"runningremedy"=>"Injury & Remedy",
		"jogging"=>"Jogging"
        )
	,"workouttype"=>array(
		"trailrunninglog"=>"Trail Running",
		"marathonrunninglog"=>"Marathon",
		"sprintlog"=>"Sprint",
		"ultramarathonlog"=>"Ultra Marathon",
		"barefootrunninglog"=>"Barefoot Running",
		"jogginglog"=>"Jogging",
		"runninglog"=>"Running"
	)
    ),
    "photography"=>array(
        "name"=>"Photography",
        "icon"=>"photography.jpg",
        "banner"=>"banner-photography.jpg",
        "link"=>"passion/photography",
        //"description"=>"All of us look at the world around us from our own perspective and with a camera in hand, we go about sharing that perspective with others.  Some of us, use our camera to appreciate nature and people, some to express the pain, some to admire what the world has to offer and some simply capture a moment for eternity.  Use this space to exhibit what you've captured through your lens.",
        "expert"=>array("fasionphotographer","wildlifephotographer","eventphotographer"),
        "posttype1"=>array(
		"wildlifephoto"=>"Wildlife Photography",
		"fasionphoto"=>"Fasion Photography",
		"environmentphoto"=>"Environment Photography",
		"abstractphoto"=>"Abstract Photography",
		"photographytechnique"=>"Photography Technique",
		"stillphotography"=>"Still Photography",
		"videophotography"=>"Video Photography"
        ),
	"posttype"=>array(
		"photography"=>"Photography",
		"wildlifephoto"=>"Wildlife Photography",
		"fashionphoto"=>"Fashion Photography",
		"environmentphoto"=>"Environment Photography",
		"abstractphoto"=>"Abstract Photography",
		"photographytechnique"=>"Photography Technique",
		"stillphotography"=>"Still Life Photography",
		"videophotography"=>"Video Photography"
        )
    ),/*
    "mountaineering"=>array(
        "name"=>"Mountaineering",
        "icon"=>"mountain.jpg",
        "banner"=>"banner-mountain.jpg",
        "link"=>"passion/mountaineering",
        "description"=>"empty",
        "expert"=>array("mountaineer")
    ),*/
    "trekkingnmountaineering"=>array(
        "name"=>"Trekking",
        "icon"=>"treknmount.jpg",
        "banner"=>"banner-mountain.jpg",
        "link"=>"passion/trekkingnmountaineering",
        //"description"=>"There’s something inherently majestic, satisfying and refreshing of traversing a span nature for a period of time with nothing but what you can carry on your back.  From the invigorating sights to the quiet moments of self-reflection, trekking and backpacking the outdoors provides a number of health benefits ranging from physical to mental to spiritual.  What are you waiting for, let's go!",
        "expert"=>array("trekker-mountaineer"),
        "posttype1"=>array(
		"trekkingtechnique"=>"Trekking Technique",
		"trekking"=>"Trekking",
		"mountainclimbing"=>"Mountain Climbing",
		"hiking"=>"Hiking"
        ),
	"posttype"=>array(
		"trekking"=>"Trekking",
		//"trekkingtechnique"=>"Technique & Safety",
		"mountainclimbing"=>"Mountain Climbing",
		"hiking"=>"Hiking",
		"amsnremedy"=>"AMS & remedy"
        ),
	"workouttype"=>array(
		"trekkinglog"=>"Trekking",
		"hikinglog"=>"Hiking"
	)
    ),
    "voyage"=>array(
        "name"=>"Voyage",
        "icon"=>"voyage.jpg",
        "banner"=>"banner-voyage.jpg",
        "link"=>"passion/voyage",
        //"description"=>"Step into the wilderness, explore the unexplored, take a journey into the unknown and let yourself go of all the inhibitions. Go on a voyage with other like minded travel enthusiasts to refresh your mind & soul. ",
        "expert"=>array("travelexpert"),
        "posttype1"=>array(
		"expedition"=>"Expedition",
		"desertsafari"=>"Desert Safari",
		"pilgrimage"=>"Pilgrimage",
		"holidays"=>"Holidays",
		"voyage"=>"Voyage",
		"vacation"=>"Vacation",
		"surfacetravel"=>"Surface Travel",
		"airtravel"=>"Air Travel"
        ),
	"posttype"=>array(
		"voyage"=>"Voyage",
		"expedition"=>"Expedition",
		"desertsafari"=>"Desert Safari",
		"pilgrimage"=>"Pilgrimage",
		"holidays"=>"Holidays",
		"adventuretrip"=>"Adventure Trip",
		"vacation"=>"Vacation",
		"surfacetravel"=>"Surface Travel",
		"airtravel"=>"Air Travel",
		"watertransport"=>"Water Transport",
		"tripadvise"=>"Trip Advise"
        )
    ),
    "dancing"=>array(
        "name"=>"Dancing",
        "icon"=>"dancing.jpg",
        "banner"=>"banner-dancing.jpg",
        "link"=>"passion/dancing",
        //"description"=>"Our bodies are here for us to use to feel free and to express how we feel to the world.  We see children express with their bodies all the time.  When they are happy they jump up and down.  When they are upset they crash to the floor and pout.  Their bodies and minds are completely connected.  As we get older we forget to express with our bodies because we feel it is not acceptable.  People who continue to move when they get older understand how important it is to keep going.  Dance, is an excellent medium. Dance helps us remember what are bodies are capable off, helps us express and live!",
        "expert"=>array("dancer"),
	"posttype1"=>array(
            "indianclassical"=>"Indian Classical",
            "westernclassical"=>"Western Classical",
            "moderndance"=>"Modern dance",
            "folkdance"=>"Folk Dance"
        ),
	"posttype"=>array(
            "dance"=>"Dance",
            "indianclassical"=>"Indian Classical Dance",
            "westernclassical"=>"Western Classical Dance",
            "moderndance"=>"Modern Dance",
            "folkdance"=>"Folk Dance",
            "contemporary"=>"Contemporary Dance",
        )
    ),
    "motorbiking"=>array(
        "name"=>"Motorbiking",
        "icon"=>"motorbiking.jpg",
        "banner"=>"banner-motorbiking.jpg",
        "link"=>"passion/motorbiking",
        //"description"=>"There’s something inherently majestic, satisfying and refreshing of traversing a span nature for a period of time with nothing but what you can carry on your back.  From the invigorating sights to the quiet moments of self-reflection, trekking and backpacking the outdoors provides a number of health benefits ranging from physical to mental to spiritual.  What are you waiting for, let's go!",
        "expert"=>array("biker"),
        "posttype1"=>array(
		"tripadvise"=>"Trip advise",
		"bikefit"=>"Bike fit & repair",
		"safetytechnique"=>"Safety & Technique",
		"injuryremedy"=>"Injury & Remedy",
		"roadracing"=>"Road Racing",
		"dirtbiking"=>"Dirt Biking",
		"crosscountry"=>"Cross Country",
		"offroad"=>"Off Road",
		"hillclimb"=>"Hill Climb"		
        ),
	"posttype"=>array(
		"motorbiking"=>"Motorbiking",
		//"tripadvise"=>"Trip advise",
		//"bikefit"=>"Fit & Repair",
		//"bikingtechnique"=>"Technique & Safety",
		//"bikingremedy"=>"Injury & Remedy",
		"roadracing"=>"Road Racing",
		"dirtbiking"=>"Dirt Biking",
		"crosscountry"=>"Cross Country",
		"offroad"=>"Off Road Motorbiking",
		//"hillclimb"=>"Hill Climb"		
        )
    ),
    "rafting"=>array(
        "name"=>"Rafting",
        "icon"=>"rafting.jpg",
        "banner"=>"banner-rafting.jpg",
        //"description"=>"White water rafting is an exhilarating adventure deep into nature’s wilderness.  Perfect excursions for adventurers who are short on time, or those with open schedules, rafting adventures are the ultimate vacation experience.River rafting trips offers several benefits for adventurers, including team building – Fostering excellent team building skills, white water rafting is a superior team activity that draws people close together.  Teaching healthy camaraderie, river rafting teaches team skills through fun and dynamic real-life scenarios.",
        "link"=>"passion/rafting",
        "expert"=>array("raftingexpert"),
        "posttype1"=>array(),
        "posttype"=>array(
		'rafting'=>'Rafting',
		'raftingtechnique'=>'Rafting Technique & Safety',
		//'raftingremedy'=>'Injury & Remedy',
		//'raftingremedy'=>'Adventure'
	)
    )
);

$PSCategories = assoc_array_shuffle($PSCategories);

/*$PSSubCategories = array(
);*/

foreach($PSCategories as $passion=>$data)
{
	if(!empty($PSCategories[$passion]['workouttype']))
	{
		//$PSCategories[$passion]['posttype'] = array_merge($PSCategories[$passion]['posttype'],$PSCategories[$passion]['workouttype']); 
		$PSCategoriesWorkout[$passion] = $data;
	}
}

/*foreach($PSSubCategories as $key=>$value){
    $temp = $value;
    foreach($temp['parentCategory'] as $parent)
    {
        $valuetemp = array();
        $valuetemp = $temp;
        unset($valuetemp['parentCategory']);
        unset($valuetemp['parentCategoryWorkout']);
        $PSSubCategoriesByCategory[$parent][$key] = $valuetemp;
    }
    $temp = $value;
    foreach($temp['parentCategoryWorkout'] as $parent)
    {
        $valuetemp = array();
        $valuetemp = $temp;
        unset($valuetemp['parentCategory']);
        unset($valuetemp['parentCategoryWorkout']);
        $PSSubCategoriesByCategoryWorkout[$parent][$key] = $valuetemp;
    }
}*/

$PSExperts = array(
    "foodcolumnist"=>"Food Columnist",
    "dietician"=>"Dietician",
    "nutritionist"=>"Nutritionist",
    "chef"=>"Chef",
    "cyclist"=>"Cyclist",
    "singer"=>"Singer",
    "musicdirector"=>"Music Director",
    "musician"=>"Musician",
    "musiccomposer"=>"Music Composer",
    "painter"=>"Painter",
    "yogaguru"=>"Yoga Guru",
    "runner"=>"Runner",
    "fasionphotographer"=>"Fashion Photographer",
    "wildlifephotographer"=>"Wildlife Photographer",
    "eventphotographer"=>"Event Photographer",
    //"mountaineer"=>"Mountaineer",
    "travelexpert"=>"Travel Expert",
    "dancer"=>"Dancer",
    "trekker-mountaineer"=>"Trekker / Mountaineer",
    "raftingexpert"=>"Rafting Expert",
);
$PSMetValues = array(
	"cycling"	=>array(
		"03.0" 	=> 03.50,
		"05.6"	=> 05.80,
		"09.5"	=> 06.80,
		"12.0"	=> 08.00,
		"14.0"	=> 10.00,
		"16.0"	=> 12.00,
		"20.0"	=> 15.80
	),
	"running"	=>array(
		"02.0"	=>4.50,
		"03.7"	=>5.80,
		"04.1"	=>9.02,
		"05.3"	=>9.90,
		"06.8"	=>11.20,
		"07.6"	=>12.10,
		"08.9"	=>12.65,
		"10.1"	=>15.65,
		"11.1"	=>18.75,
		"12.1"	=>19.55,
		"13.1"	=>22.10,
		"14.0"	=>23.50
	),
	"trekkingnmountaineering"=>array(
		"02.5"	=>5.80,
		"03.6"	=>8.00,
		"05.0"	=>9.20
	),
	"motorbiking"=>array(
		"0.00"	=>3.80
	)
);
/*
$PSSmilies = array(
    "awesome"   =>  "<img src='".DEFAULT_IMG."' data-src='images/smile.png' class='smily unveil' alt='Awesome'/>",
    "good"      =>  "<img src='".DEFAULT_IMG."' data-src='images/glad.png' class='smily unveil' alt='Good'/>",
    "awefull"   =>  "<img src='".DEFAULT_IMG."' data-src='images/sad.png' class='smily unveil' alt='Awefull'/>",
    "okok"        =>  "<img src='".DEFAULT_IMG."' data-src='images/ok-ok.png' class='smily unveil' alt='Okok'/>",
    ":("        =>  "<img src='".DEFAULT_IMG."' data-src='images/sad.png' class='smily unveil' alt='Sad'/>"
);*/
$PSSmilies = array(
    "awesome"   =>  "<img src='images/smile.png' class='smily' alt='Awesome'/>",
    "good"      =>  "<img src='images/glad.png' class='smily' alt='Good'/>",
    "awefull"   =>  "<img src='images/sad.png' class='smily' alt='Awefull'/>",
    "okok"        =>  "<img src='images/ok-ok.png' class='smily' alt='Okok'/>",
    ":("        =>  "<img src='images/sad.png' class='smily' alt='Sad'/>"
);

//$PSCategories = ksort($PSCategories);
//ksort($PSCategories);
//ksort($PSSubCategories);
//ksort($PSSubCategoriesByCategory);
//ksort($PSSubCategoriesByCategoryWorkout);
$PSParams['PSCategories'] = ($PSCategories);
$PSParams['PSCategoriesWorkout'] = ($PSCategoriesWorkout);
//$PSParams['PSSubCategories'] = $PSSubCategories;
//$PSParams['PSSubCategoriesByCategory'] = $PSSubCategoriesByCategory;
//$PSParams['PSSubCategoriesByCategoryWorkout'] = $PSSubCategoriesByCategoryWorkout;
$PSParams['PSExperts'] = $PSExperts;
$PSParams['MET'] = $PSMetValues;
$PSParams['PSSmilies'] = $PSSmilies;

$PSParams['eventTypes'] = array('Workshop','Exhibitions','Seminars','Charity','Networking','Concerts','Performances','Sports & Fitness','Fundraiser','Ceremony','Reunion','Awards Programs','Competition','Others');
unset($PSCategories);
unset($PSSubCategories);
unset($PSSubCategoriesByPassion);
unset($PSExperts);
unset($PSSubCategoriesByCategoryWorkout);
unset($PSSmilies);

$event['feepaymentoptions'][1] = array('name'=>'Buyer pays all fees','processingfeepaidby'=>'buyer','gatewayfeepaidby'=>'buyer','taxpaidby'=>'buyer');
$event['feepaymentoptions'][2] = array('name'=>'Organizer absorbs all fees','processingfeepaidby'=>'organiser','gatewayfeepaidby'=>'organiser','taxpaidby'=>'organiser');
$event['feepaymentoptions'][3] = array('name'=>'Buyer pays processing fee & organizer pays gateway fee','processingfeepaidby'=>'buyer','gatewayfeepaidby'=>'organiser','taxpaidby'=>'organiser');
$event['feepaymentoptions'][4] = array('name'=>'Buyer pays gateway fee & organizer pays processing fee','processingfeepaidby'=>'organiser','gatewayfeepaidby'=>'buyer','taxpaidby'=>'organiser');
//$event['processingfee']['relative'] = 1.5; 
//$event['processingfee']['absolute'] = 12;
$event['typesbasedonfees'][3] = array('name'=>'PAID','processingfee'=>1,'gatewayfee'=>1);
$event['typesbasedonfees'][2] = array('name'=>'NON-PROFIT','processingfee'=>0,'gatewayfee'=>1);
$event['typesbasedonfees'][1] = array('name'=>'Free','processingfee'=>0,'gatewayfee'=>0);
$event['processingfee']['relative'] = 1.75; 
$event['processingfee']['absolute'] = 15;
$event['servicetax']['relative'] = 18;
$event['servicetax']['absolute'] = 0;
$event['paymentgatewayfee']['payu']['relative'] = 2.0; 
$event['paymentgatewayfee']['payu']['absolute'] = 0;
$PSParams['eventdefault'] = $event;
$event['processingfee']['relative'] = 5; 
$event['processingfee']['absolute'] = 100;
$PSParams['tourdefault'] = $event;
$PSParams['psproducttype'] = 'event';
unset($event);
?>