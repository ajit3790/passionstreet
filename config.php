<?php
error_reporting(0);
//error_reporting(E_ALL);
define('ROOT_DIR',__DIR__);
if(SERVERTYPE == 'stg')
define('PROTOCOL','http://');
else if($_SERVER['HTTPS'] == 'on' || $_SERVER['REQUEST_SCHEME'] == 'https')
define('PROTOCOL','https://');
else
define('PROTOCOL','https://');
if(SERVERTYPE == 'stg' || SERVERTYPE == 'live'){
	$tempdir = '';
}
else{
	$tempdir = 'passionstreet3/';
}
if(empty($_SERVER['HTTP_HOST']))
{
	if(SERVERTYPE == 'live')
	$_SERVER['HTTP_HOST'] = 'passionstreet.in';
	else if(SERVERTYPE == 'local')
	$_SERVER['HTTP_HOST'] = 'passionstreet.ini';
	else if(SERVERTYPE == 'stg')
	$_SERVER['HTTP_HOST'] = 'test.passionstreet.in';
	define('executiontype','cron');
}
else
define('executiontype','application');
define('ROOT_PATH',PROTOCOL.$_SERVER['HTTP_HOST'].'/'.$tempdir);
define('UPLOAD_DIR',ROOT_DIR.DIRECTORY_SEPARATOR.'files');
define('UPLOAD_PATH',ROOT_PATH.'files/');
define('MINIFY',false);
define('MODULE_DIR',ROOT_DIR.DIRECTORY_SEPARATOR.'modules');
define('CORE_DIR',ROOT_DIR.DIRECTORY_SEPARATOR.'core');
define('TEMPLATE_DIR',CORE_DIR.DIRECTORY_SEPARATOR.'template');
define("ENCRYPTION_KEY", "jauhar");
define("CSRF_KEY", "jauhar");

define("CACHING", false);
define("PAYMENTGATEWAY", "payu");
define("DEFAULT_IMG",ROOT_PATH.'images/ps.svg');
define("DEFAULT_IMG_HORIZONTAL",ROOT_PATH.'images/ps_horizontal.svg');
define("DEFAULT_IMG_VERTICAL",ROOT_PATH.'images/ps_vertical.svg');
//echo base64_encode('<svg style="background-color:#dddddd" xmlns="http://www.w3.org/2000/svg" width="200" height="200" viewBox="0 0 200 200"><rect width="100%" height="100%" fill="#ddd"/><text x="10%" y="55%" fill="#555" stroke="#555" font-size="20">PASSIONSTREET</text></svg>');
//define("DEFAULT_IMG",'data:image/svg+xml;base64,PHN2ZyBzdHlsZT0iYmFja2dyb3VuZC1jb2xvcjojZGRkZGRkIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMDAiIGhlaWdodD0iMjAwIiB2aWV3Qm94PSIwIDAgMjAwIDIwMCI+PHJlY3Qgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgZmlsbD0iI2RkZCIvPjx0ZXh0IHg9IjEwJSIgeT0iNTUlIiBmaWxsPSIjNTU1IiBzdHJva2U9IiM1NTUiIGZvbnQtc2l6ZT0iMjAiPlBBU1NJT05TVFJFRVQ8L3RleHQ+PC9zdmc+');
//define("DEFAULT_IMG_HORIZONTAL",'data:image/svg+xml;base64,PHN2ZyBzdHlsZT0iYmFja2dyb3VuZC1jb2xvcjojZGRkZGRkIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMDAiIGhlaWdodD0iMTAwIiB2aWV3Qm94PSIwIDAgMjAwIDEwMCI+PHJlY3Qgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgZmlsbD0iI2RkZCIvPjx0ZXh0IHg9IjEwJSIgeT0iNTUlIiBmaWxsPSIjNTU1IiBzdHJva2U9IiM1NTUiIGZvbnQtc2l6ZT0iMjAiPlBBU1NJT05TVFJFRVQ8L3RleHQ+PC9zdmc+');
//define("DEFAULT_IMG_VERTICAL",'data:image/svg+xml;base64,PHN2ZyBzdHlsZT0iYmFja2dyb3VuZC1jb2xvcjojZGRkZGRkIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMDAiIGhlaWdodD0iNDAwIiB2aWV3Qm94PSIwIDAgMjAwIDQwMCI+PHJlY3Qgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgZmlsbD0iI2RkZCIvPjx0ZXh0IHg9IjEwJSIgeT0iNTUlIiBmaWxsPSIjNTU1IiBzdHJva2U9IiM1NTUiIGZvbnQtc2l6ZT0iMjAiPlBBU1NJT05TVFJFRVQ8L3RleHQ+PC9zdmc+');

date_default_timezone_set('Asia/Calcutta');

ini_set('post_max_size', '50M');
ini_set('upload_max_filesize', '50M');

@session_start();
define('PROFILE_ID',$_SESSION['user']['profile_id']);
// error_reporting(0);
$module_defaults = array(
	'module_display_style'=>'',
	'moduleWrapper'=>array("<div class='container'>","</div>")
);
$row_defaults = array(
	'rowWrapper'=>array("<div class='container'>","</div>")
);

$page_meta_defaults = array(
	'title'=>'PASSIONSTREET : Connect with people who share your passion - Live What You Love',
        'description'=>'PASSIONSTREET is a community driven social network. Create or join communities that match your passion, express what you are passionate about. A platform for event ticketing, user engagement and promotion..',
	'image'=>ROOT_PATH.'images/banner-small.jpg',
        'keywords'=>'PASSIONSTREET,Social Network,Events,Connect,Share Experience,Communities,Featured Events,Workshops,Seminar,Conference,Exhibitions,Charity,Sports and Fitness,Award Programs,Competitions,Reunion,Networking,Concerts,Members,Experts,Followers,Club,Friend Zone, Trails, Passion Zone , Passion Categories ,Passion, PassionTrails, Marathon, Event ticketing, organizer, triathlon, duathlon, ticketing, adventure tourism, adventure sports',
	'url'=>$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].@$_SERVER['REDIRECT_URL'],
);

$page_includes_defaults = array(
	"header"=>1,
	"sitemap"=>0,
	"footer"=>1,
        "navigation"=>1,
        "schemadefault"=>1,
        "layout"=>"default"
);

$status_codes = array(
    "200"=>"Success",
    "403"=>"Unauthorized",
    "405"=>"Login Required",
    "406"=>"Parameters Mismatch",
    "304"=>"Update Already Performed"
);

$social['pagetype'] = 'website';
$social['sitename'] = 'PASSIONSTREET';
$social['fb']['appid'] = '652032041525469';
$social['tw']['handle'] = 'PassionStreetIN';
$social['gg']['publisher'] = 'https://plus.google.com/103133684089210044221';
$social['gg']['siteverification'] = 'mkeCawgM2vxvgFpGtAByDXQ55J9rw98g-N-QnvrFU2g';

if(SERVERTYPE == 'local')
$google_maps_key = "AIzaSyBsbAJL6QlioClOYVf8Gjfa8TrR3wmJ8aY";
else if(SERVERTYPE == 'live')
$google_maps_key = "AIzaSyDLc3XhdXieBmApoU8XXV85B8FoezrmSMM";

$PSData = array();
$PSModData = array();
$PSJavascript['root_path'] = ROOT_PATH;
$PSJavascript['upload_path'] = UPLOAD_PATH;
$PSJavascript['status_codes'] = $status_codes;
$PSJavascript['google_maps_key'] = $google_maps_key;
$PSJavascript['login_status'] = false;
$PSJavascript['login_required'] = true;
$PSJavascript['overridehistoryurl'] = true;
$PSJavascript['page_login_required'] = true;
$PSJavascript['sns'] = ((!empty($_SESSION['sns']))?$_SESSION['sns']:false);
$PSJavascript['snsid'] = ((!empty($_SESSION['snsid']))?$_SESSION['snsid']:false);
$PSJavascript['newpost'] = false;
//$PSJavascript['croptype']['post'] = array("aspect"=>array(0.5,0.66,1,1.3,1.5,2));
$PSJavascript['croptype']['posts'] = array("aspect"=>array(0.5,1,1.5,2));
$PSJavascript['croptype']['userdp'] = array("sizes"=>array(array("200","200")));
//$PSJavascript['croptype']['bg'] = array("sizes"=>array(array("720","266")));
$PSJavascript['croptype']['userbg'] = array("sizes"=>array(array("820","350")));
$PSJavascript['croptype']['eventbanner'] = array("sizes"=>array(array("1000","500")));
$PSJavascript['callbackselector']= array();
$PSPHPConsolelogs = array();


?>