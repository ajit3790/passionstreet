<?php
require_once('initialise.php');
$page_meta = array(
	'title'=>'Event Details'
);
$PSJavascript['login_required'] = false;
$eventid = $_GET['event_id'];
$producttype = $_GET['producttype'];
if(!($eventid) ||  !($event = event_get_details($eventid,true,PROFILE_ID,$producttype))){
    header("location:".ROOT_PATH.'events');
}
$eventdatestart = getdate(strtotime($event['datestart']));
$eventdateend = getdate(strtotime($event['dateend']));
if($eventdatestart['mday'].'/'.$eventdatestart['mon'].'/'.$eventdatestart['year'] == $eventdateend['mday'].'/'.$eventdateend['mon'].'/'.$eventdateend['year'])
$event['onedayEvent'] = true;
else
$event['onedayEvent'] = false;
$GLOBALS['page_meta']['title'] = (($event['eventname'])?$event['eventname']:'');
$GLOBALS['page_meta']['description'] = 'Join the Event | '.strip_tags($event['description']);
$temp[0] = $event['eventpic'];
if($event['mapimage'])
$temp[1] = $event['mapimage'];
if($event['routeimage'])
$temp[2] = $event['routeimage'];
$GLOBALS['page_meta']['url'] = ROOT_PATH.'event/'.get_alphanumeric($event['eventname']).'/'.$event['event_id'];
$GLOBALS['page_meta']['image'] = $temp;
$PSJavascript['passiontype'] = $event['category'];
$_GET['passiontype'] = $event['category'];
$PSJavascript['tickettypes'] = $event['ticketcategories'];
$datenow = date("Y-m-d H:i:s");
if($event['multislot'] == 1)
{
    foreach($event['datetimeslots'] as $slot)
    {
        unset($slot['id']);
        unset($slot['producttype']);
        unset($slot['productid']);
        unset($slot['status']);
        if($slot['bookingenddate'] > $datenow)
            $event['upcomingdatetimeslots'][] =  $slot; 
        else
            $event['endeddatetimeslots'][] =  $slot; 
    }
}
else
{
    $slot = $event['datetimeslots'][0];
    if($slot['bookingenddate'] > $datenow)
        $event['upcomingdatetimeslots'][] =  $slot; 
    else
        $event['endeddatetimeslots'][] =  $slot; 
}
$PSJavascript['upcomingdatetimeslots'] = $event['upcomingdatetimeslots'];
//$PSJavascript['convenience'] = (int)$event['convenience']; //20 absolute value 
//$PSJavascript['service'] = (int)$event['servicetax']; //15% of convenince
$PSJavascript['orgurl'] = ROOT_PATH.$producttype.'/'.$event['seopath'];
$PSJavascript['producttype'] = $producttype;
$PSModData['producttype'] = $producttype;
if($producttype == 'tour')
{
	$PSJsincludes['external'][] = 'js/datepickerv2/js/datepickerv2.js';
	$PSCssincludes['external'][] = 'js/datepickerv2/css/base.css';
	$PSCssincludes['external'][] = 'js/datepickerv2/css/clean.css';
}

$PSJavascript['eventid'] = $event['event_id'];
if($_GET['guestcheckout'])
$PSJavascript['overridehistoryurl'] = false;
if(($event['status'] == 'unpublished' && $event['isAdmin'] == 1) || $event['status'] == 'published')
{
if($event['status'] == 'unpublished'){
$PSParams['blockbots'] = 1;
}
$PSJavascript['eventTypeIdbyFees'] = (int)$event['eventtypebyfee'];
$PSJavascript['eventFeepaymentoptions'] = (int)$event['feepaymentoption'];
$PSJavascript['organiserservicetax'] = (float)$event['organisersservicetax'];
$PSJavascript['attendeefields'] = $event['attendeefields']['fields'];
$PSJavascript['questions'] = $event['preconditionOption'];
$PSJavascript['ticketing'] = $event['ticketcategories'];
$PSJavascript['skiptoleadcapture'] = $event['skiptoleadcapture'];
}
else
{
	header('location:'.ROOT_PATH.'events'); 
}
$PSParams['currentEvent'] = $event;



$rows = array(    
	"row_1"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'rowWrapper'=>array("<div class='container sm'>","</div>"),
             'columnStructure' =>true,
             'columns'=>array(
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-7'
                     ),
                    'modules'=>array(						 
			'eventbanner'=>array(
				'modulesParams'=>array('')
			 ),
			 'eventdetail'=>array(
                            'modulesParams'=>array()
                          ),
                         'memberlisting'=>array(
                            'modulesParams'=>array()
                          ),
                         'postform'=>array(
                            'modulesParams'=>array()
                          ),
			  'postdetail'=>array(
				'modulesParams'=>array()
			  ),'wall'=>array(
				'modulesParams'=>array()
			  ),'schemaevent'=>array(
				'modulesParams'=>array()
			  ) 
                     )
                     
                 ),
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-5 sidebar-right'
                     ),
                    'modules'=>array(						  
			'eventcard'=>array(
				'modulesParams'=>array()							
			),							
			'eventsidebar'=>array(
				'modulesParams'=>array()
			),
			'eventlisting'=>array(
				'modulesParams'=>array(
					'eventlistby'=>'city',
					'city'=>$event['city'],
					'listcount'=>3
				)
			)
                     )
                     
                 )
             )
        )
);

if($_GET['layout'] == 'amp'){
$rows = array(    
	"row_1"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
             'rowWrapper'=>array("<div class='container sm'>","</div>"),
             'columnStructure' =>true,
             'columns'=>array(
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-7'
                     ),
                    'modules'=>array(						 
			'eventbanner'=>array(
				'modulesParams'=>array()
			 ),
			 'eventsidebar'=>array(
				'modulesParams'=>array('module_display_style'=>'amp')
			 ),
			 'eventdetail'=>array(
				'modulesParams'=>array('module_display_style'=>'amp')
                          ),/*
			  'eventlisting'=>array(
				'modulesParams'=>array(
					'eventlistby'=>'city',
					'city'=>$event['city'],
					'listcount'=>3
				)
			  ),*/
                          'schemaevent'=>array(
				'modulesParams'=>array()
			  ) 
                     )
                     
                 )
             )
        )
);
}


if($_GET['layout'] && in_array($_GET['layout'], array('amp','default','internaliframe','onlymodule')))
{
    $page_includes = array(
        "layout"=>$_GET['layout']
    );
    $PSParams['amp'] = true;
}
$page_meta['allowfetchfromdb'] = 1;
/*if($_GET['admin'] == 1)
{
	$_SESSION['user'] = $PSData['user'] = user_profile(array("profile_id"=>'profile-3582055793'));
	print_array($_SESSION);
}*/

echo render_modules();

?>