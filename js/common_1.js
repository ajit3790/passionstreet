var $currentobj = null; 
var $currentselector = null;
var $orgwidth = 0,$orgheight = 0,$scaledwidth = 0,$scaledheight = 0;
var $multicrop = {};
var internaliframid = null;
$(document).ready(function(){
    if((login_status == false) && (login_required == true))
    loginModal('');
    if((login_status == true) && (profile_complete == false))
    callmodaliframe("Complete Your Profile","module/registration");

    $(".reltime").each(function(i,v){
        $(this).html(timeSince($(this).attr('time')));
    });
    
    $('.ajax-btn').on('click',function(){
       $temp = $(this);
       if($temp.attr('data-aj')=='true')
       psajax($temp);
    });
    
    $("body").on("click","#crop-done",function(){
            window[$currentselector.attr('callback')]();
    });

    $("body").on("click",".collapsed",function(){
            $(this).removeClass("collapsed");
    });

    $("body").on("click",".callmodaliframe",function(event){
        event.preventDefault();
        callmodaliframe($(this).text(),$(this).attr("targetsrc"));
    });
    
    var elements = document.querySelectorAll('inp   ut,select,textarea');
    for (var i = elements.length; i--;) {
        elements[i].addEventListener('invalid', function () {
            $navHeight = $(".navbar-fixed-top").height();
            var scrolledY = window.scrollY;
            if(scrolledY){
              window.scroll(0, scrolledY - $navHeight);
            }
        });
    }
    
    $('body').click(function(){
       $(".hideable:not(.hide)").addClass('hide'); 
    });
    $('body').on("change",".filecrop",function(){
        $currentobj = $(this);
        $currentselector = $("#"+$currentobj.attr('id'));
        console.log($currentselector);
        var cropper_box = '<div id="img_crop"> \
                   <div ><img id="cropper" /></div> \
                   <button class="btn btn-primary btn-sm hide" id="crop-done">Done</button>\
                   <div class="hide"><canvas id="preview"></canvas></div>\
                   </div>';
        myModal("Cropper",cropper_box,'');
        fileSelectHandler();

    });
    
    $('body').on("change",".multifilecrop",function(){
        $multicrop['currentFileSelector'] = $(this);
        $multicrop['currentActive'] = 0;
        $multicrop['resizeRatio'] = {}
        $multicrop['resizeRatio'][0] = 1;
        $multicrop['coordinates'] = {}
        $multicrop['coordinates'][0] = {};
        $cropImageLength = $multicrop['currentFileSelector'].get(0).files.length;
        $temp = 0;
        $tabheader = '<div class="tab-header"><ul class="nav nav-pills">';
        $tabcontent = '<div class="tab-content">';
        $cropHTML = '';
        for($temp = 0;$temp<$cropImageLength;$temp++){
            if($temp == 0)
                $active = 'active';
            else
                $active = '';
            $tabheader += '<li class="'+$active+'" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#imgcropper'+$temp+'">Image '+($temp+1)+'</a></li>';
            $tabcontent += '<div id="imgcropper'+$temp+'" class="row tab-pane fade '+$active+' in" role="tabpanel">';
            $tabcontent += '<div style="width:400px"><img style="width:100%;" id="multiimagecrop'+$temp+'" src="" ></div>';
            $tabcontent += '</div>';
        }
        $tabheader += '</ul></div>';
        $tabcontent += '</div>';
        myModal("Cropper","<div class='multicropper'>"+$tabheader+$tabcontent+"</div>",'');
        for($temp = 0;$temp<$cropImageLength;$temp++){
            getDataURIfromFile($multicrop['currentFileSelector'],$temp,'#multiimagecrop'+$temp,'addCropper');
        }
        
    });

    
    $("section").each(function(i,v){
        if($.trim($(this).text()) == '')
            $(this).css('padding','0px');
    });
    
    
    
    
    $('.ajaxImageUpload').change(function() {
    formdata = false;
    if (window.FormData) {
        formdata = new FormData();
    }
    var filedata = ($(this))[0];
    var i = 0, len = filedata.files.length, img, reader, file;
    for (; i < len; i++) {
        file = filedata.files[i];

        if (window.FileReader) {
            reader = new FileReader();
            reader.onloadend = function(e) {
                //showUploadedItem(e.target.result, file.fileName);
            };
            reader.readAsDataURL(file);
        }
        if (formdata) {
            formdata.append("file[]", file);
        }
    }
    formdata.append('content_id',$(this).attr('content_id'));
    formdata.append('content_type',$(this).attr('content_type'));
    $event_gal = $(this).parents('.event-gallery').find('ul');
    if($event_gal.attr('id') === undefined)
    {
        $event_gal.attr('id','event_gal_'+getRandomInt(100,999));
    }
    $boxid = $event_gal.attr('id');
    if (formdata) {
        $.ajax({
            url: root_path+'PSAjax.php?type=imageuploadajax2&boxid='+$boxid,
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function(data) {
                $res = $.parseJSON(data);
                console.log($res);
                $('body').find('#'+$res['boxid']).prepend($res['data']);
            },       
            error: function(res) {

            }       
          });
        }
    });
    
    
    $('.membercarousel').bxSlider({
    slideWidth:170,
    minSlides: 1,
    maxSlides: 5,
    infiniteLoop: false,
    hideControlOnEnd: true,
    prevText:" ",
    nextText:" ",
    slideMargin: 10
    });
    
    $('.eventcarousel').bxSlider({
    slideWidth:320,
    minSlides: 1,
    maxSlides: 3,
    infiniteLoop: false,
    hideControlOnEnd: true,
    prevText:" ",
    nextText:" ",
    slideMargin: 20
    })  ;
    
    
    
    $("#forgot_password_btn").click(function(event){
        event.preventDefault();
        $(".login_form").toggle();
        $(".forgot_form").toggle();
        $temp = $(this).text();
        $(this).text($(this).attr('alternatetext'));
        $(this).attr('alternatetext',$temp);
    });
    $("#send_pswd_btn").click(function(){
        $(".error-text").hide();
        $temp = $("#frgt_email").val();
        if($temp == '')
        $(this).parents(".forgot_form").find(".error-text").html('Please provide Email Id').show(); 
        else
        {
            $.post(root_path+'PSAjax.php?type=forgot_pswd', {email:$temp,boxid:$(this).attr('id')}, function(data){ 
                $res = $.parseJSON(data);
                $("#"+$res['boxid']).parents(".forgot_form").find(".error-text").html($res['data']).show();
            });
        }
    });
    
    $("#login_btn").click(function(){
        $(".error-text").hide();
        $tempemail = $("#loginemail").val();
        $temppassword = $("#loginpassword").val();
        if($tempemail == '' || $temppassword == '')
        $(this).parents(".login_form").find(".error-text").html('Please provide Valid Input').show();
        else
        {    
            $.post(root_path+'PSAjax.php?type=login_process', {email:$tempemail,password:$temppassword,boxid:$(this).attr('id')}, function(data){ 
                $res = $.parseJSON(data);
                $("#"+$res['boxid']).parents(".login_form").find(".error-text").html($res['data']).show();
                if($res['login']==1)
                reloadwindow();
            });
        }
    });
    $(".sharepost").on('click',function(event){
       $temp = {};
       $temp['title'] = $(this).parents('.post').find('h3').text();
       $temp['url'] = $(this).attr('shareurl');
       sharepage($(this).attr('type'),$temp); 
    });
	$(".sharepostcustom").on('click',function(event){
		$(this).next('ul').slideToggle(200);
	});
	$(".sharepostonemail").on('click',function(){
		$parent = $(this).parent();
		$temp = $(this).prev().val();
		if($temp){
			$.post(root_path+'PSAjax.php?type=sharepostonemail', {email:$temp,post_id:$(this).attr('post_id'),boxid:$(this).attr('id')}, function(data){ 
				$res = $.parseJSON(data);
				$parent.append('<span>'+$res['data']+'</span>');
			});
		}
	});
    $(".sharepage").click(function(){
       sharepage($(this).attr('type'),''); 
    });
	$(".copybtn").on('click',function(){
		copyToClipboard($(this).attr('content'));
	});
    $(".toggle-nav").click(function(){
        $(".ps-menu").slideToggle(200);
    });
    if(q['module'] != undefined)
    {
        callmodaliframe(q['title'],'module/'+q['module']+'?'+q['p']);
    }
    $(".simpleeditor").summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']]
          ]
    });
    
    $(".addmore").click(function(event){
       event.preventDefault();
       
       $temp = $("."+$(this).attr('trgtelement')).eq(0); 
       $temp.parent().append($temp[0].outerHTML);
       if($("."+$(this).attr('trgtelement')).length >= 4)
       {
           $(this).addClass('hide');
       }
    });
    
    if(q['pagetype'] == 'userprofile')
    {
    	 $(".covr_phto").load(function() {
	    	$layerheight = $(".layer").height();
	    	$layerwidth = $(".layer").width();
	    	$coverimgheight = $(".covr_phto").prop('naturalHeight');
	    	$coverimgwidth = $(".covr_phto").prop('naturalWidth');
	    	if($coverimgheight> $layerheight ){
		    	$cvroffsetheight = ($coverimgheight - $layerheight)/(-2);
		    	$(".covr_phto").css({'position':'relative','top':$cvroffsetheight});
			}
			else if($coverimgwidth> $layerwidth ){
				$cvroffsetwidth = ($coverimgwidth - $layerwidth)/(-2);
		    	$(".covr_phto").css({'position':'relative','left':$cvroffsetwidth});
			}
			else{
				$(".covr_phto").css({'min-width':'100%','min-height':'350px'});
			}
    	});
    }
    
    
    prepare_youtube_frames();
    
    if(overridehistoryurl == true && window.location['href'] != orgurl)
    overRideHistoryState(stripQueryString(window.location['href']),'');
    
    $sns1 = {};
    $sns1 = clientid;
    if(newpost != false && sns!=false)
    {
        hello.init($sns1);
        hello(sns).api('me/share','post',newpost).then(function(json) {
            console.log(json);
        }, function(e) {
                console.log('Whoops! ' + e.error.message);
        });
    }

    $(".createloginpopup").click(function(event){
       event.preventDefault();
       $sns2 = {};
       $x = $(this).attr('type1');
       $sns2[$x] = $sns1[$x];
       
       if(login_status == false)
       {
            hello.init($sns2);
            hello($x).login({'scope':'basic,email,birthday,friends,publish'}).then(function(auth) {
                    sessionStart(auth);
            }, function(e) {
                    console.log('Signin error: ' + e.error.message);
            });
       }
        
        
        
       //src = $(this).attr('href')+'/'+$(this).attr('type');
       //popitup('loginwindow',src,src+"?loggedin=true");
    });
    
    var nicesx = $(".scroll-content").niceScroll({touchbehavior:false,cursorcolor:"#333",cursoropacitymax:0.6,cursorwidth:8});
    //Google Analytics 
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-75357427-1', 'auto');
    ga('send', 'pageview');

});
$(".invite").click(function(){
    console.log('asdasdad');
    hello(sns).api('me/friends').then(function(r) {
        console.log(r);
    });
});
function sessionStart(auth){
    //console.log(auth);
    hello(auth.network).api('/me').then(function(r) {
        $.post(root_path+'PSAjax.php?type=loginsns',{network:auth.network,data:r}, function(data){ 
            //console.log(data);
            loadPage('home');
        });
    });
    
}

var newwindow;
var newwindowTimer;
var newwindowTarget;
function popitup(name,url,trgt) {
        newwindow=window.open(url,name,'height=600,width=600,toolbar=no,titlebar=no,status=no,resizable=no,menubar=no,location=no,top=100,left=300,screenX=300,screenY=100');
	if(trgt!='')
        {
            newwindowTarget = trgt;
            windowWatcher();
        }
        if (window.focus) {newwindow.focus()}
        return false;
}
function windowWatcher(){
    newwindowTimer = setInterval(function(){
        $crntwindow = window['window']['$window'][0]['newwindow']['location']['href'];
        if($crntwindow.indexOf(newwindowTarget) > -1)
        {
            clearInterval(newwindowTimer);
            newwindow.close();
            location.assign(root_path+'home');
        }
    }, 1000);
}
function reloadwindow(){
    location.replace(location.href);
}
function calliframe(targetsrc){
    internaliframid = 'iIframe_'+getRandomInt(100,999);
    return "<iframe id='"+internaliframid+"' src='"+targetsrc+"' frameborder=0 scrolling='no' style='border:0px;width:100%'></iframe>";
}
function callmodaliframe(title,targetsrc){
    myModal(title,calliframe(targetsrc),"cframe");
}

function loadPage(url){
    window.location.href = root_path + url;
}
function stripQueryString(url) {
  return url.split("?")[0].split("#")[0];
}
function overRideHistoryState(url,title){ 
	try{
		if(!url) url = window.location;
		if(!title) title = $(document).find("title").text();
		var State = History.getState();
		/* 
		History.log('initial:', State.data, State.title, State.url);
		History.Adapter.bind(window,'statechange',function(){ 
			var nState = History.getState();
			History.log('statechange:', nState.data, nState.title, nState.url);
		}); */
		History.replaceState({}, title, url);
	} catch(e){ console.log(e); }
}
function sharepage($type,$metaObj){
    if($metaObj == '')
    {
        $url = orgurl;
        $title = meta['title'];
    }
    else{
        $url = $metaObj['url'];
        $title = $metaObj['title'];
    }
    
    if($type == 'fb')
    $url = 'http://www.facebook.com/sharer.php?u='+$url;
    else if($type == 'tw')
    $url = 'http://twitter.com/share?url='+$url+'&text='+$title;
    else if($type == 'gg')
    $url = 'https://plusone.google.com/_/+1/confirm?hl=en&url='+$url;
    
    popitup('sharewindow',$url,'');    
}
function psajaxcallback(data){
    if(status_codes[data['status']] == 'Success');
    {
        if(data['boxid'] != '')
        {
            $("#"+data['boxid']).text(data['data']);
        }
    }
}
function psajax($obj)
{
    var $params = {};
    $action = $obj.attr('data-action');
    $type = $obj.attr('data-t');
    $c = $obj.attr('data-c');
    if($c === undefined)
        $c = 'psajaxcallback';
    $ps = $obj.attr('data-p-str');
    $parray = $ps.split(',');
    $.each($parray,function($i,$v){
        $params[$v] = $obj.attr('data-'+$v);
    });
    if($obj.attr('trgtid') !== undefined)
    $params['boxid'] = $obj.attr('trgtid');
    else if($obj.attr('id') !== undefined)
    $params['boxid'] = $obj.attr('id');
    $.post(root_path+'PSAjax.php?type='+$type, $params, function(data){ 
        window[$c]($.parseJSON(data));
    });
}
function myModal($title,$content,$class)
{
    $("#modal-box h2").html($title);
    $("#modal-box .modal-dialog").prepend('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>');
    $("#modal-box .modal-container").html('<div class="row"><div class="col-md-12">'+$content+'</div></div>');
    $("#modal-box").addClass($class).modal();
}
function closemyModal(){
    $("#modal-box").modal('hide');
}
function loginModal($class)
{
    $topoffset = $(".navbar-fixed-top").height();
    $("#modal-box").addClass($class).modal({backdrop: 'static',keyboard: false});
    $(".modal-backdrop").css({"opacity":"0.95","margin-top":$topoffset+"px"});
    $(".modal").css({"margin-top":$topoffset+"px"});
    $(".modal-dialog").css({"margin-top":"5%"});
    $(".login-btn").addClass("hide");
}
function initGoogleMaps() {
  map_position_obj = $.parseJSON(map_position);
  map = new google.maps.Map(document.getElementById('map'), {
    center: map_position_obj,
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var markers = [];
  marker = new google.maps.Marker({
    map: map,
    title: 'Connaught Place, New Delhi, India',
    position: map_position_obj
  });
}

function calculateAndDisplayRoute(directionsService, directionsDisplay,start , stop , waypts) {
  
  directionsService.route({
    origin: start,
    destination: stop,
    waypoints: waypts,
    optimizeWaypoints: true,
    travelMode: google.maps.TravelMode.DRIVING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        $img = routemaptoimage();
        $("#route_pic").val($img);
      } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

function maptoimage(){
    var currentPosition=map.getCenter();
    return "http://maps.google.com/maps/api/staticmap?key="+google_maps_key+"&scale=2&sensor=false&center=" +
      currentPosition.lat() + "," + currentPosition.lng() +
      "&zoom="+map.getZoom()+"&size=300x200&markers=color:red|" +
      currentPosition.lat() + ',' + currentPosition.lng();
}
function getDataURIfromFile($fileObject,$index,$targetSelector,$callback){
    var $file = $fileObject.get(0).files[$index];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $temp = $($targetSelector).get(0);
      $temp.src = reader.result;
      $temp.onload = function () {
        if($callback != '')
        window[$callback]($targetSelector,$index);
      }
    }, false);

    if ($file) {
      reader.readAsDataURL($file);
    }
}
function addCropper($imgSelector,$index){
    $imgObj = $($imgSelector);
    $previewX = $imgObj[0]['width'];
    $previewY = $imgObj[0]['height'];
    $orgX = $imgObj[0]['naturalWidth'];
    $orgY = $imgObj[0]['naturalHeight'];
    $multicrop['currentActive'] = $index;
    $multicrop['resizeRatio'][$multicrop['currentActive']] = $previewX / $orgX;
    $imgcroptype = $multicrop['currentFileSelector'].attr('croptype');
    if($(".multicropdata").length ==0 )
    $multicrop['currentFileSelector'].after('<div class="multicropdata"></div>');
    $(".multicropdata").append("<input type='hidden' id='multicropdata_"+$multicrop['currentFileSelector'].attr('id')+"_"+$index+"' name='mutlicropdata["+$multicrop['currentFileSelector'].attr('name').replace("[]","")+"]["+$index+"]' value='' />");
    if(croptype[$imgcroptype]['aspect'] !== undefined){
	    $allowedaspects = croptype[$imgcroptype]['aspect']; 
	    $orgaspect = $orgX / $orgY;
	    $currentaspect = 2;
	    $.each($allowedaspects ,function(i,v){
	        if( $orgaspect >= $allowedaspects[i] && $orgaspect < $allowedaspects[i+1])
	        {
	            $currentaspect = $allowedaspects[i];
	        }
	    });
    }
    else
    {
    	$currentaspect =  (croptype[$imgcroptype]['size'][0] / croptype[$imgcroptype]['size'][1]);
    }
    
    $imgObj.Jcrop({
        minSize: [32, 32], // min crop size
        maxSize: [1000,1000],
        aspectRatio : $currentaspect, // keep aspect ratio 1:1
        bgFade: true, // use fade effect
        bgOpacity: .3, // fade opacity
        setSelect:[0,0,$orgX,$orgY],
        onChange : updateCordinate,
        onSelect : updateCordinate
        }, function(){

        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];

        jcrop_api = this;
    });
    $(".multicropper").on("mouseenter",".jcrop-holder",function(){
        $(".jcrop-holder").each(function(i,v){
            if($(this).is(":visible")){
                $multicrop['currentActive'] = i;
            }
        });
    });
}
function updateCordinate(c){
    console.log('hello');
    $multicrop['coordinates'][$multicrop['currentActive']] = c;
    var d = [];
    d['x'] = c['x'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['y'] = c['y'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['x2'] = c['x2'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['y2'] = c['y2'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['w'] = c['w'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['h'] = c['h'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    var e = d['x']+'_'+d['y']+'_'+d['x2']+'_'+d['y2']+'_'+d['w']+'_'+d['h'];
    $("#multicropdata_"+$multicrop['currentFileSelector'].attr('id')+"_"+$multicrop['currentActive']).val(e);
}
function fileSelectHandler() {

    // get selected file
    console.log($currentselector);
    var oFile = $currentselector[0].files[0];
    $('.error').hide();

    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (! rFilter.test(oFile.type)) {
        $('.error').html('Please select a valid image file (jpg and png are allowed)').show();
        return;
    }

    // check for file size
    if (oFile.size > 250 * 1024) {
        //$('.error').html('You have selected too big file, please select a one smaller image file').show();
        //return;
    }

    // preview element
    var oImage = document.getElementById('cropper');

    // prepare HTML5 FileReader
    var oReader = new FileReader();
        oReader.onload = function(e) {
        oImage.src = e.target.result;
        oImage.onload = function () { // onload event handler
            
            $orgwidth = oImage.naturalWidth;
            $orgheight = oImage.naturalHeight;
            $scaledwidth = $orgwidth;
            $scaledheight= $orgheight;
            if($orgwidth > 500) {
                $scaledwidth = 500;
                $scaledheight = $scaledwidth * $orgheight / $orgwidth;
            }
            $('#cropper').width($scaledwidth);
            $('#cropper').height($scaledheight);
            
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
            }
            var twidth = $currentselector.attr('org-width');
            var theight = $currentselector.attr('org-height');
            var aspect = twidth / theight;
            var marginwidth = 10 * twidth /100;
            var marginheight = 10 * theight /100;
            var setselect = [10,10,oImage.naturalWidth-10,oImage.naturalHeight-10];
            setTimeout(function(){
                $('#cropper').Jcrop({
                    minSize: [32, 32], // min crop size
                    maxSize: [1000,1000],
                    aspectRatio : aspect, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    setSelect:setselect,
                    onChange : updatePreview,
                    onSelect : updatePreview
                    }, function(){

                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    jcrop_api = this;
                });
            },1000);

        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}
function updatePreview(c) {
    if(parseInt(c.w) > 0) {
        // Show image preview
        var imageObj = $("#cropper")[0];
        var canvas = $("#preview")[0];
        var context = canvas.getContext("2d");
        canvas.width = $currentselector.attr('org-width');
        canvas.height = $currentselector.attr('org-height'); 
        context.drawImage(imageObj, c.x * $orgwidth/$scaledwidth, c.y * $orgheight/$scaledheight, c.w * $orgwidth/$scaledwidth, c.h * $orgheight/$scaledheight, 0, 0, canvas.width, canvas.height);
    }
    $("#crop-done").removeClass('hide');
};

function ajaxUpload(){
    var canvas = $("#preview")[0];
    var dataURL = canvas.toDataURL();
    var pictype = $currentselector.attr('pictype');
    $.post(root_path+'PSAjax.php?type=imageuploadajax', {imgBase64: dataURL,pictype:pictype}, function(data){ 
        $res = $.parseJSON(data);
        if($currentselector.attr('org-selector') !== undefined)
        $($currentselector.attr('org-selector')).attr('src',upload_path+$res['data']);
        $(".jcrop-holder").remove();
        $("#cropper").removeAttr("src");
        $("#crop-done").addClass('hide');
        $("#modal-box").modal("hide");
    });
}
function filetopost(){
    var canvas = $("#preview")[0];
    var dataURL = canvas.toDataURL();
    $("#"+$currentselector.attr('id')+'_post').remove();
    $currentselector.closest('form').append(
        $('<input>').attr({
            type: 'hidden',
            id: $currentselector.attr('id')+'_post',
            name: $currentselector.attr('name')+'_post',
            value:dataURL
        })
    )
    $(".jcrop-holder").remove();
    $("#cropper").removeAttr("src");
    $("#crop-done").addClass('hide');
    $("#modal-box").modal("hide");
    
}
function iframeLoaded() {
    var iFrameID = document.getElementById(internaliframid);
    if(iFrameID) {
          // here you can make the height, I delete it first, then I make it again
          iFrameID.height = "";
          iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
    }   
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function customToggle($selector){
    $temp = $($selector);
    if($temp.hasClass("hide")){
        $temp.removeClass(("hide"));
    }
    else if($temp.hasClass("show")){
        $temp.removeClass(("show"));
    }
    else
    $($selector).toggle();
}
function javascriptpost(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}


function timeSince(ts){
    now = new Date();
    ts = new Date(ts*1000);
    var delta = now.getTime() - ts.getTime();
    delta = delta/1000; //us to s
    var ps, pm, ph, pd, min, hou, sec, days;

    if(delta<=59){
        ps = Math.floor((delta>1)) ? "s": "";
        return "just now"
    }

    if(delta>=60 && delta<=3599){
        min = Math.floor(delta/60);
        sec = Math.floor(delta-(min*60));
        pm = (min>1) ? "s": "";
        ps = (sec>1) ? "s": "";
        //return min+" minute"+pm;
        return min+" minute"+pm+" ago";
    }

    if(delta>=3600 && delta<=86399){
        hou = Math.floor(delta/3600);
        min = Math.floor((delta-(hou*3600))/60);
        ph = (hou>1) ? "s": "";
        pm = (min>1) ? "s": "";
        return hou+" hour"+ph+" ago";
    } 

    if(delta>=86400){
        days = Math.floor(delta/86400);
        hou =  Math.floor((delta-(days*86400))/60/60);
        pd = (days>1) ? "s": "";
        ph = (hou>1) ? "s": "";
        return days+" day"+pd+" ago";
    }

}
function fetchExtUrlMeta($url,$callback){
    $x = encodeURI('select * from html where url="'+$url+'" and xpath in ("//title","//meta","//img","//h1","//h2")');
    $fetchurl = 'http://query.yahooapis.com/v1/public/yql?q='+$x+'&format=json&diagnostics=true&callback=?';
    $fetchedUrl = $.getJSON($fetchurl,function(data){
        $meta = data['query']['results']['meta'];
        $metalist = {};
        if(data['query']['results']['title']['content'] !== undefined)
        $metalist['title'] = data['query']['results']['title']['content'];
        else
        $metalist['title'] = data['query']['results']['title'];
        
        $metalist['img'] = data['query']['results']['img'];
        
        $.each($meta ,function(i,v){
            if(v['name']!== undefined)
            $temp = 'name';
            else if(v['property'] !== undefined)
            $temp = 'property';
            if($metalist[v[$temp]] === undefined)
            {
            if(v['content'] !== undefined)
            $metalist[v[$temp]] = v['content'];
            else if(v['value'] !== undefined)
            $metalist[v[$temp]] = v['value'];
            }
        });
        if($metalist['og:image']!==undefined && $metalist['og:image']!='')
        $metalist['pageimage'] = $metalist['og:image'];
        else if($metalist['twitter:image']!==undefined  && $metalist['twitter:image']!='')
        $metalist['pageimage'] = $metalist['twitter:image'];
        else
        $metalist['pageimage'] = $metalist['img'][0]['src'];
        if($metalist['description'] == undefined || $metalist['description'] == '')
        {
            if($metalist['og:description']!==undefined && $metalist['og:description']!='')
            $metalist['description'] = $metalist['og:description'];
            else if($metalist['twitter:description']!==undefined  && $metalist['twitter:description']!='')
            $metalist['description'] = $metalist['twitter:description'];
            else
            $metalist['description'] = '';
        }
        if($metalist['og:video:url']!==undefined)
        {
            if($url.search('youtube.com')>-1)
            {
                $videoid = youtube_parser($url);
                $metalist['pageimage'] = 'http://i.ytimg.com/vi/' + $videoid + '/sddefault.jpg';
                $metalist['og:video:url'] = "https://www.youtube.com/embed/" + $videoid + "?autoplay=1&autohide=1";
            }
        }
        var $temp = document.createElement('a');
        $temp.href = $url;
        $metalist['host'] = $temp.hostname;
        $metalist['url'] = $url;
        
        window[$callback]($metalist);
    });
}
function youtube_parser(url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11)? match[7] : false;
}
function prepare_youtube_frames(){
    $(".flexi-video").each(function() {
        if($(this).has('.play').length == 0)
        $(this).append($('<div/>', {'class': 'play','vsrc':$(this).attr('vsrc')}));
    
        $(document).delegate('.play', 'click', function() {
            var iframe_url = $(this).attr('vsrc');
            if ($(this).data('params')) iframe_url+='&'+$(this).data('params');
            iframe_url += iframe_url+"&modestbranding=1&rel=0&autoplay=1&autohide=1";
            var iframe = $('<iframe/>', {'frameborder': '0', 'src': iframe_url, 'width': $('.flexi-video').width(), 'height': $('flexi-video').height(),allowfullscreen:true })
            $(this).replaceWith(iframe);
        });
    });
}
function isEnterPressed(e){
	var keycode=null;
	if (e!=null){
		if (window.event!=undefined){
			if (window.event.keyCode) keycode = window.event.keyCode;
			else if (window.event.charCode) keycode = window.event.charCode;
		}else{
			keycode = e.keyCode;
		}
	}
	if(keycode == 13 && !(e.shiftKey ||e.ctrlKey)){
		e.preventDefault();
		return true;
	}
	else
		return false;
}

function copyToClipboard(content) {
	console.log('asdasd');
	console.log(content);
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(content).select();
    document.execCommand("copy");
    $temp.remove();
}