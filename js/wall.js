function initialisewall(){
	$(".wall,.postdetail").on("click",".post-ctrl",function(event){
        event.preventDefault();
        event.stopPropagation();
        $temp = $(this);
        $temp.parents(".post").find('.'+$temp.attr('data-trgt')).toggleClass('hide');
	if((!($temp.hasClass('commentsAdded'))) && $temp.attr('data-trgt') == 'commentbox')
        {
            $id = $temp.attr('data-post_id');
            $comment_on = "post";
            $params = {id:$id,comment_on:$comment_on};
            $.get(root_path+'PSAjax.php?type=getcomments',$params,function(data){
                $comments = $.parseJSON(data);
                $commentshtml = '';
                $.each($comments['list']['comments'],function(i,v){
                    $commentshtml += '<div class="notif_row nobg_border"> ' + 
                            '<div class="usr_dp"><img src="'+v['userdp']+'"></div> ' +
                            '<div class="user_comment"><span class="gen_usr_nm">'+v['name']+'</span> '+v['comment']+' </div>' +
                        '</div>';
                });
		addScrollbar($temp.parents(".post").find(".all_comments_container").html($commentshtml).addClass('scroll-content'));
		$temp.addClass('commentsAdded');
                $temp.parents(".post").find('.comment_tarea').on('change keyup keydown paste cut',function(){$(this).height(0).height(this.scrollHeight);});
            });    
        }
    });
    $(".wall,.postdetail").on('click','.post-edit-btn',function(event){
        event.preventDefault();
        $temp = $(this).parents(".post").find(".post-content");
        if($temp.has("textarea").length == 0)
        $temp.html('<textarea rows="" onkeypress="if(isEnterPressed(event)){post_edit(this)}" cols="" placeholder="Edit Content" comment_on="post" data-post_id="'+$(this).attr('data-post_id')+'" class="comment_tarea">'+$temp.text()+'</textarea>');
        $temp.find("textarea").focus().on('change keyup keydown paste cut',function(){$(this).height(0).height(this.scrollHeight);});
    });
    $(".wall,.postdetail").on('click','.commentbox',function(event){
        event.stopPropagation();
    });
    
    $(".wall,.postdetail").on('keypress','.commentbox .comment_tarea',function(event){
        event.stopPropagation();
	if(isEnterPressed(event)){
		post_add_comment(this)
	};
    });
    
    $(".wall,.postdetail").on('click','.shareonemail',function(event){
	event.stopPropagation();
	$(this).parents(".sharebox").find(".email-share").toggleClass("hide");
    });
    $(".wall,.postdetail").on('click','.sharepost',function(event){
       $temp = $.trim($(this).parents('.post').find('.tags').text());
       var nbsp = String.fromCharCode(160);
       var tags = $temp.split('#');
       $temp = {};
       $temp2 = $(this).parents('.post')
       $temp['title'] = $temp2.find('.post-content').text();
       $temp['description'] = $temp2.find('.metadata .user').text();
       // $temp['description'] = $temp2.find('.post-content').text();
       // $temp['title'] = $temp2.find('.metadata .user').text();
       if($temp2.find('.img_set img').length > 0)
       $temp['image'] = $temp2.find('.img_set img').eq(0).attr('src');
       $temp['url'] = $(this).attr('data-shareurl');
       $temp['tags'] = tags;
       $isdialog = ($(this).data('dialog'))?$(this).data('dialog'):0;
       sharepage($(this).attr('data-type'),$temp,$isdialog); 
    });
    
    $(".wall,.postdetail").on('click','.sharepostcustom',function(event){
            $(this).next('ul').slideToggle(200);
    });
    $(".wall,.postdetail").on('click','.sharepostonemail',function(){
            $parent = $(this).parent();
            $temp = $(this).parents(".email-share").find("textarea").val();
            if($temp){
                    $.post(root_path+'PSAjax.php?type=sharepostonemail', {emails:$temp,post_id:$(this).attr('data-post_id'),boxid:$(this).attr('id')}, function(data){ 
                            $res = $.parseJSON(data);
                            $parent.append('<div>'+$res['data']+'</div>');
                    });
            }
    });
    
    $(".wall,.postdetail").on('click','.sharepage',function(){
       sharepage($(this).attr('data-type'),''); 
    });
    $(".wall,.postdetail").on('click','.copybtn',function(){
            copyToClipboard($(this).attr('content'));
    });
}

function wallpaging(){
	$('.wall,.postdetail').cleverInfiniteScroll({
	    contentsWrapperSelector: '.wall .main-content',
	    contentSelector: '.inner',
	    nextSelector: '#nextwall',
	});
}
function post_delete(data){
    if(status_codes[data['status']] == 'Success');
    {
        if(data['boxid'] != '')
        {
            $("#"+data['boxid']).text(data['data']);
            $("#"+data['boxid']).delay(2000).hide(0);
        }
    }
}
function post_edit(o){
    $temp = $(o);
    var $params = {};
    $type = "post-edit";
    $params['boxid'] = "";
    $params['post_id'] = $temp.attr('data-post_id');
    $params['content'] = $temp.val();
    $.post(root_path+'PSAjax.php?type='+$type, $params, function(data){ 
        $temp = $.parseJSON(data);
        $("#post_"+$params['post_id']).find('.post-content').html($temp['data']);
    });
}
function post_add_comment(o){
    $tarea = $(o);
    $id = $tarea.attr("data-post_id");
    $comment_on = $tarea.attr("data-comment_on");
    $comment = $tarea.val();
    $params = {id:$id,comment_on:$comment_on,comment:$comment};
    $.post(root_path+'PSAjax.php?type=addcomment', $params, function(data){ 
        $comment = $.parseJSON(data);
        $commentshtml = '';
        var v = $comment['list']['comment'];
        $commentshtml += '<div class="notif_row nobg_border"> ' + 
                            '<div class="usr_dp"><img src="'+v['userdp']+'"></div> ' +
                            '<div class="user_comment"><span class="gen_usr_nm">'+v['name']+'</span> '+v['comment']+' </div>' +
                        '</div>';
        if(status_codes[$comment['status']] == 'Success')
        $("#post_"+$params['id']).find('.all_comments_container').append($commentshtml);
        $("#post_"+$params['id']).find(".comment_form textarea").val('');
    });
}