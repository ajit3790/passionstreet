function showquestions1(){
	$html = '';
		$.each(questions,function(i,v){
			$html += '<table width="100%" class="event-signup-form">';
			$html +=   '<tr><td><div class="row">'+v['question']+'</div></td></tr>';
			if(v['questiontype'] == 'multipleoption'){
				$html +=   '<tr><td><div class="row">';
				$html += '<select class="form-control" name="test>';
				$html += '<option>Select</option>';
				$.each(v['questionoptions'],function(i2,v2){					
					$html += '<option value="'+v2+'">'+v2+'</option>';
					
				});
				$html += '</select>';
				$html +=   '</div></td></tr>';
			}
			else if(v['questiontype'] == 'text')
			{
				$html +=   '<tr><td><div class="row">'+
								  '<textarea class="form-control" width="100%" style="height:35px" required="" name="test"></textarea>'+
							'</div></td></tr>';
			}
			else
			{
				$html +=   '<tr><td><div class="row">'+
								  '<input type="file" class="form-control filestyle" value="" required="" name="test">'+
							'</div></td></tr>';
			}
			$html +=   '</table>';
		});
		return $html;
}
function showquestions(x,y){
	$html = '';
	var questionss = {};
	questionss = questions;
	if(ticketing[x]['iscombo'] == 1 && ticketing[x]['type'] == 2)
	questionss.unshift({'questiontype':'text','question':'Team / Company Name'});
		$.each(questionss,function(i,v){
			$html += '<div>';
			$html +=   '<div class="row form-group">';
			$html +=   '<div class="col-md-6">'+v['question']+'</div>';
			$html +=   '<div class="col-md-6">';
			if(v['questiontype'] == 'multipleoption'){
				$html += '<select class="form-control" id="answerss_'+y+'_'+i+'" name="event[answers]['+x+']['+y+']['+i+']">';
				$html += '<option value="">Select</option>';
				$.each(v['questionoptions'],function(i2,v2){					
					$html += '<option value="'+v2+'">'+v2+'</option>';
					
				});
				$html += '</select>';
			}
			else if(v['questiontype'] == 'text')
			{
				$html +=   '<textarea class="form-control" id="answerss_'+y+'_'+i+'" width="100%" style="height:30px;overflow:hidden" required="" name="event[answers]['+x+']['+y+']['+i+']"></textarea>';
			}
			else
			{
				$html +=   '<input type="file" placeholder="'+v['question']+'" class="form-control filestyle" id="answerss_'+y+'_'+i+'" value="" required="" name="event[answers]['+x+']['+y+']['+i+']">';
			}
			$html +=   '</div>';
			$html +=   '</div>';
			$html +=   '</div>';
		});
		return $html;
}
function showquestionaire(){
    $(".eventbooking").removeClass('hide');
	if(!(questions))
	{
		processtopayment();
		return;
	}
	$usercount = 0;
    $(".cartdiv .ticketselector").each(function(i,v){
        $usercount = $usercount +  parseInt($(this).val());
    });
	$x = '<div class="widget-heading"><h2 class="hstyl_1 sm bx">Event Signup Form</h2></div><div id="questionss" class="bx-styl form1 carousel slide" data-ride="carousel"><div class="carousel-inner" role="listbox">';
    // i = 1;
    // v = questions;
	for($k=0;$k<$usercount;$k++)
	{
		$html = showquestions();
		$x += '<div style="margin-top:0px;" class="questionairediv item '+(($k==0)?'active':'')+'" >'+
			$html;
			if($k < ($usercount - 1))
			{
				$x += '<table width="100%">'+
						'<tr>'+
								'<td class="text-right footer"><button class="btn btn-primary nextquestion">Next</button></td>'+
						'</tr>'+
					'</table>';
			}
			else
			{
				$x += '<table width="100%">'+
						'<tr>'+
								'<td class="text-right footer"><button class="btn btn-primary" onclick="processtopayment();">Proceed to Payment</button></td>'+
						'</tr>'+
					'</table>';
			}
		$x += '</div>';
	}
	$x +='</div></div>';
	//$(".eventbooking").html($x);
	$(".eventbooking .eventbookingsteps").addClass('hide');
    $(".eventbooking .questionaire").html($x);
	$(".eventbooking .questionaire").removeClass('hide');
	$("#questionss").carousel({
		interval:false
	});
	$("#questionss").carousel('pause');
	$("#questionss .nextquestion").click(function(){
		$("#questionss").carousel('next');
	});
}
function showTicketCount(ticketid){
	var $ticket = ticketing[ticketid];
	if($ticket['iscombo'] && $ticket['iscombo']==1)
	var $x = '<select class="form-control ticketselector" data-price="'+($ticket['price'] / $ticket['combosize'])+'" data-orgprice="'+($ticket['price'] / $ticket['combosize'])+'" data-ticket="'+$ticket['id']+'">';
	else
	var $x = '<select class="form-control ticketselector" data-price="'+$ticket['price']+'" data-orgprice="'+$ticket['price']+'" data-ticket="'+$ticket['id']+'">';
	$ticketminbuy = $ticket['minbuy'];	
	$ticketmaxbuy = $ticket['maxbuy'];
	if(!($ticketminbuy))
	$ticketminbuy = 1;
	if(!($ticketmaxbuy))
	$ticketminbuy = 10;
	$x += "<option value='0'>0</option>";
	if($ticket['iscombo'] && $ticket['iscombo']==1)
	{
		$x += "<option value='"+$ticket['combosize']+"'>1</option>";
	}
	else
	{
		for($i=$ticketminbuy;$i<=$ticketmaxbuy;$i++)
		{
			$x += "<option value='"+$i+"'>"+$i+"</option>";
		}
	}
	$x +='</select>';
	return $x;
}
function showdatetimeslots(){
    addToLocalObject(q['event_id'],'showcart',1);
    scrollTo('.eventdetail');
    if($(".datetimeradio").length > 1)
    {
        $(".eventbooking .eventbookingsteps").addClass('hide');
        $(".eventbooking .datetimeslots").removeClass('hide');
        $(".eventbooking").removeClass('hide');
    }
    else
    showcart();
}
function showcart(){
    /*$ls = getLocalStorage(q['event_id']);
    if($ls && $ls['showcart'] == 1)
    {
	
    }*/
    addToLocalObject(q['event_id'],'showcart',1);
    scrollTo('.eventdetail');
    $(".eventbooking").removeClass('hide');
    $(".questionairediv").addClass('hide');
    $x = '<div class="widget-heading"><h2 class="hstyl_1 sm">Select Ticket</h2></div>';
    $html = '';
    $.each(ticketing,function(i,v){
        $html += '<tr class="ticketcategoryrow" data-ticket="'+i+'">'+
                    '<td>'+v['name']+'</td>'+
                    '<td class="price"><span class="orgprice hide strikethrough"><span>₹ '+v['price']+'</span></span><span class="userprice">₹ '+v['price']+'</span></td>'+
                    /*'<td class="qty"><input type="number" min="0" step="1" placeholder="0" data-price="'+v['price']+'" data-ticket="'+v['name']+'" value="0" required="" class="form-control ticketselector"></td>'+*/
					'<td class="cross">X</td>'+
                    '<td class="qty">'+showTicketCount(i)+'</td>'+
                    //'<td class="amount ticketvaluedisplay">0</td>'+
                 '</tr>';
    });
    $x += '<div class="bx-styl form1 cartdiv">'+
                '<table width="100%" class="event-signup-form">'+
                   // '<tr>' +
                    //    '<th>Ticket Type</th>'+
                    //    '<th>Rate / ticket</th>'+
                   //     '<th class="qty">Qty</th>'+
                   //     '<th class="amount">Amount</th>'+
                   // '</tr>' +
                    $html +
                    '<tr class="tickettotal">'+
                        '<td class="total"></td>'+
                        '<td class="total" colspan="3">'+
                                '<table>'+
                                        '<tbody><tr>'+
                                                '<td>Total</td>'+
                                                '<td class="amount">₹ <span class="ticketprice">0</span></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                                '<td>Processing fee</td>'+
                                                '<td class="amount">₹ <span class="ticketconv">0</span></td>'+
                                        '</tr>'+
                                        '<tr class="grand-total">'+
                                                '<td><span class="totaltext">Grand Total</span></td>'+
                                                '<td class="amount">₹ <span class="ticketgrandtotal">0</span></td>'+
                                        '</tr>'+
                                        '<tr class="grand-discount hide">'+
                                                '<td>Saved</td>'+
                                                '<td class="amount">₹ <span class="ticketdiscounttotal">0</span></td>'+
                                        '</tr>'+
                                        '<tr><td colspan="2" class="coupon-code">'+
                                            '<input placeholder="Have Promo Code?"  type="text" class="form-control" id="couponcode" name="couponcode" />'+
                                            '<input type="hidden" class="form-control" id="pricedistribution" name="pricedistribution" />'+
                                            '<button class="btn" onclick="applyCoupon();">Apply</button></td>'+
                                        '</tr>'+
                                '</tbody></table>'+
                        '</td>'+
                    '</tr>'+
                    '<tr>'+
                            '<td class="text-right footer" colspan="4"><button class="btn btn-primary" onclick="addAttendees();">Proceed</button></td>'+
                    '</tr>'+
                '</table>'+
        '</div>';
    //$(".eventbooking").html($x);
	$(".eventbooking .eventbookingsteps").addClass('hide');
    $(".eventbooking .cart").html($x);
	$(".eventbooking .cart").removeClass('hide');
}
function addAttendees(){
    $ticketcount = {};
    $ticketcount2 = [];
	$i = 0;
    $(".cartdiv .ticketselector").each(function(i,v){
        $tickettype = $(this).attr('data-ticket');
        $ticketcount[$tickettype] = parseInt($(this).val());
		for($x=0;$x<$ticketcount[$tickettype];$x++)
		{
			$ticketcount2.push($tickettype);
		}
    });
	if($ticketcount2.length == 0)
	{
		scrollTo('.eventdetail');
		showToolTipMsg($(".cartdiv .ticketselector").eq(0),"Please select atleast 1 ticket");
		return;
    }
	$(".eventbooking").removeClass('hide');
    $(".questionairediv").addClass('hide');
    $(".cartdiv").addClass('hide');
    
    addToLocalObject(q['event_id'],'ticketcount',$ticketcount);
    
    $x = '<div class="widget-heading"><h2 class="hstyl_1 sm ">Update Participants Detail</h2></div>';
    $html = '';
    /*$.each($ticketcount,function(i,v){
        for(j=1;j<=v;j++){
        if(j==1)
        $html += '<tr><td>'+i+'</td>';
        else
        $html += '<tr><td></td>';
        $html +=    '<td><input type="text" placeholder="Name" name="event[ticketing]['+i+']['+j+'][name]" required="" class="form-control half" ><input type="email" name="event[ticketing]['+i+']['+j+'][email]" placeholder="Email"  class="form-control half" ></td>'+
                    '<td class="gender">'+
                            '<div class="radio">'+
                                    '<label>'+
                                            '<input type="radio" value="M" required="" name="event[ticketing]['+i+']['+j+'][gender]" checked="checked">'+
                                            'M'+
                                    '</label>'+
                                    '<label>'+
                                            '<input type="radio" value="F" required="" name="event[ticketing]['+i+']['+j+'][gender]">'+
                                            'F'+
                                    '</label>'+
                            '</div>'+
                    '</td>'+
            '</tr>';
        }
    });*/
	/*
	$.each($ticketcount,function(i,v){
        for(j=1;j<=v;j++){
        if(j==1)
        $html += '<tr><td colspan=4>'+i+'</td></tr>';
        else
        $html += '<tr>';
        $html +=    '<td><input type="text" placeholder="Name" name="event[ticketing]['+i+']['+j+'][name]" required="" class="form-control half name" ><input type="email" name="event[ticketing]['+i+']['+j+'][email]" placeholder="Email"  class="form-control half email" ></td>'+
                    '<td class="gender">'+
                            '<div class="radio">'+
                                    '<label>'+
                                            '<input type="radio" value="M" required="" name="event[ticketing]['+i+']['+j+'][gender]" checked="checked gender">'+
                                            'M'+
                                    '</label>'+
                                    '<label>'+
                                            '<input type="radio" value="F" required="" name="event[ticketing]['+i+']['+j+'][gender]" class="gender">'+
                                            'F'+
                                    '</label>'+
                            '</div>'+
                    '</td>'+
					'<td><input type="text" name="event[ticketing]['+i+']['+j+'][dob]" placeholder="Date of Birth"  class="form-control dob" ></td>'+
            '</tr>';
        }
    });
	*/
	$html = '<div id="participantss" class="form1 carousel slide" data-ride="carousel"><div class="carousel-inner" role="listbox">';
	$ticketlength = $ticketcount2.length;
	for(j=0;j<$ticketlength;j++)
	{
		i = $ticketcount2[j];
                var genderstr = {};
                genderstr['male'] = 'checked = "checked"';
                genderstr['female'] = '';
                if(ticketing[i]['type'] == 3)
                {
                    if(j == 0)
                    {
                    genderstr['male'] = 'checked = "checked" onclick="javascript: return false;"';
                    genderstr['female'] = 'onclick="javascript: return false;"';
                    }
                    else if(j == 1)
                    {
                    genderstr['female'] = 'checked = "checked" onclick="javascript: return false;"';
                    genderstr['male'] = 'onclick="javascript: return false;"';
                    }
                }
		$html +='<div id="participantbox'+j+'" style="margin-top:0px;" class="item '+((j==0)?'active':'')+'">'+
					'<h4>Participant '+(j+1)+' # '+ticketing[i]['name']+'</h4>'+
					'<div class=" row"><div class="col-sm-6 form-group"><input type="text" placeholder="Name" name="event[ticketing]['+i+']['+j+'][name]" required="" class="form-control name" /></div><div class="col-sm-6 form-group"><input type="email" name="event[ticketing]['+i+']['+j+'][email]" placeholder="Email"  class="form-control email" /></div></div>'+
					'<div class="row form-group"><div class="col-xs-6">Gender</div><div class="radio col-xs-3"><label class="">'+
							'<input type="radio" value="M" required="" name="event[ticketing]['+i+']['+j+'][gender]" class="gender" '+genderstr['male']+' />'+
							'Male'+
					'</label></div>'+
					'<div class="radio col-xs-3"><label class="">'+
							'<input type="radio" value="F" required="" name="event[ticketing]['+i+']['+j+'][gender]" class="gender" '+genderstr['female']+'/>'+
							'Female'+
					'</label></div></div>';
					
		//$html +=	'<div class="form-group"><input type="text" name="event[ticketing]['+i+']['+j+'][dob]" placeholder="Date of Birth"  class="form-control dob" /></div>';
		if(ticketing[i]['requiremobile'] == 1)
		$html +=	'<div class="form-group row"><div class="col-md-6 c-label">Mobile Number</div><div class="col-md-6"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="event[ticketing]['+i+']['+j+'][mobile]" placeholder=""  class="form-control mobile" /></div></div>';
		
		$html +=	'<div class="row form-group dob dobdropdown"><div class="col-xs-12 col-sm-6 c-label">Date of Birth</div><div class="col-sm-2 col-xs-4"><select class="dobday form-control"></select></div>  <div class="col-sm-2 col-xs-4"><select class="dobmonth form-control"></select></div>  <div class="col-sm-2 col-xs-4"><select class="dobyear form-control" data-dobminage="'+ticketing[i]['agerestrict']+'"></select></div><input type="hidden" name="event[ticketing]['+i+']['+j+'][dob]" class="dobtext"></div>';
		
		$html += showquestions(i,j);			
		if(j==($ticketlength-1))			
		$html +='<div class="form-group text-center"><label for="paymenttermscheck" style="padding-top:10px;" >By clicking proceed button you are agreeing to both Passionstreet and this Event Terms &amp; Conditions. </label></div><div class="form-group text-center"><input type="button" class="btn btn-primary" id="paybutton" onclick="processtopayment();return false;" value="Proceed to Payment" /><div>';
		else
		$html +='<div class="text-right"><input type="button" class="btn btn-primary nextparticipant" value="Next" /></div>';
		$html +='</div>';
	};
		
	$html += '</div></div>'
    $x += '<div class="bx-styl form1 cartdiv1">'+
                '<form method="post" class="eventform">'+
                '<input type="hidden" name="event[productid]" value="'+eventid+'">'+
                '<input type="hidden" name="event[producttype]" value="event">'+
                '<input type="hidden" name="event[producturl]" value="'+orgurl+'">'+
                '<input type="hidden" name="couponcode" value="'+$("#couponcode").val()+'">'+
                '<input type="hidden" name="pricedistribution" value="'+$("#pricedistribution").val()+'">'+
                '<input type="hidden" name="timeslotid" value="'+$(".datetimeslots .datetimeradio").val()+'">'+
                '<div class="event-signup-form" id="event-signup-form">'+
                    /*'<tr>'+
                            '<th>Ticket Type</th>'+
                            '<th>Participants\' Details</th>'+
                            '<th>Gender</th>'+
                    '</tr>'+*/
                    $html +
                '</div>'+
                '</form>'+
        '</div>';
    //$(".eventbooking").html($x);
	$(".eventbooking .eventbookingsteps").addClass('hide');
    $(".eventbooking .participants").html($x);
	$(".eventbooking .participants").removeClass('hide');
	$("#participantss").carousel({
		interval:false
	});
	$("#participantss").carousel('pause');
	$("#participantss .nextparticipant").click(function(){
		$("#participantss").carousel('next');
	});
	scrollTo('.eventdetail');
	addDobDropdowns(".dobdropdown");
}
function processtopayment(){
    addToLocalObject(q['event_id'],'ticketparticipants',$('.eventform').serializeArray());
    $.post(root_path+'PSAjax.php?type=validateeventbooking', $('.eventform').serializeArray(), function(data){ 
        $res = $.parseJSON(data);
        if($res['status'] == 200)
		{
			processtopaymentsubmit();
		}
		else if($res['status'] == 406)
		{
			scrollTo('.eventdetail');
			if($res['error']['participants'])
			{
				$.each($res['error']['participants'],function(participantseq,data){
					participantseq = parseInt(participantseq);
					$("#participantss").carousel(participantseq);
					$("#participantss").carousel('pause');
					$.each(data,function(type,msg){
						setTimeout(function(){
							showToolTipMsg($("#participantbox"+participantseq+" ."+type).eq(0),msg);
						},1000);
					});
					return false;
				});
			}
			if($res['error']['answers'])
			{
				$.each($res['error']['answers'],function(participantseq,data){
					participantseq = parseInt(participantseq);
					$("#participantss").carousel(participantseq);
					$("#participantss").carousel('pause');
					$.each(data,function(type,msg){
						setTimeout(function(){
							showToolTipMsg($("#answerss_"+participantseq+"_"+type).eq(0),msg);
						},1000);
					});
					return false;
				});
			}
			if($res['error']['existingemailids'])
			{
				$.each($res['error']['existingemailids'],function(participantseq,data){
					participantseq = parseInt(participantseq);
					$("#participantss").carousel(participantseq);
					$("#participantss").carousel('pause');
					$.each(data,function(type,msg){
						scrollTo($("#participantbox"+participantseq+" ."+type).eq(0));
						setTimeout(function(){
							showToolTipMsg($("#participantbox"+participantseq+" ."+type).eq(0),msg);
						},1000);
					});
					return false;
				});
			}
		}
    });
    $('.eventform .email').each(function(){
	$.post(root_path+'PSAjax.php?type=logactivity&email='+$(this).val()+'&eventid='+q['event_id']+'', $('.eventform').serializeArray(), function(data){ 
	});
	    
    });
    return false;
}
function processtopaymentsubmit(){
    addToLocalObject(q['event_id'],'ticketparticipants',$('.eventform').serializeArray());
    minimal_login_callback = processtopayment;
    $.post(root_path+'PSAjax.php?type=eventbooking', $('.eventform').serializeArray(), function(data){ 
        $res = $.parseJSON(data);
        if($res['status'] == 200)
		{
			//$('#paybutton').popover({title: "Payment Instruction", content: "Use the 'Guest Checkout' option on payment page", html: false, placement: "auto"}).on("show.bs.popover", function () { $(this).data("bs.popover").tip().css("max-width", "600px"); });
			$('#paybutton').val('Proceeding to Payment');
			//$('#paybutton').popover('show');
			setTimeout(function(){
				formpost('https://secure.payu.in/_payment',$res['paymentdata']);
			}, 2000);
			addToLocalObject(q['event_id'],'transactionstarted',1);
			
		}
		else if($res['status'] == 406)
		{
			
		}
		else if($res['status'] == 4051)
		{
			setTimeout(function(){
				$("#loginemail").val($("#event-signup-form .email").eq(0).val()); 
				$("#minregistername").val($("#event-signup-form .name").eq(0).val()); 
				$("#minregisteremail").val($("#event-signup-form .email").eq(0).val()); 
				$("#minregisterdob").val($("#event-signup-form .dob").eq(0).val()); 
				$("#minregistergender").val($("#event-signup-form .gender").eq(0).val());
				$("#minregisterpassions").val(passiontype);
				//$("#minregisteremail").val($("#event-signup-form .email").eq(0)); 
				$("#minregisterpassword").val('').attr('placeholder','Create Password'); 
			}, 3000);
		}
	
        //loadPage('payumoney/PayUMoney_form.php');
    });
    return false;
}
function ticketing_popup(){
    $ticketingHtml = "<div class='ticketing'>";
    $.each(ticketing , function(i,v){
        $ticketingHtml +=   "<div class='row ticketdetails'>"+
                                "<div class='tname'>"+v['name']+"</div>"+
                                "<div class='tprice'>"+v['price']+"</div>"+
                                "<div class='tcount'><input type='text' value='<?=$event['peruserticketcount']?>' class='ticketbuycount' /></div>"+
                                "<div class='tadd'><a class='btn btn-primary ticketbuy' event='<?=$event['event_id']?>'> Add </a></div>"+
                            "</div>";
    });
    $ticketingHtml += "</div>";
    $ticketingHtml += "<div class=''><a class='btn btn-primary viewcart callmodaliframe' data-targetsrc='module/cart' > View Cart </a></div>";
    myModal("Event Ticketing",$ticketingHtml);
}
$(document).ready(function(){
    if(isMobile == true)
    {
	$("#tck_dtl").appendTo('.eventbanner');
        $(".countdowntimer").appendTo('.eventbanner');
	$(".orgnzr").appendTo('.eventdetail');
    }
    $("body").on("click",".ticketbuy",function(){
        $temp = $(this);
        $temp2 = $temp.parents(".ticketdetails");
        $teventid = $temp.attr("event");
        $tname = $temp2.find(".tname").text();
        $tprice = $temp2.find(".tprice").text();
        $tcount = $temp2.find(".ticketbuycount").val();
        $params = {productType:'event',productid:$teventid,ticketname:$tname,ticketprice:$tprice,buycount:$tcount};
        $.post(root_path+'PSAjax.php?type=addToCart', $params, function(data){ 
            $temp.html("Added To Cart");
        });
    })
    $("body").on("change",".ticketselector",function(){
        $ticketprice = 0;
        $ticketconvprice = 0;
        $ticketservprice = 0;
        $ticketgrandtotal = 0;
        $tickettotalcount = 0;
        $ticketgranddiscount = 0;
        $selectedtickets = {};
        $(".ticketselector").each(function(){
            $temp = $(this);
            $temp2 = $temp.parents(".ticketcategoryrow");
            $tprice = $temp.attr('data-price');
            $torgprice = $temp.attr('data-orgprice');
            $ttype = $temp.attr('data-ticket'); 
            $tcount = parseInt($temp.val());
            if($tcount > 0)
            {
            $selectedtickets[$ttype] = {};
            $selectedtickets[$ttype]['price'] = $tprice;
            $selectedtickets[$ttype]['count'] = $tcount;
            }
            $tickettotalcount = $tickettotalcount + $tcount;
            $ttotal = $tcount * $tprice;
            $ticketprice = $ticketprice + $ttotal;
            if($tprice != $torgprice)
            $ticketgranddiscount = $ticketgranddiscount + ($tcount * ($torgprice - $tprice));    
        });
        $data = {};
        $data['eventTypeIdbyFees'] = eventTypeIdbyFees;
        $data['eventSelectedTickets'] = $selectedtickets;
        $data['eventFeepaymentoptions'] = eventFeepaymentoptions;
        $.post(root_path+'PSAjax.php?type=eventticketeffectiveprice',$data, function(data){
            $res = $.parseJSON(data);
            $temp = $res['list'];
            $('.ticketprice').html($temp['initialcost']);
            $('.ticketconv').html(round($temp['displaycost'] - $temp['initialcost'],0));
            $('.ticketgrandtotal').html(round($temp['displaycost'],0));
            $('#pricedistribution').val($temp['distribution']);
        });
    });
    $("body").on("change",".ticketselector1",function(){
        $ticketprice = 0;
        $ticketconvprice = 0;
        $ticketservprice = 0;
        $ticketgrandtotal = 0;
        $tickettotalcount = 0;
        $ticketgranddiscount = 0;
        $(".ticketselector").each(function(){
            $temp = $(this);
            $temp2 = $temp.parents(".ticketcategoryrow");
            $tprice = $temp.attr('data-price');
            $torgprice = $temp.attr('data-orgprice');
            $ttype = $temp.attr('data-ticket'); 
            $tcount = parseInt($temp.val());
            $tickettotalcount = $tickettotalcount + $tcount;
            $ttotal = $tcount * $tprice;
            $ticketprice = $ticketprice + $ttotal;
            $temp2.find(".ticketvaluedisplay").html($ttotal);
            if($tprice != $torgprice)
            $ticketgranddiscount = $ticketgranddiscount + ($tcount * ($torgprice - $tprice));    
        });
        $ticketconvprice = round((convenience + service*convenience/100)*$tickettotalcount,0); //conveniece + 15percentage of conveineince
        //$ticketconvprice = round((convenience / 100) * $ticketprice ,2);
        //$ticketservprice = round((service / 100) * $ticketconvprice ,2);
        $ticketgrandtotal = $ticketprice + $ticketconvprice + $ticketservprice;
        $('.ticketprice').html($ticketprice);
        $('.ticketconv').html($ticketconvprice);
        $('.ticketserv').html($ticketservprice);
        $('.ticketgrandtotal').html($ticketgrandtotal);
        if($ticketgranddiscount > 0)
        {
            $(".grand-discount .ticketdiscounttotal").html($ticketgranddiscount);
            $(".grand-discount").removeClass('hide');
            $(".grand-total .totaltext").html('Discounted Total');
        }
        else
        {
            $(".grand-discount .ticketdiscounttotal").html(0);
            $(".grand-discount").addClass('hide');
            $(".grand-total .totaltext").html('Grand Total');
        }
        
    });
    
    $printticket = q['printticket'];
    if($printticket && q['txnid'])
    {
            //callmodaliframe('Downloading Ticket',root_path+'module/print?type=event&subtype=ticket&txnid='+q['txnid'],'Please wait while we try generating your ticket.<br />It will be downloaded on your system, please check your downloads folder');
            callmodaliframe('Printing Ticket',root_path+'module/print?type=event&subtype=ticket&txnid='+q['txnid'],'Please wait while we try generating your ticket.<br />Additionally it will be sent to your registered email id');
    }
});
function applyCoupon(){
    var $tickets = {};
    $(".ticketselector").each(function(i,v){
       $tickets[$(this).data('ticket')] = parseInt($(this).val()); 
    });
    console.log($("#couponcode").val());
    console.log($tickets);
    console.log('**********');
    if($("#couponcode").val().length > 0 && $tickets)
    {
    $.post(root_path+'PSAjax.php?type=applycoupon', {entityId:q['event_id'],entityType:'event',couponCode:$("#couponcode").val(),tickets:$tickets}, function(data){ 
        $res = $.parseJSON(data);
        $.each($res['list'],function(i,v){
            
        });
        
        $(".ticketcategoryrow").each(function(i,v){
           $temp = $(this).attr('data-ticket');
		$price = ticketing[$temp]['price'];
		try{
		if(ticketing[$temp]['iscombo'] == 1)
		{
		$price = $price / ticketing[$temp]['combosize'];
		}
		}
		catch(e){
		}
               $discount = 0;
               if(typeof $res['list']['All'] != 'undefined')
               $res['list'][$temp] = $res['list']['All'];
               if(typeof $res['list'][$temp] != 'undefined')
               {
                    if($res['list'][$temp]['couponType'] == 'Percentage')
                    {
                        $discount = round(($price * parseInt($res['list'][$temp]['couponDiscount']) / 100),0);
                    }
                    else if($res['list'][$temp]['couponType'] == 'Fixed')
                    {
                        $discount = round(parseInt($res['list'][$temp]['couponDiscount']),0);
                    }
                    $(this).find('.price .orgprice').removeClass('hide');
               }
               else
               {
                   $(this).find('.price .orgprice').addClass('hide');
               }
               $newPrice = $price - $discount;
               var $displayprice = $newPrice;
               try{
		if(ticketing[$temp]['iscombo'] == 1)
		{
		$displayprice = $displayprice * ticketing[$temp]['combosize'];
		}
		}
		catch(e){
		}
               
               $(this).find('.price .userprice').html('₹ '+$displayprice);
               $(this).find('.ticketselector').attr('data-price',$newPrice).trigger('change');
           
        });
    });
    }
}