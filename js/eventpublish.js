$(document).ready(function(){


    $("#invitebtn").on('click',function(){
        $("#invitebox").removeClass('hide');
        $("#inviteboxinner").html(calliframe("module/invite?relatedto=<?=$event['event_id']?>&relatedtotype=event"));
    });
	
	$("#event_precondition").on('click',function(){
		if($(this).val() == 'Yes'){
			$("#preconditionbox").removeClass('hide');
			$("#preconditioninner").html(calliframe("module/questionsadd?questiontype=eventquestionaire&relatedto="+q['event_id']));
		}
	});
	if($("#event_precondition").is(":checked"))
	{
		$("#event_precondition").trigger('click');
	}

    $(".ticketcountradio").on('click',function(){
        if($(this).val() == 'multi'){
            $(".ticketcountdefine").removeClass('hide');
            $("#ticketcount").val('1');
            $(".ticketcounttext").val('1');
        }
        else{
            $(".ticketcountdefine").addClass('hide');
            $("#ticketcount").val('1');  
            $(".ticketcounttext").val('1');
        }
    })
    $(".ticketcounttext").on('keyup keypress blur change',function(){
        if($.trim($(this).val())!='')
            $("#ticketcount").val($.trim($(this).val())); 
        else{
            $("#ticketcount").val('0');
        }
    });
    
    $(".eventtypebyfees").on('click',function(){
        if($(this).attr('data-value') == 'paid'){
            getBankAccountsList();
            $(".bank-account").removeClass('hide');
            //$(".tickets-total-count").addClass('hide');
            $(".tickets-price").removeClass('hide');
            $(".terms").removeClass('hide');
            $(".submitform").removeClass('hide');
        }
        else if($(this).attr('data-value') == 'free'){
            $(".bank-account").addClass('hide');
            //$(".tickets-total-count").removeClass('hide');
            $(".tickets-price").addClass('hide');
            $(".terms").addClass('hide');
            $(".submitform").removeClass('hide');
        }
    });
    
    $(".more-ticket-categories").on('click',function(){
        //$(".ticket-price-row-box").append($(".ticket-price-row").eq(0).wrapAll('<div>').parent().html());
        $(".ticket-price-row").eq(0).clone().appendTo(".ticket-price-row-box").find("input[type='text']").val("");
    });
    
    $(".event_include_routemap").click(function(){
        
        if($(this).val() == 'Yes'){
            $(".event_routemap").removeClass("hide");
	    $("#waypoints").html('');
	    initGoogleMapsRouteMapCreate();
        }
        else{
            $(".event_routemap").addClass("hide");
        }
    });
    
    $("#addNewBackAccount").click(function(){
    	$(".addNewBackAccountForm").html(calliframe('module/bankaccountadd'));
    });
    $('#showpagelist :radio').click(function(){
	    return false;
    });
    $('#showpagelist').popover({title: "Select from My Active Pages", content: populatePageList(), html: true, placement: "auto"}).on("show.bs.popover", function () { $(this).data("bs.popover").tip().css("max-width", "600px"); });;
    $('body').on('click','.selectpageasorganiser',function(event){
	$pageId = $(this).attr('id').replace('pg_','');
        if(typeof pageContact != "undefined")
	populateOrganiserForm(pageContact[$pageId]);
	$('#showpagelist :radio').prop("checked","checked");
	$('#showpagelist').trigger('click');
	event.preventDefault();
    });
    $("#userasorganiser").click(function(){
	populateOrganiserForm(userContact[profile_id]);
    });
    
    moreticketsadded($(document));
    $('.ticketamountbox input').trigger('change');
    $('.bookingamountbox input').trigger('change');
    
    $('input:radio[name="servicetaxincluded"]').change(function(){
		if ($(this).is(':checked') && $(this).val() == 0) {
			$("#organisersservicetaxbox").removeClass('hide');
		}
		if ($(this).is(':checked') && $(this).val() == 1) {
			$("#organisersservicetaxbox").addClass('hide');
			$("#organisersservicetax").aval(0);
		}
	});

	$('input[name="facebookpage"]').change(function(){
		if ($(this).is(':checked') && $(this).val() == 'facebookpage') {
			//popitup('_blank',root_path+'open/fbtab');
			window.open(root_path+'open/fbtab','_blank');
		}
	});
    
});
function populateOrganiserForm($data){
	$("#organiserid").val($data['id']);
	$("#organisername").val($data['name']);
	$("#organiseremail").val($data['email']);
	$("#organisercontactno").val($data['contact']);
	$("#organiseraddress").val($data['address']);
}
function populatePageList(){
	if(typeof pageContact != "undefined")
        {
	$x = '<ul class="list-group">';
        $.each(pageContact,function(i,v){
		$x += '<li class="list-group-item"><a href="#" class="selectpageasorganiser" id="pg_'+v['id']+'">'+v['name'] +'('+v['email']+')'+'</a></li>';
	});
	$x += '</ul>';
        return $x;
        }
}
function getBankAccountsList(){

	$.get("PSAjax.php", {type:'getBankAccountList'}, function(data){
	    if(data['list']['accounts'].length == 0)
	    {
		 $("#addNewBackAccount").tab('show');
		 $(".addNewBackAccountForm").html(calliframe('module/bankaccountadd'));
	    }
	    else
	    {
		 $bankliststr = '<div class="row"> <div class="col-sm-1"></div> <div class="col-sm-4"><strong>Account Name</strong></div> <div class="col-sm-4"><strong>Bank Name</strong></div> <div class="col-sm-3"><strong>Account Number</strong></div>  </div>';
		$.each(data['list']['accounts'],function(i,v){
		    $bankliststr = $bankliststr + '<div class="row"> <div class="col-sm-1"><label><input type="radio" name="ticket_bank_account" value="'+v['account_id']+'" ></label></div> <div class="col-sm-4">'+v['accountholdername']+'</div> <div class="col-sm-4">'+v['bankname']+'</div> <div class="col-sm-3">'+v['acntnmbr']+'</div> </div>'; 
		});
		$(".bank-account-list").html($bankliststr);
	    }
	}, 'json');
}
var routemap;
function initGoogleMapsRouteMapCreate() {
console.log(map_position);
  map_position_obj = $.parseJSON(map_position);
  marker_obj = [];
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;
  routemap = new google.maps.Map(document.getElementById('routeMap'), {
    zoom: 6,
    center: map_position_obj
  });
  directionsDisplay.setMap(routemap);
  
  var input2 = document.getElementById('routemapinput');
  console.log('zzzzzzzzzzzzzzz');
  console.log(input2);
  console.log($("#routemapinput"));
  google.maps.event.addDomListener(input2, 'keydown', function(e) { 
	    if (e.keyCode == 13) { 
		e.preventDefault(); 
	    }
	    console.log('XXXXXXXXXXXXX');
  });
  var searchBox = new google.maps.places.SearchBox(input2);
  console.log('YYYYYYYYY');
  routemap.addListener('bounds_changed', function() {
	console.log('bounds changed');
	searchBox.setBounds(routemap.getBounds());
  });
  
  var markers = [];
  var bounds = new google.maps.LatLngBounds();
    
  searchBox.addListener('places_changed', function() {
    document.getElementById('routemapinput').value = '';
    console.log('places changed');
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    //markers.forEach(function(marker) {
     // marker.setMap(null);
    //});
   // markers = [];

    // For each place, get the icon, name and location.
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      marker = new google.maps.Marker({
        map: routemap,
        title: place.name,
        position: place.geometry.location,
        draggable:true
      });
      markers.push(marker);
      marker_obj = {"title":marker.title, "position":{'lat':marker.position.lat(),'lng':marker.position.lng()}};
      addMarkerToRoute(marker_obj);
      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
	
	routemap.fitBounds(bounds);
	document.getElementById('btn_gen_route').addEventListener('click', function() {
	    var checkboxArray = $('#waypoints input:checked');
	    var start = $.parseJSON(checkboxArray.eq(0).attr('pos'));
	    var stop = $.parseJSON(checkboxArray.eq(checkboxArray.length-1).attr('pos'));
	    var waypts = [];
	    for(i=1;i<(checkboxArray.length-1);i++)
	    {
	      waypts.push({
		location: $.parseJSON(checkboxArray.eq(i).attr('pos')),
		stopover: true
	      });
	    }
	    calculateAndDisplayRoute(directionsService, directionsDisplay,start , stop , waypts);
	});
  })
}
function addMarkerToRoute(obj){
    var newobj = {};
    newobj['title'] = obj.title;
    newobj['position'] = obj.position;
    $("#waypoints").append("<div class='col-sm-4 col-xs-2 checkbox'><label class='form-group control-label'><input type='checkbox' checked name='waypoints[]' value='"+JSON.stringify(newobj)+"' pos='"+JSON.stringify(obj.position)+"' />"+obj.title+"</label></div>");
}

function routemaptoimage(map){
    var currentPosition=map.getCenter();
    var checkboxArray = $('#waypoints input:checked');
    $path = "";
    $markers = "";
    for(i=0;i<(checkboxArray.length);i++)
    {
        $x = $.parseJSON(checkboxArray.eq(i).attr('pos'));
        $path = $path+"|"+$x['lat']+","+$x['lng'];
        $markers = $markers +"&markers=color:red|label:"+encodeURI((i+1)+"-"+checkboxArray.eq(i).parent().text())+"|"+$x['lat']+","+$x['lng'];
    }
    $str =  "http://maps.google.com/maps/api/staticmap?key="+google_maps_key+"&sensor=false&center=" +
        currentPosition.lat() + "," + currentPosition.lng() +
        "&zoom="+map.getZoom()+"&size=300x200&path=color:0xff0000ff|weight:5"+$path+$markers;
    return $str;
}

function togglePublishComponentStatus(type,status){
    if(status == 1)
    $("a[href='#"+type+"']").removeClass('alert-warning').addClass('alert-success');
    else
    $("a[href='#"+type+"']").removeClass('alert-success').addClass('alert-warning');
    moveToNextStep(type);
    setTimeout(function(){
        unhidepublishbtn()
    },2000);
}
function moveToNextStep(type){
    $currentindex = $(".eventpublish a[data-parent='#accordion']").index($("a[href='#"+type+"']"));
    if($currentindex > -1)
    $(".eventpublish :not(.advancedoptions) a[data-parent='#accordion']").eq($currentindex + 1).trigger('click');
}
function unhidepublishbtn(){
    console.log('----------');
    if($(".eventpublish a[data-parent='#accordion'].alert-warning").length == 0)
    $(".publishbtn").removeClass('hide');
}
function moreticketsadded($ticketrowobj){
	if(($ticketrowobj).hasClass('dynamicallyadded')){
		$ticketrowobj.find('input').val('');
	}
	$ticketrowobj.find('.ticketamountbox input').on('change',function(){
		$temp = $(this);
		if($temp.val() > 5000)
		{
		$temp.parents('.ticket-price-row').eq(0).find('.bookingamountbox').removeClass('hide');
		showToolTipMsg($temp,'Include Booking amount in case you want user to buy in partial and complete transaction later or pay rest offline');
		}
		else
		{
		$temp.parents('.ticket-price-row').eq(0).find('.bookingamountbox').addClass('hide').find('input').val('').trigger('change');
		}
	});
	$ticketrowobj.find('.bookingamountbox input').on('change',function(){
		$temp = $(this);
		$temptktprice = parseInt($temp.parents('.ticket-price-row').eq(0).find('.ticketamountbox input').val());
		$tempval = parseInt($temp.val());
		if($tempval < ($temptktprice * 25 / 100) || $tempval >= ($temptktprice))
		{
		$temp.val('');
		showToolTipMsg($temp,'Booking amount should be greater than 25% of ticket price');
		}
		
		$(".lastprebookingdatebox").addClass('hide');
		$('.bookingamountbox input').each(function(){
			console.log($(this).val());
			if($(this).val() != '')
			$(".lastprebookingdatebox").removeClass('hide');
		});
	});
}