getCountdown();

setInterval(function () { getCountdown(); }, 1000);

function getCountdown(){
        $(".countdowntimer:not(.stopped)").each(function(){
            var timer_days, timer_hours, timer_minutes, timer_seconds; // variables for time units

            // var timer_target_date = Date.parse($(this).data('timerend'));
            var timer_target_date = standardDate($(this).data('timerend'));
            // find the amount of "seconds" between now and target
            var current_date = new Date().getTime();
            var seconds_left = (timer_target_date - current_date) / 1000;
            if(parseFloat(seconds_left) < parseFloat(0))
            {
            $(this).addClass('stopped').addClass('hide');
            return;
            }    
            timer_days = pad( parseInt(seconds_left / 86400),2,0 );
            seconds_left = seconds_left % 86400;

            timer_hours = pad( parseInt(seconds_left / 3600),2,0 );
            seconds_left = seconds_left % 3600;

            timer_minutes = pad( parseInt(seconds_left / 60),2,0 );
            timer_seconds = pad( parseInt( seconds_left % 60 ),2,0 );
            var x = timer_days+'-'+timer_hours+'-'+timer_minutes+'-'+timer_seconds;
            if(x == '00-00-00-00')
            $(this).addClass('stopped');
            else
            $(this).removeClass('hide');
            // format countdown string + set tag value
            $(this).find(".tiles").html("<span>" + timer_days + "</span><span>" + timer_hours + "</span><span>" + timer_minutes + "</span><span>" + timer_seconds + "</span>"); 
        })
}

