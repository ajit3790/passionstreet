$(document).on('click','.post-edit-btn',function(event){    
    $temp = $(this).parents(".post").find(".post-content");
    if($temp.has("textarea").length == 0)
    $temp.html('<textarea rows="" onkeypress="if(isEnterPressed(event)){post_edit(this)}" cols="" placeholder="Edit Content" comment_on="post" post_id="'+$(this).attr('data-post_id')+'" class="comment_tarea">'+$temp.text()+'</textarea>');
    $temp.find("textarea").focus().on('change keyup keydown paste cut',function(){$(this).height(0).height(this.scrollHeight);});
});

$(document).on('click','.post-cmnt-edit-btn',function(event){
    $requestId = $(this).attr('data-id');
    $temp = $('.post-sec-cmnt-'+$requestId).find(".post-cmnt-content");
    $('#post-cmnt-box-id-'+$requestId).hide();
    $('.post-cmnt-tm-'+$requestId).hide();
    $('.post-cmnt-uname-'+$requestId).hide();

    if($temp.has("textarea").length == 0)
        $temp.html('<textarea rows="1" onkeypress="if(isEnterPressed(event)){post_comment_edit(this)}" cols="" placeholder="Edit comment" comment_on="post" post-comment-id="'+$requestId+'" class="comment_tarea">'+$temp.text()+'</textarea>');
    $temp.find("textarea").focus().on('change keyup keydown paste cut',function(){$(this).height(0).height(this.scrollHeight);});
});

$(document).on('click','.post-delete-btn',function(event){
    var $params = {};
    $type = "deletePost";
    $params['boxid'] = "";
    $params['id'] = $(this).data('post_id');    
    bootbox.confirm('Do you want to delete post ?', function(result){ 
        if (result === true) {
            $.post(root_path+'AjaxGroup.php?requestType='+$type, $params, function(data){
                $('#post_'+$params['id']).remove();
            });
        }
    });
});



$(document).on('click','.post-cmnt-delete-btn',function(event){
    var $params = {};
    $type = "deleteGroupComment";
    $params['boxid'] = "";
    $params['id'] = $(this).attr('data-id');

    bootbox.confirm('Do you want to delete ?', function(result){ 
        if (result === true) {
            $.post(root_path+'AjaxGroup.php?requestType='+$type, $params, function(data){
                var responseData = $.parseJSON(data);
                
                var removed  = '<div class="pull-right post-control post-comment-control" id="post-cmnt-box-id-'+responseData.id+'">';
                    removed += '<i class="fa fa-close remove" data-id="'+responseData.id+'"></i></div>';
                    removed += '<div class="overlay-spinner"> Comment has been removed</div>';
                    
                $('.post-sec-cmnt-'+responseData.id).html(removed);
                $('.post-sec-cmnt-'+responseData.id).addClass('removed');
                setTimeout(function(){ 
                    $('.post-sec-cmnt-'+responseData.id).remove();
                }, 3000);
            });
        }
    });
});

$(document).on('click','.post-like-btn',function(){
    $clickEvent = $(this);
    var postId = $clickEvent.attr('data-postId');
    var totalLikeCount = $clickEvent.attr('data-likecount');
    totalLikeCount  = parseInt(totalLikeCount);
    var isUserLiked = $clickEvent.attr('data-userlike');
    
    $.ajax({
      url: root_path+'AjaxGroup.php',
      type: "POST",
      data: 'requestType=likePost&postId='+postId+'&liked='+isUserLiked,
      success: function(data) {
        var likeResponse = $.parseJSON(data);
        if (isUserLiked == 1) {
            var likeCountUpdate = totalLikeCount-1;
            $clickEvent.attr('data-likecount', likeCountUpdate);
            $clickEvent.attr('data-userlike', '0');
        } else {
            var likeCountUpdate = totalLikeCount+1;
            $clickEvent.attr('data-likecount', likeCountUpdate);
            $clickEvent.attr('data-userlike', '1');
        }
        likeCountUpdate = parseInt(likeCountUpdate);
        if (likeCountUpdate == 0) {
            $("#pl-"+postId).html('');
        } else {
            $("#pl-"+postId).html('('+likeCountUpdate+')');
        }
      },
      error: function(res) {
      }
    });

    /*var $params = {};
    $type = "deletePost";
    $params['boxid'] = "";
    $params['id'] = $(this).data('post_id');    
    */
    /*$.post(root_path+'AjaxGroup.php?requestType='+$type, $params, function(data){
        $('#post_'+$params['id']).remove();
    });
    */
});



function initialisewall(){
	$(".wall").on("click",".post-ctrl",function(event){
        event.stopPropagation();
        $temp = $(this);
        $temp.parents(".post").find('.'+$temp.attr('trgt')).toggleClass('hide');
	if((!($temp.hasClass('commentsAdded'))) && $temp.attr('trgt') == 'commentbox')
        {
            $id = $temp.attr('post_id');
            $comment_on = "post";
            $params = {id:$id,comment_on:$comment_on};
            $.get(root_path+'PSAjax.php?type=getcomments',$params,function(data){
                $comments = $.parseJSON(data);
                $commentshtml = '';
                $.each($comments['list']['comments'],function(i,v){
                    $commentshtml += '<div class="notif_row nobg_border"> ' + 
                            '<div class="usr_dp"><img src="'+v['userdp']+'"></div> ' +
                            '<div class="user_comment"><span class="gen_usr_nm">'+v['name']+'</span> '+v['comment']+' </div>' +
                        '</div>';
                });
		addScrollbar($temp.parents(".post").find(".all_comments_container").html($commentshtml).addClass('scroll-content'));
		$temp.addClass('commentsAdded');
                $temp.parents(".post").find('.comment_tarea').on('change keyup keydown paste cut',function(){$(this).height(0).height(this.scrollHeight);});
            });    
        }
    });
    
    $(".wall").on('click','.commentbox',function(event){
        //event.stopPropagation();
    });
    
    $(".wall").on('click','.shareonemail',function(event){
	event.stopPropagation();
	$(this).parents(".sharebox").find(".email-share").toggleClass("hide");
    });
    $(".wall").on('click','.sharepost',function(event){
       $temp = $.trim($(this).parents('.post').find('.tags').text());
       var nbsp = String.fromCharCode(160);
       var tags = $temp.split('#');
       $temp = {};
       $temp2 = $(this).parents('.post')
       $temp['title'] = $temp2.find('.post-content').text();
       $temp['description'] = '';
       if($temp2.find('.img_set img').length > 0)
       $temp['image'] = $temp2.find('.img_set img').eq(0).attr('src');
       $temp['url'] = $(this).attr('shareurl');
       $temp['tags'] = tags;
       sharepage($(this).attr('type'),$temp); 
    });
    
    $(".wall").on('click','.sharepostcustom',function(event){
            $(this).next('ul').slideToggle(200);
    });
    $(".wall").on('click','.sharepostonemail',function(){
            $parent = $(this).parent();
            $temp = $(this).parents(".email-share").find("textarea").val();
            if($temp){
                    $.post(root_path+'PSAjax.php?type=sharepostonemail', {emails:$temp,post_id:$(this).attr('post_id'),boxid:$(this).attr('id')}, function(data){ 
                            $res = $.parseJSON(data);
                            $parent.append('<div>'+$res['data']+'</div>');
                    });
            }
    });
    
    $(".wall").on('click','.sharepage',function(){
       sharepage($(this).attr('type'),''); 
    });
    $(".wall").on('click','.copybtn',function(){
            copyToClipboard($(this).attr('content'));
    });
}

function wallpaging(){
	$('.wall').cleverInfiniteScroll({
	    contentsWrapperSelector: '.wall .main-content',
	    contentSelector: '.inner',
	    nextSelector: '#nextwall',
	});
}

// callback for group listing page
function groupsPaging(){
    $('.grouplisting').cleverInfiniteScroll({
        contentsWrapperSelector: '.gp-scroll-box',
        contentSelector: '.ajax-post-response',
        nextSelector: '#nextGroupList',
    });
}

// callback for group members listing
function groupMembersPaging(){
    $('.groupMembers').cleverInfiniteScroll({
        contentsWrapperSelector: '.groupMembers .main-content',
        contentSelector: '.member-list',
        nextSelector: '#nextMemberList',
    });
}

function post_delete(data){
    if(status_codes[data['status']] == 'Success');
    {
        if(data['boxid'] != '')
        {
            $("#"+data['boxid']).text(data['data']);
            $("#"+data['boxid']).delay(2000).hide(0);
        }
    }
}

function post_edit(o){
    $temp = $(o);
    var $params = {};
    $type = "editPost";
    $params['boxid'] = "";
    $params['id'] = $temp.attr('post_id');
    $params['about'] = $temp.val();
    $.post(root_path+'AjaxGroup.php?requestType='+$type, $params, function(data){
        $("#post_"+$params['id']).find('.post-content').html($params['about']);
    });
}

function post_comment_edit(o){
    $temp = $(o);
    var $params = {};
    $type = "editPostComment";
    $params['boxid'] = "";
    $params['id'] = $temp.attr('post-comment-id');

    $params['comment'] = $temp.val();    
    if ($params['comment'] == '') {
        return false;
    }
    $('#post-cmnt-box-id-'+$params['id']).show();
    $('.post-cmnt-tm-'+$params['id']).show();
    $('.post-cmnt-uname-'+$params['id']).show();

    $.post(root_path+'AjaxGroup.php?requestType='+$type, $params, function(data){
        $(".post-sec-cmnt-"+$params['id']).find('.post-cmnt-content').html(" "+$params['comment']);
    });
}

function post_add_comment(request){    
    $tarea = $(request);
    var postId = $tarea.attr("post_id");
    var comment_on = $tarea.attr("comment_on");
    var comment = $tarea.val();
        comment = $.trim(comment);
    if (comment == undefined || comment == '') {
        return false;
    }

    $.ajax({
      url: root_path+'AjaxGroup.php',
      type: "POST",
      data: 'requestType=addComment&postId='+postId+'&comment='+comment,
      success: function(data) {
        var commentResponse = $.parseJSON(data);
        var commentData     = commentResponse['result'];
        var commentId       = commentData['id'];
        var $commentsHtml   = '';
        var $editCommentHtml= '';
        $editCommentHtml += '<div class="pull-right post-control post-comment-control" id="post-cmnt-box-id-'+commentId+'">';
        $editCommentHtml += "<i class='fa fa-pencil post-cmnt-edit-btn' data-id='"+commentId+"'></i>";
        $editCommentHtml += "<i class='fa fa-trash post-cmnt-delete-btn' data-action='post_cmnt' data-aj='true' data-p-str='post_cmnt_id' data-t='post-cmnt-delete' data-id='"+commentId+"' trgtid='post_cmnt_"+commentId+"' data-c='post_cmnt_delete'></i>";
        $editCommentHtml += '</div>';

        $cUserProfileUrl = commentData['profileUrl'];

        $commentsHtml += '<div class="notif_row nobg_border post-sec-cmnt-'+commentId+'">';
        $commentsHtml += $editCommentHtml;
        $commentsHtml += '<div class="usr_dp"><a href="'+$cUserProfileUrl+'"><img class="unveil" src="'+commentData['userdp']+'"></a></div>';
        $commentsHtml += '<div class="user_comment"><span class="gen_usr_nm"><a href="'+$cUserProfileUrl+'" class="post-cmnt-uname-'+commentId+'">'+commentData['name']+'</a>';
        $commentsHtml += '<span class="post-cmnt-content"> '+commentData['comment']+'</span></span>';
        $commentsHtml += "<span class='time reltime post-cmnt-tm-"+commentId+"' data-time='"+$.now()+"'>just now</span>";

        $commentsHtml += ' </div></div>';

        $("#post_"+postId).find('.all_comments_container').append($commentsHtml);
        $("#post_"+postId).find(".comment_form textarea").val('');
      },
      error: function(res) {
      }
  });
}