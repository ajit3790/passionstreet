function jqueryJcrop_callback(){
    $('body').on("change",".filecrop",function(){
        $currentobj = $(this);
        $currentselector = $("#"+$currentobj.attr('id'));
        //console.log($currentselector);
        var cropper_box = '<div id="img_crop"> \
                   <div ><img id="cropper" /></div> \
                   <button class="btn btn-primary btn-sm hide" id="crop-done">Done</button>\
                   <div class="hide"><canvas id="preview"></canvas></div>\
                   </div>';
        myModal("Cropper",cropper_box,'');
        fileSelectHandler();

    });    
}

function jquerybxslidermin_callback(){
    $('.membercarousel').bxSlider({
    slideWidth:170,
    minSlides: 1,
    maxSlides: 5,
    infiniteLoop: false,
    hideControlOnEnd: true,
    prevText:" ",
    nextText:" ",
    slideMargin: 10,
    onSliderLoad: function(){
    	//$( ".bxsliderouter:has(.membercarousel)" ).removeClass('hide');
    },
    });
    
    $('.eventcarousel').each(function(){
	$options = {
	    slideWidth:($(this).data('slidewidth') != 'undefined')?$(this).data('slidewidth'):320,
	    minSlides: 1,
	    maxSlides: ($(this).data('maxslides') != 'undefined')?$(this).data('maxslides'):3,
	    infiniteLoop: false,
	    hideControlOnEnd: true,
	    prevText:" ",
	    nextText:" ",
	    slideMargin:0
	};
	$(this).bxSlider($options)  ;
    });
    
    $('.passioncarousel').bxSlider({
    slideWidth:320,
    minSlides: 1,
    maxSlides: 12,
    infiniteLoop: false,
    hideControlOnEnd: true,
    prevText:" ",
    nextText:" ",
    slideMargin: 20
    })  ;
    
}

function summernote_callback(){
    $(".simpleeditor").summernote({
        /*toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']]
        ]*/
	callbacks: {
		onPaste: function (e) {
		    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
		    e.preventDefault();
		    setTimeout(function () {
			document.execCommand('insertText', false, bufferText);
		    }, 10);
		}
	}
    });
    $(".simpleeditormin").summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']]
        ],
	callbacks: {
		onPaste: function (e) {
		    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
		    e.preventDefault();
		    setTimeout(function () {
			document.execCommand('insertText', false, bufferText);
		    }, 10);
		}
	}
    });
}
function history_callback(){
    //return;
    if(overridehistoryurl == true && window.location['href'] != orgurl)
    overRideHistoryState(stripQueryString(orgurl),'');
}

function hello_callback(){
    if(newpost != false && sns!=false)
    {
        hello.init($sns1,{redirect_uri: root_path});
        hello(sns).api('me/share','post',newpost).then(function(json) {
            //console.log(json);
        }, function(e) {
                //console.log('Whoops! ' + e.error.message);
        });
    }
    
    
    $("body").on("click",".createloginpopup",function(event){
       event.preventDefault();
       $sns2 = {};
       $x = $(this).attr('type1');
       $sns2[$x] = $sns1[$x];
       try{
	       if($(this).data('logincallback'))
	       {
		login_callback = minimal_login;
		minimal_login_callback = $(this).data('logincallback');
	       }
       }
       catch(e){}
       if(login_status == false)
       {
            hello.init($sns2,{redirect_uri: root_path});
            hello($x).login({'scope':'basic,email,birthday,friends,publish'}).then(function(auth) {
                    sessionStart(auth);
            }, function(e) {
                    //console.log('Signin error: ' + e.error.message);
            });
       }
    });
    
}

function jquerynicescrollmin_callback(){
    //var nicesx = $(".scroll-content").niceScroll({touchbehavior:false,cursorcolor:"#333",cursoropacitymax:0.6,cursorwidth:8});
    	if(!isMobile)
    	{
	    	addScrollbar($(".scroll-content"));
	}
}



function fileinput_callback(){
    var $selector = callbackselector['fileinput'];
     //$.each($selector,function(i,v){
     $($selector).each(function(i,v){
        $(v).fileinput({
            uploadUrl: root_path+"PSAjax.php?type=imageuploadajax3&boxid=", // server upload action
            uploadAsync: true,
            maxFileCount: 5,
            //showUpload: false,
            //showCaption: false,
            browseClass: "btn btn-primary btn-lg",
	    fileType: "any",
            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
        });
     });
}

function jqueryunveil_callback($iteration){
	if(typeof $iteration == 'undefined')
	$iteration = 1;
	if(!($iteration == 1 || $iteration ==2 ))
	return;
	//return;
	$("img.unveil:not(.unveildone)").each(function(i,v){
		$(this).attr('init-src',$(this).attr('src'));
		//$(this).error(function(e){console.log('error');$(e.target).attr('src',$(e.target).attr('init-src'));});
	});
	
    if(server == 'local')
    {
        $(".unveil").each(function(){
           //$(this).attr('data-src',$(this).attr('src')); 
        });
    }
    $(".unveil").each(function(){
        $(this).addClass('psimage'); 
    });
    $(".unveil").unveil(200,function(){
        $(this).load(function() {
           
           $(this).addClass('unveildone');
           $(this).removeClass('unveil');
		   //console.log($(this).parents('.module').length);
		   /*
		   if($(this).parents('.img_set').length > 0)
		   {
			$imgHeight = $(this).height();
			$imgWidth = $(this).width();
			$parentDiv = $(this).parent();
			$parentDivHeight = $parentDiv.height();
			$parentDivWidth = $parentDiv.width();
			$widthDiff = ($imgWidth  - $parentDivWidth)/2;
			$heightDiff = ($imgHeight  - $parentDivHeight)/2;
			if($widthDiff > 0)
			$(this).css({'left':'-'+$widthDiff+'px'});
			if($heightDiff > 0)
			$(this).css({'top':'-'+$heightDiff+'px'});
			
		   }*/
        });
    }); 
    if($iteration == 1)
    jqueryunveil_callback(2);
}
function stickykit_callback(){
    //$(".sidebar").stick_in_parent({offset_top:100});
    if(isMobile == true || $(document).width() < 1200)
        return ;
    $(".sidebar-left,.sidebar-right").stick_in_parent(
    	{
    	offset_top: 67
    	}
    );
   
   $('.sidebar-left,.sidebar-right')
    .on('sticky_kit:bottom', function(e) {
        $(this).parent().css('position', 'static');
    })
    .on('sticky_kit:unbottom', function(e) {
        $(this).parent().css('position', 'relative');
    });
}
function isInViewport_callback(){
    if(isMobile == false)
        return;
    $('.play:in-viewport('+(viewportheight+100)+')').trigger('click');
    $(window).scroll(function() {
      $('.play:in-viewport('+(viewportheight+100)+')').trigger('click');
    });
}
function bootstrapdatetimepickermin_callback(){
    $("input.datepicker:not(.datepickeradded)").each(function(){
	var $type = $(this).data('type');
	var $options = {};
	$options['autoclose'] = true;
	//$options['format'] = "yyyy/mm/dd hh:mm:ss";
	
        if($type == 'hourrangeselector')
        {
            $options['hourRange'] = $(this).data('options');
            $options['pickDate'] = false;
            $options['pickTime'] = true;
            $options['pickSeconds'] = true;
            $options['outputFormat'] = "rr:mm:ss";
	//    $options['format'] = "hh:mm:ss";
	
        }
        else if($type == 'datetimepicker')
        {
	    $options['pickDate'] = true;
            $options['pickTime'] = true;
            $options['outputFormat'] = "dd/MM/yyyy hh:mm:ss";
	//    $options['format'] = "yyyy/mm/dd hh:mm:ss";
	}
	else if($type == 'timepicker')
        {
	    $options['pickDate'] = false;
            $options['pickTime'] = true;
	    $options['pickSeconds'] = false;
            $options['outputFormat'] = "HH:mm PP";
	//    $options['format'] = "HH:mm P";
	}
        else if($type == 'datepicker')
        {
	    $options['pickDate'] = true;
            $options['pickTime'] = false;
            $options['outputFormat'] = "dd/MM/yyyy";
	//    $options['format'] = "yyyy/mm/dd";
	}
	if($(this).hasClass('date-future'))
	{
		$options['startDate'] = currentFullDate();
	}
	else if($(this).hasClass('date-past'))
	{
		$options['endDate'] = currentFullDate();
	}
	else if($(this).hasClass('date-range'))
	{
		if(dateRange && dateRange['start'])
		$options['startDate'] = dateRange['start'];
		if(dateRange && dateRange['end'])
		$options['endDate'] = dateRange['end'];
	}
	$(this).datetimepicker($options);
	$(this).addClass('datepickeradded');
    });
}
function jBoxmin_callback(){
/*
    $(".jbox-img:not(.jBoxAdded)").each(function() {
	$(this).click(function(){
		gallery = $(this).parents(".jbox-gallery").find(".jbox-img");
		lastImg = gallery[gallery.length - 1].getAttribute("data-src"),
		firstImg = gallery[0].getAttribute("data-src");
		var n = $(this).attr("data-src").replace("thumb/","");
		openJBox(n, "first");
	});
	$(this).addClass('jBoxAdded');
    });
*/
    $(".img_set:not(.rearranged)").each(function(){
    	$temp = $(this);
    	var $imgset = [];
    	$temp.find('img').each(function(){
    		var src = ($(this).attr('data-src') !== undefined)?$(this).attr('data-src'):$(this).attr('src');
    		//src = src.replace('/thumb/','');
    		$imgset.push(src);
    	});
    	$temp.imagesGrid({
	    images: $imgset,
	    align: true,
	    getViewAllText: function(imgsCount) { return '+'+($imgset.length-5) }
	});
	$(this).addClass('rearranged');
    });
}
function atcmin_callback(){
	$(".addtocalendarps").toggleClass('hide');
}
function loader_callback(){
	console.log('google charts js is loaded');
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);
}
function drawChart() {

        var data = new google.visualization.DataTable();
        data.addColumn('date', 'Time of Day');
        data.addColumn('number', 'Rating');

        data.addRows([
          [new Date(2015, 0, 1), 5],  [new Date(2015, 0, 2), 7],  [new Date(2015, 0, 3), 3],
          [new Date(2015, 0, 4), 1],  [new Date(2015, 0, 5), 3],  [new Date(2015, 0, 6), 4],
          [new Date(2015, 0, 7), 3],  [new Date(2015, 0, 8), 4],  [new Date(2015, 0, 9), 2],
          [new Date(2015, 0, 10), 5], [new Date(2015, 0, 11), 8], [new Date(2015, 0, 12), 6],
          [new Date(2015, 0, 13), 3], [new Date(2015, 0, 14), 3], [new Date(2015, 0, 15), 5],
          [new Date(2015, 0, 16), 7], [new Date(2015, 0, 17), 6], [new Date(2015, 0, 18), 6],
          [new Date(2015, 0, 19), 3], [new Date(2015, 0, 20), 1], [new Date(2015, 0, 21), 2],
          [new Date(2015, 0, 22), 4], [new Date(2015, 0, 23), 6], [new Date(2015, 0, 24), 5],
          [new Date(2015, 0, 25), 9], [new Date(2015, 0, 26), 4], [new Date(2015, 0, 27), 9],
          [new Date(2015, 0, 28), 8], [new Date(2015, 0, 29), 6], [new Date(2015, 0, 30), 4],
          [new Date(2015, 0, 31), 6], [new Date(2015, 1, 1), 7],  [new Date(2015, 1, 2), 9]
        ]);


        var options = {
          title: 'Rate the Day on a Scale of 1 to 10',
          width: $("#chart_div").width(),
          height: ($("#chart_div").width() * 3/4),
          hAxis: {
            format: 'M/d/yy',
            gridlines: {count: 15}
          },
          vAxis: {
            gridlines: {color: 'none'},
            minValue: 0
          },
	  chartArea:{left:50,top:20,bottom:50,width:"100%"}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

        chart.draw(data, options);
	/*
        var button = document.getElementById('change');
        button.onclick = function () {
		// If the format option matches, change it to the new option,
		// if not, reset it to the original format.
		options.hAxis.format === 'M/d/yy' ?
		options.hAxis.format = 'MMM dd, yyyy' :
		options.hAxis.format = 'M/d/yy';
		chart.draw(data, options);
        };
	*/
}
function datepickerv2_callback(){
	//<div class="datepickerv2" data-mode="range/multiple/single" data-datebounds="2017-05-01,2017-05-20" data-dates="2017-05-10,2017-05-13"></div>
	$('.datepickerv2:not(.datepickeradded)').each(function(iv){
		$temp = $(this);
		var $dates;
		if($temp.data('dates'))
		{
		$dates = $temp.data('dates').split(',');
		$.each($dates,function(i,v){
			$dates[i] = new Date(Date.parse(v.split(' ')[0]+' 00:00:00'));
		});
		}
		else
		$dates = [null,null];
		
		var $datebounds;;
		if($temp.data('datebounds'))
		{
		$datebounds = $temp.data('datebounds').split(',');
		$.each($datebounds,function(i,v){
			$datebounds[i] = new Date(Date.parse(v.split(' ')[0]+' 00:00:00'));
		});
		}
		else
		$datebounds = [null,null];
		var callback = '';
		if($temp.data('callback'))
		callback = $temp.data('callback');
		if($temp.data('mode') == 'single')
		{
			$temp.DatePicker({
				  mode: 'single',
				  inline: true,
				  date: ($dates[0])?$dates[0]:null,
				  startDate:($datebounds[0])?$datebounds[0]:null,
				  endDate:($datebounds[1])?$datebounds[1]:null,
				  allowfuturedates:($temp.attr('data-allowfuturedates') && $temp.data('allowfuturedates') == 0)?false:true,
				  allowpastdates:($temp.attr('data-allowpastdates') && $temp.data('allowpastdates') == 0)?false:true,
				  onChange: function(dates,el){
					if(callback)
					executeFunctionByName(callback,[[dates],el]);
				  }
			});
		}
		else if($temp.data('mode') == 'multiple')
		{
			$temp.DatePicker({
				inline: true,
				calendars: 1,
				mode: 'multiple',
				date: ($dates)?$dates:[],
				startDate:($datebounds[0])?$datebounds[0]:null,
				endDate:($datebounds[1])?$datebounds[1]:null,
				allowfuturedates:($temp.attr('data-allowfuturedates') && $temp.data('allowfuturedates') == 0)?false:true,
				allowpastdates:($temp.attr('data-allowpastdates') && $temp.data('allowpastdates') == 0)?false:true,
				onChange: function(dates,el){
					if(callback)
					executeFunctionByName(callback,[dates,el]);
				}
			});
		}
		else if($temp.data('mode') == 'range')
		{	
			//$dates.sort();
			var $counts = $dates.length;
			var from = ($dates[0])?$dates[0]:null;
			var to = ($dates[$counts-1])?$dates[$counts-1]:null;
			$temp.DatePicker({
				inline: true,
				date: [from, to],
				calendars: 1,
				mode: 'range',
				current: ($dates[0])?$dates[0]:null,
				startDate:($datebounds[0])?$datebounds[0]:null,
				endDate:($datebounds[1])?$datebounds[1]:null,
				allowfuturedates:($temp.attr('data-allowfuturedates') && $temp.data('allowfuturedates') == 0)?false:true,
				allowpastdates:($temp.attr('data-allowpastdates') && $temp.data('allowpastdates') == 0)?false:true,
			
				onChange: function(dates,el) {
				// update the range display
					$('#date-range-field span').text(
						dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+
						dates[0].getFullYear()+' - '+
						dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+
						dates[1].getFullYear()
					);
					if(callback)
					executeFunctionByName(callback,[dates,el]);
				}
			});
		}
		$temp.addClass('datepickeradded');
	});
}