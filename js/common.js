var $currentobj = null; 
var $currentselector = null;
var $orgwidth = 0,$orgheight = 0,$scaledwidth = 0,$scaledheight = 0;
var $multicrop = {};
var internaliframid = null;
var $temp2 = {};
var isMobile = false; //initiate as false
var viewportwidth = window.screen.availWidth;
var viewportheight = window.screen.availHeight;
var timer = {};
var datetime = new Date();
var datetimems =  datetime.getTime();
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))
isMobile = true;


$(document).ready(function(){
    onBodyLoaded();
});
function onBodyLoaded(){
    $(".login-btn").on('click',function(){
        loginModal('');
    });
    if((login_status == false) && (login_required == true))
    loginModal('');
    if((login_status == true) && (profile_complete == false))
    callmodaliframe("Complete Your Profile","module/registration");
    if(login_status == true)
    addToLocalStorage('profile_id',profile_id);
    else
    addToLocalStorage('profile_id','unknown');
    
    onContentAdd();
    
    if(q['pagetype'] == 'index')
    {
        try{
    if(isMobile)
    {
        $("#promotionalvideo").attr('src',$("#promotionalvideo").data('src').replace('autoplay=1','autoplay=0'));
        $(".video-area .vdo_ovly").addClass('hide');
    }
    else
    $("#promotionalvideo").attr('src',$("#promotionalvideo").data('src').replace('autoplay=1','autoplay=0'));
    //$("#promotionalvideo").attr('src',$("#promotionalvideo").data('src'));
    $(".video-area .vdo_ovly").css('background','rgba(0,0,0,0)');
        }catch(e){
            
        }
    }
    
    $('body').on('click','.ajax-btn',function(event){
       event.preventDefault();
       $temp = $(this);
       if($temp.attr('data-aj')=='true')
       psajax($temp);
       event.stopPropagation();
    });
    
    $("body").on("click","#crop-done",function(){
            window[$currentselector.attr('callback')]();
    });

    $("body").on("click",".collapsed",function(){
            $(this).removeClass("collapsed");
    });

    $("body").on("click",".callmodaliframe",function(event){
        event.preventDefault();
        callmodaliframe($(this).text(),$(this).attr("data-targetsrc"));
    });
    
    $("body").on("click","[data-onclick]",function(event){
        event.preventDefault();
        $x = $(this).attr("data-onclick");
        executeFunctionByName($x);
    });
    
    var elements = document.querySelectorAll('input,select,textarea');
    for (var i = elements.length; i--;) {
        elements[i].addEventListener('invalid', function () {
            $navHeight = $(".navbar-fixed-top").height();
            var scrolledY = window.scrollY;
            if(scrolledY){
              window.scroll(0, scrolledY - $navHeight);
            }
        });
    }
    
    $('html').click(function(){
       $(".hideable:not(.hide)").addClass('hide'); 
    });
  
  
  if($(window).width()>990){    
    /*if(q['pagetype'] == 'index')
    {
        $(window).scroll(function() {  
            var scroll = $(window).scrollTop(); 
            if (scroll >= 100) {
            $("#main-nav").addClass("slide-out");
            $(".nav-open").addClass("rotate");
            } else {
            $("#main-nav").removeClass("slide-out");
            $(".nav-open").removeClass("rotate");
            }      

        });
    }*/
    /* Open when someone clicks on the span element */
   
    $(".nav-open").click(function(){
        $("#main-nav").toggleClass("slide-out");
        $(".nav-open").toggleClass("rotate");
    });
    }else{
        $(".nav-open").click(function(){
        $("#main-nav").toggleClass("slide-in");
        $(".mask").fadeIn(1000);
        $(".nav-open").toggleClass("rotate");
    }); 
    $(".mask").click(function(){
        $("#main-nav").toggleClass("slide-in");
        $(this).fadeOut();
    });   
    }
    
    $('.ajaxImageUpload').on('change',function() {
    formdata = false;
    if (window.FormData) {
        formdata = new FormData();
    }
    var filedata = ($(this))[0];
    var i = 0, len = filedata.files.length, img, reader, file;
    for (; i < len; i++) {
        file = filedata.files[i];

        if (window.FileReader) {
            reader = new FileReader();
            reader.onloadend = function(e) {
                //showUploadedItem(e.target.result, file.fileName);
            };
            reader.readAsDataURL(file);
        }
        if (formdata) {
            formdata.append("file[]", file);
        }
    }
    formdata.append('content_id',$(this).attr('content_id'));
    formdata.append('content_type',$(this).attr('content_type'));
    $event_gal = $(this).parents('.event-gallery').find('ul');
    if($event_gal.attr('id') === undefined)
    {
        $event_gal.attr('id','event_gal_'+getRandomInt(1000,9999));
    }
    $boxid = $event_gal.attr('id');
    if (formdata) {
        $.ajax({
            url: appendAjaxDefaultParameters(root_path+'PSAjax.php?type=imageuploadajax2&boxid='+$boxid),
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function(data) {
                $res = $.parseJSON(data);
                //console.log($res);
                if($res.status == 200)
                $('body').find('#'+$res['boxid']).prepend($res['data']);
                else if($res.status == 405)
                loginModal('');
            },       
            error: function(res) {

            }       
          });
        }
    });
    
    
    
    
    $("body").on("click","#forgot_password_btn",function(event){
        event.preventDefault();
        $(".login_form").toggle();
        $(".forgot_form").toggle();
        $temp = $(this).text();
        $(this).text($(this).attr('alternatetext'));
        $(this).attr('alternatetext',$temp);
    });
    $("body").on("click","#send_pswd_btn",function(){
        $(".error-text").hide();
        $temp = $("#frgt_email").val();
        if($temp == '')
        $(this).parents(".forgot_form").find(".error-text").html('Please provide Email Id').show(); 
        else
        {
            $.post(root_path+'PSAjax.php?type=forgot_pswd', {email:$temp,boxid:$(this).attr('id')}, function(data){ 
                $res = $.parseJSON(data);
                $("#"+$res['boxid']).parents(".forgot_form").find(".error-text").html($res['data']).show();
            });
        }
    });
    
    $("body").on("click","#login_btn",function(){
        $(".error-text").hide();
        $tempemail = $("#loginemail").val();
        $temppassword = $("#loginpassword").val();
        if($tempemail == '' || $temppassword == '')
        $(this).parents(".login_form").find(".error-text").html('Please provide Valid Input').show();
        else
        {    
            $.post(root_path+'PSAjax.php?type=login_process', {email:$tempemail,password:$temppassword,boxid:$(this).attr('id')}, function(data){ 
                $res = $.parseJSON(data);
                $("#"+$res['boxid']).parents(".login_form").find(".error-text").html($res['data']).show();
                if($res['login'] == 1)
                loginCallback($res);
            });
        }
    });
    
    $('body').on("change",".multifilecrop",function(){
    if(!($(this).data('crpvrsn')))
    $(this).data('crpvrsn') = 1;
    //console.log('cropper version');
    //console.log(  $(this).data('crpvrsn'));
    //console.log(  $(this).attr('data-crpvrsn'));
    $cropperversion = $(this).data('crpvrsn');
    var $cropperCallback = 'addCropper';
    if($cropperversion == 1)
    $cropperCallback = 'addCropper';
    else if($cropperversion == 2)
    $cropperCallback = 'addCropper2';
    $multicrop['currentFileSelector'] = $(this);
        $multicrop['currentActive'] = 0;
        $multicrop['resizeRatio'] = {};
        $multicrop['resizeRatio'][0] = 1;
        $multicrop['coordinates'] = {};
        $multicrop['coordinates'][0] = {};
        $cropImageLength = $multicrop['currentFileSelector'].get(0).files.length;
        $temp = 0;
        $tabheader = '<div class="tab-header"><ul class="nav nav-pills">';
        $tabcontent = '<div class="tab-content">';
        $cropHTML = '';
    for($temp = 0;$temp<$cropImageLength;$temp++){
            if($temp == 0)
                $active = 'active';
            else
                $active = '';
            $tabheader += '<li class="'+$active+ (($cropImageLength>1)?'':' hide ')+' imgcroppertab " id="imgcroppertab'+$temp+'" role="presentation"><a data-toggle="tab" role="tab" aria-controls="home" href="#imgcropper'+$temp+'">Image '+($temp+1)+'</a></li>';
            $tabcontent += '<div id="imgcropper'+$temp+'" class="row tab-pane fade '+$active+' in cropperbox" role="tabpanel">';
            $tabcontent += '<div class="img-container"><img style="width:100%;" id="multiimagecrop'+$temp+'" src="" ></div>';
            $tabcontent += '</div>';;
        };
    $tabcontent += '</div>';
        $tabheader += '</ul></div>';
        myModal("Cropper","<div class='multicropper'>"+$tabheader+$tabcontent+"</div>",'');
        for($temp = 0;$temp<$cropImageLength;$temp++){
        getDataURIfromFile($multicrop['currentFileSelector'],$temp,'#multiimagecrop'+$temp,$cropperCallback);
        };
    });
    
    $(".toggle-nav").on('click',function(){
        $(".ps-menu").slideToggle(200);
    });
    if(q['module'] != undefined)
    {
        callmodaliframe(q['title'],'module/'+q['module']+'?'+q['p']);
    }
    
    
    $(".addmore").on('click',function(event){
        event.preventDefault();
        var $temp1 = $(this).attr('data-trgtelement');
        if($temp1)
        {
            var $temp = $("."+$temp1).eq(0);
            var $temp2 = $temp.clone( true );
            $temp2.addClass("dynamicallyadded");
            $temp.parent().append($temp2[0].outerHTML);
            var $limit = ($(this).attr('data-limit'))?$(this).attr('data-limit'):4;
            if(($limit != -1) && $("."+$temp1).length >= $limit )
            {
                $(this).addClass('hide');
            }
            if($(this).attr('data-callback'))
            {
                window[$(this).attr('data-callback')]($("."+$temp1).last());
            }
        }
    });
    
    if(q['pagetype'] == 'userprofile')
    {
         $(".covr_phto").load(function() {
            $layerheight = $(".layer").height();
            $layerwidth = $(".layer").width();
            $coverimgheight = $(".covr_phto").prop('naturalHeight');
            $coverimgwidth = $(".covr_phto").prop('naturalWidth');
            if($coverimgheight> $layerheight ){
                    $cvroffsetheight = ($coverimgheight - $layerheight)/(-2);
                    $(".covr_phto").css({'position':'relative','top':$cvroffsetheight});
                }
                else if($coverimgwidth> $layerwidth ){
                    $cvroffsetwidth = ($coverimgwidth - $layerwidth)/(-2);
                    $(".covr_phto").css({'position':'relative','left':$cvroffsetwidth});
                }
                else{
                    $(".covr_phto").css({'min-width':'100%','min-height':'350px'});
                }
        });
    }
    
    
    prepare_youtube_frames();
    
    $sns1 = {};
    $sns1 = clientid;
    
   /* $(".hovercard").on({
        mouseenter: function () {
             render_card($(this).attr('htype'),$(this).attr('hentity'));
        },
        mouseleave: function () {
            //stuff to do on mouse leave
            $(".hover_menu").addClass('hide');
        }
    });*/
    // Upload group post images
    var addedImageIcon = false;
    $(".form-upload-preview").on('change',function() {
        $temp = $(this).get(0).files;
        $target = $(this).parents('.postbox').find($(this).attr('target'));
        $(".wall-post-btn").text('Upload in progress');
        $(".wall-post-btn").prop('readonly',true);
        for ($i=0;$i<$temp.length;$i++) {
            var $file = $temp[$i];
            $file['identifier'] = 'imgpreveiw_'+getRandomInt(1000,9999);
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    $tempid = theFile['identifier'];
                    $('<img/>',{'id':$tempid}).css({}).appendTo($target);
                    $("#"+$tempid).wrap("<div class='item' ></div>");
                    $temp3 = $("#"+$tempid).parent();
                    
                    $temp3.append('<div class="overlay"></div>');
                    $temp3.append('<div href="#" class="close delete-impg-preview">X</div>');
                   
                    $temp3.append('<input type="hidden" name="photoes[]" id="field_'+$tempid+'" class="imgpreviewupload"/>');
                    var lastElement = parseInt($temp.length);
                    var $temp2 = $("#"+$tempid).get(0);
                    $temp2.src = e.target.result;
                    $temp2.onload = function () {
                        formdata = false;
                        if (window.FormData) {
                            formdata = new FormData();
                        }
                        formdata.append("file[]", theFile);
                        if (formdata) {
                            $.ajax({    
                                url: appendAjaxDefaultParameters(root_path+'PSAjax.php?type=imageuploadajax2&boxid=field_'+theFile['identifier']),
                                type: "POST",
                                data: formdata,
                                processData: false,
                                contentType: false,
                                success: function(data) {
                                    $res = $.parseJSON(data);
                                    if($res.status == 405) {                                    
                                        loginModal('');
                                    } else if($res.status == 200) {
                                        $x = $('body').find('#'+$res['boxid']);
                                        $x.val($res['list']['photoes'][0]);
                                        $temp = $x.parents('.postbox').find('.imgpreviewupload');
                                        if($temp.filter(function(index){return $(this).val() != ''}).length == $temp.length) {
                                            $(".wall-post-btn").text('POST');
                                            $(".wall-post-btn").prop('readonly',false);
                                        }
                                    }
                                },       
                                error: function(res) { }       
                            });
                        }
                    }
                };
            })($file);
            
            if ($file) {
              reader.readAsDataURL($file);
            }
        }
    });

    $(".showimgpreview").on('change',function(){
        $temp = $(this).get(0).files;
        $target = $(this).parents('.postbox').find($(this).attr('target'));
        $(".postsubmit").text('Upload in progress');
        $(".postsubmit").prop('readonly',true);
    for($i=0;$i<$temp.length;$i++)
        {
            var $file = $temp[$i];
            $file['identifier'] = 'imgpreveiw_'+getRandomInt(1000,9999);
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
        return function (e) {
            $tempid = theFile['identifier'];
                    $('<img/>',{'id':$tempid}).css({}).appendTo($target);
                    $("#"+$tempid).wrap("<div class='img-frame' ></div>");
            $temp3 = $("#"+$tempid).parent();
            //console.log($temp3);
            $temp3.append('<div class="imgpreviewprogress imgpreviewoptions" "data-status"="start">Upload Progress</div>');
            $temp3.append('<div class="imgpreviewdelete imgpreviewoptions">Delete</div>');
            //console.log(theFile);
            $temp3.append('<input type="hidden" name="photoes[]" id="field_'+$tempid+'" class="imgpreviewupload"/>');
            var $temp2 = $("#"+$tempid).get(0);
                    $temp2.src = e.target.result;
                    $temp2.onload = function () {
                        //console.log('image loaded');
            formdata = false;
            if (window.FormData) {
                formdata = new FormData();
            }
            formdata.append("file[]", theFile);
            if (formdata) {
            $.ajax({
                url: appendAjaxDefaultParameters(root_path+'PSAjax.php?type=imageuploadajax2&boxid=field_'+theFile['identifier']),
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function(data) {
                $res = $.parseJSON(data);
                //console.log($res);
                if($res.status == 405)
                loginModal('');
                else if($res.status == 200)
                {
                    $x = $('body').find('#'+$res['boxid']);
                    $x.val($res['list']['photoes'][0]);
                    $temp = $x.parents('.postbox').find('.imgpreviewupload');
                    if($temp.filter(function(index){return $(this).val() != ''}).length == $temp.length)
                    {
                        $(".postsubmit").text('POST');
                        $(".postsubmit").prop('readonly',false);
                    }
                }
                },       
                error: function(res) {

                }       
              });
            }
            
                    }
                    // Render thumbnail.
                };
            })($file);

            /*var reader  = new FileReader();
            reader.addEventListener("load", function () {
              //console.log('loading');
              $tempid = 'imgpreveiw_'+getRandomInt(1000,9999);
              $('<img/>',{id:$tempid}).appendTo($target);
              $("#"+$tempid).wrap("<div class='img-frame' style='max-height:50px;max-width:50px'></div>");
              var $temp2 = $("#"+$tempid).get(0);
              //console.log($temp2);
              //console.log(reader.result);
              $temp2.src = reader.result;
              $temp2.onload = function () {
                 //console.log(this);
                //if($callback != '')
                //window[$callback]($targetSelector,$index);
                //console.log('loaded image');
              }
              
            }, false);
            */
            if ($file) {
              reader.readAsDataURL($file);
            }
        }
    });
    
    //Google Analytics 
    if(server == 'live')
    {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        if(profile_id)
        {
        ga('create', 'UA-75357427-1', 'auto',{userId:profile_id.replace('profile-','').replace('profile_','')});
        }
        else
        {
        ga('create', 'UA-75357427-1', 'auto');
        }
        /*
        var canonical_link;
        try{
          canonical_link = $('link[rel=canonical]').attr('href').split(location.hostname)[1] || undefined;
        }
        catch(e){
          canonical_link = undefined;
        }
        ga('set', 'page', canonical_link);
        */
    //ga('create', 'UA-75357427-1', 'auto');
        ga('send', 'pageview');
    
    $('body').on('click','.btn',function(){
        $temp = $(this);
        if($temp.is('button') || $temp.is('a'))
        {
            ga('send', 'event', 'btn_click', q['pagetype'], $temp.text(), 1, {'NonInteraction': 1});
        }
        else if($temp.is('input'))
        {
            ga('send', 'event', 'btn_click', q['pagetype'], $temp.val(), 1, {'NonInteraction': 1});
        }
    });
    
    if(typeof onLoadGAEvents != 'undefined'){
        $.each(onLoadGAEvents,function(i,v){
            try{
                if(typeof v == 'string')
                gaSendEvent(v,'onLoadGAEvents',1);
                else
                gaSendEvent(v['action'],'onLoadGAEvents',(typeof v['value'] == 'undefined')?1:v['value']);
            }
            catch(e){}
        })
    }
    }
    if(isMobile == true)
    {
        $(".mobnav").removeClass('hide');
    }
    
    $("body").on('click','.ratingwrapper span',function(){
            $parent = $(this).parent();
            $id = $parent.attr('data-id');
            $type = $parent.attr('data-type');
            $rating = $(this).attr('data-rating');
            if($id && $type && $rating){
                    $.post(root_path+'PSAjax.php?type=submitrating', {id:$id,type:$type,rating:$rating,boxid:$(this).attr('id')}, function(data){ 
                        $res = $.parseJSON(data);
                        if($res['status'] == 200)
                        {
                            $parent.html($res['data']);
                        }
                    });
            }
    });
    $("body").on('keypress',"input[type='number']",function(event){
        var keyCode = event.keyCode;
        if(!(keyCode == 38 || keyCode == 40 || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)))
        event.preventDefault();
    });
    
    $('.btn-number').click(function(e){
        e.preventDefault();
        
        var type      = $(this).attr('data-type');
        var input = $(this).parents('.input-group').eq(0).find('.input-number').eq(0);
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
            input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
            $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
            input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
            $(this).attr('disabled', true);
            }

        }
        } else {
        input.val(0);
        }
    });
    $('.input-number').focusin(function(){
       $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {
        
        var minValue =  parseInt($(this).attr('min'));
        var maxValue =  parseInt($(this).attr('max'));
        var valueCurrent = parseInt($(this).val());
        
        if(valueCurrent >= minValue) {
        $(this).parents('.input-group').eq(0).find(".btn-number[data-type='minus']").removeAttr('disabled')
        } else {
        //alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
        $(this).parents('.input-group').eq(0).find(".btn-number[data-type='plus']").removeAttr('disabled')
        } else {
        //alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
        }
        
        
    });
    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        });
    
    $("body").on('click','.imgpreviewdelete',function(){
       $(this).parents('.img-frame').remove();
    });
    $(document).on('click','.delete-impg-preview',function(){
       $(this).parents('.item').remove();
    });
    $('body').on('click','a[role="tab"],a[data-toggle="collapse"],a[data-toggle="tab"]',function(){
        delayfunction2(['jquery.unveil.js'],'jqueryunveil_callback','',1000);
        $temp = $('#'+$(this).attr('href').replace('#',''));
                $temp2 = $temp.find('.modaliframe');
        if($temp2.length>0)
        {
            $temp2.attr('height','');
            setTimeout(function(){
                internaliframid = $temp2.attr('id');
                iframeLoaded();
            },300);
        }
                else
                {
                    try{
                        if($temp.data('frame'))
                        $temp.find(".panel-body").html(calliframe($temp.data('frame')));
                    }
                    catch(e){
                        
                    }
                }
        setTimeout(function(){
            $(window).trigger('resize');
            if (typeof map != 'undefined')
            {
                try {
                google.maps.event.trigger(map, 'resize');
                }catch(err) {
                 //   document.getElementById("demo").innerHTML = err.message;
                }
            }
        },300);
        

        //jqueryunveil_callback();
        //triggerunveil();
    });
    $('body').on('click','.play', function() {
        var iframe_url = $(this).attr('vsrc');
        var iframeid = 'cyiframe_'+getRandomInt(1000,9999);
        if ($(this).data('params')) iframe_url+='&'+$(this).data('params');
        iframe_url += iframe_url+"&modestbranding=1&rel=0&autoplay=1&autohide=1&enablejsapi=1";
        var iframe = $('<iframe/>', {'frameborder': '0', 'src': iframe_url, 'width': $('.flexi-video').width(), 'height': $('flexi-video').height(),'id':iframeid,allowfullscreen:true });
        $(this).replaceWith(iframe);
        /*if(isMobile)
        {
            player = new YT.Player(iframeid, {
            events: {
              'onReady': onPlayerReady,
              'onStateChange': onPlayerStateChange
            }
            });
        }*/
    });
    $('.sharepage').on('click',function(){
        sharepage($(this).attr('data-type'));
    });
    $('body').on('click','.customshare',function(){
        $temp = {};
        $temp['title'] = $(this).data('title');
        $temp['description'] = $(this).data('description');
        $temp['image'] = $(this).data('image');
        $temp['url'] = $(this).data('shareurl');
        $isdialog = ($(this).data('dialog'))?$(this).data('dialog'):0;
        sharepage($(this).attr('data-type'),$temp,$isdialog);
    });
    
    if(isMobile == true || $(document).width() < 990)
        {
        $(".sidebar-right,.sidebar").insertBefore('.wall');
        }
    delayfunction2(['hoverpopup.js'],'hovercardattach','',1000);
    psanalytics();
    
    $("body").on('click','#min_register_btn',function(){
        $(".error-text").hide();
        $tempemail = $("#minregisteremail").val();
        $tempname = $("#minregistername").val();
        $tempdob = $("#minregisterdob").val();
        $tempgender = $("#minregistergender").val();
        $temppassword = $("#minregisterpassword").val();
        $temppassions = $("#minregisterpassions").val();
        if($tempemail == '' || $temppassword == '' || $tempname == '')
        $(this).parents(".login_form").find(".error-text").html('Please provide Valid Input').show();
        else
        {
                    $("#min_register_btn").prop('disabled','true');
            $.post(root_path+'PSAjax.php?type=minimal_registration_process', {name:$tempname,email:$tempemail,$dob:$tempdob,$gender:$tempgender,password:$temppassword,passions:$temppassions,boxid:$(this).attr('id')}, function(data){ 
            $res = $.parseJSON(data);
            $("#"+$res['boxid']).parents(".login_form").find(".error-text").html($res['data']).show();
            if($res['login'] == 1)
            loginCallback($res);
            });
        }
    });
        $("body").on('keydown', '.switchslider',function(e){
            e.stopPropagation();
            if(e.which==13 || e.which==32)
            {
                e.preventDefault();
                if($(this).parent().find('input[readonly]').length == 0)
                {
                $(this).click();
                }
            }
        });
        $("body").on("click",".switchslider",function(e){
            e.stopPropagation();
           if($(this).parent().find('input[readonly]').length > 0)
           {
               e.preventDefault();
           }
        });
        
        $('[data-toggle="tooltip"]').tooltip(); 
        
    if(window.location.hash) {
        var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
        var hash1 = hash.split('-');
        setTimeout(function(){
            if(hash1[0])
            {
                scrollTo("#"+hash1[0]);
                scrollTo("."+hash1[0]);
                $('a[href="#'+hash1[0]+'"]').trigger('click');
            }
            if(hash != hash1[0])
            {
                scrollTo("#"+hash);
                scrollTo("."+hash);
                $('a[href="#'+hash+'"]').trigger('click');
            }
        
        },2000);
    } else {
    }
}
function onContentAdd(){
    $(".reltime:not(.reltimerendered)").each(function(i,v){
        $(this).html(timeSince($(this).attr('data-time')));
        $(this).addClass('reltimerendered');
    });
    $("section:not(.sectionpaddingadjusted)").each(function(i,v){
        if($.trim($(this).text()) == '')
        {
        $(this).css('padding','0px');
        $(this).find('.module').css('padding','0px');
        $(this).addClass('hide');
        }
        $(this).addClass('sectionpaddingadjusted');
        
    });
    $(".module:not(.modulepaddingadjusted)").each(function(i,v){
        if($.trim($(this).text()) == '')
        {
        $(this).css('padding','0px');
        $(this).addClass('hide');
        }
        $(this).addClass('modulepaddingadjusted');
        
    });
    
    $(".img_set:not(.extraImagesAdded)").each(function(){
        $(this).find('a').last().append($(this).find('._ex'));
        $(this).addClass('extraImagesAdded');
    });
    
    prepare_youtube_frames();
    delayfunction2(['jquery.unveil.js'],'jqueryunveil_callback','',1000);
    delayfunction2(['jBox.min.js'],'jBoxmin_callback','',1000);
    delayfunction2(['jquery.nicescroll.min.js'],'jquerynicescrollmin_callback','',1000);
    //jqueryunveil_callback();
}
$(".invite").click(function(){
    hello(sns).api('me/friends').then(function(r) {
        
    });
});
function sessionStart(auth){
    //console.log(auth);
    hello(auth.network).api('/me').then(function(r) {
        $.post(root_path+'PSAjax.php?type=loginsns',{network:auth.network,data:r}, function(data){ 
            //console.log(data);
        $res = $.parseJSON(data);
            loginCallback($res);
        });
    });
    
}

var newwindow;
var newwindowTimer;
var newwindowTarget;
function popitup(name,url,trgt) {
        newwindow=window.open(url,name,'height=600,width=600,toolbar=no,titlebar=no,status=no,resizable=no,menubar=no,location=no,top=100,left=300,screenX=300,screenY=100');
    if(trgt!='')
        {
            newwindowTarget = trgt;
            windowWatcher();
        }
        if (window.focus) {newwindow.focus()}
        return false;
}
function windowWatcher(){
    newwindowTimer = setInterval(function(){
        $crntwindow = window['window']['$window'][0]['newwindow']['location']['href'];
        if($crntwindow.indexOf(newwindowTarget) > -1)
        {
            clearInterval(newwindowTimer);
            newwindow.close();
            location.assign(root_path+'home');
        }
    }, 1000);
}
function reloadwindow(){
    location.replace(location.href);
}
function calliframe(targetsrc){
    targetsrc = appendAjaxDefaultParameters(targetsrc);
    internaliframid = 'iIframe_'+getRandomInt(1000,9999);
    return "<iframe class='modaliframe hide' id='"+internaliframid+"' src='"+targetsrc+"' frameborder=0 scrolling='no' style='border:0px;width:100%'></iframe>";
}
function callmodaliframe(title,targetsrc,preloadtext){
    targetsrc = appendAjaxDefaultParameters(targetsrc);
    if(!(preloadtext))
    preloadtext = 'Loading';
    preloadtext = '<div class="iframepreloaddiv" style="display: table;table-layout: fixed;width: 100%;height:200px;"><div class="iframepreloadtext" style="display:table-cell;vertical-align:middle;text-align:center;">'+preloadtext+'</div></div>';
    myModal(title,preloadtext+calliframe(targetsrc),"cframe");
}

function loadPage(url){
    window.location.href = root_path + url;
}
function stripQueryString(url) {
  if(profile_id == 'profile_1457162238_911787')
  return url;
  else
  return url.split("?")[0].split("#")[0];
}
function overRideHistoryState(url,title){ 
    try{
        if(!url) url = window.location;
        if(!title) title = $(document).find("title").text();
        var State = History.getState();
        /* 
        History.log('initial:', State.data, State.title, State.url);
        History.Adapter.bind(window,'statechange',function(){ 
            var nState = History.getState();
            History.log('statechange:', nState.data, nState.title, nState.url);
        }); */
        History.replaceState({}, title, url);
    } catch(e){ 
    //console.log(e); 
    }
}
function sharepage($type,$metaObj,$dialog){
    var tags = [];
    var twtags = [];
    var fbtags = [];
    if($metaObj && $metaObj['tags'])
    tags = $metaObj['tags'];
    else
    tags = meta['keywords'].split(',');
    tags.unshift('passionstreet');
    var taggs = [];
    // for (i = 0; i < tags.length; i++) { 
    for (i = 0; i < 5; i++) { 
    v = tags[i];
    if(v)
    taggs.push(v.replace('#','').replace(' ',''));
    };
    for (i = 0; i < taggs.length; i++) { 
    v = taggs[i];
    twtags.push(v.replace('#',''))
    };
    for (i = 0; i < taggs.length; i++) { 
    v = taggs[i];
    fbtags.push('#'+v);
    };
    if(!($metaObj))
    {
        $url = orgurl;
        $title = meta['title'];
        $description = meta['description'];
        $image = (meta['image'][0])?meta['image'][0]:'';
    }
    else{
        $url = $metaObj['url'];
        $title = $metaObj['title'];
        $description = $metaObj['description'];
    $image = ($metaObj['image'])?$metaObj['image']:'';
    }
    $image = $image.replace('/thumb','');
    $url = encodeURIComponent($url);
    $title = encodeURIComponent($title);
    $description = encodeURIComponent($description);
    $via = clientid['twitterhandle'];
    //$shareurl = 'http://www.facebook.com/dialog/share?app_id='+clientid['facebook']+'&display=popup&href='+$url+'&title='+$title+'&description='+$description+'&picture='+$image+'&hashtag='+fbtags[0];
    //$shareurl = 'http://www.facebook.com/sharer.php?u='+$url;
    //$shareurl = 'http://www.facebook.com/dialog/feed?app_id='+clientid['facebook']+'&display=popup&link='+$url+'&caption='+$title+'&description='+$description+'&picture='+$image+'&hashtag='+fbtags[0];
    //$url = 'https://plus.google.com/share?url=google.com&hl=en&title=_TITLE_';
    if($type == 'fb')
    {
	console.log('Fb share');
	$shareurl = 'http://www.facebook.com/sharer.php?u='+$url;
        if($dialog)
	$shareurl = 'http://www.facebook.com/dialog/feed?app_id='+clientid['facebook']+'&display=popup&link='+$url+'&caption='+$title+'&description='+$description+'&picture='+$image+'&hashtag='+fbtags[0];
	console.log($shareurl);
    }
    else if($type == 'tw')
    $shareurl = 'http://twitter.com/share?url='+$url+'&text='+$title+'&hashtags='+twtags.join(',')+'&via='+$via;
    else if($type == 'gg')
    $shareurl = 'https://plusone.google.com/_/+1/confirm?hl=en&url='+$url;
    popitup('sharewindow',$shareurl,'');    
}
function psajaxcallback(data){
    //console.log(data);
    if(data['boxid'] != '')
    {
        if(status_codes[data['status']] == 'Success');
        {
            $("#"+data['boxid']).html(data['data']);
        }
        if(data['class'])
        {
            if(data['class']['add'])
            $("#"+data['boxid']).addClass(data['class']['add']);
            else if(data['class']['remove'])
            $("#"+data['boxid']).removeClass(data['class']['remove']);
        }
    }
}
function psajax($obj)
{
    var $params = {};
    $action = $obj.attr('data-action');
    $type = $obj.attr('data-t');
    $ctemp = $obj.attr('data-c');
    $cparams = [];
    $c = '';
    if($ctemp !== undefined)
    {
        $ctemp = $ctemp.split(',');
        $c = $ctemp[0];
        $cparams = $ctemp;
    }
    if($c == '')
        $c = 'psajaxcallback';
    $ps = $obj.attr('data-p-str');
    $parray = $ps.split(',');
    $.each($parray,function($i,$v){
        $params[$v] = $obj.attr('data-'+$v);
    });
    if($obj.attr('data-trgtid') !== undefined)
    $params['boxid'] = $obj.attr('data-trgtid');
    else if($obj.attr('id') !== undefined)
    $params['boxid'] = $obj.attr('id');
    else{
            $obj.attr('id','ajx-id-'+getRandomInt(1000,9999));
            $params['boxid'] = $obj.attr('id');
    }
    $.post(root_path+'PSAjax.php?type='+$type, $params, function(data){ 
        //window[$c]($.parseJSON(data));
    //var $x = [];
        //$x.push($.parseJSON(data));
        //executeFunctionByName($c,$x);
    $cparams[0] = $.parseJSON(data);
    executeFunctionByName($c,$cparams);
    });
}
function myModal($title,$content,$class)
{
    closemyModal();
    //setTimeout(function(){
    $("#modal-box h2").html($title);
    $("#modal-box .modal-dialog").prepend('<button type="button" class="close" onclick="closemyModal()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>');
    $("#modal-box .modal-container").html('<div class="row"><div class="col-md-12">'+$content+'</div></div>');
    $("#modal-box .modal-dialog").removeClass("login-box");
    $("#modal-box").addClass($class).modal();
    //},1000);
}
function closemyModal(){
    $("#modal-box").modal('hide');
    $('body').removeClass('modal-open');
    $(".modal-backdrop").remove();
}
function loginModal($class)
{
    closemyModal();
    login_callback = '';
    $topoffset = $(".navbar-fixed-top").height();
    //$("#modal-box").addClass($class).modal({backdrop: 'static',keyboard: false});
    $("#modal-box").addClass($class).removeClass('cframe');
    $("#modal-box").html(loginform).modal({backdrop: 'static',keyboard: false});
    $(".modal-backdrop").css({"opacity":"0.95","margin-top":$topoffset+"px"});
    $(".modal").css({"margin-top":$topoffset+"px"});
    $(".modal-dialog").css({"margin-top":"5%"}).addClass("login-box");
    $(".login-btn").addClass("hide");
    $("#toggleloginoptions").addClass('hide');
    $(".login-box h2").html('LOGIN TO PASSIONSTREET');
    $(".login_form h5").removeClass('hide');
    $(".login_form .new").removeClass('hide');
}
function loginModalMinimal($class)
{
    closemyModal();
    login_callback = minimal_login;
    $topoffset = $(".navbar-fixed-top").height();
    //$("#modal-box").addClass($class).modal({backdrop: 'static',keyboard: false});
    $("#modal-box").addClass($class).removeClass('cframe');
    $("#modal-box").html(loginform).modal({backdrop: 'static',keyboard: false});
    $("#modal-box .new a").attr('href','javascript:void(0);');
    $("#modal-box .new a").on('click',function(){
        $(".lgnfrm-clm-1").toggleClass('hide');
        
    });
    $(".modal-backdrop").css({"opacity":"0.95","margin-top":$topoffset+"px"});
    $(".modal").css({"margin-top":$topoffset+"px"});
    $(".modal-dialog").css({"margin-top":"5%"}).addClass("login-box");
    $(".login-btn").addClass("hide");
    $("#toggleloginoptions").removeClass('hide');
    $(".login-box h2").html('LOGIN TO PROCEED');
    $(".login_form h5").addClass('hide');
    $(".login_form .new").addClass('hide');
}
function initGoogleMaps() {
  map_position_obj = $.parseJSON(map_position);
  map = new google.maps.Map(document.getElementById('map'), {
    center: map_position_obj,
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var markers = [];
  marker = new google.maps.Marker({
    map: map,
    title: 'Connaught Place, New Delhi, India',
    position: map_position_obj
  });
}

function calculateAndDisplayRoute(directionsService, directionsDisplay,start , stop , waypts) {
  
  directionsService.route({
    origin: start,
    destination: stop,
    waypoints: waypts,
    optimizeWaypoints: true,
    provideRouteAlternatives:true,
    // travelMode: google.maps.TravelMode.DRIVING
    travelMode: google.maps.TravelMode.WALKING
    // travelMode: google.maps.TravelMode.TRANSIT
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
        $img = routemaptoimage(routemap);
        $("#route_pic").val($img);
      } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

function maptoimage(){
    var currentPosition=map.getCenter();
    return "http://maps.google.com/maps/api/staticmap?key="+google_maps_key+"&scale=2&sensor=false&center=" +
      currentPosition.lat() + "," + currentPosition.lng() +
      "&zoom="+map.getZoom()+"&size=300x200&markers=color:red|" +
      currentPosition.lat() + ',' + currentPosition.lng();
}
function getDataURIfromFile($fileObject,$index,$targetSelector,$callback){
    var $file = $fileObject.get(0).files[$index];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $temp = $($targetSelector).get(0);
      $temp.src = reader.result;
      $temp.onload = function () {
        if($callback != '')
        window[$callback]($targetSelector,$index);
      }
    }, false);

    if ($file) {
      reader.readAsDataURL($file);
    }
}
function addCropper($imgSelector,$index){
    $imgObj = $($imgSelector);
    $previewX = $imgObj[0]['width'];
    $previewY = $imgObj[0]['height'];
    $orgX = $imgObj[0]['naturalWidth'];
    $orgY = $imgObj[0]['naturalHeight'];
    $multicrop['currentActive'] = $index;
    $multicrop['resizeRatio'][$multicrop['currentActive']] = $previewX / $orgX;
    $imgcroptype = $multicrop['currentFileSelector'].attr('croptype');
    if($(".multicropdata").length ==0 )
    {
    $multicrop['currentFileSelector'].after('<div class="multicropdata"></div>');
    $(".multicropdata").append("<input type='hidden' id='multicropdata_"+$multicrop['currentFileSelector'].attr('id')+"_"+$index+"' name='multicropdata["+$multicrop['currentFileSelector'].attr('name').replace("[]","")+"]["+$index+"]' value='' />");
    }
    if(croptype[$imgcroptype]['aspect'] !== undefined){
        $allowedaspects = croptype[$imgcroptype]['aspect']; 
        $orgaspect = $orgX / $orgY;
        $currentaspect = 2;
        $.each($allowedaspects ,function(i,v){
            if( $orgaspect >= $allowedaspects[i] && $orgaspect < $allowedaspects[i+1])
            {
                $currentaspect = $allowedaspects[i];
            }
        });
    }
    else
    {
        $currentaspect =  (croptype[$imgcroptype]['size'][0] / croptype[$imgcroptype]['size'][1]);
    }
    
    $imgObj.Jcrop({
        minSize: [32, 32], // min crop size
        maxSize: [1000,1000],
        aspectRatio : $currentaspect, // keep aspect ratio 1:1
        bgFade: true, // use fade effect
        bgOpacity: .3, // fade opacity
        setSelect:[0,0,$orgX,$orgY],
        onChange : updateCordinate,
        onSelect : updateCordinate
        }, function(){

        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];

        jcrop_api = this;
    });
    $(".multicropper").on("mouseenter",".jcrop-holder",function(){
        $(".jcrop-holder").each(function(i,v){
            if($(this).is(":visible")){
                $multicrop['currentActive'] = i;
            }
        });
    });
}
function addCropper2($imgSelector,$index){
    $multicrop['currentActive'] = $index;
    //$multicrop['resizeRatio'][$multicrop['currentActive']] = $previewX / $orgX;
    if($(".multicropdata").length ==0 )
    {
    $multicrop['currentFileSelector'].after('<div class="multicropdata"></div>');
    $(".multicropdata").append("<input type='hidden' id='multicropdata_"+$multicrop['currentFileSelector'].attr('id')+"_"+$index+"' name='multicropdata["+$multicrop['currentFileSelector'].attr('name').replace("[]","")+"]["+$index+"]' value='' />");
    }
    $tab = $($imgSelector.replace('multiimagecrop','imgcroppertab'));
    $tab.on('click',function(){
    addCropper2Helper();
    });
    addCropper2Helper();
}
function addCropper2Helper(){
   setTimeout(function(){ 
    $(".imgcroppertab").each(function(i,v){
        if($(this).hasClass('active'))
        {
            $multicrop['currentActive'] = $(this).attr('id').replace('imgcroppertab','');
            $multicrop['resizeRatio'][$multicrop['currentActive']] = 1;
            $imgcroptype = $multicrop['currentFileSelector'].attr('croptype');
            $aspectRatio = croptype[$imgcroptype]['sizes'][0][0] / croptype[$imgcroptype]['sizes'][0][1]; 
            if(!($(this).data('cropperadded')))
            {
                $(this).data('cropperadded',1);
                $cropperboxid = "#"+$(this).attr('id').replace('imgcroppertab','imgcropper');
                croppernew($cropperboxid,updateCordinate,$multicrop,$aspectRatio);
            }
        }
    });
   }, 200);
}
function updateCordinate(c){
    //console.log(c);
    $multicrop['coordinates'][$multicrop['currentActive']] = c;
    var d = [];
    d['x'] = c['x'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['y'] = c['y'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['x2'] = c['x2'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['y2'] = c['y2'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['w'] = c['w'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    d['h'] = c['h'] / $multicrop['resizeRatio'][$multicrop['currentActive']];
    var e = d['x']+'_'+d['y']+'_'+d['x2']+'_'+d['y2']+'_'+d['w']+'_'+d['h'];
    $("#multicropdata_"+$multicrop['currentFileSelector'].attr('id')+"_"+$multicrop['currentActive']).val(e);
}
function fileSelectHandler() {

    // get selected file
    //console.log($currentselector);
    var oFile = $currentselector[0].files[0];
    $('.error').hide();

    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (! rFilter.test(oFile.type)) {
        $('.error').html('Please select a valid image file (jpg and png are allowed)').show();
        return;
    }

    // check for file size
    if (oFile.size > 250 * 1024) {
        //$('.error').html('You have selected too big file, please select a one smaller image file').show();
        //return;
    }

    // preview element
    var oImage = document.getElementById('cropper');

    // prepare HTML5 FileReader
    var oReader = new FileReader();
        oReader.onload = function(e) {
        oImage.src = e.target.result;
        oImage.onload = function () { // onload event handler
            
            $orgwidth = oImage.naturalWidth;
            $orgheight = oImage.naturalHeight;
            $scaledwidth = $orgwidth;
            $scaledheight= $orgheight;
            if($orgwidth > 500) {
                $scaledwidth = 500;
                $scaledheight = $scaledwidth * $orgheight / $orgwidth;
            }
            $('#cropper').width($scaledwidth);
            $('#cropper').height($scaledheight);
            
            if (typeof jcrop_api != 'undefined') {
                jcrop_api.destroy();
                jcrop_api = null;
            }
            var twidth = $currentselector.attr('org-width');
            var theight = $currentselector.attr('org-height');
            var aspect = twidth / theight;
            var marginwidth = 10 * twidth /100;
            var marginheight = 10 * theight /100;
            var setselect = [10,10,oImage.naturalWidth-10,oImage.naturalHeight-10];
            setTimeout(function(){
                $('#cropper').Jcrop({
                    minSize: [32, 32], // min crop size
                    maxSize: [1000,1000],
                    aspectRatio : aspect, // keep aspect ratio 1:1
                    bgFade: true, // use fade effect
                    bgOpacity: .3, // fade opacity
                    setSelect:setselect,
                    onChange : updatePreview,
                    onSelect : updatePreview
                    }, function(){

                    var bounds = this.getBounds();
                    boundx = bounds[0];
                    boundy = bounds[1];

                    jcrop_api = this;
                });
            },1000);

        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
}
function updatePreview(c) {
    if(parseInt(c.w) > 0) {
        // Show image preview
        var imageObj = $("#cropper")[0];
        var canvas = $("#preview")[0];
        var context = canvas.getContext("2d");
        canvas.width = $currentselector.attr('org-width');
        canvas.height = $currentselector.attr('org-height'); 
        context.drawImage(imageObj, c.x * $orgwidth/$scaledwidth, c.y * $orgheight/$scaledheight, c.w * $orgwidth/$scaledwidth, c.h * $orgheight/$scaledheight, 0, 0, canvas.width, canvas.height);
    }
    $("#crop-done").removeClass('hide');
};

function ajaxUpload(){
    var canvas = $("#preview")[0];
    var dataURL = canvas.toDataURL();
    var pictype = $currentselector.attr('pictype');
    $.post(root_path+'PSAjax.php?type=imageuploadajax', {imgBase64: dataURL,pictype:pictype}, function(data){ 
        $res = $.parseJSON(data);
        if('.'+$currentselector.attr('org-selector') !== undefined)
        $('.'+$currentselector.attr('org-selector')).attr('src',upload_path+$res['data']);
        $(".jcrop-holder").remove();
        $("#cropper").removeAttr("src");
        $("#crop-done").addClass('hide');
        $("#modal-box").modal("hide");
    });
}
function filetopost(){
    var canvas = $("#preview")[0];
    var dataURL = canvas.toDataURL();
    $("#"+$currentselector.attr('id')+'_post').remove();
    $currentselector.closest('form').append(
        $('<input>').attr({
            type: 'hidden',
            id: $currentselector.attr('id')+'_post',
            name: $currentselector.attr('name')+'_post',
            value:dataURL
        })
    );
    $(".jcrop-holder").remove();
    $("#cropper").removeAttr("src");
    $("#crop-done").addClass('hide');
    $("#modal-box").modal("hide");
    
}
function iframeLoaded() {
    //console.log('height balancing '+internaliframid);
    var iFrameID = document.getElementById(internaliframid);
    if(iFrameID) {
          // here you can make the height, I delete it first, then I make it again
          //iFrameID.height = "";
          // iFrameID.height = iFrameID.contentWindow.body.scrollHeight + "px";
      iFrameID.className = iFrameID.className.replace(/\bhide\b/,'show');
      $(".iframepreloaddiv").addClass('hide');
          // iFrameID.height = (iFrameID.contentWindow.document.body.scrollHeight - 20);
      $y = iFrameID.contentWindow.document.body.scrollHeight;
          //console.log(iFrameID.contentWindow.document.body.scrollHeight);
      iFrameID.height = ($y);
    }   
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function customToggle($selector){
    $temp = $($selector);
    if($temp.hasClass("hide")){
        $temp.removeClass(("hide"));
    }
    else if($temp.hasClass("show")){
        $temp.removeClass(("show"));
    }
    else
    $($selector).toggle();
}
function javascriptpost(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }
    body = document.getElementsByTagName('body')[0];
    body.appendChild(form);
    form.submit();
}


function timeSince(ts){
    now = new Date();
    ts = new Date(ts*1000);
    var delta = now.getTime() - ts.getTime();
    delta = delta/1000; //us to s
    var ps, pm, ph, pd, min, hou, sec, days;

    if(delta<=59){
        ps = Math.floor((delta>1)) ? "s": "";
        return "just now"
    }

    if(delta>=60 && delta<=3599){
        min = Math.floor(delta/60);
        sec = Math.floor(delta-(min*60));
        pm = (min>1) ? "s": "";
        ps = (sec>1) ? "s": "";
        //return min+" minute"+pm;
        return min+" minute"+pm+" ago";
    }

    if(delta>=3600 && delta<=86399){
        hou = Math.floor(delta/3600);
        min = Math.floor((delta-(hou*3600))/60);
        ph = (hou>1) ? "s": "";
        pm = (min>1) ? "s": "";
        return hou+" hour"+ph+" ago";
    } 

    if(delta>=86400 && delta<604800){
        days = Math.floor(delta/86400);
        hou =  Math.floor((delta-(days*86400))/60/60);
        pd = (days>1) ? "s": "";
        ph = (hou>1) ? "s": "";
        return days+" day"+pd+" ago";
    }
    if(delta>=604800){
        $temp =timeConverter(ts/1000); //unix time stamp
        return $temp['d']+'<sup>'+nth($temp['d'])+'</sup> '+$temp['M']+' '+$temp['y'];
    }
}


function timeConverter(UNIX_timestamp){
     var datecomponents = {};
     var a = new Date(UNIX_timestamp*1000);
     datecomponents['d'] = a.getUTCDate();
     datecomponents['m'] = a.getUTCMonth();
     datecomponents['M'] = monthnamesfull[datecomponents['m'] + 1];
     datecomponents['y'] = a.getUTCFullYear();
     datecomponents['h'] = a.getUTCHours();
     datecomponents['i'] = a.getUTCMinutes();
     datecomponents['s'] = a.getUTCSeconds();
     return datecomponents;
 }
function standardDate(str) //2017-01-01 to javascript date
{
    var dateStr=str; //returned from mysql timestamp/datetime field
    var a=dateStr.split(" ");
    var d=a[0].split("-");
    var t=a[1].split(":");
    var date = new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
    return date;
}
function getFormattedDate(date) { //javascript date to 2017-01-01
  var year = date.getFullYear();
  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;
  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  //return month + '/' + day + '/' + year;
  return year + '-' + month + '-' + day;
}
 
function nth(d) {
  if(d>3 && d<21) return 'th'; // thanks kennebec
  switch (d % 10) {
        case 1:  return "st";
        case 2:  return "nd";
        case 3:  return "rd";
        default: return "th";
    }
} 

function fetchExtUrlMeta($url,$callback){
    /*$x = encodeURI('select * from html where url="'+$url+'" and xpath in ("//title","//meta","//img","//h1","//h2")');
    $fetchurl = 'http://query.yahooapis.com/v1/public/yql?q='+$x+'&format=json&diagnostics=true&callback=?';
    $fetchedUrl = $.getJSON($fetchurl,function(data){
        $meta = data['query']['results']['meta'];
        $metalist = {};
        if(data['query']['results']['title']['content'] !== undefined)
        $metalist['title'] = data['query']['results']['title']['content'];
        else
        $metalist['title'] = data['query']['results']['title'];
        
        $metalist['img'] = data['query']['results']['img'];
    */
    //$x = encodeURI($url);
    $x = encodeURIComponent($url);
    $fetchurl = root_path+'/PSAjax.php?type=fetchExtUrl&url='+$x;
    $fetchedUrl = $.getJSON($fetchurl,function(data){
        $metalist = data['list'];
        
        var $temp = document.createElement('a');
        $temp.href = $url;
        $metalist['host'] = $temp.hostname;
        $metalist['url'] = $url;
        
        window[$callback]($metalist);
    });
}
function youtube_parser(url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    return (match&&match[7].length==11)? match[7] : false;
}
function prepare_youtube_frames(){
    /*if(isMobile || q['pagetype'] == 'index')
        {
        var tag = document.createElement('script');
        tag.id = 'iframe-demo';
        tag.src = 'https://www.youtube.com/iframe_api';
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }*/
    $(".flexi-video:not(.playAdded)").each(function() {
        if($(this).has('.play').length == 0)
        $(this).append($('<div/>', {'class': 'play','vsrc':$(this).attr('vsrc')}));
        $(this).addClass('playAdded');
    });
}
  var player;
  function onYouTubeIframeAPIReady() {
    player = new YT.Player('promotionalvideo', {
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChangePromoVideo
        }
    });
  }
  function onPlayerReady(event) {
    event.target.playVideo();
    //player.playVideo();
    
  }
  function onPlayerStateChangePromoVideo(event) {
    if(event.data == 0)
    event.target.playVideo();
  }
  function onPlayerStateChange(event) {
    //changeBorderColor(event.data);
  }
function isEnterPressed(e){
    var keycode=null;
    if (e!=null){
        if (window.event!=undefined){
            if (window.event.keyCode) keycode = window.event.keyCode;
            else if (window.event.charCode) keycode = window.event.charCode;
        }else{
            keycode = e.keyCode;
        }
    }
    if(keycode == 13 && !(e.shiftKey ||e.ctrlKey)){
        e.preventDefault();
        return true;
    }
    else
        return false;
}

function copyToClipboard(content) {
    //console.log(content);
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(content).select();
    document.execCommand("copy");
    $temp.remove();
}

function defercallfunction(x,obj,param){
    if($temp2['fn'][x] === undefined)
    {
        if($(window)[x] === undefined)
        {
            $temp2['tm'][x] = setInterval(function(){
                if($(window)[x] !== undefined || $temp2['fn'][x] !==undefined)
                {
                    //console.log($temp2)
                    clearInterval($temp2['tm'][x]);
                    $temp2['fn'][x] = 1;
                    obj[x](param);
                }
            },2000);
        }
    }
    else
    {
        obj[x](param);
    }
}

function fndefined(x){
    if(($(window)[x] !== undefined) || (window[x] !== undefined))
    return true;
    else
    return false;
}
function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}
function render_card(type,entity)
{
    var $entitydetail = '';
    $x = '';
    if(type == 'profile')
    {
    if(typeof users === 'undefined' || users == null)
    return;
        $entitydetail = users[entity];
        $x1 = '';
        $x2 = '';
        $passiondisplaycount = 0;
        if('passion' in $entitydetail)
        {
            if('isExpert' in $entitydetail['passion'])
            {
                $.each($entitydetail['passion']['isExpert'],function(i,v){
                    if($passiondisplaycount > 3)
                    return;
                    $x1 += '<img src="'+root_path+'images/'+v+'.jpg" alt="Expert In '+v+'">';
                    $passiondisplaycount++;
                });
            }
            if('joinedPassion' in $entitydetail['passion'])
            {
                $.each($entitydetail['passion']['joinedPassion'],function(i,v){
                    if($passiondisplaycount > 3)
                    return;
                    $x1 += '<img src="'+root_path+'images/'+v+'.jpg" alt="Joined '+v+'">';
                    $passiondisplaycount++;
                });
            }
        }
        if($entitydetail['amifollowinghim'])
        {
            //$x2 = '<button class="btn btn-default">Unfollow</button>';
            $x2 = '<button class="followbtn btn btn-primary btn-sm ajax-btn" data-action="post" data-aj="true" data-p-str="profile_id" data-t="user-follow" data-profile_id="'+$entitydetail['profile_id']+'">UNFOLLOW</button>';
        }
        else
        {
            // $x2 = '<button class="btn btn-default">Follow</button>';
            $x2 = '<button class="followbtn btn btn-primary btn-sm ajax-btn" data-action="post" data-aj="true" data-p-str="profile_id" data-t="user-follow" data-profile_id="'+$entitydetail['profile_id']+'">FOLLOW</button>';
        }
        if($entitydetail['ishefollowingme'] !== null)
        $x3 = '<small>Following you</small>';
        else
        $x3 = '';
        
         $x = '<div class="hover_menu_contents">'+
                    '<div class="ObjectCard">'+
                        '<div class="OC_hdr clearfix">'+
                            $x2+
                            '<a class="name" href="'+root_path+'profile/'+$entitydetail['seopath']+'">'+
                            '<img class="pic unveil" alt="" src="'+DEFAULT_IMG+'" data-src="'+$entitydetail['userdp']+'">'+$entitydetail['fullname']+
                            '</a>'+
                            $x3+
                        '</div>'+
                        '<div class="OC_body">'+
                            '<a class="name" href="'+root_path+'profile/'+$entitydetail['seopath']+'">'+
                            '<img class="cvr_phto unveil" alt="" alt="" src="'+DEFAULT_IMG_HORIZONTAL+'" data-src="'+$entitydetail['userbg']+'">'+
                            '<div class="overlay">'+
                                     $x1+
                            '</div>'+
                            '</a>'+
                        '</div>'+
                        '<div class="OC_footer">'+
                            '<a class="name" href="'+root_path+'profile/'+$entitydetail['seopath']+'">'+
                            '<div class="stat clearfix">'+
                                '<section>'+
                                    'FOLLOWERS  <strong>('+$entitydetail['follower']+')</strong>'+
                                '</section>'+
                                '<section>'+
                                     'FOLLOWING <strong>('+$entitydetail['following']+')</strong>'+
                                '</section>'+
                            '</div>'+
                            '</a>'+
                        '</div>'+
                    '</div>'+
            '</div>';
            return $x;
        if($("body .hover_menu").length == 0)
        {
            $("body").append('<div  class="hover_menu show_nub">'+$x+'</div>');
        }
        else
        {
            $("body .hover_menu").html($x).removeClass('hide');
        }
    }
}
function hovercardattach(){
    $('.hovercard').each(function(key, val){
        $('#'+$(this).attr('id')).attachHoverPopup('popuphovercard');
    });
}
function calljscallback(v){
    var x = v.split('/');
    x = x[x.length-1];
    x = x.split('?');
    x = x[0].replace('.js','');
    x = x.replace(/\./g,'');
    x = x+'_callback';
    if(window[x] !== undefined)
    window[x]();
    jsfileQueueAdd(v,1);
    var tdatetime = new Date();
    var tdatetimems =  datetime.getTime();
    if(server == 'live')
    {
        /*
        ga('send', {
            hitType: 'timing',
            timingCategory: 'JS Dependencies',
            timingVar: v,
            timingValue: tdatetimems - datetimems
        });
        */
    }
}
function triggerunveil(){
        $(window).resize();
    $(".unveil").trigger("unveil");
}
function round(num,places)
{
    $x = Math.pow(10,places);
    return Math.round(num * $x) / $x;
}
function formpost($target,$data){
    var newForm = $('<form>', {
        'action': $target,
        'method': 'post'
    });
    $.each($data,function(i,v){
        newForm.append($('<input>', {
            'name': i,
            'value': v,
            'type': 'hidden'
        }));
    });
    newForm.append($('<input>', {
        'value': 'Submit',
        'type': 'submit'
    }));
    newForm.appendTo('body').submit().remove();
}
function geocodePositionFromMarker(marker,callback)
{
    return geocodePosition(marker.getPosition(),callback);
}
function geocodePosition(pos,callback) {
  geocoder = new google.maps.Geocoder();
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      if(responses[0] && responses[0]['address_components'])
      {
          $locationFinal = getLocationFromPlaceObject(responses[0]);
          if(callback)
          window[callback]($locationFinal);    
          return $locationFinal;
      }
    } else {
      return null;
    }
  });
}
function getLocationFromPlaceObject(place){
    var $location = {};
    if(!(place['address_components']))
    return null;
    $.each(place['address_components'].reverse(), function(i,v) {
      if($location['country'] && $location['state'] && $location['city'])
      return false;
      $.each(v['types'],function(i2,type){
          if($location['country'] && $location['state'] && $location['city'])
          return false;
          if(!($location['country']) && type == 'country')
          {
              $location['country'] = v['long_name'];
          }
          if(!($location['state']) && type == 'administrative_area_level_1')
          {
              $location['state'] = v['long_name'];
          }
          if(!($location['city']) && type == 'locality')
          {
              $location['city'] = v['long_name'];
          }
      });
    });
    $location['name'] = place['name'];
    var $locationFinal = {};
    $locationFinal['components'] = $location;
    $locationFinal['formatted_address'] = place['formatted_address'];
    return $locationFinal;
}
function delayfunction($fn,$paramsArray,$delay){
    if(!($delay))
    $delay = 1000;
    if(timer[$fn]) {
            clearTimeout(timer[$fn]);
            timer[$fn] = null;
    }
    timer[$fn] = setTimeout(function() {
            if($paramsArray.length == 0)
            return window[$fn]();
            else if($paramsArray.length == 1)
            return window[$fn]($paramsArray[0]);
            else if($paramsArray.length == 2)
            return window[$fn]($paramsArray[0],$paramsArray[1]);
            else if($paramsArray.length == 3)
            return window[$fn]($paramsArray[0],$paramsArray[1],$paramsArray[2]);
            else if($paramsArray.length == 4)
            return window[$fn]($paramsArray[0],$paramsArray[1],$paramsArray[2],$paramsArray[3]);
    }, $delay);
}
function delayfunction2($fileArray,$fn,$paramsArray,$delay){
    if(!($delay))
    $delay = 1000;
    $librariesLoaded = 1;
    $.each($fileArray,function(i,v){
    if(jsfileQueueGet(v) != 1)
    {
    //console.log(v + ' not loaded yet');
    $librariesLoaded = 0;
    }
    else
    {
    //console.log(v + ' loaded');
    }
    });
    //console.log('libraries loaded');
    //console.log($librariesLoaded);
    
    if($librariesLoaded) {
            clearTimeout(timer[$fn]);
            timer[$fn] = null;
        if($paramsArray.length == 0)
            return window[$fn]();
            else if($paramsArray.length == 1)
            return window[$fn]($paramsArray[0]);
            else if($paramsArray.length == 2)
            return window[$fn]($paramsArray[0],$paramsArray[1]);
            else if($paramsArray.length == 3)
            return window[$fn]($paramsArray[0],$paramsArray[1],$paramsArray[2]);
            else if($paramsArray.length == 4)
            return window[$fn]($paramsArray[0],$paramsArray[1],$paramsArray[2],$paramsArray[3]);
    }
    else
    {
    //if(timer[$fn] == null)
    //{
        timer[$fn] = setTimeout(function(){
            delayfunction2($fileArray,$fn,$paramsArray,$delay);    
        }, $delay);
    //}
    }
}
function jsfileQueueAdd($file ,$flag){
    // var x = $file.split('/');
    // x = x[x.length-1];
    var x = $file.replace('js/','');
    x = x.replace('js\/','');
    x = x.split('?');
    $file = x[0];
    $jsLoaded[$file] = $flag;
}
function jsfileQueueGet($file){
    // var x = $file.split('/');
    // x = x[x.length-1];
    var x = $file.replace('js/','');
    x = x.replace('js\/','');
    x = x.split('?');
    $file = x[0];
    return $jsLoaded[$file];
}
function showToolTipMsg($selector,$msg){
    if(jQuery.type($selector) === 'string')
    $selector = $($selector);
    $selector.tooltip({title: $msg, trigger: "manual",placement:"auto bottom",delay:{hide:"1000"}});
    $selector.tooltip('show');
    setTimeout(function(){
        $selector.tooltip('hide');
        $selector.tooltip('destroy');
    },4000);
}
function getAddedTags($obj)
{
    var $result = {};
    $result['list'] = {};
    $result['custom'] = 0;
    $result['autosuggest'] = 0;
    $obj.find('.as-selection-item').each(function(i,v){
        $temp = $(this).data('asmode');
        $temp2 = JSON.parse($(this).data('obj'));
        $temp2['asmode'] = $temp;
        $result[$temp2['asmode']]++;
        $result['list'][i] = $temp2;
    });
    return $result;
}
function getAddedTagsv2($selector)
{
    if(jQuery.type($selector) === 'string')
    $selector = $($selector);
    var $result = {};
    $result['custom'] = [];
    $result['autosuggest'] = [];
    $selector.find('.as-selection-item').each(function(i,v){
        var $temp = $.parseJSON(($(this).data('obj')));
        $result[$temp['mode']].push($temp);
    });
    return $result;
}
function addToLocalStorage(key,data){
    $temp = [];
    $temp = searchInLocalStorage(key,data);
    if(!$temp)
    $temp = [] ;
    //if(!$temp)
    //{
        var exdays = 1;
        $temp.push(data);
        setLocalStorage(key,$temp,exdays);
        return true;
    //}
    //else
    //return false;
}
function addToLocalObject(key,i,v){
    $temp = {};
    $temp = getLocalStorage(key);
    if(!$temp)
    $temp = {} ;
    var exdays = 1;
    $temp[i] = v;
    setLocalStorage(key,$temp,exdays);
    return true;
}
function searchInLocalStorage(key,data){
    $temp = [];
    $temp = getLocalStorage(key);
    if($.inArray(data,$temp))
    return $temp;
    else
    return false;
}
function setLocalStorage(key,data,exdays){
    var $data = {};
    $data['expires'] = Math.floor(Date.now() / 1000) + exdays*24*60*60;
    $data['data'] = data;
    localStorage.setItem(key,JSON.stringify($data)); 
}
function getLocalStorage(key){
    var $data = localStorage.getItem(key);
    if($data !=null)
    {
        $data = JSON.parse($data);
        var lsexpires = $data['expires'];
        if(Math.floor(Date.now() / 1000) >= lsexpires)
        {
            deleteLocalStorage(key);
            return null;
        }
        if('data' in $data)
            return $data['data'];
        else
        {
            deleteLocalStorage(key);
            return null;
        }
    }
    else 
    return null;
}
function deleteLocalStorage(key){
    localStorage.removeItem(key);
}
function addScrollbar($selector){
    $selector.each(function(){
        if(!($(this).hasClass('scrollAdded')))
        {
            $(this).enscroll({
                showOnHover: false,
                verticalTrackClass: 'track3',
                verticalHandleClass: 'handle3'
            }).addClass('scrollAdded');
        }
    });
}
function currentFullDate () {
    var temp = new Date();
    console.log(temp);
    var date = temp.getFullYear()+'-'+(temp.getMonth()+1)+'-'+temp.getDate();
    var time = temp.getHours() + ":" + temp.getMinutes() + ":" + temp.getSeconds();
    return  date+' '+time;
}
function ajaxUpload2($selector){
    var $selectorObj = $('#'+$selector);
    $pictype = $selectorObj.attr('pictype');
    formdata = new FormData();
    var $cropData = {};
    $selectorObj.parent().find('.multicropdata input').each(function(){
        formdata.append($(this).attr('name'),$(this).attr('value'));
    });
    var filedata = $selectorObj[0];
    var i = 0, len = filedata.files.length, img, reader, file;
    for (; i < len; i++) {
        file = filedata.files[i];

        if (window.FileReader) {
            reader = new FileReader();
            reader.onloadend = function(e) {
            };
            reader.readAsDataURL(file);
        }
        formdata.append($pictype+"[]", file);
    }
    $boxid = $selectorObj.attr('org-selector');
    formdata.append('pictype',$pictype);
    $selectorObj.replaceWith( $selectorObj = $selectorObj.clone( true ) );
    if (formdata) {
        $.ajax({
            url: appendAjaxDefaultParameters(root_path+'PSAjax.php?type=imageuploadajax4&boxid='+$boxid),
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function(data) {
                $res = $.parseJSON(data);
                if($res.status == 405)
                loginModal('');
                else if($res.status == 200)
                {
                $('#'+$res['boxid']).attr('src',upload_path+$res['data']);
                }
            },       
            error: function(res) {

            }       
          });
        };
}
function psanalytics(){
    //console.log('psanalytics');
    var d = new Date(); 
    addToLocalStorage('views',{'time':d.getTime(),'url':orgurl});
}
function scrollTo($selector){
    //console.log('selctror');
    //console.log($selector);
    if(jQuery.type($selector) === 'string')
    $selector = $($selector);
    //console.log($selector.length);
    
    if($selector.length)
    {
        $('html, body').animate({
        scrollTop: $selector.offset().top - ($("header").outerHeight() + 5)
        }, 1000);
    }
}
function loginCallback($meta){
    $("#min_register_btn").removeProp('disabled');
    if($meta['profile_id'])
    {
        profile_id = $meta['profile_id'];
        login_status = true;
        if(server == 'live')
        ga('set', 'userId', $meta['profile_id'].replace('profile-','').replace('profile_',''));
    }
    if(login_callback)
    login_callback({'profile_id':$meta['profile_id'],'sns':$meta['sns'],'snsid':$meta['snsid']});
    else{
        if(relurl == '\/')
        loadPage('home');
        else
        reloadwindow();
    }
}
function minimal_login($meta){
    //alert('minimul login');
    if(minimal_login_callback)
    {
    closemyModal();
    login_status = true;
    profile_id = $meta['profile_id'];
    profile_complete = true;
    if(typeof minimal_login_callback == 'function')
    minimal_login_callback($meta);
    else
    window[minimal_login_callback]($meta);
    }
    else
    {
        login_callback = '';
        loginCallback();
    }
}
function addDobDropdowns($selector1){
    if(jQuery.type($selector1) === 'string')
    $selector1 = $($selector1);
    $selector1.each(function(i,v)
    {
        if(!($(this).hasClass('dobdropdownadded')))
        {
            $selector = $(this);
            var $yearselector = $selector.find('.dobyear');
            var $minAge = ($yearselector.data('dobminage'))?$yearselector.data('dobminage'):13;
            var $monthselector = $selector.find('.dobmonth');
            var $dayselector = $selector.find('.dobday');
            var $datetextselector = $selector.find('.dobtext');
            var $dateallselector = $selector.find('select');
            $selector.each(function(i,v){
                var year = new Date().getFullYear();
                // load years
                $yearselector.append('<option value="">YYYY</option>');
                for (var i= (year - $minAge); i>=1916; i--) $yearselector.append('<option value=' + i + '>' + i + '</option>');
                // load months
                $monthselector.append('<option value="">MM</option>');
                for (var i=1; i<=12; i++) $monthselector.append('<option value=' + i + '>' + i + '</option>');
                // load days
                $dayselector.append('<option value="">DD</option>');
                for (var i=1; i<=31; i++) $dayselector.append('<option value=' + i + '>' + i + '</option>');
                $datetextselector.val('');
            });
            
            
            $yearselector.change(function() {
                if($yearselector.val() == '')
                return;
                var now = new Date();
                if ($yearselector.val()==now.getFullYear()) {
                  $monthselector.find('option').each(function() {
                    if ($(this).val()>(now.getMonth()+1)) $(this).remove();
                  });
                } else {
                  for (var i=1; i<$minAge; i++)
                    if ($monthselector.find("option[value='" + i + "']").val()==undefined)
                      $monthselector.append('<option value=' + i + '>' + i + '</option>');
                }
                
                checkMonth($selector);
            });

            $monthselector.on('change',function(){
            checkMonth($selector);
            });
            $dateallselector.on('change',function(){
                if($yearselector.val()!='' && $monthselector.val()!='' && $dayselector.val()!='')
                $datetextselector.val(pad($yearselector.val(),2)+'-'+pad($monthselector.val(),2)+'-'+pad($dayselector.val(),4));
                else
                $datetextselector.val('');
            });
            $(this).addClass('dobdropdownadded');
        }
    });
}
function checkMonth($selector) {
    if(jQuery.type($selector) === 'string')
    $selector = $($selector);
    var now = new Date();
    //console.log($selector);
    var $yearselector = $selector.find('.dobyear');
    var $monthselector = $selector.find('.dobmonth');
    var $dayselector = $selector.find('.dobday');
    var $dayoptions = $dayselector.find('option');
        
  if ($yearselector.val()==now.getFullYear() && $monthselector.val()==(now.getMonth()+1)) {
    $dayoptions.each(function() {
      if ($(this).val()>now.getDate()) $(this).remove();
    });
  } else {
    var days = 31;
    var month = $monthselector.val();
    if (month==2) {
      if (($yearselector.val() % 4) == 0) days = 29; // leap year
      else days = 28;
    } else if (month==2 || month==4 || month==6 || month==9 || month==11) {
      days = 30;
    }
    for (var i=1; i<32; i++)
      if (i>days)
        $dayselector.find("option[value='" + i + "']").remove();
      else if ($dayselector.find("option[value='" + i + "']").val()==undefined)
        $dayselector.append('<option value=' + i + '>' + i + '</option>');
  }
}
function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
function appendAjaxDefaultParameters($url){
    if ($url.indexOf("?") >= 0)
    return $url = $url+'&login_status='+login_status+'&token='+formtoken;
    else
    return $url = $url+'?login_status='+login_status+'&token='+formtoken;
}
function preloadImage (url) {
        try {
        var _img = new Image();
        _img.src = url;
        } catch (e) { }
}
function executeFunctionByName(functionName) {
  //arguments as array in second parameter
  context = window;
  //if(typeof parentwindowcontext != 'undefined' && parentwindowcontext.length > 0)
  //functionName = parentwindowcontext+'.'+functionName;
  //var args = [].slice.call(arguments).splice(1);
  var args = arguments[1];
  var namespaces = functionName.split(".");
  var func = namespaces.pop();
  for(var i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  return context[func].apply(context, args);
}
function jsontocsv(objArray)
{
    objArray = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    if(typeof objArray == 'object' && 'list' in objArray)
    objArray = objArray['list'][Object.keys(objArray['list'])[0]];
    var array = objArray;
    var str = '';
    var head = '';
    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if(i == 0)
            head += index + ',';
            line += '"'+array[i][index]+'"' + ',';
        }
        if(i == 0)
        head.slice(0,head.Length-1); 
        line.slice(0,line.Length-1); 
        str += line + '\r\n';
    }
    var uri = "data:text/csv;charset=utf-8," + escape(head+'\r\n'+str);
    var downloadLink = document.createElement("a");
    downloadLink.href = uri;
    downloadLink.download = "data.csv";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}
function jsontotable(objArray,$selector,tableclass)
{
    if(jQuery.type($selector) === 'string')
    $selector = $($selector);
    
    objArray = typeof objArray != 'object' ? $.parseJSON(objArray) : objArray;
    if(typeof objArray == 'object' && 'list' in objArray)
    objArray = objArray['list'];
    //objArray = objArray['list'][Object.keys(objArray['list'])[0]];
    var array = objArray;
    var str = '';
    var head = '';
    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if(i == 0)
            head += '<th>' + index + '</th>';
            line += '<td>' + array[i][index] + '</td>';
        }
        str +='<tr>'+ line + '</tr>';
    }
    //var data = '<table class="'+tableclass+' jsontable hide" style="width:100%;table-layout: fixed" data-jsondata="'+objArray+'">'+'<thead>'+head+'</thead>'+str+'</table>';
    tableid = 'jsontable_'+getRandomInt(1000,9999);
    var data = '<input type="button" class="btn btn-primary pull-right tabletocsv" value="Export to CSV" onclick="tabletocsv(\''+tableid+'\')"><br /><table id="'+tableid+'" class="'+tableclass+' jsontable"  border=1>'+'<thead>'+head+'</thead>'+str+'</table>';
    $selector.html(data);
    $("#"+tableid).data('jsondata',objArray);
    //$cellinrow = $selector.find('th').size();
    //console.log($cellinrow);
    //$selector.find('td,th').css({width:(100/$cellinrow)+'%'});
    //$selector.find('.jsontable').removeClass('hide');
}
function tabletocsv(id)
{
    $table = $("#"+id);
    $jsondata = $table.data('jsondata');
    //console.log($jsondata);
    jsontocsv($jsondata);
}
function get_alphanumeric($string){
    $string = $string.toLowerCase();
    $string = $string.replace(/\s+/, '-');
    $string = $string.replace(/-+/, '-');
    return $string.replace(/[^A-Za-z0-9-]/, '');;
}
;(function($){
    $.fn.extend({
        donetyping: function(callback,timeout){
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress paste change',function(e){
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type=='keyup' && e.keyCode!=8) return;
                    
                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    //console.log(e.keyCode);
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);

function getGeoLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(setGeoLocationHTML5);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function setGeoLocationHTML5(position) {
    currentuserlocation['coordinates'] = {};
    currentuserlocation['coordinates']['lat'] = position.coords.latitude;
    currentuserlocation['coordinates']['long'] = position.coords.longitude;
    currentuserlocation['accuracy'] = position.coords.accuracy;
    if(typeof google == 'undefined')
    {
        var url = "https://maps.googleapis.com/maps/api/js?key="+google_maps_key;
        $.getScript( url, function() {
        getGeolocationAddress(position);
        });
    }
    else
    getGeolocationAddress(position);
}
function getGeolocationAddress(position){
    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({'location': {lat: position.coords.latitude, lng: position.coords.longitude}}, function(results, status) {
        var location = getLocationFromPlaceObject(results[0]);
        currentuserlocation['location'] = location;
    });
};
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
function getUsersInMyNetwork(callback){
    $.post(root_path+'PSAjax.php?type=getmynetwork', {}, function(data){ 
        $cparams = $.parseJSON(data);
        if($cparams['status'] == 200){
            $users = formatmembersforautosuggest($cparams['list']);
            executeFunctionByName(callback,[$users]);
        }
    });
}
function formatmembersforautosuggest($users){
    var $newusers = [];
    $.each($users,function($i,$v){
        var $temp = {};
        $temp['text'] = $v['fullname'];
        $temp['value'] = $v['profile_id'];
        $newusers.push($temp); 
    });
    return $newusers;
}
function autoSuggestUser($selector,$data,$opt){
    if(jQuery.type($selector) === 'string')
    $selector = $($selector);
    var $options = {
        selectedItemProp: "text", 
        searchObjProps: "text",
        usePlaceholder:true,
        startText:$selector.attr('placeholder'),
        neverSubmit:true,
        selectionAdded:function(elem,elem2){},
    }
    $options = $.extend($options,$opt);
    $selector.autoSuggest($data,$options);
}
function gaSendEvent(operation,category,value){
    if(server == 'live')
    ga('send', 'event', (category)?category:'undefined', q['pagetype'], (operation)?operation:'undefined', (value)?value:1, {'NonInteraction': 1});
}