$(document).on('click', '.join-group', function(event){
  var userId  = ps_uid;
  var groupId = $(this).attr('data-id');
  if (groupId == '' || groupId == undefined || groupId == 'undefined') {
    bootbox.alert('Requested action can not be allowed');
    return false;
  }
  
  $.ajax({
      url: root_path+'AjaxGroup.php',
      type: "POST",
      data: 'requestType=joinGroup&groupId='+groupId+'&user='+userId,
      success: function(data) {
        var discoveredGroup = jQuery.parseJSON(data);
        if (discoveredGroup.responseCode == '2003') {
          // window.location.href = root_path+'community/'+groupId;
        } else {
          // bootbox.alert('Some error occured. Please try after some time.');
          //location.reload();
        }
        window.location.href = root_path+'community/'+groupId;
      },
      error: function(res) {
      }
  });
});

$(document).on('click','.member-admin-action', function(){
  var $requestObj = $(this);
  var memberId  = $requestObj.attr('data-member');
  var groupId   = $requestObj.attr('data-id');
  var requestId = $requestObj.attr('data-action');
  if (requestId == 1) {
    var infoMsg = 'As a community admin, user will be able to edit community settings, remove members and give other members admin status.';
  } else {
    var infoMsg = 'You are about to remove as a community admin.';
  }
  bootbox.confirm(infoMsg, function(result){ 
    if (result === true) {
      $.ajax({
          url: root_path+'AjaxGroup.php',
          type: "POST",
          data: 'requestType=memberAdmin&groupId='+groupId+'&memberId='+memberId+'&requestId='+requestId,
          success: function(data) {
              var discoveredGroup = jQuery.parseJSON(data);
              location.reload();
          },
          error: function(res) {
          }
      });
    }
  });
  return false;
});

$(document).on('click','.member-moderator-action', function(){
  var $requestObj = $(this);
  var memberId  = $requestObj.attr('data-member');
  var groupId   = $requestObj.attr('data-id');
  var requestId = $requestObj.attr('data-action');
  if (requestId == 2) {
    var infoMsg = 'Moderators are like admins, but with limited controls. User will be able to manage membership, and review posts and comments in the community';
  } else {
    var infoMsg = 'You are about to remove as a community moderator.';
  }

  bootbox.confirm(infoMsg, function(result){ 
    if (result === true) {
      $.ajax({
          url: root_path+'AjaxGroup.php',
          type: "POST",
          data: 'requestType=memberAdmin&groupId='+groupId+'&memberId='+memberId+'&requestId='+requestId,
          success: function(data) {
              var discoveredGroup = jQuery.parseJSON(data);
              location.reload();
          },
          error: function(res) {
          }
      });
    }
  });
  return false;
});

$(document).on('click', '.exit-group', function(event){
    bootbox.confirm("Do you want to leave community ?", function(result){ 
      if (result === true) {
        var userId  = ps_uid;
        var groupId = $('.exit-group').attr('data-id');
        $.ajax({
            url: root_path+'AjaxGroup.php',
            type: "POST",
            data: 'requestType=leaveGroup&groupId='+groupId+'&user='+userId,
            success: function(data) {
                var discoveredGroup = jQuery.parseJSON(data);
                if (discoveredGroup.responseCode == '2001') {
                  location.reload();
                } else {
                  bootbox.alert('Community has been removed. Since you were the last member.');
                  location.reload();
                }
            },
            error: function(res) {
            }
        });
      }
    });
    return false;
  });

// 1 to follow 0 unfollow
$(document).on('click', '.follow-action', function(){
  $requestedAction = $(this);
  var actionId  = $($requestedAction).attr('data-action');
  var groupId   = $($requestedAction).attr('data-id');

  var action     = '';
  var actionText = '';

  if (actionId == 1) {
    action = 'followGroup';
    actionText = 'follow';
  } else {
    action = 'unfollowGroup';
    actionText = 'unfollow';
  }

  bootbox.confirm("Do you want to "+actionText+" community ?", function(result) {
    if (result === true) {
      $.ajax({
        url: root_path+'AjaxGroup.php',
        type: "POST",
        data: 'requestType='+action+'&user='+ps_uid+'&groupId='+groupId,
        success: function(data) {
          location.href = root_path+'community/'+groupId;
        },
        error: function(res) {
        }
      });
    }
  });
  return false;
});
