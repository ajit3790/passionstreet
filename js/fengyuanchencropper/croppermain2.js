function croppernew($container,callback,data,aspect){
	var currentFileSelector = data['currentFileSelector'];
	var callback2 = '';
	if(currentFileSelector.attr('callback'))
	callback2 = currentFileSelector.attr('callback')+'(\''+currentFileSelector.attr('id')+'\')';
	var $image = $($container +" > .img-container > img"),
	$dataX = $("#dataX"),
        $dataY = $("#dataY"),
        $dataHeight = $("#dataHeight"),
        $dataWidth = $("#dataWidth"),
        options = {
          aspectRatio: (aspect)?aspect:(16/9),
	  viewMode:3,
          preview: ".img-preview",
          done: function (data) {
            // $dataX.val(Math.round(data.x));
            // $dataY.val(Math.round(data.y));
            // $dataHeight.val(Math.round(data.height));
            // $dataWidth.val(Math.round(data.width));
          },
	  crop: function (e) {
		e.x = e.x; 
		e.y = e.y; 
		e.w = e.width; 
		e.h = e.height; 
		e.x2 = e.x + e.width; 
		e.y2 = e.y + e.height; 
		callback(e);
		// $dataX.val(Math.round(e.x));
		// $dataY.val(Math.round(e.y));
		// $dataHeight.val(Math.round(e.height));
		// $dataWidth.val(Math.round(e.width));
		//$dataRotate.val(e.rotate);
		//$dataScaleX.val(e.scaleX);
		//$dataScaleY.val(e.scaleY);
	  }
        };
	$image.cropper(options).on({
	      "build.cropper": function (e) {
		console.log(e.type);
	      },
	      "built.cropper": function (e) {
		console.log(e.type);
		$image.cropper('setDragMode', 'move');
	      }
	});
	
	$($container).append('<div class="docs-toolbar">           <div class="btn-group">             <button class="btn btn-primary" data-method="zoom" data-option="0.1" type="button" title="Zoom In">               <span class="docs-tooltip" data-toggle="tooltip" title="Zoom In">                 <span class="glyphicon glyphicon-zoom-in"></span>               </span>             </button>             <button class="btn btn-primary" data-method="zoom" data-option="-0.1" type="button" title="Zoom Out">               <span class="docs-tooltip" data-toggle="tooltip" title="Zoom Out">                 <span class="glyphicon glyphicon-zoom-out"></span>               </span>             </button>             <label class="btn btn-primary" for="inputImage" title="Upload image file">               <input class="hide inputImage" id="inputImage" name="file" type="file" accept="image/*">               <span class="docs-tooltip" data-toggle="tooltip" title="Change Image">                 Change Image               </span>              </label>         <button class="btn btn-primary" type="button" title="Save" onclick="$(\'#modal-box\').modal(\'hide\');'+callback2+';"><span class="docs-tooltip" data-toggle="tooltip" title="Save" >                 Save               </span></button>  </div>         </div>');
	
	$(document).on("click", $container+" [data-method]", function () {
		var data = $(this).data();
		if (data.method) {
			$(this).parents($container).find('.img-container > img').eq(0).cropper(data.method, data.option);
			//$image.cropper(data.method, data.option);
		}
	});
	$($container).find('.inputImage').change(function () {
		var blobURL;
		var files = this.files,
		    file;

		if (files && files.length) {
		  file = files[0];

		  if (/^image\/\w+$/.test(file.type)) {
		    if (blobURL) {
		      URL.revokeObjectURL(blobURL); // Revoke the old one
		    }

		    blobURL = URL.createObjectURL(file);
		    $(this).parents($container).find('.img-container > img').eq(0).cropper("reset", true).cropper("replace", blobURL);
		    $(this).val("");
		  } else {
		    showMessage("Please choose an image file.");
		  }
		}
	});
	
    
}