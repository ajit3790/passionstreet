function showquestions1(){
	$html = '';
		$.each(questions,function(i,v){
			$html += '<table width="100%" class="event-signup-form">';
			$html +=   '<tr><td><div class="row">'+v['question']+'</div></td></tr>';
			if(v['questiontype'] == 'multipleoption'){
				$html +=   '<tr><td><div class="row">';
				$html += '<select class="form-control" name="test>';
				$html += '<option>Select</option>';
				$.each(v['questionoptions'],function(i2,v2){					
					$html += '<option value="'+v2+'">'+v2+'</option>';
					
				});
				$html += '</select>';
				$html +=   '</div></td></tr>';
			}
			else if(v['questiontype'] == 'text')
			{
				$html +=   '<tr><td><div class="row">'+
								  '<textarea class="form-control" width="100%" style="height:35px" required="" name="test"></textarea>'+
							'</div></td></tr>';
			}
			else
			{
				$html +=   '<tr><td><div class="row">'+
								  '<input type="file" class="form-control filestyle" value="" required="" name="test">'+
							'</div></td></tr>';
			}
			$html +=   '</table>';
		});
		return $html;
}
function showquestions(x,y){
	$html = '';
	var questionss = {};
	questionss = questions;
	if(ticketing[x]['iscombo'] == 1 && ticketing[x]['type'] == 2)
	questionss.unshift({'questiontype':'text','question':'Team / Company Name'});
		$.each(questionss,function(i,v){
			$html += '<div>';
			$html +=   '<div class="row form-group">';
			$html +=   '<div class="col-md-6">'+v['question']+'</div>';
			$html +=   '<div class="col-md-6">';
			if(v['questiontype'] == 'multipleoption'){
				$html += '<select class="form-control" id="answerss_'+y+'_'+i+'" name="event[answers]['+x+']['+y+']['+i+']">';
				$html += '<option value="">Select</option>';
				$.each(v['questionoptions'],function(i2,v2){					
					$html += '<option value="'+v2+'">'+v2+'</option>';
					
				});
				$html += '</select>';
			}
			else if(v['questiontype'] == 'text')
			{
				$html +=   '<textarea class="form-control" id="answerss_'+y+'_'+i+'" width="100%" style="height:30px;overflow:hidden" required="" name="event[answers]['+x+']['+y+']['+i+']"></textarea>';
			}
			else
			{
				$html +=   '<input type="file" placeholder="'+v['question']+'" class="form-control filestyle" id="answerss_'+y+'_'+i+'" value="" required="" name="event[answers]['+x+']['+y+']['+i+']">';
			}
			$html +=   '</div>';
			$html +=   '</div>';
			$html +=   '</div>';
		});
		return $html;
}
function showquestionaire(){
    $(".eventbooking").removeClass('hide');
	if(!(questions))
	{
		processtopayment();
		return;
	}
	$usercount = 0;
    $(".cartdiv .ticketselector").each(function(i,v){
        $usercount = $usercount +  parseInt($(this).val());
    });
	$x = '<div class="widget-heading"><h2 class="hstyl_1 sm bx">Event Signup Form</h2></div><div id="questionss" class="bx-styl form1 carousel slide" data-ride="carousel"><div class="carousel-inner" role="listbox">';
    // i = 1;
    // v = questions;
	for($k=0;$k<$usercount;$k++)
	{
		$html = showquestions();
		$x += '<div style="margin-top:0px;" class="questionairediv item '+(($k==0)?'active':'')+'" >'+
			$html;
			if($k < ($usercount - 1))
			{
				$x += '<table width="100%">'+
						'<tr>'+
								'<td class="text-right footer"><button class="btn btn-primary nextquestion">Next</button></td>'+
						'</tr>'+
					'</table>';
			}
			else
			{
				$x += '<table width="100%">'+
						'<tr>'+
								'<td class="text-right footer"><button class="btn btn-primary" onclick="processtopayment();">Proceed to Payment</button></td>'+
						'</tr>'+
					'</table>';
			}
		$x += '</div>';
	}
	$x +='</div></div>';
	//$(".eventbooking").html($x);
	$(".eventbooking .eventbookingsteps").addClass('hide');
    $(".eventbooking .questionaire").html($x);
	$(".eventbooking .questionaire").removeClass('hide');
	$("#questionss").carousel({
		interval:false
	});
	$("#questionss").carousel('pause');
	$("#questionss .nextquestion").click(function(){
		$("#questionss").carousel('next');
	});
}
function showTicketCount(ticketid){
	var $ticket = ticketing[ticketid];
	if($ticket['iscombo'] && $ticket['iscombo']==1)
	var $x = '<select class="form-control ticketselector" data-pricetype="price" data-size="'+$ticket['combosize']+'" data-price="'+$ticket['price']+'" data-orgprice="'+$ticket['price']+'" data-ticket="'+$ticket['id']+'">';
	else
	var $x = '<select class="form-control ticketselector" data-pricetype="price" data-size="1" data-price="'+$ticket['price']+'" data-orgprice="'+$ticket['price']+'" data-ticket="'+$ticket['id']+'">';
	$ticketminbuy = $ticket['minbuy'];	
	$ticketmaxbuy = $ticket['maxbuy'];
	if(!($ticketminbuy))
	$ticketminbuy = 1;
	if(!($ticketmaxbuy))
	$ticketmaxbuy = 10;
	$x += "<option value='0'>0</option>";
	if($ticket['iscombo'] && $ticket['iscombo']==1)
	{
		$x += "<option value='1'>1</option>";
	}
	else
	{
		for($i=$ticketminbuy;$i<=$ticketmaxbuy;$i++)
		{
			$x += "<option value='"+$i+"'>"+$i+"</option>";
		}
	}
	$x +='</select>';
	return $x;
}
function showdatetimeslots(){
    addToLocalObject(q['event_id'],'showcart',1);
    scrollTo('.eventdetail');
    if($(".datetimeradio").length > 1)
    {
        $(".eventbooking .eventbookingsteps").addClass('hide');
        $(".eventbooking .datetimeslots").removeClass('hide');
        $(".eventbooking").removeClass('hide');
    }
    else
    showcart();
}
function showcustomdateselector(){
	addToLocalObject(q['event_id'],'showcart',1);
	scrollTo('.eventdetail');
	var slots = upcomingdatetimeslots[0];
	
	//$(".customdateselectordiv").html('<div class="datepickerv2" data-mode="single"  data-allowpastdates=0  data-datebounds="2017-06-15,2017-07-20" data-dates="2017-06-10,2017-06-18"></div>');
	$(".customdateselectordiv").html('<div class="datepickerv2" data-mode="single" data-callback="dateselectionschanged" data-allowpastdates=0  data-datebounds="'+slots['datestart']+','+slots['dateend']+'"></div>');
	$(".eventbooking .eventbookingsteps").addClass('hide');
	$(".eventbooking .customdateselector").removeClass('hide');
	$(".eventbooking").removeClass('hide');
	datepickerv2_callback();
}
function showcart(){
    /*$ls = getLocalStorage(q['event_id']);
    if($ls && $ls['showcart'] == 1)
    {
	
    }*/
    addToLocalObject(q['event_id'],'showcart',1);
    scrollTo('.eventdetail');
    $(".eventbooking").removeClass('hide');
    $(".questionairediv").addClass('hide');
    $x = '<div class="widget-heading"><h2 class="hstyl_1 sm">Select Ticket</h2></div>';
    $html = '';
    $showbookingoption = 0;
    $.each(ticketing,function(i,v){
	if(v['bookingprice'] != 0)
	$showbookingoption = 1;
        $html += '<tr class="ticketcategoryrow" data-ticket="'+i+'">'+
                    '<td>'+v['name']+'&nbsp;<span class="paymenttypedisplaytext hide" style="font-size:10px;vertical-align:middle;">( Full Amount )</span></td>'+
                    '<td class="price"><span class="orgprice hide strikethrough"><span>₹ '+v['price']+'</span></span><span class="userprice">₹ '+v['price']+'</span></td>'+
                    /*'<td class="qty"><input type="number" min="0" step="1" placeholder="0" data-price="'+v['price']+'" data-ticket="'+v['name']+'" value="0" required="" class="form-control ticketselector"></td>'+*/
					'<td class="cross">X</td>'+
                    '<td class="qty">'+showTicketCount(i)+'</td>'+
                    //'<td class="amount ticketvaluedisplay">0</td>'+
                 '</tr>';
	/*
	if(v['bookingprice'] != 0)
	{
		$html += '<tr class="ticketcategoryrow1" data-ticket="'+i+'">'+
                    '<td>&nbsp;</td>'+
                    '<td class="price">Full Amount @ ₹  '+v['price']+'</td>'+
                    		'<td class="cross"><input type="radio" class="choosefullpaymentoptionticketwise" value="1" data-ticket="'+i+'" name="choosefullpaymentoptionticketwise_'+i+'" checked > </td>'+
                    '<td class="qty"> </td>'+
                 '</tr>';
		 $html += '<tr class="ticketcategoryrow1" data-ticket="'+i+'">'+
                    '<td>&nbsp;</td>'+
                    '<td class="price">Booking Amount @ ₹  '+v['bookingprice']+'</td>'+
                    		'<td class="cross"><input type="radio" class="choosefullpaymentoptionticketwise" value="0" data-ticket="'+i+'" name="choosefullpaymentoptionticketwise_'+i+'"> </td>'+
                    '<td class="qty"> </td>'+
                 '</tr>';
	}
	*/
    });
    $x += '<div class="bx-styl form1 cartdiv">'+
		'<div class="payment-mode hide" style="margin-bottom:10px;border-bottom:1px solid #ccc">'+
			'<div class="row">'+
				'<div class="col-md-6">'+
				'<h5>Select your payment option</h5>'+
				'</div>'+
				'<div class="col-md-3 col-sm-6 col-xs-6">'+
					'<div class="radio">'+
						'<label><input type="radio" class="choosefullpaymentoption" name="choosefullpaymentoption" value="1" checked /> Full amount</label>'+
					'</div>'+
				'</div>'+
				'<div class="col-md-3 col-sm-6 col-xs-6">'+
					'<div class="radio">'+
						'<label><input type="radio" class="choosefullpaymentoption" name="choosefullpaymentoption" value="0"/> Booking amount</label>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
                '<table width="100%" class="event-signup-form">'+
                   // '<tr>' +
                    //    '<th>Ticket Type</th>'+
                    //    '<th>Rate / ticket</th>'+
                   //     '<th class="qty">Qty</th>'+
                   //     '<th class="amount">Amount</th>'+
                   // '</tr>' +
                    $html +
                    '<tr class="tickettotal pricebox hide">'+
                        '<td colspan="4">'+
                        '<table width="100%"><tr>'+
							((isMobile == false)?('<td class="total">'+
									'<table class="coupon-code"><tr><td><span><i class="fa fa-scissors"></i> Have a discount code ?</span></td></tr><tr><td>'+
									'<input placeholder="Redeem it here"  type="text" class="form-control" id="couponcode" name="couponcode" />'+
									'<input type="hidden" class="form-control" id="pricedistribution" name="pricedistribution" />'+
									'<button class="btn" onclick="applyCoupon();">Apply</button></td></tr></table>'+((typeof donation != 'undefined' && donation['allow'] == 1)?(
									'<table class="coupon-code charitybox" style="border-top:1px solid #ccc;margin-top:10px"><tr><td style="padding-top:10px"><span><i class="fa fa-handshake-o"></i>I want to contribute to this charity</span></td></tr><tr><td>'+
									'<input placeholder="Enter amount"  type="text" class="form-control" id="embeddeddonationfield" name="donationamount" />'+
									'<button class="btn" id="embeddeddonationbtn" >Contribute</button></td></tr></table>'):'')+
							'</td>'):'')+
							'<td class="total" colspan="3">'+
                                '<table>'+
                                        '<tbody><tr>'+
                                                '<td style="min-width:160px;">Ticket Price</td>'+
                                                '<td class="amount">₹ <span class="ticketprice">0</span></td>'+
                                        '</tr>'+
                                        '<tr class="hide">'+
                                                '<td>GST @ '+organiserservicetax+' %</td>'+
                                                '<td class="amount">₹ <span class="ticketorganisertax">0</span></td>'+
                                        '</tr>'+
										'<tr class="hide">'+
                                                '<td style="border-top: 1px solid #d5d5d5;">Total Ticket Price</td>'+
                                                '<td class="amount" style="border-top: 1px solid #d5d5d5;">₹ <span class="tickettotalprice">0</span></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                                '<td><a href="#" id="processingfeebreakup" data-toggle="tooltip" data-placement="right" data-html="true" title="Internet Handling charge<br />Payment Gateway Charge<br />GST">Processing fee*</a></td>'+
                                                '<td class="amount">₹ <span class="ticketconv">0</span></td>'+
                                        '</tr>'+
										'<tr class="grand-total">'+
                                                '<td><span class="totaltext">Gross Amount</span></td>'+
                                                '<td class="amount">₹ <span class="ticketgrandtotal">0</span></td>'+
                                        '</tr>'+
                                        '<tr class="grand-discount hide">'+
                                                '<td>Discount*</td>'+
                                                '<td class="amount">- ₹ <span class="ticketdiscounttotal">0</span></td>'+
                                        '</tr>'+
										'<tr class="user-contribution hide">'+
                                                '<td>Contribution</td>'+
                                                '<td class="amount">+ ₹ <span class="ticketcontribution">0</span></td>'+
                                        '</tr>'+
										'<tr class="net-payable hide">'+
                                                '<td>Total Payable</td>'+
                                                '<td class="amount">₹ <span class="ticketnetpayable">0</span></td>'+
                                        '</tr>'+
                                        ((isMobile == true)?('<tr><td colspan="2" class="coupon-code">'+
                                            '<input placeholder="Have Promo Code?"  type="text" class="form-control" id="couponcode" name="couponcode" />'+
                                            '<input type="hidden" class="form-control" id="pricedistribution" name="pricedistribution" />'+
                                            '<button class="btn" onclick="applyCoupon();">Apply</button></td>'+
                                        '</tr>'+((typeof donation != 'undefined' && donation['allow'] == 1)?(
										'<tr><td colspan="2" class="coupon-code"><br /><br /><span><i class="fa fa-handshake-o"></i>I want to contribute to this charity</span><br />'+
                                    '<input placeholder="Enter contribution amount"  type="text" class="form-control" id="embeddeddonationfield" name="donationamount" />'+
									'<button class="btn" id="embeddeddonationbtn" >Contribute</button></td></tr>'):'')):'')+
                                '</tbody></table>'+
                        '</td>'+
			'</tr></table>'+
			'</td>'+
                    '</tr>'+
                    '<tr class="pricebox hide">'+
                            '<td class="text-right footer" colspan="4"><button class="btn btn-primary" id="addAttendees" onclick="addAttendees();">Proceed</button></td>'+
                    '</tr>'+
                '</table>'+
        '</div>';
    //$(".eventbooking").html($x);
	$(".eventbooking .eventbookingsteps").addClass('hide');
	$(".eventbooking .cart").html($x);
	$(".eventbooking .cart").removeClass('hide');
	if($showbookingoption)
	$(".payment-mode").removeClass('hide');
	var $selectedticketsatstep1 = {};
	$(".ticketselectsidebar").each(function(){
		$selectedticketsatstep1[$(this).data('ticketid')] = $(this).val();
	});
	if($selectedticketsatstep1)
	{
	$(".ticketselector").each(function(){
		if($selectedticketsatstep1[$(this).data('ticket')])
		$(this).val($selectedticketsatstep1[$(this).data('ticket')]).change();
	});
	}
}
function addAttendees(){
    $ticketcount = {};
    $ticketcount2 = [];
	$i = 0;
	$html2 = '';
    $(".cartdiv .ticketselector").each(function(i,v){
        $tickettype = $(this).attr('data-ticket');
        $ticketpricetype = $(this).attr('data-pricetype');
        $ticketcount[$tickettype] = parseInt($(this).val()) * parseInt($(this).attr('data-size'));
		for($x=0;$x<$ticketcount[$tickettype];$x++)
		{
			$ticketcount2.push($tickettype);
		}
	$html2 = $html2 + '<input type="hidden" name="event[ticketpricetype]['+$tickettype+']" value="'+$ticketpricetype+'" />';
		
    });
	if($ticketcount2.length == 0)
	{
		scrollTo('.eventdetail');
		showToolTipMsg($(".cartdiv .ticketselector").eq(0),"Please select atleast 1 ticket");
		return;
    }
	$(".eventbooking").removeClass('hide');
    $(".questionairediv").addClass('hide');
    $(".cartdiv").addClass('hide');
    
    addToLocalObject(q['event_id'],'ticketcount',$ticketcount);
    
    $x = '<div class="widget-heading"><h2 class="hstyl_1 sm ">'+((skiptoleadcapture == 1)?'Enter your details':'Update Participants Detail')+'</h2></div>';
    $html = '';
    $html = '<div id="participantss" class="form1 carousel slide" data-ride="carousel"><div class="carousel-inner" role="listbox">';
	$ticketlength = $ticketcount2.length;
	for(j=0;j<$ticketlength;j++)
	{
		i = $ticketcount2[j];
                var genderstr = {};
                genderstr['male'] = 'checked = "checked"';
                genderstr['female'] = '';
                if(ticketing[i]['type'] == 3)
                {
                    if(j == 0)
                    {
                    genderstr['male'] = 'checked = "checked" onclick="javascript: return false;"';
                    genderstr['female'] = 'onclick="javascript: return false;"';
                    }
                    else if(j == 1)
                    {
                    genderstr['female'] = 'checked = "checked" onclick="javascript: return false;"';
                    genderstr['male'] = 'onclick="javascript: return false;"';
                    }
                }
		$html +='<div id="participantbox'+j+'" style="margin-top:0px;" class="item '+((j==0)?'active':'')+'">'+
					'<h4 class="'+((skiptoleadcapture == 1)?'hide':'')+'">Participant '+(j+1)+' # '+ticketing[i]['name']+'</h4>';
		// if(!(eventid == 'event-2803402336' || eventid == 'event-11216441114'))
		if(false)
		{
		$html += '<div class=" row"><div class="col-sm-6 form-group"><input type="text" placeholder="Name" name="event[ticketing]['+i+']['+j+'][name]" required="" class="form-control name" /></div><div class="col-sm-6 form-group"><input type="email" name="event[ticketing]['+i+']['+j+'][email]" placeholder="Email"  class="form-control email" /></div></div>'+
					'<div class="row form-group"><div class="col-xs-6">Gender</div><div class="radio col-xs-3"><label class="">'+
							'<input type="radio" value="M" required="" name="event[ticketing]['+i+']['+j+'][gender]" class="gender" '+genderstr['male']+' />'+
							'Male'+
					'</label></div>'+
					'<div class="radio col-xs-3"><label class="">'+
							'<input type="radio" value="F" required="" name="event[ticketing]['+i+']['+j+'][gender]" class="gender" '+genderstr['female']+'/>'+
							'Female'+
					'</label></div></div>';
					
		//$html +=	'<div class="form-group"><input type="text" name="event[ticketing]['+i+']['+j+'][dob]" placeholder="Date of Birth"  class="form-control dob" /></div>';
		if(ticketing[i]['requiremobile'] == 1)
		$html +=	'<div class="form-group row"><div class="col-md-6 c-label">Mobile Number</div><div class="col-md-6"><input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="event[ticketing]['+i+']['+j+'][mobile]" placeholder=""  class="form-control mobile" /></div></div>';
		
		$html +=	'<div class="row form-group dob dobdropdown"><div class="col-xs-12 col-sm-6 c-label">Date of Birth</div><div class="col-sm-2 col-xs-4"><select class="dobday form-control"></select></div>  <div class="col-sm-2 col-xs-4"><select class="dobmonth form-control"></select></div>  <div class="col-sm-2 col-xs-4"><select class="dobyear form-control" data-dobminage="'+ticketing[i]['agerestrict']+'"></select></div><input type="hidden" name="event[ticketing]['+i+']['+j+'][dob]" class="dobtext"></div>';
		}
		else
		$html += showattendeeform(i,j);			
		$html += showquestions(i,j);			
		if(j==($ticketlength-1))			
		{
			if(skiptoleadcapture == 1)
			$html +='<div class="form-group text-center"><label class="paymenttermscheck" for="paymenttermscheck" ><input type="checkbox" checked />By clicking the Proceed / Register button , I accept to the terms & conditions of PASSIONSTREET. I want to participate in the event and therefore authorize the event organizer to connect with me.   </label></div>';
			else
			$html +='<div class="form-group text-center"><label class="paymenttermscheck" for="paymenttermscheck" ><input type="checkbox" checked />I understand that PASSIONSTREET is a networking & ticketing platform; it is not an organizer for this event/trip. By <span>clicking the proceed button</span> I accept that I am fully aware of the inclusions, exclusions, cancellation policies and other terms and conditions, including the waiver of liabilities of the event/trip from the organizer and PASSIONSTREET.   </label></div>';
			if(q['guestcheckout'])
			$html +='<div class="form-group text-center"><label class=""><input type="checkbox" id="guestcheckout" value="1" />&nbsp;Guest Checkout</label></div>';
			$html +='<div class="form-group text-center"><input type="button" class="btn btn-primary" id="paybutton" onclick="processtopayment();return false;" value="'+((skiptoleadcapture == 1)?'Submit':'Proceed to Payment')+'" /><div>';
		}
		else
		$html +='<div class="text-right"><input type="button" class="btn btn-primary nextparticipant" value="Next" /></div>';
		$html +='</div>';
	};
		
	$html += '</div></div>'
    $x += '<div class="bx-styl form1 cartdiv1">'+
                '<form method="post" class="eventform">'+
                '<input type="hidden" name="event[productid]" value="'+eventid+'">'+
                '<input type="hidden" name="event[producttype]" value="'+producttype+'">'+
                '<input type="hidden" name="event[producturl]" value="'+orgurl+'">'+
                '<input type="hidden" name="couponcode" value="'+$("#couponcode").val()+'">'+
                '<input type="hidden" name="pricedistribution" value="'+$("#pricedistribution").val()+'">'+
                '<input type="hidden" name="timeslotid" value="'+$(".datetimeslots .datetimeradio").val()+'">'+
                '<input type="hidden" name="customdates" value="'+$(".customdateselector .customdates").val()+'">'+
                '<input type="hidden" name="donationamount" value="'+(($("#embeddeddonationfield").val() == undefined)?0:$("#embeddeddonationfield").val())+'">'+
                '<input type="hidden" name="guestcheckout" value="'+(($("#guestcheckout").is(':checked') && $("#guestcheckout").val() == 1)?1:0)+'">'+
                '<div class="event-signup-form" id="event-signup-form">'+
                    /*'<tr>'+
                            '<th>Ticket Type</th>'+
                            '<th>Participants\' Details</th>'+
                            '<th>Gender</th>'+
                    '</tr>'+*/
                    $html +
		    $html2 + 
                '</div>'+
                '</form>'+
        '</div>';
    //$(".eventbooking").html($x);
	$(".eventbooking .eventbookingsteps").addClass('hide');
    $(".eventbooking .participants").html($x);
	$(".eventbooking .participants").removeClass('hide');
	$("#participantss").carousel({
		interval:false
	});
	$("#participantss").carousel('pause');
	$("#participantss .nextparticipant").click(function(){
		$("#participantss").carousel('next');
	});
	scrollTo('.eventdetail');
	addDobDropdowns(".dobdropdown");
}
function showattendeeform(ticketid,index){
	console.log('*************');
	console.log(i);
	console.log(j);
	console.log('-------------');
	$html = '';
	$ticketingparams = ['name','email','gender','mobile','dob'];
	$.each(attendeefields,function(i,v){
		if(v[3] == 0)
		{
			return false;
		}
		$html += '<div>';
			$html +=   '<div class="row form-group">';
			$html +=   '<div class="col-md-6">'+v[0]+'</div>';
			
			$html +=   '<div class="col-md-6">';
			if(!(i.startsWith('custom'))){
				if(i == 'dob')
				{
					$html +=	'<div class="row form-group dob dobdropdown"><div class="col-sm-4 col-xs-4"><select class="dobday form-control"></select></div>  <div class="col-sm-4 col-xs-4"><select class="dobmonth form-control"></select></div>  <div class="col-sm-4 col-xs-4"><select class="dobyear form-control" data-dobminage="'+ticketing[ticketid]['agerestrict']+'"></select></div><input type="hidden" name="event[ticketing]['+ticketid+']['+index+'][dob]" class="dobtext"></div>';
				}
				else if(i == 'gender')
				{
					genderstr = {};
					genderstr['male'] = 'checked = "checked"';
					genderstr['female'] = '';
					if(ticketing[ticketid]['type'] == 3)
					{
						if(j == 0)
						{
						genderstr['male'] = 'checked = "checked" onclick="javascript: return false;"';
						genderstr['female'] = 'onclick="javascript: return false;"';
						}
						else if(j == 1)
						{
						genderstr['female'] = 'checked = "checked" onclick="javascript: return false;"';
						genderstr['male'] = 'onclick="javascript: return false;"';
						}
					}
					$html += '<div class="row form-group"><div class="radio col-xs-6"><label class="">'+
							'<input type="radio" value="M" required="" name="event[ticketing]['+ticketid+']['+index+'][gender]" class="gender" '+genderstr['male']+' />'+
							'Male'+
					'</label></div>'+
					'<div class="radio col-xs-6"><label class="">'+
							'<input type="radio" value="F" required="" name="event[ticketing]['+ticketid+']['+index+'][gender]" class="gender" '+genderstr['female']+'/>'+
							'Female'+
					'</label></div></div>';
				}
				else
				{
					$placeholdertext = v[0];
					if($.inArray(i,$ticketingparams) > -1)
					{
					$param = 'ticketing';
					$placeholdertext = 'Enter '+v[0];
					if(i == 'name')
					$placeholdertext = 'Enter Full Name';
					}
					else
					$param = 'answers';
					if(v[2] == 'text' || v[2] == 'email')
					$html += '<input  id="answerss_'+index+'_'+i+'" type="text" placeholder="'+$placeholdertext+'" name="event['+$param+']['+ticketid+']['+index+']['+i+']" required="" class="form-control '+i+'" />';
					else if(v[2] == 'number')
					$html += '<input id="answerss_'+index+'_'+i+'" type="text" placeholder="'+$placeholdertext+'" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="'+v[0]+'" name="event['+$param+']['+ticketid+']['+index+']['+i+']" required="" class="form-control '+i+'" />';
					else if(v[2] == 'longtext')
					$html +=   '<textarea id="answerss_'+index+'_'+i+'" width="100%" style="height:60px;overflow:hidden" required=""  class="form-control '+i+'" name="event['+$param+']['+ticketid+']['+index+']['+i+']"></textarea>';
				}
			}
			else
			{
				if(v[2] == 'text' || v[2] == 'email')
				$html += '<input type="text" id="answerss_'+index+'_'+i+'" placeholder="'+v[0]+'" name="event[answers]['+ticketid+']['+index+']['+i+']" required="" class="form-control '+i+'" />';
				else if(v[2] == 'number')
				$html += '<input type="text" id="answerss_'+index+'_'+i+'" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="'+v[0]+'" name="event[answers]['+ticketid+']['+index+']['+i+']" required="" class="form-control '+i+'" />';
				else if(v[2] == 'longtext')
				$html +=   '<textarea class="form-control" id="answerss_'+index+'_'+i+'" width="100%" style="height:60px;overflow:hidden" required="" name="event[answers]['+ticketid+']['+index+']['+i+']"></textarea>';
			}
			/*
			else if(v['questiontype'] == 'text')
			{
				$html +=   '<textarea class="form-control" id="answerss_'+index+'_'+i+'" width="100%" style="height:30px;overflow:hidden" required="" name="event[answers]['+ticketid+']['+index+']['+i+']"></textarea>';
			}
			else
			{
				$html +=   '<input type="file" placeholder="'+v['question']+'" class="form-control filestyle" id="answerss_'+index+'_'+i+'" value="" required="" name="event[answers]['+ticketid+']['+index+']['+i+']">';
			}
			*/
			$html +=   '</div>';
			
			$html +=   '</div>';
			$html +=   '</div>';
	});
	return $html;
}
function processtopayment(){
    addToLocalObject(q['event_id'],'ticketparticipants',$('.eventform').serializeArray());
    $.post(root_path+'PSAjax.php?type=validateeventbooking', $('.eventform').serializeArray(), function(data){ 
        $res = $.parseJSON(data);
        if($res['status'] == 200)
		{
			processtopaymentsubmit();
		}
		else if($res['status'] == 406)
		{
			scrollTo('.eventdetail');
			if($res['error']['participants'])
			{
				$.each($res['error']['participants'],function(participantseq,data){
					participantseq = parseInt(participantseq);
					$("#participantss").carousel(participantseq);
					$("#participantss").carousel('pause');
					$.each(data,function(type,msg){
						setTimeout(function(){
							showToolTipMsg($("#participantbox"+participantseq+" ."+type).eq(0),msg);
						},1000);
					});
					return false;
				});
			}
			if($res['error']['answers'])
			{
				$.each($res['error']['answers'],function(participantseq,data){
					participantseq = parseInt(participantseq);
					$("#participantss").carousel(participantseq);
					$("#participantss").carousel('pause');
					$.each(data,function(type,msg){
						//console.log("#answerss_"+participantseq+"_"+type);
						setTimeout(function(){
							showToolTipMsg($("#answerss_"+participantseq+"_"+type).eq(0),msg);
						},1000);
					});
					return false;
				});
			}
			if($res['error']['existingemailids'])
			{
				$.each($res['error']['existingemailids'],function(participantseq,data){
					participantseq = parseInt(participantseq);
					$("#participantss").carousel(participantseq);
					$("#participantss").carousel('pause');
					$.each(data,function(type,msg){
						scrollTo($("#participantbox"+participantseq+" ."+type).eq(0));
						setTimeout(function(){
							showToolTipMsg($("#participantbox"+participantseq+" ."+type).eq(0),msg);
						},1000);
					});
					return false;
				});
			}
		}
    });
    $('.eventform .email').each(function(){
	$.post(root_path+'PSAjax.php?type=logactivity&email='+$(this).val()+'&eventid='+q['event_id']+'', $('.eventform').serializeArray(), function(data){ 
	});
	    
    });
    return false;
}
function processtopaymentsubmit(){
    addToLocalObject(q['event_id'],'ticketparticipants',$('.eventform').serializeArray());
    minimal_login_callback = processtopayment;
    $endpoint = root_path+'PSAjax.php?type=eventbooking';
    if ($("#guestcheckout").is(':checked') && $("#guestcheckout").val() == 1)
    $endpoint = root_path+'PSAjax.php?type=eventbookingguestcheckout';
    $.post($endpoint, $('.eventform').serializeArray(), function(data){ 
        $res = $.parseJSON(data);
        if($res['status'] == 200)
		{
			if(typeof $res['leadcaptureflag'] == 'undefined')
			{
				//$('#paybutton').popover({title: "Payment Instruction", content: "Use the 'Guest Checkout' option on payment page", html: false, placement: "auto"}).on("show.bs.popover", function () { $(this).data("bs.popover").tip().css("max-width", "600px"); });
				$('#paybutton').val('Proceeding to Payment');
				//$('#paybutton').popover('show');
				setTimeout(function(){
					formpost('https://secure.payu.in/_payment',$res['paymentdata']);
				}, 2000);
				addToLocalObject(q['event_id'],'transactionstarted',1);
				gaSendEvent('paymentstarted','onLoadGAEvents');
			}
			else
			{
				if(typeof $res['txnid'] != 'undefined')
				{
				    //callmodaliframe('Downloading Ticket',root_path+'module/print?type=event&subtype=ticket&txnid='+q['txnid'],'Please wait while we try generating your ticket.<br />It will be downloaded on your system, please check your downloads folder');
					callmodaliframe('Printing Ticket',root_path+'module/print?type=event&subtype=ticket&txnid='+$res['txnid'],'Please wait while we try generating your ticket.<br />Additionally it will be sent to your registered email id');
					gaSendEvent('registrationleadcaptured','onLoadGAEvents');
					$(".eventbookingsteps").addClass('hide');
					scrollTo(".module.eventbanner");
				}
				else
				{
					myModal("Thank you !","We have forwarded your interest to the event organiser",'');
					gaSendEvent('aggregatedleadcaptured','onLoadGAEvents');
					$(".eventbookingsteps").addClass('hide');
					scrollTo(".module.eventbanner");
				}
			}
		}
		else if($res['status'] == 406)
		{
			
		}
		else if($res['status'] == 4051)
		{
			setTimeout(function(){
				$("#loginemail").val($("#event-signup-form .email").eq(0).val()); 
				$("#minregistername").val($("#event-signup-form .name").eq(0).val()); 
				$("#minregisteremail").val($("#event-signup-form .email").eq(0).val()); 
				$("#minregisterdob").val($("#event-signup-form .dob").eq(0).val()); 
				$("#minregistergender").val($("#event-signup-form .gender").eq(0).val());
				$("#minregisterpassions").val(passiontype);
				//$("#minregisteremail").val($("#event-signup-form .email").eq(0)); 
				$("#minregisterpassword").val('').attr('placeholder','Create Password'); 
			}, 1000);
		}
	
        //loadPage('payumoney/PayUMoney_form.php');
    });
    return false;
}
function ticketing_popup(){
    $ticketingHtml = "<div class='ticketing'>";
    $.each(ticketing , function(i,v){
        $ticketingHtml +=   "<div class='row ticketdetails'>"+
                                "<div class='tname'>"+v['name']+"</div>"+
                                "<div class='tprice'>"+v['price']+"</div>"+
                                "<div class='tcount'><input type='text' value='<?=$event['peruserticketcount']?>' class='ticketbuycount' /></div>"+
                                "<div class='tadd'><a class='btn btn-primary ticketbuy' event='<?=$event['event_id']?>'> Add </a></div>"+
                            "</div>";
    });
    $ticketingHtml += "</div>";
    $ticketingHtml += "<div class=''><a class='btn btn-primary viewcart callmodaliframe' data-targetsrc='module/cart' > View Cart </a></div>";
    myModal("Event Ticketing",$ticketingHtml);
}
$(document).ready(function(){
    if(isMobile == true || $(window).width() < 990)
    {
	$("#tck_dtl").appendTo('.eventbanner');
        $(".countdowntimer").appendTo('.eventbanner');
	$(".donationwdgt").appendTo('.eventdetail');
	$(".orgnzr").appendTo('.eventdetail');
    }
    $("body").on("click",".ticketbuy",function(){
        $temp = $(this);
        $temp2 = $temp.parents(".ticketdetails");
        $teventid = $temp.attr("event");
        $tname = $temp2.find(".tname").text();
        $tprice = $temp2.find(".tprice").text();
        $tcount = $temp2.find(".ticketbuycount").val();
        $params = {productType:'event',productid:$teventid,ticketname:$tname,ticketprice:$tprice,buycount:$tcount};
        $.post(root_path+'PSAjax.php?type=addToCart', $params, function(data){ 
            $temp.html("Added To Cart");
        });
    })
    $("body").on("change",".ticketselector",function(){
        $ticketprice = 0;
        $ticketconvprice = 0;
        $ticketservprice = 0;
        $ticketgrandtotal = 0;
        $tickettotalcount = 0;
        $ticketgranddiscount = 0;
        $selectedtickets = {};
        $(".ticketselector").each(function(){
            $temp = $(this);
            $temp2 = $temp.parents(".ticketcategoryrow");
            $tprice = $temp.attr('data-price');
            $torgprice = $temp.attr('data-orgprice');
            $ttype = $temp.attr('data-ticket'); 
            $tpricetype = $temp.attr('data-pricetype'); 
            $tcount = parseInt($temp.val());
            if($tcount > 0)
            {
            $selectedtickets[$ttype] = {};
            //$selectedtickets[$ttype]['price'] = $tprice;
            $selectedtickets[$ttype]['price'] = $torgprice;
            $selectedtickets[$ttype]['count'] = $tcount;
            $selectedtickets[$ttype]['pricetype'] = $tpricetype;
            }
            $tickettotalcount = $tickettotalcount + $tcount;
            $ttotal = $tcount * $tprice;
            $ticketprice = $ticketprice + $ttotal;
            if($tprice != $torgprice)
            $ticketgranddiscount = $ticketgranddiscount + ($tcount * ($torgprice - $tprice));    
        });
        $data = {};
        $data['eventTypeIdbyFees'] = eventTypeIdbyFees;
        $data['eventSelectedTickets'] = $selectedtickets;
        $data['eventFeepaymentoptions'] = eventFeepaymentoptions;
        $data['eventId'] = q['event_id'];
        $.post(root_path+'PSAjax.php?type=eventticketeffectiveprice',$data, function(data){
            $res = $.parseJSON(data);
            $temp = $res['list'];
	    if(typeof $temp['initialcost'] == 'undefined')
	    $temp['initialcost'] = 0;
            $('.ticketprice').html($temp['initialcost']);
            if($temp['organiserservicetax'] > 0)
	    {
            $('.ticketorganisertax').html(round($temp['organiserservicetax'],0)).parents('tr').eq(0).removeClass('hide');
            $('.tickettotalprice').html(parseInt($('.ticketprice').text()) + parseInt($('.ticketorganisertax').text())).parents('tr').eq(0).removeClass('hide');
	    }
	    else
	    {
	    $('.ticketorganisertax').html(0).parents('tr').eq(0).addClass('hide');
	    $('.tickettotalprice').html(parseInt($('.ticketprice').text()) + parseInt($('.ticketorganisertax').text())).parents('tr').eq(0).addClass('hide');
	    }
			//$('.ticketconv').html(round($temp['processingfee'] + $temp['totaltaxfee'] + $temp['paymentgatewayfee'],0));
			$('.ticketconv').html(round($temp['displaycost'],0) - round($temp['initialcost'],0) - round($temp['organiserservicetax'],0));
			$("#processingfeebreakup").attr('title','A - Processing Fee - '+$temp['processingfee']+'<br />B - Payment Gateway Fee - '+$temp['paymentgatewayfee']+'<br />GST @ 18% on A + B - '+$temp['totaltaxfee']).tooltip('fixTitle').tooltip('show');
            $('.ticketgrandtotal').html(round($temp['displaycost'],0));
			
            $('#pricedistribution').val($temp['distribution']);
			if($ticketgranddiscount > 0)
			{
				$('.grand-discount').removeClass('hide').find('.amount .ticketdiscounttotal').html($ticketgranddiscount);
				$(".net-payable").removeClass("hide").find(".amount .ticketnetpayable").html(parseInt($(".grand-total .ticketgrandtotal").text()) - $ticketgranddiscount);
			}
			else
			{
				$('.grand-discount').addClass('hide').find('.amount .ticketdiscounttotal').html($ticketgranddiscount);
				$(".net-payable").addClass("hide").find(".amount .ticketnetpayable").html(parseInt($(".grand-total .ticketgrandtotal").text()) - $ticketgranddiscount);
			}
			
			$ticketcontribution = parseInt($("#embeddeddonationfield").val());
			if($ticketcontribution < 0)
			$ticketcontribution = 0;
			if($("#embeddeddonationfield").length > 0 &&  $ticketcontribution > 0)
			{
				$('.ticketcontribution').html($ticketcontribution).parents('.user-contribution').removeClass('hide');
				$(".net-payable").removeClass("hide").find(".amount .ticketnetpayable").html(parseInt($(".amount .ticketnetpayable").text()) + $ticketcontribution);
			}
			else
			{
				$("#embeddeddonationfield").val('');
				$('.ticketcontribution').html('').parents('.user-contribution').addClass('hide');
				$(".net-payable").addClass("hide").find(".amount .ticketnetpayable").html(parseInt($(".amount .ticketnetpayable").text()) + $ticketcontribution);
			}
			
	    if($temp['initialcost'] == 0 || $temp['initialcost'] == '')
	    $(".pricebox").addClass('hide');
	    else
	    $(".pricebox").removeClass('hide');
        });
    });
    $("body").on("change",".ticketselector1",function(){
        $ticketprice = 0;
        $ticketconvprice = 0;
        $ticketservprice = 0;
        $ticketgrandtotal = 0;
        $tickettotalcount = 0;
        $ticketgranddiscount = 0;
        $(".ticketselector").each(function(){
            $temp = $(this);
            $temp2 = $temp.parents(".ticketcategoryrow");
            $tprice = $temp.attr('data-price');
            $torgprice = $temp.attr('data-orgprice');
            $ttype = $temp.attr('data-ticket'); 
            $tcount = parseInt($temp.val());
            $tickettotalcount = $tickettotalcount + $tcount;
            $ttotal = $tcount * $tprice;
            $ticketprice = $ticketprice + $ttotal;
            $temp2.find(".ticketvaluedisplay").html($ttotal);
            if($tprice != $torgprice)
            $ticketgranddiscount = $ticketgranddiscount + ($tcount * ($torgprice - $tprice));    
        });
        $ticketconvprice = round((convenience + service*convenience/100)*$tickettotalcount,0); //conveniece + 15percentage of conveineince
        //$ticketconvprice = round((convenience / 100) * $ticketprice ,2);
        //$ticketservprice = round((service / 100) * $ticketconvprice ,2);
        $ticketgrandtotal = $ticketprice + $ticketconvprice + $ticketservprice;
        $('.ticketprice').html($ticketprice);
        $('.ticketconv').html($ticketconvprice);
        $('.ticketserv').html($ticketservprice);
        $('.ticketgrandtotal').html($ticketgrandtotal);
        if($ticketgranddiscount > 0)
        {
            $(".grand-discount .ticketdiscounttotal").html($ticketgranddiscount);
            $(".grand-discount").removeClass('hide');
            $(".grand-total .totaltext").html('Discounted Total');
        }
        else
        {
            $(".grand-discount .ticketdiscounttotal").html(0);
            $(".grand-discount").addClass('hide');
            $(".grand-total .totaltext").html('Grand Total');
        }
        
    });
    $("body").on("change",".choosefullpaymentoption",function(){
	var $pricefield = 'price';
	if ($(this).is(':checked') && $(this).val() == 0) {
		$pricefieldselected = 'bookingprice';
	}
	if ($(this).is(':checked') && $(this).val() == 1) {
		$pricefieldselected = 'price';
	}
	var $currentticket = {};
	$(".ticketcategoryrow").each(function(){
		$ticketrow = $(this);
		$currentticketid = $ticketrow.attr('data-ticket');
		$currentticket = tickettypes[$currentticketid];
		$pricefield = $pricefieldselected;
		if($currentticket['bookingprice'] == 0)
		$pricefield = 'price';
		if($pricefieldselected == 'price')
		{
			$ticketrow.find('.paymenttypedisplaytext').html('( Full Amount )').removeClass('hide');
			$ticketrow.removeClass('hide');
		}
		else if($pricefieldselected == 'bookingprice')
		{
			$ticketrow.find('.paymenttypedisplaytext').html('( Booking Amount )').removeClass('hide');
			if($currentticket['bookingprice'] == 0)
			{
			$ticketrow.addClass('hide'); //hide full amount ticket in case booking amount is selected
			$ticketrow.find('.ticketselector').val(0); //hide full amount ticket in case booking amount is selected
			}
		}
		$ticketrow.find('.price .orgprice span').html('₹ '+$currentticket[$pricefield]);
		$ticketrow.find('.price .userprice').html('₹ '+$currentticket[$pricefield]);
		$ticketrow.find('.ticketselector').attr('data-price',$currentticket[$pricefield]);
		$ticketrow.find('.ticketselector').attr('data-orgprice',$currentticket[$pricefield]);
		$ticketrow.find('.ticketselector').attr('data-pricetype',$pricefield);
	});
	$(".ticketcategoryrow").eq(0).find('.ticketselector').trigger('change');
    });
    $("body").on("change",".choosefullpaymentoptionticketwise",function(){
	var $currentticketid = $(this).data('ticket');
	var $pricefield = 'price';
	if ($(this).is(':checked') && $(this).val() == 0) {
		$pricefield = 'bookingprice';
	}
	if ($(this).is(':checked') && $(this).val() == 1) {
		$pricefield = 'price';
	}
	var $ticketrow = $(".ticketcategoryrow[data-ticket="+$currentticketid+"]");
	console.log($ticketrow);
	console.log(tickettypes[$currentticketid]);
	var $currentticket = tickettypes[$currentticketid];
	$ticketrow.find('.price .orgprice span').html('₹ '+$currentticket[$pricefield]);
	$ticketrow.find('.price .userprice').html('₹ '+$currentticket[$pricefield]);
	$ticketrow.find('.ticketselector').attr('data-price',$currentticket[$pricefield]);
	$ticketrow.find('.ticketselector').attr('data-orgprice',$currentticket[$pricefield]);
	$ticketrow.find('.ticketselector').attr('data-pricetype',$pricefield);
	//$ticketrow.find('.ticketselector').trigger('change');
    });
    $(".ticketcategoryrow").eq(0).find('.ticketselector').trigger('change');
    $printticket = q['printticket'];
    if(q['action'] && q['action'] == 'claimeventfromemail')
    {
    claimeventfromemail();
    gaSendEvent('claimeventfromemail','onLoadGAEvents');
    }
    else if(q['action'] && q['action'] == 'eventauthorizeotherprofile')
    {
    eventauthorizeotherprofile();
    gaSendEvent('eventauthorizeotherprofile','onLoadGAEvents');
    }
    if($printticket && q['txnid'])
    {
            //callmodaliframe('Downloading Ticket',root_path+'module/print?type=event&subtype=ticket&txnid='+q['txnid'],'Please wait while we try generating your ticket.<br />It will be downloaded on your system, please check your downloads folder');
            callmodaliframe('Printing Ticket',root_path+'module/print?type=event&subtype=ticket&txnid='+q['txnid'],'Please wait while we try generating your ticket.<br />Additionally it will be sent to your registered email id');
	gaSendEvent('printeventticket','onLoadGAEvents');
    }
	$("#donateamount").on('focus',function(){
		if($(".donationuserdata.hide").length > 0)
		scrollTo(".widget.donation");
		$(".donationuserdata").removeClass('hide');
	});
	$("#donatebutton").on('click',function(){
		$amount = parseInt($("#donateamount").val());
		if($amount <=0)
		{
			showToolTipMsg('#donateamount','Donation amount show be greated than 0');
		}
		else if($("#donateename").val() == '' || $("#donateemobile").val() == '' || $("#donateeemail").val() == '')
		{
			showToolTipMsg('#donatebutton','Please fill in all the fields above');
		}
		else if(!(isValidEmailAddress($("#donateeemail").val())))
		{
			showToolTipMsg('#donateeemail','Provide valid email');
		}
		else
		{
		minimal_login_callback = proceedwithdonation;
		$.post(root_path+'PSAjax.php?type=startdonation', {amount:$("#donateamount").val(),name:$("#donateename").val(),mobile:$("#donateemobile").val(),email:$("#donateeemail").val(),producttype:producttype,productid:eventid,producturl:orgurl}, function(data){ 
			$res = $.parseJSON(data);
			if($res['status'] == 200)
			{
				//$('#paybutton').popover({title: "Payment Instruction", content: "Use the 'Guest Checkout' option on payment page", html: false, placement: "auto"}).on("show.bs.popover", function () { $(this).data("bs.popover").tip().css("max-width", "600px"); });
				$('#donatebutton').val('Proceeding to Payment Page');
				//$('#paybutton').popover('show');
				setTimeout(function(){
					formpost('https://secure.payu.in/_payment',$res['paymentdata']);
				}, 2000);
				addToLocalObject(q['event_id'],'donationstarted',1);
			}
			else if($res['status'] == 406)
			{
				
			}
			else if($res['status'] == 4051)
			{
				setTimeout(function(){
					$("#loginemail").val($("#donateeemail").val()); 
					$("#minregistername").val($("#donateename").val()); 
					$("#minregisteremail").val($("#donateeemail").val()); 
					$("#minregisterdob").val(); 
					$("#minregistergender").val();
					$("#minregisterpassions").val(passiontype);
					//$("#minregisteremail").val($("#event-signup-form .email").eq(0)); 
					$("#minregisterpassword").val('').attr('placeholder','Create Password'); 
				}, 1000);
			}
		});
		}
	});
	$("body").on('click','#embeddeddonationbtn',function(){
		$(".ticketselector").trigger('change');
	});
	$("body").on('blur','#embeddeddonationfield',function(){
		$(".ticketselector").trigger('change');
	});
});
function proceedwithdonation(){
	console.log('logged in , donation click triggered');
	$("#donatebutton").trigger('click');
}
function applyCoupon(){
    var $tickets = {};
    $(".ticketselector").each(function(i,v){
       $tickets[$(this).data('ticket')] = parseInt($(this).val()); 
    });
    console.log($("#couponcode").val());
    console.log($tickets);
    console.log('**********');
    if($("#couponcode").val().length > 0 && $tickets)
    {
    $.post(root_path+'PSAjax.php?type=applycoupon', {entityId:q['event_id'],entityType:'event',couponCode:$("#couponcode").val(),tickets:$tickets}, function(data){ 
        $res = $.parseJSON(data);
        $.each($res['list'],function(i,v){
            
        });
        $totaldiscount = 0;
        $(".ticketcategoryrow").each(function(i,v){
           $temp = $(this).attr('data-ticket');
		$ticketselector = $(this).find('.ticketselector');
		if($ticketselector.attr('data-pricetype') == 'price')
               $price = ticketing[$temp]['price'];
	       else if($ticketselector.attr('data-pricetype') == 'bookingprice')
	       $price = ticketing[$temp]['bookingprice'];
	       
		$price = parseInt($price);
		$price = $price + (organiserservicetax / 100 * $price);
               $discount = 0;
               if(typeof $res['list']['All'] != 'undefined')
               $res['list'][$temp] = $res['list']['All'];
               if(typeof $res['list'][$temp] != 'undefined')
               {
                    if($res['list'][$temp]['couponType'] == 'Percentage')
                    {
                        $discount = round(($price * parseInt($res['list'][$temp]['couponDiscount']) / 100),0);
                    }
                    else if($res['list'][$temp]['couponType'] == 'Fixed')
                    {
                        $discount = round(parseInt($res['list'][$temp]['couponDiscount']),0);
                    }
                    //$(this).find('.price .orgprice').removeClass('hide');
               }
               else
               {
                   $(this).find('.price .orgprice').addClass('hide');
               }
	       $newPrice = $price - $discount;
	       var $displayprice = $newPrice;
               
	       //$(this).find('.price .userprice').html('₹ '+$displayprice);
               $(this).find('.ticketselector').attr('data-price',$newPrice);
               //$(this).find('.ticketselector').trigger('change');
	       
	       $totaldiscount = $totaldiscount + ($(this).find('.ticketselector').val() * $discount);
	});
	$(".grand-discount").removeClass("hide").find(".amount .ticketdiscounttotal").html($totaldiscount);
	$(".net-payable").removeClass("hide").find(".amount .ticketnetpayable").html(parseInt($(".grand-total .ticketgrandtotal").text()) - $totaldiscount);
    });
    }
}
function dateselectionschanged(dates,el)
{
	$.each(dates,function(i,v){
		dates[i] = getFormattedDate(v);
	});
	var datestr = dates.join(); 
	$(".customdateselector .customdates").val(datestr);
	$(".customdateselector .btn-primary").removeClass('hide');
}
function claimeventfromemail(){
	minimal_login_callback = claimeventfromemail;
	//$.post(root_path+'PSAjax.php?type=claimeventfromemail&',{event_id:q['event_id'],email_id:q['email_id'],mailtoken:q['mailtoken'],time:q['t']}, function(data){
	$.post(root_path+'PSAjax.php?type=claimeventfromemail&',{event_id:q['event_id'],mailtoken:q['mailtoken']}, function(data){
		$res = $.parseJSON(data);
		if($res['status'] == 200 || $res['status'] == 403)
		{
			myModal($res['responsearray']['title'],'<div class="msgbox1 text-center">'+$res['responsearray']['content']+'</div>','cframe');
		}
		else if($res['status'] == 406)
		{
			
		}
		else if($res['status'] == 4051)
		{
			setTimeout(function(){
				$("#loginemail").val(q['email_id']); 
				$("#minregisteremail").val(q['email_id']); 
			}, 1000);
		}
	});
}
function eventauthorizeotherprofile(){
	minimal_login_callback = eventauthorizeotherprofile;
	$.post(root_path+'PSAjax.php?type=eventauthorizeotherprofile&',{event_id:q['event_id'],mailtoken:q['mailtoken']}, function(data){
		$res = $.parseJSON(data);
		if($res['status'] == 200 || $res['status'] == 403)
		{
			myModal($res['responsearray']['title'],'<div class="msgbox1 text-center">'+$res['responsearray']['content']+'</div>','cframe');
		}
		else if($res['status'] == 406)
		{
			
		}
		else if($res['status'] == 4051)
		{
			setTimeout(function(){
				$("#loginemail").val(q['email_id']); 
				$("#minregisteremail").val(q['email_id']); 
			}, 1000);
		}
	});
}
function claimevent(){
	$.post(root_path+'PSAjax.php?type=claimevent',{event_id:q['event_id']}, function(data){
		$res = $.parseJSON(data);
		if($res['status'] == 200 || $res['status'] == 403)
		{
			myModal($res['responsearray']['title'],'<div class="msgbox1 text-center">'+$res['responsearray']['content']+'</div>','cframe');
		}
	});
}
function showleadcaptureform(){
	showdatetimeslots();
	$('.ticketselector option[value=1]').attr('selected','selected').trigger('change');
	$('#addAttendees').trigger('click');
}