/**
 * jQuery Unveil
 * A very lightweight jQuery plugin to lazy load images
 * http://luis-almeida.github.com/unveil
 *
 * Licensed under the MIT license.
 * Copyright 2013 Luís Almeida
 * https://github.com/luis-almeida
 */

;(function($) {

  $.fn.unveil = function(threshold, callback) {

    var $w = $(window),
        th = threshold || 0,
        retina = window.devicePixelRatio > 1,
        attrib = retina? "data-src-retina" : "data-src",
        images = this,
        loaded;

    this.one("unveil", function() {
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-src");
      if (source) {
        if($(this).is('img'))
		{
		//this.setAttribute("src", source);
		$tempimg = $(this);
		// var img = $("<img />").attr('init-src',$tempimg.attr('init-src')).attr('src', source)
		var img = $(this).attr('src', source)
		    .on('load', function() {
				this.addEventListener("error", function(){
					console.log('error occured');
				});
				if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0 || (this.naturalWidth == 100 && this.naturalHeight == 66)) 			  {
					$(this).attr('src',$(this).attr('init-src'));
				} else {
				}
		    });
		}
        else
        this.style.backgroundImage = "url('"+source+"')";
        if (typeof callback === "function") callback.call(this);
      }
    });

    function unveil() {
      var inview = images.filter(function() {
        var $e = $(this);
        if ($e.is(":hidden")) return;

        var wt = $w.scrollTop(),
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();

        return eb >= wt - th && et <= wb + th;
      });

      loaded = inview.trigger("unveil");
      images = images.not(loaded);
    }

    $w.on("scroll.unveil resize.unveil lookup.unveil", unveil);

    unveil();

    return this;

  };

})(window.jQuery || window.Zepto);
