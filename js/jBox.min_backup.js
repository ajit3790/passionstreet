function openJBox(n, t) {
    if (n != "lastOrFirst") {
        jBoxContainer.fadeIn(animationSpeed);
        var i, r;
        t == "next" && (i = "fade-in-right", r = "fade-out-left");
        t == "prev" && (i = "fade-in-left", r = "fade-out-right");
        t != "first" ? transitionIsOver && (jBoxContainerImg.addClass(r), setTimeout(function() {
            jBoxContainerImg.removeClass(r);
            jBoxContainerImg.addClass(i);
            transitionIsOver = !1;
            jBoxContainerImg.attr("src", n.replace("thumb/",""));
            altTextBox.text(getAltText(n));
            setTimeout(function() {
                jBoxContainerImg.removeClass(i);
                transitionIsOver = !0
            }, cssAnimationDuration)
        }, cssAnimationDuration)) : (jBoxContainerImg.attr("src", n.replace("thumb/","")), altTextBox.text(getAltText(n)));
        n == firstImg.replace("thumb/","") ? disableButton("prev") : enableButton("prev");
        n == lastImg.replace("thumb/","") ? disableButton("next") : enableButton("next");
        disableScroll();
        jboxOpen = 1;
    }
}

function closeJBox() {
    jBoxContainer.fadeOut(animationSpeed);
    jBoxContainerImg.attr("src", "");
    enableScroll();
    jboxOpen = 0;
}

function getNextImg(n) {
    var r, i, t ,temp;
    if (n == "prev" && jBoxContainerImg.attr("src") == gallery[0].getAttribute("data-src").replace("thumb/","") || n == "next" && jBoxContainerImg.attr("src") == gallery[gallery.length - 1].getAttribute("data-src").replace("thumb/","")) return "lastOrFirst";
    if (r = jBoxContainerImg.attr("src"), n == "next") {
        for (t = 0; t < gallery.length; t++)
            if (r == gallery[t].getAttribute("data-src").replace("thumb/","")) {
                t++;
                i = gallery[t].getAttribute("data-src").replace("thumb/","");
                break
            }
        return i
    }
    if (n == "prev") {
        for (t = 0; t < gallery.length; t++)
            if (r == gallery[t].getAttribute("data-src").replace("thumb/","")) {
                t--;
                i = gallery[t].getAttribute("data-src").replace("thumb/","");
                break
            }
        return i
    }
}

function getAltText(n) {
    for (var t = "", i = 0; i < gallery.length - 1; i++)
        if (n == gallery[i].getAttribute("data-src")) {
            t = gallery[i].getAttribute("alt");
            break
        }
    return t == null && (t = ""), t
}

function disableButton(n) {
    n == "prev" && buttonPrev.addClass("hidden-btn");
    n == "next" && buttonNext.addClass("hidden-btn")
}

function enableButton(n) {
    n == "prev" && buttonPrev.removeClass("hidden-btn");
    n == "next" && buttonNext.removeClass("hidden-btn")
}

function buttonNextPopUp() {
    mouseIsOnButtons || (buttonNext.addClass("pop-up"), setTimeout(function() {
        buttonNext.removeClass("pop-up")
    }, buttonPopUpDelay))
}

function buttonPrevPopUp() {
    mouseIsOnButtons || (buttonPrev.addClass("pop-up"), setTimeout(function() {
        buttonPrev.removeClass("pop-up")
    }, buttonPopUpDelay))
}

function buttonClosePopUp() {
    mouseIsOnButtons || (buttonClose.addClass("pop-up"), setTimeout(function() {
        buttonClose.removeClass("pop-up")
    }, buttonPopUpDelay))
}

function preventDefault(n) {
    n = n || window.event;
    n.preventDefault && n.preventDefault();
    n.returnValue = !1
}

function preventDefaultForScrollKeys(n) {
    if (keys[n.keyCode]) return preventDefault(n), !1
}

function disableScroll() {
    window.addEventListener && window.addEventListener("DOMMouseScroll", preventDefault, !1);
    window.onwheel = preventDefault;
    window.onmousewheel = document.onmousewheel = preventDefault;
    window.ontouchmove = preventDefault;
    document.onkeydown = preventDefaultForScrollKeys
}

function enableScroll() {
    window.removeEventListener && window.removeEventListener("DOMMouseScroll", preventDefault, !1);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null
}
var keys;
(function(n) {
    typeof define == "function" && define.amd && define.amd.jQuery ? define(["jquery"], n) : typeof module != "undefined" && module.exports ? n(require("jquery")) : n(jQuery)
})(function(n) {
    function ft(t) {
        return t && t.allowPageScroll === undefined && (t.swipe !== undefined || t.swipeStatus !== undefined) && (t.allowPageScroll = p), t.click !== undefined && t.tap === undefined && (t.tap = t.click), t || (t = {}), t = n.extend({}, n.fn.swipe.defaults, t), this.each(function() {
            var r = n(this),
                i = r.data(h);
            i || (i = new et(this, t), r.data(h, i))
        })
    }

    function et(ft, et) {
        function tr(t) {
            if (!pu() && !(n(t.target).closest(et.excludedElements, ot).length > 0)) {
                var r = t.originalEvent ? t.originalEvent : t,
                    f, u = r.touches,
                    e = u ? u[0] : r;
                return (st = rt, u ? ct = u.length : et.preventDefaultEvents !== !1 && t.preventDefault(), at = 0, vt = null, yt = null, kt = null, lt = 0, gt = 0, ni = 0, pt = 1, bt = 0, li = ku(), dr(), wi(0, e), !u || ct === et.fingers || et.fingers === l || oi() ? (gi = ii(), ct == 2 && (wi(1, u[1]), gt = ni = cr(ht[0].start, ht[1].start)), (et.swipeStatus || et.pinchStatus) && (f = wt(r, st))) : f = !1, f === !1) ? (st = i, wt(r, st), f) : (et.hold && (ei = setTimeout(n.proxy(function() {
                    ot.trigger("hold", [r.target]);
                    et.hold && (f = et.hold.call(ot, r, r.target))
                }, this), et.longTapThreshold)), pi(!0), null)
            }
        }

        function ir(n) {
            var f = n.originalEvent ? n.originalEvent : n,
                e, h;
            if (st !== t && st !== i && !yi()) {
                var s, r = f.touches,
                    c = r ? r[0] : f,
                    u = gr(c);
                ai = ii();
                r && (ct = r.length);
                et.hold && clearTimeout(ei);
                st = o;
                ct == 2 && (gt == 0 ? (wi(1, r[1]), gt = ni = cr(ht[0].start, ht[1].start)) : (gr(r[1]), ni = cr(ht[0].end, ht[1].end), kt = gu(ht[0].end, ht[1].end)), pt = du(gt, ni), bt = Math.abs(gt - ni));
                ct === et.fingers || et.fingers === l || !r || oi() ? (vt = iu(u.start, u.end), yt = iu(u.last, u.end), uu(n, yt), at = nf(u.start, u.end), lt = tu(), bu(vt, at), s = wt(f, st), (!et.triggerOnTouchEnd || et.triggerOnTouchLeave) && (e = !0, et.triggerOnTouchLeave && (h = rf(this), e = uf(u.end, h)), !et.triggerOnTouchEnd && e ? st = fr(o) : et.triggerOnTouchLeave && !e && (st = fr(t)), (st == i || st == t) && wt(f, st))) : (st = i, wt(f, st));
                s === !1 && (st = i, wt(f, st))
            }
        }

        function rr(n) {
            var r = n.originalEvent ? n.originalEvent : n,
                u = r.touches;
            if (u) {
                if (u.length && !yi()) return yu(r), !0;
                if (u.length && yi()) return !0
            }
            return yi() && (ct = nr), ai = ii(), lt = tu(), or() || !er() ? (st = i, wt(r, st)) : et.triggerOnTouchEnd || et.triggerOnTouchEnd == !1 && st === o ? (et.preventDefaultEvents !== !1 && n.preventDefault(), st = t, wt(r, st)) : !et.triggerOnTouchEnd && br() ? (st = t, dt(r, st, k)) : st === o && (st = i, wt(r, st)), pi(!1), null
        }

        function ui() {
            ct = 0;
            ai = 0;
            gi = 0;
            gt = 0;
            ni = 0;
            pt = 1;
            dr();
            pi(!1)
        }

        function ur(n) {
            var i = n.originalEvent ? n.originalEvent : n;
            et.triggerOnTouchLeave && (st = fr(t), wt(i, st))
        }

        function lr() {
            ot.unbind(hi, tr);
            ot.unbind(ci, ui);
            ot.unbind(ki, ir);
            ot.unbind(di, rr);
            ri && ot.unbind(ri, ur);
            pi(!1)
        }

        function fr(n) {
            var r = n,
                f = ar(),
                u = er(),
                e = or();
            return !f || e ? r = i : u && n == o && (!et.triggerOnTouchEnd || et.triggerOnTouchLeave) ? r = t : !u && n == t && et.triggerOnTouchLeave && (r = i), r
        }

        function wt(n, r) {
            var u, f = n.touches;
            return (eu() || sr()) && (u = dt(n, r, w)), (fu() || oi()) && u !== !1 && (u = dt(n, r, b)), au() && u !== !1 ? u = dt(n, r, tt) : vu() && u !== !1 ? u = dt(n, r, it) : lu() && u !== !1 && (u = dt(n, r, k)), r === i && (sr() && (u = dt(n, r, w)), oi() && (u = dt(n, r, b)), ui(n)), r === t && (f ? f.length || ui(n) : ui(n)), u
        }

        function dt(o, s, h) {
            var c;
            if (h == w) {
                if (ot.trigger("swipeStatus", [s, vt || null, at || 0, lt || 0, ct, ht, yt]), et.swipeStatus && (c = et.swipeStatus.call(ot, o, s, vt || null, at || 0, lt || 0, ct, ht, yt), c === !1)) return !1;
                if (s == t && yr()) {
                    if (clearTimeout(fi), clearTimeout(ei), ot.trigger("swipe", [vt, at, lt, ct, ht, yt]), et.swipe && (c = et.swipe.call(ot, o, vt, at, lt, ct, ht, yt), c === !1)) return !1;
                    switch (vt) {
                        case r:
                            ot.trigger("swipeLeft", [vt, at, lt, ct, ht, yt]);
                            et.swipeLeft && (c = et.swipeLeft.call(ot, o, vt, at, lt, ct, ht, yt));
                            break;
                        case u:
                            ot.trigger("swipeRight", [vt, at, lt, ct, ht, yt]);
                            et.swipeRight && (c = et.swipeRight.call(ot, o, vt, at, lt, ct, ht, yt));
                            break;
                        case f:
                            ot.trigger("swipeUp", [vt, at, lt, ct, ht, yt]);
                            et.swipeUp && (c = et.swipeUp.call(ot, o, vt, at, lt, ct, ht, yt));
                            break;
                        case e:
                            ot.trigger("swipeDown", [vt, at, lt, ct, ht, yt]);
                            et.swipeDown && (c = et.swipeDown.call(ot, o, vt, at, lt, ct, ht, yt))
                    }
                }
            }
            if (h == b) {
                if (ot.trigger("pinchStatus", [s, kt || null, bt || 0, lt || 0, ct, pt, ht]), et.pinchStatus && (c = et.pinchStatus.call(ot, o, s, kt || null, bt || 0, lt || 0, ct, pt, ht), c === !1)) return !1;
                if (s == t && vr()) switch (kt) {
                    case v:
                        ot.trigger("pinchIn", [kt || null, bt || 0, lt || 0, ct, pt, ht]);
                        et.pinchIn && (c = et.pinchIn.call(ot, o, kt || null, bt || 0, lt || 0, ct, pt, ht));
                        break;
                    case y:
                        ot.trigger("pinchOut", [kt || null, bt || 0, lt || 0, ct, pt, ht]);
                        et.pinchOut && (c = et.pinchOut.call(ot, o, kt || null, bt || 0, lt || 0, ct, pt, ht))
                }
            }
            return h == k ? (s === i || s === t) && (clearTimeout(fi), clearTimeout(ei), hr() && !su() ? (ti = ii(), fi = setTimeout(n.proxy(function() {
                ti = null;
                ot.trigger("tap", [o.target]);
                et.tap && (c = et.tap.call(ot, o, o.target))
            }, this), et.doubleTapThreshold)) : (ti = null, ot.trigger("tap", [o.target]), et.tap && (c = et.tap.call(ot, o, o.target)))) : h == tt ? (s === i || s === t) && (clearTimeout(fi), clearTimeout(ei), ti = null, ot.trigger("doubletap", [o.target]), et.doubleTap && (c = et.doubleTap.call(ot, o, o.target))) : h == it && (s === i || s === t) && (clearTimeout(fi), ti = null, ot.trigger("longtap", [o.target]), et.longTap && (c = et.longTap.call(ot, o, o.target))), c
        }

        function er() {
            var n = !0;
            return et.threshold !== null && (n = at >= et.threshold), n
        }

        function or() {
            var n = !1;
            return et.cancelThreshold !== null && vt !== null && (n = nu(vt) - at >= et.cancelThreshold), n
        }

        function ru() {
            return et.pinchThreshold !== null ? bt >= et.pinchThreshold : !0
        }

        function ar() {
            return et.maxTimeThreshold ? lt >= et.maxTimeThreshold ? !1 : !0 : !0
        }

        function uu(n, t) {
            if (et.preventDefaultEvents !== !1)
                if (et.allowPageScroll === p) n.preventDefault();
                else {
                    var i = et.allowPageScroll === nt;
                    switch (t) {
                        case r:
                            (et.swipeLeft && i || !i && et.allowPageScroll != d) && n.preventDefault();
                            break;
                        case u:
                            (et.swipeRight && i || !i && et.allowPageScroll != d) && n.preventDefault();
                            break;
                        case f:
                            (et.swipeUp && i || !i && et.allowPageScroll != g) && n.preventDefault();
                            break;
                        case e:
                            (et.swipeDown && i || !i && et.allowPageScroll != g) && n.preventDefault()
                    }
                }
        }

        function vr() {
            var n = pr(),
                t = wr(),
                i = ru();
            return n && t && i
        }

        function oi() {
            return !!(et.pinchStatus || et.pinchIn || et.pinchOut)
        }

        function fu() {
            return !!(vr() && oi())
        }

        function yr() {
            var n = ar(),
                t = er(),
                i = pr(),
                r = wr(),
                u = or();
            return !u && r && i && t && n
        }

        function sr() {
            return !!(et.swipe || et.swipeStatus || et.swipeLeft || et.swipeRight || et.swipeUp || et.swipeDown)
        }

        function eu() {
            return !!(yr() && sr())
        }

        function pr() {
            return ct === et.fingers || et.fingers === l || !c
        }

        function wr() {
            return ht[0].end.x !== 0
        }

        function br() {
            return !!et.tap
        }

        function hr() {
            return !!et.doubleTap
        }

        function ou() {
            return !!et.longTap
        }

        function kr() {
            if (ti == null) return !1;
            var n = ii();
            return hr() && n - ti <= et.doubleTapThreshold
        }

        function su() {
            return kr()
        }

        function hu() {
            return (ct === 1 || !c) && (isNaN(at) || at < et.threshold)
        }

        function cu() {
            return lt > et.longTapThreshold && at < ut
        }

        function lu() {
            return !!(hu() && br())
        }

        function au() {
            return !!(kr() && hr())
        }

        function vu() {
            return !!(cu() && ou())
        }

        function yu(n) {
            vi = ii();
            nr = n.touches.length + 1
        }

        function dr() {
            vi = 0;
            nr = 0
        }

        function yi() {
            var n = !1,
                t;
            return vi && (t = ii() - vi, t <= et.fingerReleaseThreshold && (n = !0)), n
        }

        function pu() {
            return !!(ot.data(h + "_intouch") === !0)
        }

        function pi(n) {
            ot && (n === !0 ? (ot.bind(ki, ir), ot.bind(di, rr), ri && ot.bind(ri, ur)) : (ot.unbind(ki, ir, !1), ot.unbind(di, rr, !1), ri && ot.unbind(ri, ur, !1)), ot.data(h + "_intouch", n === !0))
        }

        function wi(n, t) {
            var i = {
                start: {
                    x: 0,
                    y: 0
                },
                last: {
                    x: 0,
                    y: 0
                },
                end: {
                    x: 0,
                    y: 0
                }
            };
            return i.start.x = i.last.x = i.end.x = t.pageX || t.clientX, i.start.y = i.last.y = i.end.y = t.pageY || t.clientY, ht[n] = i, i
        }

        function gr(n) {
            var i = n.identifier !== undefined ? n.identifier : 0,
                t = wu(i);
            return t === null && (t = wi(i, n)), t.last.x = t.end.x, t.last.y = t.end.y, t.end.x = n.pageX || n.clientX, t.end.y = n.pageY || n.clientY, t
        }

        function wu(n) {
            return ht[n] || null
        }

        function bu(n, t) {
            t = Math.max(t, nu(n));
            li[n].distance = t
        }

        function nu(n) {
            return li[n] ? li[n].distance : undefined
        }

        function ku() {
            var n = {};
            return n[r] = bi(r), n[u] = bi(u), n[f] = bi(f), n[e] = bi(e), n
        }

        function bi(n) {
            return {
                direction: n,
                distance: 0
            }
        }

        function tu() {
            return ai - gi
        }

        function cr(n, t) {
            var i = Math.abs(n.x - t.x),
                r = Math.abs(n.y - t.y);
            return Math.round(Math.sqrt(i * i + r * r))
        }

        function du(n, t) {
            var i = t / n * 1;
            return i.toFixed(2)
        }

        function gu() {
            return pt < 1 ? y : v
        }

        function nf(n, t) {
            return Math.round(Math.sqrt(Math.pow(t.x - n.x, 2) + Math.pow(t.y - n.y, 2)))
        }

        function tf(n, t) {
            var r = n.x - t.x,
                u = t.y - n.y,
                f = Math.atan2(u, r),
                i = Math.round(f * 180 / Math.PI);
            return i < 0 && (i = 360 - Math.abs(i)), i
        }

        function iu(n, t) {
            var i = tf(n, t);
            return i <= 45 && i >= 0 ? r : i <= 360 && i >= 315 ? r : i >= 135 && i <= 225 ? u : i > 45 && i < 135 ? e : f
        }

        function ii() {
            var n = new Date;
            return n.getTime()
        }

        function rf(t) {
            t = n(t);
            var i = t.offset();
            return {
                left: i.left,
                right: i.left + t.outerWidth(),
                top: i.top,
                bottom: i.top + t.outerHeight()
            }
        }

        function uf(n, t) {
            return n.x > t.left && n.x < t.right && n.y > t.top && n.y < t.bottom
        }
        var et = n.extend({}, et),
            si = c || s || !et.fallbackToMouseEvents,
            hi = si ? s ? a ? "MSPointerDown" : "pointerdown" : "touchstart" : "mousedown",
            ki = si ? s ? a ? "MSPointerMove" : "pointermove" : "touchmove" : "mousemove",
            di = si ? s ? a ? "MSPointerUp" : "pointerup" : "touchend" : "mouseup",
            ri = si ? s ? "mouseleave" : null : "mouseleave",
            ci = s ? a ? "MSPointerCancel" : "pointercancel" : "touchcancel",
            at = 0,
            vt = null,
            yt = null,
            lt = 0,
            gt = 0,
            ni = 0,
            pt = 1,
            bt = 0,
            kt = 0,
            li = null,
            ot = n(ft),
            st = "start",
            ct = 0,
            ht = {},
            gi = 0,
            ai = 0,
            vi = 0,
            nr = 0,
            ti = 0,
            fi = null,
            ei = null;
        try {
            ot.bind(hi, tr);
            ot.bind(ci, ui)
        } catch (ff) {
            n.error("events not supported " + hi + "," + ci + " on jQuery.swipe")
        }
        this.enable = function() {
            return ot.bind(hi, tr), ot.bind(ci, ui), ot
        };
        this.disable = function() {
            return lr(), ot
        };
        this.destroy = function() {
            lr();
            ot.data(h, null);
            ot = null
        };
        this.option = function(t, i) {
            if (typeof t == "object") et = n.extend(et, t);
            else if (et[t] !== undefined) {
                if (i === undefined) return et[t];
                et[t] = i
            } else if (t) n.error("Option " + t + " does not exist on jQuery.swipe.options");
            else return et;
            return null
        }
    }
    var r = "left",
        u = "right",
        f = "up",
        e = "down",
        v = "in",
        y = "out",
        p = "none",
        nt = "auto",
        w = "swipe",
        b = "pinch",
        k = "tap",
        tt = "doubletap",
        it = "longtap",
        d = "horizontal",
        g = "vertical",
        l = "all",
        ut = 10,
        rt = "start",
        o = "move",
        t = "end",
        i = "cancel",
        c = "ontouchstart" in window,
        a = window.navigator.msPointerEnabled && !window.navigator.pointerEnabled && !c,
        s = (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && !c,
        h = "TouchSwipe";
    n.fn.swipe = function(t) {
        var r = n(this),
            i = r.data(h);
        if (i && typeof t == "string") {
            if (i[t]) return i[t].apply(this, Array.prototype.slice.call(arguments, 1));
            n.error("Method " + t + " does not exist on jQuery.swipe")
        } else if (i && typeof t == "object") i.option.apply(this, arguments);
        else if (!i && (typeof t == "object" || !t)) return ft.apply(this, arguments);
        return r
    };
    n.fn.swipe.version = "1.6.15";
    n.fn.swipe.defaults = {
        fingers: 1,
        threshold: 75,
        cancelThreshold: null,
        pinchThreshold: 20,
        maxTimeThreshold: null,
        fingerReleaseThreshold: 250,
        longTapThreshold: 500,
        doubleTapThreshold: 200,
        swipe: null,
        swipeLeft: null,
        swipeRight: null,
        swipeUp: null,
        swipeDown: null,
        swipeStatus: null,
        pinchIn: null,
        pinchOut: null,
        pinchStatus: null,
        click: null,
        tap: null,
        doubleTap: null,
        longTap: null,
        hold: null,
        triggerOnTouchEnd: !0,
        triggerOnTouchLeave: !1,
        allowPageScroll: "auto",
        fallbackToMouseEvents: !0,
        excludedElements: "label, button, input, select, textarea, a, .noSwipe",
        preventDefaultEvents: !0
    };
    n.fn.swipe.phases = {
        PHASE_START: rt,
        PHASE_MOVE: o,
        PHASE_END: t,
        PHASE_CANCEL: i
    };
    n.fn.swipe.directions = {
        LEFT: r,
        RIGHT: u,
        UP: f,
        DOWN: e,
        IN: v,
        OUT: y
    };
    n.fn.swipe.pageScroll = {
        NONE: p,
        HORIZONTAL: d,
        VERTICAL: g,
        AUTO: nt
    };
    n.fn.swipe.fingers = {
        ONE: 1,
        TWO: 2,
        THREE: 3,
        FOUR: 4,
        FIVE: 5,
        ALL: l
    }
});
var currentGallery = $(".jbox-img");
var jBoxContainer = $(".jbox-container"),
    jBoxContainerImg = $(".jbox-container img"),
    altTextBox = $(".jbox-container .img-alt-text"),
    buttonPrev = $(".jbox-container #prev"),
    buttonNext = $(".jbox-container #next"),
    buttonClose = $(".jbox-container #close"),
    buttons = $(".jbox-container #next, .jbox-container #prev, .jbox-container #close"),
    animationSpeed = 250,
    cssAnimationDuration = 150,
    mouseIsOnImg = !1,
    mouseIsOnButtons = !1,
    buttonPopUpDelay = 300,
    transitionIsOver = !0,
    gallery = currentGallery,
    lastImg = gallery[gallery.length - 1].getAttribute("data-src"),
    firstImg = gallery[0].getAttribute("data-src"),
    jboxOpen = 0;
jBoxContainerImg.mouseenter(function() {
    mouseIsOnImg = !0
});
jBoxContainerImg.mouseleave(function() {
    mouseIsOnImg = !1
});
buttons.mouseenter(function() {
    mouseIsOnImg = !0;
    mouseIsOnButtons = !0
});
buttons.mouseleave(function() {
    mouseIsOnImg = !1;
    mouseIsOnButtons = !1
});
jBoxContainer.click(function() {
    mouseIsOnImg || closeJBox()
});
buttonNext.click(function() {
    openJBox(getNextImg("next"), "next")
});
buttonPrev.click(function() {
    openJBox(getNextImg("prev"), "next")
});
buttonClose.click(function() {
    closeJBox()
});
jBoxContainer.swipe({
    swipe: function(n, t) {
        t == "left" && openJBox(getNextImg("next"), "next");
        t == "right" && openJBox(getNextImg("prev"), "prev");
        (t == "up" || t == "down") && closeJBox()
    }
});
$(document).keyup(function(n) {
    if(jboxOpen == 1){
        n.keyCode == 27 && (closeJBox(), buttonClosePopUp());
        (n.keyCode || n.which) == 37 && (openJBox(getNextImg("prev"), "prev"), buttonPrevPopUp());
        (n.keyCode || n.which) == 39 && (openJBox(getNextImg("next"), "next"), buttonNextPopUp())
    }
});
keys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
};
//# sourceMappingURL=jBox.min.js.map