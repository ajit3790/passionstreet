$(document).ready(function(){
    /*$('#event-pic').filestyle({
        icon : false,
        buttonText : ' Browse',
        'placeholder': 'If Event Banner is not uploaded, we will use default'
    });
    $('#TnC').filestyle({
        icon : false,
        buttonText : ' Browse',
        'placeholder': 'Upload T&amp;C doc file'
    });*/
    $('#upload2').filestyle({
        icon : false,
        buttonText : ' Browse',
        'placeholder': 'Upload banner image'
    });
    $("#category").change(function(){
        var $curVal = $(this).val();
        if($curVal == 'others')
        {
            $(".categorydiv").removeClass('hide');
            $('.eventTypeRow div').addClass('col-md-4').removeClass('col-md-6');
            $("#othercat").removeAttr('disabled').focus();
        }
        else
        {
            $(".categorydiv").addClass('hide');
            $('.eventTypeRow div').addClass('col-md-6').removeClass('col-md-4');
            $("#othercat").attr('disabled','disabled');
        }
    });
    attachDateSelectorEvents(".datestart");
    
    $("input[name='eventtypebyfees']").on('click',function(){
        if((eventdefault['typesbasedonfees'][$(this).val()]['processingfee'] + eventdefault['typesbasedonfees'][$(this).val()]['gatewayfee'])>0)
        {
            //$(".date-element").removeClass('hide').removeClass('col-md-6').addClass('col-md-4');
            $(".date-element .datebookingends").attr('placeholder','Last date of booking');
        }
        else
        {
            //$(".paid-event").addClass('hide');
            //$(".date-element").removeClass('col-md-4').addClass('col-md-6');
            $(".date-element .datebookingends").attr('placeholder','Last date of registration');
        }
    });
    //delayfunction2(['fengyuanchencropper/cropper.js','fengyuanchencropper/croppermain2.js'],'cropperimplementation','',1000);
});
function addMoreDates($elementObj){
	$elementObj.find('.datepicker').removeClass('datepickeradded');
	try{
	$elementObj.find('.datepicker').data("datetimepicker").destroy();
	}
	catch(e){
	}
	bootstrapdatetimepickermin_callback();
	setTimeout(function(){
		attachDateSelectorEvents($elementObj.find('.datestart'));
	},1000);
	
}
function attachDateSelectorEvents($selector){
	//return;
	if(typeof $selector == 'string')
	$selector = $($selector);
	$selector.blur(function(){
		console.log('blur triggered');
		var $options = {};
		var $currentrow = $selector.parents('.row').eq(0);
		$options['pickDate'] = true;
		$options['pickTime'] = true;
		if($currentrow.find(".datestart").data('type') == 'datepicker')
		{
		$options['outputFormat'] = "dd/MM/yyyy";
		$options['pickTime'] = false;
		}
		else
		{
		$options['outputFormat'] = "dd/MM/yyyy hh:mm:ss";
		$options['pickTime'] = true;
		}
		$options['startDate'] = $currentrow.find(".datestart").data("datetimepicker").getDate();
		$currentrow.find(".dateend").data("datetimepicker").destroy();
		$currentrow.find(".dateend").datetimepicker($options);
		$options['startDate'] = currentFullDate();
		$options['endDate'] = $currentrow.find(".datestart").data("datetimepicker").getDate();
		$currentrow.find(".datebookingends").data("datetimepicker").destroy();
		$currentrow.find(".datebookingends").datetimepicker($options);
	});
}
var marker,placesearch;
function initGoogleMapsAutocomplete(){
    map_position_obj = $.parseJSON(map_position);
    map = new google.maps.Map(document.getElementById('map'), {
      center: map_position_obj,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl :false,
      streetViewControl :false
    });
    
    var input = document.getElementById('map-input');
    google.maps.event.addDomListener(input, 'keydown', function(e) { 
	    if (e.keyCode == 13) { 
		e.preventDefault(); 
	    }
    }); 
    var searchBox = new google.maps.places.Autocomplete(input);
    //searchBox.setTypes(['establishment']);
    
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });
    //map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);
    //map.setZoom(Math.min(map.getZoom(),12));
    
    var markers = [];
    
    searchBox.addListener('place_changed', function(event) {
        var place = searchBox.getPlace();
        if (!place && !place.geometry) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        //places.forEach(function(place) {
        placesearch = place;
        var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          marker = new google.maps.Marker({
            map: map,
            title: place.name,
            position: place.geometry.location,
            draggable:true
          });
          marker.addListener('drag', locchangedMarker);
          markers.push(marker);

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        //});
        map.fitBounds(bounds);
        locchangedSearch();
  });
  
}
function locchangedSearch(){
    $location = getLocationFromPlaceObject(placesearch);
    if($location)
    processLocation($location);
    else
    locchangedMarker();
}
function locchangedMarker(){
    delayfunction('geocodePositionFromMarker',[marker,'processLocation'],500);
}
function processLocation($location){
    $staticimage = maptoimage();
    $("#map_loc_coordinate").remove();
    $("#map_loc_image").remove();
    //var $location = geocodePositionFromMarker(marker);
    if($location)
    {
        $("#address").html($location['formatted_address']);
        //$("#map-input").val('');
        //console.log(marker);
        $("#map-input").closest('form').append(
            $('<input>').attr({
                type: 'hidden',
                id: 'map_loc_coordinate',
                name: 'map_loc_coordinate',
                value:'{"lat":'+marker.position.lat()+',"lng":'+marker.position.lng()+'}'
            })
        ).append(
            $('<input>').attr({
                type: 'hidden',
                id: 'map_loc_image',
                name: 'map_loc_image',
                value:$staticimage
            })
        ).append(
            $('<input>').attr({
                type: 'hidden',
                id: 'map_loc_components',
                name: 'map_loc_components',
                value:JSON.stringify($location['components'])
            })
        )
        if($location['components']['name'])
        $("#map-input").val($location['components']['name']);
        else
        {
            $searchText = $("#map-input").val();
            $locationName = $searchText.split(",");
            $("#map-input").val($locationName[0]);
        }
    }
}
