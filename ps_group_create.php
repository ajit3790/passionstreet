<?php
require_once('initialise.php');

$rows = array(
    "row_1" => array(
        'sectionParams' => array(
            'class' => 'section'
        ),
        'rowWrapper' => array("<div class='container sm'>","</div>"),
        'modules' => array(
            'groupCreate' => array(
                'modulesParams' => array()
            )
        )
    )
);

$page_meta = array(
	'title'=>'Community'
);

echo render_modules();

?>