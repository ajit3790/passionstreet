<?php
require_once('initialise.php');
//$PSJavascript['login_required'] = false;
$rows = array(
    "row_2"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
              'rowWrapper'=>array("<div class='container sm'>","</div>"),
             
             'columnStructure' =>true,
             'columns'=>array(
                 array(
                     'columnParams'=>array(
                        'class'=>'col-md-12'
                     ),
                    'modules'=>array(
                         'messagedetail'=>array(
                            'modulesParams'=>array()
                          )                    
                     )
                     
                 )
            )
        )
);

$page_meta['title'] = 'My Messages on PASSIONSTREET';
$page_meta['description'] = 'Check all the messages received .';

if(!loggedId())
header('location:'.ROOT_PATH);
echo render_modules();

?>