<?php
require_once('initialise.php');
$rows = array(
    "row_1"=>array(
            'sectionParams'=>array(
                'class'=>'section'
             ),
	    'rowWrapper'=>array("<div class='container sm'>","</div>"),
            'modules'=>array(
                'admintools'=>array(
                    'modulesParams'=>array()
                )
            )
        )
);

$page_meta = array(
	'title'=>'Admin Tools'
);

$PSParams['blockbots'] = 1;

if(!loggedId() || !isAdminLoggedIn())
header('location:'.ROOT_PATH); 

echo render_modules();

?>